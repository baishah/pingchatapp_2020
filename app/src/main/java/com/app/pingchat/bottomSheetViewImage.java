package com.app.pingchat;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class bottomSheetViewImage extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;

    public String getMediaURL() {
        return mediaURL;
    }

    public void setMediaURL(String mediaURL) {
        this.mediaURL = mediaURL;
    }

    private String mediaURL;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }
    ImageView pic,pic1,avatar;
    VideoView video;
    ImageButton playBtn;
    TextView caption_txt,displayname,timeStart;
    RelativeLayout vRl;
    private DatabaseReference userinfo;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_view_image, container, false);

        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog d = (BottomSheetDialog) dialogInterface;
                View bottomSheetInternal = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        userinfo = FirebaseDatabase.getInstance().getReference().child("Users");
        String key = getArguments().getString("key");
        String imageurl = getArguments().getString("url");
        String vUrl = getArguments().getString("vurl");
        String type = getArguments().getString("type");
        String caption = getArguments().getString("caption");
        String ownerId = getArguments().getString("ownerid");
        String pingid = getArguments().getString("pingid");
        String time = getArguments().getString("datestart");

        pic = v.findViewById(R.id.picture);
        pic1 = v.findViewById(R.id.picture1);
        video = v.findViewById(R.id.video);
        avatar = v.findViewById(R.id.user_dp);
        displayname =v.findViewById(R.id.display_name);
        timeStart = v.findViewById(R.id.time);
        caption_txt = v.findViewById(R.id.caption_txt);
        vRl = v.findViewById(R.id.vRl);
        playBtn = v.findViewById(R.id.playvideo);
        progressBar = v.findViewById(R.id.progrss);

        System.out.println("type:"+type);
//        System.out.println("vUrl:"+imageurl);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getContext());
        circularProgressDrawable.setStrokeWidth(10f);
        circularProgressDrawable.setCenterRadius(50f);
        circularProgressDrawable.start();

        if(type.equals("image")){
//            caption_txt.setText(caption);
            if(key.equals("p")){
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
                GlideApp.with(getContext())
                        .load(storageReference)
                        .placeholder(circularProgressDrawable).into(pic);
            }else{
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
                GlideApp.with(getContext())
                        .load(storageReference)
                        .placeholder(circularProgressDrawable).into(pic);
            }
        }else {
            if(key.equals("p")){
                pic.setVisibility(View.GONE);
                pic1.setVisibility(View.VISIBLE);
                StorageReference storageReference1 = FirebaseStorage.getInstance().getReference().child(vUrl);
                GlideApp.with(getContext())
                        .load(storageReference1)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(circularProgressDrawable)
                        .into(pic1);
                playBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        vRl.setVisibility(View.VISIBLE);
                        pic1.setVisibility(View.GONE);
                        if(!video.isPlaying()){
                            video.start();
                            playBtn.setImageResource(R.drawable.pausewhite);
                        }else{
                            video.pause();
                            playBtn.setImageResource(R.drawable.play_icon1);
                        }
                    }
                });

                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        if(!video.isPlaying()){
                            video.setVideoURI(uri);
                            video.setZOrderMediaOverlay(true);
                            video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mediaPlayer) {
                                    playBtn.setImageResource(R.drawable.play_icon1);
                                }
                            });
                        }else{
                            video.pause();
//                            playBtn.setImageResource(R.drawable.pausewhite);
                        }
                        video.requestFocus();
                        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mediaPlayer) {
                                mediaPlayer.setLooping(true);
                                video.setZOrderOnTop(false);
                                video.setBackgroundColor(Color.TRANSPARENT);
                                progressBar.setVisibility(View.INVISIBLE);
//                                video.start();
//                                playBtn.setImageResource(R.drawable.pausewhite);

                            }
                        });
                    }
                });

                video.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                        if(i == mediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            pic1.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            return true;
                        }
                        else if(i==mediaPlayer.MEDIA_INFO_BUFFERING_START){
                            progressBar.setVisibility(View.VISIBLE);
                        }
                        else if( i== mediaPlayer.MEDIA_INFO_BUFFERING_END){
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        return false;
                    }
                });

            }else{
                vRl.setVisibility(View.VISIBLE);
                pic.setVisibility(View.GONE);
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        if(!video.isPlaying()){
                            video.setVideoURI(uri);
                            video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mediaPlayer) {
                                    playBtn.setImageResource(R.drawable.play_icon1);
                                }
                            });

                        }else{
                            video.pause();
//                            playBtn.setImageResource(R.drawable.pausewhite);

                        }
                        video.requestFocus();
                        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mediaPlayer) {
                                mediaPlayer.setLooping(true);
                                progressBar.setVisibility(View.INVISIBLE);

//                                video.start();
//                                playBtn.setImageResource(R.drawable.pausewhite);

                            }
                        });
                    }
                });
                playBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!video.isPlaying()){
                            video.start();
                            playBtn.setImageResource(R.drawable.pausewhite);
                        }else{
                            video.pause();
                            playBtn.setImageResource(R.drawable.play_icon1);
                        }
                    }
                });
                video.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                        if(i==mediaPlayer.MEDIA_INFO_BUFFERING_START){
                            progressBar.setVisibility(View.VISIBLE);
                        }
                        else if( i== mediaPlayer.MEDIA_INFO_BUFFERING_END){
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        return false;
                    }
                });
            }
        }
        caption_txt.setText(caption);
        userinfo.child(ownerId).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UD_displayName")){
                    String retrievename = dataSnapshot.child("UD_displayName").getValue().toString();
                    displayname.setText(retrievename);
                }
                if(dataSnapshot.hasChild("UD_avatarURL")){
                    String dpicture = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    if (!dpicture.startsWith("h")) {
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(dpicture);
                        GlideApp.with(getContext())
                                .load(storageReference)
                                .into(avatar);
                    } else {
                        GlideApp.with(getContext())
                                .load(dpicture)
                                .into(avatar);
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}

        });

        return v;
    }

    public interface BottomSheetListener {
        void onClickImage(String text);
    }
}

