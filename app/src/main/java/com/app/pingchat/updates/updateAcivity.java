package com.app.pingchat.updates;

import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class updateAcivity extends AppCompatActivity {

    RecyclerView updateList;
    String updateTitle[];
    String updateDescription[];
    ArrayList<UANDROID_class> update_list;
    Button backButton;
    public DatabaseReference update, update_full;
    String current_user;
    FirebaseAuth mAuth;
    public DatabaseReference db_update;
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_activity_layout);

        updateList = findViewById(R.id.updateList);
        mAuth = FirebaseAuth.getInstance();
        current_user = mAuth.getCurrentUser().getUid();
        db_update = FirebaseDatabase.getInstance().getReference().child("Updates");
//            update = FirebaseDatabase.getInstance().getReference().child("Updates").child("ANDROID");
        backButton = findViewById(R.id.backButton);

//            updateTitle = getResources().getStringArray(R.array.updateTitle);
//            updateDescription = getResources().getStringArray(R.array.updateDescription);

        updateList.setLayoutManager(new LinearLayoutManager(this));
        getNewUpdateList();

//            final long unixtime = timestamp.getTime();
//
//            String updateId = update.push().getKey();
//            String desc = "Introducing New PingChatApp";
//            String details = "Thank you for waiting, we releasing the new version of PingChat App. Check out our PingAR, PingQR, Video Message, New Partner Profile, Citizen Alert, New Discover with News, Marketplace, and Ping. Be Unique, Be Different and Stay Awesome!";
//            String version = "1.0";
//            String url = "updates/ANDROID/"+version+".jpeg";
//            update_full = FirebaseDatabase.getInstance().getReference().child("Updates");
//
//            final Map Up_android = new HashMap();
//            Up_android.put("UANDROID_updateID", updateId);
//            Up_android.put("UANDROID_details",details);
//            Up_android.put("UANDROID_mediaURL", url);
//            Up_android.put("UANDROID_timestamp", unixtime);
//            Up_android.put("UANDROID_version", version);
//            Up_android.put("UANDROID_description",desc);
//
//            update_full.child("ANDROID").child(updateId).setValue(Up_android);

    }

    private void getNewUpdateList() {
        update_list = new ArrayList();

        UpdateListAdapter updateListAdapter = new UpdateListAdapter(this,update_list);
        updateList.setAdapter(updateListAdapter);

        db_update.child("ANDROID").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                        if(dataSnapshot1.hasChild("UANDROID_description")) {
                            String updateID = dataSnapshot1.getKey();
                            System.out.println("updateID:" + updateID);
                            String subTitle = dataSnapshot1.child("UANDROID_description").getValue().toString();
                            String subDesc = dataSnapshot1.child("UANDROID_details").getValue().toString();
                            String updateUrl = dataSnapshot1.child("UANDROID_mediaURL").getValue().toString();
                            long updateDate = (long) dataSnapshot1.child("UANDROID_timestamp").getValue();
                            String version = dataSnapshot1.child("UANDROID_version").getValue().toString();

                            UANDROID_class uandroid_class= new UANDROID_class();
                            uandroid_class.setUANDROID_description(subTitle);
                            uandroid_class.setUANDROID_details(subDesc);
                            uandroid_class.setUANDROID_mediaURL(updateUrl);
                            uandroid_class.setUANDROID_timestamp(updateDate);
                            uandroid_class.setUANDROID_version(version);

                            update_list.add(uandroid_class);
                        }
                        updateListAdapter.notifyDataSetChanged();
                    }

//                    db_update.child(updateID).addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                        }
//
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                        }
//                    });
//                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
//                        String updateID = dataSnapshot1.getKey();
//                        UANDROID_class uandroid_class= dataSnapshot1.child(updateID).getValue(UANDROID_class.class);
//                        String subTitle = uandroid_class.getUANDROID_description();
//                        String subDesc = uandroid_class.getUANDROID_details();
//                        String updateUrl = uandroid_class.getUANDROID_mediaURL();
//                        long updateDate = uandroid_class.getUANDROID_timestamp();
//                        String version = uandroid_class.getUANDROID_version();
//                        final UANDROID_class uandroid_class1 = new UANDROID_class();
//
////                        UANDROID_class uandroid_class= new UANDROID_class();
//                        uandroid_class1.setUANDROID_description(subTitle);
//                        uandroid_class1.setUANDROID_details(subDesc);
//                        uandroid_class1.setUANDROID_mediaURL(updateUrl);
//                        uandroid_class1.setUANDROID_timestamp(updateDate);
//                        uandroid_class1.setUANDROID_version(version);
//
//                        update_list.add(uandroid_class1);
//                    }
//                    updateListAdapter.notifyDataSetChanged();
//                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
