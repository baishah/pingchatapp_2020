package com.app.pingchat.updates;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.app.pingchat.donation.DonationItem;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class UpdateListAdapter extends RecyclerView.Adapter<UpdateListAdapter.UpdateListViewHolder> {
    int i=1;
    Context context;
    String updateTitleData[], updateDescriptionData[];
    private int positionSelected = -1;
    boolean isTextViewClicked = false;
    ArrayList<UANDROID_class> updateList;
    StorageReference updateimage,storeRef;
    FirebaseStorage storage;

    public UpdateListAdapter(Context ct, ArrayList<UANDROID_class> up_list ) {
        context = ct;
        this.updateList = up_list;
    }


    @NonNull
    @Override
    public UpdateListAdapter.UpdateListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.update_layout_item1, parent, false);
        return new UpdateListViewHolder(view);
    }


    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onBindViewHolder(@NonNull UpdateListAdapter.UpdateListViewHolder holder, int position) {
        final UANDROID_class updateClass = updateList.get(position);
        String update_title = updateClass.getUANDROID_description();
        String update_details = updateClass.getUANDROID_details();
        String update_version = updateClass.getUANDROID_version();
        String url = updateClass.getUANDROID_mediaURL();
        long date = updateClass.getUANDROID_timestamp();
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
        String dateString = format.format(new Date((date)));

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(10f);
        circularProgressDrawable.setCenterRadius(50f);
        circularProgressDrawable.start();

        updateimage = FirebaseStorage.getInstance().getReference().child("updates").child("ANDROID");
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();


        //To get the initial hight before text is inserted(empty TextView).
//        holder.updateDescription.measure(0,0);
//        int initial_height = holder.updateDescription.getMeasuredHeight();
        holder.updateTitle.setText(update_title);
        holder.updateDescription.setText(update_details);
        holder.updateTime.setText(dateString);
        holder.updateVersion.setText(update_version);


        storeRef.child(url).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                GlideApp.with(context.getApplicationContext())
                        .load(uri)
                        .placeholder(circularProgressDrawable)
                        .into(holder.updateImage);
//                System.out.println("media_url:"+uri);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });

    }


    @Override
    public int getItemCount() {
        return updateList.size();
    }

    public class UpdateListViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout updateItem;
        RelativeLayout updateRow;
        TextView updateTitle, updateVersion, updateTime;
        TextView updateDescription,less;
        ImageView updateImage;


        public UpdateListViewHolder(@NonNull View itemView) {
            super(itemView);

            updateItem = itemView.findViewById(R.id.updateItem);
            updateRow = itemView.findViewById(R.id.updateRow);
            updateTitle = itemView.findViewById(R.id.subTitle);
            updateDescription = itemView.findViewById(R.id.subDescription);
            updateTime = itemView.findViewById(R.id.updatedtime);
            updateVersion = itemView.findViewById(R.id.updatedversion);
            less = itemView.findViewById(R.id.lesss);
            updateImage = itemView.findViewById(R.id.image_update);

            updateDescription.post(new Runnable() {
                @Override
                public void run() {
                    updateDescription.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isTextViewClicked) {
                                //This will shrink textview to 2 lines if it is expanded.
                                updateDescription.setMaxLines(1);
                                isTextViewClicked = false;
                                System.out.println("false");
                                less.setVisibility(View.GONE);
                            } else {
                                //This will expand the textview if it is of 2 lines
                                updateDescription.setMaxLines(Integer.MAX_VALUE);
                                isTextViewClicked = true;
                                System.out.println("true");
                                less.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    less.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            less.setVisibility(View.GONE);
                            System.out.println("less");
                            updateDescription.setMaxLines(1);
                        }
                    });
                }
            });
        }
    }
}

