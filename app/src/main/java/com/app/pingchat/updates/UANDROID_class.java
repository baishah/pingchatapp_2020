package com.app.pingchat.updates;

public class UANDROID_class {

    public String UANDROID_updateID;
    public String UANDROID_details;
    public String UANDROID_mediaURL;
    public long UANDROID_timestamp;
    public String UANDROID_version;
    public String UANDROID_mediaType;
    public String UANDROID_description;

    public String getUANDROID_updateID() {
        return UANDROID_updateID;
    }

    public void setUANDROID_updateID(String UANDROID_updateID) {
        this.UANDROID_updateID = UANDROID_updateID;
    }

    public String getUANDROID_details() {
        return UANDROID_details;
    }

    public void setUANDROID_details(String UANDROID_details) {
        this.UANDROID_details = UANDROID_details;
    }

    public String getUANDROID_mediaURL() {
        return UANDROID_mediaURL;
    }

    public void setUANDROID_mediaURL(String UANDROID_mediaURL) {
        this.UANDROID_mediaURL = UANDROID_mediaURL;
    }

    public long getUANDROID_timestamp() {
        return UANDROID_timestamp;
    }

    public void setUANDROID_timestamp(long UANDROID_timestamp) {
        this.UANDROID_timestamp = UANDROID_timestamp;
    }

    public String getUANDROID_version() {
        return UANDROID_version;
    }

    public void setUANDROID_version(String UANDROID_version) {
        this.UANDROID_version = UANDROID_version;
    }

    public String getUANDROID_mediaType() {
        return UANDROID_mediaType;
    }

    public void setUANDROID_mediaType(String UANDROID_mediaType) {
        this.UANDROID_mediaType = UANDROID_mediaType;
    }

    public String getUANDROID_description() {
        return UANDROID_description;
    }

    public void setUANDROID_description(String UANDROID_description) {
        this.UANDROID_description = UANDROID_description;
    }
}
