package com.app.pingchat;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.app.pingchat.Profile.ViewProfileActivityAfterScan;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRcodecamera extends AppCompatActivity {

    ZXingScannerView qrCodeScanner;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.empty);
        scan();

    }

    ZXingScannerView.ResultHandler v = new ZXingScannerView.ResultHandler() {
        @Override
        public void handleResult(Result result) {
          //  qrCodeScanner.animate();Toast.makeText(getApplicationContext(),"name: "+result.getText().toString(),Toast.LENGTH_SHORT).show();
         //   qrCodeScanner.resumeCameraPreview(v);
            String id = result.getText().toString();
            Intent intent = new Intent(QRcodecamera.this, ViewProfileActivityAfterScan.class);
            intent.putExtra("id",id);
            startActivity(intent);
        }
    };


    public void scan(){
        qrCodeScanner =new ZXingScannerView(this);
        setContentView(qrCodeScanner);
        qrCodeScanner.setMinimumHeight(50);
        qrCodeScanner.setMinimumWidth(50);
        qrCodeScanner.setResultHandler(v);
        qrCodeScanner.startCamera();

    }

        @Override public void onResume(){
        super.onResume();
        scan();
    }

    @Override
    public void onBackPressed(){
        qrCodeScanner.stopCamera();
        finish();
    }
}
