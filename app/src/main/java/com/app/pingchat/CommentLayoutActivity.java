package com.app.pingchat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Discover.DiscoveryComments;
import com.app.pingchat.Discover.DiscoveryCommentsAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class CommentLayoutActivity extends AppCompatActivity {

    RecyclerView commentlist;
    private RequestQueue mQueue;
    private ArrayList<DiscoveryComments> commentarraylist = new ArrayList<>();
    private RecyclerView.Adapter adapter;
    EditText typecomment;
    Button sendcomment;

    String typedcomment;
    String discoverid,currentuser,ownerid,friendId;
    DatabaseReference user;
    String datetimes;
    String datess;
    DiscoveryComments comments;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_section_layout);

        Intent i = getIntent();
        discoverid = i.getStringExtra("discoverpostid");
        currentuser = i.getStringExtra("currentuser");
        friendId = i.getStringExtra("friendID");
        ownerid = i.getStringExtra("ownerid");

        System.out.println("discoverid"+discoverid);
        typecomment = findViewById(R.id.type_comment);
        sendcomment = findViewById(R.id.send_comment);

        commentlist = findViewById(R.id.commentlist);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        commentlist.setLayoutManager(layoutManager);
        layoutManager.setStackFromEnd(true);
        mQueue  = Volley.newRequestQueue(this);
        user = FirebaseDatabase.getInstance().getReference().child("Users");
        getComment(discoverid);
        sendcomment.setEnabled(false);

        typecomment.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(typecomment.length() == 0) {
                    sendcomment.setEnabled(false);
                    //Toast.makeText(ChatsActivity.this, "Cannot send an empty message", Toast.LENGTH_SHORT).show();
                }
                else{
                    sendcomment.setEnabled(true);

                }
            }
        });

        sendcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
            }
        });

        adapter = new DiscoveryCommentsAdapter(this,commentarraylist);
    }

    public void onStart(){
        super.onStart();
    }
//
public void sendComment(){
    final ProgressDialog progressDialog = new ProgressDialog(this);
    progressDialog.setMessage("sending message");
    progressDialog.show();
    typedcomment = typecomment.getText().toString();
    String commentid = user.push().getKey();

    final Map UDS_Comment = new HashMap();
    UDS_Comment.put("UDSC_ownerID",ownerid);
    UDS_Comment.put("UDSC_comment",typedcomment);
    UDS_Comment.put("UDSC_timestamp", ServerValue.TIMESTAMP);
    UDS_Comment.put("UDSC_mediaType","image");
    UDS_Comment.put("UDSC_mediaURL", ServerValue.TIMESTAMP);

    final Map UDS_Comment_noMedia = new HashMap();
    UDS_Comment_noMedia.put("UDSC_ownerID",currentuser);
    UDS_Comment_noMedia.put("UDSC_comment",typedcomment);
    UDS_Comment_noMedia.put("UDSC_timestamp",ServerValue.TIMESTAMP);
    UDS_Comment_noMedia.put("UDSC_mediaType","noMedia");

    final Date d = new Date();
    DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(d);
    calendar.add(Calendar.HOUR_OF_DAY,24);
    String jj = calendar.getTime().toString();
    String hh = dateFormat.format(new Date(jj)).toString();

    long unixtime = System.currentTimeMillis() / 1000L;

    String timetemporary = (unixtime)+"000";

    user.child(ownerid).child("userDiscovers").child("UDS_List").child(discoverid).child("UDS_Comments").child(commentid).setValue(UDS_Comment_noMedia).addOnCompleteListener(new OnCompleteListener<Void>() {
        @Override
        public void onComplete(@NonNull Task<Void> task) {
            if(task.isSuccessful()){
                progressDialog.dismiss();
                commentNoti(discoverid);
                user.child(currentuser).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String dname = dataSnapshot.child("UD_displayName").getValue().toString();
                        String avatarurl = dataSnapshot.child("UD_avatarURL").getValue().toString();

                        System.out.println("dname from bottom sheet :"+dname);
                        comments = new DiscoveryComments();
                        comments.setUsername(dname);
                        comments.setUser_profile(avatarurl);
                        comments.setUDSC_comment(typedcomment);
                        comments.setUDSC_timestamp(getdatetime(Long.parseLong(timetemporary)));
                        commentarraylist.add(comments);
                        if(!commentarraylist.isEmpty()){
                            adapter.notifyItemInserted(commentarraylist.size()-1);
                        }
                        else {
                            commentlist.setAdapter(adapter);
                            adapter.notifyItemInserted(0);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
        }
    });
    typecomment.setText("");
}

    public void getComment(String discoverpostid){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading comments");
        progressDialog.show();
        commentarraylist = new ArrayList<>();

        user.child(ownerid).child("userDiscovers").child("UDS_List").child(discoverpostid).child("UDS_Comments").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "No comment for this post", Toast.LENGTH_SHORT).show();
                    commentlist.setAdapter(adapter);
                }
                else{
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        final DiscoveryComments comments = new DiscoveryComments();
                        String comment = dataSnapshot1.child("UDSC_comment").getValue().toString();
                        long timestamps = Long.parseLong(dataSnapshot1.child("UDSC_timestamp").getValue().toString());
                        String commentownerid = dataSnapshot1.child("UDSC_ownerID").getValue().toString();

                        comments.setUDSC_timestamp(getdatetime(timestamps));
                        comments.setUDSC_comment(comment);

                        user.child(commentownerid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                String picture = dataSnapshot.child("UD_avatarURL").getValue().toString();

                                comments.setUsername(name);
                                comments.setUser_profile(picture);
                                commentarraylist.add(comments);

                                commentlist.setAdapter(adapter);
                                progressDialog.dismiss();
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void commentNoti(String discoverid){
        RequestQueue queue = Volley.newRequestQueue(this);
        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/notification/"+currentuser+"/commentDiscover/"+ownerid;


        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
        postParams.put("discoverPostID",discoverid);
        postParams.put("userSenderID",currentuser);
        postParams.put("userReceiverID",ownerid);
//        System.out.println("chatid:"+chat_id);
//        System.out.println("url:"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }


    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Date  currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        String dateplusone = df.format(ccc);
        datess = dateplusone;
        return datess;
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);
        System.out.println("datetimestamp"+datetimestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
            System.out.println("datetimes"+datetimes);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = "2 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = "3 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = "4 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = "5 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = "6 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = "7 days ago";
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

}
