package com.app.pingchat.CitizenAlert;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Map.MapsFragment;
import com.app.pingchat.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AlertAdapter extends RecyclerView.Adapter<AlertAdapter.MyViewHolder> {

    ArrayList<UCA_Details> alertMetaDataList;
    Context context;
    String currentuser;
    double currentlan,currentlon;
    alertrecycleradapterlist alistlistener;


    public AlertAdapter(ArrayList<UCA_Details> alertMetaData){
        alertMetaDataList = alertMetaData;
    }

    public AlertAdapter(Context c, ArrayList<UCA_Details> d, String user,alertrecycleradapterlist clicklistener){
        context = c;
        alertMetaDataList = d;
        currentuser = user;
        alistlistener = clicklistener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.alert_sel_layout,parent,false);
        return new MyViewHolder (v,alistlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final UCA_Details newalert = alertMetaDataList.get(position);
        String typeAlert = newalert.getUCAD_alertType();
        String ownerID = newalert.getUCAD_ownerID();
//        String newLat = String.valueOf(newalert.getUCAL_latitude());
//        String newLon = String.valueOf(newalert.getUCAL_longitude());
//        double latialert = Double.parseDouble(newLat);
//        double longalert = Double.parseDouble(newLon);
//        double radiusInMeters = 500.0;
//        float[] distance = new float[2];
//
        System.out.println("jumlah alert adapter: "+alertMetaDataList.size());
//
//        currentlan = MapsFragment.getInstance().lastlatitude;
//        currentlon = MapsFragment.getInstance().lastlongitude;
//        Location.distanceBetween(currentlan, currentlon, latialert, longalert, distance);

            if(typeAlert.equals("accident")){
                holder.i.setImageResource(R.drawable.accident);
            }else if (typeAlert.equals("landslide")) {
                holder.i.setImageResource(R.drawable.landslide);
            }else if (typeAlert.equals("rain")) {
                holder.i.setImageResource(R.drawable.rain);
            }else if (typeAlert.equals("trafficJam")) {
                holder.i.setImageResource(R.drawable.trafficjam);
            }else if (typeAlert.equals("police")) {
                holder.i.setImageResource(R.drawable.police);
            }else if (typeAlert.equals("closure")) {
                holder.i.setImageResource(R.drawable.closure);
            }else if (typeAlert.equals("hazard")) {
                holder.i.setImageResource(R.drawable.hazard);
            }else if (typeAlert.equals("trafficLight")) {
                holder.i.setImageResource(R.drawable.trafficlight);
            }else if (typeAlert.equals("hot")) {
                holder.i.setImageResource(R.drawable.hot);
            }
//        }

    }

    @Override
    public int getItemCount() {
        return alertMetaDataList.size();
    }

    public interface alertrecycleradapterlist{
        void onUserClickedalertlist(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView i;
        alertrecycleradapterlist alistlistener;

        public MyViewHolder(@NonNull View itemView, alertrecycleradapterlist  clicklistener) {
            super(itemView);

            i = itemView.findViewById(R.id.user_dp);
            alistlistener = clicklistener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            alistlistener.onUserClickedalertlist(getAdapterPosition());
        }
    }
}
