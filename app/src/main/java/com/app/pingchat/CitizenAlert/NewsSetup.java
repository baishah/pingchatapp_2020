package com.app.pingchat.CitizenAlert;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.GlideApp;
import com.app.pingchat.Ping.CompleteSendPing;
import com.app.pingchat.Ping.PingSetup;
import com.app.pingchat.PlaceFieldSelector;
import com.app.pingchat.R;
import com.app.pingchat.bottomSheetAlertMenu;
import com.app.pingchat.bottomSheetMapMenu;
import com.app.pingchat.bottomSheetSetDuration;
import com.app.pingchat.filterActivitity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class NewsSetup extends AppCompatActivity implements OnMapReadyCallback,GoogleApiClient.OnConnectionFailedListener{

    private TextView uname,type,photo,cancel,location_box,duration;
    private FloatingActionButton proceedping;
    private ImageView photo_p_holder;
    private FirebaseAuth mAuth;
    private CircleImageView dp;
    private GoogleMap map;
//    private EditText caption;
    private Button done;
    private byte[] avatar;
    private long timeend;
    private int hours;

    private DatabaseReference userCitizenAlert;
    public StorageReference citizenalertimage;

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private BitmapDescriptor icon;

    String pingloc,captions,durations,alerts,alertsTxt,durationhour,posttypess, durationnum;
    double pinglat,pinglon;


    public void setPingloc(String loc) { this.pingloc = loc; }
    public String getPingloc() { return pingloc; }

    public int getHours() { return hours; }

    public void setHours(int hours) { this.hours = hours; }


    public void setPinglat(double lat){this.pinglat = lat;}
    public double getPinglat(){return pinglat; }

    public void setPinglon(double lon){this.pinglon = lon;}
    public double getPinglon(){return pinglon; }

    public String getDurationHour() {
        return durationhour;
    }

    public void setDurationhour(String durationhour) {
        this.durationhour = durationhour;
    }

    private Location mLastLocation;
    private GoogleApiClient googleApiClient;

    DatabaseReference locations;
    String currentuser;
    String value = null;

    private BottomSheetBehavior bottomSheetBehavior;
    private bottomSheetSetDuration bottomSheetSetDuration;
//    private bottomSheetAlertMenu bottomSheetAlertMenu;
    private View bottom_sheet_ping_detail;

    private String api_key = "AIzaSyDOewqTvU226Fe7ZTYIobI58oJomTnPIMQ";



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAnimation();
        setContentView(R.layout.news_setup_layout);

        mAuth = FirebaseAuth.getInstance();
        currentuser = mAuth.getCurrentUser().getUid();
        userCitizenAlert = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userCitizenAlert");
        citizenalertimage = FirebaseStorage.getInstance().getReference().child("citizenAlertImage");

//        bottom_sheet_ping_detail = findViewById(R.id.bottom_ping_detail);
//        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_ping_detail);

//        bottomSheetSetDuration = new bottomSheetSetDuration();
//        bottomSheetSetDuration.setListener(this);

//        bottomSheetAlertMenu = new bottomSheetAlertMenu();
//        bottomSheetAlertMenu.setListener(this);

        cancel = (TextView) findViewById(R.id.cancel);
        uname = findViewById(R.id.username);
        dp = findViewById(R.id.user_dp);
//        type = findViewById(R.id.type);
//        photo = findViewById(R.id.photo);
//        photo_p_holder = findViewById(R.id.photo_p_holder);
//        duration = findViewById(R.id.duration);
//        caption = findViewById(R.id.ed_caption);
//        alert = findViewById(R.id.ed_title);
        done = findViewById(R.id.done);

        proceedping = findViewById(R.id.proceedping);
        location_box = findViewById(R.id.location_box);
        icon = BitmapDescriptorFactory.fromResource(R.drawable.alert_marker_);

        ImageButton search = findViewById(R.id.search);
        Intent intent = getIntent();

//        uname.setText(intent.getStringExtra("username"));
//
//        String avatar = intent.getStringExtra("avatar");


        pickAlert();

//        if(avatar.startsWith("p")){
//            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
//            if(getApplicationContext()!=null){
//                GlideApp.with(getApplicationContext())
//                        .load(storageReference)
//                        .into(dp);
//            }
//        }
//        else if(avatar.startsWith("h")){
//            if(getApplicationContext()!=null){
//                GlideApp.with(getApplicationContext().getApplicationContext())
//                        .load(avatar)
//                        .into(dp);
//            }
//        }

//        done.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                captions = caption.getText().toString().trim();
////                durations = duration.getText().toString().trim();
////                alerts = alert.getText().toString().trim();
//                if((getAvatar()!=null)&&(captions!=null && !captions.isEmpty())&&(getPingloc()!=null || !getPingloc().isEmpty())&& !durations.equals("Duration") && (alert!=null)){
//                    newalert(getHours());
//                }
//                else{
//                    Toast.makeText(NewsSetup.this,"Please fill in all the information",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//        duration.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                bottomSheetSetDuration.show(getSupportFragmentManager(),"example");
//            }
//        });

//        alert.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                bottomSheetAlertMenu.show(getSupportFragmentManager(),"e");
//            }
//        });
//
//        type.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               if(type.getText() == "Public"){
//                   type.setText("Friend");
//               }
//               else{
//                   type.setText("Public");
//               }
//            }
//        });

//        photo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
//                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
//                }
//                else{
//                    CropImage.startPickImageActivity(NewsSetup.this);
//                }
//            }
//        });

//        photo_p_holder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                    CropImage.startPickImageActivity(NewsSetup.this);
//            }
//        });

        Places.initialize(getApplicationContext(),api_key);

        if (!Places.isInitialized()) {
            Places.
                    initialize(getApplicationContext(), api_key);
        }

        PlaceFieldSelector fieldSelector = new PlaceFieldSelector();

        locations = FirebaseDatabase.getInstance().getReference().child("Users");
        search.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN,fieldSelector.getAllFields()).build(NewsSetup.this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            }
        });

       cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        }

        public void pickAlert(){
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_alert_type, null);
        Button hot, rain, landslide, hazard, trafficlight, accident, police, closure, trafficjam;
        final Button next, exitBtn;

        hot = view.findViewById(R.id.hot);
        rain = view.findViewById(R.id.rain);
        landslide = view.findViewById(R.id.landslide);
        hazard = view.findViewById(R.id.hazard);
        trafficlight = view.findViewById(R.id.trafficlight);
        accident = view.findViewById(R.id.accident);
        police = view.findViewById(R.id.police);
        closure = view.findViewById(R.id.closure);
        trafficjam = view.findViewById(R.id.trafficjam);
        next = view.findViewById(R.id.nextBtn);
        exitBtn = view.findViewById(R.id.exit_alert);

        next.setEnabled(false);
        hot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked:hot");
                value ="hot";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        rain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked:rain");
                value ="rain";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        landslide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked:landslide");
                value ="landslide";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        hazard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked:hazard");
                value ="hazard";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        trafficlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked:trafficLight");
                value ="trafficLight";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        accident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked:accident");
                value ="accident";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked:police");
                value ="police";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        closure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               System.out.println("clicked:closure");
                value ="closure";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        trafficjam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked:trafficJam");
                value ="trafficJam";
                next.setEnabled(true);
                next.setBackgroundResource(R.drawable.round_blue);
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("okay sini");
                alerts = value;
                alertDialog.dismiss();
            }
        });
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        proceedping.hide();

        proceedping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                openSetupDialog();
            }
        });
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                map.clear();
                map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("location :"+latLng)
                        .icon(icon)
                        .draggable(true));
                setPingloc(getCompleteAddressString(latLng.latitude,latLng.longitude));
                location_box.setText(getCompleteAddressString(latLng.latitude,latLng.longitude));
                setPinglat(latLng.latitude);
                setPinglon(latLng.longitude);
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                proceedping.show();
            }
        });
      displayuserLocation();
    }

    private void openSetupDialog() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_alertcaption, null);
        final EditText caption_text;
        final TextView alert_txt;
        final Button type_status, duration_btn, upload_btn,mediabtn, exitAlert;

        caption_text= view.findViewById(R.id.ed_caption);
        alert_txt = view.findViewById(R.id.ed_title);
        type_status = view.findViewById(R.id.statusBtn);
        duration_btn = view.findViewById(R.id.durationBtn);
        upload_btn = view.findViewById(R.id.uploadBtn);
        mediabtn = view.findViewById(R.id.mediaBtn);
        exitAlert = view.findViewById(R.id.exit_alert);
        alert_txt.setText(alerts);

        type_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type_status.getText() == "Public"){
                    type_status.setText("Friend");
                    type_status.setTextColor(Color.parseColor("#FFFFFF"));
                    type_status.setBackgroundResource(R.drawable.round_blue);

                }
                else{
                    type_status.setText("Public");
                    type_status.setTextColor(Color.parseColor("#979797"));
                    type_status.setBackgroundResource(R.drawable.round_stroke_grey);
                }
            }
        });

        duration_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(duration_btn.getText() == "Duration"){
                    duration_btn.setText("3 hours");
                    durationnum = "3";
                    duration_btn.setTextColor(Color.parseColor("#FFFFFF"));
                    duration_btn.setBackgroundResource(R.drawable.round_blue);
                } else if(duration_btn.getText() == "3 hours"){
                    duration_btn.setText("6 hours");
                    durationnum = "6";
                    duration_btn.setTextColor(Color.parseColor("#FFFFFF"));
                    duration_btn.setBackgroundResource(R.drawable.round_blue);
                }else if(duration_btn.getText() == "6 hours"){
                    duration_btn.setText("12 hours");
                    durationnum = "12";
                    duration_btn.setTextColor(Color.parseColor("#FFFFFF"));
                    duration_btn.setBackgroundResource(R.drawable.round_blue);
                }else if(duration_btn.getText() == "3 hours"){
                    duration_btn.setText("Duration");
                    duration_btn.setTextColor(Color.parseColor("#979797"));
                    duration_btn.setBackgroundResource(R.drawable.round_stroke_grey);
                }else{
                    duration_btn.setText("Duration");
                    duration_btn.setTextColor(Color.parseColor("#979797"));
                    duration_btn.setBackgroundResource(R.drawable.round_stroke_grey);
                }
            }
        });

        mediabtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
                }
                else{
                    CropImage.startPickImageActivity(NewsSetup.this);
                }
            }
        });

        upload_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                posttypess = type_status.getText().toString().toLowerCase();
                captions = caption_text.getText().toString().trim();
                durations = duration_btn.getText().toString().trim();
                System.out.println("duration:"+durationnum);
                alertsTxt = alert_txt.getText().toString().trim();
                if((getAvatar()!=null)&& (captions!=null && !captions.isEmpty())&&(getPingloc()!=null || !getPingloc().isEmpty())&& !durations.equals("Duration") && (alert_txt!=null)){
                    alertDialog.dismiss();
                    setHours(Integer.valueOf(durationnum));
                    confirmUpload();
                } else{
                    Toast.makeText(NewsSetup.this,"Please fill in all the information",Toast.LENGTH_SHORT).show();
                }
            }
        });

        exitAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                confirmExit();
                alertDialog.dismiss();
            }
        });


        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);

    }

//    private void confirmExit() {
//        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
//        androidx.appcompat.app.AlertDialog alertDialog = builder.create();
//        View view = LayoutInflater.from(this).inflate(R.layout.dialog_alertexit, null);
//        final Button done;
//
//        done = view.findViewById(R.id.btndone);
//        done.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                newalert(getHours());
//            }
//        });
//
//        alertDialog.setView(view);
//        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        alertDialog.show();
//        alertDialog.setCanceledOnTouchOutside(true);
//    }

    private void confirmUpload() {
        map.clear();
        proceedping.hide();
        androidx.appcompat.app.AlertDialog.Builder builder_1 = new androidx.appcompat.app.AlertDialog.Builder(this);
        androidx.appcompat.app.AlertDialog alertDialog_1 = builder_1.create();
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_alertconfirm, null);
        final Button done;

        done = view.findViewById(R.id.btndone);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newalert(getHours());
                alertDialog_1.dismiss();
            }
        });

        alertDialog_1.setView(view);
        alertDialog_1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog_1.show();
        alertDialog_1.setCanceledOnTouchOutside(true);
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        if (requestCode ==PLACE_AUTOCOMPLETE_REQUEST_CODE){
            if (resultCode == RESULT_OK) {
                Place place;
                place = Autocomplete.getPlaceFromIntent(data);
                String place2 = place.getAddress();
                setPingloc(place2);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
                map.clear();
                map.addMarker(new MarkerOptions()
                        .position(place.getLatLng())
                        .title("location :"+place.getLatLng())
                        .icon(icon)
                        .draggable(true));
                setPingloc(getCompleteAddressString(place.getLatLng().latitude,place.getLatLng().longitude));
                location_box.setText(getCompleteAddressString(place.getLatLng().latitude,place.getLatLng().longitude));
                setPinglat(place.getLatLng().latitude);
                setPinglon(place.getLatLng().longitude);
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                proceedping.show();

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(getIntent());
                // TODO: Handle the error.
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = getApplicationContext().getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it

//                    photo.setVisibility(View.GONE);
//                    photo_p_holder.setVisibility(View.VISIBLE);
//                    photo_p_holder.setImageBitmap(selectedImage);
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    setAvatar(datas);

                    Intent i = new Intent(this, filterActivitity.class);
                    i.putExtra("Image",datas);
                    startActivityForResult(i,100);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == 100){
            if(resultCode == Activity.RESULT_OK){
                byte[] bytes = data.getByteArrayExtra("PUBLIC_STRING_IDENTIFIER");
                Bitmap b = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                setAvatar(bytes);
//                photo.setVisibility(View.GONE);
//                photo_p_holder.setImageBitmap(b);
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(this,data);

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(this);
        }
        }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(NewsSetup.this);
        }
    }

    private void setAvatar(byte[] bytes) { this.avatar = bytes; }
    public byte[] getAvatar() { return avatar; }

    private String getCompleteAddressString(double lat,double lon){
        String strrAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try{
            List<Address> myAddress = geocoder.getFromLocation(lat,lon,1);
            if(myAddress!=null){
                Address returnedAdd = myAddress.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for(int i=0;i<=returnedAdd.getMaxAddressLineIndex();i++){
                    strReturnedAddress.append(returnedAdd.getAddressLine(i)).append("\n");
                }
                strrAdd = strReturnedAddress.toString();
                Log.w("location address", strReturnedAddress.toString());
            }
            else{
                Log.w("location address", "No Address returned!");
            }

        }catch(Exception e){
            e.printStackTrace();
            Log.w("location address", "Cannot get Address!");
        }
        return strrAdd;
    }

    public void displayuserLocation(){
        locations.child(currentuser).child("userStatus").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("US_latitude") && dataSnapshot.hasChild("US_longitude")){
                    String lat = dataSnapshot.child("US_latitude").getValue().toString();
                    String lon = dataSnapshot.child("US_longitude").getValue().toString();

                    if(!(lat.equals("0")&&lon.equals("0"))){
                        final LatLng userloc = new LatLng(Double.parseDouble(lat),Double.parseDouble(lon));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(userloc,15));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void setAnimation() {
        if (Build.VERSION.SDK_INT > 20) {
            Slide slide = new Slide();
            slide.setSlideEdge(Gravity.BOTTOM);
            slide.setDuration(300);
            slide.setInterpolator(new DecelerateInterpolator());
            getWindow().setExitTransition(slide);
            getWindow().setEnterTransition(slide);
        }
    }

//    @Override
//    public void onSetDurationClicked(String text) {
//        System.out.println("clicked: "+text);
//        duration.setText(text+" "+"Hour");
//        setHours(Integer.valueOf(text));
//    }

    public void newalert(int h){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Uploading new Alert News..");
        dialog.show();

//        String posttypess = type.getText().toString().toLowerCase();
        String alertid = userCitizenAlert.push().getKey();
        String imageurl = "citizenAlertImage/"+alertid+".jpg";

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("timestamp start ping: "+timestamp.getTime());
        System.out.println("timestamp end ping: "+ gettimeexpired(timestamp.getTime(),h));

        final Map UCA_Details = new HashMap();
        UCA_Details.put("UCAD_alertID",alertid);
        UCA_Details.put("UCAD_type",posttypess);
        UCA_Details.put("UCAD_address",getPingloc());
        UCA_Details.put("UCAD_alertType",alertsTxt);
        UCA_Details.put("UCAD_description",captions);
        UCA_Details.put("UCAD_dateStart",timestamp.getTime());
        UCA_Details.put("UCAD_dateEnd",gettimeexpired(timestamp.getTime(),h));
        UCA_Details.put("UCAD_ownerID",currentuser);
        UCA_Details.put("UCAD_mediaType","image");
        UCA_Details.put("UCAD_mediaURL",imageurl);

        final Map UCA_Location = new HashMap();
        UCA_Location.put("UCAL_longitude",pinglon);
        UCA_Location.put("UCAL_latitude",pinglat);

        final Map UCA_Available = new HashMap();
        UCA_Available.put(alertid,posttypess);

        StorageReference filepath = citizenalertimage.child(alertid+".jpg");
        UploadTask uploadTask = filepath.putBytes(getAvatar());
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(NewsSetup.this, "Please try again later", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                userCitizenAlert.child("UCA_List").child(alertid).child("UCA_Details").setValue(UCA_Details);
                userCitizenAlert.child("UCA_List").child(alertid).child("UCA_Location").setValue(UCA_Location);

                if(posttypess.equals("public")){
                    DatabaseReference publicPing = FirebaseDatabase.getInstance().getReference().child("PublicCitizenAlert");
                    publicPing.child(alertid).child("PC_ownerID").setValue(currentuser);
                }

                userCitizenAlert.child("UCA_Available").child(alertid).setValue(posttypess).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        sendPingComplete();
                    }
                });
            }
        });
    }

    public long gettimeexpired(long timestart,int hour){

        final Date d = new Date(timestart);
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.add(Calendar.HOUR_OF_DAY,hour);
        String jj = calendar.getTime().toString();
        String hh = dateFormat.format(new Date(jj));

        try{
            Date date = dateFormat.parse(hh);
            long unixtime =(long)date.getTime()/1000;

            timeend = unixtime*1000L;
        }
        catch (Exception e){
        }
        return timeend;
    }

    public void sendPingComplete() {
        Intent intent = new Intent(getApplicationContext(), CompleteSendPing.class);
        intent.putExtra("type", "alert");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

//    @Override
//    public void onButtonAlertSticker(String text) {
//        alert.setText(text);
//        bottomSheetAlertMenu.dismiss();
//    }
}
