package com.app.pingchat.CitizenAlert;

public class UCA_Details {

    private String UCAD_address,UCAD_alertID,UCAD_alertType,UCAD_description,UCAD_mediaType,UCAD_mediaURL,UCAD_ownerID,UCAD_type;
    private long UCAD_dateEnd,UCAD_dateStart;
    private double UCAL_latitude,UCAL_longitude;

    public UCA_Details(){}

    public UCA_Details(String UCAD_address,String UCAD_alertID,String UCAD_alertType,String UCAD_description, String UCAD_mediaType, String UCAD_mediaURL, String UCAD_ownerID, String UCAD_type, long UCAD_dateEnd, long UCAD_dateStart,double UCAL_latitude, double UCAL_longitude){
        this.UCAD_address = UCAD_address;
        this.UCAD_alertID = UCAD_alertID;
        this.UCAD_alertType = UCAD_alertType;
        this.UCAD_description = UCAD_description;
        this.UCAD_mediaType = UCAD_mediaType;
        this.UCAD_mediaURL = UCAD_mediaURL;
        this.UCAD_ownerID = UCAD_ownerID;
        this.UCAD_type = UCAD_type;
        this.UCAD_dateEnd = UCAD_dateEnd;
        this.UCAD_dateStart =UCAD_dateStart;
        this.UCAL_latitude = UCAL_latitude;
        this.UCAL_longitude = UCAL_longitude;
    }


    public String getUCAD_address() {
        return UCAD_address;
    }

    public void setUCAD_address(String UCAD_address) {
        this.UCAD_address = UCAD_address;
    }

    public String getUCAD_alertID() {
        return UCAD_alertID;
    }

    public void setUCAD_alertID(String UCAD_alertID) {
        this.UCAD_alertID = UCAD_alertID;
    }

    public String getUCAD_alertType() {
        return UCAD_alertType;
    }

    public void setUCAD_alertType(String UCAD_alertType) {
        this.UCAD_alertType = UCAD_alertType;
    }

    public String getUCAD_description() {
        return UCAD_description;
    }

    public void setUCAD_description(String UCAD_description) {
        this.UCAD_description = UCAD_description;
    }
    public String getUCAD_mediaType() {
        return UCAD_mediaType;
    }

    public void setUCAD_mediaType(String UCAD_mediaType) {
        this.UCAD_mediaType = UCAD_mediaType;
    }

    public String getUCAD_mediaURL() {
        return UCAD_mediaURL;
    }

    public void setUCAD_mediaURL(String UCAD_mediaURL) {
        this.UCAD_mediaURL = UCAD_mediaURL;
    }

    public String getUCAD_ownerID() {
        return UCAD_ownerID;
    }

    public void setUCAD_ownerID(String UCAD_ownerID) {
        this.UCAD_ownerID = UCAD_ownerID;
    }
    public String getUCAD_type() {
        return UCAD_type;
    }

    public void setUCAD_type(String UCAD_type) {
        this.UCAD_type = UCAD_type;
    }

    public long getUCAD_dateStart() {
        return UCAD_dateStart;
    }

    public void setUCAD_dateStart(long UCAD_dateStart) {
        this.UCAD_dateStart = UCAD_dateStart;
    }

    public long getUCAD_dateEnd() {
        return UCAD_dateEnd;
    }

    public void setUCAD_dateEnd(long UCAD_dateEnd) {
        this.UCAD_dateEnd = UCAD_dateEnd;
    }

    public double getUCAL_latitude() {
        return UCAL_latitude;
    }

    public void setUCAL_latitude(double UCAL_latitude) {
        this.UCAL_latitude = UCAL_latitude;
    }

    public double getUCAL_longitude() {
        return UCAL_longitude;
    }

    public void setUCAL_longitude(double UCAL_longitude) {
        this.UCAL_longitude = UCAL_longitude;
    }
}
