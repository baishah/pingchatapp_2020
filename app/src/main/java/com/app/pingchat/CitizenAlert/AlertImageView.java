package com.app.pingchat.CitizenAlert;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.sql.Timestamp;

import jp.shts.android.storiesprogressview.StoriesProgressView;

public class AlertImageView extends AppCompatActivity implements StoriesProgressView.StoriesListener {

    private StoriesProgressView storiesProgressView;

    private static final int PROGRESS_COUNT = 1;

    ImageView pic;

    TextView captiontext;

    String imageurl,description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alertmedialayout);

        Intent i = getIntent();
        imageurl = i.getStringExtra("mediaUrl");
        description = i.getStringExtra("description");


        pic = findViewById(R.id.alertpicture);
        captiontext = findViewById(R.id.caption_text);


        storiesProgressView = (StoriesProgressView) findViewById(R.id.stories);
        storiesProgressView.setStoriesCount(PROGRESS_COUNT); // <- set stories
        storiesProgressView.setStoryDuration(5000L); // <- set a story duration
        storiesProgressView.setStoriesListener(this); // <- set listener
        storiesProgressView.startStories(); // <- start progress

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getApplicationContext());
        circularProgressDrawable.setStrokeWidth(10f);
        circularProgressDrawable.setCenterRadius(50f);
        circularProgressDrawable.start();

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
        GlideApp.with(getApplicationContext()).load(storageReference).placeholder(circularProgressDrawable).into(pic);

        captiontext.setText(description);

    }

    @Override
    public void onNext() {

    }

    @Override
    public void onPrev() {

    }

    @Override
    public void onComplete() {
        finish();
    }
}
