package com.app.pingchat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Main.MainActivity;

public class CompleteCreateGroup extends AppCompatActivity {

    Button gotochats;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complete_create_group);



        gotochats = findViewById(R.id.gotochat);
        gotochats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(CompleteCreateGroup.this, MainActivity.class);
                newIntent.putExtra("completedgroup","group");
                startActivity(newIntent);
            }
        });
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
