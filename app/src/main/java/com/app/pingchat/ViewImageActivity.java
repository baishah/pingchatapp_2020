package com.app.pingchat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class ViewImageActivity extends AppCompatActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_image_layout);

        ImageButton back = findViewById(R.id.back);
        ImageView pic = findViewById(R.id.picture);
        Intent intent=getIntent();

        String test = intent.getStringExtra("test");

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(test);
        GlideApp.with(getApplicationContext())
            .load(storageReference)
            .into(pic);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
