package com.app.pingchat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class bottomSheetSetDuration extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;

    public String getDurationHour() {
        return durationhour;
    }

    public void setDurationhour(String durationhour) {
        this.durationhour = durationhour;
    }

    private String durationhour;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }
    private Button camera,gallery;
    RelativeLayout rl1,rl2,rl3;
    ImageView rl1blue,rl2blue,rl3blue;

    private final int REQUEST_IMAGE_CAPTURE = 1,REQUEST_IMAGE_GALLERY = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_select_duration, container, false);

        rl1 = v.findViewById(R.id.rr);
        rl2 = v.findViewById(R.id.rr2);
        rl3 = v.findViewById(R.id.rr3);

        rl1blue = v.findViewById(R.id.threeh);
        rl2blue = v.findViewById(R.id.sixh);
        rl3blue = v.findViewById(R.id.twelveh);

        if(getDurationHour()!=null){
            switch (getDurationHour()){
                case "3":
                    rl1blue.setVisibility(View.VISIBLE);
                    break;
                case "6":
                    rl2blue.setVisibility(View.VISIBLE);
                    break;
                case "9":
                    rl3blue.setVisibility(View.VISIBLE);
                    break;
            }
        }

        rl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSetDurationClicked("3");
                rl1blue.setVisibility(View.VISIBLE);
                rl2blue.setVisibility(View.INVISIBLE);
                rl3blue.setVisibility(View.INVISIBLE);
                setDurationhour("3");
                dismiss();
            }
        });
        rl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSetDurationClicked("6");
                rl2blue.setVisibility(View.VISIBLE);
                rl1blue.setVisibility(View.INVISIBLE);
                rl3blue.setVisibility(View.INVISIBLE);
                setDurationhour("6");
                dismiss();
            }
        });

        rl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSetDurationClicked("12");
                rl3blue.setVisibility(View.VISIBLE);
                rl1blue.setVisibility(View.INVISIBLE);
                rl2blue.setVisibility(View.INVISIBLE);
                setDurationhour("12");
                dismiss();
            }
        });
        return v;
    }

    public interface BottomSheetListener {
        void onSetDurationClicked(String text);
    }

}
