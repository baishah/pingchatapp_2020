package com.app.pingchat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class WebActivity extends AppCompatActivity {

    WebView webview;
    TextView title2;
    Button backbtn;
    String key,title,d_url,imgUrl;
    ImageView icon;
    private FirebaseAuth mAuths;
    private String current_user;
    StorageReference servisImage,storeRef;
    FirebaseStorage storage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        webview = findViewById(R.id.webView);
        title2 = findViewById(R.id.title_toolbar);
        icon = findViewById(R.id.icon_img);


        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.setOverScrollMode(WebView.OVER_SCROLL_NEVER);

        mAuths = FirebaseAuth.getInstance();
        current_user = mAuths.getCurrentUser().getUid();
        servisImage = FirebaseStorage.getInstance().getReference().child("Services");
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        backbtn = findViewById(R.id.backBtn);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Intent i = getIntent();
        key = i.getStringExtra("key");
        title = i.getStringExtra("title");
        d_url = i.getStringExtra("url");
        imgUrl = i.getStringExtra("icon_url");

                if(key.equals("services")){
//                    GlideApp.with(getApplicationContext())
//                            .load(imgUrl)
//                            .into(icon);
                    storeRef.child(imgUrl).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            // Got the download URL for 'users/me/profile.png'
                            GlideApp.with(getApplicationContext())
                                    .load(uri)
                                    .into(icon);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });
                    title2.setText(title);
                    webview.loadUrl(d_url);

                }else if(key.equals("donate")){
                        icon.setImageResource(R.drawable.donate_icon);
                        title2.setText(title);
                        webview.loadUrl(d_url);
                }else if(key.equals("news")){
                    icon.setVisibility(View.GONE);
                    title2.setText(d_url);
                    webview.loadUrl(d_url);
                }
        }

        public interface BottomSheetListener {
            void onClickWeb(String text);
        }
    }
