package com.app.pingchat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.UserAuth.LoginActivity;
import com.app.pingchat.UserAuth.Register2Activity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class Settings2Fragment extends AppCompatActivity implements View.OnClickListener{

    //Button btn;
    TextView btn;
    TextView name, caption, since,privacysettings,termservice,business, help;
    CircleImageView disppic;

    private DatabaseReference userinfo;
    FirebaseAuth mAuth;
    String current_user;
    StorageReference storageRef;
    FirebaseStorage storage;
    String startdate;


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings2fragment);

        btn = findViewById(R.id.logout_txt);

//        name = (TextView) rootView.findViewById(R.id.namaUser);
//        disppic = (CircleImageView) rootView.findViewById(R.id.gambar);
//        caption = (TextView) rootView.findViewById(R.id.myCaption);
//        since = (TextView) rootView.findViewById(R.id.usersince);
        help = findViewById(R.id.help_txt);
        privacysettings =findViewById(R.id.privacy);
        termservice = findViewById(R.id.terms);
        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();

    }

    public void onClick(View view){
        switch(view.getId()){
            case R.id.logout_txt:
//                ProgressDialog dialog= new ProgressDialog(getApplicationContext());
//                dialog.setMessage("Logging out...");
//                dialog.show();
                userinfo.child(current_user).child("userStatus").child("US_onlineStatus").setValue("offline").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        userinfo.child(current_user).child("userStatus").child("US_timestamp").setValue(ServerValue.TIMESTAMP).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            //    dialog.dismiss();
                                FirebaseAuth.getInstance().signOut();
                                Intent in = new Intent(getApplicationContext(), LoginActivity.class);
                                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(in);
                            }
                        });
                    }
                });
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mAuth.getCurrentUser()!=null){
            current_user = mAuth.getCurrentUser().getUid();
            userinfo = FirebaseDatabase.getInstance().getReference().child("Users");
            userinfo.keepSynced(true);
            privacysettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent p = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.pingchat.app/privacy-policy/"));
                    startActivity(p);
                }
            });
            termservice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent t = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.pingchat.app/terms-of-service/"));
                    startActivity(t);
                }
            });

            help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Settings2Fragment.this, BugsReportActivity.class));
                }
            });


            btn.setOnClickListener(this);
        }
    }
}
