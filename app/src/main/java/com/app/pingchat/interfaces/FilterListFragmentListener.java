package com.app.pingchat.interfaces;

import com.zomato.photofilters.imageprocessors.Filter;

public interface FilterListFragmentListener {
    void onFilterSelected(Filter filter);
}
