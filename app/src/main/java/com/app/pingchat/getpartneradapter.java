package com.app.pingchat;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class getpartneradapter extends RecyclerView.Adapter<getpartneradapter.ViewHolder>{

    private Context context;
    private List<partnerDetailsClass> list;
    private partnerlistrecycleradapter mclicklistener;

    public getpartneradapter(Context context,List<partnerDetailsClass> list,partnerlistrecycleradapter clicklistener){
        this.context = context;
        this.list = list;
        this.mclicklistener = clicklistener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.partner_selector_layout,parent,false);
        return new ViewHolder(v,mclicklistener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final partnerDetailsClass getpartnerclass = list.get(position);

        System.out.println("url dalam adapter "+getpartnerclass.getP_profileURL());
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(getpartnerclass.getP_profileURL());
        GlideApp.with(context.getApplicationContext())
                .load(storageReference)
                .into(holder.i);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView businessPartnerID, businessCreator, businessName,businessRegister,businessLastUpdate,businessPhone,businessEmail,businessLatitude,businessLongitude,
        businessActualAddress,businessOnMapAddress,businessCategory,businessProfileURL;

        CircleImageView i;
        partnerlistrecycleradapter mclicklistener;
        public ViewHolder(View itemView, partnerlistrecycleradapter clickListener) {
            super(itemView);

            mclicklistener = clickListener;
            itemView.setOnClickListener(this);
            i = itemView.findViewById(R.id.user_dp);

        }


        @Override
        public void onClick(View v) {
            mclicklistener.onUserClicked(getAdapterPosition());
        }
    }

        public interface partnerlistrecycleradapter{
            void onUserClicked(int position);
        }
}
