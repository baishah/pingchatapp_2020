package com.app.pingchat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;

import com.app.pingchat.adapter.ViewPagerAdapter;
import com.app.pingchat.interfaces.EditImageFragmentListener;
import com.google.android.material.tabs.TabLayout;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter;

import java.io.ByteArrayOutputStream;

public class filterActivitity extends AppCompatActivity implements FilterListFragment.FilterListFragmentListener ,EditImageFragmentListener{

    public static Bitmap b;
    ImageView img_preview;
    TabLayout tabLayout;
    ViewPager viewPager;
    CoordinatorLayout coordinatorLayout;


    Bitmap originalBitmap,filteredBitmap,finalBitmap;

    FilterListFragment filterListFragment;
    EditImageFragment editImageFragment;

    int brightnessFinal = 0;
    float saturationFinal =1.0f;
    float constrantFinal = 1.0f;

    private Button done;

    static {
        System.loadLibrary("NativeImageProcessor");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_activity_layout);

        img_preview = findViewById(R.id.image_preview);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        coordinatorLayout = findViewById(R.id.coordinator);
        done = findViewById(R.id.done);

        byte[] bytes = getIntent().getByteArrayExtra("Image");

        b = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
        originalBitmap = b;
        filteredBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        finalBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        img_preview.setImageBitmap(originalBitmap);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                getFinalBitmap().compress(Bitmap.CompressFormat.JPEG,50,stream);
                byte[] by = stream.toByteArray();
                Intent resultIntent = new Intent();
                resultIntent.putExtra("PUBLIC_STRING_IDENTIFIER",by);
                setResult(Activity.RESULT_OK,resultIntent);
                finish();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        filterListFragment = new FilterListFragment();
        filterListFragment.setListener(this);

        editImageFragment = new EditImageFragment();
        editImageFragment.setListener(this);

        adapter.addFragment(filterListFragment,"Filters");
        adapter.addFragment(editImageFragment,"Edit");

        viewPager.setAdapter(adapter);
    }


    @Override
    public void onFilterSelected(Filter filter) {
        resetControl();
        filteredBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        img_preview.setImageBitmap(filter.processFilter(filteredBitmap));
        finalBitmap = filteredBitmap.copy(Bitmap.Config.ARGB_8888,true);
    }

    private void resetControl(){
        if(editImageFragment!=null){
            editImageFragment.resetControls();
            brightnessFinal=0;
            saturationFinal=1.0f;
            constrantFinal =1.0f;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.ok){
            System.out.println("ok selected");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBrightnessChanged(int brightness) {
        brightnessFinal = brightness;
        Filter myfilter = new Filter();
        myfilter.addSubFilter(new BrightnessSubFilter(brightness));
        img_preview.setImageBitmap(myfilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));
    }

    @Override
    public void onSaturationChanged(float saturation) {
        saturationFinal = saturation;
        Filter myfilter = new Filter();
        myfilter.addSubFilter(new SaturationSubfilter(saturation));
        img_preview.setImageBitmap(myfilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));
    }

    @Override
    public void onConstrantChanged(float constrant) {
        constrantFinal = constrant;
        Filter myfilter = new Filter();
        myfilter.addSubFilter(new ContrastSubFilter(constrant));
        img_preview.setImageBitmap(myfilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));
    }

    @Override
    public void onEditStarted() {}

    @Override
    public void onEditCompleted() {
        Bitmap bitmap = filteredBitmap.copy(Bitmap.Config.ARGB_8888,true);
        Filter myfilter =new Filter();
        myfilter.addSubFilter(new BrightnessSubFilter(brightnessFinal));
        myfilter.addSubFilter(new SaturationSubfilter(saturationFinal));
        myfilter.addSubFilter(new ContrastSubFilter(constrantFinal));

        finalBitmap = myfilter.processFilter(bitmap);
        setFinalBitmap(finalBitmap);
    }

    public Bitmap getFinalBitmap() {
        return finalBitmap;
    }

    public void setFinalBitmap(Bitmap finalBitmap) {
        this.finalBitmap = finalBitmap;
    }
}

