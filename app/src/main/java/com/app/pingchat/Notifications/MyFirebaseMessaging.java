package com.app.pingchat.Notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.app.pingchat.Chat.ChatsActivity;
import com.app.pingchat.CommentLayoutActivity;
import com.app.pingchat.Friend.ContactsFragment;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class MyFirebaseMessaging extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage message){
        System.out.println("masuksinifirebasemessaging");
        super.onMessageReceived(message);
//        showNotification(message.getNotification(),message);

        if (message.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + message.getData().get("notificationType"));

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                //scheduleJob();
                Log.d(TAG, "masuk if: ");
            } else {
                // Handle message within 10 seconds
              //  handleNow();
                Log.d(TAG, "masuk else: ");
            }
        }
        else{
            Log.d(TAG, "onmessagereceived masuk sini: ");
        }
    }

//    public void showNotification(RemoteMessage.Notification notification,RemoteMessage message){
//        Intent i =new Intent();
//        FirebaseAuth mAuth = FirebaseAuth.getInstance();
//        if(mAuth.getCurrentUser()!=null){
//            String friendid = mAuth.getCurrentUser().getUid().toString();
//            if(message.getData().get("notificationType").equals("chatPersonal")){
//                i = new Intent(this, ChatsActivity.class);
//                i.putExtra("chatid",message.getData().get("chatID"));
//                i.putExtra("current_id",friendid);
//            }
//            else if(message.getData().get("notificationType").equals("addFriendViaQR")){
//                i = new Intent(this, ContactsFragment.class);
//                i.putExtra("id",friendid);
//                i.putExtra("fromnoti","openfromnotiqr");
//            }
//            else if(message.getData().get("notificationType").equals("addFriend")){
//                i = new Intent(this, ContactsFragment.class);
//                i.putExtra("id",friendid);
//                i.putExtra("fromnoti","openfromnoti");
//            }
//            else if(message.getData().get("notificationType").equals("commentDiscover")){
//                i = new Intent(this, CommentLayoutActivity.class);
//                i.putExtra("discoverPostid",message.getData().get("discoverID"));
//                i.putExtra("currentuser",mAuth.getCurrentUser().getUid());
//                System.out.println("discoverid"+message.getData().get("discoverID"));
//            }
//            else if(message.getData().get("notificationType").equals("clapDiscover")){
//                i = new Intent(this, CommentLayoutActivity.class);
//                i.putExtra("discoverPostid",message.getData().get("discoverID"));
//                System.out.println("discoverid"+message.getData().get("discoverID"));
//            }
//        }
//        else{
//            i = new Intent(this, MainActivity.class);
//        }
//        Intent intent = i;
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//       // intent.putExtra("friendid)
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//        String channelId = "Default";
//        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
//                .setSmallIcon(R.drawable.pinglogo)
//                .setSound(uri)
//                .setContentTitle(notification.getTitle())
//                .setContentText(notification.getBody()).setAutoCancel(true).setContentIntent(pendingIntent);;
//        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
//            manager.createNotificationChannel(channel);
//        }
//        manager.notify(0, builder.build());
//    }

}
