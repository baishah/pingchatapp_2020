package com.app.pingchat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Partner.PingPartnersActivity;
import com.app.pingchat.donation.DonateSelection;
import com.app.pingchat.events.EventAcitivity;
import com.app.pingchat.rewards.RewardsHomeActivity;
import com.app.pingchat.updates.updateAcivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class moreAdapter extends RecyclerView.Adapter<moreAdapter.MyViewHolder> {

    private Context mContext;
    private List<More> mData;

    public moreAdapter(Context mContext, List<More> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater minflater = LayoutInflater.from(mContext);
        view = minflater.inflate(R.layout.more_item,parent,false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(mData.get(position).getBtnName());
        holder.iconBtn.setImageResource(mData.get(position).getThumbnail());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends  RecyclerView.ViewHolder {

        private final Context context;
        TextView title;
        ImageView iconBtn;
        CardView cardVId;
        DatabaseReference users;
        String current_user;
        FirebaseAuth mAuth;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            title = (TextView)itemView.findViewById(R.id.title_btn);
            iconBtn = (ImageView)itemView.findViewById(R.id.icon_btn);
            cardVId = (CardView)itemView.findViewById(R.id.cardviewmore);
            context = itemView.getContext();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            mAuth = FirebaseAuth.getInstance();
            current_user = mAuth.getCurrentUser().getUid();
            cardVId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent intent;
                    switch (getAdapterPosition()){
                        case 0:
                            intent =  new Intent(context, updateAcivity.class);
                            break;

                        case 1:
                            intent = new Intent(context, PingPartnersActivity.class);
                            break;

                        case 2:
                            intent =  new Intent(context, ArActivity.class);
                            break;

                        case 3:
                            intent =  new Intent(context, EventAcitivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            break;

                        case 4:
                            intent =  new Intent(context, MarketplaceActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            break;

                        case 5:
                            intent =  new Intent(context, RewardsHomeActivity.class);
                            break;

                        case 6:
                            intent =  new Intent(context, ServicesActivity.class);
                            break;

                        case 7:
                            intent =  new Intent(context, DonateSelection.class);
                            break;

                        case 8:
                            intent =  new Intent(context, Settings2Fragment.class);
                            break;

                        default:
                            intent =  new Intent(context, UnderDevelopmentActivity.class);
                            break;
                    }
                    context.startActivity(intent);

                }
            });
        }

        }

}



