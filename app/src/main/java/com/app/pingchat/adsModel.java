package com.app.pingchat;

public class adsModel {
    private int image;
    private String title;

    public adsModel() {
        // Empty construct
    }

    public adsModel(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }
}
