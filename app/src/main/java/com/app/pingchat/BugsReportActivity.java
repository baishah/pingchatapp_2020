package com.app.pingchat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Discover.DiscoverSetup;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.UserAuth.Register2Activity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

public class BugsReportActivity extends AppCompatActivity {
    private static final String TAG = "BugsReport";
    private static final int PICK_IMAGE = 0;

    private Button backBtn,chooseBtn,editBtn,sendReport;
    private TextView imgURl;
    private EditText reportTitle, reportDesc;
    private String URP_title,URP_description, new_reportid;
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    private DatabaseReference userinfo,userreport;
    public StorageReference reportstorage;
    private byte[] URP_mediaURL;
    FirebaseAuth mAuth;
    String current_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_layout);

        backBtn = findViewById(R.id.back_btn);
        chooseBtn = findViewById(R.id.choose_btn);
        editBtn = findViewById(R.id.edit_btn);
        sendReport = findViewById(R.id.send_report);


        imgURl = findViewById(R.id.upload_scre);
        reportTitle = findViewById(R.id.report_title);
        reportDesc = findViewById(R.id.report_txtbox);
        mAuth = FirebaseAuth.getInstance();
        userreport = FirebaseDatabase.getInstance().getReference().child("UserReportBugs");
        reportstorage = FirebaseStorage.getInstance().getReference().child("bugsreport");

        if (mAuth.getCurrentUser() != null) {
            current_user = mAuth.getCurrentUser().getUid();
            userinfo = FirebaseDatabase.getInstance().getReference().child("Users");
            userinfo.keepSynced(true);
        }


        chooseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 786);
                }
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });

        sendReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendReportBugs();
            }
        });
    }

    private void sendReportBugs(){
        ProgressDialog dialog= new ProgressDialog(BugsReportActivity.this);
        final String title = reportTitle.getText().toString();
        final String decs = reportDesc.getText().toString();
        if(((title !=null && !title.isEmpty()) && (decs !=null && !decs.isEmpty()))){

            dialog.setMessage("Sumbit report on progress\nPlease wait");
            dialog.show();
            new_reportid = userreport.push().push().getKey();
            String mediareportUrl = ("reports/" + current_user + "/" + new_reportid + ".jpg");
            HashMap new_bugsreport = new HashMap();

            new_bugsreport.put("URP_senderId", current_user);
            new_bugsreport.put("URP_title", title);
            new_bugsreport.put("URP_description", decs);
            new_bugsreport.put("URP_timestamp", timestamp.getTime());
            new_bugsreport.put("URP_mediaURL", mediareportUrl);
            new_bugsreport.put("URP_mediaType", "image");

            userreport.child(new_reportid).child("URP_Details").setValue(new_bugsreport).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "upload");
                        StorageReference filepath = reportstorage.child(current_user).child(new_reportid + ".jpg");
                        UploadTask uploadTask = filepath.putBytes(getURP_mediaURL());
                        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                dialog.dismiss();
                                Toast.makeText(BugsReportActivity.this, "Your report have been submit.", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), CompleteSendBugsReport.class);
                                startActivity(intent);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dialog.dismiss();
                                Toast.makeText(BugsReportActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        //                             mProgress.dismiss();
                        dialog.dismiss();
                        String message = task.getException().toString();
                        Toast.makeText(getApplicationContext(), "error:" + message, Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE && resultCode == RESULT_OK){
            final InputStream imageStream;
            final Uri imageUri = data.getData();
            try {
                imageStream = getApplicationContext().getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Bitmap lastbitmap = null;
                lastbitmap = selectedImage;
//                setURP_mediaURL(getStringImage(lastbitmap));
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
                imgURl.setText(imageUri.toString());
                byte[] datas = byteArrayOutputStream.toByteArray();
                setURP_mediaURL(datas);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG,70,baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedimage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
        return encodedimage;
    }

    public String getURP_title() {
        return URP_title;
    }

    public void URP_title()
    {
        this.URP_title = URP_title;
    }
    public String getURP_description() {
        return URP_description;
    }

    public void URP_description()
    {
        this.URP_description = URP_description;
    }

    public byte[] getURP_mediaURL() {
        return URP_mediaURL;
    }

    public void setURP_mediaURL(byte[] URP_mediaURL)
    {
        this.URP_mediaURL = URP_mediaURL;
    }
}
