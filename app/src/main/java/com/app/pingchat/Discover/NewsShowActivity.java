package com.app.pingchat.Discover;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.app.pingchat.WebActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import jp.shts.android.storiesprogressview.StoriesProgressView;

public class NewsShowActivity extends AppCompatActivity implements StoriesProgressView.StoriesListener{


    ImageView story_image;
    TextView story_username, timestamp;
    RelativeLayout storyview;
    private TextView see_more, desc, dismiss;
    private Button websitebtn;
    //    private bottomSheetNewsDetails bottomsheetNewsDetails;
    private BottomSheetBehavior mBottomSheetBehavior;

    //    List<String> images;
//    List<String> storyids;
    String userid, imageurl,title, time_Stamp, newsUrl;

    StoriesProgressView storiesProgressView;
    ImageView image;

    RelativeLayout bottomSheet;

    long pressTime = 0L;
    long limit = 500L;

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_story_layout);

        storiesProgressView = findViewById(R.id.story_time);
        image = findViewById(R.id.mediapicture);
        timestamp = findViewById(R.id.timestamp);
        see_more = findViewById(R.id.see_more);
//        bottomSheet = findViewById(R.id.bottom_sheet);
        desc = findViewById(R.id.news_details);
        final View bottom_sheet = findViewById(R.id.bottom_sheet_story);
        websitebtn = findViewById(R.id.go_to_website);


//        story_image = findViewById(R.id.story_image);
        story_username = findViewById(R.id.story_username);
        storyview = findViewById(R.id.storyView);

        Intent i = getIntent();
        imageurl = i.getStringExtra("mediaUrl");
        newsUrl = i.getStringExtra("newUrl");
        title = i.getStringExtra("storyUsername");
        time_Stamp = i.getStringExtra("timeStamp");
        String descrip = i.getStringExtra("description");

//        bottomsheetNewsDetails = new bottomSheetNewsDetails();
//        bottomsheetNewsDetails.setListener(this);


        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
        GlideApp.with(getApplicationContext()).load(storageReference).into(image);
        story_username.setText(title);
        timestamp.setText(time_Stamp);
//        story_image.setImageResource(getIntent().getIntExtra("profilePic", 0));
//        story_username.setText(getIntent().getStringExtra("storyUsername"));


        storiesProgressView.setStoriesCount(1);
        storiesProgressView.setStoryDuration(5000L);
        storiesProgressView.setStoriesListener(NewsShowActivity.this);
        storiesProgressView.startStories();

//        storyview.setOnTouchListener(onTouchListener);

        desc.setText(descrip);
        see_more.bringToFront();

        View previous = findViewById(R.id.previous);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        mBottomSheetBehavior.setPeekHeight(100);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomSheetBehavior.setPeekHeight(100);
                    storiesProgressView.resume();
                    see_more.setText("See More");
                    System.out.println("-----------------------1");
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    System.out.println("-----------------------2");
                    storiesProgressView.pause();
                    see_more.setText("Dismiss");
                } else if(newState== BottomSheetBehavior.STATE_SETTLING) {
                    bottomSheet.scrollTo(0,0);
                    System.out.println("-----------------------3");
                } else if(newState == BottomSheetBehavior.STATE_DRAGGING) {
                    System.out.println("-----------------------4");
                    storiesProgressView.pause();
                }
            }

            @Override
            public void onSlide(View bottomSheet, final float slideOffset) {

            }
        });

        see_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    storiesProgressView.pause();
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    descriptionBox.setMovementMethod(new ScrollingMovementMethod());

                } else if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                }
            }
        });
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.reverse();
            }
        });
        previous.setOnTouchListener(onTouchListener);

        View next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.skip();
            }
        });
        next.setOnTouchListener(onTouchListener);
        websitebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WebActivity.class);
                intent.putExtra("key","news");
                intent.putExtra("url",newsUrl);
                intent.putExtra("title",title);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

//        see_more.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bundle args= new Bundle();
//                args.putString("title",title);
//                args.putString("newsurl",newsUrl);
//                args.putString("description", descrip);
//                storiesProgressView.pause();
//                bottomsheetNewsDetails.show(getSupportFragmentManager(),"news_details");
//                bottomsheetNewsDetails.setArguments(args);
//            }
//        });
    }

    protected void onPause() {
        storiesProgressView.pause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        storiesProgressView.resume();
        super.onResume();
    }

    @Override
    public void onNext() {

    }

    @Override
    public void onPrev() {

    }

    @Override
    public void onComplete() {
        finish();
    }

    @Override
    protected void onDestroy() {
        storiesProgressView.destroy();
        super.onDestroy();
    }

}
