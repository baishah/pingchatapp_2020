package com.app.pingchat.Discover;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CompleteDeleteDiscoverPost extends AppCompatActivity {

    Button todiscover;
    public DatabaseReference discoverSession;
    // int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        discoverSession = FirebaseDatabase.getInstance().getReference().child("DiscoverSession").child("Friend");
        final String discoverid = intent.getStringExtra("discoverid");
        setContentView(R.layout.complete_delete_post_layout);
        todiscover = findViewById(R.id.gotodisc);
        todiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("completediscover","ada");
                startActivity(intent);
                finish();
            }
        });
    }

}
