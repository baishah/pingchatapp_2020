package com.app.pingchat.Discover;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Ping.PingSetup;
import com.app.pingchat.R;
import com.app.pingchat.UserAuth.RegisterActivity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.android.volley.VolleyLog.TAG;
//import static com.app.pingchat.Register2Activity.contextOfApplication;

public class DiscoverFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    ImageButton newpost, searchBtn;
    Uri uriSavedImage;
    EditText searchFilter;
    private Toolbar toolbar;
    private Uri post;
    FirebaseAuth mAuth;
    private String current_user;
    private String uname;
    private String avatar;
    private String UDSD_caption;
    private RecyclerView discoverRecyclerView, storyView;
    RelativeLayout btnnewpost;
    ImageView nopost;
    private ArrayList discover_key_array = new ArrayList();
    ArrayList<Discovers> discoversArrayList;
    ArrayList<NW_detailsClass> adsDiscoverList;
    ArrayList<UDS_DetailsClass>alldiscoverarraylist2;
    DiscoverAdapter discoverAdapter;
    DiscoverAdapter2 discoverAdapter2;
    public DatabaseReference discover,discoverSession,metadata,userDiscover,publicdiscover,adsNews,media,userfrienddiscover;
    public void setPost(Uri post) { this.post = post; }
    public static Context contextOfApplication;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList alldiscoverkey = new ArrayList();
    StorageReference storeRef;
    FirebaseStorage storage;
    private RequestQueue mQueue;

    SearchView searchView;


//    ArrayList alldiscoverarraylist2 = new ArrayList<UDS_DetailsClass>();

    String datetimes;
    String datess;
    Date datesss,datetocompare;
    LottieAnimationView animationView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.discover_fragment, container, false);
        newpost = (ImageButton) v.findViewById(R.id.newPost);
        toolbar = v.findViewById(R.id.toolbar);
//        searchFilter = v.findViewById(R.id.search_view);
        nopost = v.findViewById(R.id.noPost);
        mAuth = FirebaseAuth.getInstance();

        discoverRecyclerView = (RecyclerView) v.findViewById(R.id.discoverlist);
        storyView = v.findViewById(R.id.storyView);
        discoverRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // SwipeRefreshLayout
//        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
//        mSwipeRefreshLayout.setOnRefreshListener(this);
//        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
//                android.R.color.holo_green_dark,
//                android.R.color.holo_orange_dark,
//                android.R.color.holo_blue_dark);

        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        mQueue = Volley.newRequestQueue(getContext());


        v.findViewById(R.id.rel_lay).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });
        contextOfApplication = getContext().getApplicationContext();

        btnnewpost = (RelativeLayout) v.findViewById(R.id.btnNewpost);
        discoverRecyclerView .addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //dx horizontal distance scrolled in pixels
                //dy vertical distance scrolled in pixels
                Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.hide);
                Animation animation1 = AnimationUtils.loadAnimation(getContext(),R.anim.alpha);
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0  && btnnewpost.getVisibility() != View.VISIBLE) {
                    btnnewpost.setVisibility(View.GONE);
                    btnnewpost.startAnimation(animation);
                }
                else if (dy < 0 && btnnewpost.getVisibility() != View.VISIBLE)
                {
                    btnnewpost.setVisibility(View.GONE);
                    btnnewpost.startAnimation(animation);
                }
                else {
                    btnnewpost.setVisibility(View.VISIBLE);
                    btnnewpost.startAnimation(animation1);
                }
            }
        });


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        storyView.setLayoutManager(layoutManager);

        newpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotodiscoversetup();
            }
        });

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        animationView = (LottieAnimationView) v.findViewById(R.id.animation_view);

//        getAlldiscover(current_user);
        if(mAuth.getCurrentUser()!=null){
            current_user = mAuth.getCurrentUser().getUid();
            discover = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Discover");
            discoverSession = FirebaseDatabase.getInstance().getReference().child("DiscoverSession").child("Friend");
            metadata = FirebaseDatabase.getInstance().getReference().child("Users");
            userDiscover = FirebaseDatabase.getInstance().getReference().child("Users");
            publicdiscover = FirebaseDatabase.getInstance().getReference().child("PublicDiscover");
            adsNews = FirebaseDatabase.getInstance().getReference().child("News");
            adsNews.keepSynced(true);

            retrieve_name_avatar(current_user);
            animationView.setAnimation("loading_list.json");
            animationView.playAnimation();

//            animationView.setVisibility(View.VISIBLE);
//            getAdsDiscover(current_user);
//            getAlldiscover(current_user);
//            getAlldiscover(current_user);
            getAlldiscover(current_user);
            getAdsDiscover(current_user);
//            mSwipeRefreshLayout.post(new Runnable() {
//                @Override
//                public void run() {
//                    //getDiscovers();
//                    //getalldiscover();
//                    mSwipeRefreshLayout.setRefreshing(true);
////                    get_all_discover2();
//                    getAdsDiscover(current_user);
//                    display_discover_on_refresh();
//                    getAlldiscover(current_user);
//                }
//            });
        }
        else{
            Intent in = new Intent(getActivity(), RegisterActivity.class);
            startActivity(in);
        }
        return v;
    }

    @Override
    public void onRefresh() {
        //getDiscovers();
        //getalldiscover();
        // get_all_discover2();
        getAlldiscover(current_user);
        getAdsDiscover(current_user);
//        display_discover_on_refresh();
    }


//    private void filter(String text){
//        DiscoverAdapter2 discoverAdapter2 = new DiscoverAdapter2(getContext(),alldiscoverarraylist2,current_user);
//        ArrayList<UDS_DetailsClass> filteredlist = new ArrayList<>();
//
//        System.out.println("user ff punya "+alldiscoverarraylist2.size());
//        if(alldiscoverarraylist2.size()!=0) {
//            for (int i = 0; i < alldiscoverarraylist2.size(); i++) {
//                Log.e("list", "discover " + alldiscoverarraylist2.get(i).getUDSD_caption());
//            }
//            for (UDS_DetailsClass item : alldiscoverarraylist2) {
//                if (item.getUDSD_caption().toLowerCase().contains(text.toLowerCase())){
//                    filteredlist.add(item);
//                    System.out.println("filter : "+item);
//                }
//
//            }
////            alldiscoverarraylist2.clear();
////            alldiscoverarraylist2 = filteredlist;
//            discoverAdapter2.filterList(filteredlist);
//            System.out.println("filter list: "+filteredlist);
//
//        }
//    }

    @Override
    public void onStart(){
        super.onStart();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                Intent i = new Intent(getActivity(), DiscoverSetup.class);
                startActivity(i);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        DiscoverAdapter2 discoverAdapter2 = new DiscoverAdapter2(getContext(),alldiscoverarraylist2,current_user);
//        inflater.inflate(R.menu.menu2,menu);
        inflater.inflate(R.menu.menu_chat, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

//                    discoverAdapter2.getFilter().filter(query);
//                    Toast.makeText(getContext(), "No Match found",Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                discoverAdapter2.getFilter().filter(newText);
//                display_discover_on_refresh();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

//    public void display_discover_on_refresh(){
//        System.out.println("display_discover_on_refresh"+alldiscoverarraylist2);
//        DiscoverAdapter2 discoverAdapter2 = new DiscoverAdapter2(getContext(),alldiscoverarraylist2,current_user);
//        discoverRecyclerView.setAdapter(discoverAdapter2);
////        DiscoverAdapter2 discoverAdapter2 = new DiscoverAdapter2(getContext(),alldiscoverarraylist2,current_user);
////        Collections.sort(alldiscoverarraylist2,UDS_DetailsClass.uds_detailsClassComparator);
////        discoverRecyclerView.setAdapter(discoverAdapter2);
////        getAlldiscover(current_user);
//        mSwipeRefreshLayout.setRefreshing(false);
//    }


    private void getAdsDiscover(String c_user){

        adsDiscoverList = new ArrayList();
        NewsListAdapter newsListAdapter = new NewsListAdapter(getContext(),adsDiscoverList,c_user);
        storyView.setAdapter(newsListAdapter);

        String url = "https://devbm.ml/news/all";

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("dataNews");

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject value = jsonArray.getJSONObject(i);

                                String nwd_newsID = value.getString("NWD_newsID");
                                long  nwd_newsTimestamp = Long.parseLong(value.getString("NWD_newsTimestamp"));
                                String nwd_newsTitle = value.getString("NWD_newsTitle");
                                String nwd_newsURL = value.getString("NWD_newsURL");
                                String nwd_newsAddress = value.getString("NWD_newsAddress");
                                String nwd_newsDescription = value.getString("NWD_newsDescription");
                                String nwl_latitude = value.getString("NWL_latitude");
                                String nwl_longitude = value.getString("NWL_longitude");
                                String nwm_mediaType = value.getString("NWM_mediaType");
                                String nwm_mediaURL = value.getString("NWM_mediaURL");

                                NW_detailsClass nw_detailsClass= new NW_detailsClass();

                                nw_detailsClass.setNWD_newsID(nwd_newsID);
                                nw_detailsClass.setNWD_newsTimestamp(nwd_newsTimestamp);
                                nw_detailsClass.setNWD_newsTitle(nwd_newsTitle);
                                nw_detailsClass.setNWD_newsURL("");
                                nw_detailsClass.setNWD_newsAddress(nwd_newsAddress);
                                nw_detailsClass.setNWD_newsDescription(nwd_newsDescription);
                                nw_detailsClass.setNWM_mediaType(nwm_mediaType);
                                nw_detailsClass.setNWM_mediaURL(nwm_mediaURL);

                                adsDiscoverList.add(nw_detailsClass);
                            }
                            newsListAdapter.notifyDataSetChanged();
//                            progressDialog.dismiss();
//                            nearbyMarketAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);

//        adsNews.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if(dataSnapshot.exists()){
//                    String adsId = dataSnapshot.getKey();
//                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
//                        final NW_detailsClass nw_detailsClass= new NW_detailsClass();
//                        NW_detailsClass m = dataSnapshot1.child("NW_details").getValue(NW_detailsClass.class);
////
//                        String address = m.getNWD_newsAddress();
//                        String description = m.getNWD_newsDescription();
//                        String ads_id = m.getNWD_newsID();
//                        long timestamp = m.getNWD_newsTimestamp();
//                        String newsURL = m.getNWD_newsURL();
//                        String title = m.getNWD_newsTitle();
//
//                        for (DataSnapshot dataSnapshot2:dataSnapshot1.child("NW_media").getChildren()){
//                            String mediaUrl = dataSnapshot2.child("NWM_mediaURL").getValue().toString();
//                            String mediaType = dataSnapshot2.child("NWM_mediaType").getValue().toString();
//                            System.out.println("mediaurl:"+mediaUrl);
////                            nw_detailsClass.setNW(mediaType);
//                            nw_detailsClass.setNWM_mediaURL(mediaUrl);
//                        }
//                        nw_detailsClass.setNWD_newsAddress(address);
//                        nw_detailsClass.setNWD_newsDescription(description);
//                        nw_detailsClass.setNWD_newsID(ads_id);
//                        nw_detailsClass.setNWD_newsTimestamp(timestamp);
//                        nw_detailsClass.setNWD_newsURL("");
//                        nw_detailsClass.setNWD_newsTitle(title);
//
//                        adsDiscoverList.add(nw_detailsClass);
//
//                    }
//                    newsListAdapter.notifyDataSetChanged();
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
    }

    private void getAlldiscover(String id){
        alldiscoverarraylist2 = new ArrayList();
        String url = "https://devbm.ml/discover/allBody/";
//        String url = "https://devbm.ml/discover/all?keyUID/"+id;
        animationView.cancelAnimation();
        animationView.setVisibility(View.GONE);
        DiscoverAdapter2 discoverAdapter2 = new DiscoverAdapter2(getContext(),alldiscoverarraylist2,current_user);
        discoverRecyclerView.setAdapter(discoverAdapter2);

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
        postParams.put("keyUID",id);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        progressDialog.dismiss();
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("dataDiscoverPublic");

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject value = jsonArray.getJSONObject(i);

//                                animationView.cancelAnimation();
//                                animationView.setVisibility(View.GONE);
                                System.out.println("value:"+value);
//                                Collections.sort(alldiscoverarraylist2,UDS_DetailsClass.uds_detailsClassComparator);
                                String discover_type = value.getString("UDSD_mediaType");
//                                Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));

                                UDS_DetailsClass detailsClass = new UDS_DetailsClass();
                                    if(discover_type.equals("image")){
                                        String address = value.getString("UDSD_address");
                                        String caption = value.getString("UDSD_caption");
                                        long discover_date = value.getLong("UDSD_date");
                                        String discover_id = value.getString("UDSD_discoverID");
                                        String discover_url = value.getString("UDSD_mediaURL");
                                        String ownerID = value.getString("UDSD_ownerID");
                                        Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
                                        String dis_type = value.getString("UDSD_type");

                                        detailsClass.setDatetocompare(getdate(discover_date));
                                        detailsClass.setDate(getdatetime(discover_date));
                                        detailsClass.setUDSD_address(address);
                                        detailsClass.setUDSD_caption(caption);
//                                        detailsClass.setUDSD_date(discover_date);
                                        detailsClass.setUDSD_discoverID(discover_id);
                                        detailsClass.setUDSD_mediaType(discover_type);
                                        detailsClass.setUDSD_mediaURL(discover_url);
                                        detailsClass.setUDSD_ownerID(ownerID);
                                        detailsClass.setUDSD_status(discover_status);
                                        detailsClass.setUDSD_type(dis_type);
                                    }else{
                                        String address = value.getString("UDSD_address");
                                        String caption = value.getString("UDSD_caption");
                                        long discover_date = value.getLong("UDSD_date");
                                        String discover_id = value.getString("UDSD_discoverID");
                                        String ownerID = value.getString("UDSD_ownerID");
                                        Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
                                        String dis_type = value.getString("UDSD_type");

                                        detailsClass.setDatetocompare(getdate(discover_date));
                                        detailsClass.setDate(getdatetime(discover_date));
                                        detailsClass.setUDSD_address(address);
                                        detailsClass.setUDSD_caption(caption);
//                                        detailsClass.setUDSD_date(discover_date);
                                        detailsClass.setUDSD_discoverID(discover_id);
                                        detailsClass.setUDSD_mediaType(discover_type);
                                        detailsClass.setUDSD_ownerID(ownerID);
                                        detailsClass.setUDSD_status(discover_status);
                                        detailsClass.setUDSD_type(dis_type);
                                    }
                                alldiscoverarraylist2.add(detailsClass);
                            }

                            JSONArray jsonArray1 = response.getJSONArray("dataDiscoverSelf");
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                JSONObject value1 = jsonArray1.getJSONObject(j);

                                String discover_type = value1.getString("UDSD_mediaType");
//                                Collections.sort(alldiscoverarraylist2,UDS_DetailsClass.uds_detailsClassComparator);

                                UDS_DetailsClass detailsClass1 = new UDS_DetailsClass();
                                if(discover_type.equals("image")){
                                    String address = value1.getString("UDSD_address");
                                    String caption = value1.getString("UDSD_caption");
                                    long discover_date = value1.getLong("UDSD_date");
                                    String discover_id = value1.getString("UDSD_discoverID");
                                    String discover_url = value1.getString("UDSD_mediaURL");
                                    String ownerID = value1.getString("UDSD_ownerID");
                                    Boolean discover_status = Boolean.valueOf(value1.getString("UDSD_status"));
                                    String dis_type = value1.getString("UDSD_type");

                                    detailsClass1.setDatetocompare(getdate(discover_date));
                                    detailsClass1.setDate(getdatetime(discover_date));
                                    detailsClass1.setUDSD_address(address);
                                    detailsClass1.setUDSD_caption(caption);
//                                    detailsClass1.setUDSD_date(discover_date);
                                    detailsClass1.setUDSD_discoverID(discover_id);
                                    detailsClass1.setUDSD_mediaType(discover_type);
                                    detailsClass1.setUDSD_mediaURL(discover_url);
                                    detailsClass1.setUDSD_ownerID(ownerID);
                                    detailsClass1.setUDSD_status(discover_status);
                                    detailsClass1.setUDSD_type(dis_type);
                                }else{
                                    String address = value1.getString("UDSD_address");
                                    String caption = value1.getString("UDSD_caption");
                                    long discover_date = value1.getLong("UDSD_date");
                                    String discover_id = value1.getString("UDSD_discoverID");
                                    String ownerID = value1.getString("UDSD_ownerID");
                                    Boolean discover_status = Boolean.valueOf(value1.getString("UDSD_status"));
                                    String dis_type = value1.getString("UDSD_type");

                                    detailsClass1.setDatetocompare(getdate(discover_date));
                                    detailsClass1.setDate(getdatetime(discover_date));
                                    detailsClass1.setUDSD_address(address);
                                    detailsClass1.setUDSD_caption(caption);
//                                    detailsClass1.setUDSD_date(discover_date);
                                    detailsClass1.setUDSD_discoverID(discover_id);
                                    detailsClass1.setUDSD_mediaType(discover_type);
                                    detailsClass1.setUDSD_ownerID(ownerID);
                                    detailsClass1.setUDSD_status(discover_status);
                                    detailsClass1.setUDSD_type(dis_type);
                                }
                                alldiscoverarraylist2.add(detailsClass1);
                            }
                            JSONArray jsonArray2 = response.getJSONArray("dataDiscoverFriend");
                            for (int k = 0; k < jsonArray2.length(); k++) {
                                JSONObject value2 = jsonArray2.getJSONObject(k);

                                System.out.println("value:"+value2);

                                String discover_type = value2.getString("UDSD_mediaType");
//                                Collections.sort(alldiscoverarraylist2,UDS_DetailsClass.uds_detailsClassComparator);

                                UDS_DetailsClass detailsClass2 = new UDS_DetailsClass();
                                if(discover_type.equals("image")){
                                    String address = value2.getString("UDSD_address");
                                    String caption = value2.getString("UDSD_caption");
                                    long discover_date = Long.parseLong(value2.getString("UDSD_date"));
                                    String discover_id = value2.getString("UDSD_discoverID");
//                                    String discover_type = value2.getString("UDSD_mediaType");
                                    String discover_url = value2.getString("UDSD_mediaURL");
                                    String ownerID = value2.getString("UDSD_ownerID");
                                    Boolean discover_status = Boolean.valueOf(value2.getString("UDSD_status"));
                                    String dis_type = value2.getString("UDSD_type");


                                    detailsClass2.setDatetocompare(getdate(discover_date));
                                    detailsClass2.setDate(getdatetime(discover_date));
                                    detailsClass2.setUDSD_address(address);
                                    detailsClass2.setUDSD_caption(caption);
                                    detailsClass2.setUDSD_date(discover_date);
                                    detailsClass2.setUDSD_discoverID(discover_id);
                                    detailsClass2.setUDSD_mediaType(discover_type);
                                    detailsClass2.setUDSD_mediaURL(discover_url);
                                    detailsClass2.setUDSD_ownerID(ownerID);
                                    detailsClass2.setUDSD_status(discover_status);
                                    detailsClass2.setUDSD_type(dis_type);
                                }else{
                                    String address = value2.getString("UDSD_address");
                                    String caption = value2.getString("UDSD_caption");
                                    long discover_date = Long.parseLong(value2.getString("UDSD_date"));
                                    String discover_id = value2.getString("UDSD_discoverID");
//                                    String discover_type = value2.getString("UDSD_mediaType");
//                                    String discover_url = value2.getString("UDSD_mediaURL");
                                    String ownerID = value2.getString("UDSD_ownerID");
                                    Boolean discover_status = Boolean.valueOf(value2.getString("UDSD_status"));
                                    String dis_type = value2.getString("UDSD_type");

                                    detailsClass2.setDatetocompare(getdate(discover_date));
                                    detailsClass2.setDate(getdatetime(discover_date));
                                    detailsClass2.setUDSD_address(address);
                                    detailsClass2.setUDSD_caption(caption);
                                    detailsClass2.setUDSD_date(discover_date);
                                    detailsClass2.setUDSD_discoverID(discover_id);
                                    detailsClass2.setUDSD_mediaType(discover_type);
//                                    detailsClass2.setUDSD_mediaURL(discover_url);
                                    detailsClass2.setUDSD_ownerID(ownerID);
                                    detailsClass2.setUDSD_status(discover_status);
                                    detailsClass2.setUDSD_type(dis_type);
                                }

                                alldiscoverarraylist2.add(detailsClass2);
                                Collections.sort(alldiscoverarraylist2,UDS_DetailsClass.uds_detailsClassComparator);
                            }
//                            animationView.cancelAnimation();
//                            animationView.setVisibility(View.GONE);
                            discoverAdapter2.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(70000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);
    }

    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Date  currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        String dateplusone = df.format(ccc);
        datess = dateplusone;
        return datess;
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
            System.out.println("datetimes"+datetimes);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = "2 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = "3 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = "4 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = "5 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = "6 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = "7 days ago";
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }

    public void gotodiscoversetup(){
        Intent i = new Intent(getContext(), DiscoverSetup.class);
        i.putExtra("username", getUname());
        i.putExtra("avatar",getAvatar());

        if(Build.VERSION.SDK_INT>20){
            ActivityOptions options =
                    ActivityOptions.makeSceneTransitionAnimation(getActivity());
            startActivity(i,options.toBundle());
        }else {
            startActivity(i);
        }
    }

    public void retrieve_name_avatar(String userid){
        metadata.child(userid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UD_displayName")){
                    String uname = dataSnapshot.child("UD_displayName").getValue().toString();
                    setUname(uname);
                }
                if(dataSnapshot.hasChild("UD_avatarURL")){
                    String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    setAvatar(avatar);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public Date getdate(long timestamp){
        datesss = new Date(timestamp);
        return datesss;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUDSD_caption() {
        return UDSD_caption;
    }

    public void setUDSD_caption(String UDSD_caption) {
        this.UDSD_caption = UDSD_caption;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}