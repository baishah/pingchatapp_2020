package com.app.pingchat.Discover;

public class NW_detailsClass {

    private String NWD_newsAddress,NWD_newsDescription,NWD_newsID,NWD_newsTitle,NWD_newsURL;
    private long NWD_newsTimestamp;
    private String NWM_mediaType;
    private String NWM_mediaURL;
    private String NWL_latitude;
    private String NWL_longitude;


    public NW_detailsClass(){}

    public NW_detailsClass(String NWD_newsAddress,String NWD_newsDescription,String NWD_newsURL,String NWD_newsID, String NWD_newsTitle, long NWD_newsTimestamp, String NWM_mediaType, String NWM_mediaURL, String NWL_latitude, String NWL_longitude) {
        this.NWD_newsAddress=NWD_newsAddress;
        this.NWD_newsDescription = NWD_newsDescription;
        this.NWD_newsID = NWD_newsID;
        this.NWD_newsTimestamp = NWD_newsTimestamp;
        this.NWD_newsTitle = NWD_newsTitle;
        this.NWD_newsURL=NWD_newsURL;
        this.NWM_mediaType = NWM_mediaType;
        this.NWM_mediaURL= NWM_mediaURL;
        this.NWL_latitude=NWL_latitude;
        this.NWL_longitude=NWL_longitude;
    }


    public String getNWD_newsAddress() {
        return NWD_newsAddress;
    }

    public void setNWD_newsAddress(String NWD_newsAddress) {
        this.NWD_newsAddress = NWD_newsAddress;
    }

    public String getNWD_newsDescription() {
        return NWD_newsDescription;
    }

    public void setNWD_newsDescription(String NWD_newsDescription) {
        this.NWD_newsDescription = NWD_newsDescription;
    }

    public String getNWD_newsID() {
        return NWD_newsID;
    }

    public void setNWD_newsID(String NWD_newsID) {
        this.NWD_newsID = NWD_newsID;
    }

    public String getNWD_newsTitle() {
        return NWD_newsTitle;
    }

    public void setNWD_newsTitle(String NWD_newsTitle) {
        this.NWD_newsTitle = NWD_newsTitle;
    }

    public String getNWD_newsURL() {
        return NWD_newsURL;
    }

    public void setNWD_newsURL(String NWD_newsURL) {
        this.NWD_newsURL = NWD_newsURL;
    }

    public long getNWD_newsTimestamp() {
        return NWD_newsTimestamp;
    }

    public void setNWD_newsTimestamp(long NWD_newsTimestamp) {
        this.NWD_newsTimestamp = NWD_newsTimestamp;
    }

    public String getNWM_mediaType() {
        return NWM_mediaType;
    }

    public void setNWM_mediaType(String NWM_mediaType) {
        this.NWM_mediaType = NWM_mediaType;
    }

    public String getNWM_mediaURL() {
        return NWM_mediaURL;
    }

    public void setNWM_mediaURL(String NWM_mediaURL) {
        this.NWM_mediaURL = NWM_mediaURL;
    }

//    public String getNWL_latitude() {
//        return NWL_latitude;
//    }
//
//    public void setNWL_latitude(String NWL_latitude) {
//        this.NWL_latitude = NWL_latitude;
//    }
//
//    public String getNWL_longitude() {
//        return NWL_longitude;
//    }
//
//    public void setNWL_longitude(String NWL_longitude) {
//        this.NWL_longitude = NWL_longitude;
//    }
}
