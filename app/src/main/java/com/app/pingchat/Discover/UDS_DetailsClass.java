package com.app.pingchat.Discover;

import java.util.Comparator;
import java.util.Date;

public class UDS_DetailsClass {

    private String UDSD_discoverID,UDSD_type,UDSD_address,UDSD_caption,UDSD_ownerID,UDSD_mediaType,UDSD_mediaURL,UDSD_mediaThumbnailURL;
    private String displayName;
    private String avatarURL;
    private String deviceToken;
    private boolean UDSD_status;
    private long UDSD_date;
    private String date;
    private Date datetocompare;

    public UDS_DetailsClass(){}

    public UDS_DetailsClass(Date datetocompare,String date,String displayName,String avatarURL,String deviceToken,String UDSD_discoverID, String UDSD_type, String UDSD_address, String UDSD_caption, String UDSD_ownerID, String UDSD_mediaType, String UDSD_mediaURL,boolean UDSD_status, long UDSD_date,String UDSD_mediaThumbnailURL) {
        this.UDSD_discoverID = UDSD_discoverID;
        this.UDSD_type = UDSD_type;
        this.UDSD_address = UDSD_address;
        this.UDSD_caption = UDSD_caption;
        this.UDSD_ownerID = UDSD_ownerID;
        this.UDSD_mediaType = UDSD_mediaType;
        this.UDSD_mediaURL = UDSD_mediaURL;
        this.UDSD_status = UDSD_status;
        this.UDSD_date = UDSD_date;
        this.displayName = displayName;
        this.avatarURL = avatarURL;
        this.deviceToken = deviceToken;
        this.date = date;
        this.datetocompare = datetocompare;
        this.UDSD_mediaThumbnailURL =UDSD_mediaThumbnailURL;
    }

    public String getUDSD_discoverID() {
        return UDSD_discoverID;
    }

    public void setUDSD_discoverID(String UDSD_discoverID) {
        this.UDSD_discoverID = UDSD_discoverID;
    }

    public boolean isUDSD_status() {
        return UDSD_status;
    }

    public void setUDSD_status(boolean UDSD_status) {
        this.UDSD_status = UDSD_status;
    }

    public String getUDSD_type() {
        return UDSD_type;
    }

    public void setUDSD_type(String UDSD_type) {
        this.UDSD_type = UDSD_type;
    }

    public String getUDSD_address() {
        return UDSD_address;
    }

    public void setUDSD_address(String UDSD_address) {
        this.UDSD_address = UDSD_address;
    }

    public String getUDSD_caption() {
        return UDSD_caption;
    }

    public void setUDSD_caption(String UDSD_caption) {
        this.UDSD_caption = UDSD_caption;
    }

    public String getUDSD_ownerID() {
        return UDSD_ownerID;
    }

    public void setUDSD_ownerID(String UDSD_ownerID) {
        this.UDSD_ownerID = UDSD_ownerID;
    }

    public String getUDSD_mediaType() {
        return UDSD_mediaType;
    }

    public void setUDSD_mediaType(String UDSD_mediaType) {
        this.UDSD_mediaType = UDSD_mediaType;
    }

    public String getUDSD_mediaURL() {
        return UDSD_mediaURL;
    }

    public void setUDSD_mediaURL(String UDSD_mediaURL) {
        this.UDSD_mediaURL = UDSD_mediaURL;
    }

    public long getUDSD_date() {
        return UDSD_date;
    }

    public void setUDSD_date(long UDSD_date) {
        this.UDSD_date = UDSD_date;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }

    public Date getDatetocompare() { return datetocompare; }

    public void setDatetocompare(Date datetocompare) { this.datetocompare = datetocompare; }

    public String getUDSD_mediaThumbnailURL() {
        return UDSD_mediaThumbnailURL;
    }

    public void setUDSD_mediaThumbnailURL(String UDSD_mediaThumbnailURL) {
        this.UDSD_mediaThumbnailURL = UDSD_mediaThumbnailURL;
    }

    public  static Comparator<UDS_DetailsClass> uds_detailsClassComparator = new Comparator<UDS_DetailsClass>() {
        @Override
        public int compare(UDS_DetailsClass t0, UDS_DetailsClass t1) {
            return t1.getDatetocompare().compareTo(t0.getDatetocompare());
        }
    };
}
