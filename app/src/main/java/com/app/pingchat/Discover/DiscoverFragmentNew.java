package com.app.pingchat.Discover;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.R;
import com.app.pingchat.UserAuth.RegisterActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class DiscoverFragmentNew  extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{

    ImageButton newpost, searchBtn, refresh;
    Uri uriSavedImage;
    EditText searchFilter;
    private Toolbar toolbar;
    private Uri post;
    FirebaseAuth mAuth;
    private String current_user;
    private String uname;
    private String avatar;
    private String UDSD_caption;
    private RecyclerView discoverRecyclerView, storyView;
    RelativeLayout btnnewpost;
    ImageView nopost;
    private ArrayList discover_key_array = new ArrayList();
    ArrayList<Discovers> discoversArrayList;
    ArrayList<NW_detailsClass> adsDiscoverList;
    ArrayList<UDS_DetailsClass>alldiscoverarraylist2;
    ArrayList<UDS_DetailsClass>alldiscovernewlist;
    DiscoverAdapter discoverAdapter;
    DiscoverAdapter2 discoverAdapter2;
    public DatabaseReference discover,discoverSession,metadata,userDiscover,publicdiscover,adsNews,media,userfrienddiscover,discoverpartner;
    public void setPost(Uri post) { this.post = post; }
    public static Context contextOfApplication;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList alldiscoverkey = new ArrayList();
    StorageReference storeRef;
    FirebaseStorage storage;
    private RequestQueue mQueue;
    private int page=1;


    SearchView searchView;
    ScrollView scrollView;
    private Button loadmore;



    String datetimes;
    String datess;
    Date datesss,datetocompare;
    LottieAnimationView animationView;
    ProgressDialog progressDialog;
    String time = "5";

    LinearLayoutManager manager;
    ProgressBar progressBar;
    private Boolean isNoMore = false;
    private int visibleThreshold = 1;
    private int pastVisibleItems, visibleItemCount,totalItemcount, previous = 0;
    String apiUrl= "https://devbm.ml/discover/allPost";

    private int totalitemcount;
    private int firstvisibleitem;
    private int visibleitemcount;
    private int pages =0;
    private int previoustotal;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.discover_fragment, container, false);

        newpost = (ImageButton) v.findViewById(R.id.newPost);
        toolbar = v.findViewById(R.id.toolbar);
        refresh = v.findViewById(R.id.refresh);
//        searchFilter = v.findViewById(R.id.search_view);
        nopost = v.findViewById(R.id.noPost);
        mAuth = FirebaseAuth.getInstance();
        manager = new LinearLayoutManager(getContext());

        discoverRecyclerView = (RecyclerView) v.findViewById(R.id.discoverlist);
        storyView = v.findViewById(R.id.storyView);
        discoverRecyclerView.setLayoutManager(manager);
        progressBar = v.findViewById(R.id.progressBar);
        loadmore = v.findViewById(R.id.loadMore);


        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        mQueue = Volley.newRequestQueue(getContext());
        alldiscoverarraylist2 = new ArrayList();
        scrollView = v.findViewById(R.id.scrollview);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading more list");

//            discoverRecyclerView.setVisibility(View.GONE);
//            animationView.cancelAnimation();
//            animationView.setVisibility(View.GONE);
            discoverAdapter2 = new DiscoverAdapter2(getContext(),alldiscoverarraylist2,current_user);
            discoverRecyclerView.setAdapter(discoverAdapter2);


        discoverRecyclerView.addOnScrollListener(discoverOnScrollListener);


        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this::getMoredata);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        v.findViewById(R.id.rel_lay).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });
        contextOfApplication = getContext().getApplicationContext();

        btnnewpost = (RelativeLayout) v.findViewById(R.id.btnNewpost);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        storyView.setLayoutManager(layoutManager);

        newpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            discoverpartner.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        String UPD_partnerID = dataSnapshot.child("UPD_partnerID").getValue().toString();
                        DatabaseReference partner = FirebaseDatabase.getInstance().getReference().child("Partners").child(UPD_partnerID);
                        partner.child("P_partnerDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String p_name = dataSnapshot.child("P_partnerName").getValue().toString();
                                    String p_avatar = dataSnapshot.child("P_profileURL").getValue().toString();
                                    System.out.println("partner id"+UPD_partnerID);
                                    CharSequence option[] = new CharSequence[]
                                            {
                                                    "Create New Post",
                                                    "Create New Post as Partners"
                                            };
                                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
                                    builder.setItems(option, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if(i==0){
                                                gotodiscoversetup("user",getUname(),getAvatar());
                                            }
                                            else if(i==1){
                                                gotodiscoversetup("partner",p_name,p_avatar);
                                            }
                                        }
                                    });
                                    builder.show();
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }
                    else{
                        gotodiscoversetup("user",getUname(),getAvatar());
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                animationView.cancelAnimation();
//                animationView.setVisibility(View.GONE);
                getAdsDiscover(current_user);
             //   getDiscover();
               // getMoredata();
            }
        });

        loadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("loadmore", "clicked");
//                progressBar.setVisibility(View.VISIBLE);
                progressDialog.show();
                loadmore.setVisibility(View.VISIBLE);
                getMoredata();
            }
        });

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        animationView = (LottieAnimationView) v.findViewById(R.id.animation_view);
//        animationView.setVisibility(View.VISIBLE);
//        animationView.setAnimation("loading_list.json");
//        animationView.playAnimation();


        if(mAuth.getCurrentUser()!=null){
            current_user = mAuth.getCurrentUser().getUid();
            discover = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Discover");
            discoverSession = FirebaseDatabase.getInstance().getReference().child("DiscoverSession").child("Friend");
            metadata = FirebaseDatabase.getInstance().getReference().child("Users");
            userDiscover = FirebaseDatabase.getInstance().getReference().child("Users");
            publicdiscover = FirebaseDatabase.getInstance().getReference().child("PublicDiscover");
            adsNews = FirebaseDatabase.getInstance().getReference().child("News");
            adsNews.keepSynced(true);
            discoverpartner = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("userPartnerDetails");

            retrieve_name_avatar(current_user);
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {

                    //getalldiscover();
                    mSwipeRefreshLayout.setRefreshing(true);
                    //   getMoredata();
                    //getDiscover();
                    getAdsDiscover(current_user);
                }
            });
        }
        else{
            Intent in = new Intent(getActivity(), RegisterActivity.class);
            startActivity(in);
        }
        return v;
    }

    private RecyclerView.OnScrollListener discoverOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

            Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.hide);
            Animation animation1 = AnimationUtils.loadAnimation(getContext(),R.anim.alpha);
            super.onScrolled(recyclerView, dx, dy);

//
            if (isLastItemDisplaying(recyclerView)) {
//                getMoredata();
                Log.i("arraylist", "loadMore");

                loadmore.setVisibility(View.VISIBLE);

            }
//
            if (dy > 0  && btnnewpost.getVisibility() != View.VISIBLE) {
                btnnewpost.setVisibility(View.GONE);
                btnnewpost.startAnimation(animation);
            }
            else if (dy < 0 && btnnewpost.getVisibility() != View.VISIBLE)
            {
                btnnewpost.setVisibility(View.GONE);
                btnnewpost.startAnimation(animation);
            }
            else {
                btnnewpost.setVisibility(View.VISIBLE);
                btnnewpost.startAnimation(animation1);
            }
        }
    };


    private boolean isLastItemDisplaying(RecyclerView recyclerView){
        if(recyclerView.getAdapter().getItemCount() != 0){
//            System.out.println("count:"+recyclerView.getAdapter().getItemCount());
            int lastVisibleItemPosition = ((LinearLayoutManager)recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
//            System.out.println("count:"+lastVisibleItemPosition);
            System.out.println("count:"+recyclerView.getAdapter().getItemCount());
            if(lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount()-1)
                System.out.println("scroll");
            return true;
        }
        return false;
    }

    @Override
    public void onStart(){
        super.onStart();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        DiscoverAdapter2 discoverAdapter2 = new DiscoverAdapter2(getContext(),alldiscoverarraylist2,current_user);
//        inflater.inflate(R.menu.menu2,menu);
        inflater.inflate(R.menu.menu_chat, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

//                    discoverAdapter2.getFilter().filter(query);
//                    Toast.makeText(getContext(), "No Match found",Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                discoverAdapter2.getFilter().filter(newText);
//                display_discover_on_refresh();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRefresh() {
        //getDiscovers();
        //getalldiscover();
        // get_all_discover2();
        getAdsDiscover(current_user);
     //   getDiscover();
       // getMoredata();
//        display_discover_on_refresh();
    }

    private void getAdsDiscover(String c_user) {

        adsDiscoverList = new ArrayList();
        NewsListAdapter newsListAdapter = new NewsListAdapter(getContext(), adsDiscoverList, c_user);
        storyView.setAdapter(newsListAdapter);

        String url = "https://devbm.ml/news/all";

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("dataNews");

                            for (int i = 0; i < jsonArray.length()/2; i++) {
                                JSONObject value = jsonArray.getJSONObject(i);

                                String nwd_newsID = value.getString("NWD_newsID");
                                long nwd_newsTimestamp = Long.parseLong(value.getString("NWD_newsTimestamp"));
                                String nwd_newsTitle = value.getString("NWD_newsTitle");
                                String nwd_newsURL = value.getString("NWD_newsURL");
                                String nwd_newsAddress = value.getString("NWD_newsAddress");
                                String nwd_newsDescription = value.getString("NWD_newsDescription");
                                String nwl_latitude = value.getString("NWL_latitude");
                                String nwl_longitude = value.getString("NWL_longitude");
                                String nwm_mediaType = value.getString("NWM_mediaType");
                                String nwm_mediaURL = value.getString("NWM_mediaURL");

                                NW_detailsClass nw_detailsClass = new NW_detailsClass();

                                nw_detailsClass.setNWD_newsID(nwd_newsID);
                                nw_detailsClass.setNWD_newsTimestamp(nwd_newsTimestamp);
                                nw_detailsClass.setNWD_newsTitle(nwd_newsTitle);
                                nw_detailsClass.setNWD_newsURL(nwd_newsURL);
                                nw_detailsClass.setNWD_newsAddress(nwd_newsAddress);
                                nw_detailsClass.setNWD_newsDescription(nwd_newsDescription);
                                nw_detailsClass.setNWM_mediaType(nwm_mediaType);
                                nw_detailsClass.setNWM_mediaURL(nwm_mediaURL);

                                adsDiscoverList.add(nw_detailsClass);
                            }
                            newsListAdapter.notifyDataSetChanged();
                         //   getDiscover();
                         //
                            getMoredata();
//                            progressDialog.dismiss();
//                            nearbyMarketAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);
    }

    private void getDiscover() {

        String url = "https://devbm.ml/discover/allDiscoverPosts";

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
        postParams.put("keyUID",current_user);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("dataDiscovers");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject value = jsonArray.getJSONObject(i);

                                String discover_type = value.getString("UDSD_mediaType");
                                System.out.println("zzzzz"+value.getString("UDSD_type"));
                                UDS_DetailsClass detailsClass = new UDS_DetailsClass();
                                if (discover_type.equals("image")) {
                                    String address = value.getString("UDSD_address");
                                    String caption = value.getString("UDSD_caption");
                                    long discover_date = value.getLong("UDSD_date");
                                    String discover_id = value.getString("UDSD_discoverID");
                                    String discover_url = value.getString("UDSD_mediaURL");
                                    String ownerID = value.getString("UDSD_ownerID");
                                    Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
                                    String dis_type = value.getString("UDSD_type");

                                    detailsClass.setDatetocompare(getdate(discover_date));
                                    detailsClass.setDate(getdatetime(discover_date));
                                    detailsClass.setUDSD_address(address);
                                    detailsClass.setUDSD_caption(caption);
                                    //                  detailsClass.setUDSD_date(discover_date);
                                    detailsClass.setUDSD_discoverID(discover_id);
                                    detailsClass.setUDSD_mediaType(discover_type);
                                    detailsClass.setUDSD_mediaURL(discover_url);
                                    detailsClass.setUDSD_ownerID(ownerID);
                                    detailsClass.setUDSD_status(discover_status);
                                    detailsClass.setUDSD_type(dis_type);
                                } else if(discover_type.equals("video")){
                                    String address = value.getString("UDSD_address");
                                    String caption = value.getString("UDSD_caption");
                                    long discover_date = value.getLong("UDSD_date");
                                    String discover_id = value.getString("UDSD_discoverID");
                                    String discover_url = value.getString("UDSD_mediaURL");
                                    String discoverThumb = value.getString("UDSD_mediaThumbnailURL");
                                    String ownerID = value.getString("UDSD_ownerID");
                                    Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
                                    String dis_type = value.getString("UDSD_type");

                                    detailsClass.setDatetocompare(getdate(discover_date));
                                    detailsClass.setDate(getdatetime(discover_date));
                                    detailsClass.setUDSD_address(address);
                                    detailsClass.setUDSD_caption(caption);
                                    detailsClass.setUDSD_mediaThumbnailURL(discoverThumb);
                                    detailsClass.setUDSD_discoverID(discover_id);
                                    detailsClass.setUDSD_mediaType(discover_type);
                                    detailsClass.setUDSD_mediaURL(discover_url);
                                    detailsClass.setUDSD_ownerID(ownerID);
                                    detailsClass.setUDSD_status(discover_status);
                                    detailsClass.setUDSD_type(dis_type);
                                }
                                else {
                                    String address = value.getString("UDSD_address");
                                    String caption = value.getString("UDSD_caption");
                                    long discover_date = value.getLong("UDSD_date");
                                    String discover_id = value.getString("UDSD_discoverID");
                                    String ownerID = value.getString("UDSD_ownerID");
                                    Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
                                    String dis_type = value.getString("UDSD_type");

                                    detailsClass.setDatetocompare(getdate(discover_date));
                                    detailsClass.setDate(getdatetime(discover_date));
                                    detailsClass.setUDSD_address(address);
                                    detailsClass.setUDSD_caption(caption);
                                    //                  detailsClass.setUDSD_date(discover_date);
                                    detailsClass.setUDSD_discoverID(discover_id);
                                    detailsClass.setUDSD_mediaType(discover_type);
                                    detailsClass.setUDSD_ownerID(ownerID);
                                    detailsClass.setUDSD_status(discover_status);
                                    detailsClass.setUDSD_type(dis_type);
                                }
                                alldiscoverarraylist2.add(detailsClass);
                            }

                            discoverAdapter2.notifyDataSetChanged();
                            mSwipeRefreshLayout.setRefreshing(false);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);
    }

    private void getMoredata() {
        System.out.println("1 cycle");
        mQueue.add(getDataFromServer(page,current_user));

        page=+20;
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private JsonObjectRequest getDataFromServer(final int page, String id) {
        System.out.println("2 cycle");
        //loading progress

        String url = "https://devbm.ml/discover/allDiscoverPosts";

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
        postParams.put("keyUID",id);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        progressDialog.dismiss();
                        Log.d("Volley", response.toString());
                        parseData(page,response);

                        Log.d("URL:", url+ String.valueOf(page));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(70000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        mQueue.add(jsonObjReq);

        return jsonObjReq;
    }

    private void parseData(int page ,JSONObject response) {
        System.out.println("3 cycle");
        try {

            JSONArray jsonArray = response.getJSONArray("dataDiscovers");

            for (int i= 0; i < 50; i++) {
                JSONObject value = jsonArray.getJSONObject(i);

                String discover_type = value.getString("UDSD_mediaType");
                System.out.println("zzzzz"+value.getString("UDSD_type"));
                UDS_DetailsClass detailsClass = new UDS_DetailsClass();
                if (discover_type.equals("image")) {
                    String address = value.getString("UDSD_address");
                    String caption = value.getString("UDSD_caption");
                    long discover_date = value.getLong("UDSD_date");
                    String discover_id = value.getString("UDSD_discoverID");
                    String discover_url = value.getString("UDSD_mediaURL");
                    String ownerID = value.getString("UDSD_ownerID");
                    Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
                    String dis_type = value.getString("UDSD_type");

                    detailsClass.setDatetocompare(getdate(discover_date));
                    detailsClass.setDate(getdatetime(discover_date));
                    detailsClass.setUDSD_address(address);
                    detailsClass.setUDSD_caption(caption);
                    //                  detailsClass.setUDSD_date(discover_date);
                    detailsClass.setUDSD_discoverID(discover_id);
                    detailsClass.setUDSD_mediaType(discover_type);
                    detailsClass.setUDSD_mediaURL(discover_url);
                    detailsClass.setUDSD_ownerID(ownerID);
                    detailsClass.setUDSD_status(discover_status);
                    detailsClass.setUDSD_type(dis_type);
                } else if(discover_type.equals("video")){
                    String address = value.getString("UDSD_address");
                    String caption = value.getString("UDSD_caption");
                    long discover_date = value.getLong("UDSD_date");
                    String discover_id = value.getString("UDSD_discoverID");
                    String discover_url = value.getString("UDSD_mediaURL");
                    String discoverThumb = value.getString("UDSD_mediaThumbnailURL");
                    String ownerID = value.getString("UDSD_ownerID");
                    Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
                    String dis_type = value.getString("UDSD_type");

                    detailsClass.setDatetocompare(getdate(discover_date));
                    detailsClass.setDate(getdatetime(discover_date));
                    detailsClass.setUDSD_address(address);
                    detailsClass.setUDSD_caption(caption);
                    detailsClass.setUDSD_mediaThumbnailURL(discoverThumb);
                    detailsClass.setUDSD_discoverID(discover_id);
                    detailsClass.setUDSD_mediaType(discover_type);
                    detailsClass.setUDSD_mediaURL(discover_url);
                    detailsClass.setUDSD_ownerID(ownerID);
                    detailsClass.setUDSD_status(discover_status);
                    detailsClass.setUDSD_type(dis_type);
                }
                else {
                    String address = value.getString("UDSD_address");
                    String caption = value.getString("UDSD_caption");
                    long discover_date = value.getLong("UDSD_date");
                    String discover_id = value.getString("UDSD_discoverID");
                    String ownerID = value.getString("UDSD_ownerID");
                    Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
                    String dis_type = value.getString("UDSD_type");

                    detailsClass.setDatetocompare(getdate(discover_date));
                    detailsClass.setDate(getdatetime(discover_date));
                    detailsClass.setUDSD_address(address);
                    detailsClass.setUDSD_caption(caption);
                    //                  detailsClass.setUDSD_date(discover_date);
                    detailsClass.setUDSD_discoverID(discover_id);
                    detailsClass.setUDSD_mediaType(discover_type);
                    detailsClass.setUDSD_ownerID(ownerID);
                    detailsClass.setUDSD_status(discover_status);
                    detailsClass.setUDSD_type(dis_type);
                }
                alldiscoverarraylist2.add(detailsClass);
            }

            discoverAdapter2.notifyDataSetChanged();
//            if(page > totalPage){
//                progressDialog.dismiss();
//                loadmore.setVisibility(View.GONE);
//                Toast.makeText(getContext(),"All data load completely",Toast.LENGTH_SHORT).show();
//            }
//
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Date  currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        String dateplusone = df.format(ccc);
        datess = dateplusone;
        return datess;
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
            System.out.println("datetimes"+datetimes);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = "2 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = "3 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = "4 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = "5 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = "6 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = "7 days ago";
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }

    public void gotodiscoversetup(String role,String uname,String avatar){

     //   Toast.makeText(getContext(),"gotodiscoversetup "+role+uname+avatar,Toast.LENGTH_SHORT).show();

        Intent i = new Intent(getContext(), DiscoverSetup.class);
        i.putExtra("username", uname);
        i.putExtra("avatar",avatar);
        i.putExtra("role",role);

        if(Build.VERSION.SDK_INT>20){
            ActivityOptions options =
                    ActivityOptions.makeSceneTransitionAnimation(getActivity());
            startActivity(i,options.toBundle());
        }else {
            startActivity(i);
        }
    }

    public void retrieve_name_avatar(String userid){
        metadata.child(userid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UD_displayName")){
                    String uname = dataSnapshot.child("UD_displayName").getValue().toString();
                    setUname(uname);
                }
                if(dataSnapshot.hasChild("UD_avatarURL")){
                    String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    setAvatar(avatar);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public Date getdate(long timestamp){
        datesss = new Date(timestamp);
        return datesss;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUDSD_caption() {
        return UDSD_caption;
    }

    public void setUDSD_caption(String UDSD_caption) {
        this.UDSD_caption = UDSD_caption;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
