package com.app.pingchat.Discover;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class DiscoveryCommentsAdapter extends RecyclerView.Adapter<DiscoveryCommentsAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference metadata;
    Context context;
    ArrayList<DiscoveryComments> discoveryCommentsArrayList;
    private FirebaseAuth mAuths;
    boolean isTextViewClicked = false;
    private String aftertrimmed;
    private String currentuser;
    public DatabaseReference discover,discoverSession,user;
    ArrayList list_of_discover;

    public DiscoveryCommentsAdapter(ArrayList<DiscoveryComments> discoveryCommentsArrayList1){
        discoveryCommentsArrayList = discoveryCommentsArrayList1;
    }

    public DiscoveryCommentsAdapter(Context c, ArrayList<DiscoveryComments> d){
        context = c;
        discoveryCommentsArrayList = d;

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.comment_item_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final DiscoveryComments discoveryComments = discoveryCommentsArrayList.get(position);
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        System.out.println("current user "+currentuser);

        holder.uname.setText(discoveryComments.getUsername());
        holder.comment.setText(discoveryComments.getUDSC_comment());
        holder.likebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.likebtn.setVisibility(View.INVISIBLE);
                holder.likedbtn.setVisibility(View.VISIBLE);
            }
        });

        holder.time.setText(discoveryComments.getUDSC_timestamp());

        String avatarurl= discoveryComments.getUser_profile();

        if(avatarurl.startsWith("p")){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatarurl);
            GlideApp.with(context).load(storageReference).placeholder(R.drawable.loading_img).into(holder.userimage);
        }
        else if(avatarurl.startsWith("h")){
            GlideApp.with(context).load(avatarurl).placeholder(R.drawable.loading_img).into(holder.userimage);
        }
//        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(discoveryComments.getUser_profile());
//        GlideApp.with(context.getApplicationContext())
//                .load(storageReference)
//                .into(holder.userimage);

    }


    @Override
    public int getItemCount() {
        return discoveryCommentsArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView uname,comment,time;
        CircleImageView userimage;
        ImageButton likebtn, likedbtn;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);
            uname = itemView.findViewById(R.id.user_name);
            comment = itemView.findViewById(R.id.status);
            time = itemView.findViewById(R.id.time);
            userimage = itemView.findViewById(R.id.user_avatar_bg);
            likebtn = itemView.findViewById(R.id.likebtn);
            likedbtn = itemView.findViewById(R.id.likedbtn);

        }
    }

}