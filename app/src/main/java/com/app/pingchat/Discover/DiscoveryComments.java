package com.app.pingchat.Discover;

public class DiscoveryComments {

   String UDSC_ownerID,UDSC_comment,UDSC_timestamp,UDSC_mediaType,UDSC_mediaURL,username,user_profile;


   public DiscoveryComments (){}

    public DiscoveryComments(String UDSC_ownerID, String UDSC_comment, String UDSC_timestamp, String UDSC_mediaType, String UDSC_mediaURL) {
        this.UDSC_ownerID = UDSC_ownerID;
        this.UDSC_comment = UDSC_comment;
        this.UDSC_timestamp = UDSC_timestamp;
        this.UDSC_mediaType = UDSC_mediaType;
        this.UDSC_mediaURL = UDSC_mediaURL;
    }

    public String getUDSC_ownerID() {
        return UDSC_ownerID;
    }

    public void setUDSC_ownerID(String UDSC_ownerID) {
        this.UDSC_ownerID = UDSC_ownerID;
    }

    public String getUDSC_comment() {
        return UDSC_comment;
    }

    public void setUDSC_comment(String UDSC_comment) {
        this.UDSC_comment = UDSC_comment;
    }

    public String getUDSC_timestamp() {
        return UDSC_timestamp;
    }

    public void setUDSC_timestamp(String UDSC_timestamp) {
        this.UDSC_timestamp = UDSC_timestamp;
    }

    public String getUDSC_mediaType() {
        return UDSC_mediaType;
    }

    public void setUDSC_mediaType(String UDSC_mediaType) {
        this.UDSC_mediaType = UDSC_mediaType;
    }

    public String getUDSC_mediaURL() {
        return UDSC_mediaURL;
    }

    public void setUDSC_mediaURL(String UDSC_mediaURL) {
       this.UDSC_mediaURL = UDSC_mediaURL;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_profile() {
        return user_profile;
    }

    public void setUser_profile(String user_profile) {
        this.user_profile = user_profile;
    }
}
