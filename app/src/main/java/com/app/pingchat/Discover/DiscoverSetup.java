package com.app.pingchat.Discover;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Slide;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;

import com.app.pingchat.CompleteSendDiscoverPost;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Ping.PingSetup;
import com.app.pingchat.Ping.PingSetup2;
import com.app.pingchat.PlaceFieldSelector;
import com.app.pingchat.R;
import com.app.pingchat.filterActivitity;
import com.app.pingchat.location;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jackandphantom.blurimage.BlurImage;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.media.MediaRecorder.VideoSource.CAMERA;
import static androidx.constraintlayout.widget.Constraints.TAG;
public class DiscoverSetup extends AppCompatActivity {

    private DatabaseReference userdiscover,discoversession,userfriends,userDiscover;
    private FirebaseAuth mAuth;
    private String current_user,videoByte,datatype,role;
    private ImageView photo_p_holder,thumbV;
    private TextView caption,uname,type,photo,video,loc, reminder;
    private CircleImageView dp;
    public StorageReference discoverimage,discovervideo,discoverThumbnail;
    private ImageButton close;
    private byte[] avatar;
    private Button done;
    private VideoView videoView;
    private LinearLayout compressionMsg;

    private String yourcaption,yourlocation;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private int GALLERY = 2, CAMERA = 3;

    private PlaceFieldSelector fieldSelector;
    private String api_key = "AIzaSyDOewqTvU226Fe7ZTYIobI58oJomTnPIMQ";

    com.app.pingchat.location location1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discover_setup_layout2);
        setAnimation();

        mAuth = FirebaseAuth.getInstance();
        current_user = mAuth.getCurrentUser().getUid();
        userfriends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Conversation").child("Personal");
        discoversession = FirebaseDatabase.getInstance().getReference().child("DiscoverSession").child("Friend");
        userdiscover = FirebaseDatabase.getInstance().getReference().child("Users");
        discoverimage = FirebaseStorage.getInstance().getReference().child("postImage");
        discovervideo = FirebaseStorage.getInstance().getReference().child("postVideo");
        discoverThumbnail = FirebaseStorage.getInstance().getReference().child("postVideoThumbnails");
        userDiscover = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("userDiscovers");
        datatype = "noMedia";

//      uname = findViewById(R.id.username);
        dp = findViewById(R.id.user_dp);
        type = findViewById(R.id.type);
        photo = findViewById(R.id.photo);
        photo_p_holder = findViewById(R.id.photo_p_holder);
        loc = findViewById(R.id.location);
        caption = findViewById(R.id.yourcaption);
        done = findViewById(R.id.done);
        close = findViewById(R.id.closeButton);
        reminder = findViewById(R.id.reminder);
        thumbV= findViewById(R.id.video_p_holder);
        video = findViewById(R.id.video);
        videoView = findViewById(R.id.videoView);

        Intent intent = getIntent();
//      uname.setText(intent.getStringExtra("username"));

        String avatar = intent.getStringExtra("avatar");
        role = intent.getStringExtra("role");

        if(role.equals("partner")){
            video.setVisibility(View.GONE);
            type.setVisibility(View.GONE);
        }

        if (avatar.startsWith("p")) {
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
            if (getApplicationContext() != null) {
                GlideApp.with(getApplicationContext())
                        .load(storageReference)
                        .into(dp);
            }
        } else if (avatar.startsWith("h")) {
            if (getApplicationContext() != null) {
                GlideApp.with(getApplicationContext().getApplicationContext())
                        .load(avatar)
                        .into(dp);
            }
        }

        Places.initialize(getApplicationContext(), api_key);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), api_key);
        }

        fieldSelector = new PlaceFieldSelector();

        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                reminder.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 786);
                } else {
                    CropImage.startPickImageActivity(DiscoverSetup.this);
                }
            }
        });

        photo_p_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(DiscoverSetup.this);
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    showPictureDialog();
            }
        });
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yourcaption = caption.getText().toString();
                yourlocation = loc.getText().toString();

                if(yourcaption.isEmpty()){
                    Toast.makeText(DiscoverSetup.this, "Please specify your caption", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(yourlocation.isEmpty()){
                    Toast.makeText(DiscoverSetup.this, "Please add your location to proceed", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(location1==null){
                    Toast.makeText(DiscoverSetup.this, "Please add your location to proceed", Toast.LENGTH_SHORT).show();
                    return;
                }
                newdiscover();
//                if ((yourcaption != null && !yourcaption.isEmpty()) && (yourlocation != null && !yourlocation.isEmpty())&&(location1!=null)) {
//                    if(yourcaption != null && !yourcaption.isEmpty()){
//                        Toast.makeText(DiscoverSetup.this, "Please specify your caption", Toast.LENGTH_SHORT).show();
//                    }
//                    else if(yourlocation != null && !yourlocation.isEmpty()){
//                        Toast.makeText(DiscoverSetup.this, "Please add your location to proceed", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//
//                    }
//
//                } else {
//                    Toast.makeText(DiscoverSetup.this, "Please fill in all required information", Toast.LENGTH_SHORT).show();
//               }
            }
        });

        type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.getText() == "Public") {
                    type.setText("Friend");
                    type.setBackgroundResource(R.drawable.round_blue);
                    type.setTextColor(Color.WHITE);
                } else {
                    type.setText("Public");
                    type.setBackgroundResource(R.drawable.round_gray);
                    type.setTextColor(Color.BLACK);
                }
            }
        });

        loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fieldSelector.getAllFields()).build(DiscoverSetup.this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            }
        });
    }

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select video from gallery",
                "Record video from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                chooseVideoFromGallery();
                                break;
                            case 1:
                                takeVideoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void chooseVideoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takeVideoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    public void sendPostComplete() {
        Intent intent = new Intent(getApplicationContext(), CompleteSendDiscoverPost.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void newdiscover(){
        if(role.equals("user")){
            ProgressDialog dialog= new ProgressDialog(getApplicationContext());
            dialog.setMessage("Uploading new Post..");
            dialog.show();

            String discoverid = discoversession.push().getKey();
            String mediaURL = "postImage/"+discoverid+".jpg";
            String videoUrl = "postVideo/" + discoverid + ".3gp";
            String imageUrl = "postVideoThumbnails/" + discoverid + ".jpg";
            String posttypes = type.getText().toString().toLowerCase();

            final Map UDS_Details = new HashMap();
            UDS_Details.put("UDSD_discoverID",discoverid);
            UDS_Details.put("UDSD_type",posttypes);
            UDS_Details.put("UDSD_address", yourlocation);
            UDS_Details.put("UDSD_caption",yourcaption);
            UDS_Details.put("UDSD_date",ServerValue.TIMESTAMP);
            UDS_Details.put("UDSD_ownerID",current_user);
            UDS_Details.put("UDSD_mediaType","image");
            UDS_Details.put("UDSD_status",true);
            UDS_Details.put("UDSD_mediaURL",mediaURL);

            final Map UDS_Details_video = new HashMap();
            UDS_Details_video.put("UDSD_discoverID",discoverid);
            UDS_Details_video.put("UDSD_type",posttypes);
            UDS_Details_video.put("UDSD_address", yourlocation);
            UDS_Details_video.put("UDSD_caption",yourcaption);
            UDS_Details_video.put("UDSD_date",ServerValue.TIMESTAMP);
            UDS_Details_video.put("UDSD_mediaThumbnailURL", imageUrl);
            UDS_Details_video.put("UDSD_ownerID",current_user);
            UDS_Details_video.put("UDSD_mediaType","video");
            UDS_Details_video.put("UDSD_status",true);
            UDS_Details_video.put("UDSD_mediaURL",videoUrl);
//
            final Map UDS_Details_text = new HashMap();
            UDS_Details_text.put("UDSD_discoverID",discoverid);
            UDS_Details_text.put("UDSD_type",posttypes);
            UDS_Details_text.put("UDSD_address", yourlocation);
            UDS_Details_text.put("UDSD_caption",yourcaption);
            UDS_Details_text.put("UDSD_date",ServerValue.TIMESTAMP);
            UDS_Details_text.put("UDSD_ownerID",current_user);
            UDS_Details_text.put("UDSD_mediaType","noMedia");
            UDS_Details_text.put("UDSD_status",true);

            final Map UDS_Location = new HashMap();
            UDS_Location.put("UDSL_latitude",location1.getLatitude());
            UDS_Location.put("UDSL_longitude",location1.getLongitude());

            StorageReference videopath = discovervideo.child(discoverid + ".3gp");
            if(datatype.equals("image")){
                System.out.println("datatype:image1");
                StorageReference filepath = discoverimage.child(discoverid + ".jpg");
                UploadTask uploadTask = filepath.putBytes(getAvatar());
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        Toast.makeText(DiscoverSetup.this, "Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        userDiscover.child("UDS_List").child(discoverid).child("UDS_Details").setValue(UDS_Details);
                        if (posttypes.equals("public")) {
                            DatabaseReference publicdiscover = FirebaseDatabase.getInstance().getReference().child("PublicDiscover");
                            publicdiscover.child(discoverid).child("PD_ownerID").setValue(current_user);
                        }
                        userDiscover.child("UDS_List").child(discoverid).child("UDS_Location").setValue(UDS_Location).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                dialog.dismiss();
                                sendPostComplete();
                            }
                        });
                    }
                });
            }
            else if(datatype.equals("video")){

                System.out.println("datatype:video1");
                StorageReference filepath = discoverThumbnail.child(discoverid + ".jpg");
                UploadTask uploadTask = filepath.putBytes(getAvatar());
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        Toast.makeText(DiscoverSetup.this, "Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if(getVideoByte()!=null){
                            UploadTask uploadvideo = videopath.putFile(Uri.parse(getVideoByte()));
                            uploadvideo.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    userDiscover.child("UDS_List").child(discoverid).child("UDS_Details").setValue(UDS_Details_video);
                                    if(posttypes.equals("public")){
                                        DatabaseReference publicdiscover = FirebaseDatabase.getInstance().getReference().child("PublicDiscover");
                                        publicdiscover.child(discoverid).child("PD_ownerID").setValue(current_user);
                                    }
                                    userDiscover.child("UDS_List").child(discoverid).child("UDS_Location").setValue(UDS_Location).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            dialog.dismiss();
                                            sendPostComplete();
                                        }
                                    });
                                    dialog.dismiss();
                                }
                            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    double p = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                                    dialog.setMessage((int) p+" % Uploading...");
                                }
                            });
                        }
                        else{
                            Toast.makeText(DiscoverSetup.this,"nothing to upload",Toast.LENGTH_LONG).show();
                        }

                    }
                });
            }
            else{
                userDiscover.child("UDS_List").child(discoverid).child("UDS_Details").setValue(UDS_Details_text);
                if(posttypes.equals("public")){
                    DatabaseReference publicdiscover = FirebaseDatabase.getInstance().getReference().child("PublicDiscover");
                    publicdiscover.child(discoverid).child("PD_ownerID").setValue(current_user);
                }
                userDiscover.child("UDS_List").child(discoverid).child("UDS_Location").setValue(UDS_Location).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        sendPostComplete();
                    }
                });
            }
        }
        else if(role.equals("partner")){
            datatype = "images";
            if(datatype.equals("images")){
                DatabaseReference discoverpartner = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("userPartnerDetails");
                discoverpartner.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
//                                    ProgressDialog dialog= new ProgressDialog(getApplicationContext());
//                                    dialog.setMessage("Uploading new Post..");
//                                    dialog.show();
                            String partnerid = dataSnapshot.child("UPD_partnerID").getValue().toString();
                            String newpartnerpost = discoverpartner.push().getKey();
                            String mediaURL = "postImage/"+newpartnerpost+".jpg";
                            DatabaseReference partnerstree = FirebaseDatabase.getInstance().getReference().child("Partners").child(partnerid).child("P_partnerDiscover");
                            final Map UDS_Details = new HashMap();
                            UDS_Details.put("UDSD_discoverID",newpartnerpost);
                            UDS_Details.put("UDSD_type","partner");
                            UDS_Details.put("UDSD_address", yourlocation);
                            UDS_Details.put("UDSD_caption",yourcaption);
                            UDS_Details.put("UDSD_date",ServerValue.TIMESTAMP);
                            UDS_Details.put("UDSD_ownerID",partnerid);
                            UDS_Details.put("UDSD_mediaType","image");
                            UDS_Details.put("UDSD_status",true);
                            UDS_Details.put("UDSD_mediaURL",mediaURL);

                            final Map UDS_Location = new HashMap();
                            UDS_Location.put("UDSL_latitude",location1.getLatitude());
                            UDS_Location.put("UDSL_longitude",location1.getLongitude());

                            System.out.println("datatype:image1");
                            StorageReference filepath = discoverimage.child(newpartnerpost + ".jpg");
                            UploadTask uploadTask = filepath.putBytes(getAvatar());
                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    //         dialog.dismiss();
                                    Toast.makeText(DiscoverSetup.this, "Please try again later", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    partnerstree.child(newpartnerpost).child("UDS_Details").setValue(UDS_Details);
                                    partnerstree.child(newpartnerpost).child("UDS_Location").setValue(UDS_Location).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            //                dialog.dismiss();
                                            sendPostComplete();
                                        }
                                    });
                                }
                            });
                        }
                        else{ Toast.makeText(DiscoverSetup.this, "Sorry, you dont have partners yet.", Toast.LENGTH_SHORT).show(); }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
            else{
                Toast.makeText(DiscoverSetup.this, "Partners only allowed to post image at moment", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place;
                place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getLatLng());
                loc.setText(place.getAddress());

                LatLng queriedloc = place.getLatLng();
                location1 = new location(queriedloc.latitude,queriedloc.longitude);

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(this.getIntent());
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    datatype = "image";
                    imageStream = getApplicationContext().getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
                   // userprofileimage.setVisibility(View.VISIBLE);
                    photo.setVisibility(View.GONE);
                    video.setVisibility(View.GONE);
                    photo_p_holder.setVisibility(View.VISIBLE);
                    photo_p_holder.setImageBitmap(selectedImage);
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    setAvatar(datas);

                    Intent i = new Intent(this, filterActivitity.class);
                    i.putExtra("Image",datas);
                    startActivityForResult(i,100);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == 100){
            if(resultCode == Activity.RESULT_OK){
                datatype = "image";
                byte[] bytes = data.getByteArrayExtra("PUBLIC_STRING_IDENTIFIER");
                Bitmap b = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                setAvatar(bytes);
                photo.setVisibility(View.GONE);
                video.setVisibility(View.GONE);
                photo_p_holder.setImageBitmap(b);
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(this,data);
            datatype = "image";
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(this);
        }
        else if (requestCode == GALLERY) {
            Log.d("what","gale");
            datatype = "video";
            photo.setVisibility(View.GONE);
            video.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
//            thumbV.setVisibility(View.VISIBLE);
            if (data != null) {
                Uri contentURI = data.getData();
                String selectedVideoPath = getPath(getApplicationContext(),contentURI);
                Log.d("path",selectedVideoPath);
                saveVideoToInternalStorage(selectedVideoPath);
                videoView.setVideoURI(contentURI);
                videoView.requestFocus();
                videoView.start();

                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(selectedVideoPath,
                        MediaStore.Images.Thumbnails.MINI_KIND);
                thumbV.setImageBitmap(thumb);
                thumbV.setDrawingCacheEnabled(true);
                thumbV.buildDrawingCache();
                Bitmap bitmap = ((BitmapDrawable) thumbV.getDrawable()).getBitmap();
                System.out.println("isi bitmap:"+bitmap);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos);
                byte[] datasii = baos.toByteArray();
                setAvatar(datasii);
//                    byte[] videoBytes = null;

                setVideoByte(String.valueOf(contentURI));
                System.out.println("isi:"+getVideoByte()+"isi:"+getAvatar());


            }
        } else if (requestCode == CAMERA) {
//                video.setVisibility(View.GONE);
            datatype = "video";
            photo.setVisibility(View.GONE);
            video.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
//            photo_p_holder.setVisibility(View.VISIBLE);
            System.out.println("datatype:"+datatype);
            Uri contentURI = data.getData();
            String recordedVideoPath = getPath(getApplicationContext(),contentURI);
            Log.d("frrr",recordedVideoPath);
            saveVideoToInternalStorage(recordedVideoPath);
            videoView.setVideoURI(contentURI);
            videoView.requestFocus();
            videoView.start();
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(recordedVideoPath,
                    MediaStore.Images.Thumbnails.MINI_KIND);
            thumbV.setImageBitmap(thumb);
            thumbV.setDrawingCacheEnabled(true);
            thumbV.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) thumbV.getDrawable()).getBitmap();
            System.out.println("isi bitmap:"+bitmap);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos);
            byte[] datasii = baos.toByteArray();
            setAvatar(datasii);

            setVideoByte(String.valueOf(contentURI));
            System.out.println("isi:"+getVideoByte()+"isi:"+getAvatar());
        }
    }

    private void saveVideoToInternalStorage (String filePath) {
        File newfile;

        try {
            File currentFile = new File(filePath);
            File wallpaperDirectory = new File(String.valueOf(Environment.getExternalStorageDirectory()));
            newfile = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".mp4");

            if (!wallpaperDirectory.exists()) {
                wallpaperDirectory.mkdirs();
            }
            if(currentFile.exists()){
                InputStream in = new FileInputStream(currentFile);
                OutputStream out = new FileOutputStream(newfile);
                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                Log.v("vii", "Video file saved successfully.");
            }else{
                Log.v("vii", "Video saving failed. Source file missing.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getPath( Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Video.Media.DATA};
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }

//    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {
//
//        Context mContext;
//
//        public VideoCompressAsyncTask(Context context){
//            mContext = context;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
////            imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_photo_camera_white_48px));
//            compressionMsg.setVisibility(View.VISIBLE);
////            picDescription.setVisibility(View.GONE);
//        }
//
//        @Override
//        protected String doInBackground(String... paths) {
//            String filePath = null;
//            try {
//
//                filePath = KplCompressor.with(mContext).compressVideo(paths[0], paths[1]);
//
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//            }
//            return  filePath;
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String compressedFilePath) {
//            super.onPostExecute(compressedFilePath);
//            File imageFile = new File(compressedFilePath);
//            float length = imageFile.length() / 1024f; // Size in KB
//            String value;
//            if(length >= 1024)
//                value = length/1024f+" MB";
//            else
//                value = length+" KB";
//            String text = String.format(Locale.US, "%s\nName: %s\nSize: %s", getString(R.string.video_complete), imageFile.getName(), value);
//            compressionMsg.setVisibility(View.GONE);
////            picDescription.setVisibility(View.VISIBLE);
////            picDescription.setText(text);
//            Log.i("Silicompressor", "Path: "+compressedFilePath);
//        }
//    }

//    public String getPath(Uri uri) {
//        String[] projection = { MediaStore.Video.Media.DATA };
//        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//        if (cursor != null) {
//            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
//            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
//            int column_index = cursor
//                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } else
//            return null;
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(DiscoverSetup.this);
        }
    }

    public void setAnimation() {
        if (Build.VERSION.SDK_INT > 20) {
            Slide slide = new Slide();
            slide.setSlideEdge(Gravity.BOTTOM);
            slide.setDuration(300);
            slide.setInterpolator(new DecelerateInterpolator());
            getWindow().setExitTransition(slide);
            getWindow().setEnterTransition(slide);
        }
    }

    public byte[] getAvatar() { return avatar; }
    public void setAvatar(byte[] avatar) { this.avatar = avatar; }

    private void setVideoByte(String videoByte) { this.videoByte = videoByte; }
    public String getVideoByte() { return videoByte; }
}