package com.app.pingchat.Discover;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Chat.ChatListAdapter;
import com.app.pingchat.Chat.ChatListClass;
import com.app.pingchat.CommentLayoutActivity;
import com.app.pingchat.Friend.Friends;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.PlayVideoActivity;
import com.app.pingchat.Profile.ViewUserProfileActivity;
import com.app.pingchat.R;
import com.app.pingchat.UserAuth.Register2Activity;
import com.app.pingchat.bottomSheetShowComment;
import com.app.pingchat.bottomSheetViewImage;
import com.app.pingchat.filterActivitity;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.SimpleTimeZone;


public class DiscoverAdapter2 extends RecyclerView.Adapter<DiscoverAdapter2.MyViewHolder> implements bottomSheetShowComment.BottomSheetListener, Filterable {
    FirebaseStorage storage;
    StorageReference storeRef, discoverimage;
    DatabaseReference metadata, discoverclap, usersRef, pDiscover;
    Context context;
    ArrayList<UDS_DetailsClass> alldiscoverarraylist2;
    ArrayList<UDS_DetailsClass> alldiscoverarraylist2Full;
    MyFilter discoverFilter;
    private FirebaseAuth mAuths;
    boolean isTextViewClicked = false;
    private String aftertrimmed;
    private String currentuser;
    public DatabaseReference discover, discoverSession, user;
    ArrayList list_of_discover;
    private RequestQueue mQueue;
    Boolean clapChecker = false;
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public DiscoverAdapter2(ArrayList<UDS_DetailsClass> Discoverlist) {
        this.alldiscoverarraylist2 = Discoverlist;
//        alldiscoverarraylist2Full = new ArrayList<>(Discoverlist);

    }

    public DiscoverAdapter2(Context c, ArrayList<UDS_DetailsClass> d, String y) {
        context = c;
        alldiscoverarraylist2 = d;
        alldiscoverarraylist2Full = new ArrayList<UDS_DetailsClass>();
        currentuser = y;
    }

//    public void filterList(ArrayList<UDS_DetailsClass> filteredlist) {
//        alldiscoverarraylist2= filteredlist;
//        notifyDataSetChanged();
//    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.discover_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final UDS_DetailsClass newdiscover = alldiscoverarraylist2.get(position);
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        discoverimage = FirebaseStorage.getInstance().getReference().child("postImage");
        mAuths = FirebaseAuth.getInstance();
        currentuser = mAuths.getCurrentUser().getUid();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        pDiscover = FirebaseDatabase.getInstance().getReference().child("PublicDiscover");
        DatabaseReference userdetails = FirebaseDatabase.getInstance().getReference().child("Users");


        mQueue = Volley.newRequestQueue(context);

//        System.out.println("ownerid dlm discover adapter 2" + newdiscover.getUDSD_caption());

        holder.status.setText(newdiscover.getUDSD_caption());
        holder.status1.setText(newdiscover.getUDSD_caption());
        holder.loc.setText(newdiscover.getUDSD_address());
        holder.date.setText(newdiscover.getDate());
        holder.onClick(newdiscover.getUDSD_discoverID(), alldiscoverarraylist2.get(position), newdiscover.getUDSD_mediaURL());

        bottomSheetShowComment bottomSheetShowComment;
        bottomSheetShowComment = new bottomSheetShowComment();
        bottomSheetShowComment.setListener(this);

        userdetails.child(newdiscover.getUDSD_ownerID()).child("userDiscovers").child("UDS_List").child(newdiscover.getUDSD_discoverID()).child("UDS_Comments").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    int total = 0;
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        String key = dataSnapshot1.getKey();
//                        System.out.println("key: " + key);
                        total = total + 1;
                    }
                    holder.numbercomment.setText(String.valueOf(total));
                } else {
                    holder.numbercomment.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        holder.user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newdiscover.getUDSD_ownerID().equals(currentuser)){
//                    ViewProfileActivity newFragment = new ViewProfileActivity();
                    Intent i = new Intent(context, MainActivity.class);
                    i.putExtra("id", "profile");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }else{
                    Intent i = new Intent(context, ViewUserProfileActivity.class);
                    i.putExtra("id", newdiscover.getUDSD_ownerID());
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
            }
        });



//        if(clapChecker==true){
//            holder.unclap.setVisibility(View.VISIBLE);
//            holder.clap.setVisibility(View.INVISIBLE);
//        }
//        else{
//            holder.unclap.setVisibility(View.INVISIBLE);
//            holder.clap.setVisibility(View.VISIBLE);
//        }




        if(newdiscover.getUDSD_type().equals("partner")){

            DatabaseReference discoverpartner = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userPartnerDetails");
            discoverpartner.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        String UPD_partnerID = dataSnapshot.child("UPD_partnerID").getValue().toString();
                        DatabaseReference partner = FirebaseDatabase.getInstance().getReference().child("Partners").child(UPD_partnerID);
                        partner.child("P_partnerDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String p_name = dataSnapshot.child("P_partnerName").getValue().toString();
                                    String p_avatar = dataSnapshot.child("P_profileURL").getValue().toString();
                                    holder.user.setText(p_name);
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(p_avatar);
                                    GlideApp.with(context.getApplicationContext())
                                        .asBitmap()
                                        .load(storageReference)
                                        .placeholder(R.drawable.loading_img)
                                        .into(new SimpleTarget<Bitmap>(100,100) {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                holder.userimage.setImageBitmap(resource);
                                            }
                                        });
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }
                    else{}
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
        }
        else{
            userdetails.child(newdiscover.getUDSD_ownerID()).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild("UD_displayName")) {
                        holder.user.setText(dataSnapshot.child("UD_displayName").getValue().toString());
                    }

                    if (dataSnapshot.hasChild("UD_avatarURL")) {
                        String avatarurl = dataSnapshot.child("UD_avatarURL").getValue().toString();
                        if (!avatarurl.startsWith("h")) {

                            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatarurl);
                            GlideApp.with(context.getApplicationContext())
                                    .asBitmap()
                                    .load(storageReference)
                                    .placeholder(R.drawable.loading_img)
                                    .into(new SimpleTarget<Bitmap>(100,100) {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            holder.userimage.setImageBitmap(resource);
                                        }
                                    });
                        } else {
                            CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
                            circularProgressDrawable.setStrokeWidth(5f);
                            circularProgressDrawable.setCenterRadius(10f);
                            circularProgressDrawable.start();
                            GlideApp.with(context.getApplicationContext())
                                    .asBitmap()
                                    .load(avatarurl)
                                    .placeholder(R.drawable.loading_img)
                                    .into(new SimpleTarget<Bitmap>(100,100) {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            holder.userimage.setImageBitmap(resource);
                                        }
                                    });
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            holder.clapStatus(newdiscover.getUDSD_discoverID(), newdiscover.getUDSD_ownerID());

            holder.unclap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    clapChecker = true;
                    usersRef.child(newdiscover.getUDSD_ownerID()).child("userDiscovers").child("UDS_List").child(newdiscover.getUDSD_discoverID()).child("UDS_Claps").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (clapChecker.equals(true)) {
                                if (dataSnapshot.hasChild(currentuser)) {
//                                System.out.println("re-clap");
//                                holder.unclap.setVisibility(View.VISIBLE);
//                                holder.clap.setVisibility(View.INVISIBLE);
                                    usersRef.child(newdiscover.getUDSD_ownerID()).child("userDiscovers").child("UDS_List").child(newdiscover.getUDSD_discoverID()).child("UDS_Claps").child(currentuser).removeValue();
                                    clapChecker = false;

                                } else {
//                                System.out.println("new clap");
//                                holder.unclap.setVisibility(View.INVISIBLE);
//                                holder.clap.setVisibility(View.VISIBLE);
                                    final Map UDSclaps = new HashMap();
                                    UDSclaps.put("UDSC_status", false);
                                    UDSclaps.put("UDSC_timestamp", timestamp.getTime());
                                    usersRef.child(newdiscover.getUDSD_ownerID()).child("userDiscovers").child("UDS_List").child(newdiscover.getUDSD_discoverID()).child("UDS_Claps").child(currentuser).setValue(UDSclaps);
                                    clapChecker = false;
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }
            });

            holder.comments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(context, CommentLayoutActivity.class);
                    i.putExtra("discoverpostid", newdiscover.getUDSD_discoverID());
                    i.putExtra("currentuser", currentuser);
                    i.putExtra("ownerid", newdiscover.getUDSD_ownerID());
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);

//                bottomSheetViewImage.setListener(this);
//                user.child(currentuser).child("metaData").addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//                    }
//                });
                }
            });
        }

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(10f);
        circularProgressDrawable.setCenterRadius(50f);
        circularProgressDrawable.start();

//        System.out.println("mediatype: " + newdiscover.getUDSD_mediaType());
//        System.out.println("caption :" + newdiscover.getUDSD_caption());

        if (newdiscover.getUDSD_mediaType().equals("noMedia")) {
            holder.image.setVisibility(View.GONE);
            holder.status.setVisibility(View.GONE);
            holder.moreless1.setVisibility(View.GONE);
            holder.line1.setVisibility(View.VISIBLE);
            holder.status1.setVisibility(View.VISIBLE);
            holder.moreless.setVisibility(View.VISIBLE);
//            holder.gap.setVisibility(View.VISIBLE);

        } else if (newdiscover.getUDSD_mediaType().equals("image")) {
//            if(newdiscover.getUDSD_mediaURL()!=null){
            holder.image.setVisibility(View.VISIBLE);
            holder.status.setVisibility(View.VISIBLE);
            holder.moreless1.setVisibility(View.VISIBLE);
            holder.line1.setVisibility(View.GONE);
            holder.status1.setVisibility(View.GONE);
            holder.moreless.setVisibility(View.GONE);
//            holder.gap.setVisibility(View.GONE);
//            System.out.println("masuk sini newdiscover.getPostImageURL()!=");
            StorageReference storageReference1 = FirebaseStorage.getInstance().getReference().child(newdiscover.getUDSD_mediaURL());
            GlideApp.with(context.getApplicationContext())
                    .asBitmap()
                    .load(storageReference1)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(circularProgressDrawable)
                    .into(new SimpleTarget<Bitmap>(400,400) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            holder.image.setImageBitmap(resource);
                        }
                    });
            //    }
//            else if(newdiscover.getUDSD_mediaURL().equals("")){
//                holder.image.setVisibility(View.GONE);
//            }
//            else{
//                holder.image.setVisibility(View.GONE);
//            }
        }else if(newdiscover.getUDSD_mediaType().equals("video")){
            holder.image.setVisibility(View.INVISIBLE);
            holder.image1.setVisibility(View.VISIBLE);
            holder.videoRl.setVisibility(View.VISIBLE);
            holder.status.setVisibility(View.VISIBLE);
            holder.moreless1.setVisibility(View.VISIBLE);
            holder.line1.setVisibility(View.GONE);
            holder.status1.setVisibility(View.GONE);
            holder.moreless.setVisibility(View.GONE);
//            holder.gap.setVisibility(View.GONE);
//            System.out.println("masuk sini newdiscover.getPostImageURL()!=");
            StorageReference storageReference1 = FirebaseStorage.getInstance().getReference().child(newdiscover.getUDSD_mediaThumbnailURL());
            GlideApp.with(context.getApplicationContext())
                    .asBitmap()
                    .load(storageReference1)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(circularProgressDrawable)
                    .into(new SimpleTarget<Bitmap>(400,400) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            holder.image1.setImageBitmap(resource);
                        }
                    });

            holder.playbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent i = new Intent(context, PlayVideoActivity.class);
//                    i.putExtra("test",newdiscover.getUDSD_mediaURL());
//                    context.startActivity(i);
                    holder.videoView.setVisibility(View.VISIBLE);
                    holder.image1.setVisibility(View.INVISIBLE);
                    if(!holder.videoView.isPlaying()){
                        holder.videoView.start();
                        holder.playbtn.setImageResource(R.drawable.pausewhite);
                    }else{
//                        holder.videoView.setVisibility(View.INVISIBLE);
//                        holder.image1.setVisibility(View.VISIBLE);
                        holder.videoView.pause();
                        holder.playbtn.setImageResource(R.drawable.play_icon1);
                    }
                }
            });
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(newdiscover.getUDSD_mediaURL());
            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    holder.progressBar.setVisibility(View.GONE);
                    if(!holder.videoView.isPlaying()){
                        holder.videoView.setVideoURI(uri);
                        holder.videoView.setZOrderMediaOverlay(true);
//                        holder.videoView.setZOrderOnTop(true);
                        holder.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                holder.playbtn.setImageResource(R.drawable.play_icon1);
                            }
                        });

                    }else{
                        holder.videoView.pause();
//                        holder.playbtn.setImageResource(R.drawable.pausewhite);
                    }
                    holder.videoView.requestFocus();
                    holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            mediaPlayer.setLooping(true);
                            holder.videoView.setZOrderOnTop(false);
                            holder.videoView.setBackgroundColor(Color.TRANSPARENT);
                            holder.progressBar.setVisibility(View.INVISIBLE);
//                            holder.videoView.setVisibility(View.INVISIBLE);
//                            holder.image1.setVisibility(View.GONE);
//                            holder.videoView.start();
//                            holder.playbtn.setImageResource(R.drawable.pausewhite);
                        }
                    });
                }
            });

            holder.videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                    if(i == mediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                        holder.image1.setVisibility(View.GONE);
                        holder.progressBar.setVisibility(View.GONE);
                        return true;
                    }
                    else if(i==mediaPlayer.MEDIA_INFO_BUFFERING_START){
                        holder.progressBar.setVisibility(View.VISIBLE);
                    }
                    else if( i== mediaPlayer.MEDIA_INFO_BUFFERING_END){
                        holder.progressBar.setVisibility(View.GONE);
                    }
                    return false;
                }
            });
        }
        else {
            holder.image.setVisibility(View.GONE);
            holder.videoRl.setVisibility(View.GONE);
            holder.status.setVisibility(View.GONE);
            holder.moreless1.setVisibility(View.GONE);
//            holder.gap.setVisibility(View.VISIBLE);
            holder.line1.setVisibility(View.VISIBLE);
            holder.status1.setVisibility(View.VISIBLE);
            holder.moreless.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return alldiscoverarraylist2.size();
    }

    @Override
    public void onClickComment(String text) {

    }

    //    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        discoverArraylist.clear();
//        if (charText.length() == 0) {
//            discoverArraylist.addAll(discoverArraylist);
//        } else {
//            for (UDS_DetailsClass wp : discoverArraylist) {
//                if (wp.getUDSD_caption().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    discoverArraylist.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView status, loc, date, user, more, less, numbercomment, numberclap,status1, more1,less1,clap_text;
        ImageView image, image1, userimage, clap, comments, unclap;
        ImageButton menuBtn, menu,playbtn;
        RelativeLayout moreless, moreless1,videoRl;
        VideoView videoView;
        View line1;
        Button add;
        int countClaps;
        ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status3);
            image = itemView.findViewById(R.id.picture);
            image1 = itemView.findViewById(R.id.picture1);
            videoRl = itemView.findViewById(R.id.rl18);
            videoView = itemView.findViewById(R.id.video);
            playbtn = itemView.findViewById(R.id.playvideo);
            loc = itemView.findViewById(R.id.loc);
            date = itemView.findViewById(R.id.date);
            user = itemView.findViewById(R.id.user);
            more = itemView.findViewById(R.id.moree);
            less = itemView.findViewById(R.id.lesss);
            userimage = itemView.findViewById(R.id.user_avatar_bg);
            menu = itemView.findViewById(R.id.triple_btn);
            progressBar = itemView.findViewById(R.id.progrss);
//            clap = itemView.findViewById(R.id.clap1);
            comments = itemView.findViewById(R.id.comments);
            numberclap = itemView.findViewById(R.id.numberclap);
            numbercomment = itemView.findViewById(R.id.numbercomment);
            unclap = itemView.findViewById(R.id.clap);
            line1 = itemView.findViewById(R.id.line1);
            status1 = itemView.findViewById(R.id.status2);
            more1 = itemView.findViewById(R.id.moree1);
            less1 = itemView.findViewById(R.id.lesss1);
            moreless = itemView.findViewById(R.id.moreless);
            moreless1 = itemView.findViewById(R.id.moreless1);
            clap_text = itemView.findViewById(R.id.clap_count_txt);
//            gap = itemView.findViewById(R.id.gap_caption);

            status.post(new Runnable() {
                @Override
                public void run() {
//            status.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//
//                @Override
//                public boolean onPreDraw() {
//                    // Remove listener because we don't want this called before _every_ frame
//                    status.getViewTreeObserver().removeOnPreDrawListener(this);
//                    // Drawing happens after layout so we can assume getLineCount() returns the correct value
//                    if(status.getLineCount() > 2) {
                    status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isTextViewClicked) {
                                //This will shrink textview to 2 lines if it is expanded.
                                status.setMaxLines(1);
                                isTextViewClicked = false;
//                                    System.out.println("false");
                                less.setVisibility(View.GONE);
                            } else {
                                //This will expand the textview if it is of 2 lines
                                status.setMaxLines(Integer.MAX_VALUE);
                                isTextViewClicked = true;
//                                    System.out.println("true");
                                less.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    less.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            less.setVisibility(View.GONE);
//                                System.out.println("less");
                            status.setMaxLines(1);
                        }
                    });
//                    else{
//                        less1.setVisibility(View.GONE);
////                        status1.setMaxLines(1);
//                    }
//                    return true; // true because we don't want to skip this frame
                }
            });


            status1.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    // Remove listener because we don't want this called before _every_ frame
                    status1.getViewTreeObserver().removeOnPreDrawListener(this);
                    // Drawing happens after layout so we can assume getLineCount() returns the correct value
                    // Do whatever you want in case text view has more than 2 lines
                    status1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isTextViewClicked) {
                                //This will shrink textview to 2 lines if it is expanded.
                                status1.setMaxLines(1);
                                isTextViewClicked = false;
                                less1.setVisibility(View.GONE);
                            } else {
                                //This will expand the textview if it is of 2 lines
                                status1.setMaxLines(Integer.MAX_VALUE);
                                isTextViewClicked = true;
                                less1.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    less1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            less1.setVisibility(View.GONE);
//                                System.out.println("less");
                            status1.setMaxLines(1);
                        }
                    });
                    return true; // true because we don't want to skip this frame
                }
            });

//            status1.post(new Runnable() {
//                @Override
//                public void run() {
//                        status1.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                if (isTextViewClicked) {
//                                    //This will shrink textview to 2 lines if it is expanded.
//                                    status1.setMaxLines(3);
//                                    isTextViewClicked = false;
//                                    less1.setVisibility(View.GONE);
//                                } else {
//                                    //This will expand the textview if it is of 2 lines
//                                    status1.setMaxLines(Integer.MAX_VALUE);
//                                    isTextViewClicked = true;
//                                    less1.setVisibility(View.VISIBLE);
//                                }
//                            }
//                        });
//                        less1.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                less1.setVisibility(View.GONE);
////                                System.out.println("less");
//                                status1.setMaxLines(3);
//                            }
//                        });
//                }
//            });



        }

        private void showdialog(String discoverid) {
            AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
            String[] pictureDialogItems = {"Edit", "Delete"};
            pictureDialog.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
//                            System.out.println("edit");
                            break;
                        case 1:
                            //System.out.println(discoverid);
                            break;
                    }
                }
            });
            pictureDialog.show();
        }

        public void onClick(final String discoverKey, UDS_DetailsClass position, String newdiscoverurl) {
            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    usersRef.child(currentuser).child("userDiscovers").child("UDS_List").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                boolean value = false;
                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                    String discoveruserkey = dataSnapshot1.getKey();
                                    if (discoveruserkey.equals(discoverKey)) {
                                        value = true;
                                        CharSequence option[] = new CharSequence[]
                                                {
                                                        "Edit",
                                                        "Share",
                                                        "Delete",
                                                        "Cancel"
                                                };
                                        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(menu.getContext());
                                        builder.setTitle("Menu");
                                        builder.setItems(option, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (i == 0) {
//                                                    System.out.println("edit");
                                                    usersRef.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            if (dataSnapshot.exists()) {
                                                                String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
                                                                String caption = dataSnapshot.child("UDSD_caption").getValue().toString();

                                                                Intent intent = new Intent(context, editDiscoverActivity.class);
                                                                intent.putExtra("mediaType", mediaType);
                                                                intent.putExtra("caption", caption);
                                                                intent.putExtra("discoverKey", discoverKey);
                                                                intent.putExtra("discoverUrl", newdiscoverurl);
                                                                context.startActivity(intent);
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });
                                                } else if (i == 1) {
//                                                    System.out.println("share");
                                                    usersRef.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            if (dataSnapshot.exists()) {
                                                                String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
                                                                String caption = dataSnapshot.child("UDSD_caption").getValue().toString();
//                                                                    String mediaUrl = dataSnapshot.child("UDSD_mediaURL").getValue().toString();
                                                                if (mediaType.equals("image")) {
                                                                    discoverimage.child(discoverKey + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                                        @Override
                                                                        public void onSuccess(Uri uri) {
//                                                                            System.out.println("share image");
                                                                            Toast.makeText(context, "In development mode", Toast.LENGTH_SHORT).show();
//                                                                                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
//                                                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                                                                intent.putExtra(Intent.EXTRA_STREAM, uri);
//                                                                                intent.setType("image/png");
//                                                                                context.startActivity(Intent.createChooser(intent, "Share image via"));
                                                                        }
                                                                    });
                                                                } else {
                                                                    Toast.makeText(context, "In development mode", Toast.LENGTH_SHORT).show();
//                                                                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                                                                        sharingIntent.setType("text/plain");
//                                                                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, caption);
//                                                                        context.startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
                                                                }
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });
//
                                                } else if (i == 2) {
                                                    ProgressDialog dialog = new ProgressDialog(context);
                                                    dialog.setMessage("Delete on progress\nPlease wait");
                                                    dialog.show();
//                                                    System.out.println("delete");
                                                    usersRef.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").child("UDSD_status").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            dialog.dismiss();
                                                            Toast.makeText(context, "Post deleted!...", Toast.LENGTH_SHORT).show();
                                                            alldiscoverarraylist2.remove(position);
                                                            notifyDataSetChanged();
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                        builder.show();
                                        break;
                                    }
                                }
                                if (!value) {
//                                    System.out.println("Bukan post dia!");
                                    CharSequence option[] = new CharSequence[]
                                            {
                                                    "Share",
                                                    "Cancel"
                                            };
                                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(menu.getContext());
                                    builder.setTitle("Menu");
                                    builder.setItems(option, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (i == 0) {
//                                                System.out.println("share");
                                                pDiscover.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot.exists()) {
                                                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                                                String publicdiscoverkey = dataSnapshot1.getKey();
                                                                if (publicdiscoverkey.equals(discoverKey)) {
                                                                    if (dataSnapshot1.hasChild("PD_ownerID")) {
                                                                        String pdownerid = dataSnapshot1.child("PD_ownerID").getValue().toString();
//                                                                        System.out.println("ownerId:" + pdownerid);
//                                                                        System.out.println("publicKey:" + publicdiscoverkey);
                                                                        usersRef.child(pdownerid).child("userDiscovers").child("UDS_List").child(publicdiscoverkey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                            @Override
                                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                                if (dataSnapshot.exists()) {
                                                                                    String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
                                                                                    String caption = dataSnapshot.child("UDSD_caption").getValue().toString();
                                                                                    if (mediaType.equals("image")) {
                                                                                        discoverimage.child(publicdiscoverkey + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                                                            @Override
                                                                                            public void onSuccess(Uri uri) {
                                                                                                // Got the download URL for 'users/me/profile.png'
//                                                                                                System.out.println("share image");
//                                                                                                System.out.println("uri:" + uri);
//                                                                                                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
//                                                                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                                                                                intent.putExtra(Intent.EXTRA_STREAM, uri);
//                                                                                                intent.setType("image/png");
//                                                                                                context.startActivity(Intent.createChooser(intent, "Share image via"));
                                                                                                Toast.makeText(context, "In development mode", Toast.LENGTH_SHORT).show();
                                                                                            }
                                                                                        }).addOnFailureListener(new OnFailureListener() {
                                                                                            @Override
                                                                                            public void onFailure(@NonNull Exception exception) {
                                                                                                // Handle any errors
                                                                                            }
                                                                                        });

                                                                                    } else {
                                                                                        Toast.makeText(context, "In development mode", Toast.LENGTH_SHORT).show();
//                                                                                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                                                                                        sharingIntent.setType("text/plain");
//                                                                                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, caption);
//                                                                                        context.startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
                                                                                    }
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });
                                    builder.show();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            });
        }

        public void clapStatus(final String discoverKey, String ownerId) {

            usersRef.child(ownerId).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Claps").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()) {
//                        numberclap.setVisibility(View.VISIBLE);
                        clap_text.setVisibility(View.VISIBLE);
                        if (dataSnapshot.hasChild(currentuser)) {
//                            System.out.println("ada:clap");
                            countClaps = (int) dataSnapshot.getChildrenCount();
                            unclap.setImageResource(R.drawable.clap2);
//                            numberclap.setText(Integer.toString(countClaps));
                            clap_text.setText(Integer.toString(countClaps) + "\tpeople clap this post");

                        }else{
//                            System.out.println("tiada:clap");
                            countClaps = (int) dataSnapshot.getChildrenCount();
                            unclap.setImageResource(R.drawable.clap1);
//                            numberclap.setText(Integer.toString(countClaps));
                            clap_text.setText(Integer.toString(countClaps) + "\tpeople clap this post");
                        }
                    }else{
                        numberclap.setVisibility(View.GONE);
                        clap_text.setVisibility(View.GONE);
                        unclap.setImageResource(R.drawable.clap1);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
    }

    private String gettrimming(String discoverid) {
        String yuyu = discoverid.replace("postImage/", "");
        String y1 = yuyu.substring(0, yuyu.indexOf('.'));
//        System.out.println("lepas trim " + y1);
        aftertrimmed = y1;
        return aftertrimmed;
    }

    public void removeItem(final View v) {
        Animation.AnimationListener al = new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                notifyDataSetChanged();
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        };
        collapse(v, al);
    }

    private void collapse(final View v, Animation.AnimationListener animation) {

        final int initialHeight = v.getHeight();
        Animation anim = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        if (animation != null) {
            anim.setAnimationListener(animation);
        }
        anim.setDuration(250);
        v.startAnimation(anim);
    }

    @Override
    public Filter getFilter() {
        if (discoverFilter == null) {
            alldiscoverarraylist2Full.clear();
            alldiscoverarraylist2Full.addAll(this.alldiscoverarraylist2);
            discoverFilter = new DiscoverAdapter2.MyFilter(this, alldiscoverarraylist2Full);
        }
        return discoverFilter;
    }

    private static class MyFilter extends Filter {

        private final DiscoverAdapter2 discoverAdapter2;
        private final ArrayList<UDS_DetailsClass> alldiscoverarraylistFull; //filter
        private final ArrayList<UDS_DetailsClass> alldiscoverarraylist;
        Context context;

        private MyFilter(DiscoverAdapter2 myAdapter, ArrayList<UDS_DetailsClass> alldiscoverarraylist) {
            this.discoverAdapter2 = myAdapter;
            this.alldiscoverarraylist = alldiscoverarraylist;
            this.alldiscoverarraylistFull = new ArrayList<UDS_DetailsClass>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            alldiscoverarraylistFull.clear();
            final FilterResults results = new FilterResults();
            if (charSequence.length() == 0) {
//                Toast.makeText(context, "No match found", Toast.LENGTH_LONG).show();
                alldiscoverarraylistFull.addAll(alldiscoverarraylist);
            } else {
                final String filterPattern = charSequence.toString().toLowerCase().trim();
                for (UDS_DetailsClass detailsClass : alldiscoverarraylist) {
                    if (detailsClass.getUDSD_caption().toLowerCase().contains(filterPattern) || detailsClass.getUDSD_address().toLowerCase().contains(filterPattern)) {
                        alldiscoverarraylistFull.add(detailsClass);
                    }

                }
            }

            results.values = alldiscoverarraylistFull;
            results.count = alldiscoverarraylistFull.size();
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            discoverAdapter2.alldiscoverarraylist2.clear();
            discoverAdapter2.alldiscoverarraylist2.addAll((ArrayList<UDS_DetailsClass>) filterResults.values);
            discoverAdapter2.notifyDataSetChanged();

        }
    }


//    @Override
//    public Filter getFilter() {
//        return discoverFilter;
//    }
//
//    private Filter discoverFilter = new Filter() {
//        @Override
//        protected FilterResults performFiltering(CharSequence charSequence) {
//            ArrayList<UDS_DetailsClass>filteredList = new ArrayList<>();
//
//            if(charSequence == null || charSequence.length() == 0){
//                filteredList.addAll(alldiscoverarraylist2Full);
//            }else {
//                String filterPattern = charSequence.toString().toLowerCase().trim();
//
//                for(UDS_DetailsClass detailsClass : alldiscoverarraylist2Full){
//                    if(detailsClass.getUDSD_caption().toLowerCase().contains(filterPattern)){
//                        filteredList.add(detailsClass);
//                    }
//                }
//            }
//            FilterResults results = new FilterResults();
//            results.values = filteredList;
//            System.out.println("isi:"+results.values);
//
//            return results;
//        }
//
//        @Override
//        protected void publishResults(CharSequence charSequence, FilterResults results) {
//            alldiscoverarraylist2.clear();
//            alldiscoverarraylist2.addAll((ArrayList) results.values);
//
//            notifyDataSetChanged();
//        }
//    };

}