package com.app.pingchat.Discover;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.app.pingchat.R;
import com.app.pingchat.WebActivity;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.w3c.dom.Text;

public class bottomSheetNewsDetails extends BottomSheetDialogFragment implements View.OnClickListener {
    private BottomSheetListener mListener;
    private TextView desc, dismiss;
    private Button websitebtn;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_news_details, container, false);

        String title = getArguments().getString("title");
        String newsUrl = getArguments().getString("newsurl");
        String description = getArguments().getString("description");
        desc = v.findViewById(R.id.news_details);
        dismiss = v.findViewById(R.id.dismiss);

        websitebtn = v.findViewById(R.id.go_to_website);
        websitebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), WebActivity.class);
                intent.putExtra("key","news");
                intent.putExtra("url",newsUrl);
                intent.putExtra("title",title);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        desc.setText(description);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return  v;
    }


    @Override
    public void onClick(View v) {

    }

    public void setListener(NewsShowActivity newsShowActivity) {
    }

    public interface BottomSheetListener {
    }
}
