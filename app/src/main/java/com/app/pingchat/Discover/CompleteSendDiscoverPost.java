package com.app.pingchat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Main.MainActivity;

public class CompleteSendDiscoverPost extends AppCompatActivity {

    Button todiscover;

    // int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complete_send_post_layout);
        todiscover = findViewById(R.id.gotodisc);
        todiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("completediscover","ada");
                startActivity(intent);
                finish();
            }
        });
    }

}
