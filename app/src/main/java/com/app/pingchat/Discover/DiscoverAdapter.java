package com.app.pingchat.Discover;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.aware.DiscoverySession;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.CommentLayoutActivity;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.jackandphantom.blurimage.BlurImage;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class DiscoverAdapter extends RecyclerView.Adapter<DiscoverAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference metadata,discoverclap;
    Context context;
    ArrayList<Discovers> discoverArraylist;
    private FirebaseAuth mAuths;
    boolean isTextViewClicked = false;
    private String aftertrimmed;
    private String currentuser;
    public DatabaseReference discover,discoverSession,user;
    ArrayList list_of_discover;
    private RequestQueue mQueue;

    public DiscoverAdapter(ArrayList<Discovers> Discoverlist){
        discoverArraylist = Discoverlist;
    }

    public DiscoverAdapter(Context c, ArrayList<Discovers> d,String y,ArrayList z){
        context = c;
        discoverArraylist = d;
        currentuser = y;
        list_of_discover = z;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.discover_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final Discovers newdiscover = discoverArraylist.get(position);
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        mQueue  = Volley.newRequestQueue(context);

        discoverclap = FirebaseDatabase.getInstance().getReference().child("DiscoverSession").child("Friend").child(newdiscover.getDiscoverkey()).child("discoverDetails").child("countClap");
        user = FirebaseDatabase.getInstance().getReference().child("Users");
        System.out.println("current user "+currentuser);


        if(!newdiscover.getClapcount().equals("0")){
            holder.numberclap.setText(newdiscover.getClapcount());

        }

        if(!newdiscover.getCommentcount().equals("0")){
            holder.numbercomment.setText(newdiscover.getCommentcount());
        }

        if(newdiscover.isClap()==false){
            holder.unclap.setVisibility(View.VISIBLE);
            holder.clap.setVisibility(View.INVISIBLE);
        }
        else{
            holder.unclap.setVisibility(View.INVISIBLE);
            holder.clap.setVisibility(View.VISIBLE);
        }

        holder.status.setText(newdiscover.getPostCaption());
        holder.loc.setText(newdiscover.getPostAddress());
        holder.user.setText(newdiscover.getDisplayName());
        holder.comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.child(currentuser).child("metaData").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final String displayname = dataSnapshot.child("displayName").getValue().toString();
                        final String url = dataSnapshot.child("avatarURL").getValue().toString();

                        Intent i = new Intent(context, CommentLayoutActivity.class);
                        i.putExtra("discoverPostid",newdiscover.getDiscoverkey());
                        i.putExtra("currentuser",currentuser);
                        i.putExtra("displayname",displayname);
                        i.putExtra("avatarurl",url);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
        });

        holder.unclap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.unclap.setVisibility(View.INVISIBLE);
                holder.clap.setVisibility(View.VISIBLE);
                long unixtime = System.currentTimeMillis() / 1000L;
                RequestQueue queue = Volley.newRequestQueue(context);
                String url = "https://api.pingchat.app/discover/v3/clap/";
                System.out.println("discoverkey"+newdiscover.getDiscoverkey());
                StringRequest sq = new StringRequest
                        (Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {}
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }){
                    protected Map<String,String> getParams(){
                        Map<String,String> parr = new HashMap<String, String>();
                        parr.put("clapPostID", newdiscover.getDiscoverkey());
                        parr.put("clapUserID", currentuser);
                        parr.put("type", "discoverClap");
                        parr.put("clapTimestamp", Long.toString(unixtime)+"000");
                        return parr;
                    }
                };
                sq.setRetryPolicy(new DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(sq);
            }
        });

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(newdiscover.getAvatarURL());
        GlideApp.with(context.getApplicationContext())
                .load(storageReference)
                .into(holder.userimage);

        if(newdiscover.getPostImageURL()!=""){
            System.out.println("masuk sini newdiscover.getPostImageURL()!=");
            StorageReference storageReference1 = FirebaseStorage.getInstance().getReference().child(newdiscover.getPostImageURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference1)
                    .into(holder.image);
        }
        else{
            System.out.println("masuk sini do nothing");
            //do nothing
        }

        System.out.println("getdate"+newdiscover.getDate());
        holder.date.setText(newdiscover.getDate());

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentuser.equals(newdiscover.getPostSenderID())){
                    holder.showdialog(gettrimming(newdiscover.getPostImageURL()));
                }
                else{
                    System.out.println("newdiscover.getPostSenderID()!=currentuser");
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return discoverArraylist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView status,loc,date,user,more,less,numbercomment,numberclap;
        ImageView image,userimage,menu,clap,comments,unclap;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status2);
            image = itemView.findViewById(R.id.picture);
            loc = itemView.findViewById(R.id.loc);
            date = itemView.findViewById(R.id.date);
            user = itemView.findViewById(R.id.user);
            more = itemView.findViewById(R.id.moree);
            less = itemView.findViewById(R.id.lesss);
            userimage = itemView.findViewById(R.id.user_avatar_bg);
            menu = itemView.findViewById(R.id.triple_btn);
            clap = itemView.findViewById(R.id.clap);
            comments = itemView.findViewById(R.id.comments);
            numberclap = itemView.findViewById(R.id.numberclap);
            numbercomment = itemView.findViewById(R.id.numbercomment);
//            unclap = itemView.findViewById(R.id.clap1);

            status.post(new Runnable() {
                @Override
                public void run() {
                status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    if (isTextViewClicked) {
                        //This will shrink textview to 2 lines if it is expanded.
                        status.setMaxLines(1);
                        isTextViewClicked = false;
                        less.setVisibility(View.GONE);
                    } else {
                        //This will expand the textview if it is of 2 lines
                        status.setMaxLines(Integer.MAX_VALUE);
                        isTextViewClicked = true;
                        less.setVisibility(View.VISIBLE);
                    }
                    }
                });
                less.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    less.setVisibility(View.GONE);
                    status.setMaxLines(1);
                    }
                });
                }
            });
        }

        private void showdialog(final String discoverid){
            AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
            String[] pictureDialogItems = {"Edit","Delete"};
            pictureDialog.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) { switch (which) {
                    case 0:
                        System.out.println("edit");
                        break;
                    case 1:
                        deletepost(discoverid);
                        //System.out.println(discoverid);
                        break;
                }
                }
            });
            pictureDialog.show();
        }

        private void deletepost(final String discoverid){
            removeItem(itemView);
            //System.out.println("sss"+list_of_discover);
            discover = FirebaseDatabase.getInstance().getReference().child("Users");
            discoverSession = FirebaseDatabase.getInstance().getReference().child("DiscoverSession").child("Friend");

            discoverSession.child(discoverid).child("members").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        final String useridd = dataSnapshot1.getKey().toString();
                        discover.child(useridd).child("Discover").child(discoverid).removeValue();
                        System.out.println("masuk sini");
                    }

                    discover.child(currentuser).child("Discover").child(discoverid).removeValue();
                    discoverSession.child(discoverid).removeValue();
                                //alter sini bisuk, tengok remove item
//                                Intent intent = new Intent(context,CompleteDeleteDiscoverPost.class);
//                                intent.putExtra("discoverid",discoverid);
//                                context.startActivity(intent);
                                //discoverSession.child(discoverid).removeValue();

                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
            // notifyDataSetChanged();
        }

    }

    public void filterList(ArrayList<Discovers> filteredlist){
        discoverArraylist = filteredlist;
        notifyDataSetChanged();
    }



    private String gettrimming(String discoverid){
        String yuyu = discoverid.replace("postImage/","");
        String y1 = yuyu.substring(0,yuyu.indexOf('.'));
        System.out.println("lepas trim "+y1);
        aftertrimmed = y1;
        return aftertrimmed;
    }

    public void removeItem(final View v) {
        Animation.AnimationListener al = new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                notifyDataSetChanged();
            }
            @Override public void onAnimationRepeat(Animation arg0) {}
            @Override public void onAnimationStart(Animation arg0) {}
        };
        collapse(v, al);
    }

    private void collapse(final View v, Animation.AnimationListener animation) {

        final int initialHeight = v.getHeight();
        Animation anim = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                }
                else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        if (animation!=null) {
            anim.setAnimationListener(animation);
        }
        anim.setDuration(250);
        v.startAnimation(anim);
    }

    //besok remove item dlu baru delete ok

}