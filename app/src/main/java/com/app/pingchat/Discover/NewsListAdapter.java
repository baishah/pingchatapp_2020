package com.app.pingchat.Discover;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.Chat.addMemberAdapter;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.android.volley.VolleyLog.TAG;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.StoryListViewHolder> {
//    String userNameData[];
//    int storyImages[];
    ArrayList<NW_detailsClass> nw_detailsList;
    Context context;
    private int positionSelected = -1;
    String currentuser;
    private byte[] mDisplaybytes;



    public NewsListAdapter(Context ct,ArrayList<NW_detailsClass> nw,String user) {
        context = ct;
        nw_detailsList = nw;
        currentuser = user;
//        userNameData = userName;
//        storyImages = storyImage;
    }

    @NonNull
    @Override
    public StoryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View view = inflater.inflate(R.layout.ads_discover_item, parent, false);
//        return new StoryListViewHolder(view);
        View v = LayoutInflater.from(context).inflate(R.layout.ads_discover_item,parent,false);
        return new StoryListViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull final StoryListViewHolder holder,final int position) {

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        final NW_detailsClass nw_detailsClass = nw_detailsList.get(position);

        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");

        String imageUrl = nw_detailsClass.getNWM_mediaURL();
        long timeStamp = nw_detailsClass.getNWD_newsTimestamp();
        String dateString = format.format(new Date(Long.parseLong(String.valueOf(timeStamp))));
        String title = nw_detailsClass.getNWD_newsTitle();
        String description = nw_detailsClass.getNWD_newsDescription();
        String newurl = nw_detailsClass.getNWD_newsURL();
        holder.storyUsername.setText(title);
//        holder.storyPhoto.setImageResource(nw_detailsClass.getNWM_mediaURL());


        if(imageUrl.startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(imageUrl)
                    .override(600,600)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(circularProgressDrawable)
                    .into(holder.storyPhoto);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageUrl);
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .override(600, 600)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(circularProgressDrawable)
                    .into(holder.storyPhoto);
        }

        if (position == positionSelected) {
//            holder.storyPhoto.setBorderColor(Color.parseColor("#FFFFFF"));

            final Intent intent = new Intent(context, NewsShowActivity.class);
            intent.putExtra("mediaUrl",imageUrl );
            intent.putExtra("newUrl", newurl);
            intent.putExtra("storyUsername", title);
            intent.putExtra("timeStamp",dateString);
            intent.putExtra("description",description);

            context.startActivity(intent);
        } else {
//            holder.storyPhoto.setBorderColor(Color.parseColor("#0D47A1"));
        }

        //        holder.itemView.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, StoryActivity.class);
//                context.startActivity(intent);
//            }
//        });
    }
    @Override
    public int getItemCount() {
        return nw_detailsList.size();
    }


    public class StoryListViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout storyItem, storyOutline;
        TextView storyUsername;
        RoundedImageView storyPhoto;

        public StoryListViewHolder(@NonNull View itemView) {
            super(itemView);

            storyPhoto = itemView.findViewById(R.id.storyPhoto);
            storyUsername = itemView.findViewById(R.id.storyUsername);
            storyItem = itemView.findViewById(R.id.storyItem);
            storyOutline = itemView.findViewById(R.id.storyItem);

            storyItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() == positionSelected) {
                        // Unselect currently selected w/c means no selection.
                        positionSelected = -1;
                    } else {
                        positionSelected = getAdapterPosition();
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }








}
