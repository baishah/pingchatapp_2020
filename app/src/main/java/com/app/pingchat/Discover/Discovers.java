package com.app.pingchat.Discover;

public class Discovers {
    public String postAddress;
    public String postCaption;
    public String postImageURL;
    public String postSenderID;
    public String postType;
    public String displayName;
    public String avatarURL;
    public String deviceToken;
    public String discoverkey;
    public String clapcount;
    public String commentcount;
    public boolean clap;


    public String date;
    public Long postDate;



    public Discovers(boolean clap,String discoverkey,String deviceToken,String date,String displayName, String avatarURL, String postAddress, String postCaption, Long postDate, String postImageURL, String postSenderID, String postType) {
        this.postAddress = postAddress;
        this.postCaption = postCaption;
        this.postDate = postDate;
        this.postImageURL = postImageURL;
        this.postSenderID = postSenderID;
        this.postType = postType;
        this.displayName = displayName;
        this.avatarURL = avatarURL;
        this.date = date;
        this.deviceToken = deviceToken;
        this.discoverkey = discoverkey;
        this.clap = clap;
    }

    public Discovers(){}

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }

    public void setPostAddress(String postAddress) {
        this.postAddress = postAddress;
    }

    public void setPostCaption(String postCaption) {
        this.postCaption = postCaption;
    }

    public void setPostDate(Long postDate) {
        this.postDate = postDate;
    }

    public void setPostImageURL(String postImageURL) {
        this.postImageURL = postImageURL;
    }

    public void setPostSenderID(String postSenderID) {
        this.postSenderID = postSenderID;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostAddress() {
        return postAddress;
    }

    public String getPostCaption() {
        return postCaption;
    }

    public Long getPostDate() {
        return postDate;
    }

    public String getPostImageURL() {
        return postImageURL;
    }

    public String getPostSenderID() {
        return postSenderID;
    }

    public String getPostType() {
        return postType;
    }

    public String getDisplayName() { return displayName; }

    public void setDisplayName(String displayName) { this.displayName = displayName; }

    public String getAvatarURL() { return avatarURL; }

    public void setAvatarURL(String avatarURL) { this.avatarURL = avatarURL; }

    public String getDeviceToken() { return deviceToken; }

    public void setDeviceToken(String deviceToken) { this.deviceToken = deviceToken; }

    public String getDiscoverkey() { return discoverkey; }

    public void setDiscoverkey(String discoverkey) { this.discoverkey = discoverkey; }

    public boolean isClap() { return clap; }

    public void setClap(boolean clap) { this.clap = clap; }

    public String getClapcount() {
        return clapcount;
    }

    public void setClapcount(String clapcount) {
        this.clapcount = clapcount;
    }

    public String getCommentcount() {
        return commentcount;
    }

    public void setCommentcount(String commentcount) {
        this.commentcount = commentcount;
    }
}
