package com.app.pingchat.Discover;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class editDiscoverActivity extends AppCompatActivity {
    StorageReference discoverimage;
    DatabaseReference usersRef,pDiscover;
    private FirebaseAuth mAuths;
    private ImageView imagepriviu;
    private EditText editCaption;
    private Button saveBtn;
    private ImageButton close;
    private String currentuser;
    String mediaType,caption,discoverKey,discoverUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_discover_layout);

        imagepriviu = findViewById(R.id.imagepreview);
        editCaption = findViewById(R.id.edit_caption);
        saveBtn = findViewById(R.id.save_btn);
        close = findViewById(R.id.close_btn);
        discoverimage = FirebaseStorage.getInstance().getReference().child("postImage");
        mAuths = FirebaseAuth.getInstance();
        currentuser = mAuths.getCurrentUser().getUid();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");

        Intent i = getIntent();
        mediaType = i.getStringExtra("mediaType");
        caption= i.getStringExtra("caption");
        discoverKey = i.getStringExtra("discoverKey");
        discoverUrl = i.getStringExtra("discoverUrl");

        if(mediaType.equals("noMedia")){
            imagepriviu.setVisibility(View.GONE);
            editCaption.setText(caption);
            editCaption.setSelection(editCaption.getText().length());
        }else{
            imagepriviu.setVisibility(View.VISIBLE);
            editCaption.setText(caption);
            editCaption.setSelection(editCaption.getText().length());
            CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(this);
            circularProgressDrawable.setStrokeWidth(10f);
            circularProgressDrawable.setCenterRadius(50f);
            circularProgressDrawable.start();
            System.out.println("masuk sini newdiscover.getPostImageURL()!=");
            StorageReference storageReference1 = FirebaseStorage.getInstance().getReference().child(discoverUrl);
            GlideApp.with(this.getApplicationContext())
                    .load(storageReference1)
                    .placeholder(circularProgressDrawable)
                    .into(imagepriviu);
        }

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveEdit(discoverKey);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void saveEdit(String discoverKey) {
        usersRef.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").child("UDSD_caption").setValue(editCaption.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(),"Post update successfully...", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
