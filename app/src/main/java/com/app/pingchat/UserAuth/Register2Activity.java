package com.app.pingchat.UserAuth;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Discover.DiscoverSetup;
import com.app.pingchat.GlideApp;
import com.app.pingchat.GuidePingChat;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hbb20.CountryCodePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class Register2Activity extends AppCompatActivity {

    EditText displaydate;
    EditText uname;
    EditText phonenumber;
    TextView checkavail;
    CountryCodePicker ccp;
    Button bg,submit, back;
    String e,p,type,photourl,username,currentid,emailU;
    boolean unameavailability = false;
    private FirebaseAuth mAuth;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    CircleImageView userprofileimage;
    FrameLayout layout;
    ProgressDialog dialog;
    private StorageReference userimage;
    public static Context contextOfApplication;
    private byte[] avatar;
    HashMap<String, Object> timestampCreated;
    public byte[] getAvatar() {
        return avatar;
    }
    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }
    public HashMap<String, Object> getTimestampCreated(){
        return timestampCreated;
    }
    private int choice;
    private String month_in_string;
    private Bitmap defaultimage;
    private TypedArray images;
    private boolean registerclicked=false;

    Bundle extras;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register2_activity);

        dialog = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
        userimage = FirebaseStorage.getInstance().getReference().child("profile");
        contextOfApplication = getApplicationContext();
        extras = getIntent().getExtras();
        if(extras!=null){
            e = extras.getString("email");
            p = extras.getString("password");
            Log.d(e,"email");
            Log.d(p,"password");

            type = extras.getString("type");
            emailU = extras.getString("emailU");
            photourl = extras.getString("photourl");
            username = extras.getString("username");
            currentid = extras.getString("id");
            Log.d(type,"type");
            Log.d(photourl,"photourl");
            Log.d(username,"username");
            Log.d(currentid,"id");

        }

        displaydate = findViewById(R.id.birthday);
        uname = findViewById(R.id.username);
        phonenumber = findViewById(R.id.phone);
        ccp = findViewById(R.id.ccp);
        submit = findViewById(R.id.submitbutton);
        back = findViewById(R.id.backbutton);
        userprofileimage = findViewById(R.id.avatar);
        bg = findViewById(R.id.cam_btn);
        layout = findViewById(R.id.frameLayout);
        checkavail = findViewById(R.id.checkavail);

        images = getResources().obtainTypedArray(R.array.defaultProfile);
        choice = (int)(Math.random()*images.length());
        userprofileimage.setImageResource(images.getResourceId(choice,R.drawable.asset_1));
        defaultimage = BitmapFactory.decodeResource(getResources(),images.getResourceId(choice,R.drawable.asset_1));
        //images.recycle();

        findViewById(R.id.ll3).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        ccp.registerCarrierNumberEditText(phonenumber);
        ccp.setDefaultCountryUsingNameCode("MY");
        ccp.resetToDefaultCountry();
        ccp.isValidFullNumber();

        displaydate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        Register2Activity.this, android.R.style.Theme_Holo_Light_Dialog, mDateSetListener, year, month, day
                );
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;

                String date = day + ", " + getStringmonth(month) + ", " + year;
                if(year > 2001){
                    Toast.makeText(Register2Activity.this, "Minimum age is 18 years", Toast.LENGTH_SHORT).show();
                }
                else{
                    displaydate.setText(date);
                    displaydate.setTextColor(Color.BLACK);
                }
            }
        };

        uname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()>0){
                    checkavail.setVisibility(View.VISIBLE);
                }
                else{
                   checkavail.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String get_username = uname.getText().toString();
                checkusernameunique(get_username);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
                }
                else{
                    CropImage.startPickImageActivity(Register2Activity.this);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                confirmation();
            }
        });

        if(registerclicked){
            newregister();
        }
        else{
            Toast.makeText(Register2Activity.this, "Register clicked", Toast.LENGTH_SHORT).show();
        }

        if(extras.containsKey("type")){
            back.setVisibility(View.GONE);
            uname.setText(username);
            GlideApp.with(getApplicationContext())
                    .load(photourl)
                    .into(userprofileimage);

        }
        else{
            System.out.println("masuk sini");
        }

    }

    public String getStringmonth(int month){
        switch (month){
            case 1:
                month_in_string = "January";
                break;

            case 2:
                month_in_string = "February";
                break;

            case 3:
                month_in_string = "March";
                break;

            case 4:
                month_in_string = "April";
                break;

            case 5:
                month_in_string = "May";
                break;

            case 6:
                month_in_string = "June";
                break;

            case 7:
                month_in_string = "July";
                break;

            case 8:
                month_in_string = "August";
                break;

            case 9:
                month_in_string = "September";
                break;

            case 10:
                month_in_string = "October";
                break;

            case 11:
                month_in_string = "November";
                break;

            case 12:
                month_in_string = "December";
                break;
        }
        return  month_in_string;
    }

    public void checkusernameunique(String username){
        checkavail.setTextColor(Color.parseColor("#bababa"));
        checkavail.setText("Checking availability ...");

        DatabaseReference userdatabase = FirebaseDatabase.getInstance().getReference();

        Query q = userdatabase.child("Users").orderByChild("userDetails/UD_username");
        q.equalTo(username).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue()!=null){
                        checkavail.setText("This username is not available.Try again");
                        checkavail.setTextColor(Color.parseColor("#f94545"));
                        unameavailability = false;
                    }
                    else{
                        checkavail.setText("Username is available");
                        checkavail.setTextColor(Color.parseColor("#01ee00"));
                        unameavailability = true;
                    }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void updatedatabase(){

        final DatabaseReference user = FirebaseDatabase.getInstance().getReference().child("Users").child(currentid).child("userDetails");
        final byte[] avatar1 = getAvatar();
        final String phoneNo = phonenumber.getText().toString();
        final String phone = ccp.getFullNumberWithPlus();
        final String bday = displaydate.getText().toString();
        final String displayname = uname.getText().toString();


        if(((phoneNo !=null && !phoneNo.isEmpty()) && (bday !=null && !bday.isEmpty()) && (displayname!=null && !displayname.isEmpty()) && (unameavailability=true))){
            dialog.setMessage("Completing profile on progress\nPlease wait");
            dialog.show();

            String status = "Hi I am " + displayname;
            user.child("UD_avatarURL").setValue(photourl);
            user.child("UD_displayName").setValue(displayname);
            user.child("UD_email").setValue(emailU);
            user.child("UD_registeredOn").setValue(ServerValue.TIMESTAMP);
            user.child("UD_userID").setValue(currentid);
            user.child("UD_signInType").setValue("google");
            user.child("UD_aboutMe").setValue(status);
            user.child("UD_phoneNo").setValue(phone);
            user.child("UD_birthday").setValue(bday);
            user.child("UD_username").setValue(displayname).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        dialog.dismiss();
                        Toast.makeText(Register2Activity.this, "Profile complete", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }

        else if(((getAvatar()!=null)&&(phoneNo !=null && !phoneNo.isEmpty()) && (bday !=null && !bday.isEmpty()) && (displayname!=null && !displayname.isEmpty()) && (unameavailability=true))){
            dialog.setMessage("Completing profile on progress\nPlease wait");
            dialog.show();

            StorageReference filepath = userimage.child(currentid + ".jpg");
            UploadTask uploadTask = filepath.putBytes(getAvatar());
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    String y = "profile/" + currentid + ".jpg";
                    String status = "Hi I am " + displayname;

                    user.child("UD_email").setValue(emailU);
                    user.child("UD_registeredOn").setValue(ServerValue.TIMESTAMP);
                    user.child("UD_userID").setValue(currentid);
                    user.child("UD_signInType").setValue("google");
                    user.child("UD_avatarURL").setValue(y);
                    user.child("UD_aboutMe").setValue(status);
                    user.child("UD_birthday").setValue(bday);
                    user.child("UD_displayName").setValue(displayname);
                    user.child("UD_phoneNo").setValue(phone);
                    user.child("UD_username").setValue(displayname).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                dialog.dismiss();
                                Toast.makeText(Register2Activity.this, "Profile complete", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        }
                    });
                }
            });
        }
    }

    public void newregister(){
        final byte[] avatar1 = getAvatar();
        final String phoneNo = phonenumber.getText().toString();
        final String phone = ccp.getFullNumberWithPlus();
        final String bday = displaydate.getText().toString();

        final String displayname = uname.getText().toString();

        if(getAvatar()==null){
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            defaultimage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
            byte[] datas = byteArrayOutputStream.toByteArray();
            setAvatar(datas);
        }

        if((phoneNo !=null && !phoneNo.isEmpty()) && (bday !=null && !bday.isEmpty()) && (displayname!=null && !displayname.isEmpty()) && (unameavailability=true)){
            dialog.setMessage("Registration on progress..\nPlease wait");
            dialog.show();
            mAuth.createUserWithEmailAndPassword(e, p).addOnCompleteListener(Register2Activity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        Toast.makeText(Register2Activity.this, "Signup error!"+ task.getException(), Toast.LENGTH_SHORT).show();
                    } else {
                        final String uid = mAuth.getCurrentUser().getUid();
                        final DatabaseReference user = FirebaseDatabase.getInstance().getReference().child("Users");
                        HashMap<String, Object> timestampnow = new HashMap<>();
                        timestampnow.put("timestamp", ServerValue.TIMESTAMP);

                        StorageReference filepath = userimage.child(uid + ".jpg");
                        UploadTask uploadTask = filepath.putBytes(getAvatar());
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                String y = "profile/" + uid + ".jpg";

                                String status = "Hi I am " + displayname;

                                final Map newPost = new HashMap();
                                newPost.put("UD_avatarURL", y);
                                newPost.put("UD_aboutMe",status);
                                newPost.put("UD_birthday", bday);
                                newPost.put("UD_displayName", displayname);
                                newPost.put("UD_email", e);
                                newPost.put("UD_password", p);
                                newPost.put("UD_phoneNo", phone);
                                newPost.put("UD_registeredOn", ServerValue.TIMESTAMP);
                                newPost.put("UD_userID", uid);
                                newPost.put("UD_username", displayname);
                                newPost.put("UD_signInType","email");

                                user.child(uid).child("userDetails").setValue(newPost).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            dialog.dismiss();
                                            //Toast.makeText(Register2Activity.this, "Profile registered successfully", Toast.LENGTH_SHORT).show();
                                            sendEmailVerification();
                                            mAuth.signOut();
                                            finish();

                                        } else {
                                            String message = task.getException().toString();
                                            Toast.makeText(Register2Activity.this, "error:" + message, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
        else{
            Toast.makeText(Register2Activity.this, "Please fill in all required information", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendEmailVerification() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // email sent

                    // after email is sent just logout the user and finish this activity

//                        Toast.makeText(SignupActivity.this, "Check your email to verify", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Verification email sent to " + user.getEmail(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Register2Activity.this, GuidePingChat.class));
                    finish();
                }
                else
                {
                    // email not sent, so display message and restart the activity or do whatever you wish to do

                    //restart this activity
                    overridePendingTransition(0, 0);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = contextOfApplication.getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
                    userprofileimage.setVisibility(View.VISIBLE);
                    bg.setVisibility(View.VISIBLE);
                    userprofileimage.setImageBitmap(selectedImage);
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    setAvatar(datas);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(this,data);

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(Register2Activity.this);
        }
    }

    @Override
    public void onBackPressed(){
      finish();
      deleteuserfromauth();
      Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
      startActivity(intent);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //deleteuserfromauth();
    }

    public void deleteuserfromauth(){
        FirebaseUser currentuser=mAuth.getCurrentUser();
        currentuser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d("TAG","OK! Works fine!");
                }
                else{
                    Log.d("TAG","something is wrong");
                }
            }
        });
    }

    public void confirmation(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog alertDialog = builder.create();
        View view = LayoutInflater.from(this).inflate(R.layout.confirm_register_popup_layout,null);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);

        CircleImageView userimage = view.findViewById(R.id.avatarss);
        TextView email = view.findViewById(R.id.email);
        TextView username = view.findViewById(R.id.Username);
        TextView phone = view.findViewById(R.id.Phone);
        TextView birthday = view.findViewById(R.id.Birthday);
        Button submit = view.findViewById(R.id.submit);
        Button back = view.findViewById(R.id.back);
        Button close = view.findViewById(R.id.x);

        if(extras.containsKey("type")){
            email.setText("Email :"+emailU);
            if(getAvatar()!=null){
                GlideApp.with(this)
                        .load(getAvatar())
                        .into(userimage);
            }
            else{
                GlideApp.with(this)
                        .load(photourl)
                        .into(userimage);
            }

        }
        else{
            email.setText("Email :"+e);
            if(getAvatar()!=null){
                GlideApp.with(this)
                        .load(getAvatar())
                        .into(userimage);
            }
            else{
                GlideApp.with(this)
                        .load(defaultimage)
                        .into(userimage);
            }
        }


        username.setText("Username :"+uname.getText().toString());
        phone.setText("Phone No :"+phonenumber.getText().toString());
        birthday.setText("Birthday :"+displaydate.getText().toString());
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(extras.containsKey("type")){
                    updatedatabase();
                }
                else{
                    registerclicked = true;
                    newregister();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        return;
    }
}