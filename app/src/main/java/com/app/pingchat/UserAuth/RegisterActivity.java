package com.app.pingchat.UserAuth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {
    EditText registeremail,registerpassword,confirmpassword;
    Button registerButton, btnReset,googlesignin;
    TextView textLogin, textRegister,loginButton;
    public static String email;
    public static String password,confirm_pwd;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 9001;
    FirebaseAuth firebaseAuth;
    ProgressDialog dialog;
    public DatabaseReference userRef;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        registeremail = findViewById(R.id.email);
        registerpassword = findViewById(R.id.password);
        confirmpassword = findViewById(R.id.re_enter_pwd);
        loginButton = findViewById(R.id.login_link);
        registerButton = findViewById(R.id.sign_up);
        btnReset = findViewById(R.id.btn_reset_password);
        textLogin = findViewById(R.id.textLogin);
        textRegister = findViewById(R.id.textRegister);
        dialog = new ProgressDialog(this);
        googlesignin = findViewById(R.id.google_sign_in);

        firebaseAuth = FirebaseAuth.getInstance();
        userRef = FirebaseDatabase.getInstance().getReference().child("Users");

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("321197428675-4gvf69puuv128rkooc3qebqmndhempm7.apps.googleusercontent.com")
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);

        googlesignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
    }

    public void registerUser(){
        email = registeremail.getText().toString().trim();
        password = registerpassword.getText().toString().trim();
        confirm_pwd = confirmpassword.getText().toString().trim();

        if(email.isEmpty()){
            registeremail.setError("email required");
            registeremail.requestFocus();
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            registeremail.setError("set valid email");
            registeremail.requestFocus();
            return;
        }

        if(password.isEmpty()){
            registerpassword.setError("password required");
            registerpassword.requestFocus();
            return;
        }

        if(confirm_pwd.isEmpty()){
            confirmpassword.setError("re-enter password required");
            confirmpassword.requestFocus();
            return;
        }

        if(!password.equals(confirm_pwd)){
            confirmpassword.setError("password dont match");
            confirmpassword.requestFocus();
            return;
        }

        if(password.length()<6){
            registerpassword.setError("minimum password should be 6");
            registerpassword.requestFocus();
            return;
        }

        Intent intent = new Intent(getApplicationContext(),Register2Activity.class);
        intent.putExtra("email",email);
        intent.putExtra("password",password);
        startActivity(intent);
    }

    private void signIn() {
        Intent signIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signIntent,RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try{
                GoogleSignInAccount account = task.getResult(ApiException.class);
                System.out.println("masuk sini on activity");
//                String name = account.getDisplayName();
                firebaseAuthWithGoogle(account);
            }
            catch (ApiException e){
                Log.w("tag", "Google sign in failed", e);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.show();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            String id = user.getUid();
                            String email = user.getEmail();
                            String photourl = user.getPhotoUrl().toString();
                            String[] parts = email.split("@");
                            String first = parts[0];
                            String second = parts[1];
                            String username = first;
                            checkuser(id,photourl,username,email);
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void checkuser(String id, String photourl, String username, String email){
        userRef = FirebaseDatabase.getInstance().getReference().child("Users");
        userRef.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    System.out.println("profile already exists");
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    Intent intent = new Intent(getApplicationContext(), Register2Activity.class);
                    intent.putExtra("type","google_signin");
                    intent.putExtra("emailU",email);
                    intent.putExtra("photourl",photourl);
                    intent.putExtra("username",username);
                    intent.putExtra("id",id);
                    startActivity(intent);
                    finish();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}