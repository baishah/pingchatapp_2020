package com.app.pingchat;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Discover.DiscoveryComments;
import com.app.pingchat.Discover.DiscoveryCommentsAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class bottomSheetShowComment extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;

    private String datetimes,datess,discoverid,typedcomment,currentuser,displayname,avatar,discoverpostid,ownerid;
    private EditText typecomment;
    private Button sendcomment;
    private String mediaURL;
    private FirebaseUser mAuth;
    DiscoveryComments comments;

    RecyclerView commentlist;
    private ArrayList<DiscoveryComments> commentarraylist = new ArrayList<>();
    private RecyclerView.Adapter adapter;

    DatabaseReference user;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.comment_section_layout, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        typecomment = v.findViewById(R.id.type_comment);
        sendcomment = v.findViewById(R.id.send_comment);

        commentlist = v.findViewById(R.id.commentlist);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        commentlist.setLayoutManager(layoutManager);
        layoutManager.setStackFromEnd(true);
        System.out.println("discover id "+discoverid);
        user = FirebaseDatabase.getInstance().getReference().child("Users");

        adapter = new DiscoveryCommentsAdapter(getContext(),commentarraylist);


        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog d = (BottomSheetDialog) dialogInterface;
                View bottomSheetInternal = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);

                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetInternal);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                getComment(getDiscoverpostid());

//                if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
//                    commentarraylist = new ArrayList<>();
//                }
            }
        });

        sendcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
            }
        });

        return v;
    }

    public void getComment(String discoverpostid){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading comments");
        progressDialog.show();
        commentarraylist = new ArrayList<>();

        user.child(getOwnerid()).child("userDiscovers").child("UDS_List").child(discoverpostid).child("UDS_Comments").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    progressDialog.dismiss();
                    Toast.makeText(getContext().getApplicationContext(), "No comment for this post", Toast.LENGTH_SHORT).show();
                }
                else{
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        final DiscoveryComments comments = new DiscoveryComments();
                        String comment = dataSnapshot1.child("UDSC_comment").getValue().toString();
                        long timestamps = Long.parseLong(dataSnapshot1.child("UDSC_timestamp").getValue().toString());
                        String commentownerid = dataSnapshot1.child("UDSC_ownerID").getValue().toString();

                        comments.setUDSC_timestamp(getdatetime(timestamps));
                        comments.setUDSC_comment(comment);

                        user.child(commentownerid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                String picture = dataSnapshot.child("UD_avatarURL").getValue().toString();

                                comments.setUsername(name);
                                comments.setUser_profile(picture);
                                commentarraylist.add(comments);

                                commentlist.setAdapter(adapter);
                                progressDialog.dismiss();
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void sendComment(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("sending message");
        progressDialog.show();
        typedcomment = typecomment.getText().toString();
        String commentid = user.push().getKey();

        final Map UDS_Comment = new HashMap();
        UDS_Comment.put("UDSC_ownerID",discoverid);
        UDS_Comment.put("UDSC_comment",typedcomment);
        UDS_Comment.put("UDSC_timestamp", ServerValue.TIMESTAMP);
        UDS_Comment.put("UDSC_mediaType","image");
        UDS_Comment.put("UDSC_mediaURL", ServerValue.TIMESTAMP);

        final Map UDS_Comment_noMedia = new HashMap();
        UDS_Comment_noMedia.put("UDSC_ownerID",getCurrentuser());
        UDS_Comment_noMedia.put("UDSC_comment",typedcomment);
        UDS_Comment_noMedia.put("UDSC_timestamp",ServerValue.TIMESTAMP);
        UDS_Comment_noMedia.put("UDSC_mediaType","noMedia");

        final Date d = new Date();
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.add(Calendar.HOUR_OF_DAY,24);
        String jj = calendar.getTime().toString();
        String hh = dateFormat.format(new Date(jj)).toString();

        long unixtime = System.currentTimeMillis() / 1000L;

        String timetemporary = (unixtime)+"000";

        user.child(getOwnerid()).child("userDiscovers").child("UDS_List").child(getDiscoverpostid()).child("UDS_Comments").child(commentid).setValue(UDS_Comment_noMedia).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    progressDialog.dismiss();
                    user.child(getCurrentuser()).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String dname = dataSnapshot.child("UD_displayName").getValue().toString();
                            String avatarurl = dataSnapshot.child("UD_avatarURL").getValue().toString();

                            System.out.println("dname from bottom sheet :"+dname);
                            comments = new DiscoveryComments();
                            comments.setUsername(dname);
                            comments.setUser_profile(avatarurl);
                            comments.setUDSC_comment(typedcomment);
                            comments.setUDSC_timestamp(getdatetime(Long.parseLong(timetemporary)));
                            commentarraylist.add(comments);
                            if(!commentarraylist.isEmpty()){
                                adapter.notifyItemInserted(commentarraylist.size()-1);

                            }
                            else {
                                commentlist.setAdapter(adapter);
                                adapter.notifyItemInserted(0);
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
        });
        typecomment.setText("");
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);
        System.out.println("datetimestamp"+datetimestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
            System.out.println("datetimes"+datetimes);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = "2 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = "3 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = "4 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = "5 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = "6 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = "7 days ago";
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }

    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Date  currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        String dateplusone = df.format(ccc);
        datess = dateplusone;
        return datess;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDiscoverpostid() {
        return discoverpostid;
    }

    public void setDiscoverpostid(String discoverpostid) {
        this.discoverpostid = discoverpostid;
    }
    public String getCurrentuser() {
        return currentuser;
    }

    public void setCurrentuser(String currentuser) {
        this.currentuser = currentuser;
    }

    public String getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(String ownerid) {
        this.ownerid = ownerid;
    }

    public interface BottomSheetListener {
        void onClickComment(String text);
    }

}

