package com.app.pingchat.Friend;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.Profile.ViewUserProfileActivity;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendRequestAdapter extends RecyclerView.Adapter<FriendRequestAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,notification;
    Context context;
    ArrayList<userDetailsClass> mFriendRequest;
    private FirebaseAuth mAuths;
    private ArrayList<String> filteruserlist;
    private String currentuser;

    public FriendRequestAdapter(ArrayList<userDetailsClass> Friendlist){
        mFriendRequest = Friendlist;
    }

    public FriendRequestAdapter(Context c, ArrayList<userDetailsClass> f){
        context = c;
        mFriendRequest = f;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.friend_request_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        mAuths = FirebaseAuth.getInstance();
        currentuser = mAuths.getCurrentUser().getUid();

        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        notification = FirebaseDatabase.getInstance().getReference().child("Notification").child("FriendRequest");
        storeRef = storage.getReference();
        filteruserlist = new ArrayList<>();

        userDetailsClass mynewfriend = mFriendRequest.get(position);
        final String friendid = mynewfriend.getUD_userID();
        holder.u.setText(mynewfriend.getUD_displayName());
        holder.s.setText(mynewfriend.getUD_aboutMe());
        holder.onClick(mynewfriend.getUD_userID());
        ///Picasso.with(context.getApplicationContext()).load(mynewfriend.getAvatarURL()).into(holder.i);
        if(mynewfriend.getUD_avatarURL().startsWith("p")){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(mynewfriend.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }
        else{
            GlideApp.with(context.getApplicationContext())
                    .load(mynewfriend.getUD_avatarURL())
                    .into(holder.i);
        }

    }

    @Override
    public int getItemCount() {
        return mFriendRequest.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s;
        CircleImageView i;
        Button accept,reject;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            i = itemView.findViewById(R.id.gambar);
            accept = itemView.findViewById(R.id.accept);
            reject = itemView.findViewById(R.id.reject);
        }

        public void onClick(final String friendid){
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    usersRef.child(currentuser).child("userFriends").child(friendid).setValue("friend").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                removeItem(itemView);
                                usersRef.child(currentuser).child("userFriends").child(friendid).setValue("friend");
                                usersRef.child(friendid).child("userFriends").child(currentuser).setValue("friend");
                                usersRef.child(currentuser).child("userConversations").child("Personal").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()) {
                                            boolean value = false;
                                            for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                                if (dataSnapshot2.hasChild("UCP_friendID")) {
                                                    String chatId = dataSnapshot2.getKey();
                                                    String friendId = dataSnapshot2.child("UCP_friendID").getValue().toString();
                                                    if (friendId.equals(friendid)){
                                                        value = true;
                                                        usersRef.child(currentuser).child("userConversations").child("Personal").child(chatId).child("UCP_status").setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                usersRef.child(friendid).child("userConversations").child("Personal").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        if(dataSnapshot.exists()) {
                                                                            for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                                                                if (dataSnapshot2.hasChild("UCP_friendID")) {
                                                                                    String chatId = dataSnapshot2.getKey();
                                                                                    String friendId = dataSnapshot2.child("UCP_friendID").getValue().toString();
                                                                                    if (friendId.equals(currentuser)) {
                                                                                        usersRef.child(friendid).child("userConversations").child("Personal").child(chatId).child("UCP_status").setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                            @Override
                                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                                Toast.makeText(context, "Friend added successfully!", Toast.LENGTH_SHORT).show();
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    }
                                                                });
                                                            }
                                                        });
                                                        break;
                                                    }
                                                }
                                            }

                                            if (!value){
                                                send_new_chat_text(friendid);
                                            }
                                        }
                                        else{
                                            send_new_chat_text(friendid);
                                        }
                                    }
                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                                });
                            }
                        }
                    });
                }
            });
            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(itemView);
                    usersRef.child(currentuser).child("userFriends").child(friendid).removeValue();
                    usersRef.child(friendid).child("userFriends").child(currentuser).removeValue();
                }
            });
        }
    }

    public void removeItem(final View v) {
        Animation.AnimationListener al = new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                notifyDataSetChanged();
            }
            @Override public void onAnimationRepeat(Animation arg0) {}
            @Override public void onAnimationStart(Animation arg0) {}
        };
        collapse(v, al);
    }

    private void collapse(final View v, Animation.AnimationListener animation) {
        final int initialHeight = v.getHeight();
        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                }
                else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        if (animation!=null) {
            anim.setAnimationListener(animation);
        }
        anim.setDuration(250);
        v.startAnimation(anim);
    }

    public void filterList(ArrayList<userDetailsClass> filteredlist){
        mFriendRequest = filteredlist;
        notifyDataSetChanged();
    }

    public ArrayList<userDetailsClass> getmFriendRequest() {
        return mFriendRequest;
    }

    private void send_new_chat_text(String friendid){

        DatabaseReference personalchat = FirebaseDatabase.getInstance().getReference().child("Chats").child("Personal");

        String new_chatid = personalchat.push().getKey();
        String new_messageid = personalchat.push().push().getKey();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        HashMap new_chat_details = new HashMap();
        new_chat_details.put("CP_chatID",new_chatid);
        new_chat_details.put("CP_friendID",friendid);
        new_chat_details.put("CP_lastMessage","Start your conversations");
        new_chat_details.put("CP_lastMessageTimestamp",timestamp.getTime());
        new_chat_details.put("CP_lastMessageMediaType","notifier");

        HashMap personal_chat = new HashMap();
        personal_chat.put("CP_message","Start your conversations");
        personal_chat.put("CP_messageSenderID",currentuser);
        personal_chat.put("CP_mediaType","notifier");
        personal_chat.put("CP_messageTimestamp",timestamp.getTime());

        HashMap user_conv = new HashMap();
        user_conv.put("UCP_status","true");
        user_conv.put("UCP_friendID",friendid);
        user_conv.put("UCP_createdDate",timestamp.getTime());
        user_conv.put("UCP_lastMessageID",new_messageid);
        user_conv.put("UCP_lastSeenMessageID",new_messageid);

        HashMap user_conv_friend = new HashMap();
        user_conv_friend.put("UCP_status","true");
        user_conv_friend.put("UCP_friendID",currentuser);
        user_conv_friend.put("UCP_createdDate",timestamp.getTime());
        user_conv_friend.put("UCP_lastMessageID",new_messageid);
        user_conv_friend.put("UCP_lastSeenMessageID",new_messageid);

        personalchat.child(new_chatid).child("CP_personalDetails").setValue(new_chat_details);
        personalchat.child(new_chatid).child("CP_personalChatSession").child(new_messageid).setValue(personal_chat);
        usersRef.child(currentuser).child("userConversations").child("Personal").child(new_chatid).setValue(user_conv);
        usersRef.child(friendid).child("userConversations").child("Personal").child(new_chatid).setValue(user_conv_friend);

    }
}