package com.app.pingchat.Friend;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class AddNewFriendFragment extends Fragment {

    private DatabaseReference usersRef,chatReq,notification;
    private RecyclerView FindFriendRecyclerList;
    String add_friend;
    private FirebaseAuth mAuths;
    public String id,uid,True,Received,Sent;
    ArrayList<Friends> mFriendlist;
    ArrayList<Friends> usersff;
    AddFriendAdapter addFriendAdapter,userAdapter;

    Button add;
    ImageButton request;
    EditText search_to_add;

    private RecyclerView mrequestlist,mrequestlist2;
    FirebaseStorage storage;
    StorageReference storeRef;

    ArrayList<String> friendlist;
    ArrayList<String> alluserlist;
    ArrayList<String> filteruser;

    ArrayList<String> friendoffriend = new ArrayList<>();

    private LinkedHashMap<String,String> linkedHashMap = new LinkedHashMap<>();
    private ArrayList<String> phonenumber = new ArrayList<>();

    public ArrayList<String> getSss() {
        return sss;
    }

    public void setSss(ArrayList<String> sss) {
        this.sss = sss;
    }
    final ArrayList userf = new ArrayList<>();
     private ArrayList<String> sss = new ArrayList<>();
    ArrayList q= new ArrayList();
    private static final int CONTACT_PERMISSION = 1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_new_friend,container,false);

        mrequestlist = (RecyclerView) view.findViewById(R.id.add_friend);
        mrequestlist.setLayoutManager(new LinearLayoutManager(getContext()));

//        mrequestlist2 = (RecyclerView) view.findViewById(R.id.friends_to_invite);
//        mrequestlist2.setLayoutManager(new LinearLayoutManager(getContext()));

        mAuths = FirebaseAuth.getInstance();
        id = mAuths.getCurrentUser().getUid();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        usersRef.keepSynced(true);
        chatReq = FirebaseDatabase.getInstance().getReference().child("Users").child("Friends");
        chatReq.keepSynced(true);
        notification = FirebaseDatabase.getInstance().getReference().child("Notification").child("FriendRequest");
        ImageButton req = (ImageButton) view.findViewById(R.id.request);
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        True = "true";
        Received = "Received";
        Sent ="sent";
        search_to_add = view.findViewById(R.id.search_to_add);

        getContactPermission();
        view.findViewById(R.id.add_friend).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
            if(getActivity().getCurrentFocus()!=null){
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
            }
            return false;
            }
        });

        req.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Fragment fragment = new FriendRequestFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container,fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        search_to_add.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        friendlist = new ArrayList<String>();
        alluserlist = new ArrayList<String>();
        filteruser = new ArrayList<String>();

        Intent i = getActivity().getIntent();
        friendlist = i.getStringArrayListExtra("alreadyfriend");
        alluserlist = i.getStringArrayListExtra("alluser");

        Log.e("dari fragment","friendlist"+friendlist);
        Log.e("dari fragment","alluser"+alluserlist);

        getUserFriend();
        //filtereduser();
        //searchbyusername();
        //displaycontacts();
        //friendoffriend();
        return view;
    }

    public void filtereduser(){
        for(String item: alluserlist){
            if(friendlist.contains(item)){
                Log.e("list","friend "+friendlist);
            }
            else{
                filteruser.add(item);
                Log.e("list","not friend "+filteruser );
            }
        }
    }

    public void getUserFriend() {
        final ArrayList userss = new ArrayList<>();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String currentuser = mAuth.getCurrentUser().getUid();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");

        usersRef.child(currentuser).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String key = dataSnapshot1.getKey();
                    userss.add(key);
                    addFriendAdapter.setUserf(userss);
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getphoneandname(){
            mFriendlist = new ArrayList<Friends>();
            HashSet<String> hashSet = new HashSet<String>();
            String yy;
            String lastnumber = "0";
            Cursor phone  = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
            final int nameIndex = phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            final int numberindex = phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            String name,number;
            while(phone.moveToNext()){
                name = phone.getString(nameIndex);
                number = phone.getString(numberindex);

                if(number.equals(lastnumber)){

                }
                else{
                    lastnumber = number;
                    final Friends newfriend= new Friends();
                    yy = lastnumber.replaceAll("[\\s\\-()]","");
                    //  linkedHashMap.put(name,phoneNumber);
                    System.out.println("iii"+yy);
                    phonenumber.add(yy);

                    System.out.println("namak "+name);
                    newfriend.setDisplayName(name);

                    if(yy.startsWith("0")){
                        yy = "+6"+yy;
                    }
                    else if(yy.startsWith("6")){
                        yy = "+"+yy;
                    }
                    newfriend.setPhone(yy);

                    mFriendlist.add(newfriend);
                }
                }

            addFriendAdapter = new AddFriendAdapter(getContext(),mFriendlist,userf);
            mrequestlist.setAdapter(addFriendAdapter);

            hashSet.addAll(phonenumber);
            phonenumber.clear();
            phonenumber.addAll(hashSet);
            System.out.println("getphoneandname c "+phonenumber);

            ListIterator<String> itr = phonenumber.listIterator();
            while (itr.hasNext()) {
                itr.set(itr.next().replaceAll("[\\s\\-()]", ""));
                //itr.set(itr.next().replaceAll("",""));
            }
            System.out.println("trimmed "+phonenumber);

            for(int j=0;j<phonenumber.size();j++){
                if(phonenumber.get(j).startsWith("0")){
                    System.out.println("start with 0"+phonenumber.get(j));

                    String y = "+6"+phonenumber.get(j);
                    phonenumber.set(j,y);

                }
                else if(phonenumber.get(j).startsWith("6")){
                    String z ="+"+phonenumber.get(j);
                    phonenumber.set(j,z);
                }
            }

            System.out.println("phonenumber last"+phonenumber);
            Set<String> set = new HashSet<>(phonenumber);
            phonenumber.clear();
            phonenumber.addAll(set);

            System.out.println("remove duplicates "+phonenumber);
            phone.close();
    }

    public ArrayList<Friends>  removeDuplicates(ArrayList<Friends> list){
        Set<Friends> set = new TreeSet(new Comparator<Friends>() {

            @Override
            public int compare(Friends o1, Friends o2) {
                if(o1.getDisplayName().equalsIgnoreCase(o2.getDisplayName())){
                    return 0;
                }
                return 1;
            }
        });
      //  list.clear();
        set.addAll(list);

        System.out.println("\n***** After removing duplicates *******\n");
        System.out.println(mFriendlist);
        for (int i=0;i<list.size();i++){
            System.out.println("hello "+list.get(i).getDisplayName());
        }
        list = new ArrayList(set);
        return list;
    }

    private void filter(String text){
        ArrayList<Friends> filteredlist = new ArrayList<>();
        System.out.println("user ff punya "+mFriendlist.size());
        if(mFriendlist.size()!=0) {
            for (int i = 0; i < mFriendlist.size(); i++) {
                Log.e("list", "friend " + mFriendlist.get(i).getDisplayName());
            }
            for (Friends item : mFriendlist) {
                if (item.getDisplayName().toLowerCase().contains(text.toLowerCase())) {
                    filteredlist.add(item);
                }
            }
            addFriendAdapter.filterList(filteredlist);
        }
    }

    public void getContactPermission(){
        if(ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.READ_CONTACTS)) {
                    // Show our own UI to explain to the user why we need to read the contacts
                    // before actually requesting the permission and showing the default UI
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                        CONTACT_PERMISSION);
            }
        }
        else{
            getphoneandname();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CONTACT_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }



}