package com.app.pingchat.Friend;

import android.net.Uri;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;

public class Friends implements Serializable {
    public String displayName;
    public String phone;
    public String avatarURL;
    public String userAbout;
    public String userID;
    public String deviceToken;

    public String sender;
    private String lastMessage;
    private long messageDate;
    private String date;
    FirebaseStorage storage;
    StorageReference storeRef;
    public Friends(){}

    public Friends(String sender,String displayName, String phone, String avatarURL, String userAbout,String userID,String lastMessage, long messageDate,String date) {
        this.displayName = displayName;
        this.phone = phone;
        this.avatarURL = avatarURL;
        this.userAbout = userAbout;
        this.userID = userID;
        this.lastMessage = lastMessage;
        this.messageDate = messageDate;
        this.date = date;
        this.sender = sender;
    }

    public String getSender() { return sender; }

    public void setSender(String sender) { this.sender = sender; }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String message) {
        this.lastMessage = message;
    }

    public long getMessageDate() {
        return messageDate;
    }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) { this.avatarURL = avatarURL; }

    public String getUserAbout() {
        return userAbout;
    }

    public void setUserAbout(String userAbout) {
        this.userAbout = userAbout;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDeviceToken() { return deviceToken; }

    public void setDeviceToken(String deviceToken) { this.deviceToken = deviceToken; }
}
