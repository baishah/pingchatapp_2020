package com.app.pingchat.Friend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.GlideApp;
import com.app.pingchat.Ping.UP_DetailsClass;
import com.app.pingchat.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendSelectorAdapter extends RecyclerView.Adapter<FriendSelectorAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference metadata;
    Context context;
    ArrayList<userDetailsClass> UD_DetailsClassList;
    friendselrecycleradapterlist flistlistener;
    String currentuser;


    public FriendSelectorAdapter(ArrayList<userDetailsClass> pingMetaData){
        UD_DetailsClassList = pingMetaData;
    }

    public FriendSelectorAdapter(Context c, ArrayList<userDetailsClass> d, String user,friendselrecycleradapterlist clicklistener){
        context = c;
        UD_DetailsClassList = d;
        currentuser = user;
        flistlistener = clicklistener;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.friend_selector_layout,parent,false);
        return new MyViewHolder(v,flistlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final userDetailsClass friends = UD_DetailsClassList.get(position);

        holder.name.setText(friends.getUD_displayName());
        if(friends.getUD_avatarURL().startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(friends.getUD_avatarURL())
                    .into(holder.i);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(friends.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }
    }

    @Override
    public int getItemCount() {
        return UD_DetailsClassList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CircleImageView i;
        TextView name;
        friendselrecycleradapterlist flistlistener;
        public MyViewHolder(View itemView, friendselrecycleradapterlist clicklistener) {
            super(itemView);
            i = itemView.findViewById(R.id.user_dp);
            name = itemView.findViewById(R.id.display_name);
            flistlistener = clicklistener;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            flistlistener.onUserClickedfriendlist(getAdapterPosition());
        }
    }

    public interface friendselrecycleradapterlist {
        void onUserClickedfriendlist(int position);
    }

    public void filterlist(ArrayList<userDetailsClass> filteredlist){
        UD_DetailsClassList = filteredlist;
        notifyDataSetChanged();
    }
}