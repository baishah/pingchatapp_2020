package com.app.pingchat.Friend;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.app.pingchat.R;
import com.app.pingchat.Main.TabAccessorAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class AddfriendlistActivity extends AppCompatActivity {
    ListView l1;
    private static final int CONTACT_PERMISSION = 1;
    private DatabaseReference usersRef,chatReq;
    private RecyclerView FindFriendRecyclerList;
    private  Button addfriend;
    String add_friend;
    private FirebaseAuth mAuths;
    public String id,uid,True,Received,Sent;
    Button gofragment;
    ArrayList<String> yuyu;
    private LinkedHashMap<String,String> linkedHashMap = new LinkedHashMap<>();
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabAccessorAdapter tabAccessorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addfriendlist_activity);

        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tab_layout);

        setupViewPager(viewPager);
        tabAccessorAdapter = new TabAccessorAdapter(getSupportFragmentManager());

//        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(viewPager);
        mAuths = FirebaseAuth.getInstance();
        id = mAuths.getCurrentUser().getUid();
        usersRef = FirebaseDatabase.getInstance().getReference().child("registeredUser");
        usersRef.keepSynced(true);
        chatReq = FirebaseDatabase.getInstance().getReference().child("Users");
        chatReq.keepSynced(true);
        True = "true";
        Received = "Received";
        Sent ="sent";

    }

    public void get(){
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
        startManagingCursor(cursor);

        String[] from = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone._ID};

      //  String ContactDisplayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))zz
        int[] to = {R.id.line1,R.id.line2};

        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this,R.layout.list_item_card,cursor,from,to){
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the Item from ListView
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView tv = (TextView) view.findViewById(R.id.line1);
                TextView tv2 = (TextView) view.findViewById(R.id.line2);
              //  ImageButton gambar = (ImageButton) view.findViewById(R.id.gambar);
                return view;
            }
        };
        l1.setAdapter(simpleCursorAdapter);
        l1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

    private void setupViewPager(ViewPager viewPager){
        TabAccessorAdapter adapter = new TabAccessorAdapter(getSupportFragmentManager());
        adapter.addFragment(new AddNewFriendFragment(), "Find Friend");
        adapter.addFragment(new FriendRequestFragment(), "Received");
        adapter.addFragment(new FriendRequestSentFragment(), "Sent");
        viewPager.setAdapter(adapter);
    }
    public void getphoneandname(){
        Cursor phone  = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
        while(phone.moveToNext()){
            String name =phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            linkedHashMap.put(name,phoneNumber);
        }
    }

    @Override
    public void onBackPressed(){
        finish();
    }

}
