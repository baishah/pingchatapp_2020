package com.app.pingchat.Friend;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.app.pingchat.R;
import com.app.pingchat.Main.TabAccessorAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ContactsFragment extends AppCompatActivity {

    FirebaseStorage storage;
    StorageReference storeRef;

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabAccessorAdapter tabAccessorAdapter;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_fragment_layout);

        getContactPermission();
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.pager);
        setupViewPager(viewPager);
        tabAccessorAdapter = new TabAccessorAdapter(getSupportFragmentManager());

        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
        tabLayout.setTabTextColors(Color.parseColor("#ffffff"),Color.parseColor("#ffffff"));
        tabLayout.setupWithViewPager(viewPager);

        if(getIntent().getExtras()!=null){
            String noti = getIntent().getExtras().getString("fromnoti");
            if((noti.equals("openfromnotiqr"))||(noti.equals("openfromnoti"))){
                viewPager.setCurrentItem(1);
            }
            else{
                viewPager.setCurrentItem(0);
            }
        }

    }

    private void setupViewPager(ViewPager viewPager){
        TabAccessorAdapter adapter = new TabAccessorAdapter(getSupportFragmentManager());
        adapter.addFragment(new FriendsFragment(), "Contacts");
        adapter.addFragment(new FriendRequestFragment(), "Friend Request");
        adapter.addFragment(new AddNewFriendFragment(), "Add Friend");
        viewPager.setAdapter(adapter);
    }

    public void getContactPermission(){
        if(ContextCompat.checkSelfPermission(ContactsFragment.this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.READ_CONTACTS)) {
                    // Show our own UI to explain to the user why we need to read the contacts
                    // before actually requesting the permission and showing the default UI
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                        1);
            }
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

}
