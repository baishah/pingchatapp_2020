package com.app.pingchat.Friend;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,notification;
    Context context;
    ArrayList<Friends> mFriendlist;
    private FirebaseAuth mAuths;
    private ArrayList<String> filteruserlist;

    public ContactsAdapter(ArrayList<Friends> Friendlist){
        mFriendlist =Friendlist;
    }

    public ContactsAdapter(Context c, ArrayList<Friends> f){
        context = c;
        mFriendlist = f;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_card,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        filteruserlist = new ArrayList<>();

        Friends mynewfriend = mFriendlist.get(position);
        holder.u.setText(mynewfriend.getDisplayName());
        holder.s.setText(mynewfriend.getUserAbout());
        holder.p.setText(mynewfriend.getPhone());
        holder.onClick(mynewfriend.getUserID());
        storeRef.child(mynewfriend.getAvatarURL()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String dapat = uri.toString();
                System.out.println(dapat);
                Picasso.with(context.getApplicationContext()).load(dapat).into(holder.i);
            }
       });
    }

    @Override
    public int getItemCount() {
        return mFriendlist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s,p;
        CircleImageView i;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            p = itemView.findViewById(R.id.line3);
            i = itemView.findViewById(R.id.gambar);
            add = itemView.findViewById(R.id.add);
        }

        public void onClick(final String friendid){
            mAuths = FirebaseAuth.getInstance();
            final String currentuser = mAuths.getCurrentUser().getUid();
            usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
            notification = FirebaseDatabase.getInstance().getReference().child("Notification").child("FriendRequest");
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("user id "+currentuser);
                    System.out.println("friend id "+friendid);
                    usersRef.child(currentuser).child("Friends").child(friendid).setValue("sending").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                removeItem(itemView);
                                HashMap<String,String> chatnotification = new HashMap<>();
                                chatnotification.put("from",currentuser);
                                chatnotification.put("type","request");
                                usersRef.child(currentuser).child("Friends").child(friendid).setValue("sent");
                                usersRef.child(friendid).child("Friends").child(currentuser).setValue("received");
                            }
                        }
                    });
                }
            });
        }
    }

    public void removeItem(final View v) {
        v.animate()
                .translationY(0)
                .setDuration(350)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        int removeIndex = 0;
                        mFriendlist.remove(removeIndex);
                        notifyItemRemoved(removeIndex);
                        notifyItemRangeChanged(removeIndex,getItemCount());
                        notifyDataSetChanged();
                    }
                });
    }

    public void filterList(ArrayList<Friends> filteredlist){
        mFriendlist = filteredlist;
        notifyDataSetChanged();
    }
}
