package com.app.pingchat.Friend;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.app.pingchat.Profile.ViewUserProfileActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.lang.reflect.Array;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,notification, users,friend;
    Context context;
    ArrayList<userDetailsClass> mContactslist;
    Array datas;
    Friends[] baru;
    //private List<Friends> mlist;
    Friends[] mlist;

    private FirebaseAuth mAuths;

    public FriendsAdapter(ArrayList<userDetailsClass> Friendlist){
        mContactslist = Friendlist;
    }

    public FriendsAdapter(Context c, ArrayList<userDetailsClass> f){
        context = c;
        mContactslist = f;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.friends_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        users = FirebaseDatabase.getInstance().getReference().child("Users");
        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();

        final String currentuser = mAuth.getCurrentUser().getUid();

        friend = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userFriends");
        friend.keepSynced(true);

        final userDetailsClass mynewfriend = mContactslist.get(position);
        holder.u.setText(mynewfriend.getUD_displayName());
        holder.s.setText(mynewfriend.getUD_aboutMe());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                users.child(currentuser).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            String myFriend = mynewfriend.getUD_userID();
                            String status = dataSnapshot.child(myFriend).getValue(String.class);
                            if (status.equals("friend")) {
                                Intent i = new Intent(context.getApplicationContext(), ViewUserProfileActivity.class);
                                i.putExtra("id",mynewfriend.getUD_userID());
                                context.startActivity(i);
                            }
                            else if(status.equals("block"))
                            {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                final AlertDialog alertDialog = builder.create();

                                View view = LayoutInflater.from(context).inflate(R.layout.unfriends_popup_layout,null);

                                TextView friendname = view.findViewById(R.id.friend_name);
                                TextView cancelBtn = view.findViewById(R.id.cancel);
                                TextView unfriendBtn = view.findViewById(R.id.unfriend_txt);
                                CircleImageView avatarimg = view.findViewById(R.id.pro_pic);
                                TextView block = view.findViewById(R.id.un_friend);

                                block.setText("Unblock");
                                unfriendBtn.setText("Unblock");

                                users.child(myFriend).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                        if(dataSnapshot.hasChild("UD_displayName")){
                                            String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                            friendname.setText(name);
                                            System.out.println("name :"+name);
                                        }

                                        if(dataSnapshot.hasChild("UD_avatarURL")){
                                            String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                                            System.out.println("url :"+ avatar);
                                            if(avatar.startsWith("p")){
                                                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                                GlideApp.with(context).load(storageReference).placeholder(R.drawable.loading_img).into(avatarimg);
                                            }
                                            else if(avatar.startsWith("h")){
                                                GlideApp.with(context).load(avatar).placeholder(R.drawable.loading_img).into(avatarimg);
                                            }
                                        }

                                        cancelBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertDialog.dismiss();
                                            }
                                        });

                                        unfriendBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                alertDialog.dismiss();
//                                                confirmUnblock(myFriend,currentuser);
                                                ProgressDialog dialog= new ProgressDialog(context);
                                                dialog.setMessage("Unblock friend.. Please wait");
                                                dialog.setCancelable(false);
                                                dialog.setCanceledOnTouchOutside(false);
                                                dialog.show();

                                                friend.child(myFriend).setValue("friend").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        users.child(myFriend).child("userFriends").child(currentuser).setValue("friend").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                dialog.dismiss();
                                                                Toast.makeText(context, "Unblock successfully", Toast.LENGTH_SHORT).show();
                                                                //                        startActivity(new Intent(ViewUserProfileActivity.this, ViewProfileActivity.class));
                                                            }
                                                        });
                                                    }
                                                });

                                            }
                                        });

                                    }
                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                                alertDialog.setView(view);
                                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                alertDialog.show();
                                alertDialog.setCanceledOnTouchOutside(true);
                                return;
                            }
                            else if(status.equals("blocked")){
                                Toast.makeText(context, "You had been blocked by this user", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });

        if(mynewfriend.getUD_avatarURL().startsWith("p")){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(mynewfriend.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }
        else{
            GlideApp.with(context.getApplicationContext())
                    .load(mynewfriend.getUD_avatarURL())
                    .into(holder.i);
        }
    }


    @Override
    public int getItemCount() {
        return mContactslist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s,p;
        CircleImageView i;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            i = itemView.findViewById(R.id.gambar);

        }
    }
    public void filterList(ArrayList<userDetailsClass> filteredlist){
        mContactslist = filteredlist;
        notifyDataSetChanged();
    }
}
