package com.app.pingchat.Friend;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class contacts {

    public String displayName, phone,avatarURL,userAbout,userID,lastMessage;
    public boolean isSelected = false;

    public contacts(){}

    public contacts(String displayName, String phone, String avatarURL, String userAbout,String userID,String lastMessage) {
        this.displayName = displayName;
        this.phone = phone;
        this.avatarURL = avatarURL;
        this.userAbout = userAbout;
        this.userID = userID;
        this.lastMessage = lastMessage;
    }

    public String getName() {
        return displayName;
    }

    public void setName(String name) {
        this.displayName = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() { return avatarURL; }

    public void setImage(String image) { this.avatarURL = image; }

    public String getStatus() {
        return userAbout;
    }

    public void setStatus(String status) {
        this.userAbout = status;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String id) {
        this.userID = id;
    }

    public void setSelected(boolean selected){
        isSelected = selected;
    }

    public boolean isSelected(){
        return isSelected;
    }
}
