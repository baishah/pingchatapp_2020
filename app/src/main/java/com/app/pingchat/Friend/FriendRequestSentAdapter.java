package com.app.pingchat.Friend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendRequestSentAdapter extends RecyclerView.Adapter<FriendRequestSentAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,notification;
    Context context;
    ArrayList<Friends> mFriendRequestSent;
    private FirebaseAuth mAuths;
    private ArrayList<String> filteruserlist;

    public FriendRequestSentAdapter(ArrayList<Friends> Friendlist){
        mFriendRequestSent =Friendlist;
    }

    public FriendRequestSentAdapter(Context c, ArrayList<Friends> f){
        context = c;
        mFriendRequestSent = f;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.send_request_card,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        filteruserlist = new ArrayList<>();

        Friends mynewfriend = mFriendRequestSent.get(position);
        final String friendid = mynewfriend.getUserID();
        holder.u.setText(mynewfriend.getDisplayName());
        holder.s.setText(mynewfriend.getUserAbout());
        holder.onClick(mynewfriend.getUserID());
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(mynewfriend.getAvatarURL());
        GlideApp.with(context.getApplicationContext())
                .load(storageReference)
                .into(holder.i);
        //Picasso.with(context.getApplicationContext()).load(mynewfriend.getAvatarURL()).into(holder.i);

    }

    @Override
    public int getItemCount() {
        return mFriendRequestSent.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s;
        CircleImageView i;
        Button cancel;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            i = itemView.findViewById(R.id.gambar);
            cancel = itemView.findViewById(R.id.cancelrequest);
        }

        public void onClick(final String friendid){
            mAuths = FirebaseAuth.getInstance();
            final String currentuser = mAuths.getCurrentUser().getUid();
            usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
            notification = FirebaseDatabase.getInstance().getReference().child("Notification").child("FriendRequest");

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(itemView);
                    usersRef.child(currentuser).child("Friends").child(friendid).removeValue();
                    usersRef.child(friendid).child("Friends").child(currentuser).removeValue();
                }
            });
        }
    }

    public void removeItem(final View v) {
        Animation.AnimationListener al = new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                notifyDataSetChanged();
            }
            @Override public void onAnimationRepeat(Animation arg0) {}
            @Override public void onAnimationStart(Animation arg0) {}
        };
        collapse(v, al);
    }

    private void collapse(final View v, Animation.AnimationListener animation) {
        final int initialHeight = v.getHeight();
        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                }
                else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        if (animation!=null) {
            anim.setAnimationListener(animation);
        }
        anim.setDuration(250);
        v.startAnimation(anim);
    }

    public void filterList(ArrayList<Friends> filteredlist){
        mFriendRequestSent = filteredlist;
        notifyDataSetChanged();
    }

    public ArrayList<Friends> getmFriendRequestSent() {
        return mFriendRequestSent;
    }
}