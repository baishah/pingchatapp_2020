package com.app.pingchat.Friend;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.appinvite.AppInviteInvitation;

public class InviteFriendActivity extends AppCompatActivity {

    private static final String TAG = "InviteFriendActivity";
    private static final int REQUEST_INVITE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String phone = getIntent().getStringExtra("phone");


        Uri uri = Uri.parse("smsto: " + phone);
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", "Hi, check out PingChat App, I use it to message and stay connected with the people I care about. Get it for free at https://pingchat.app/Download/");
        startActivityForResult(it, REQUEST_INVITE);
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ",resultCode+" + resultCode);
        if(requestCode == REQUEST_INVITE){
            if(resultCode == RESULT_OK){
                String[] ok = AppInviteInvitation.getInvitationIds(requestCode, data);
                for(String id : ok){
                    Log.d(TAG, "onActivityResult: sent invitation" +id);
                }
            }else {
                Toast.makeText(InviteFriendActivity.this, "Failed", Toast.LENGTH_LONG).show();
            }
        }
    }

}