package com.app.pingchat.Friend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.lang.reflect.Field;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddFriendAdapter extends RecyclerView.Adapter<AddFriendAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,notification;
    Context context;
    ArrayList<Friends> mFriendlist;
    private FirebaseAuth mAuths;
    String currentuser;
    private ArrayList<String> filteruserlist;

    public ArrayList<String> getUserf() {
        return userf;
    }

    public void setUserf(ArrayList<String> userf) {
        this.userf = userf;
    }

    private ArrayList<String> userf;
    PopupFriendAdapter popupFriendAdapter;

    public AddFriendAdapter(ArrayList<String> usersss,Context c){
        userf =usersss;
        context = c;
    }

    public AddFriendAdapter(Context c, ArrayList<Friends> f,ArrayList z){
        context = c;
        mFriendlist = f;
        userf = z;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_card,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        TypedArray images;
        int choice;
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        filteruserlist = new ArrayList<>();
        mAuths = FirebaseAuth.getInstance();
        currentuser = mAuths.getCurrentUser().getUid();

        Friends mynewfriend = mFriendlist.get(position);
        holder.u.setText(mynewfriend.getDisplayName());
        holder.p.setText(mynewfriend.getPhone());
        holder.onClick(mynewfriend.getPhone(),mynewfriend.getDisplayName());
    }

    @Override
    public int getItemCount() {
        return mFriendlist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s,p;
        CircleImageView i;
        Button invite;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            p = itemView.findViewById(R.id.line3);
            invite = itemView.findViewById(R.id.add);
        }

        public void onClick(final String phone,final String name){
            invite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkuser(phone,name);
                }
            });
        }

        private void popupfriend(ArrayList b){
            final ArrayList<userDetailsClass> yuyu = new ArrayList<>();
            usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
            System.out.println("user berkaitan "+b);
            System.out.println("user friend "+getUserf());
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            final AlertDialog alertDialog = builder.create();

            View view = LayoutInflater.from(context).inflate(R.layout.add_friend_popup,null);
            final RecyclerView users = view.findViewById(R.id.list_of_user);
            users.setLayoutManager(new LinearLayoutManager(context));

            for(int i=0;i<b.size();i++){
                usersRef.child(b.get(i).toString()).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userDetailsClass f = dataSnapshot.getValue(userDetailsClass.class);
                        final userDetailsClass user = new userDetailsClass();

                        final String displayname = f.getUD_displayName();
                        final String phonenumber = f.getUD_phoneNo();
                        final String userabout = f.getUD_aboutMe();
                        final String avatar = f.getUD_avatarURL();
                        final String id = f.getUD_userID();

                            System.out.println("displaynamesss"+displayname);
                            user.setUD_displayName(displayname);
                            user.setUD_phoneNo(phonenumber);
                            user.setUD_aboutMe(userabout);
                            user.setUD_avatarURL(avatar);
                            user.setUD_userID(id);
                            yuyu.add(user);

                            popupFriendAdapter = new PopupFriendAdapter(context,yuyu);
                            users.setAdapter(popupFriendAdapter);

                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }
            alertDialog.setView(view);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();

            alertDialog.setCanceledOnTouchOutside(true);
            return;
        }

        private void checkuser(final String phone,final String name){
            final ArrayList a = new ArrayList();
            final ArrayList b = new ArrayList();
            DatabaseReference userdb;
            userdb = FirebaseDatabase.getInstance().getReference().child("Users");

            userdb.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        //check sini
                        System.out.println("masuk sisni");
                      //  String phonenumber = dataSnapshot1.child("metaData").child("phone").getValue().toString();
                        DataSnapshot sn = dataSnapshot1.child("userDetails").child("UD_phoneNo");

                        if(sn.exists()){
                            String phonenumber = dataSnapshot1.child("userDetails").child("UD_phoneNo").getValue().toString();
                            String uid = dataSnapshot1.child("userDetails").child("UD_userID").getValue().toString();

                            if(phonenumber == null && uid == null){
                                System.out.println("details x lengkap");
                                } else if (phonenumber.equals("") && uid.equals("")) {
                                System.out.println("details x lengkap");
//                                 userdb.child(uid).child("userDetails").child("UD_phoneNo").setValue("+60100000000");
                                } else {
                                if (phonenumber.substring(0, 1).equals("0")) {
                                    phonenumber = "+6" + phonenumber;
                                    a.add(phonenumber);
                                } else if (phonenumber.substring(0, 1).equals("6")) {
                                    phonenumber = "+" + phonenumber;
                                    a.add(phonenumber);
                                } else {
                                    a.add(phonenumber);
                                }

                                if (phone.equals(phonenumber)) {
                                    b.add(uid);
                                }
                            }
                        }
                        else{
                            System.out.println("takde");
                        }
                    }
                    if(a.contains(phone)){
                        if(!b.contains(currentuser)){
                            System.out.println("currentuser "+currentuser +" userid "+b);
                            System.out.println("phone position "+phone);
                            popupfriend(b);
                        }
                        else{
                            Toast.makeText(context, "This is apparently your phone number", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                   // popupnootfriend(phone,name);
                    System.out.println("not founds");
                    Intent intent = new Intent(context, InviteFriendActivity.class);
                    intent.putExtra("phone",phone);
                    context.startActivity(intent);
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
        }
    }

    public void removeItem(final View v) {
        Animation.AnimationListener al = new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                notifyDataSetChanged();
            }
            @Override public void onAnimationRepeat(Animation arg0) {}
            @Override public void onAnimationStart(Animation arg0) {}
        };

        collapse(v, al);
    }

    private void collapse(final View v, Animation.AnimationListener animation) {

        final int initialHeight = v.getHeight();
        Animation anim = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                }
                else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        if (animation!=null) {
            anim.setAnimationListener(animation);
        }
        anim.setDuration(250);
        v.startAnimation(anim);
    }

    public void filterList(ArrayList<Friends> filteredlist){
        mFriendlist = filteredlist;
        notifyDataSetChanged();
    }

    public ArrayList<Friends> getmFriendlist() {
        return mFriendlist;
    }

    private void popupnootfriend(String phone,String name){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog alertDialog = builder.create();

        View view = LayoutInflater.from(context).inflate(R.layout.not_friend_popup,null);

        TextView names = view.findViewById(R.id.name);

        names.setText(name);

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        alertDialog.setCanceledOnTouchOutside(true);
        return;
    }
}