package com.app.pingchat.Friend;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.R;
import com.app.pingchat.Request;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class FriendRequestSentFragment extends Fragment {
    FirebaseAuth mAuth;
    Button accept,reject;
    ImageButton friend;
    public String current_id;
    private DatabaseReference friendrequest,users;

    String p,key;
    Request request;

    ArrayList<Friends> mFriendRequestSent;
    FriendRequestSentAdapter friendRequestAdapter;

    private RecyclerView mrequestlist;
    TextView nofriend;

    FirebaseStorage storage;
    StorageReference storeRef;



    String testest;

    Object s;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View requestfriendview = inflater.inflate(R.layout.friendrequestsentfragment, container, false);

        mrequestlist = (RecyclerView) requestfriendview.findViewById(R.id.friend_sent);
        mrequestlist.setLayoutManager(new LinearLayoutManager(getContext()));

        mFriendRequestSent = new ArrayList<Friends>();

        nofriend = requestfriendview.findViewById(R.id.norequestsent);
        mAuth = FirebaseAuth.getInstance();
        current_id = mAuth.getCurrentUser().getUid();
        friendrequest = FirebaseDatabase.getInstance().getReference().child("Users").child(current_id).child("Friends");
        users = FirebaseDatabase.getInstance().getReference().child("Users");
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        friendrequestsent();
        return requestfriendview;
    }

    private void friendrequestsent(){
        friendrequest.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                for (final DataSnapshot projectsnapshot:dataSnapshot.getChildren()){
                    final Friends requestfriend = new Friends();
                    key = projectsnapshot.getKey();
                    System.out.println(key);
                    s = projectsnapshot.getValue();
                    if(s.equals("sent")){
                        users.child(key).child("metaData").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                                Friends f = dataSnapshot1.getValue(Friends.class);

                                final String displayname = f.getDisplayName();
                                final String status = f.getUserAbout();
                                final String avatar = f.getAvatarURL();
                                final String userid = f.getUserID();

                                    requestfriend.setDisplayName(displayname);
                                    requestfriend.setUserAbout(status);
                                    requestfriend.setAvatarURL(avatar);
                                    requestfriend.setUserID(userid);
                                    mFriendRequestSent.add(requestfriend);
                                    friendRequestAdapter = new FriendRequestSentAdapter(getContext(),mFriendRequestSent);
                                    mrequestlist.setAdapter(friendRequestAdapter);

                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }

                }

                //checkArray(friendRequestAdapter.getmFriendRequestSent());

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void checkArray(ArrayList<Friends>array){
        if(array.size()==0){
            nofriend.setVisibility(View.VISIBLE);
            System.out.println("array check array 0");
        }
        else{
            nofriend.setVisibility(View.INVISIBLE);
            System.out.println("array check array ada isi");
        }
    }
}