package com.app.pingchat.Friend;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Friend.FriendsAdapter;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FriendsFragment extends Fragment {

    FirebaseAuth mAuth;
    private DatabaseReference friends,users,userFriends;
    private ListView listView;
    private String current_id;
    private RecyclerView friendList;
    private EditText search_friend;

    ArrayList<userDetailsClass> mContactslist;
    FriendsAdapter friendsAdapter;

    ArrayList<String> friendskey;
    ArrayList<String> alluserid;

    FirebaseStorage storage;
    StorageReference storeRef;

    String av;
    ImageView nofriend;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.friends_fragment,container,false);

        //ImageButton contact = (ImageButton) view.findViewById(R.id.contacts);
        search_friend = view.findViewById(R.id.search_friends);
        nofriend = view.findViewById(R.id.nocontacts);

        view.findViewById(R.id.layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(getActivity().getCurrentFocus()!=null){
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
                }
                return false;
            }
        });
        view.findViewById(R.id.friendlistR).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(getActivity().getCurrentFocus()!=null){
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
                }
                return false;
            }
        });

        friendList = view.findViewById(R.id.friendlistR);
        friendList.setLayoutManager(new LinearLayoutManager(getContext()));

        mContactslist = new ArrayList<>();

        search_friend.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser()!=null) {
            current_id = mAuth.getCurrentUser().getUid();
            friends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_id).child("userFriends");
            friends.keepSynced(true);
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            storage = FirebaseStorage.getInstance();
            storeRef = storage.getReference();
            displayfriend();
            //loadalreadyfriend();
            //loadalluser();
        }
        return view;
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    private void filter(String text){
        ArrayList<userDetailsClass> filteredlist = new ArrayList<>();
        if(mContactslist.size()!=0){
            for(int i=0;i<mContactslist.size();i++)
            {
                Log.e("list","friend "+mContactslist.get(i).getUD_displayName() );
            }

            for (userDetailsClass item:mContactslist){
                if(item.getUD_displayName().toLowerCase().contains(text.toLowerCase())){
                    filteredlist.add(item);
                }
            }
            friendsAdapter.filterList(filteredlist);
        }
        else{
            System.out.println("kenapa ada sini?" );
        }
    }

    public void displayfriend(){
        ProgressDialog dialog= new ProgressDialog(getContext());
        dialog.setMessage("Loading contacts..");
        dialog.show();
        mContactslist = new ArrayList<>();
        ArrayList checkstatus = new ArrayList();
        friends.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    final userDetailsClass userDetailsClass = new userDetailsClass();
                    String status = dataSnapshot1.getValue().toString();
                    String id = dataSnapshot1.getKey();
                    checkstatus.add(status);
                    if(status.equals("friend")){
                        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                userDetailsClass udc = dataSnapshot.getValue(userDetailsClass.class);
                                userDetailsClass.setUD_userID(udc.getUD_userID());
                                userDetailsClass.setUD_avatarURL(udc.getUD_avatarURL());
                                userDetailsClass.setUD_aboutMe(udc.getUD_aboutMe());
                                userDetailsClass.setUD_phoneNo(udc.getUD_phoneNo());
                                userDetailsClass.setUD_displayName(udc.getUD_displayName());
                                userDetailsClass.setUD_birthday(udc.getUD_birthday());
                                userDetailsClass.setUD_email(udc.getUD_email());
                                mContactslist.add(userDetailsClass);
                                friendsAdapter = new FriendsAdapter(getContext(),mContactslist);
                                friendList.setAdapter(friendsAdapter);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }else if(status.equals("block")){
                        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                userDetailsClass udc = dataSnapshot.getValue(userDetailsClass.class);
                                userDetailsClass.setUD_userID(udc.getUD_userID());
                                userDetailsClass.setUD_avatarURL(udc.getUD_avatarURL());
                                userDetailsClass.setUD_aboutMe(udc.getUD_aboutMe());
                                userDetailsClass.setUD_phoneNo(udc.getUD_phoneNo());
                                userDetailsClass.setUD_displayName(udc.getUD_displayName());
                                userDetailsClass.setUD_birthday(udc.getUD_birthday());
                                userDetailsClass.setUD_email(udc.getUD_email());
                                mContactslist.add(userDetailsClass);
                                friendsAdapter = new FriendsAdapter(getContext(),mContactslist);
                                friendList.setAdapter(friendsAdapter);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }else if(status.equals("blocked")){

                        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                userDetailsClass udc = dataSnapshot.getValue(userDetailsClass.class);
                                userDetailsClass.setUD_userID(udc.getUD_userID());
                                userDetailsClass.setUD_avatarURL(udc.getUD_avatarURL());
                                userDetailsClass.setUD_aboutMe(udc.getUD_aboutMe());
                                userDetailsClass.setUD_phoneNo(udc.getUD_phoneNo());
                                userDetailsClass.setUD_displayName(udc.getUD_displayName());
                                userDetailsClass.setUD_birthday(udc.getUD_birthday());
                                userDetailsClass.setUD_email(udc.getUD_email());
                                mContactslist.add(userDetailsClass);
                                friendsAdapter = new FriendsAdapter(getContext(),mContactslist);
                                friendList.setAdapter(friendsAdapter);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }
                }
                if(!checkstatus.contains("friend")){
                    dialog.dismiss();
                    nofriend.setVisibility(View.VISIBLE);
                }
                else{
                    dialog.dismiss();
                    nofriend.setVisibility(View.GONE);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void loadalreadyfriend(){
        friendskey = new ArrayList<>();
        users.child(current_id).child("Friends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    final String friendstatus = dataSnapshot1.getValue().toString();
                    final String friendid = dataSnapshot1.getKey();
                    friendskey.add(friendid);
                    System.out.println("friend status ,friendid"+friendstatus+friendid);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }
    public void loadalluser(){
        alluserid = new ArrayList<>();
        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    final String userid = dataSnapshot1.getKey().toString();
                    alluserid.add(userid);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
