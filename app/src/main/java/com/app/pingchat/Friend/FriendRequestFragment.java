package com.app.pingchat.Friend;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.R;
import com.app.pingchat.Request;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FriendRequestFragment extends Fragment {
    FirebaseAuth mAuth;
    Button accept,reject;
    ImageButton refresh;
    public String current_id;
    private DatabaseReference friendrequest,users;

    String p,key;
    Request request;

    ArrayList<userDetailsClass> mFriendRequest;
    FriendRequestAdapter friendRequestAdapter;
    ImageView nofriend;
    private RecyclerView mrequestlist;

    FirebaseStorage storage;
    StorageReference storeRef;

    EditText search_friends;

    Object s;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View requestfriendview = inflater.inflate(R.layout.new_friend_request_friend,container,false);
        refresh = requestfriendview.findViewById(R.id.refresh);
        nofriend = requestfriendview.findViewById(R.id.norequestfriend);

        mrequestlist = (RecyclerView) requestfriendview.findViewById(R.id.friend_request);
        mrequestlist.setLayoutManager(new LinearLayoutManager(getContext()));
        mFriendRequest = new ArrayList<>();
        search_friends = requestfriendview.findViewById(R.id.search_friends);

        requestfriendview.findViewById(R.id.friend_request).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(getActivity().getCurrentFocus()!=null){
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
                }
                return false;
            }
        });


        search_friends.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });

        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser()!=null){
            mAuth = FirebaseAuth.getInstance();
            current_id = mAuth.getCurrentUser().getUid();
            friendrequest = FirebaseDatabase.getInstance().getReference().child("Users").child(current_id).child("userFriends");
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            newfriendrequest2();

            refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newfriendrequest2();
                }
            });
        }

        return requestfriendview;
    }

    public void onStart(){
        super.onStart();

    }

    private void newfriendrequest2(){
        ProgressDialog dialog= new ProgressDialog(getContext());
        dialog.setMessage("Loading request..");
        dialog.show();
        mFriendRequest = new ArrayList<>();
        ArrayList checkstatus = new ArrayList();
        friendrequest.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                for (final DataSnapshot projectsnapshot:dataSnapshot.getChildren()){
                    final userDetailsClass userDetailsClass = new userDetailsClass();
                    String status = projectsnapshot.getValue().toString();
                    String id = projectsnapshot.getKey();
                    checkstatus.add(status);
                    if(status.equals("received")){
                        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                userDetailsClass udc = dataSnapshot.getValue(userDetailsClass.class);
                                userDetailsClass.setUD_userID(udc.getUD_userID());
                                userDetailsClass.setUD_avatarURL(udc.getUD_avatarURL());
                                userDetailsClass.setUD_aboutMe(udc.getUD_aboutMe());
                                userDetailsClass.setUD_phoneNo(udc.getUD_phoneNo());
                                userDetailsClass.setUD_displayName(udc.getUD_displayName());
                                userDetailsClass.setUD_birthday(udc.getUD_birthday());
                                userDetailsClass.setUD_email(udc.getUD_email());
                                mFriendRequest.add(userDetailsClass);

                                friendRequestAdapter = new FriendRequestAdapter(getContext(),mFriendRequest);
                                mrequestlist.setAdapter(friendRequestAdapter);

                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }
                }
                if(!checkstatus.contains("received")){
                    dialog.dismiss();
                    nofriend.setVisibility(View.VISIBLE);
                }
                else{
                    dialog.dismiss();
                    nofriend.setVisibility(View.GONE);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }


    public void checkArray(ArrayList<Friends> array){
        if(array.size()==0){
           nofriend.setVisibility(View.VISIBLE);
        }
        else{
            nofriend.setVisibility(View.INVISIBLE);
        }
    }
}