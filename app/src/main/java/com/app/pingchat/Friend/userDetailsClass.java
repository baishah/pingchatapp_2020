package com.app.pingchat.Friend;

import java.io.Serializable;

public class userDetailsClass implements Serializable {
    private String UD_avatarURL,UD_aboutMe,UD_birthday,UD_displayName,UD_email,UD_phoneNo,UD_userID,
            UD_username,UD_signInType,CG_Role;
    private long UD_registeredOn;

    public userDetailsClass(String CG_Role,String UD_avatarURL, String UD_aboutMe, String UD_birthday, String UD_displayName, String UD_email, String UD_phoneNo, String UD_userID, String UD_username, String UD_signInType, long UD_registeredOn) {
        this.UD_avatarURL = UD_avatarURL;
        this.UD_aboutMe = UD_aboutMe;
        this.UD_birthday = UD_birthday;
        this.UD_displayName = UD_displayName;
        this.UD_email = UD_email;
        this.UD_phoneNo = UD_phoneNo;
        this.UD_userID = UD_userID;
        this.UD_username = UD_username;
        this.UD_signInType = UD_signInType;
        this.UD_registeredOn = UD_registeredOn;
        this.CG_Role = CG_Role;
    }

    public  userDetailsClass(){}

    public String getUD_avatarURL() {
        return UD_avatarURL;
    }

    public void setUD_avatarURL(String UD_avatarURL) {
        this.UD_avatarURL = UD_avatarURL;
    }

    public String getUD_aboutMe() {
        return UD_aboutMe;
    }

    public void setUD_aboutMe(String UD_aboutMe) {
        this.UD_aboutMe = UD_aboutMe;
    }

    public String getUD_birthday() {
        return UD_birthday;
    }

    public void setUD_birthday(String UD_birthday) {
        this.UD_birthday = UD_birthday;
    }

    public String getUD_displayName() {
        return UD_displayName;
    }

    public void setUD_displayName(String UD_displayName) {
        this.UD_displayName = UD_displayName;
    }

    public String getUD_email() {
        return UD_email;
    }

    public void setUD_email(String UD_email) {
        this.UD_email = UD_email;
    }

    public String getUD_phoneNo() {
        return UD_phoneNo;
    }

    public void setUD_phoneNo(String UD_phoneNo) {
        this.UD_phoneNo = UD_phoneNo;
    }

    public String getUD_userID() {
        return UD_userID;
    }

    public void setUD_userID(String UD_userID) {
        this.UD_userID = UD_userID;
    }

    public String getUD_username() {
        return UD_username;
    }

    public void setUD_username(String UD_username) {
        this.UD_username = UD_username;
    }

    public String getUD_signInType() {
        return UD_signInType;
    }

    public void setUD_signInType(String UD_signInType) {
        this.UD_signInType = UD_signInType;
    }

    public long getUD_registeredOn() {
        return UD_registeredOn;
    }

    public void setUD_registeredOn(long UD_registeredOn) {
        this.UD_registeredOn = UD_registeredOn;
    }

    public String getCG_Role() { return CG_Role; }

    public void setCG_Role(String CG_Role) { this.CG_Role = CG_Role; }
}
