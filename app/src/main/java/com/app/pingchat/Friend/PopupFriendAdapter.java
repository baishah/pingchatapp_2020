package com.app.pingchat.Friend;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.Profile.ViewUserProfileActivity;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class PopupFriendAdapter extends RecyclerView.Adapter<PopupFriendAdapter.MyViewHolder> {
    ArrayList<userDetailsClass> userList;
    ArrayList userf=new ArrayList();
    Context context;
    DatabaseReference usersRef,notification;
    FirebaseAuth mAuths;
    PopupFriendAdapter popupFriendAdapter;
    public PopupFriendAdapter(ArrayList u){
        userf =u;
    }

    public ArrayList getUserf() {
        return userf;
    }

    public void setUserf(ArrayList userf) {
        this.userf = userf;
    }

    public PopupFriendAdapter(Context c, ArrayList<userDetailsClass> f){
        context = c;
        userList = f;
    }

    @NonNull
    @Override
    public PopupFriendAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new PopupFriendAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.add_friend_card,parent,false));
    }

    public void onBindViewHolder(@NonNull final PopupFriendAdapter.MyViewHolder holder, int position) {
        //getUserFriend();
        final userDetailsClass user = userList.get(position);
        holder.u.setText(user.getUD_displayName());
        holder.p.setText(user.getUD_aboutMe());
        holder.onClick(user.getUD_userID());
        if(user.getUD_avatarURL().startsWith("p")){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(user.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }
        else{
            GlideApp.with(context.getApplicationContext())
                    .load(user.getUD_avatarURL())
                    .into(holder.i);
        }

        final ArrayList userss = new ArrayList<>();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String currentuser = mAuth.getCurrentUser().getUid().toString();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        usersRef.child(currentuser).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String key = dataSnapshot1.getKey();
                    userss.add(key);
                    setUserf(userss);
                    //popupFriendAdapter = new PopupFriendAdapter(userss);
                    //setUserf(userss);
                }
                System.out.println("sssii" +getUserf());
                if(getUserf().contains(user.getUD_userID())){
                    usersRef.child(currentuser).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                String myFriend = user.getUD_userID();
                                String status = dataSnapshot.child(myFriend).getValue(String.class);
                                if(status.equals("friend")){
                                    holder.add.setVisibility(View.INVISIBLE);
                                    holder.added.setVisibility(View.VISIBLE);
                                }else if(status.equals("block")){
                                    holder.add.setVisibility(View.INVISIBLE);
                                    holder.added.setVisibility(View.VISIBLE);
                                    holder.added.setText("Block");
                                }else if(status.equals("blocked")){
                                    holder.add.setVisibility(View.INVISIBLE);
                                    holder.added.setVisibility(View.VISIBLE);
                                    holder.added.setText("Blocked");
                                }else if(status.equals("sent")) {
                                    holder.add.setVisibility(View.INVISIBLE);
                                    holder.added.setVisibility(View.VISIBLE);
                                    holder.added.setText("Sent");
                                }else if(status.equals("remove")) {
                                    holder.add.setVisibility(View.VISIBLE);
                                    holder.added.setVisibility(View.INVISIBLE);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

//                    holder.add.setVisibility(View.INVISIBLE);
//                    holder.added.setVisibility(View.VISIBLE);
                }


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void getUserFriend() {
        final ArrayList userss = new ArrayList<>();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String currentuser = mAuth.getCurrentUser().getUid().toString();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        usersRef.child(currentuser).child("Friends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String key = dataSnapshot1.getKey().toString();
                    userss.add(key);
                    popupFriendAdapter = new PopupFriendAdapter(userss);
                    //setUserf(userss);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s,p,added;
        CircleImageView i;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            //  s = itemView.findViewById(R.id.line2);
            p = itemView.findViewById(R.id.line3);
            i = itemView.findViewById(R.id.gambar);
            add = itemView.findViewById(R.id.add);
            added = itemView.findViewById(R.id.addded);
        }

        public void onClick(final String id){
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAuths = FirebaseAuth.getInstance();
                    String currentuser = mAuths.getCurrentUser().getUid().toString();
                    usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
                    usersRef.child(currentuser).child("userFriends").child(id).setValue("sent");
                    usersRef.child(id).child("userFriends").child(currentuser).setValue("received").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                //  addfriendfunc(id,currentuser);
                                final Map newReq = new HashMap();
                                newReq.put("UNF_dismissStatus",false);
                                newReq.put("UNF_timestamp", ServerValue.TIMESTAMP);
                                usersRef.child(id).child("userNotifications").child("UN_type").child("UN_friendRequest").child(currentuser).setValue(newReq).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        RequestQueue queue = Volley.newRequestQueue(context);
                                        String url = "https://devbm.ml/notification/"+currentuser+"/send-friend-request/"+id;

                                        StringRequest sq = new StringRequest
                                                (Request.Method.POST, url, new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {}
                                                }, new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {

                                                    }
                                                }){
                                            protected Map<String,String> getParams(){
                                                Map<String,String> parr = new HashMap<String, String>();
//                                              parr.put("friendid", friendid);
                                                return parr;
                                            }
                                        };
                                        sq.setRetryPolicy(new DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                        queue.add(sq);
                                        Toast.makeText(context, "Request sent!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                add.setVisibility(View.INVISIBLE);
                                added.setVisibility(View.VISIBLE);
                                added.setText("Sent");
                            }
                        }
                    });
                }
            });
        }
    }

    public int getItemCount() {
        return userList.size();
    }

    //hold
    public void addfriendfunc(String friendid,String current){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "https://api.pingchat.app/notification/v3/sendFriendRequest/";
        StringRequest sq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //   progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            protected Map<String,String> getParams(){
                Map<String,String> parr = new HashMap<String, String>();
                parr.put("userReceiverID",friendid);
                parr.put("userSenderID", current);
                return parr;
            }
        };
        queue.add(sq);
        //}

        //   }
        // });

    }

}
