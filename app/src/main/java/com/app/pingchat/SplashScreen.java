package com.app.pingchat;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.core.app.NotificationCompat;

import com.app.pingchat.Chat.ChatsActivity;
import com.app.pingchat.Friend.ContactsFragment;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.Map.MapsFragment;
import com.google.firebase.auth.FirebaseAuth;

import java.io.File;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class SplashScreen extends Activity {

    String Value;
    Intent i;
    FirebaseAuth mAuth;


    private final int SPLASH_DISPLAY_LENGTH = 4400;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    /**
     * Called when the activity is first created.
     */
    Thread splashTread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        mAuth = FirebaseAuth.getInstance();
        i = getIntent();
        deleteCache(this);

        StartAnimations();

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                /* Create an Intent that will start the Menu-Activity. */
////                Intent mainIntent = new Intent(SplashScreen.this, MainActivity.class);
////                SplashScreen.this.startActivity(mainIntent);
////                SplashScreen.this.finish();
//            }
//        }, SPLASH_DISPLAY_LENGTH);

    }


    private void StartAnimations() {
//        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
//        anim.reset();
//        LinearLayout linearlayout=(LinearLayout) findViewById(R.id.l_l);
//        linearlayout.clearAnimation();
//        linearlayout.startAnimation(anim);

//        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
//        anim.reset();
//        ImageView iv = (ImageView) findViewById(R.id.icon);
//        iv.clearAnimation();
//        iv.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 4490) {
                        sleep(100);
                        waited += 100;
                    }
                    if (i.getExtras() != null) {
                        System.out.println("masuk sini getintent");
                        Bundle params = i.getExtras();
                        if (params != null) {
                            Value = params.getString("notificationType");
                            System.out.println("Value" + Value);
                            if (Value != null) {
                                Log.d(TAG, "notificationType: " + Value);
                                if (mAuth.getCurrentUser() != null) {
                                    String currentuser = mAuth.getCurrentUser().getUid();
                                    if (Value.equals("chatPersonal")) {
                                        String friendid = params.getString("friendid");
                                        String chat_id = params.getString("chatID");
                                        String name = params.getString("name");
                                        String avatar = params.getString("avatarurl");
                                        String type = params.getString("type");
                                        System.out.println("typenotichat" + type);

                                        Intent i = new Intent(SplashScreen.this,
                                                MainActivity.class);
                                        i.putExtra("completedgroup", "openfromnotiqr");
                                        i.putExtra("chatid", chat_id);
                                        i.putExtra("type", type);
                                        i.putExtra("friendid", friendid);
                                        i.putExtra("name", name);
                                        i.putExtra("avatarurl", avatar);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                        finish();
                                    } else if (Value.equals("groupChat")) {
                                        String chat_id = params.getString("groupChatGroupID");
                                        String avatar = params.getString("groupChatAvatarURL");
                                        String name = params.getString("groupChatName");
                                        String msgType = params.getString("msgType");
                                        String msgtxt = params.getString("msgText");
                                        String type = params.getString("type");
                                        String friendid = params.getString("userSenderID");

                                        Intent i = new Intent(SplashScreen.this,
                                                MainActivity.class);
                                        i.putExtra("chatid", chat_id);
                                        i.putExtra("type", type);
                                        i.putExtra("friendid", friendid);
                                        i.putExtra("name", name);
                                        i.putExtra("avatarurl", avatar);
                                        startActivity(i);
                                        SplashScreen.this.finish();
                                    } else if (Value.equals("wakeup")) {
//                                    String friendid=params.getString("senderUser");
                                        Intent i = new Intent(SplashScreen.this,
                                                MainActivity.class);
//                                    i.putExtra("id",friendid);
                                        i.putExtra("fromnoti", "openfromnotiqwake");
                                        startActivity(i);
                                        SplashScreen.this.finish();
                                    } else if (Value.equals("addFriendViaQR")) {
                                        String friendid = params.getString("userSenderID");
                                        Intent i = new Intent(SplashScreen.this,
                                                ContactsFragment.class);
                                        i.putExtra("id", friendid);
                                        i.putExtra("fromnoti", "openfromnotiqr");
                                        startActivity(i);
                                        SplashScreen.this.finish();
                                    } else if (Value.equals("addFriend")) {
                                        String friendid = params.getString("sender");
                                        Intent i = new Intent(SplashScreen.this,
                                                ContactsFragment.class);
                                        i.putExtra("id", friendid);
                                        i.putExtra("fromnoti", "openfromnoti");
                                        startActivity(i);
                                        SplashScreen.this.finish();
                                    } else if (Value.equals("commentDiscover")) {
                                        String postid = params.getString("discoverID");
                                        String ownerID = params.getString("userReceiverID");
                                        String senderID = params.getString("userSenderID");
                                        Intent i = new Intent(SplashScreen.this,
                                                CommentLayoutActivity.class);
                                        i.putExtra("discoverpostid", postid);
                                        i.putExtra("currentuser", currentuser);
                                        i.putExtra("ownerid", ownerID);
                                        i.putExtra("friendID", senderID);

                                        startActivity(i);
                                        SplashScreen.this.finish();
                                    } else if (Value.equals("clapDiscover")) {
                                        String postid = params.getString("discoverID");
                                        Intent i = new Intent(SplashScreen.this,
                                                CommentLayoutActivity.class);
                                        i.putExtra("discoverPostid", postid);
                                        startActivity(i);
                                        SplashScreen.this.finish();
                                    }
                                } else {
                                    System.out.println("Please login");
                                }
                            } else {
                                Intent intent = new Intent(SplashScreen.this,
                                        MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                SplashScreen.this.finish();
                            }
                        } else {

                            Intent intent = new Intent(SplashScreen.this,
                                    MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            SplashScreen.this.finish();
                        }
                    } else {
                        Intent intent = new Intent(SplashScreen.this,
                                MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        SplashScreen.this.finish();
                    }

                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    SplashScreen.this.finish();
                }
            }
        };
        splashTread.start();

    }

    //Playing notification sound
//    public void playNotificationSound() {
//        try {
//
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
//            r.play();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onStop() {
        super.onStop();
        finish();
    }

}

