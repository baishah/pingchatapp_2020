package com.app.pingchat.events;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EventAcitivity extends AppCompatActivity {

    private RequestQueue mQueue;
    private RecyclerView mRecyclerView;
    private EventAdapter mEventAdapter;
    private ArrayList<EventItem> mEventList;
    private FirebaseAuth Mauth;
    private String currenct;
    LottieAnimationView animationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        mRecyclerView = findViewById(R.id.eventList);
//        mRecyclerView.setHasFixedSize(true);
        mQueue = Volley.newRequestQueue(this);

        mEventList = new ArrayList<>();

        mEventAdapter = new EventAdapter(EventAcitivity.this, mEventList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mEventAdapter);


        animationView = (LottieAnimationView) findViewById(R.id.animation_view);
        animationView.setAnimation("loading_loop.json");
        animationView.playAnimation();
        animationView.setVisibility(View.VISIBLE);
        getEventapi();

    }
    private void getEventapi(){
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Loading event");
//        progressDialog.show();
        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/tamuku-discover";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        progressDialog.dismiss();
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("dataEvent");

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject value = jsonArray.getJSONObject(i);
//                                jsonObject = jsonArray.getJSONObject(i);
                                animationView.cancelAnimation();
                                animationView.setVisibility(View.GONE);
                                String eId = value.getString("dE_id");
                                String e_type = value.getString("dE_mediaType");
                                String eUrl = value.getString("dE_mediaUrlComplete");
                                String eCountry = value.getString("dE_Country");
                                String eStateCountry = value.getString("dE_StateCountry");
                                String e_timeStart = value.getString("dE_timeStart");
                                String eTitle = value.getString("dE_title");

                                EventItem eventItem = new EventItem();
                                eventItem.sete_type(e_type);
                                eventItem.seteUrl(eUrl);
                                eventItem.seteCountry(eCountry);
                                eventItem.seteStateCountry(eStateCountry);
                                eventItem.seteTitle(eTitle);
                                eventItem.sete_timeStart(e_timeStart);

                                mEventList.add(eventItem);
//                                Collections.sort(mEventList,EventItem.eventItemComparator);
                            }
                            mEventAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(70000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(request);

    }

    public void onBackPressed(){
//        super.onBackPressed();
        finish();
    }
}
