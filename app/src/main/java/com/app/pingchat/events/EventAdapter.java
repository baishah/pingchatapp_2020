package com.app.pingchat.events;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jp.wasabeef.glide.transformations.BlurTransformation;

//import jp.wasabeef.glide.transformations.BlurTransformation;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {
    private Context mContext;
    private ArrayList<EventItem> mEventList;
//    private OnItemClickListener mListener;



    public interface OnItemClickListener{
        void onItemClick(int position);
    }
//    public void setOnItemClickListener(OnItemClickListener listener){
//        mListener = listener;
//    }

    public EventAdapter(Context context, ArrayList<EventItem> eventList){
        mContext = context;
        mEventList = eventList;
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.event_item, parent, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder eventViewHolder, int position) {

        EventItem currentItem = mEventList.get(position);
//        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        String imageUrl = currentItem.geteUrl();
        String dateNow = currentItem.gete_timeStart();

        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
        String dateString = format.format(new Date(Long.parseLong(dateNow)));


        eventViewHolder.eTitle.setText(currentItem.geteTitle());
        eventViewHolder.eLoc.setText(currentItem.geteCountry());
        eventViewHolder.eLoc1.setText(currentItem.geteStateCountry());
        eventViewHolder.eDate.setText(dateString);

        System.out.println("date_event:"+dateString);


        GlideApp.with(mContext)
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .into(eventViewHolder.mImageView1);

        GlideApp.with(mContext)
                .load(imageUrl)
                .apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 3)))
                .override(500,500)
                .into(eventViewHolder.mImageView);

//        Picasso.with(mContext).load(imageUrl).fit().centerInside().into(marketViewHolder.mImageView);

    }

    @Override
    public int getItemCount() {
//        System.out.println(mEventList);
        return mEventList.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView,mImageView1;
        public TextView eTitle, eDate, eLoc, eLoc1;


        public EventViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.event_img);
            mImageView1 = itemView.findViewById(R.id.event_img1);
            eTitle = itemView.findViewById(R.id.event_title);
            eDate = itemView.findViewById(R.id.event_date);
            eLoc = itemView.findViewById(R.id.event_location);
            eLoc1 = itemView.findViewById(R.id.event_state);


//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mListener != null){
//                        int position = getAdapterPosition();
//                        if (position != RecyclerView.NO_POSITION){
//                            mListener.onItemClick(position);
//                        }
//
//                    }
//                }
//            });
        }
    }
}
