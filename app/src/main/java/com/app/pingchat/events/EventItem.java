package com.app.pingchat.events;

public class EventItem {
    private String eTitle;
    private String eUrl;
    private String eId;
    private String e_type;
    private String eCountry;
    private String eStateCountry;
    private String e_timeStart;

    public EventItem() {
    }

    public void seteTitle(String eTitle) {
        this.eTitle = eTitle;
    }

    public String geteTitle() {
        return eTitle;
    }

    public String geteId() {
        return eId;
    }

    public void seteId(String eId) {
        this.eId = eId;
    }

    public String gete_type() {
        return e_type;
    }

    public void sete_type(String e_type) {
        this.e_type = e_type;
    }

    public String geteCountry() {
        return eCountry;
    }

    public void seteCountry(String eCountry) {
        this.eCountry = eCountry;
    }

    public String geteStateCountry() {
        return eStateCountry;
    }

    public void seteStateCountry(String eStateCountry) {
        this.eStateCountry = eStateCountry;
    }

    public String gete_timeStart() {
        return e_timeStart;
    }

    public void sete_timeStart(String e_timeStart) {
        this.e_timeStart = e_timeStart;
    }

    public String geteUrl() {
        return eUrl;
    }

    public void seteUrl(String eUrl) {
        this.eUrl = eUrl;
    }
}
