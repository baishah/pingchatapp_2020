package com.app.pingchat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class bottomSheetMapMenu extends BottomSheetDialogFragment implements View.OnClickListener {
    private BottomSheetListener mListener;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }
    private Button search_location,send_news,send_ping,friend_selector,ping_selector,partner_selector;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_map_menu, container, false);

        search_location = v.findViewById(R.id.searchlocation);
        send_news = v.findViewById(R.id.send_news);
        send_ping = v.findViewById(R.id.sendping);
        friend_selector = v.findViewById(R.id.friendselector);
        ping_selector = v.findViewById(R.id.pingselector);
        partner_selector = v.findViewById(R.id.partnerselector);

        search_location.setOnClickListener(this);
        send_news.setOnClickListener(this);
        send_ping.setOnClickListener(this);
        friend_selector.setOnClickListener(this);
        ping_selector.setOnClickListener(this);
        partner_selector.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.searchlocation:
                mListener.onButtonMapClicked("search_location");
                break;
            case R.id.send_news:
                mListener.onButtonMapClicked("send_news");
                dismiss();
                break;
            case R.id.sendping:
                mListener.onButtonMapClicked("send_ping");
                break;
            case R.id.friendselector:
                mListener.onButtonMapClicked("friend_selector");
                dismiss();
                break;
            case R.id.pingselector:
                mListener.onButtonMapClicked("ping_selector");
                dismiss();
                break;
            case R.id.partnerselector:
                mListener.onButtonMapClicked("partner_selector");
                dismiss();
                break;
        }
    }

    public interface BottomSheetListener {
        void onButtonMapClicked(String text);
    }

}
