package com.app.pingchat;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.Profile.ViewProfileActivityAfterScan;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.zxing.Result;

import java.sql.Timestamp;
import java.util.HashMap;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCouponScan  extends AppCompatActivity {

    private DatabaseReference users;
    FirebaseAuth mAuth;
    String current_user,rewards_id;

    ZXingScannerView qrCodeScanner;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.empty);

        mAuth = FirebaseAuth.getInstance();

        current_user = mAuth.getCurrentUser().getUid();
        users = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user);

        scan();

    }

    ZXingScannerView.ResultHandler v = new ZXingScannerView.ResultHandler() {
        @Override
        public void handleResult(Result result) {
            //  qrCodeScanner.animate();Toast.makeText(getApplicationContext(),"name: "+result.getText().toString(),Toast.LENGTH_SHORT).show();
            //   qrCodeScanner.resumeCameraPreview(v);
            String id = result.getText().toString();
            if(id.equals("@adseureka")){
                qrCodeScanner.stopCamera();
                openPopup();
            }
//            Toast.makeText(getApplicationContext(), " data:"+id, Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(QRCouponScan.this, ViewProfileActivityAfterScan.class);
//            intent.putExtra("id",id);
//            startActivity(intent);
        }
    };

    private void openPopup() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = LayoutInflater.from(this).inflate(R.layout.advertisement_coupon, null);

        HashMap claim_rewards = new HashMap();
        rewards_id = users.push().push().getKey();
        java.sql.Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String titles = "Eureka";
        claim_rewards.put("R_ID", rewards_id);
        claim_rewards.put("R_Title","Eureka");
        claim_rewards.put("R_Description","10% off");
        claim_rewards.put("R_status", true);
        claim_rewards.put("R_Timestamp", timestamp.getTime());

        users.child("userRewards").child("EUREKA").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        String title = dataSnapshot1.child("R_Title").getValue().toString();
                        if(!title.equals(titles)){
                            users.child("userRewards").child("EUREKA").child(rewards_id).setValue(claim_rewards).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Toast.makeText(QRCouponScan.this, "Coupon claimed!, please check Rewards to review.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else{
                            Toast.makeText(QRCouponScan.this, "Coupon already claimed", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    users.child("userRewards").child("EUREKA").child(rewards_id).setValue(claim_rewards).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(QRCouponScan.this, "Coupon claimed!, please check Rewards to review.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final ImageButton x_btn = view.findViewById(R.id.x_btn);

        x_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                qrCodeScanner.stopCamera();
                finish();
            }
        });

        alertDialog.setView(view);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
    }


    public void scan(){
        qrCodeScanner =new ZXingScannerView(this);
        setContentView(qrCodeScanner);
        qrCodeScanner.setMinimumHeight(50);
        qrCodeScanner.setMinimumWidth(50);
        qrCodeScanner.setResultHandler(v);
        qrCodeScanner.startCamera();

    }

    @Override public void onResume(){
        super.onResume();
        scan();
    }

    @Override
    public void onBackPressed(){
        qrCodeScanner.stopCamera();
        finish();
    }
}
