package com.app.pingchat.rewards;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.Discover.NW_detailsClass;
import com.app.pingchat.Discover.NewsListAdapter;
import com.app.pingchat.Discover.NewsShowActivity;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.app.pingchat.moreAdapter;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class rewardsAdapter extends RecyclerView.Adapter<rewardsAdapter.MyViewHolder> {
    //    String userNameData[];
//    int storyImages[];
    ArrayList<RewardsClass> rewardsList;
    Context context;
    String currentuser;
    private DatabaseReference users,userRewards;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position, String rID);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public rewardsAdapter(Context ct,ArrayList<RewardsClass> rl,String user) {
        context = ct;
        rewardsList = rl;
        currentuser = user;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.rewards_item, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        final RewardsClass rewardsItem = rewardsList.get(position);
        userRewards = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userRewards");
        String rewardId = rewardsItem.getR_ID();
        String title = rewardsItem.getR_Title();
        String desc = rewardsItem.getR_Description();
        String status = rewardsItem.getR_status();
        holder.titleText.setText(title);
        holder.descText.setText(desc);
        if(status.equals("true")){
            holder.redeembtn.setEnabled(true);
            holder.redeembtn.setBackgroundResource(R.drawable.round_blue);
            holder.redeembtn.setText("Redeem");
        }else{
            holder.redeembtn.setEnabled(false);
            holder.redeembtn.setBackgroundResource(R.drawable.round_gray);
            holder.redeembtn.setText("Redeemed");
        }
        holder.redeembtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position,rewardId);
//                Toast.makeText(context,"Coupon redeemed!",Toast.LENGTH_SHORT).show();
//                userRewards.child("EUREKA").child(rewardId).child("R_status").setValue(false);
//                notifyDataSetChanged();
//                holder.redeembtn.setBackgroundResource(R.drawable.round_gray);
//                holder.redeembtn.setText("Redeemed");
            }
        });

    }
    @Override
    public int getItemCount() {
        return rewardsList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView titleText, descText;
        Button redeembtn;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            titleText = itemView.findViewById(R.id.titleText);
            descText = itemView.findViewById(R.id.descText);
            redeembtn = itemView.findViewById(R.id.redeem);
        }
    }
}
