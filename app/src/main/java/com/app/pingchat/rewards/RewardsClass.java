package com.app.pingchat.rewards;

public class RewardsClass {

    private String R_ID,R_Title,R_Description;
    private String R_Timestamp;
    private String R_status;

    public RewardsClass(){}

    public RewardsClass(String R_ID,String R_status, String R_Timestamp, String R_Title,String R_Description) {
       this.R_ID = R_ID;
       this.R_status = R_status;
       this.R_Timestamp = R_Timestamp;
       this.R_Title = R_Title;
       this.R_Description = R_Description;
    }

    public String getR_ID() {
        return R_ID;
    }

    public void setR_ID(String r_ID) {
        R_ID = r_ID;
    }

    public String getR_status() {
        return R_status;
    }

    public void setR_status(String r_status) {
        R_status = r_status;
    }

    public String getR_Timestamp() {
        return R_Timestamp;
    }

    public void setR_Timestamp(String r_Timestamp) {
        R_Timestamp = r_Timestamp;
    }

    public String getR_Title() {
        return R_Title;
    }

    public void setR_Title(String r_Title) {
        R_Title = r_Title;
    }

    public String getR_Description() {
        return R_Description;
    }

    public void setR_Description(String r_Description) {
        R_Description = r_Description;
    }
}
