package com.app.pingchat.rewards;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RewardsFragment extends Fragment {

    private RecyclerView rewardsList;
    FirebaseAuth mAuth;
    private DatabaseReference users,userRewards;
    String current_id;
    ArrayList<RewardsClass> mRewardslist;
    private rewardsAdapter rewardsadapter;
    private RelativeLayout noReward;
    private RequestQueue mQueue;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View v = inflater.inflate(R.layout.rewards_layout, null, false);
//            return inflater.inflate(R.layout.rewards_layout, container, false);

            rewardsList = v.findViewById(R.id.reward_list);
            noReward = v.findViewById(R.id.no_rewards);
            rewardsList.setLayoutManager(new LinearLayoutManager(getContext()));
            mQueue = Volley.newRequestQueue(getContext());

            mAuth = FirebaseAuth.getInstance();

            if(mAuth.getCurrentUser()!=null) {
                current_id = mAuth.getCurrentUser().getUid();

                users = FirebaseDatabase.getInstance().getReference().child("Users");
                userRewards = FirebaseDatabase.getInstance().getReference().child("Users").child(current_id).child("userRewards");
            }
            getAllrewards();
            return v;
        }

        private void getAllrewards() {
            mRewardslist = new ArrayList<>();
            userRewards.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        noReward.setVisibility(View.GONE);
                        DataSnapshot E = dataSnapshot.child("EUREKA");
                        if(E.exists()){
                            for(DataSnapshot dataSnapshot1:E.getChildren()){
                                final RewardsClass rewards = new RewardsClass();
                                final String rewardsID = dataSnapshot1.getKey();
                                System.out.println("rewardsID:"+rewardsID);
                                String r_id = dataSnapshot1.child("R_ID").getValue().toString();
                                String r_title = dataSnapshot1.child("R_Title").getValue().toString();
                                String r_desc = dataSnapshot1.child("R_Description").getValue().toString();
                                String r_timestamp = dataSnapshot1.child("R_Timestamp").getValue().toString();
                                String r_status = dataSnapshot1.child("R_status").getValue().toString();
                                System.out.println("rewardsTitle:"+r_title);

                                rewards.setR_ID(r_id);
                                rewards.setR_Title(r_title);
                                rewards.setR_Description(r_desc);
                                rewards.setR_status(r_status);
                                rewards.setR_Timestamp(r_timestamp);

                                mRewardslist.add(rewards);
                                System.out.println("arraysize:"+mRewardslist.size());
                            }
                            rewardsadapter = new rewardsAdapter(getContext(),mRewardslist,current_id);
                            rewardsList.setAdapter(rewardsadapter);

                            rewardsadapter.setOnItemClickListener(new rewardsAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(int position, String rID) {
                                    System.out.println("rewardID:"+rID);
                                    displayQR(rID);
                                }
                            });
                        }
                    }else{
                        noReward.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    private void displayQR(String rID) {
        String url = "https://devbm.ml/users/"+rID+"/qr-ads";

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog= builder.create();
        View view = LayoutInflater.from(getContext()).inflate(R.layout.qr_ads_layout, null);

        final ImageView imageView = view.findViewById(R.id.qrcode);
        final Button exit = view.findViewById(R.id.exit_);
        final ProgressBar pbar = view.findViewById(R.id.progresssbar);
        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {
                            String dataimage = response.getString("dataQR");
                            System.out.println("Data Qr: " + dataimage);
                            byte[] imageBytes = dataimage.getBytes();
                            byte[] decodedString = Base64.decode(imageBytes, Base64.DEFAULT);
                            Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            System.out.println("Decoded: " + decodedImage);
                            if(imageView != null){
                                imageView.setImageBitmap(decodedImage);
                                pbar.setVisibility(View.GONE);
                            }else{
                                pbar.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
    }
}
