package com.app.pingchat;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Chat.Chats;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class messageAdapter extends RecyclerView.Adapter<messageAdapter.MessageViewHolder>
{
    FirebaseAuth mauth;
    DatabaseReference userschat;
    String userid;
    private List<Chats> userMessagesList;
    public messageAdapter(List<Chats> userMessagesList){
        this.userMessagesList = userMessagesList;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_messages_layout,parent,false);

                return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        userid = mauth.getCurrentUser().getUid();
        userschat = FirebaseDatabase.getInstance().getReference().child("Users").child(userid).child("Conversation").child("Personal");

        userschat.child("chatID").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return userMessagesList.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder{

        public TextView senderMessageText,receiverMessageText;

        public MessageViewHolder(@NonNull View itemView){
            super(itemView);

            senderMessageText = (TextView) itemView.findViewById(R.id.sendermessage);
            receiverMessageText=(TextView) itemView.findViewById(R.id.receivermessage);
        }
    }
}
