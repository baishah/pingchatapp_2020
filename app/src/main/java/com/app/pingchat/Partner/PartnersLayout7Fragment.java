package com.app.pingchat.Partner;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Ping.PingSetup2;
import com.app.pingchat.R;
import com.app.pingchat.UserAuth.Register2Activity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class PartnersLayout7Fragment extends Fragment {

    private static final String TAG = "partner7";

    CircleImageView logo;
    byte[] avatar;
    DatabaseReference Partner, users,media_partner;
    public StorageReference partners;
    FirebaseAuth mAuth;
    String key,partner_id;

    public ArrayList<String> mediaPartners;

    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    String c_user,logoimage, image, P_partnerMediaURL;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.partners_layout_7,container,false);

        logo = rootView.findViewById(R.id.gambar);
        Button next = rootView.findViewById(R.id.next_btn);
        EditText b_des = rootView.findViewById(R.id.business_description);

        ArrayList<String> mediaPartners;

        mediaPartners = getArguments().getStringArrayList("key");

        Log.d(TAG, "onComplete: List Array: " + mediaPartners);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Creating your business..");
                progressDialog.show();

                Partner = FirebaseDatabase.getInstance().getReference().child("Partners");
                DatabaseReference newDatabaseReference = Partner.push();

                partner_id = newDatabaseReference.getKey();
                mAuth = FirebaseAuth.getInstance();
                users = FirebaseDatabase.getInstance().getReference().child("Users");
                users.keepSynced(true);
                partners = FirebaseStorage.getInstance().getReference().child("partners");
                media_partner = FirebaseDatabase.getInstance().getReference().child("partners").child(partner_id).child("P_partnerMedia");
                c_user = PartnersLayout1Fragment.getInstance().getCurrentuser();

                String bisnes_des = b_des.getText().toString();
                final Date d = new Date();


                DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(d);
                calendar.add(Calendar.HOUR_OF_DAY,24);
                String jj = calendar.getTime().toString();
                String hh = dateFormat.format(new Date(jj)).toString();
                try{
                    Date date = dateFormat.parse(hh);
//                    final long unixtime =(long)date.getTime()* 1000;
                    final long unixtime = timestamp.getTime();
                    System.out.println("time_date:"+unixtime);
                    final Map USD_partner = new HashMap();
                    USD_partner.put("UPD_eligibility", true);
                    USD_partner.put("UPD_role","owner");
                    USD_partner.put("UPD_partnerID", partner_id);
                    users.child(c_user).child("userPartnerDetails").setValue(USD_partner).addOnCompleteListener(new OnCompleteListener<Void>() {
                        //                Partner.child(partner_id).child(c_user).setValue("owner").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                String partnerURL = "partners/"+partner_id+"/partnerLogo/"+partner_id+".jpg";
                                {
                                    final Map partnerDetails = new HashMap();
                                    partnerDetails.put("P_partnerID", partner_id);
                                    partnerDetails.put("P_partnerName", PartnersLayout2Fragment.getInstance().getBusinessnametext());
                                    partnerDetails.put("P_partnerEmail", PartnersLayout2Fragment.getInstance().getEmailtext());
                                    partnerDetails.put("P_partnerPhone", PartnersLayout2Fragment.getInstance().getPhonetext());
                                    partnerDetails.put("P_partnerCreatedOn", unixtime);
                                    partnerDetails.put("P_partnerCategory", PartnersLayout3Fragment.getInstance().getCat());
                                    partnerDetails.put("P_partnerCaption", bisnes_des);
                                    partnerDetails.put("P_partnerAddress", PartnersLayout5Fragment.getInstance().getPingloc());
                                    partnerDetails.put("P_profileURL", partnerURL);
                                    partnerDetails.put("P_partnerVerify", false);
                                    StorageReference filepath = partners.child(partner_id).child("partnerLogo").child(partner_id + ".jpg");
//                                    UploadTask uploadTask = filepath.putBytes(getAvatar());

                                    filepath.putBytes(getAvatar())
                                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                @Override
                                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                    Partner.child(partner_id).child("P_partnerDetails").setValue(partnerDetails).
                                                            addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {
                                                                                              //                                                       Toast.makeText(getContext().getApplicationContext(), "Register Success!", Toast.LENGTH_SHORT).show();

                                                                    } else {
                                                                        String message = task.getException().toString();
                                                                        Toast.makeText(getContext().getApplicationContext(), "error:" + message, Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }

                                                            }
                                                            );
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception exception) {
                                                    // Handle unsuccessful uploads
                                                    // ...
                                                }
                                            });
//                                parr.put("P_partnerLatitude", Double.toString(PartnersLayout5Fragment.getInstance().getPinglat()));
//                                parr.put("P_partnerLongitude", Double.toString(PartnersLayout5Fragment.getInstance().getPinglon()));
//                                    Partner.child(partner_id).child("P_partnerDetails").setValue(partnerDetails).
////                                            addOnCompleteListener(new OnCompleteListener<Void>() {
////                                               @Override
////                                               public void onComplete(@NonNull Task<Void> task) {
////                                                   if (task.isSuccessful()) {
//////                                                       Toast.makeText(getContext().getApplicationContext(), "Register Success!", Toast.LENGTH_SHORT).show();
////
////                                                   }else{
////                                                       String message = task.getException().toString();
////                                                       Toast.makeText(getContext().getApplicationContext(), "error:" + message, Toast.LENGTH_SHORT).show();
////                                                   }
////                                               }
////
////                                           }
////                                    );}
                                }
                                {
                                    final Map partnerLocation = new HashMap();

                                    partnerLocation.put("P_partnerLatitude", PartnersLayout5Fragment.getInstance().getPinglat());
                                    partnerLocation.put("P_partnerLongitude", PartnersLayout5Fragment.getInstance().getPinglon());
                                    Partner.child(partner_id).child("P_partnerLocation").setValue(partnerLocation).addOnCompleteListener(new OnCompleteListener<Void>() {
                                     @Override
                                     public void onComplete(@NonNull Task<Void> task) {
                                         if (task.isSuccessful()) {

                                         }else{
                                             String message = task.getException().toString();
                                             Toast.makeText(getContext().getApplicationContext(), "error:" + message, Toast.LENGTH_SHORT).show();

                                         }
                                     }
                                 }
                                    );}
                                {
                                    for (int j = 0; j < mediaPartners.size(); j++) {
                                        final Map mediaUrl = new HashMap();
                                        String image_id = media_partner.push().getKey();
                                        String mediaPartUrl = "partners/"+partner_id+"/partnerMedia/"+image_id +".jpg";
                                        mediaUrl.put("P_partnerMediaURL", mediaPartUrl);
                                        mediaUrl.put("P_partnerMediaType", "image");
                                        int finalJ = j;
                                        int finalJ1 = j;
                                        StorageReference filepath = partners.child(partner_id).child("partnerMedia").child(image_id +".jpg");
//                                        UploadTask uploadTask = filepath.putFile(Uri.parse(mediaPartners.get(finalJ)));
                                        filepath.putFile(Uri.parse(mediaPartners.get(finalJ)))
                                                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                    @Override
                                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                        // Get a URL to the uploaded content
//                                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                                        Partner.child(partner_id).child("P_partnerMedia").child(image_id).setValue(mediaUrl).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                Log.d(TAG, "onComplete: List Array: " + mediaPartners);
                                                                if (task.isSuccessful()) {
                                                                    progressDialog.dismiss();
                                                                    PartnersLayout8Fragment fragment8 = new PartnersLayout8Fragment();
                                                                    FragmentManager fragmentManager = getFragmentManager();
                                                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                                    fragmentTransaction.replace(R.id.fragment_container, fragment8);
                                                                    fragmentTransaction.addToBackStack(null);
                                                                    fragmentTransaction.commit();
                //                                                    StorageReference filepath = partners.child(partner_id).child("partnerMedia").child(image_id +".jpg");
                //                                                    UploadTask uploadTask = filepath.putFile(Uri.parse(mediaPartners.get(finalJ)));
                                                                } else {
                                                                    String message = task.getException().toString();
                                                                    Toast.makeText(getContext().getApplicationContext(), "error:" + message, Toast.LENGTH_SHORT).show();

                                                                }
                                                            }
                                                        });
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception exception) {
                                                        // Handle unsuccessful uploads
                                                        // ...
                                                    }
                                                });
                                    }
                                }
                                {
                                    final Map partnerMember = new HashMap();

                                    partnerMember.put("P_partnerJoined", unixtime);
                                    partnerMember.put("P_partnerRole", "owner");
                                    Partner.child(partner_id).child("P_partnerMember").child(c_user).setValue(partnerMember).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                            }else{
                                                String message = task.getException().toString();
                                                Toast.makeText(getContext().getApplicationContext(), "error:" + message, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                    );}
                            }
                        }
                    });
                }
                catch (Exception e){
                }
            }
        });

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
                }
                else{
                    CropImage.startPickImageActivity(getContext(),PartnersLayout7Fragment.this);
                }
            }
        });
        return rootView;
    }

    public void loadImageLibrary(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setBorderLineColor(Color.BLUE)
                .setBorderLineThickness(1)
                .setFixAspectRatio(true)
                .start(getContext(),this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = getContext().getApplicationContext().getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    Bitmap lastbitmap = null;
                    lastbitmap = selectedImage;
                    setLogoimage(getStringImage(lastbitmap));
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
                    logo.setImageBitmap(selectedImage);
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    setAvatar(datas);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(getContext(),data);
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(getContext(),this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(getContext(),this);
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedimage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
        return encodedimage;
    }

    public byte[] getAvatar() { return avatar; }
    public void setAvatar(byte[] avatar) { this.avatar = avatar; }

    public String getLogoimage() {
        return logoimage;
    }

    public void setLogoimage(String logoimage) {
        this.logoimage = logoimage;
    }

    public String getP_partnerMediaURL() {
        return P_partnerMediaURL;
    }

    public void setP_partnerMediaURL(String P_partnerMediaURL) {
        this.P_partnerMediaURL = P_partnerMediaURL;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
