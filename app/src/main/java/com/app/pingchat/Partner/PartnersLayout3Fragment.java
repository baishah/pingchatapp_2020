package com.app.pingchat.Partner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.pingchat.R;

public class PartnersLayout3Fragment extends Fragment {

    EditText category;
    TextView local,music,art,personal,shopping,product,supermarket, backbtn;
    private static PartnersLayout3Fragment instance = null;

    String P_partnerCategory;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.partners_layout_3,container,false);

        Button next = rootView.findViewById(R.id.next);
        category = rootView.findViewById(R.id.category_txt);
        local = rootView.findViewById(R.id.local_bisnes);
        music = rootView.findViewById(R.id.musician);
        art = rootView.findViewById(R.id.art);
        personal = rootView.findViewById(R.id.personal);
        shopping = rootView.findViewById(R.id.shopping);
        product = rootView.findViewById(R.id.product);
        supermarket = rootView.findViewById(R.id.supermarket);
        backbtn = rootView.findViewById(R.id.back);

        instance = this;

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setText("Local Business");
                setCat("Local Business");
            }
        });

        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setText("Musician/Band");
                setCat("Musician/Band");
            }
        });

        art.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setText("Art");
                setCat("Art");
            }
        });

        personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setText("Personal Blog");
                setCat("Personal Blog");
            }
        });

        shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setText("Shopping/Retail");
                setCat("Shopping/Retail");
            }
        });

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setText("Product/Service");
                setCat("Product/Service");
            }
        });

        supermarket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setText("Supermarket/Convenience");
                setCat("Supermarket/Convenience");
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                P_partnerCategory = category.getText().toString();

                if((!P_partnerCategory.equals(null)) && (!P_partnerCategory.equals(""))){
                    PartnersLayout5Fragment fragment5 = new PartnersLayout5Fragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container,fragment5);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                    setCat(P_partnerCategory);
                }
                else{
                    Toast.makeText(getContext().getApplicationContext(),"Please fill in your business category",Toast.LENGTH_SHORT).show();
                }

            }
        });
        return rootView;
    }

    public String getCat() {
        return P_partnerCategory;
    }

    public void setCat(String cat) {
        this.P_partnerCategory = cat;
    }

    public static PartnersLayout3Fragment getInstance(){
        return instance;
    }
}