package com.app.pingchat.Partner;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;

public class PartnersLayout8Fragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.partners_layout_8,container,false);

        Button next = rootView.findViewById(R.id.fin_btn);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext().getApplicationContext(), MainActivity.class);
                intent.putExtra("completedbusiness","ada");
                startActivity(intent);
                getActivity().finish();
               }
        });
        return rootView;
    }
}
