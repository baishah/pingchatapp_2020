package com.app.pingchat.Partner;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.MotionEventCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.pingchat.PlaceFieldSelector;
import com.app.pingchat.R;
import com.app.pingchat.location;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static androidx.constraintlayout.widget.Constraints.TAG;

public class PartnersLayout5Fragment extends Fragment implements OnMapReadyCallback {

    TextView location,uname,cancel, backbtn;
    String loc,strAdd,twnship,pcode,town,country,e,p;

    EditText txt_street_add,txt_village,txt_postcode,txt_towncity,txt_country;

    private FirebaseAuth mAuth;
    FrameLayout layout;
    private GoogleMap map;

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    private BitmapDescriptor icon;

    String P_partnerAddress;
    double P_partnerLatitude,P_partnerLongitude;

    private static PartnersLayout5Fragment instance = null;

    DatabaseReference locations, users;
    String currentuser;

    Button instruction,next;
    FloatingActionButton proceedping;

    private String api_key = "AIzaSyDOewqTvU226Fe7ZTYIobI58oJomTnPIMQ";
    private BottomSheetBehavior bottomSheetBehavior;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.partners_layout_5,container,false);

        mAuth = FirebaseAuth.getInstance();
        currentuser = mAuth.getCurrentUser().getUid();

        final FloatingActionButton search = rootView.findViewById(R.id.search);
        instruction = rootView.findViewById(R.id.instruction);
        proceedping = rootView.findViewById(R.id.proceedping);

        txt_street_add = rootView.findViewById(R.id.txt_street_add);
        txt_country = rootView.findViewById(R.id.txt_country);
        txt_postcode = rootView.findViewById(R.id.txt_postcode);
        txt_towncity = rootView.findViewById(R.id.txt_towncity);
        txt_village = rootView.findViewById(R.id.txt_village);

        users = FirebaseDatabase.getInstance().getReference().child("Users");
        users.keepSynced(true);
        locations = users.child(currentuser).child("userStatus");

        next = rootView.findViewById(R.id.next_btn);
        location = rootView.findViewById(R.id.txt_loca);
        cancel = rootView.findViewById(R.id.cancel);
        backbtn = rootView.findViewById(R.id.back);

        instance = this;
        final View bottom_sheet_location = rootView.findViewById(R.id.bottom_sheet_location);

        Places.initialize(getContext().getApplicationContext(),api_key);

        if (!Places.isInitialized()) {
            Places.
                    initialize(getContext().getApplicationContext(), api_key);
        }

        PlaceFieldSelector fieldSelector = new PlaceFieldSelector();

        search.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN,fieldSelector.getAllFields()).build(getContext());
//                new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(PingSetup.this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_location);

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                String state = "N/A";

                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    state = "STATE_EXPANDED";
                } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    state = "STATE_COLLAPSED";

                }
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                bottomSheet.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        int action = MotionEventCompat.getActionMasked(event);
                        switch (action) {
                            case MotionEvent.ACTION_DOWN:
                                return false;
                            default:
                                return true;
                        }
                    }
                });
            }
        });
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                if(getPingloc()!=""){
//                    location.setText(getPingloc());
//                }
            }
        });

        location.setText("");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strAdd = txt_street_add.getText().toString();
                twnship= txt_village.getText().toString();
                pcode= txt_postcode.getText().toString();
                town= txt_towncity.getText().toString();
                country= txt_country.getText().toString();

                PartnersLayout7_1Fragment fragment7_1 = new PartnersLayout7_1Fragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container,fragment7_1);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;


        icon = BitmapDescriptorFactory.fromResource(R.drawable.locked_ping);


        proceedping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if(getPingloc()!=""){
                    location.setText(getPingloc());
                    txt_country.setText(getCountry());
                    txt_postcode.setText(getPcode());
                    txt_street_add.setText(getStrAdd());
                    txt_towncity.setText(getTown());
                    txt_village.setText(getTwnship());
                }
            }
        });
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                map.clear();
                map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("location :"+latLng)
                        .icon(icon)
                        .draggable(true));
                setPingloc(getCompleteAddressString(latLng.latitude,latLng.longitude));
                //  PartnersLayout5Fragment.getInstance().setLoc(getCompleteAddressString(latLng.latitude,latLng.longitude));
                setPinglat(latLng.latitude);
                setPinglon(latLng.longitude);
                instruction.setVisibility(View.INVISIBLE);
            }
        });
//        displayuserLocation();
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        if (requestCode ==PLACE_AUTOCOMPLETE_REQUEST_CODE){
            if (resultCode == RESULT_OK) {
                Place place;
                place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getLatLng());
                String place2 = place.getAddress().toString();
                setPingloc(place2);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }



    public String getCompleteAddressString(double lat,double lon){
        String strrAdd = "";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try{
            List<Address> myAddress = geocoder.getFromLocation(lat,lon,1);
            if(myAddress!=null){
                Address returnedAdd = myAddress.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for(int i=0;i<=returnedAdd.getMaxAddressLineIndex();i++){
                    strReturnedAddress.append(returnedAdd.getAddressLine(i)).append("\n");
                }
                strrAdd = strReturnedAddress.toString();

                setCountry(returnedAdd.getCountryName());
                setPcode(returnedAdd.getPostalCode());
                setStrAdd(returnedAdd.getFeatureName());
                setTown(returnedAdd.getAdminArea());
                setTwnship(returnedAdd.getLocality());

                Log.w("location address", strReturnedAddress.toString());
            }
            else{
                Log.w("location address", "No Address returned!");
            }

        }catch(Exception e){
            e.printStackTrace();
            Log.w("location address", "Cannot get Address!");
        }
        return strrAdd;
    }

//    public void displayuserLocation(){
//        locations.child(currentuser).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                location userlocation = dataSnapshot.getValue(location.class);
//                final LatLng userloc = new LatLng(userlocation.latitude,userlocation.longitude);
//
//                map.moveCamera(CameraUpdateFactory.newLatLngZoom(userloc,15));
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }

    public static PartnersLayout5Fragment getInstance(){
        return instance;
    }

    public void setPingloc(String loc) {
        this.P_partnerAddress = loc;
    }
    public String getPingloc() {
        return P_partnerAddress;
    }

    public void setPinglat(double lat){
        this.P_partnerLatitude = lat;
    }
    public double getPinglat(){
        return P_partnerLatitude;
    }

    public void setPinglon(double lon){
        this.P_partnerLongitude = lon;
    }
    public double getPinglon(){
        return P_partnerLongitude;
    }

    public String getStrAdd() {
        return strAdd;
    }

    public void setStrAdd(String strAdd) {
        this.strAdd = strAdd;
    }

    public String getTwnship() {
        return twnship;
    }

    public void setTwnship(String twnship) {
        this.twnship = twnship;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}