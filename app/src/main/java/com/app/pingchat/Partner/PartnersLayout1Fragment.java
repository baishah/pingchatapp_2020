package com.app.pingchat.Partner;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LOCATION_SERVICE;

public class PartnersLayout1Fragment extends Fragment {

    FirebaseAuth mAuth;
    String currentuser, UPD_eligibility,UPD_role;
    CircleImageView userdp;
    TextView userdisplayname;
    private DatabaseReference users;
    LocationManager locationManager;

    private static PartnersLayout1Fragment instance = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.partners_layout_1,container,false);

        Button next = rootView.findViewById(R.id.next);
        userdp = rootView.findViewById(R.id.user_avatar);
        userdisplayname = rootView.findViewById(R.id.username);

        mAuth = FirebaseAuth.getInstance();
        currentuser = mAuth.getCurrentUser().getUid();
        users = FirebaseDatabase.getInstance().getReference().child("Users");
        users.keepSynced(true);

        instance = this;

        next.setEnabled(false);

        locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            next.setEnabled(true);
            next.setBackgroundResource(R.drawable.round_blue);
        }else{
//            showGPSDisabledAlertToUser();
            showAlert();
        }

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PartnersLayout2Fragment fragment2 = new PartnersLayout2Fragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return rootView;
    }

    @Override
    public void onStart(){
        super.onStart();
        if(mAuth.getCurrentUser()!=null){
            currentuser = mAuth.getCurrentUser().getUid();
            setCurrentuser(currentuser);
            retreivepicname();
        }
    }
    public void onBackPressed(){
        getFragmentManager().popBackStackImmediate();
    }


    public void retreivepicname(){
        users.child(currentuser).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String avatarurl = dataSnapshot.child("UD_avatarURL").getValue().toString();
                String displayname = dataSnapshot.child("UD_displayName").getValue().toString();

                userdisplayname.setText(displayname);

                if(avatarurl.startsWith("h")){
                    GlideApp.with(getContext().getApplicationContext())
                            .load(avatarurl)
                            .into(userdp);
                }
                else{
                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatarurl);
                    GlideApp.with(getContext().getApplicationContext())
                            .load(storageReference)
                            .into(userdp);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void showAlert(){
        String message,title,btntext;

        message = "your locations settings is set to off. Please enable location";
        title = "Enable location";
        btntext = "Location Settings";

        final android.app.AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setCancelable(false);
        dialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton(btntext, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent callGPSSettingIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }



    public String getCurrentuser() {
        return currentuser;
    }

    public void setCurrentuser(String currentuser) {
        this.currentuser = currentuser;
    }

    public static PartnersLayout1Fragment getInstance(){
        return instance;
    }
}