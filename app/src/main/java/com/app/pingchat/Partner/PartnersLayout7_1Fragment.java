package com.app.pingchat.Partner;

import androidx.fragment.app.Fragment;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.pingchat.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class PartnersLayout7_1Fragment extends Fragment {

    private static final String TAG = "PartnersLayout7_1";

    private static final int PICK_IMAGE_MULTIPLE = 0;
    private ImageView img1,img2, img3;
    Button next;
    Button uploadBtn;
    byte[] images;
    private Uri uri;
    String URL_IMAGE = "";
    TextView alert, backbtn;
    private ArrayList<String> mediaPartners;
    ArrayList<Uri> ImageList = new ArrayList<Uri>();
    String loc,strAdd,twnship,pcode,town,country,e,p, P_partnerMediaURL;
    EditText txt_street_add,txt_village,txt_postcode,txt_towncity,txt_country;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.partners_layout_7_1, container, false);

        uploadBtn = rootView.findViewById(R.id.upload_picture);
        img1 = rootView.findViewById(R.id.img1);
        img2 = rootView.findViewById(R.id.img2);
        img3 = rootView.findViewById(R.id.img3);
        next = rootView.findViewById(R.id.next_btn);
        alert = rootView.findViewById(R.id.alert);
        txt_street_add = rootView.findViewById(R.id.txt_street_add);
        txt_country = rootView.findViewById(R.id.txt_country);
        txt_postcode = rootView.findViewById(R.id.txt_postcode);
        txt_towncity = rootView.findViewById(R.id.txt_towncity);
        txt_village = rootView.findViewById(R.id.txt_village);
        backbtn = rootView.findViewById(R.id.back);

//        String mediaUrl = getIntent().getStringExtra("P_partnerMediaURL");

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_MULTIPLE);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("key", mediaPartners);

                PartnersLayout7Fragment fragment7 = new PartnersLayout7Fragment();
                fragment7.setArguments(bundle);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container,fragment7);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK){
            ClipData clipData = data.getClipData();

            if(clipData != null){
                mediaPartners = new ArrayList<>();
                img1.setImageURI(clipData.getItemAt(0).getUri());
                img2.setImageURI(clipData.getItemAt(1).getUri());
                img3.setImageURI(clipData.getItemAt(2).getUri());

                for (int i=0; i < clipData.getItemCount(); i++){
                    Uri uri = clipData.getItemAt(i).getUri();
                    final InputStream imageStream;
                    Log.e("image:", uri.toString());
                    Log.e(TAG,"image" + clipData);
                    System.out.println("image:"+clipData);

                    try {
                        imageStream = getContext().getApplicationContext().getContentResolver().openInputStream(uri);
                        Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        Bitmap lastbitmap = null;
                        lastbitmap = selectedImage;
                        setP_partnerMediaURL(getStringImage(lastbitmap));
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream); //compress it
//                            byte[] datas = byteArrayOutputStream.toByteArray();
//                            setImages(datas);
                        mediaPartners.add(String.valueOf(uri));


                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
            }

//                    int countClipData = data.getClipData().getItemCount();
//                    int currentImageSelect = 0;
//
//                    while (currentImageSelect < countClipData){
//                        uri = data.getClipData().getItemAt(currentImageSelect).getUri();
//                        ImageList.add(uri);
//                        currentImageSelect = currentImageSelect + 1;
//
//                    }
//                    alert.setVisibility(View.VISIBLE);
//                    alert.setText(ImageList.size()+" images have been selected");
////                    uploadBtn.setVisibility(View.GONE);
//
//                }else {
//                    Toast.makeText(getContext(), "Please select 3 Image", Toast.LENGTH_LONG).show();
//                }

//        }

        }


    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG,70,baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedimage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
        return encodedimage;
    }

    public byte[] getImages() { return images; }
    public void setImages(byte[] images) { this.images = images; }

    public String getP_partnerMediaURL() {
        return P_partnerMediaURL;
    }

    public void setP_partnerMediaURL(String P_partnerMediaURL) {
        this.P_partnerMediaURL = P_partnerMediaURL;
    }


}
