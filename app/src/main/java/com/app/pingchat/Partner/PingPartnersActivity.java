package com.app.pingchat.Partner;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.app.pingchat.R;
import com.app.pingchat.ViewBusinessProfileActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PingPartnersActivity extends AppCompatActivity {
    DatabaseReference users;
    String current_user;
    private FirebaseAuth mAuth;
    LottieAnimationView animationView;


    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progressbar);

        mAuth = FirebaseAuth.getInstance();
//
        animationView = (LottieAnimationView) findViewById(R.id.animation_view);
        animationView.setAnimation("blue_man_skeleton.json");
        animationView.playAnimation();


        if(mAuth.getCurrentUser()!=null) {
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            current_user = mAuth.getCurrentUser().getUid();
            checkBusiness();
        }

//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
//            Toast.makeText(this, "GPS is Enabled in your devide", Toast.LENGTH_SHORT).show();
//        }else{
//            showGPSDisabledAlertToUser();
//        }
    }

    public void checkBusiness(){
        users.child(current_user).child("userPartnerDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if(dataSnapshot.hasChild("UPD_eligibility")){
                        String eligibility = dataSnapshot.child("UPD_eligibility").getValue().toString();
                        System.out.println("eligibility:"+eligibility);
                        if(eligibility.equals("true")){
                            String ppid = dataSnapshot.child("UPD_partnerID").getValue().toString();
                            animationView.cancelAnimation();
                            animationView.setVisibility(View.GONE);
                            System.out.println("has partner account");
                            Intent i = new Intent(getApplication(), ViewBusinessProfileActivity.class);
                            i.putExtra("id",current_user);
                            i.putExtra("partnerid", ppid);
                            startActivity(i);
                            Toast.makeText(getApplication(), "You already have Partner Account!", Toast.LENGTH_SHORT).show();
                            finish();
                        }else {
                            animationView.cancelAnimation();
                            animationView.setVisibility(View.GONE);
                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new PartnersLayout1Fragment()).commit();
                            System.out.println("no partner account");
//                            Intent i = new Intent(getApplication(), PingPartnersActivity.class);
//                            startActivity(i);
                        }
                    }
                }else {
                    animationView.cancelAnimation();
                    animationView.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new PartnersLayout1Fragment()).commit();
                    System.out.println("no partner account");
//                            Intent i = new Intent(getApplication(), PingPartnersActivity.class);
//                            startActivity(i);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });

    }

    public void onBackPressed(){
//        finish();
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count <= 0) {
            super.onBackPressed();
            finish();
            //additional code
        } else {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }
}

