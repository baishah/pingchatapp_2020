package com.app.pingchat.Partner;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;
import com.app.pingchat.UserAuth.LoginActivity;

public class  PartnersLayout2Fragment extends Fragment {

    Button next;
    TextView backbtn;
    EditText businessname,email,phone;
    String P_partnerName,P_partnerEmail,P_partnerPhone;
    private static PartnersLayout2Fragment instance = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.partners_layout_2,container,false);


        next = rootView.findViewById(R.id.next);
        backbtn = rootView.findViewById(R.id.back);
        businessname = rootView.findViewById(R.id.edit_business);
        email = rootView.findViewById(R.id.edit_email);
        phone  = rootView.findViewById(R.id.edit_phone);
        instance = this;

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                P_partnerName = businessname.getText().toString();
                P_partnerEmail=email.getText().toString();
                P_partnerPhone=phone.getText().toString();

                if ((P_partnerEmail.matches(emailPattern)) && (!P_partnerName.equals("")) && (!P_partnerPhone.equals("")))
                {
                    setBusinessnametext(P_partnerName);
                    setEmailtext(P_partnerEmail);
                    setPhonetext(P_partnerPhone);

                    PartnersLayout3Fragment fragment3 = new PartnersLayout3Fragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container,fragment3);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
                else
                {
                    Toast.makeText(getContext().getApplicationContext(),"Invalid email address or some field are empty",Toast.LENGTH_SHORT).show();
                }


            }
        });
        return rootView;
    }
    public static PartnersLayout2Fragment getInstance(){
        return instance;
    }

    public String getBusinessnametext() {
        return P_partnerName;
    }

    public void setBusinessnametext(String businessnametext) {
        this.P_partnerName = businessnametext;
    }

    public String getEmailtext() {
        return P_partnerEmail;
    }

    public void setEmailtext(String emailtext) {
        this.P_partnerEmail = emailtext;
    }

    public String getPhonetext() {
        return P_partnerPhone;
    }

    public void setPhonetext(String phonetext) {
        this.P_partnerPhone = phonetext;
    }
}