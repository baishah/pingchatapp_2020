package com.app.pingchat;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.app.pingchat.Ping.PingSetup;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class bottomSheetCameraGallery extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;

    public String getGhosttype() {
        return ghosttype;
    }

    public void setGhosttype(String ghosttype) {
        this.ghosttype = ghosttype;
    }

    private String ghosttype;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }
    private Button camera,gallery;
    RelativeLayout rl1,rl2,rl3;
    ImageView rl1blue,rl2blue,rl3blue;

    private final int REQUEST_IMAGE_CAPTURE = 1,REQUEST_IMAGE_GALLERY = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet, container, false);

        rl1 = v.findViewById(R.id.rr);
        rl2 = v.findViewById(R.id.rr2);
        rl3 = v.findViewById(R.id.rr3);

        rl1blue = v.findViewById(R.id.h_precise);
        rl2blue = v.findViewById(R.id.h_frozen);
        rl3blue = v.findViewById(R.id.h_father);

        switch (getGhosttype()){
            case "normal":
                rl1blue.setVisibility(View.VISIBLE);
                break;
            case "frozens":
                rl2blue.setVisibility(View.VISIBLE);
                break;
            case "fathers":
                rl3blue.setVisibility(View.VISIBLE);
                break;
        }
        rl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onButtonClicked("precise");
                rl1blue.setVisibility(View.VISIBLE);
                rl2blue.setVisibility(View.INVISIBLE);
                rl3blue.setVisibility(View.INVISIBLE);
                setGhosttype("normal");
            }
        });
        rl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onButtonClicked("frozen");
                rl2blue.setVisibility(View.VISIBLE);
                rl1blue.setVisibility(View.INVISIBLE);
                rl3blue.setVisibility(View.INVISIBLE);
                setGhosttype("frozens");
            }
        });

        rl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onButtonClicked("father");
                rl3blue.setVisibility(View.VISIBLE);
                rl1blue.setVisibility(View.INVISIBLE);
                rl2blue.setVisibility(View.INVISIBLE);
                setGhosttype("fathers");
            }
        });
        return v;
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

}
