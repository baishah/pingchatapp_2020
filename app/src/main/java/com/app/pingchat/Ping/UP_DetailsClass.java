package com.app.pingchat.Ping;

import java.util.Comparator;
import java.util.Date;

public class UP_DetailsClass {
    private String UPD_address,UPD_caption,UPD_mediaType,UPD_mediaURL,UPD_mediaThumbnailURL,UPD_ownerID,UPD_pingID,UPD_type;
    private long UPD_dateEnd,UPD_dateStart;
    private String senderImage,displayName;
    private Date datetocompare;

    public UP_DetailsClass(Date datetocompare,String senderImage, String displayName, String UPD_address, String UPD_caption, long UPD_dateEnd, long UPD_dateStart, String UPD_mediaType, String UPD_mediaURL,String UPD_mediaThumbnailURL, String UPD_ownerID, String UPD_pingID, String UPD_type) {
        this.UPD_address = UPD_address;
        this.UPD_caption = UPD_caption;
        this.UPD_dateEnd = UPD_dateEnd;
        this.UPD_dateStart = UPD_dateStart;
        this.UPD_mediaType = UPD_mediaType;
        this.UPD_mediaURL = UPD_mediaURL;
        this.UPD_mediaThumbnailURL = UPD_mediaThumbnailURL;
        this.UPD_ownerID = UPD_ownerID;
        this.UPD_pingID = UPD_pingID;
        this.UPD_type = UPD_type;
        this.senderImage = senderImage;
        this.displayName = displayName;
        this.datetocompare = datetocompare;
    }

    public UP_DetailsClass(){}

    public String getUPD_address() {
        return UPD_address;
    }

    public void setUPD_address(String UPD_address) {
        this.UPD_address = UPD_address;
    }

    public String getUPD_caption() {
        return UPD_caption;
    }

    public void setUPD_caption(String UPD_caption) {
        this.UPD_caption = UPD_caption;
    }

    public long getUPD_dateEnd() {
        return UPD_dateEnd;
    }

    public void setUPD_dateEnd(long UPD_dateEnd) {
        this.UPD_dateEnd = UPD_dateEnd;
    }

    public long getUPD_dateStart() {
        return UPD_dateStart;
    }

    public void setUPD_dateStart(long UPD_dateStart) {
        this.UPD_dateStart = UPD_dateStart;
    }

    public String getUPD_mediaType() {
        return UPD_mediaType;
    }

    public void setUPD_mediaType(String UPD_mediaType) {
        this.UPD_mediaType = UPD_mediaType;
    }

    public String getUPD_mediaURL() {
        return UPD_mediaURL;
    }

    public void setUPD_mediaURL(String UPD_mediaURL) {
        this.UPD_mediaURL = UPD_mediaURL;
    }

    public String getUPD_mediaThumbnailURL() {
        return UPD_mediaThumbnailURL;
    }

    public void setUPD_mediaThumbnailURL(String UPD_mediaThumbnailURL) {
        this.UPD_mediaThumbnailURL = UPD_mediaThumbnailURL;
    }

    public String getUPD_ownerID() {
        return UPD_ownerID;
    }

    public void setUPD_ownerID(String UPD_ownerID) {
        this.UPD_ownerID = UPD_ownerID;
    }

    public String getUPD_pingID() {
        return UPD_pingID;
    }

    public void setUPD_pingID(String UPD_pingID) {
        this.UPD_pingID = UPD_pingID;
    }

    public String getUPD_type() {
        return UPD_type;
    }

    public void setUPD_type(String UPD_type) {
        this.UPD_type = UPD_type;
    }

    public String getSenderImage() { return senderImage; }

    public void setSenderImage(String senderImage) { this.senderImage = senderImage; }

    public String getDisplayName() { return displayName; }

    public void setDisplayName(String displayName) { this.displayName = displayName; }

    public Date getDatetocompare() { return datetocompare; }

    public void setDatetocompare(Date datetocompare) { this.datetocompare = datetocompare; }

    public  static Comparator<UP_DetailsClass> uds_detailsClassComparator = new Comparator<UP_DetailsClass>() {
        @Override
        public int compare(UP_DetailsClass t0, UP_DetailsClass t1) {
            return t1.getDatetocompare().compareTo(t0.getDatetocompare());
        }
    };
}
