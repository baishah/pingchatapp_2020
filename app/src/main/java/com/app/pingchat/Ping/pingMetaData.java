package com.app.pingchat.Ping;

public class pingMetaData {

    String pingAddress;
    String pingCaption;
    Long pingDate;
    Long pingExp;
    String pingImageURL;
    String pingSenderID;
    String pingType;
    String senderimage;
    String pingid;
    String displayName;

    public pingMetaData(Long pingDate,Long pingExp, String pingAddress, String pingCaption, String pingImageURL, String pingSenderID, String pingType) {
        this.pingAddress = pingAddress;
        this.pingCaption = pingCaption;
        this.pingDate = pingDate;
        this.pingExp = pingExp;
        this.pingImageURL = pingImageURL;
        this.pingSenderID = pingSenderID;
        this.pingType = pingType;
    }

    public pingMetaData(){}

    public String getPingid() {
        return pingid;
    }

    public void setPingid(String pingid) {
        this.pingid = pingid;
    }

    public String getSenderimage() {
        return senderimage;
    }

    public void setSenderimage(String senderimage) {
        this.senderimage = senderimage;
    }

    public Long getPingDate() {
        return pingDate;
    }

    public void setPingDate(Long pingDate) {
        this.pingDate = pingDate;
    }

    public Long getPingExp() {
        return pingExp;
    }

    public void setPingExp(Long pingExp) {
        this.pingExp = pingExp;
    }

    public String getPingAddress() {
        return pingAddress;
    }

    public void setPingAddress(String pingAddress) {
        this.pingAddress = pingAddress;
    }

    public String getPingCaption() {
        return pingCaption;
    }

    public void setPingCaption(String pingCaption) {
        this.pingCaption = pingCaption;
    }


    public String getPingImageURL() {
        return pingImageURL;
    }

    public void setPingImageURL(String pingImageURL) {
        this.pingImageURL = pingImageURL;
    }

    public String getPingSenderID() {
        return pingSenderID;
    }

    public void setPingSenderID(String pingSenderID) {
        this.pingSenderID = pingSenderID;
    }

    public String getPingType() {
        return pingType;
    }

    public void setPingType(String pingType) {
        this.pingType = pingType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
