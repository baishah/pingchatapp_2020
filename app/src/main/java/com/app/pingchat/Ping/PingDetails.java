package com.app.pingchat.Ping;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.R;
import com.bumptech.glide.Glide;

public class PingDetails extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ping_detail_layout);

        Intent i = getIntent();
        String location = i.getStringExtra("location");
        String image = i.getStringExtra("image");

        TextView t = (TextView) findViewById(R.id.locationame);
        ImageView y = (ImageView) findViewById(R.id.pingimage);

        Glide.with(this).load(image).into(y);
        t.setText(location);
    }
}
