package com.app.pingchat.Ping;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PingAdapter extends RecyclerView.Adapter<PingAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference metadata;
    Context context;
    ArrayList<pingMetaData> pingMetaDataList;
    pingrecycleradapterlist plistlistener;
    String currentuser;


    public PingAdapter(ArrayList<pingMetaData> pingMetaData){
        pingMetaDataList = pingMetaData;
    }

    public PingAdapter(Context c, ArrayList<pingMetaData> d,String user,pingrecycleradapterlist clicklistener){
        context = c;
        pingMetaDataList = d;
        currentuser = user;
        plistlistener = clicklistener;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.ping_selector_layout,parent,false);
        return new MyViewHolder(v,plistlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final pingMetaData newping = pingMetaDataList.get(position);
        if(newping.getPingSenderID().equals(currentuser)){
            holder.lock_unlock.setVisibility(View.GONE);
        }
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(newping.getSenderimage());
        GlideApp.with(context.getApplicationContext())
                .load(storageReference)
                .into(holder.i);
    }

    @Override
    public int getItemCount() {
        return pingMetaDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CircleImageView i;
        ImageView lock_unlock;
        pingrecycleradapterlist plistlistener;
        public MyViewHolder(View itemView,pingrecycleradapterlist clicklistener) {
            super(itemView);
            i = itemView.findViewById(R.id.user_dp);
            lock_unlock = itemView.findViewById(R.id.lock_unlock);
            plistlistener = clicklistener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            plistlistener.onUserClickedpinglist(getAdapterPosition());
        }
    }

    public interface pingrecycleradapterlist{
        void onUserClickedpinglist(int position);
    }
}