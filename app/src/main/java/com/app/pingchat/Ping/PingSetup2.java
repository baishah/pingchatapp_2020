package com.app.pingchat.Ping;
import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.EditProfileActivity;
import com.app.pingchat.Friend.Friends;
import com.app.pingchat.R;
import com.app.pingchat.filterActivitity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PingSetup2 extends AppCompatActivity implements PingSetup2Adapter.OnRecyclerViewItemClickListener{

    FirebaseAuth mAuth;
    private DatabaseReference userfriends,users,pingsession,notificationmessage,userPing;
    private FirebaseStorage storage;
    private StorageReference storeref;
    private RecyclerView friendlist;
    private String current_user;
    private RadioGroup radiogroup;
    private CheckBox checkBox;
    private TextView mTextView,addpicture,posttype;
    private EditText mEditText;
    private ImageButton back;
    private Button sendPing;
    PingSetup2Adapter itemArrayList = new PingSetup2Adapter();
    public String ping_loc_name,caption;
    public double pinglat,pinglon;
    public StorageReference pingimage;
    public Uri fileUri;
    public byte[] data;
    boolean availableping = true;
    public static Context contextOfApplication;
    public ImageView testingbitmapimage;
    private byte[] avatar;
    private PingSetup2Adapter pingSetup2Adapter;
    ArrayList<Friends> listoffriend;
    private ArrayList<String> yuyu=new ArrayList<>();
    public boolean clickbox = false;

    ProgressBar progressBar;
    private SwitchCompat toggle;

    public ArrayList<String> arraystring = new ArrayList<>();
    int selectedItemCount = 0;

    private long timeend;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ping_setup_2_layout);

        contextOfApplication = getApplicationContext();
        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        storeref = storage.getReference();
        current_user = mAuth.getCurrentUser().getUid();

        userfriends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Conversation").child("Personal");
        users = FirebaseDatabase.getInstance().getReference().child("User");
        pingsession = FirebaseDatabase.getInstance().getReference().child("PingSession").child("Friend");
        pingimage = FirebaseStorage.getInstance().getReference().child("pingImage");
        notificationmessage = FirebaseDatabase.getInstance().getReference().child("Notification").child("NewPing");
        userPing = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("userPing");

        testingbitmapimage = findViewById(R.id.bitmapimage);

        Intent intent = getIntent();
        if(intent!=null){
            Bundle d = intent.getExtras();
            if(d!=null){
                pinglat = d.getDouble("lat");
                pinglon = d.getDouble("lon");
            }
        }

        ping_loc_name = intent.getStringExtra("pinglocname");
        checkBox = (CheckBox) findViewById(R.id.select_all);

        friendlist = findViewById(R.id.pingsetupfriend);
        friendlist.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        mTextView = findViewById(R.id.sentenceCount);
        mEditText = findViewById(R.id.yourcaption);
        mEditText.addTextChangedListener(mTextEditorWatcher);
        addpicture = findViewById(R.id.addpic);
        progressBar = findViewById(R.id.progresssbar);
        toggle = findViewById(R.id.toggle);
        posttype = findViewById(R.id.posttype);

        sendPing = findViewById(R.id.done);
        back = findViewById(R.id.backbutton);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addpicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
                }
                else{
                    CropImage.startPickImageActivity(PingSetup2.this);
                }
               //loadImageLibrary();
            }
        });

        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                if(isChecked){
                    posttype.setText("public");
                }
                else{
                    posttype.setText("friend");
                }
            }
        });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox.isChecked()){
                    System.out.println("clicked");
                    pingSetup2Adapter.getallfriend();
                    clickbox=true;
                    pingSetup2Adapter.setclicked(clickbox);
                }
                else{
                    System.out.println("unclicked");
                    pingSetup2Adapter.removeallfriend();
                }
            }
        });

        sendPing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            caption = mEditText.getText().toString().trim();

            if((getAvatar()!=null)&&(caption!=null && !caption.isEmpty())){
             //   checkping();
                popuppinglocation();
            }
            else{
                Toast.makeText(PingSetup2.this,"Please fill in all the information",Toast.LENGTH_SHORT).show();
            }
        }
        });
        findViewById(R.id.codiLayout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            return false;
            }
        });
    }

    @Override
    public void selectedItemCount(int count) {
        selectedItemCount = count;

    }
    private void popuppinglocation(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(PingSetup2.this);
        final AlertDialog alertDialog = builder.create();
        View view = LayoutInflater.from(PingSetup2.this).inflate(R.layout.ping_confirmation_layout,null);

        TextView location = view.findViewById(R.id.pingaddress);
        Button cancel = view.findViewById(R.id.cancel);
        Button ok = view.findViewById(R.id.ok);
        String alert = "Your Ping will be located at "+ping_loc_name+" and only valid for 24 hours.";
        location.setText(alert);
        System.out.println("location :" + ping_loc_name);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            alertDialog.dismiss();
            newping();
            Log.e("itemArrayList " ,"----"+itemArrayList);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Toast.makeText(PingSetup2.this,"cancel clicked",Toast.LENGTH_SHORT).show();
            alertDialog.dismiss();
            }
        });
        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //This sets a textview to the current length
            mTextView.setText(String.valueOf(500-s.length()));
        }
        public void afterTextChanged(Editable s) {
        }
    };

    public long gettimeexpired(long timestart){

        final Date d = new Date(timestart);
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.add(Calendar.DATE,1);
        String jj = calendar.getTime().toString();
        String hh = dateFormat.format(new Date(jj));

        try{
            Date date = dateFormat.parse(hh);
            long unixtime =(long)date.getTime()/1000;

            timeend = unixtime*1000L;
        }
        catch (Exception e){
        }
        return timeend;
    }

    public void newping(){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Uploading new Ping..");
        dialog.show();

        String posttypess = posttype.getText().toString();
        String pingid = userPing.push().getKey();
        String imageurl = "pingImage/"+pingid+".jpg";

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("timestamp start ping: "+timestamp.getTime());
        System.out.println("timestamp end ping: "+gettimeexpired(timestamp.getTime()));

        final Map UP_Details = new HashMap();
        UP_Details.put("UPD_pingID",pingid);
        UP_Details.put("UPD_type",posttypess);
        UP_Details.put("UPD_address",ping_loc_name);
        UP_Details.put("UPD_caption",caption);
        UP_Details.put("UPD_dateStart",timestamp.getTime());
        UP_Details.put("UPD_dateEnd",gettimeexpired(timestamp.getTime()));
        UP_Details.put("UPD_ownerID",current_user);
        UP_Details.put("UPD_mediaType","image");
        UP_Details.put("UPD_mediaURL",imageurl);

        final Map UP_Location = new HashMap();
        UP_Location.put("UPL_longitude",pinglon);
        UP_Location.put("UPL_latitude",pinglat);

        final Map UP_Available = new HashMap();
        UP_Available.put(pingid,posttypess);

        StorageReference filepath = pingimage.child(pingid+".jpg");
        UploadTask uploadTask = filepath.putBytes(getAvatar());
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(PingSetup2.this, "Please try again later", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                userPing.child("UP_List").child(pingid).child("UP_Details").setValue(UP_Details);
                userPing.child("UP_List").child(pingid).child("UP_Location").setValue(UP_Location);

                if(posttypess.equals("public")){
                    DatabaseReference publicPing = FirebaseDatabase.getInstance().getReference().child("PublicPing");
                    publicPing.child(pingid).child("PP_ownerID").setValue(current_user);
                }

                userPing.child("UP_Available").child(pingid).setValue(posttypess).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        sendnotification(pingid);
                        sendPingComplete();
                    }
                });
            }
        });
    }

    public void sendPingComplete() {
        Intent intent = new Intent(getApplicationContext(), CompleteSendPing.class);
        intent.putExtra("type", "ping");
        startActivity(intent);
        finish();
    }

    public void sendnotification(String pingid){
        long unixtime = System.currentTimeMillis() / 1000L;
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://api.pingchat.app/notification/v3/sendPingFriends/";

        StringRequest sq = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {}
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
            protected Map<String,String> getParams(){
                Map<String,String> parr = new HashMap<String, String>();
                parr.put("pingID", pingid);
                return parr;
            }
        };
        sq.setRetryPolicy(new DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(sq);

    }

    public void loadImageLibrary(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setBorderLineColor(Color.BLUE)
                .setBorderLineThickness(1)
                .setFixAspectRatio(true)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = contextOfApplication.getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it

                    testingbitmapimage.setImageBitmap(selectedImage);
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    setAvatar(datas);

                    Intent i = new Intent(this, filterActivitity.class);
                    i.putExtra("Image",datas);
                    startActivityForResult(i,100);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == 100){
            if(resultCode == Activity.RESULT_OK){
                byte[] bytes = data.getByteArrayExtra("PUBLIC_STRING_IDENTIFIER");
                Bitmap b = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                setAvatar(bytes);
                testingbitmapimage.setImageBitmap(b);
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(this,data);

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(PingSetup2.this);
        }
    }

    public byte[] getAvatar() { return avatar; }
    public void setAvatar(byte[] avatar) { this.avatar = avatar; }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}




