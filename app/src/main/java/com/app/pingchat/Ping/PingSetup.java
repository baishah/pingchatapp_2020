package com.app.pingchat.Ping;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Chat.ChatsActivity;
import com.app.pingchat.GlideApp;
import com.app.pingchat.PlaceFieldSelector;
import com.app.pingchat.R;
import com.app.pingchat.filterActivitity;
import com.app.pingchat.location;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.media.MediaRecorder.VideoSource.CAMERA;


public class PingSetup extends AppCompatActivity implements OnMapReadyCallback,GoogleApiClient.OnConnectionFailedListener{

    private TextView uname,type,photo,video,cancel,location_box;
    private FloatingActionButton proceedping;
    private ImageView photo_p_holder, thumbV;
    private FirebaseAuth mAuth;
    private CircleImageView dp;
    private GoogleMap map;
    private byte[] avatar;
    String videoByte;
    private long timeend;
    private EditText caption;
    private Button done;
    private VideoView videoView;

    private DatabaseReference userPing;
    public StorageReference pingimage, pingVideoThumb, pingVideo;

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private BitmapDescriptor icon;

    String pingloc,captions;
    double pinglat,pinglon;


    public void setPingloc(String loc) {
        this.pingloc = loc;
    }
    public String getPingloc() {
        return pingloc;
    }

    public void setPinglat(double lat){this.pinglat = lat;}
    public double getPinglat(){return pinglat; }

    public void setPinglon(double lon){this.pinglon = lon;}
    public double getPinglon(){return pinglon; }

    private Location mLastLocation;
    private GoogleApiClient googleApiClient;

    DatabaseReference locations;
    String currentuser;

    private BottomSheetBehavior bottomSheetBehavior;
    private View bottom_sheet_ping_detail;

    private String api_key = "AIzaSyDOewqTvU226Fe7ZTYIobI58oJomTnPIMQ";
    private int GALLERY = 2, CAMERA = 3;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAnimation();
        setContentView(R.layout.ping_setup_layout);

        mAuth = FirebaseAuth.getInstance();
        currentuser = mAuth.getCurrentUser().getUid();
        userPing = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userPing");
        pingimage = FirebaseStorage.getInstance().getReference().child("pingImage");
        pingVideo = FirebaseStorage.getInstance().getReference().child("pingVideo");
        pingVideoThumb = FirebaseStorage.getInstance().getReference().child("pingVideoThumbnails");
        bottom_sheet_ping_detail = findViewById(R.id.bottom_ping_detail);
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_ping_detail);

        cancel = (TextView) findViewById(R.id.cancel);
        uname = findViewById(R.id.username);
        dp = findViewById(R.id.user_dp);
        type = findViewById(R.id.type);
        photo = findViewById(R.id.photo);
        video = findViewById(R.id.video);
        photo_p_holder = findViewById(R.id.photo_p_holder);
        thumbV= findViewById(R.id.video_p_holder);
        caption = findViewById(R.id.yourcaption);
        done = findViewById(R.id.done);
        videoView = findViewById(R.id.videoView);

        proceedping = findViewById(R.id.proceedping);
        location_box = findViewById(R.id.location_box);
        icon = BitmapDescriptorFactory.fromResource(R.drawable.locked_ping);

        ImageButton search = findViewById(R.id.search);
        Intent intent = getIntent();

        uname.setText(intent.getStringExtra("username"));

        String avatar = intent.getStringExtra("avatar");

        if(avatar.startsWith("p")){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
            if(getApplicationContext()!=null){
                GlideApp.with(getApplicationContext())
                        .load(storageReference)
                        .into(dp);
            }
        }
        else if(avatar.startsWith("h")){
            if(getApplicationContext()!=null){
                GlideApp.with(getApplicationContext().getApplicationContext())
                        .load(avatar)
                        .into(dp);
            }
        }

        type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.getText() == "Public"){
                    type.setText("Friend");
                }
                else{
                    type.setText("Public");
                }
            }
        });

        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
                }
                else{
                    CropImage.startPickImageActivity(PingSetup.this);
                }
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
//                    CropImage.startPickImageActivity(PingSetup.this);
//                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
//                    startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
//                }else {
//                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
//                    startActivity(intent);
//                }
            }
        });
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        photo_p_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(PingSetup.this);
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captions = caption.getText().toString().trim();
                if((getAvatar()!=null||getVideoByte()!=null)&&(captions!=null && !captions.isEmpty())&&(getPingloc()!=null || !getPingloc().isEmpty())){
                    checkping();
                }
                else{
                    Toast.makeText(PingSetup.this,"Please fill in all the information",Toast.LENGTH_SHORT).show();
                }
            }
        });

        Places.initialize(getApplicationContext(),api_key);

        if (!Places.isInitialized()) {
            Places.
                    initialize(getApplicationContext(), api_key);
        }

        PlaceFieldSelector fieldSelector = new PlaceFieldSelector();

        locations = FirebaseDatabase.getInstance().getReference().child("Users");
        search.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN,fieldSelector.getAllFields()).build(PingSetup.this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select video from gallery",
                "Record video from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                chooseVideoFromGallery();
                                break;
                            case 1:
                                takeVideoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void chooseVideoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takeVideoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;


        proceedping.hide();

        proceedping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                map.clear();
                map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("location :"+latLng)
                        .icon(icon)
                        .draggable(true));
                setPingloc(getCompleteAddressString(latLng.latitude,latLng.longitude));
                location_box.setText(getCompleteAddressString(latLng.latitude,latLng.longitude));
                setPinglat(latLng.latitude);
                setPinglon(latLng.longitude);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                proceedping.show();
            }
        });
        displayuserLocation();
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        if (requestCode ==PLACE_AUTOCOMPLETE_REQUEST_CODE){
            if (resultCode == RESULT_OK) {
                Place place;
                place = Autocomplete.getPlaceFromIntent(data);
                String place2 = place.getAddress();
                setPingloc(place2);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
                map.clear();
                map.addMarker(new MarkerOptions()
                        .position(place.getLatLng())
                        .title("location :"+place.getLatLng())
                        .icon(icon)
                        .draggable(true));
                setPingloc(getCompleteAddressString(place.getLatLng().latitude,place.getLatLng().longitude));
                location_box.setText(getCompleteAddressString(place.getLatLng().latitude,place.getLatLng().longitude));
                setPinglat(place.getLatLng().latitude);
                setPinglon(place.getLatLng().longitude);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                proceedping.show();

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(getIntent());
                // TODO: Handle the error.
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = getApplicationContext().getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it

                    photo.setVisibility(View.GONE);
                    photo_p_holder.setVisibility(View.VISIBLE);
                    photo_p_holder.setImageBitmap(selectedImage);
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    setAvatar(datas);

                    Intent i = new Intent(this, filterActivitity.class);
                    i.putExtra("Image",datas);
                    startActivityForResult(i,100);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == 100){
            if(resultCode == Activity.RESULT_OK){
                byte[] bytes = data.getByteArrayExtra("PUBLIC_STRING_IDENTIFIER");
                Bitmap b = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                setAvatar(bytes);
                photo.setVisibility(View.GONE);
                photo_p_holder.setImageBitmap(b);
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(this,data);

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(this);
        }
        else if (requestCode == GALLERY) {
            Log.d("what","gale");
//                video.setVisibility(View.GONE);

            photo.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
            if (data != null) {
                Uri contentURI = data.getData();

                String selectedVideoPath = getPath(getApplicationContext(),contentURI);
                Log.d("path",selectedVideoPath);
                saveVideoToInternalStorage(selectedVideoPath);
                videoView.setVideoURI(contentURI);
                videoView.requestFocus();
                videoView.start();

                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(selectedVideoPath,
                        MediaStore.Images.Thumbnails.MINI_KIND);
                thumbV.setImageBitmap(thumb);
                thumbV.setDrawingCacheEnabled(true);
                thumbV.buildDrawingCache();
                Bitmap bitmap = ((BitmapDrawable) thumbV.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] datasii = baos.toByteArray();
                setAvatar(datasii);
//                    byte[] videoBytes = null;

                setVideoByte(String.valueOf(contentURI));
                System.out.println("isi:"+getVideoByte()+"isi:"+getAvatar());


            }
        } else if (requestCode == CAMERA) {
//                video.setVisibility(View.GONE);
            photo.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
            Uri contentURI = data.getData();
            String recordedVideoPath = getPath(getApplicationContext(),contentURI);
            Log.d("frrr",recordedVideoPath);
            saveVideoToInternalStorage(recordedVideoPath);
            videoView.setVideoURI(contentURI);
            videoView.requestFocus();
            videoView.start();
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(recordedVideoPath,
                    MediaStore.Images.Thumbnails.MINI_KIND);
            thumbV.setImageBitmap(thumb);
            thumbV.setDrawingCacheEnabled(true);
            thumbV.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) thumbV.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] datasii = baos.toByteArray();
            setAvatar(datasii);

            setVideoByte(String.valueOf(contentURI));
            System.out.println("isi:"+getVideoByte()+"isi:"+getAvatar());
        }
    }

    private void saveVideoToInternalStorage (String filePath) {

        File newfile;

        try {

            File currentFile = new File(filePath);
            File wallpaperDirectory = new File(String.valueOf(Environment.getExternalStorageDirectory()));
            newfile = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".mp4");

            if (!wallpaperDirectory.exists()) {
                wallpaperDirectory.mkdirs();
            }
            if(currentFile.exists()){

                InputStream in = new FileInputStream(currentFile);
                OutputStream out = new FileOutputStream(newfile);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                Log.v("vii", "Video file saved successfully.");
            }else{
                Log.v("vii", "Video saving failed. Source file missing.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String getPath( Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Video.Media.DATA};
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }

//    public String getPath(Uri uri) {
//        String[] projection = { MediaStore.Video.Media.DATA };
//        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//        if (cursor != null) {
//            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
//            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
//            int column_index = cursor
//                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } else
//            return null;
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(PingSetup.this);
        }
    }

    private void setAvatar(byte[] bytes) { this.avatar = bytes; }

    public byte[] getAvatar() { return avatar; }
    private void setVideoByte(String videoByte) { this.videoByte = videoByte; }

    public String getVideoByte() { return videoByte; }

    private String getCompleteAddressString(double lat,double lon){
        String strrAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try{
            List<Address> myAddress = geocoder.getFromLocation(lat,lon,1);
            if(myAddress!=null){
                Address returnedAdd = myAddress.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for(int i=0;i<=returnedAdd.getMaxAddressLineIndex();i++){
                    strReturnedAddress.append(returnedAdd.getAddressLine(i)).append("\n");
                }
                strrAdd = strReturnedAddress.toString();
                Log.w("location address", strReturnedAddress.toString());
            }
            else{
                Log.w("location address", "No Address returned!");
            }

        }catch(Exception e){
            e.printStackTrace();
            Log.w("location address", "Cannot get Address!");
        }
        return strrAdd;
    }

    public void displayuserLocation(){
        locations.child(currentuser).child("userStatus").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("US_latitude") && dataSnapshot.hasChild("US_longitude")){
                    String lat = dataSnapshot.child("US_latitude").getValue().toString();
                    String lon = dataSnapshot.child("US_longitude").getValue().toString();

                    if(!(lat.equals("0")&&lon.equals("0"))){
                        final LatLng userloc = new LatLng(Double.parseDouble(lat),Double.parseDouble(lon));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(userloc,15));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void setAnimation() {
        if (Build.VERSION.SDK_INT > 20) {
            Slide slide = new Slide();
            slide.setSlideEdge(Gravity.BOTTOM);
            slide.setDuration(300);
            slide.setInterpolator(new DecelerateInterpolator());
            getWindow().setExitTransition(slide);
            getWindow().setEnterTransition(slide);
        }
    }

    public void newping(){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Uploading new Ping..");
        dialog.show();
        if(getAvatar() != null && getVideoByte() == null){
            System.out.println("DATA: IMAGE");
            String posttypess = type.getText().toString().toLowerCase();
            String pingid = userPing.push().getKey();
            String imageurl = "pingImage/"+pingid+".jpg";

            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println("timestamp start ping: "+timestamp.getTime());
            System.out.println("timestamp end ping: "+ gettimeexpired(timestamp.getTime()));

            final Map UP_Details = new HashMap();
            UP_Details.put("UPD_pingID",pingid);
            UP_Details.put("UPD_type",posttypess);
            UP_Details.put("UPD_address",getPingloc());
            UP_Details.put("UPD_caption",captions);
            UP_Details.put("UPD_dateStart",timestamp.getTime());
            UP_Details.put("UPD_dateEnd",gettimeexpired(timestamp.getTime()));
            UP_Details.put("UPD_ownerID",currentuser);
            UP_Details.put("UPD_mediaType","image");
            UP_Details.put("UPD_mediaURL",imageurl);

            final Map UP_Location = new HashMap();
            UP_Location.put("UPL_longitude",pinglon);
            UP_Location.put("UPL_latitude",pinglat);

            final Map UP_Available = new HashMap();
            UP_Available.put(pingid,posttypess);

            StorageReference filepath = pingimage.child(pingid+".jpg");
            UploadTask uploadTask = filepath.putBytes(getAvatar());
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    dialog.dismiss();
                    Toast.makeText(PingSetup.this, "Please try again later", Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    userPing.child("UP_List").child(pingid).child("UP_Details").setValue(UP_Details);
                    userPing.child("UP_List").child(pingid).child("UP_Location").setValue(UP_Location);

                    if(posttypess.equals("public")){
                        DatabaseReference publicPing = FirebaseDatabase.getInstance().getReference().child("PublicPing");
                        publicPing.child(pingid).child("PP_ownerID").setValue(currentuser);
                    }

                    userPing.child("UP_Available").child(pingid).setValue(posttypess).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            dialog.dismiss();
                            sendPingComplete();
                        }
                    });
                }
            });
        }else {
            System.out.println("DATA: VIDEO");
            String posttypess = type.getText().toString().toLowerCase();
            String pingid = userPing.push().getKey();
//            String imageurl = "pingImage/"+pingid+".jpg";
            String imageUrl = "pingVideoThumbnails/" + pingid + ".jpg";
            String videoUrl = "pingVideo/" + pingid + ".3gp";

            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println("timestamp start ping: " + timestamp.getTime());
            System.out.println("timestamp end ping: " + gettimeexpired(timestamp.getTime()));

            final Map UP_Details = new HashMap();
            UP_Details.put("UPD_pingID", pingid);
            UP_Details.put("UPD_type", posttypess);
            UP_Details.put("UPD_address", getPingloc());
            UP_Details.put("UPD_caption", captions);
            UP_Details.put("UPD_dateStart", timestamp.getTime());
            UP_Details.put("UPD_dateEnd", gettimeexpired(timestamp.getTime()));
            UP_Details.put("UPD_ownerID", currentuser);
            UP_Details.put("UPD_mediaThumbnailURL", imageUrl);
            UP_Details.put("UPD_mediaType", "video");
            UP_Details.put("UPD_mediaURL", videoUrl);

            final Map UP_Location = new HashMap();
            UP_Location.put("UPL_longitude", pinglon);
            UP_Location.put("UPL_latitude", pinglat);

            final Map UP_Available = new HashMap();
            UP_Available.put(pingid, posttypess);

            StorageReference videopath = pingVideo.child(pingid + ".3gp");

            if (getAvatar() != null) {
                StorageReference filepath = pingVideoThumb.child(pingid + ".jpg");
                UploadTask uploadTask = filepath.putBytes(getAvatar());
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        Toast.makeText(PingSetup.this, "Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if(getVideoByte()!=null){
                            UploadTask uploadvideo = videopath.putFile(Uri.parse(getVideoByte()));
                            uploadvideo.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    userPing.child("UP_List").child(pingid).child("UP_Details").setValue(UP_Details);
                                    userPing.child("UP_List").child(pingid).child("UP_Location").setValue(UP_Location);

                                    if (posttypess.equals("public")) {
                                        DatabaseReference publicPing = FirebaseDatabase.getInstance().getReference().child("PublicPing");
                                        publicPing.child(pingid).child("PP_ownerID").setValue(currentuser);
                                    }

                                    userPing.child("UP_Available").child(pingid).setValue(posttypess).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            dialog.dismiss();
                                            sendPingComplete();
                                        }
                                    });
                                    dialog.dismiss();
                                }
                            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    double p = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                                    dialog.setMessage((int) p+" % Uploading...");
                                }
                            });
                        }
                        else{
                            Toast.makeText(PingSetup.this,"nothing to upload",Toast.LENGTH_LONG).show();
                        }

                    }
                });
            }
        }
    }

    public long gettimeexpired(long timestart){

        final Date d = new Date(timestart);
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.add(Calendar.DATE,1);
        String jj = calendar.getTime().toString();
        String hh = dateFormat.format(new Date(jj));

        try{
            Date date = dateFormat.parse(hh);
            long unixtime =(long)date.getTime()/1000;

            timeend = unixtime*1000L;
        }
        catch (Exception e){
        }
        return timeend;
    }

    public void sendPingComplete() {
        Intent intent = new Intent(getApplicationContext(), CompleteSendPing.class);
        intent.putExtra("type", "ping");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void checkping(){
        DatabaseReference rootref = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser);
        rootref.child("userPing").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UP_Available")){
                    Toast.makeText(PingSetup.this,"User only allowed to send one ping within 24 hours",Toast.LENGTH_SHORT).show();
                }
                else{
                    newping();
                    System.out.println("tiada ping");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }
}
