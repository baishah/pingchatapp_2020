package com.app.pingchat.Ping;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;

import java.util.HashMap;
import java.util.Map;

public class CompleteSendPing extends AppCompatActivity {

    Button gotomap;
    TextView pingText;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complete_send_ping_layout);




        Intent i = getIntent();
        String pingid = i.getStringExtra("pingid");
        String type = i.getStringExtra("type");


     //   sendnotification(pingid);

        gotomap = findViewById(R.id.gotomap);
        pingText= findViewById(R.id.ping_text);

        if(type.equals("ping")){
            pingText.setText("Your Ping has been sent!");
            gotomap.setText("View Ping on Map");
        }else{
            pingText.setText("Your Alert has been sent!");
            gotomap.setText("View Alert on Map");
        }

        gotomap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void sendnotification(String pingid){
        long unixtime = System.currentTimeMillis() / 1000L;
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://api.pingchat.app/notification/v3/sendPingFriends/";

        StringRequest sq = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {}
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
            protected Map<String,String> getParams(){
                Map<String,String> parr = new HashMap<String, String>();
                parr.put("pingID", pingid);
                return parr;
            }
        };
        sq.setRetryPolicy(new DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(sq);
    }

    @Override
    public void onBackPressed(){
//        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
