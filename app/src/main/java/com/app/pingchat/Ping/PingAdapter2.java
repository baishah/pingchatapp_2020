package com.app.pingchat.Ping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PingAdapter2 extends RecyclerView.Adapter<PingAdapter2.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference metadata;
    Context context;
    ArrayList<UP_DetailsClass> pingMetaDataList;
    pingrecycleradapterlist plistlistener;
    String currentuser;


    public PingAdapter2(ArrayList<UP_DetailsClass> pingMetaData){
        pingMetaDataList = pingMetaData;
    }

    public PingAdapter2(Context c, ArrayList<UP_DetailsClass> d, String user, pingrecycleradapterlist clicklistener){
        context = c;
        pingMetaDataList = d;
        currentuser = user;
        plistlistener = clicklistener;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.ping_selector_layout,parent,false);
        return new MyViewHolder(v,plistlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final UP_DetailsClass newping = pingMetaDataList.get(position);
        if(newping.getUPD_ownerID().equals(currentuser)){
            holder.lock_unlock.setVisibility(View.GONE);
        }
        if(newping.getSenderImage().startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(newping.getSenderImage())
                    .into(holder.i);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(newping.getSenderImage());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }
    }

    @Override
    public int getItemCount() {
        return pingMetaDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CircleImageView i;
        ImageView lock_unlock;
        pingrecycleradapterlist plistlistener;
        public MyViewHolder(View itemView,pingrecycleradapterlist clicklistener) {
            super(itemView);
            i = itemView.findViewById(R.id.user_dp);
            lock_unlock = itemView.findViewById(R.id.lock_unlock);
            plistlistener = clicklistener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            plistlistener.onUserClickedpinglist(getAdapterPosition());
        }
    }

    public interface pingrecycleradapterlist{
        void onUserClickedpinglist(int position);
    }

    public int getposition(String valuea){
        int length = pingMetaDataList.size();
        for (int i =0;i<length;i++){
            if(pingMetaDataList.get(i).getUPD_pingID().equals(valuea)){
                return i;
            }
        }
        return -1;
    }
}