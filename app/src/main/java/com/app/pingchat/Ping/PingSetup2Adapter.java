package com.app.pingchat.Ping;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.Friends;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PingSetup2Adapter extends RecyclerView.Adapter<PingSetup2Adapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    Context context;
    ArrayList<Friends> listoffriend;
    public ArrayList<String> itemArrayList = new ArrayList<>();
    SparseBooleanArray sparseBooleanArray;
    OnRecyclerViewItemClickListener listener;
    int selectedItemCount;
    String id;
    boolean clicked=true;


    public PingSetup2Adapter(Context c, ArrayList<Friends> f,ArrayList<String> itemArrayList,boolean clicked){
        context = c;
        listoffriend = f;
        sparseBooleanArray = new SparseBooleanArray();
        this.listener = listener;
        this.itemArrayList = itemArrayList;
        this.clicked = clicked;
        selectedItemCount = 0;
    }

    public PingSetup2Adapter(){
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.ping_friend_setup_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();

        final String currentuser = mAuth.getCurrentUser().getUid();

        final Friends myfriend = listoffriend.get(position);

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(myfriend.getAvatarURL());
        GlideApp.with(context.getApplicationContext())
                .load(storageReference)
                .into(holder.i);
        //Picasso.with(context.getApplicationContext()).load(myfriend.getAvatarURL()).into(holder.i);

        if(sparseBooleanArray.get(position)){
            holder.d.setVisibility(View.VISIBLE);
            holder.s.setVisibility(View.VISIBLE);
        }
        else{
            holder.d.setVisibility(View.INVISIBLE);
            holder.s.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return listoffriend.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ImageView d,s;
        CircleImageView i;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);
            d = itemView.findViewById(R.id.bg_done);
            i = itemView.findViewById(R.id.user_dp);
            s = itemView.findViewById(R.id.selected);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (!sparseBooleanArray.get(getAdapterPosition()))
            {
                sparseBooleanArray.put(getAdapterPosition(),true);
                itemArrayList.add(listoffriend.get(getAdapterPosition()).getUserID());
                Log.e("itemArrayList " ,"----"+itemArrayList);
                setArraylist(itemArrayList);
                notifyItemChanged(getAdapterPosition());
            }
            else // if clicked item is already selected
            {
                sparseBooleanArray.put(getAdapterPosition(),false);
                itemArrayList.remove(listoffriend.get(getAdapterPosition()).getUserID());
                Log.e("itemArrayList " ,"----"+itemArrayList);
                setArraylist(itemArrayList);
                notifyItemChanged(getAdapterPosition());
            }
            setArraylist(getItemArraylist());
            System.out.println("fet"+getItemArraylist());
        }

    }

    public interface OnRecyclerViewItemClickListener{
        public void selectedItemCount(int count);
    }

    public void setArraylist(ArrayList<String> set){
        this.itemArrayList = set;
    }

    public ArrayList<String> getItemArraylist() {
        System.out.println("dalam getitemarraylist"+itemArrayList);
        return itemArrayList;
    }

    public ArrayList<String> getallfriend() {
        for(int i=0;i<listoffriend.size();i++){
            itemArrayList.add(listoffriend.get(i).getUserID().toString());
        }
        System.out.println(itemArrayList);
        return itemArrayList;
    }

    public boolean getclicked(){
        return clicked;
    }

    public void setclicked(boolean c){
        c=clicked;
    }

    public ArrayList<String> removeallfriend() {
        if(itemArrayList.size()!=0){
            itemArrayList = new ArrayList<>();
            System.out.println("itemarraylist "+itemArrayList.size());
        }
        else{
            System.out.println("tida payah buat apa2");
        }
        return itemArrayList;
    }

    private void highlight(MyViewHolder holder)
    {
        holder.d.setVisibility(View.VISIBLE);
        holder.s.setVisibility(View.VISIBLE);
    }
}
