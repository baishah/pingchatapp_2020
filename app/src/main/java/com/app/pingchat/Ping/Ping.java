package com.app.pingchat.Ping;

import com.app.pingchat.location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Ping {

    String Status;
    ArrayList<pingMetaData> pingMetaData;
    ArrayList<location> locations;
    public Ping(){}

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public ArrayList<pingMetaData> getPingMetaData() {
        return pingMetaData;
    }

    public void setPingMetaData(ArrayList<pingMetaData> pingMetaData) {
        this.pingMetaData = pingMetaData;
    }

    public ArrayList<location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<location> locations) {
        this.locations = locations;
    }



}
