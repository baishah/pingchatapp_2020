package com.app.pingchat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;

public class SelectedItem extends AppCompatActivity {

    LinearLayout sliderDotspanel;
    ImageButton chat_btn,share_btn;
    public int dotscount;
    private ImageView[] dots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pick_item_layout);

        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);
        ViewPager viewPager = findViewById(R.id.slider);
        ImageAdapter imageAdapter = new ImageAdapter(this);
        viewPager.setAdapter(imageAdapter);

        dotscount = imageAdapter.getCount();
        dots = new ImageView[dotscount];

        chat_btn = (ImageButton) findViewById(R.id.message);

        chat_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  startActivity(new Intent(SelectedItem.this, MessageActivity.class));
            }
        });
        share_btn = (ImageButton) findViewById(R.id.share);

        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // startActivity(new Intent(SelectedItem.this, ShareActivity.class));
            }
        });


        for(int i = 0; i < dotscount; i++){

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dots));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dots));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dots));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dots));

            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//
        ArrayList<adsModel> items = new ArrayList<>();
        CustomAdapter adapter = new CustomAdapter(this, items);

        RecyclerView recyclerView = findViewById(R.id.ads_img_recyler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        recyclerView.setAdapter(adapter);

        items.add(new adsModel(R.drawable.image1, "Product Name"));
        items.add(new adsModel(R.drawable.image2, "Product Name2"));
        items.add(new adsModel(R.drawable.image3, "Product Name3"));
        items.add(new adsModel(R.drawable.image4, "Product Name4"));
        items.add(new adsModel(R.drawable.image5, "Product Name5"));


        // let's create 10 random items

//        for (int i = 0; i < 10; i++) {
//
////            items.add(new adsModel(R.drawable.image1, "Product Name "));
//            items.add(new adsModel(R.drawable.image1, ));
//            adapter.notifyDataSetChanged();
//        }

    }

    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}