package com.app.pingchat;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.ArrayList;

public class NearbyMarketAdapter  extends RecyclerView.Adapter<NearbyMarketAdapter.MarketViewHolder> {
    private Context mContext;
    private ArrayList<MarketItem> mMarketList;
    private MarketAdapter.OnItemClickListener mListener;


    public interface OnItemClickListener{
        void onItemClick(int position,String pID,String pOwner);
    }
    public void setOnItemClickListener(MarketAdapter.OnItemClickListener listener){
        mListener = listener;
    }

    public NearbyMarketAdapter(Context context, ArrayList<MarketItem> marketList){
        mContext = context;
        mMarketList = marketList;
    }

    @NonNull
    @Override
    public NearbyMarketAdapter.MarketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.friend_selector_layout, parent, false);
        return new NearbyMarketAdapter.MarketViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NearbyMarketAdapter.MarketViewHolder marketViewHolder, int position) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(mContext);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(20f);
        circularProgressDrawable.setBackgroundColor(R.color.colorPrimary);
        circularProgressDrawable.start();

        final ProgressDialog progressDialog = new ProgressDialog(mContext);
//        progressDialog.setMessage("Loading MarketPlace list");
//        progressDialog.show();

        MarketItem currentItem = mMarketList.get(position);
        String imageUrl = currentItem.getmImageUrl();
        String pname = currentItem.getmName();

        marketViewHolder.mName.setText(pname);

//        new DownloadImageTask(marketViewHolder.mImageView)
//                .execute(imageUrl);
//
//
//        GlideApp.with(mContext.getApplicationContext())
//                .load(imageUrl)
//                .fitCenter()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .placeholder(circularProgressDrawable)
//                .into(marketViewHolder.mImageView);

        GlideApp.with(mContext)
                .load(imageUrl)

                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressDialog.dismiss();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressDialog.dismiss();
                        return false;
                    }
                })
                .placeholder(circularProgressDrawable)
                .into(marketViewHolder.mImageView);

            marketViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(position,currentItem.getmPID(),currentItem.getmPowner());
                }
            });


//        Picasso.with(mContext).load(imageUrl).fit().centerInside().into(marketViewHolder.mImageView);

    }

    @Override
    public int getItemCount() {
        System.out.println(mMarketList);
        return mMarketList.size();
    }

    public class MarketViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mName;


        public MarketViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.user_dp);
            mName = itemView.findViewById(R.id.display_name);
//            mPrice = itemView.findViewById(R.id.productprice);
//            mDesc = itemView.findViewById(R.id.productdetails);


//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mListener != null){
//                        int position = getAdapterPosition();
//                        if (position != RecyclerView.NO_POSITION){
//                            mListener.onItemClick(position);
//                        }
//
//                    }
//                }
//            });
        }
    }

//    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//        ImageView bmImage;
//
//        public DownloadImageTask(ImageView bmImage) {
//            this.bmImage = bmImage;
//        }
//
//        protected Bitmap doInBackground(String... urls) {
//            String urldisplay = urls[0];
//            Bitmap mIcon11 = null;
//            try {
//                InputStream in = new java.net.URL(urldisplay).openStream();
//                mIcon11 = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//            return mIcon11;
//        }
//
//        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(result);
//        }
//    }
}
