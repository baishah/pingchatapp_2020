package com.app.pingchat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MoreFragment extends Fragment{

    GridLayout gridLayout;
    List<More> moreBtn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_more_fragment, container, false);

        moreBtn = new ArrayList<>();
        moreBtn.add(new More("PingChat",R.drawable.pchat_icon1));
        moreBtn.add(new More("PingPartner",R.drawable.ppart_icon1));
        moreBtn.add(new More("PingAR",R.drawable.ping_ar_icon1));
        moreBtn.add(new More("Events",R.drawable.event_icon1));
        moreBtn.add(new More("Market\nPlace",R.drawable.market_icon1));
        moreBtn.add(new More("Rewards",R.drawable.reward_icon1));
        moreBtn.add(new More("Services",R.drawable.service_icon1));
        moreBtn.add(new More("Donates",R.drawable.donate_icon_1));
        moreBtn.add(new More("Settings",R.drawable.sett_icon1));

        RecyclerView myrv = (RecyclerView)view.findViewById(R.id.morebtnRv);
        moreAdapter myAdapter = new moreAdapter(getContext(),moreBtn);
        myrv.setLayoutManager(new GridLayoutManager(getContext(),3));
        myrv.setHasFixedSize(true);
        myrv.setAdapter(myAdapter);
//        gridLayout=(GridLayout)view.findViewById(R.id.mainGrid);

//        CardView marketplace = view.findViewById(R.id.market_bt);
//        CardView pchat = view.findViewById(R.id.ping_bt);
//        CardView ppay = view.findViewById(R.id.pingpay_bt);
//        CardView pAr = view.findViewById(R.id.ping_ar);
//        CardView QRc = view.findViewById(R.id.qr_bt);
//        CardView rewards = view.findViewById(R.id.reward_bt);
//        CardView donations = view.findViewById(R.id.donate_bt);
//        CardView events = view.findViewById(R.id.event_bt);
//        CardView settings = view.findViewById(R.id.setting_bt);

//        marketplace.setOnClickListener(this);
//
//        pchat.setOnClickListener(this);
//        ppay.setOnClickListener(this);
//        pAr.setOnClickListener(this);
//        QRc.setOnClickListener(this);
//        rewards.setOnClickListener(this);
//        donations.setOnClickListener(this);
//        events.setOnClickListener(this);
//        settings.setOnClickListener(this);

        return view;
    }


//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
////            case R.id.market_bt:
////                Intent intent = new Intent(getContext(), MarketplaceActivity.class);
////                startActivity(intent);
////                break;
////
////            case R.id.setting_bt:
////                Intent i = new Intent(getContext(), Settings2Fragment.class);
////                startActivity(i);
////                break;
////
////            case R.id.donate_bt:
////                Intent i2 = new Intent(getContext(), ServicesActivity.class);
////                startActivity(i2);
////                break;
////
////            case R.id.reward_bt:
////                Intent i3 = new Intent(getContext(), RewardsHomeActivity.class);
////                startActivity(i3);
////                break;
//
//            default:
//                Intent i4 = new Intent(getContext(), UnderDevelopmentActivity.class);
//                startActivity(i4);
//                break;
//        }
//
//    }
}