package com.app.pingchat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.google.ar.core.ArCoreApk;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRcodeViewActivity extends AppCompatActivity {

//    ZXingScannerView qrCodeScanner;
    private RequestQueue mQueue;
    private ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_code_layout);

        mQueue = Volley.newRequestQueue(getApplicationContext());
        Intent i = getIntent();
        String id = i.getStringExtra("id");
        String partner_id = i.getStringExtra("partner_id");
        String qr_code = i.getStringExtra("qr_code");
        RequestQueue queue = Volley.newRequestQueue(this);
        imageView = findViewById(R.id.qrcode);

        Button scanqrcode = findViewById(R.id.scanqr);
        Button myqr = findViewById(R.id.myqr);
        TextView qr_text = findViewById(R.id.qr_text);

//        myqr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(QRcodeViewActivity.this, ArActivity.class);
//                startActivity(intent);
//            }
//        });

        scanqrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QRcodeViewActivity.this, QRcodecamera.class);
                startActivity(intent);
            }
        });

        if(qr_code.equals("user")){
            System.out.println("masuk sini dalam json");
            String url = "https://devbm.ml/users/" + id + "/qr-test";

            final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, new JSONObject(postParams),
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Volley", response.toString());
                            try {
                                String dataimage = response.getString("dataQR");
                                System.out.println("Data Qr: " + dataimage);
                                byte[] imageBytes = dataimage.getBytes();
                                byte[] decodedString = Base64.decode(imageBytes, Base64.DEFAULT);
                                Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                System.out.println("Decoded: " + decodedImage);
                                imageView.setImageBitmap(decodedImage);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("TAG", "Error: " + error.getMessage());

                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mQueue.add(jsonObjReq);
        }else if(qr_code.equals("partner")){
            System.out.println("masuk sini dalam json");
            String url = "https://devbm.ml/users/" + partner_id + "/qr-partner";

            qr_text.setText("MyPartnerQR");

            final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, new JSONObject(postParams),
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Volley", response.toString());
                            try {
                                String dataimage = response.getString("dataQR");
                                System.out.println("Data Qr: " + dataimage);
                                byte[] imageBytes = dataimage.getBytes();
                                byte[] decodedString = Base64.decode(imageBytes, Base64.DEFAULT);
                                Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                System.out.println("Decoded: " + decodedImage);
                                imageView.setImageBitmap(decodedImage);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("TAG", "Error: " + error.getMessage());

                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mQueue.add(jsonObjReq);
        }else{
            Toast.makeText(getApplicationContext(), "No Match found",Toast.LENGTH_LONG).show();
        }


//        System.out.println("masuk sini dalam json");
//        String url = "https://devbm.ml/users/" + id + "/qr-test";
//
//        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
//
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, new JSONObject(postParams),
//                new com.android.volley.Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d("Volley", response.toString());
//                        try {
//                            String dataimage = response.getString("dataQR");
//                            System.out.println("Data Qr: " + dataimage);
//                            byte[] imageBytes = dataimage.getBytes();
//                            byte[] decodedString = Base64.decode(imageBytes, Base64.DEFAULT);
//                            Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//                            System.out.println("Decoded: " + decodedImage);
//                            imageView.setImageBitmap(decodedImage);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d("TAG", "Error: " + error.getMessage());
//
//            }
//        }) {
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }
//        };
//
//        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        mQueue.add(jsonObjReq);
    }

//        String imageserverurl = "https://api.pingchat.app/QR-Code/?userID="+id;
//        String imageserverurl = "https://devbm.ml/users/"+id+"/qr-test";
//        ImageRequest imageRequest = new ImageRequest(imageserverurl, new Response.Listener<Bitmap>() {
//            @Override
//            public void onResponse(Bitmap response) {
//                imageView.setImageBitmap(response);
//                System.out.println("qr:"+response);
//            }
//
//        }, 0, 0, ImageView.ScaleType.CENTER_INSIDE, null, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(QRcodeViewActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
//                error.printStackTrace();
//            }
//        });
//        queue.add(imageRequest);

//    }

    public void maybeEnableArButton() {
        ArCoreApk.Availability availability = ArCoreApk.getInstance().checkAvailability(this);
        if (availability.isTransient()) {
            // Re-query at 5Hz while compatibility is checked in the background.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    maybeEnableArButton();
                }
            }, 200);
        }
        if (availability.isSupported()) {
            Intent intent = new Intent(QRcodeViewActivity.this, ArActivity.class);
            startActivity(intent);
            //mArButton.setVisibility(View.VISIBLE);
//            mArButton.setEnabled(true);
            // indicator on the button.
        } else { // Unsupported or unknown.
//            mArButton.setVisibility(View.INVISIBLE);
//            mArButton.setEnabled(false);
            Toast.makeText(QRcodeViewActivity.this, "This Device is not support ARcore ", Toast.LENGTH_SHORT).show();

        }
    }
//    public void scan(){
//        qrCodeScanner =new ZXingScannerView(this);
//        setContentView(qrCodeScanner);
//        qrCodeScanner.setMinimumHeight(50);
//        qrCodeScanner.setMinimumWidth(50);
//        qrCodeScanner.setResultHandler(v);
//        qrCodeScanner.startCamera();
//
//    }

//    ZXingScannerView.ResultHandler v = new ZXingScannerView.ResultHandler() {
//        @Override
//        public void handleResult(Result result) {
//          //  qrCodeScanner.animate();
//            Toast.makeText(getApplicationContext(),"name: ",Toast.LENGTH_SHORT).show();
//         //   qrCodeScanner.resumeCameraPreview(v);
//            Intent intent = new Intent(QRcodeViewActivity.this, ViewProfileActivityAfterScan.class);
//            startActivity(intent);
//
//        }
//    };

//    @Override public void onResume(){
//        super.onResume();
//        scan();
//    }

    @Override
    public void onBackPressed(){
//        qrCodeScanner.stopCamera();
        finish();
    }
}
