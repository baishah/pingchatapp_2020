package com.app.pingchat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.pingchat.UserAuth.RegisterActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsFragment extends Fragment {

    //Button btn;
    TextView btn;
    TextView name, caption, since,privacysettings,termservice;
    CircleImageView disppic;

    private DatabaseReference userinfo;
    FirebaseAuth mAuth;
    String current_user;
    StorageReference storageRef;
    FirebaseStorage storage;
    String startdate;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.settings2fragment,container,false);
//        btn = (TextView) rootView.findViewById(R.id.logout);
//        name = (TextView) rootView.findViewById(R.id.namaUser);
//        disppic = (CircleImageView) rootView.findViewById(R.id.gambar);
//        caption = (TextView) rootView.findViewById(R.id.myCaption);
//        since = (TextView) rootView.findViewById(R.id.usersince);
//        privacysettings = rootView.findViewById(R.id.privacysettings);
//        termservice = rootView.findViewById(R.id.termofservicesettings);
//        mAuth = FirebaseAuth.getInstance();
//        storage = FirebaseStorage.getInstance();
//        storageRef = storage.getReference();
//        btn.setOnClickListener(this);
//        disppic.setOnClickListener(this);
        return rootView;
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        if(mAuth.getCurrentUser()!=null){
//            current_user = mAuth.getCurrentUser().getUid();
//            userinfo = FirebaseDatabase.getInstance().getReference().child("Users");
//            userinfo.keepSynced(true);
//            getUserInfo();
//            disppic.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    editProfile();
//                }
//            });
//            privacysettings.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent p = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.pingchat.app/privacy-policy/"));
//                    startActivity(p);
//                }
//            });
//            termservice.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent t = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.pingchat.app/terms-of-service/"));
//                    startActivity(t);
//                }
//            });
//        }
//
//    }
//
//    private void getUserInfo(){
//        userinfo.child(current_user).child("metaData").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
//                if((dataSnapshot.exists())&&(dataSnapshot.hasChild("displayName") && (dataSnapshot.hasChild("avatarURL")))){
//                    String retrievename = dataSnapshot.child("displayName").getValue().toString();
//                    String dpicture = dataSnapshot.child("avatarURL").getValue().toString();
//                    String aboutuser = dataSnapshot.child("userAbout").getValue().toString();
//                    String usersince = dataSnapshot.child("userCreation").getValue().toString();
//
//                    String k = gettimestamptodate(usersince);
//                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(dpicture);
//
//                    if(getContext()!=null){
//                        GlideApp.with(getContext().getApplicationContext())
//                                .load(storageReference)
//                                .into(disppic);
//                    }
//                    else{
//                        System.out.println("sini null");
//                    }
//                    name.setText(retrievename);
//                    caption.setText(aboutuser);
//                    since.setText("PingChat user since " + k);
//                }
//                else if ((dataSnapshot.exists())&&(dataSnapshot.hasChild("displayName"))){
//                    String retrievename = dataSnapshot.child("displayName").getValue().toString();
//                    name.setText(retrievename);
//                }
//                else {}
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//            }
//        });
//    }
//
//    public void onClick(View view){
//        switch(view.getId()){
//            case R.id.logout:
//                FirebaseAuth.getInstance().signOut();
//                Intent in = new Intent(getActivity(), RegisterActivity.class);
//                startActivity(in);
//        }
//    }
//
//    public void editProfile() {
//        Intent intent = new Intent(getActivity().getApplicationContext(), EditProfileActivity.class);
//        startActivity(intent);
//    }
//
//    private String gettimestamptodate(String timestamp){
//        long lg = Long.parseLong(timestamp);
//        final Date d = new Date();
//        final SimpleDateFormat sfd = new SimpleDateFormat("dd MMM yyyy");
//
//        String dlmt = sfd.format(new Date(lg)).toString();
//        this.startdate = dlmt;
//        return startdate;
//    }

}
