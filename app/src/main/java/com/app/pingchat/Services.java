package com.app.pingchat;

public class Services {

    public String SD_serviceID;
    public long SD_serviceStartDate;
    public String SD_serviceTitle;
    public String SD_serviceURL;
    public String SD_serviceAddress;
    public String SD_serviceDescription;
    public String SL_longitude;
    public String SL_latitude;
    public String SM_mediaType;
    public String SM_mediaURL;

    public Services() {
    }

    public String getSD_serviceID() {
        return SD_serviceID;
    }

    public void setSD_serviceID(String SD_serviceID) {
        this.SD_serviceID = SD_serviceID;
    }

    public long getSD_serviceStartDate() {
        return SD_serviceStartDate;
    }

    public void setSD_serviceStartDate(long SD_serviceStartDate) {
        this.SD_serviceStartDate = SD_serviceStartDate;
    }

    public String getSD_serviceTitle() {
        return SD_serviceTitle;
    }

    public void setSD_serviceTitle(String SD_serviceTitle) {
        this.SD_serviceTitle = SD_serviceTitle;
    }

    public String getSD_serviceURL() {
        return SD_serviceURL;
    }

    public void setSD_serviceURL(String SD_serviceURL) {
        this.SD_serviceURL = SD_serviceURL;
    }

    public String getSD_serviceAddress() {
        return SD_serviceAddress;
    }

    public void setSD_serviceAddress(String SD_serviceAddress) {
        this.SD_serviceAddress = SD_serviceAddress;
    }

    public String getSD_serviceDescription() {
        return SD_serviceDescription;
    }

    public void setSD_serviceDescription(String SD_serviceDescription) {
        this.SD_serviceDescription = SD_serviceDescription;
    }

    public String getSL_longitude() {
        return SL_longitude;
    }

    public void setSL_longitude(String SL_longitude) {
        this.SL_longitude = SL_longitude;
    }

    public String getSL_latitude() {
        return SL_latitude;
    }

    public void setSL_latitude(String SL_latitude) {
        this.SL_latitude = SL_latitude;
    }

    public String getSM_mediaType() {
        return SM_mediaType;
    }

    public void setSM_mediaType(String SM_mediaType) {
        this.SM_mediaType = SM_mediaType;
    }

    public String getSM_mediaURL() {
        return SM_mediaURL;
    }

    public void setSM_mediaURL(String SM_mediaURL) {
        this.SM_mediaURL = SM_mediaURL;
    }
}
