package com.app.pingchat;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetPartnerActivity extends AppCompatActivity {

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<getpartnerclass> getpartnerclassList;
    private RecyclerView.Adapter adapter;

    private RequestQueue mQueue;

    private String url = "https://api.pingchat.app/partner/getPartner/";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_partner_layout);
        mList = findViewById(R.id.main_list);

        mQueue = Volley.newRequestQueue(this);
        getpartnerclassList = new ArrayList<>();
       // adapter = new getpartneradapter(getApplicationContext(),getpartnerclassList);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);

        //getData();
        //jsonParse();
    }

    private void getData(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        getpartnerclass getpartnerclass = new getpartnerclass();
                        getpartnerclass.setBusinessPartnerID(jsonObject.getString("businessPartnerID"));
                        getpartnerclass.setBusinessCreator(jsonObject.getString("businessCreator"));
                        getpartnerclass.setBusinessName(jsonObject.getString("businessName"));
                        getpartnerclass.setBusinessRegister(jsonObject.getString("businessRegister"));
                        getpartnerclass.setBusinessLastUpdate(jsonObject.getString("businessLastUpdate"));
                        getpartnerclass.setBusinessPhone(jsonObject.getString("businessPhone"));
                        getpartnerclass.setBusinessEmail(jsonObject.getString("businessEmail"));
                        getpartnerclass.setBusinessLatitude(jsonObject.getString("businessLatitude"));
                        getpartnerclass.setBusinessLongitude(jsonObject.getString("businessLongitude"));
                        getpartnerclass.setBusinessCreator(jsonObject.getString("businessActualAddress"));
                        getpartnerclass.setBusinessOnMapAddress(jsonObject.getString("businessOnMapAddress"));
                        getpartnerclass.setBusinessCategory(jsonObject.getString("businessCategory"));
                        getpartnerclass.setBusinessProfileURL(jsonObject.getString("businessProfileURL"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
                adapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley",error.toString());
                progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    private void jsonParse() {

        String url = "https://api.pingchat.app/partner/getPartner/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("merchant");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                //JSONObject jsonObject = response.getJSONObject(i);

                                getpartnerclass getpartnerclass = new getpartnerclass();
                                getpartnerclass.setBusinessPartnerID(jsonObject.getString("businessPartnerID"));
                                getpartnerclass.setBusinessCreator(jsonObject.getString("businessCreator"));
                                getpartnerclass.setBusinessName(jsonObject.getString("businessName"));
                                getpartnerclass.setBusinessRegister(jsonObject.getString("businessRegister"));
                                getpartnerclass.setBusinessLastUpdate(jsonObject.getString("businessLastUpdate"));
                                getpartnerclass.setBusinessPhone(jsonObject.getString("businessPhone"));
                                getpartnerclass.setBusinessEmail(jsonObject.getString("businessEmail"));
                                getpartnerclass.setBusinessLatitude(jsonObject.getString("businessLatitude"));
                                getpartnerclass.setBusinessLongitude(jsonObject.getString("businessLongitude"));
                                getpartnerclass.setBusinessCreator(jsonObject.getString("businessActualAddress"));
                                getpartnerclass.setBusinessCreator(jsonObject.getString("businessOnMapAddress"));
                                getpartnerclass.setBusinessActualAddress(jsonObject.getString("businessCategory"));
                                getpartnerclass.setBusinessProfileURL(jsonObject.getString("businessProfileURL"));

                                getpartnerclassList.add(getpartnerclass);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mQueue.add(request);
    }
}
