package com.app.pingchat;

public class Request {

    private String avatarURL;
    private String displayName;
    private String userAbout;
    private String userID;

    public Request(){

    }

    public Request(String avatarURL, String displayName, String userAbout,String userID) {
        this.avatarURL = avatarURL;
        this.displayName = displayName;
        this.userAbout = userAbout;
        this.userID = userID;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUserAbout() {
        return userAbout;
    }

    public void setUserAbout(String userAbout) {
        this.userAbout = userAbout;
    }

    public String getuserID(){ return userID; }

    public void setUserID(String userID){ this.userID = userID; }
}
