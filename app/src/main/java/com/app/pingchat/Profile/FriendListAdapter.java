package com.app.pingchat.Profile;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.ContactsAdapter;
import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.MyViewHolder> {

    ArrayList<userDetailsClass> mFriendlist;
    Context context;
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,notification, users,friend;

    public FriendListAdapter(ArrayList<userDetailsClass> Friendlist){
        mFriendlist =Friendlist;
    }

    public FriendListAdapter(Context c, ArrayList<userDetailsClass> f){
        context = c;
        mFriendlist = f;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.friendlistitem,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final userDetailsClass mynewfriend = mFriendlist.get(position);

        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        users = FirebaseDatabase.getInstance().getReference().child("Users");
        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();

        final String currentuser = mAuth.getCurrentUser().getUid();

        holder.u.setText(mynewfriend.getUD_displayName());
        holder.s.setText(mynewfriend.getUD_aboutMe());

        if(mynewfriend.getUD_avatarURL().startsWith("p")){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(mynewfriend.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }
        else{
            GlideApp.with(context.getApplicationContext())
                    .load(mynewfriend.getUD_avatarURL())
                    .into(holder.i);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewUserProfileActivity.class);
                i.putExtra("id", mynewfriend.getUD_userID());
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFriendlist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s,p;
        CircleImageView i;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            i = itemView.findViewById(R.id.gambar);

        }
    }
}
