package com.app.pingchat.Profile;

public class Caption {

    private String CaptionText;

    public Caption(){

    }

    public Caption(String captiontext){
        CaptionText = captiontext;
    }

    //Getter

    public String getCaptionText(){
        return CaptionText;
    }

    //Setter
    public void setCaptionText(String captiontext){
        CaptionText = captiontext;
    }
}
