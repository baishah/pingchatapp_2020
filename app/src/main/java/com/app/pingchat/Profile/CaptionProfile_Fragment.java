package com.app.pingchat.Profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CaptionProfile_Fragment extends Fragment {

    View view;
    private RecyclerView recylerView;
    private List<Caption> Caption;

    Caption caption;


    public CaptionProfile_Fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.captionprofile_fragment,container,false);

        Caption = new ArrayList<>();
//        final CaptionRecyclerViewAdapter mAdapter = new CaptionRecyclerViewAdapter(getContext(),Caption);
//        recylerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        recylerView.setAdapter(mAdapter);

        //database
//        firebaseAuth = FirebaseAuth.getInstance();
//        firebaseUser = firebaseAuth.getCurrentUser();
//        mDatabase = FirebaseDatabse.getInstance().getReference();
//
//        mDatabase.child("").child("").addValueListener(new ValueEventListener(){
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot){
//                if(dataSnapshot.exists()){
//                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
//                        String CaptionText = snapshot.child("CaptionText").getValue().toString();
//
//                        Caption.add(new Caption(CaptionText));
//                    }
//                    mAdapter.notifyDataSetChanged();
//                }
//
//                @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError){
//
//                }
//            }
//        });

        return view;
    }
}
