package com.app.pingchat.Profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.CommentLayoutActivity;
import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.Discover.editDiscoverActivity;
import com.app.pingchat.Profile.Caption;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.security.AccessController;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;


public class CaptionRecyclerViewAdapter extends RecyclerView.Adapter<CaptionRecyclerViewAdapter.MyViewHolder> {
    Context mContext;
    ArrayList<UDS_DetailsClass> captionList;
    DatabaseReference users, pDiscover;
    private String datetimes,datess;
    boolean isTextViewClicked = false;
    Boolean clapChecker = false;
    private String currentuser;
    private FirebaseAuth mAuths;
    StorageReference discoverimage;
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());


    public CaptionRecyclerViewAdapter(Context mContext, ArrayList<UDS_DetailsClass> captionList, String y) {
        this.mContext = mContext;
        this.captionList = captionList;
        currentuser = y;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.captionpost, parent, false);
        return new MyViewHolder(itemView);
//        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.captionpost,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final UDS_DetailsClass discovers = captionList.get(position);
        String caption = discovers.getUDSD_caption();
        String onwerID =discovers.getUDSD_ownerID();
        String discoverKey = discovers.getUDSD_discoverID();
        String loc = discovers.getUDSD_address();
        long datePost = discovers.getUDSD_date();
        mAuths = FirebaseAuth.getInstance();
        currentuser = mAuths.getCurrentUser().getUid();
        discoverimage = FirebaseStorage.getInstance().getReference().child("postImage");
        users = FirebaseDatabase.getInstance().getReference().child("Users");
        pDiscover = FirebaseDatabase.getInstance().getReference().child("PublicDiscover");

        if(discovers.getUDSD_mediaType().equals("noMedia")){
            holder.captiontext.setText(caption);
            users.child(onwerID).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.hasChild("UD_displayName")){
                        String name = dataSnapshot.child("UD_displayName").getValue().toString();
                        holder.user.setText(name);
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
            holder.loc.setText(loc);
            holder.date.setText(getdatetime(datePost));

            if(onwerID.equals(currentuser)){
                holder.menu.setVisibility(View.VISIBLE);
            }else{
                holder.menu.setVisibility(View.GONE);
            }
//            holder.onClick(onwerID,discovers.getUDSD_discoverID(), captionList.get(position));
            holder.menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onwerID.equals(currentuser)){

                        users.child(currentuser).child("userDiscovers").child("UDS_List").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
//                                    boolean value = false;
                                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                        String discoveruserkey = dataSnapshot1.getKey();
                                        if (discoveruserkey.equals(discoverKey)) {
//                                            value = true;
                                            System.out.println("post currentuser");
//                                        System.out.println("discoverkey:" + discoveruserkey);
                                            CharSequence option[] = new CharSequence[]
                                                    {
                                                            "Edit",
                                                            "Share",
                                                            "Delete",
                                                            "Cancel"
                                                    };
                                            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(holder.menu.getContext());
                                            builder.setTitle("Menu");
                                            builder.setItems(option, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    if (i == 0) {
//                                                    System.out.println("edit");
                                                        users.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                if (dataSnapshot.exists()) {
                                                                    String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
                                                                    String caption = dataSnapshot.child("UDSD_caption").getValue().toString();

                                                                    Intent intent = new Intent(mContext, editDiscoverActivity.class);
                                                                    intent.putExtra("mediaType", mediaType);
                                                                    intent.putExtra("caption", caption);
                                                                    intent.putExtra("discoverKey", discoverKey);
//                                                                intent.putExtra("discoverUrl", newdiscoverurl);
                                                                    mContext.startActivity(intent);
                                                                }
                                                            }
                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            }
                                                        });
                                                    } else if (i == 1) {
//                                                    System.out.println("share");
                                                        users.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                if (dataSnapshot.exists()) {
                                                                    String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
                                                                    String caption = dataSnapshot.child("UDSD_caption").getValue().toString();
//                                                                    String mediaUrl = dataSnapshot.child("UDSD_mediaURL").getValue().toString();
                                                                    if (mediaType.equals("image")) {
                                                                        discoverimage.child(discoverKey + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                                            @Override
                                                                            public void onSuccess(Uri uri) {
//                                                                            System.out.println("share image");
                                                                                Toast.makeText(mContext, "In development mode", Toast.LENGTH_SHORT).show();
//                                                                                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
//                                                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                                                                intent.putExtra(Intent.EXTRA_STREAM, uri);
//                                                                                intent.setType("image/png");
//                                                                                context.startActivity(Intent.createChooser(intent, "Share image via"));
                                                                            }
                                                                        });
                                                                    } else {
                                                                        Toast.makeText(mContext, "In development mode", Toast.LENGTH_SHORT).show();
//                                                                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                                                                        sharingIntent.setType("text/plain");
//                                                                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, caption);
//                                                                        context.startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
                                                                    }
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });
//
                                                    } else if (i == 2) {
                                                        ProgressDialog dialog = new ProgressDialog(mContext);
                                                        dialog.setMessage("Delete on progress\nPlease wait");
                                                        dialog.show();
//                                                    System.out.println("delete");
                                                        users.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").child("UDSD_status").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                dialog.dismiss();
                                                                Toast.makeText(mContext, "Post deleted!...", Toast.LENGTH_SHORT).show();
                                                                captionList.remove(position);
                                                                notifyDataSetChanged();
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                            builder.show();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
//                    }else{
//
//                        System.out.println("post other user");
////                                        System.out.println("discoverkey:" + discoveruserkey);
//                        CharSequence option[] = new CharSequence[]
//                                {
//                                        "Share",
//                                        "Cancel"
//                                };
//                        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(holder.menu.getContext());
//                        builder.setTitle("Menu");
//                        builder.setItems(option, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                if (i == 0) {
//                                    System.out.println("share");
//                                    users.child(onwerID).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
//                                        @Override
//                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                            if (dataSnapshot.exists()) {
//                                                String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
//                                                String caption = dataSnapshot.child("UDSD_caption").getValue().toString();
//                                                if (mediaType.equals("image")) {
//                                                    discoverimage.child(discoverKey + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//                                                        @Override
//                                                        public void onSuccess(Uri uri) {
//                                                            // Got the download URL for 'users/me/profile.png'
////                                                                 System.out.println("share image");
//////                                                                                                System.out.println("uri:" + uri);
//////                                                                                                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
//////                                                                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//////                                                                                                intent.putExtra(Intent.EXTRA_STREAM, uri);
//////                                                                                                intent.setType("image/png");
//////                                                                                                context.startActivity(Intent.createChooser(intent, "Share image via"));
//                                                            Toast.makeText(mContext, "In development mode", Toast.LENGTH_SHORT).show();
//                                                        }
//                                                    }).addOnFailureListener(new OnFailureListener() {
//                                                        @Override
//                                                        public void onFailure(@NonNull Exception exception) {
//                                                            // Handle any errors
//                                                        }
//                                                    });
//
//                                                } else {
//                                                    Toast.makeText(mContext, "In development mode", Toast.LENGTH_SHORT).show();
////                                                                                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
////                                                                                        sharingIntent.setType("text/plain");
////                                                                                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, caption);
////                                                                                        context.startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
//                                                }
//                                            }
//                                        }
//
//                                        @Override
//                                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                        }
//                                    });
//                                }
//                            }
//                        });
//                        builder.show();
                    }
                }
            });
            holder.comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, CommentLayoutActivity.class);
                    i.putExtra("discoverpostid", discovers.getUDSD_discoverID());
                    i.putExtra("currentuser", currentuser);
                    i.putExtra("ownerid", onwerID);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(i);
                }
            });

            holder.clapStatus(discovers.getUDSD_discoverID(), onwerID);

            holder.unclap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clapChecker = true;
                    users.child(discovers.getUDSD_ownerID()).child("userDiscovers").child("UDS_List").child(discovers.getUDSD_discoverID()).child("UDS_Claps").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (clapChecker.equals(true)) {
                                if (dataSnapshot.hasChild(currentuser)) {
//                                System.out.println("re-clap");
//                                holder.unclap.setVisibility(View.VISIBLE);
//                                holder.clap.setVisibility(View.INVISIBLE);
                                    users.child(onwerID).child("userDiscovers").child("UDS_List").child(discovers.getUDSD_discoverID()).child("UDS_Claps").child(currentuser).removeValue();
                                    clapChecker = false;

                                } else {
//                                System.out.println("new clap");
//                                holder.unclap.setVisibility(View.INVISIBLE);
//                                holder.clap.setVisibility(View.VISIBLE);
                                    final Map UDSclaps = new HashMap();
                                    UDSclaps.put("UDSC_status", false);
                                    UDSclaps.put("UDSC_timestamp", timestamp.getTime());
                                    users.child(onwerID).child("userDiscovers").child("UDS_List").child(discovers.getUDSD_discoverID()).child("UDS_Claps").child(currentuser).setValue(UDSclaps);
                                    clapChecker = false;
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            });
            users.child(onwerID).child("userDiscovers").child("UDS_List").child(discovers.getUDSD_discoverID()).child("UDS_Comments").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        int total = 0;
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            String key = dataSnapshot1.getKey();
//                        System.out.println("key: " + key);
                            total = total + 1;
                        }
                        holder.numbercomment.setText(String.valueOf(total));
                    } else {
                        holder.numbercomment.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else if (discovers.getUDSD_mediaType().equals("image")){
            System.out.println("Media");
        }else{
            System.out.println("else");
//            holder.image.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return captionList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView captiontext,user,loc,date,less,numbercomment, numberclap,clap_text;
        ImageView comment,unclap;
        ImageButton menu;
        int countClaps;

        public MyViewHolder(View itemView) {
            super(itemView);
            captiontext = (TextView) itemView.findViewById(R.id.status2);
            user = itemView.findViewById(R.id.user);
            loc = itemView.findViewById(R.id.loc);
            date = itemView.findViewById(R.id.date);
            less = itemView.findViewById(R.id.lesss);
            comment = itemView.findViewById(R.id.comments);
            unclap = itemView.findViewById(R.id.clap);
            numberclap = itemView.findViewById(R.id.numberclap);
            numbercomment = itemView.findViewById(R.id.numbercomment);
            clap_text = itemView.findViewById(R.id.clap_count_txt);
            menu = itemView.findViewById(R.id.triple_btn);

            captiontext.post(new Runnable() {
                @Override
                public void run() {
                    captiontext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isTextViewClicked) {
                                //This will shrink textview to 2 lines if it is expanded.
                                captiontext.setMaxLines(1);
                                isTextViewClicked = false;
//                                    System.out.println("false");
                                less.setVisibility(View.GONE);
                            } else {
                                //This will expand the textview if it is of 2 lines
                                captiontext.setMaxLines(Integer.MAX_VALUE);
                                isTextViewClicked = true;
//                                    System.out.println("true");
                                less.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    less.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            less.setVisibility(View.GONE);
//                                System.out.println("less");
                            captiontext.setMaxLines(1);
                        }
                    });
                }
            });
        }

//        public void onClick(final String ownerID,String discoverKey, UDS_DetailsClass position) {
//            menu.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                        users.child(currentuser).child("userDiscovers").child("UDS_List").addListenerForSingleValueEvent(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                if (dataSnapshot.exists()) {
//                                    boolean value = false;
//                                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
//                                        String discoveruserkey = dataSnapshot1.getKey();
//                                        if (discoveruserkey.equals(discoverKey)) {
//                                            value = true;
//                                            System.out.println("post currentuser");
////                                        System.out.println("discoverkey:" + discoveruserkey);
//                                            CharSequence option[] = new CharSequence[]
//                                                    {
//                                                            "Edit",
//                                                            "Share",
//                                                            "Delete",
//                                                            "Cancel"
//                                                    };
//                                            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(menu.getContext());
//                                            builder.setTitle("Menu");
//                                            builder.setItems(option, new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(DialogInterface dialogInterface, int i) {
//                                                    if (i == 0) {
////                                                    System.out.println("edit");
//                                                        users.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
//                                                            @Override
//                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                                                if (dataSnapshot.exists()) {
//                                                                    String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
//                                                                    String caption = dataSnapshot.child("UDSD_caption").getValue().toString();
//
//                                                                    Intent intent = new Intent(mContext, editDiscoverActivity.class);
//                                                                    intent.putExtra("mediaType", mediaType);
//                                                                    intent.putExtra("caption", caption);
//                                                                    intent.putExtra("discoverKey", discoverKey);
////                                                                intent.putExtra("discoverUrl", newdiscoverurl);
//                                                                    mContext.startActivity(intent);
//                                                                }
//                                                            }
//
//                                                            @Override
//                                                            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                                            }
//                                                        });
//                                                    } else if (i == 1) {
////                                                    System.out.println("share");
//                                                        users.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
//                                                            @Override
//                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                                                if (dataSnapshot.exists()) {
//                                                                    String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
//                                                                    String caption = dataSnapshot.child("UDSD_caption").getValue().toString();
////                                                                    String mediaUrl = dataSnapshot.child("UDSD_mediaURL").getValue().toString();
//                                                                    if (mediaType.equals("image")) {
//                                                                        discoverimage.child(discoverKey + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//                                                                            @Override
//                                                                            public void onSuccess(Uri uri) {
////                                                                            System.out.println("share image");
//                                                                                Toast.makeText(mContext, "In development mode", Toast.LENGTH_SHORT).show();
////                                                                                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
////                                                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                                                                                intent.putExtra(Intent.EXTRA_STREAM, uri);
////                                                                                intent.setType("image/png");
////                                                                                context.startActivity(Intent.createChooser(intent, "Share image via"));
//                                                                            }
//                                                                        });
//                                                                    } else {
//                                                                        Toast.makeText(mContext, "In development mode", Toast.LENGTH_SHORT).show();
////                                                                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
////                                                                        sharingIntent.setType("text/plain");
////                                                                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, caption);
////                                                                        context.startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
//                                                                    }
//                                                                }
//                                                            }
//
//                                                            @Override
//                                                            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                                            }
//                                                        });
////
//                                                    } else if (i == 2) {
//                                                        ProgressDialog dialog = new ProgressDialog(mContext);
//                                                        dialog.setMessage("Delete on progress\nPlease wait");
//                                                        dialog.show();
////                                                    System.out.println("delete");
//                                                        users.child(currentuser).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Details").child("UDSD_status").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                                            @Override
//                                                            public void onComplete(@NonNull Task<Void> task) {
//                                                                dialog.dismiss();
//                                                                Toast.makeText(mContext, "Post deleted!...", Toast.LENGTH_SHORT).show();
//                                                                captionList.remove(position);
//                                                                notifyDataSetChanged();
//                                                            }
//                                                        });
//                                                    }
//                                                }
//                                            });
//                                            builder.show();
//                                            break;
//                                        }
//                                    }
//                                    if (!value) {
//                                    System.out.println("Bukan post dia!");
//                                        CharSequence option[] = new CharSequence[]
//                                                {
//                                                        "Share",
//                                                        "Cancel"
//                                                };
//                                        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(menu.getContext());
//                                        builder.setTitle("Menu");
//                                        builder.setItems(option, new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialogInterface, int i) {
//                                                if (i == 0) {
////                                                System.out.println("share");
//                                                    pDiscover.addListenerForSingleValueEvent(new ValueEventListener() {
//                                                        @Override
//                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                                            if (dataSnapshot.exists()) {
//                                                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
//                                                                    String publicdiscoverkey = dataSnapshot1.getKey();
//                                                                    if (publicdiscoverkey.equals(discoverKey)) {
//                                                                        if (dataSnapshot1.hasChild("PD_ownerID")) {
//                                                                            String pdownerid = dataSnapshot1.child("PD_ownerID").getValue().toString();
//                                                                        System.out.println("ownerId:" + pdownerid);
////                                                                        System.out.println("publicKey:" + publicdiscoverkey);
//                                                                            users.child(pdownerid).child("userDiscovers").child("UDS_List").child(publicdiscoverkey).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
//                                                                                @Override
//                                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                                                                    if (dataSnapshot.exists()) {
//                                                                                        String mediaType = dataSnapshot.child("UDSD_mediaType").getValue().toString();
//                                                                                        String caption = dataSnapshot.child("UDSD_caption").getValue().toString();
//                                                                                        if (mediaType.equals("image")) {
//                                                                                            discoverimage.child(publicdiscoverkey + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//                                                                                                @Override
//                                                                                                public void onSuccess(Uri uri) {
//                                                                                                    // Got the download URL for 'users/me/profile.png'
////                                                                                                System.out.println("share image");
////                                                                                                System.out.println("uri:" + uri);
////                                                                                                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
////                                                                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                                                                                                intent.putExtra(Intent.EXTRA_STREAM, uri);
////                                                                                                intent.setType("image/png");
////                                                                                                context.startActivity(Intent.createChooser(intent, "Share image via"));
//                                                                                                    Toast.makeText(mContext, "In development mode", Toast.LENGTH_SHORT).show();
//                                                                                                }
//                                                                                            }).addOnFailureListener(new OnFailureListener() {
//                                                                                                @Override
//                                                                                                public void onFailure(@NonNull Exception exception) {
//                                                                                                    // Handle any errors
//                                                                                                }
//                                                                                            });
//
//                                                                                        } else {
//                                                                                            Toast.makeText(mContext, "In development mode", Toast.LENGTH_SHORT).show();
////                                                                                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
////                                                                                        sharingIntent.setType("text/plain");
////                                                                                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, caption);
////                                                                                        context.startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
//                                                                                        }
//                                                                                    }
//                                                                                }
//
//                                                                                @Override
//                                                                                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                                                                }
//                                                                            });
//                                                                        }
//                                                                    }
//                                                                }
//                                                            }
//                                                        }
//
//                                                        @Override
//                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                                        }
//                                                    });
//                                                }
//                                            }
//                                        });
//                                        builder.show();
//                                    }
//                                }
//                            }
//
//                            @Override
//                            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                            }
//                        });
//                }
//            });
//        }

        public void clapStatus(final String discoverKey, String ownerId) {

            users.child(ownerId).child("userDiscovers").child("UDS_List").child(discoverKey).child("UDS_Claps").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()) {
//                        numberclap.setVisibility(View.VISIBLE);
                        clap_text.setVisibility(View.VISIBLE);
                        if (dataSnapshot.hasChild(currentuser)) {
//                            System.out.println("ada:clap");
                            countClaps = (int) dataSnapshot.getChildrenCount();
                            unclap.setImageResource(R.drawable.clap2);
//                            numberclap.setText(Integer.toString(countClaps));
                            clap_text.setText(Integer.toString(countClaps) + "\tpeople clap this post");

                        }else{
//                            System.out.println("tiada:clap");
                            countClaps = (int) dataSnapshot.getChildrenCount();
                            unclap.setImageResource(R.drawable.clap1);
//                            numberclap.setText(Integer.toString(countClaps));
                            clap_text.setText(Integer.toString(countClaps) + "\tpeople clap this post");
                        }
                    }else{
                        numberclap.setVisibility(View.GONE);
                        clap_text.setVisibility(View.GONE);
                        unclap.setImageResource(R.drawable.clap1);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Date currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        String dateplusone = df.format(ccc);
        datess = dateplusone;
        return datess;
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
            System.out.println("datetimes"+datetimes);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = "2 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = "3 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = "4 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = "5 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = "6 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = "7 days ago";
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }
}
