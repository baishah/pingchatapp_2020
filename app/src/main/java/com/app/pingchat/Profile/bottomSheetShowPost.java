package com.app.pingchat.Profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.CommentLayoutActivity;
import com.app.pingchat.Discover.DiscoveryComments;
import com.app.pingchat.Discover.DiscoveryCommentsAdapter;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import afu.org.checkerframework.checker.nullness.qual.Nullable;

public class bottomSheetShowPost extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;

    private FirebaseAuth mAuth;

    StorageReference storeRef, discoverimage;

    private ArrayList<DiscoveryComments> commentarraylist = new ArrayList<>();

    DatabaseReference users, pdiscovers;
    String currentuser;

    TextView status, loc, date, user, more, less, numbercomment, numberclap,status1, more1,less1,clap_text;
    ImageView image, image1, userimage, clap, comments, unclap;
    ImageButton menuBtn, menu,playbtn;
    RelativeLayout moreless, moreless1,videoRl;
    VideoView videoView;
    View line1;
    Button add;
    int countClaps;
    ProgressBar progressBar;
    boolean isTextViewClicked = false;
    Boolean clapChecker = false;
    Context context;
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.discover_post_view, container, false);

        status = v.findViewById(R.id.status3);
        image = v.findViewById(R.id.picture);
        image1 = v.findViewById(R.id.picture1);
        videoRl = v.findViewById(R.id.rl18);
        videoView = v.findViewById(R.id.video);
        playbtn = v.findViewById(R.id.playvideo);
        loc = v.findViewById(R.id.loc);
        date = v.findViewById(R.id.date);
        user = v.findViewById(R.id.user);
        more = v.findViewById(R.id.moree);
        less = v.findViewById(R.id.lesss);
        userimage = v.findViewById(R.id.user_avatar_bg);
        menu = v.findViewById(R.id.triple_btn);
        progressBar = v.findViewById(R.id.progrss);
//            clap = itemView.findViewById(R.id.clap1);
        comments = v.findViewById(R.id.comments);
        numberclap = v.findViewById(R.id.numberclap);
        numbercomment = v.findViewById(R.id.numbercomment);
        unclap = v.findViewById(R.id.clap);
        line1 = v.findViewById(R.id.line1);
//        status1 = v.findViewById(R.id.status2);
//        more1 = v.findViewById(R.id.moree1);
//        less1 = v.findViewById(R.id.lesss1);
        moreless = v.findViewById(R.id.moreless);
//        moreless1 = v.findViewById(R.id.moreless1);
        clap_text = v.findViewById(R.id.clap_count_txt);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            currentuser = mAuth.getCurrentUser().getUid();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            discoverimage = FirebaseStorage.getInstance().getReference().child("postImage");
            pdiscovers = FirebaseDatabase.getInstance().getReference().child("PublicDiscover");
            context = getContext();
        }

        String key = getArguments().getString("key");
        String ownerid = getArguments().getString("ownerId");
        String discoverId = getArguments().getString("discoverId");
        String mediaUrl = getArguments().getString("mediaUrl");
        String dates = getArguments().getString("date");
        System.out.println("data:"+ownerid+discoverId);


        getDiscover(discoverId,ownerid,dates,mediaUrl);

        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getContext(), CommentLayoutActivity.class);
                i.putExtra("discoverpostid", discoverId);
                i.putExtra("currentuser", currentuser);
                i.putExtra("ownerid", ownerid);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
        unclap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clapUnclap(ownerid,discoverId);
            }
        });

        return v;
    }

    private void clapUnclap(String ownerid, String discoverId) {

                clapChecker = true;
                users.child(ownerid).child("userDiscovers").child("UDS_List").child(discoverId).child("UDS_Claps").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (clapChecker.equals(true)) {
                            if (dataSnapshot.hasChild(currentuser)) {
//                                System.out.println("re-clap");
//                                holder.unclap.setVisibility(View.VISIBLE);
//                                holder.clap.setVisibility(View.INVISIBLE);
                                users.child(ownerid).child("userDiscovers").child("UDS_List").child(discoverId).child("UDS_Claps").child(currentuser).removeValue();
                                clapChecker = false;

                            } else {
//                                System.out.println("new clap");
//                                holder.unclap.setVisibility(View.INVISIBLE);
//                                holder.clap.setVisibility(View.VISIBLE);
                                final Map UDSclaps = new HashMap();
                                UDSclaps.put("UDSC_status", false);
                                UDSclaps.put("UDSC_timestamp", timestamp.getTime());
                                users.child(ownerid).child("userDiscovers").child("UDS_List").child(discoverId).child("UDS_Claps").child(currentuser).setValue(UDSclaps);
                                clapChecker = false;
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getDiscover(String discoverID,String ownerID,String dates,String mediaURL) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(mediaURL);
        GlideApp.with(context)
                .load(storageReference)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(circularProgressDrawable)
                .into(image);
        users.child(ownerID).child("userDiscovers").child("UDS_List").child(discoverID).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String address = dataSnapshot.child("UDSD_address").getValue().toString();
                    String caption = dataSnapshot.child("UDSD_caption").getValue().toString();

                    status.setText(caption);
                    loc.setText(address);

                    status.post(new Runnable() {
                        @Override
                        public void run() {
                            status.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (isTextViewClicked) {
                                        //This will shrink textview to 2 lines if it is expanded.
                                        status.setMaxLines(1);
                                        isTextViewClicked = false;
//                                    System.out.println("false");
                                        less.setVisibility(View.GONE);
                                    } else {
                                        //This will expand the textview if it is of 2 lines
                                        status.setMaxLines(Integer.MAX_VALUE);
                                        isTextViewClicked = true;
//                                    System.out.println("true");
                                        less.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            less.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    less.setVisibility(View.GONE);
//                                System.out.println("less");
                                    status.setMaxLines(1);
                                }
                            });
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        users.child(ownerID).child("userDiscovers").child("UDS_List").child(discoverID).child("UDS_Claps").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
//                        numberclap.setVisibility(View.VISIBLE);
                    clap_text.setVisibility(View.VISIBLE);
                    if (dataSnapshot.hasChild(currentuser)) {
//                            System.out.println("ada:clap");
                        countClaps = (int) dataSnapshot.getChildrenCount();
                        unclap.setImageResource(R.drawable.clap2);
//                            numberclap.setText(Integer.toString(countClaps));
                        clap_text.setText(Integer.toString(countClaps) + "\tpeople clap this post");

                    }else{
//                            System.out.println("tiada:clap");
                        countClaps = (int) dataSnapshot.getChildrenCount();
                        unclap.setImageResource(R.drawable.clap1);
//                            numberclap.setText(Integer.toString(countClaps));
                        clap_text.setText(Integer.toString(countClaps) + "\tpeople clap this post");
                    }
                }else{
                    numberclap.setVisibility(View.GONE);
                    clap_text.setVisibility(View.GONE);
                    unclap.setImageResource(R.drawable.clap1);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        users.child(ownerID).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("UD_displayName")) {
                    user.setText(dataSnapshot.child("UD_displayName").getValue().toString());
                }

                if (dataSnapshot.hasChild("UD_avatarURL")) {
                    String avatarurl = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    if (!avatarurl.startsWith("h")) {

                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatarurl);
                        GlideApp.with(context.getApplicationContext())
                                .load(storageReference)
                                .placeholder(R.drawable.loading_img)
                                .into(userimage);
                    } else {
                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
                        circularProgressDrawable.setStrokeWidth(5f);
                        circularProgressDrawable.setCenterRadius(10f);
                        circularProgressDrawable.start();
                        GlideApp.with(context.getApplicationContext())
                                .load(avatarurl)
                                .placeholder(R.drawable.loading_img)
                                .into(userimage);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        users.child(ownerID).child("userDiscovers").child("UDS_List").child(discoverID).child("UDS_Comments").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    int total = 0;
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        String key = dataSnapshot1.getKey();
//                        System.out.println("key: " + key);
                        total = total + 1;
                    }
                    numbercomment.setText(String.valueOf(total));
                } else {
                    numbercomment.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface BottomSheetListener {
        void onClickComment(String text);
    }
}
