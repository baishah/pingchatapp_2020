package com.app.pingchat.Profile;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Discover.DiscoverAdapter2;
import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class PhotoProfile_Fragment extends Fragment {
    View view;
    private DatabaseReference users,discovers;
    private RecyclerView recyclerviewlistpost;
    PostImageInProfileAdapter.imagerecycleradapterlist imagerecycleradapterlist;
    private ArrayList <UDS_DetailsClass> postlist =  new ArrayList<>();
//    ArrayList<UDS_DetailsClass>postlist;
    private FirebaseAuth Mauth;
    private String currenct;
    private RequestQueue mQueue;
    private ImageView nopost;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.photoprofile_fragment,container,false);

        Mauth = FirebaseAuth.getInstance();

        mQueue  = Volley.newRequestQueue(getContext());
        if(Mauth.getCurrentUser()!=null) {
            currenct = Mauth.getCurrentUser().getUid();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            discovers = users.child(currenct).child("userDiscovers").child("UDS_List");
            discovers.keepSynced(true);

            recyclerviewlistpost = view.findViewById(R.id.discoverpost3);
            nopost = view.findViewById(R.id.no_post);

            recyclerviewlistpost.setLayoutManager(new GridLayoutManager(getContext(),3));
            getpostpicture(currenct);

        }



//        imagerecycleradapterlist = new PostImageInProfileAdapter.imagerecycleradapterlist() {
//            @Override
//            public void onUserClickedImage(final int position) {
//                popupimage(postlist.get(position).getUDSD_mediaURL());
//            }
//        };

        return view;
    }


    private void getpostpicture(final String id){
//        final ArrayList postlist = new ArrayList();
        final ArrayList postkey = new ArrayList();
        postlist = new ArrayList();
//        String url = "https://devbm.ml/discover/all?keyUID="+id;
//        DiscoverAdapter2 discoverAdapter2 = new DiscoverAdapter2(getContext(),alldiscoverarraylist2,current_user);
//        PostImageInProfileAdapter adapter = new PostImageInProfileAdapter(getContext().getApplicationContext(),postlist,imagerecycleradapterlist);
//        recyclerviewlistpost.setAdapter(adapter);
        discovers.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    nopost.setVisibility(View.VISIBLE);
                    System.out.println("nopost:");
                }
                else{
                    nopost.setVisibility(View.INVISIBLE);
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        String post = dataSnapshot1.getKey().toString();
                        postkey.add(post);
                        System.out.println("post:");
                    }
                    Collections.sort(postkey, Collections.reverseOrder());
                    for(int i=0;i<postkey.size();i++){
                        final UDS_DetailsClass discoverss = new UDS_DetailsClass();
                        discovers.child(postkey.get(i).toString()).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    UDS_DetailsClass d = dataSnapshot.getValue(UDS_DetailsClass.class);
                                    String postSenderid = d.getUDSD_ownerID();
                                    String postimageurl = d.getUDSD_mediaURL();
                                    String postmediatype = d.getUDSD_mediaType();
                                    boolean status = d.isUDSD_status();

                                    if(postSenderid.equals(id) && status==true){
                                        if(postmediatype.equals("noMedia")){
                                            System.out.println("noMedia");
                                        }else{
                                            discoverss.setUDSD_mediaURL(postimageurl);
                                            discoverss.setUDSD_mediaType(postmediatype);
                                            postlist.add(discoverss);
//                                            System.out.println("postsenderid"+postSenderid);
                                            PostImageInProfileAdapter adapter = new PostImageInProfileAdapter(getContext().getApplicationContext(),postlist,imagerecycleradapterlist);
                                            recyclerviewlistpost.setAdapter(adapter);
                                        }
                                    }
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });

//        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
//        postParams.put("keyUID",id);
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, new JSONObject(postParams),
//                new com.android.volley.Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
////                        progressDialog.dismiss();
//                        Log.d("Volley", response.toString());
//                        try {
//                            JSONArray jsonArray = response.getJSONArray("dataDiscoverSelf");
//
//                            for (int i = 0; i < jsonArray.length(); i++){
//                                JSONObject value = jsonArray.getJSONObject(i);
//
////                                animationView.cancelAnimation();
////                                animationView.setVisibility(View.GONE);
//                                System.out.println("value:"+value);
////                                Collections.sort(alldiscoverarraylist2,UDS_DetailsClass.uds_detailsClassComparator);
//                                String discover_type = value.getString("UDSD_mediaType");
//                                Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
//
//                                UDS_DetailsClass detailsClass = new UDS_DetailsClass();
//                                if(discover_type.equals("image") && discover_status.equals("true")){
//                                    String address = value.getString("UDSD_address");
//                                    String caption = value.getString("UDSD_caption");
//                                    long discover_date = value.getLong("UDSD_date");
//                                    String discover_id = value.getString("UDSD_discoverID");
//                                    String discover_url = value.getString("UDSD_mediaURL");
//                                    String ownerID = value.getString("UDSD_ownerID");
////                                    Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
//                                    String dis_type = value.getString("UDSD_type");
//
////                                    detailsClass.setDatetocompare(getdate(discover_date));
////                                    detailsClass.setDate(getdatetime(discover_date));
//                                    detailsClass.setUDSD_address(address);
//                                    detailsClass.setUDSD_caption(caption);
////                                        detailsClass.setUDSD_date(discover_date);
//                                    detailsClass.setUDSD_discoverID(discover_id);
//                                    detailsClass.setUDSD_mediaType(discover_type);
//                                    detailsClass.setUDSD_mediaURL(discover_url);
//                                    detailsClass.setUDSD_ownerID(ownerID);
//                                    detailsClass.setUDSD_status(discover_status);
//                                    detailsClass.setUDSD_type(dis_type);
//                                }
////                                else{
////                                    String address = value.getString("UDSD_address");
////                                    String caption = value.getString("UDSD_caption");
////                                    long discover_date = value.getLong("UDSD_date");
////                                    String discover_id = value.getString("UDSD_discoverID");
////                                    String ownerID = value.getString("UDSD_ownerID");
//////                                    Boolean discover_status = Boolean.valueOf(value.getString("UDSD_status"));
////                                    String dis_type = value.getString("UDSD_type");
////
//////                                    detailsClass.setDatetocompare(getdate(discover_date));
//////                                    detailsClass.setDate(getdatetime(discover_date));
////                                    detailsClass.setUDSD_address(address);
////                                    detailsClass.setUDSD_caption(caption);
//////                                        detailsClass.setUDSD_date(discover_date);
////                                    detailsClass.setUDSD_discoverID(discover_id);
////                                    detailsClass.setUDSD_mediaType(discover_type);
////                                    detailsClass.setUDSD_ownerID(ownerID);
////                                    detailsClass.setUDSD_status(discover_status);
////                                    detailsClass.setUDSD_type(dis_type);
////                                }
//                                postlist.add(detailsClass);
//                            }
////                            animationView.cancelAnimation();
////                            animationView.setVisibility(View.GONE);
//                            adapter.notifyDataSetChanged();
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//    }, new Response.ErrorListener() {
//        @Override
//        public void onErrorResponse(VolleyError error) {
//            error.printStackTrace();
//        }
//    });
//        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(70000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        mQueue.add(jsonObjReq);

    }

//    private void popupimage(String imageurl){
//        ImageView image;
//
//        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        final AlertDialog alertDialog = builder.create();
//        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_image_previewer,null);
//
//        image = view.findViewById(R.id.rectangle);
//
//        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
//        GlideApp.with(getContext()).load(storageReference).into(image);
//
//        alertDialog.setView(view);
//        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        alertDialog.getWindow().setGravity(Gravity.CENTER);
//        alertDialog.show();
//
//    }



}
