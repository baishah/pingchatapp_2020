package com.app.pingchat.Profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Chat.ChatsActivity;
import com.app.pingchat.Discover.Discovers;
import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;
import com.app.pingchat.ViewBusinessProfileActivity;
import com.app.pingchat.ViewBusinessProfileOther;
import com.app.pingchat.bottomMenuProfile;
import com.app.pingchat.bottomSheetShowFriends;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ViewUserProfileActivity extends AppCompatActivity implements View.OnClickListener, bottomMenuProfile.BottomSheetListener,bottomSheetShowFriends.BottomSheetListener{

    private DatabaseReference users, discovers, partner,conversation,friend;
    TextView displayname, userstatus, totalpost, totalfriend, contct_icon, unblock_txt,friendtxt;
    CircleImageView avatarimage;
    ImageView nopost;
    LinearLayout nopostPic,nopostCap,nopostTag;
    ImageButton back,msg, unfrenbtn, unblock_btn,menu_btn,bs_icon, friendBtn,addfriendBtn;
    RecyclerView recyclerviewlistpost,recyclerviewcaptionpost;
    RelativeLayout chatbtn, menu_btn1,rl_btn;
    String chatid,id,currentuser,type,friendid,ctuser,discoverID,key;
    private ArrayList <UDS_DetailsClass> postlist =  new ArrayList<>();
    private ArrayList <UDS_DetailsClass> captionlist =  new ArrayList<>();
    FirebaseAuth mAuth;
    PostImageInProfileAdapter.imagerecycleradapterlist imagerecycleradapterlist;

    BottomSheetBehavior bottomSheetBehavior;
    bottomMenuProfile bottomMenuProfile;
    private bottomSheetShowFriends bottomsheetshowFriends;

    private Button photoButton,captionButton,tagButton;

    private RelativeLayout friendNum, postNum;

    private Context context;

    LinearLayoutManager manager;

    private static final String TAG = "ViewUserProfileActivity";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_user_activity_layout);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        key = intent.getStringExtra("key");

//        discoverID = intent.getStringExtra("discoverpostid");
//        ctuser = intent.getStringExtra("currentuser");
//        friendid = intent.getStringExtra("ownerid");

        photoButton = findViewById(R.id.photoB);
        captionButton = findViewById(R.id.captionB);
        tagButton = findViewById(R.id.tagB);

        bottomsheetshowFriends = new bottomSheetShowFriends();
        bottomsheetshowFriends.setListener(this);

        manager = new LinearLayoutManager(getApplicationContext());
        context = this;

        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser()!=null){
            currentuser = mAuth.getCurrentUser().getUid();

            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            friend = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userFriends");
            friend.keepSynced(true);
            discovers = users.child(id).child("userDiscovers").child("UDS_List");
            discovers.keepSynced(true);
            conversation = users.child(currentuser).child("userConversations").child("Personal");
            conversation.keepSynced(true);
            type = getIntent().getStringExtra("type");

            displayname = findViewById(R.id.user_name);
            userstatus = findViewById(R.id.txt_status);
            totalpost = findViewById(R.id.post_num);
            totalfriend = findViewById(R.id.friend_num);
            avatarimage = findViewById(R.id.pro_pic);
            back = findViewById(R.id.back);
            msg = findViewById(R.id.msg_btn);
            nopostPic = findViewById(R.id.no_post_profile);
            nopostCap = findViewById(R.id.no_post_caption);
            nopostTag = findViewById(R.id.no_post_tag);
            menu_btn = findViewById(R.id.menu_btn1);
            menu_btn1 = findViewById(R.id.menu_btn);
            bs_icon = findViewById(R.id.bs_icon);
            friendBtn = findViewById(R.id.frend_btn);
            addfriendBtn = findViewById(R.id.addfrendbtn);
            friendtxt = findViewById(R.id.contct_txt);
            chatbtn = findViewById(R.id.chat_btn);
            rl_btn = findViewById(R.id.qr_rl);
            friendNum = findViewById(R.id.friendNum);
            postNum = findViewById(R.id.postNum);

            friendDetails();

            addfriendBtn.setOnClickListener(this);
            bs_icon.setOnClickListener(this);
            chatbtn.setOnClickListener(this);
            menu_btn.setOnClickListener(this);

//            addfriendBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    addfriendfunc(id,currentuser);
//                }
//            });

            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            friendNum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle args = new Bundle();
                    args.putString("key", id);
                    bottomSheetShowFriends dialog = new bottomSheetShowFriends();
                    dialog.setArguments(args);
                    dialog.show(getSupportFragmentManager(),"c");
                    System.out.println("dapat_tekan");
                }
            });


//            bs_icon.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    users.child(id).child("userPartnerDetails").addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                            if (dataSnapshot.exists()) {
//                                if(dataSnapshot.hasChild("UPD_eligibility")){
//                                    String eligibility = dataSnapshot.child("UPD_eligibility").getValue().toString();
//                                    System.out.println("eligibility:"+eligibility);
//                                    if(eligibility.equals("true")){
//                                        System.out.println("has partner account");
//                                        Intent i = new Intent(getApplication(), ViewBusinessProfileOther.class);
//                                        i.putExtra("id",id);
//                                        startActivity(i);
//                                        //                                    Toast.makeText(getApplication(), "You already have Partner Account!", Toast.LENGTH_SHORT).show();
//                                        finish();
//                                    }else {
//                                        Toast.makeText(getApplicationContext(), "This user does not have Business account yet!", Toast.LENGTH_SHORT).show();
//                                        //                                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new PartnersLayout1Fragment()).commit();
//                                        //                                    System.out.println("no partner account");
//                                        //                            Intent i = new Intent(getApplication(), PingPartnersActivity.class);
//                                        //                            startActivity(i);
//                                    }
//                                }
//                            }else {
//                                Toast.makeText(getApplicationContext(), "This user does not have Business account yet!", Toast.LENGTH_SHORT).show();
//                                //                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new PartnersLayout1Fragment()).commit();
//                                //                            System.out.println("no partner account");
//                                //                            Intent i = new Intent(getApplication(), PingPartnersActivity.class);
//                                //                            startActivity(i);
//                            }
//                        }
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {}
//                    });
//                }
//            });

//        unfrenbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                removeFriend(id);
//            }
//        });

//            menu_btn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                userMenu(id);
////                FragmentManager fm = getActivity().getFragmentManager();
//                    Bundle args = new Bundle();
//                    args.putString("key", "f");
//                    args.putString("friend_id",id);
//                    bottomMenuProfile dialog = new bottomMenuProfile();
//                    dialog.setArguments(args);
//                    dialog.show(getSupportFragmentManager(),"f");
//                    Log.d(TAG,"press");
//                }
//            });


            msg.setOnClickListener(this);
            recyclerviewlistpost = findViewById(R.id.discoverpost);
            recyclerviewcaptionpost = findViewById(R.id.discovercaption);
            recyclerviewlistpost.setLayoutManager(new GridLayoutManager(ViewUserProfileActivity.this,3));
            recyclerviewcaptionpost.setLayoutManager(manager);
            getmetadata(id);
            gettotalfriend(id);
            gettotalpost(id);
            getpostpicture(id);
            System.out.println("id ViewUserProfileActivity" + id);

            imagerecycleradapterlist = new PostImageInProfileAdapter.imagerecycleradapterlist() {
                @Override
                public void onUserClickedImage(final int position) {
//                    popupimage(postlist.get(position).getUDSD_mediaURL());
                    Bundle args = new Bundle();
                    args.putString("key", currentuser);
                    args.putString("ownerId", postlist.get(position).getUDSD_ownerID());
                    args.putString("discoverId", postlist.get(position).getUDSD_discoverID());
                    args.putString("mediaUrl",postlist.get(position).getUDSD_mediaURL());
                    args.putString("date",postlist.get(position).getDate());
                    bottomSheetShowPost dialog = new bottomSheetShowPost();
                    dialog.setArguments(args);
                    dialog.show(getSupportFragmentManager(),"c");
                    System.out.println("dapat_tekan");
                }
            };

            captionButton.setOnClickListener(this);
            tagButton.setOnClickListener(this);
            photoButton.setOnClickListener(this);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bs_icon:
                getBusinessAcc(id);
                break;

            case R.id.addfrendbtn:
                addfriendfunc(id,currentuser);
                break;
            case R.id.msg_btn:
                msgfriend(id);
                break;

            case R.id.menu_btn1:
                 Bundle args = new Bundle();
                    args.putString("key", "f");
                    args.putString("friend_id",id);
                    bottomMenuProfile dialog = new bottomMenuProfile();
                    dialog.setArguments(args);
                    dialog.show(getSupportFragmentManager(),"f");
                    Log.d(TAG,"press");
                break;

            case R.id.photoB:
                nopostCap.setVisibility(View.INVISIBLE);
                nopostTag.setVisibility(View.INVISIBLE);
                recyclerviewcaptionpost.setVisibility(View.GONE);
                if (postlist.size() != 0) {
                    recyclerviewlistpost.setVisibility(View.VISIBLE);
                    nopostPic.setVisibility(View.GONE);
                } else {
                    nopostPic.setVisibility(View.VISIBLE);
                    recyclerviewlistpost.setVisibility(View.GONE);
                }
                break;
            case R.id.captionB:
                recyclerviewlistpost.setVisibility(View.GONE);
                nopostTag.setVisibility(View.INVISIBLE);
                nopostPic.setVisibility(View.GONE);
                if (captionlist.size() != 0) {
                    recyclerviewcaptionpost.setVisibility(View.VISIBLE);
                } else {
                    nopostCap.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tagB:
                recyclerviewcaptionpost.setVisibility(View.GONE);
                nopostTag.setVisibility(View.VISIBLE);
                nopostCap.setVisibility(View.INVISIBLE);
                nopostPic.setVisibility(View.GONE);
                recyclerviewlistpost.setVisibility(View.GONE);
                break;
        }
    }

    public void getBusinessAcc(String ownerId){
        users.child(ownerId).child("userPartnerDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if(dataSnapshot.hasChild("UPD_eligibility")){
                        String eligibility = dataSnapshot.child("UPD_eligibility").getValue().toString();
                        System.out.println("eligibility:"+eligibility);
                        if(eligibility.equals("true")){
                            System.out.println("has partner account");
                            Intent i = new Intent(getApplication(), ViewBusinessProfileOther.class);
                            i.putExtra("id",ownerId);
                            startActivity(i);
                            finish();
                        }else {
                            Toast.makeText(getApplicationContext(), "This user does not have Business account yet!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    Toast.makeText(getApplicationContext(), "This user does not have Business account yet!", Toast.LENGTH_SHORT).show();
                    //                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new PartnersLayout1Fragment()).commit();
                    //                            System.out.println("no partner account");
                    //                            Intent i = new Intent(getApplication(), PingPartnersActivity.class);
                    //                            startActivity(i);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void addfriendfunc(String friendid,String current){
        users.child(current).child("userFriends").child(friendid).setValue("sent");
        users.child(friendid).child("userFriends").child(current).setValue("received").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    //  addfriendfunc(id,currentuser);
                    final Map newReq = new HashMap();
                    newReq.put("UNF_dismissStatus",false);
                    newReq.put("UNF_timestamp", ServerValue.TIMESTAMP);
                    users.child(friendid).child("userNotifications").child("UN_type").child("UN_friendRequest").child(current).setValue(newReq).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                            String url = "https://devbm.ml/notification/"+current+"/send-friend-request/"+friendid;

                            StringRequest sq = new StringRequest
                                    (Request.Method.POST, url, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {}
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                        }
                                    }){
                                protected Map<String,String> getParams(){
                                    Map<String,String> parr = new HashMap<String, String>();
//                                              parr.put("friendid", friendid);
                                    return parr;
                                }
                            };
                            sq.setRetryPolicy(new DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            queue.add(sq);
                            Toast.makeText(getApplicationContext(), "Request sent!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    addfriendBtn.setVisibility(View.GONE);
                    friendtxt.setText("Sent");
                    friendBtn.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    private void friendDetails(){
        friend.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String status = dataSnapshot.getValue().toString();
                    if (status.equals("friend")) {
                        System.out.println("status:friend");
                        friendBtn.setVisibility(View.VISIBLE);
                        addfriendBtn.setVisibility(View.GONE);
                        chatbtn.setVisibility(View.VISIBLE);
                        menu_btn1.setVisibility(View.VISIBLE);
                        rl_btn.setVisibility(View.VISIBLE);

                    }else if (status.equals("sent")) {
                        addfriendBtn.setVisibility(View.VISIBLE);
                        friendBtn.setVisibility(View.GONE);
                        friendtxt.setText("Sent");
                        chatbtn.setVisibility(View.GONE);
                        menu_btn1.setVisibility(View.GONE);
                        rl_btn.setVisibility(View.GONE);
                    } else if (status.equals("received")) {
                        addfriendBtn.setVisibility(View.VISIBLE);
                        friendBtn.setVisibility(View.GONE);
                        friendtxt.setText("Accept");
                        chatbtn.setVisibility(View.GONE);
                        menu_btn1.setVisibility(View.GONE);
                        rl_btn.setVisibility(View.GONE);
                    } else if (status.equals("remove")) {
                        addfriendBtn.setVisibility(View.VISIBLE);
                        friendBtn.setVisibility(View.GONE);
                        friendtxt.setText("Add");
                        chatbtn.setVisibility(View.GONE);
                        menu_btn1.setVisibility(View.GONE);
                        rl_btn.setVisibility(View.GONE);
                    } else if (status.equals("blocked")) {
                        addfriendBtn.setVisibility(View.GONE);
                        friendBtn.setVisibility(View.VISIBLE);
                        friendBtn.setBackgroundResource(R.drawable.block_icon);
                        friendtxt.setText("Blocked");
                        chatbtn.setVisibility(View.GONE);
                        menu_btn1.setVisibility(View.GONE);
                        rl_btn.setVisibility(View.GONE);
                    } else if (status.equals("block")) {
                        addfriendBtn.setVisibility(View.GONE);
                        friendBtn.setVisibility(View.VISIBLE);
                        friendBtn.setBackgroundResource(R.drawable.block_icon);
                        friendtxt.setText("Block");
                        chatbtn.setVisibility(View.GONE);
                        menu_btn1.setVisibility(View.GONE);
                        rl_btn.setVisibility(View.GONE);
                    }
                }else{
                    System.out.println("status:notfriend");
                    friendBtn.setVisibility(View.GONE);
                    addfriendBtn.setVisibility(View.VISIBLE);
                    friendtxt.setText("Add");
                    chatbtn.setVisibility(View.GONE);
                    menu_btn1.setVisibility(View.GONE);
                    rl_btn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void removeFriend(String id) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog alertDialog = builder.create();

        View view = LayoutInflater.from(this).inflate(R.layout.unfriends_popup_layout,null);

        TextView friendname = view.findViewById(R.id.friend_name);
        TextView cancelBtn = view.findViewById(R.id.cancel);
        TextView removebtn = view.findViewById(R.id.unfriend_txt);
        CircleImageView avatarimg = view.findViewById(R.id.pro_pic);
        TextView remove = view.findViewById(R.id.un_friend);

        remove.setText("Remove");
        removebtn.setText("Remove");

        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild("UD_displayName")){
                    String name = dataSnapshot.child("UD_displayName").getValue().toString();
                    friendname.setText(name);
                    System.out.println("name :"+name);
                }

                if(dataSnapshot.hasChild("UD_avatarURL")){
                    String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    System.out.println("url :"+ avatar);
                    if(avatar.startsWith("p")){
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                        GlideApp.with(getApplicationContext()).load(storageReference).placeholder(R.drawable.loading_img).into(avatarimg);
                    }
                    else if(avatar.startsWith("h")){
                        GlideApp.with(getApplicationContext()).load(avatar).placeholder(R.drawable.loading_img).into(avatarimg);
                    }
                }

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                removebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        confirmRemove(id);
                    }
                });



            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        return;
    }

    private void confirmRemove(String id) {
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Remove friend.. Please wait");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        friend.child(id).setValue("remove").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                users.child(id).child("userFriends").child(currentuser).setValue("remove").addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        dialog.dismiss();
                        conversation.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()) {
                                    for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                        if(dataSnapshot2.hasChild("UCP_friendID")) {
                                            String chatId = dataSnapshot2.getKey();
                                            String friendId = dataSnapshot2.child("UCP_friendID").getValue().toString();
                                            if(friendId.equals(id)){
                                                conversation.child(chatId).child("UCP_status").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        users.child(id).child("userConversations").child("Personal").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                Log.d(TAG,"false");
                                                                if(dataSnapshot.exists()) {
                                                                    for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                                                        if (dataSnapshot2.hasChild("UCP_friendID")) {
                                                                            String chatId = dataSnapshot2.getKey();
                                                                            String friendId = dataSnapshot2.child("UCP_friendID").getValue().toString();
                                                                            if (friendId.equals(currentuser)) {
                                                                                users.child(id).child("userConversations").child("Personal").child(chatId).child("UCP_status").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                                        Toast.makeText(getApplicationContext(), "Remove friend successfully", Toast.LENGTH_SHORT).show();
                                                                                        startActivity(new Intent(ViewUserProfileActivity.this, MainActivity.class));
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });
            }
        });
    }

    private void getmetadata(String id) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getApplicationContext());
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.setBackgroundColor(R.color.colorPrimary);
        circularProgressDrawable.start();

        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild("UD_aboutMe")){
                    String status = dataSnapshot.child("UD_aboutMe").getValue().toString();
                    userstatus.setText(status);
                }

                if(dataSnapshot.hasChild("UD_displayName")){
                    String name = dataSnapshot.child("UD_displayName").getValue().toString();
                    displayname.setText(name);
                }

                if(dataSnapshot.hasChild("UD_avatarURL")){
                    String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    if(avatar.startsWith("p")){
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                        GlideApp.with(getApplicationContext()).load(storageReference).placeholder(R.drawable.loading_img).into(avatarimage);
                    }
                    else if(avatar.startsWith("h")){
                        GlideApp.with(getApplicationContext()).load(avatar).placeholder(R.drawable.loading_img).into(avatarimage);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void gettotalfriend(String id) {
        users.child(id).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int total = 0;
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String friend = dataSnapshot1.getValue().toString();

                    if(friend.equals("friend")){
                        total = total+1;
                    }
                }
                System.out.println("total friend "+total);
                String num = String.valueOf(total);
                totalfriend.setText(num);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void gettotalpost(final String id) {
        final ArrayList postkey = new ArrayList();
        users.child(id).child("userDiscovers").child("UDS_List").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int total = 0;
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String key = dataSnapshot1.getValue().toString();
                    String postId = dataSnapshot1.getKey();
                    System.out.println("keyId:"+postId);
                    DataSnapshot userDetails = dataSnapshot1.child("UDS_Details");
                    String status = userDetails.child("UDSD_status").getValue().toString();
                    if(status.equals("true")){
                        postkey.add(key);
                        if(dataSnapshot.exists()){
                            total = total+1;
                        }
                    }
                }
//                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
//                    String key = dataSnapshot1.getValue().toString();
//                    postkey.add(key);
//                    if(dataSnapshot.exists()){
//                        total = total+1;
//                    }
//                }
                System.out.println("total post "+total);
                String num = String.valueOf(total);
                totalpost.setText(num);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    private void getpostpicture(final String id){
        final ArrayList postkey = new ArrayList();
        users.child(id).child("userDiscovers").child("UDS_List").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    nopostPic.setVisibility(View.VISIBLE);
                }
                else{
                    nopostPic.setVisibility(View.INVISIBLE);
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        String post = dataSnapshot1.getKey().toString();
                        postkey.add(post);
                    }
                    Collections.sort(postkey, Collections.reverseOrder());
                    for(int i=0;i<postkey.size();i++){
                        final UDS_DetailsClass discoverss = new UDS_DetailsClass();
                        discovers.child(postkey.get(i).toString()).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    UDS_DetailsClass d = dataSnapshot.getValue(UDS_DetailsClass.class);
                                    String postSenderid = d.getUDSD_ownerID();
                                    String postimageurl = d.getUDSD_mediaURL();
                                    String postmediatype = d.getUDSD_mediaType();
                                    String postcaption = d.getUDSD_caption();
                                    String postAddress = d.getUDSD_address();
                                    String postOwner = d.getUDSD_ownerID();
                                    String postID = d.getUDSD_discoverID();
                                    long datePost = d.getUDSD_date();

                                    boolean status = d.isUDSD_status();

                                    discoverss.setUDSD_mediaType(postmediatype);

                                    if(postSenderid.equals(id) && status==true){
                                        if(postmediatype.equals("noMedia")){
                                            System.out.println("noMedia");
                                            discoverss.setUDSD_caption(postcaption);
                                            discoverss.setUDSD_address(postAddress);
                                            discoverss.setUDSD_ownerID(postOwner);
                                            discoverss.setUDSD_date(datePost);
                                            discoverss.setUDSD_discoverID(postID);
                                            discoverss.setUDSD_mediaType(postmediatype);
                                            captionlist.add(discoverss);
                                            CaptionRecyclerViewAdapter mAdapter = new CaptionRecyclerViewAdapter(context,captionlist,id);
                                            recyclerviewcaptionpost.setAdapter(mAdapter);
                                        }else if(postmediatype.equals("image")){
                                            discoverss.setUDSD_mediaURL(postimageurl);
                                            discoverss.setUDSD_mediaType(postmediatype);
                                            discoverss.setUDSD_date(datePost);
                                            discoverss.setUDSD_ownerID(postOwner);
                                            discoverss.setUDSD_discoverID(postID);
                                            postlist.add(discoverss);
                                            System.out.println("postsenderid"+postSenderid);
                                            PostImageInProfileAdapter adapter = new PostImageInProfileAdapter(getApplicationContext(),postlist,imagerecycleradapterlist);
                                            recyclerviewlistpost.setAdapter(adapter);
                                        } else{
                                            System.out.println("video");
                                        }
                                    }
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void msgfriend(String userid){
        users.child(userid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("UD_displayName").getValue().toString();
                String avatarurl = dataSnapshot.child("UD_avatarURL").getValue().toString();

                users.child(userid).child("userConversations").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(!dataSnapshot.exists()){
                        }
                        else{
                            System.out.println("ada chat id");
                            DataSnapshot sn = dataSnapshot.child("Personal");
                            if(sn.exists()){
                                for(DataSnapshot dataSnapshot1:sn.getChildren()){
                                    String ucp_friendid = dataSnapshot1.child("UCP_friendID").getValue().toString();
                                    System.out.println("id from firebase "+ucp_friendid);
                                    System.out.println("masuk sini userid = ucp friendid"+userid+" "+ucp_friendid);
                                    if(currentuser.equals(ucp_friendid)){

                                        String chatid = dataSnapshot1.getKey();
                                        Intent in = new Intent(ViewUserProfileActivity.this, ChatsActivity.class);
                                        in.putExtra("friendid",userid);
                                        in.putExtra("name",name);
                                        in.putExtra("avatarurl",avatarurl);
                                        in.putExtra("chatid",chatid);
                                        in.putExtra("type","personal");
                                        startActivity(in);
                                    }
                                }
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.back:
//                finish();
//                break;
//
//            case R.id.msg_btn:
//                msgfriend(id);
//        }
//    }

    private void popupimage(String imageurl){
        ImageView image;

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog alertDialog = builder.create();
        View view = LayoutInflater.from(this).inflate(R.layout.view_image_previewer,null);

        image = view.findViewById(R.id.rectangle);

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
        GlideApp.with(getApplicationContext()).load(storageReference).into(image);

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.show();

    }

    public String getChatid() {
        return chatid;
    }
    public void setChatid(String chatid) {
        this.chatid = chatid;
    }

    @Override
    public void onProfilemenu(String text) {

    }
}