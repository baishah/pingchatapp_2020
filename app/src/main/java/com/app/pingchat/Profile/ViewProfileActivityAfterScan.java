package com.app.pingchat.Profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Discover.Discovers;
import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.app.pingchat.ViewBusinessProfileActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ViewProfileActivityAfterScan extends AppCompatActivity {
    private RequestQueue mQueue;
    TextView displayname,userstatus,totalpost,totalfriend,txt_add, friend;
    CircleImageView avatarimage;
    RecyclerView recyclerviewlistpost;
    Button addfriend, business,friendadded;
    String currentuser,id;
    DatabaseReference user,friends;
    PostImageInProfileAdapter.imagerecycleradapterlist imagerecycleradapterlist;
    private ArrayList <UDS_DetailsClass> postlist =  new ArrayList<>();
    RelativeLayout linear1, linear2, rl29, rl30;
    private ImageView nopost;

    FirebaseAuth mAuth;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity_layout_scan);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
//        String friendId = id.substring(4);

//        System.out.println("id_user:"+id);
//        System.out.println("id_user_aftertrim:"+friendId);

        mAuth = FirebaseAuth.getInstance();
        currentuser = mAuth.getCurrentUser().getUid();

        displayname = findViewById(R.id.user_name);
        userstatus = findViewById(R.id.txt_status);
        totalpost = findViewById(R.id.post_num);
        totalfriend = findViewById(R.id.friend_num);
        avatarimage = findViewById(R.id.pro_pic);
        addfriend = findViewById(R.id.add_friend);
        business = findViewById(R.id.view_business);
        friendadded = findViewById(R.id.friend_added);
        recyclerviewlistpost = findViewById(R.id.discoverpost);
        linear1 = findViewById(R.id.linear1);
        linear2 = findViewById(R.id.linear2);
        rl29 = findViewById(R.id.rl29);
        rl30 = findViewById(R.id.rl30);
        txt_add = findViewById(R.id.txt_add);
        friend = findViewById(R.id.friend1);
        nopost = findViewById(R.id.no_post);

        recyclerviewlistpost.setLayoutManager(new GridLayoutManager(this,3));
        mQueue = Volley.newRequestQueue(this);

        user = FirebaseDatabase.getInstance().getReference().child("Users");
        friends = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userFriends");
        friends.keepSynced(true);

        business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewProfileActivityAfterScan.this, ViewBusinessProfileActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
            }
        });

        addfriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addfriendfunc(id,currentuser);
                addfriend.setVisibility(View.GONE);
                friendadded.setVisibility(View.VISIBLE);
            }
        });
        friendDetails(id);
        getBusiness(id);
        getdetails(id);

        imagerecycleradapterlist = new PostImageInProfileAdapter.imagerecycleradapterlist() {
            @Override
            public void onUserClickedImage(final int position) {
//                popupimage(postlist.get(position).getUDSD_mediaURL());
                Bundle args = new Bundle();
                args.putString("key", currentuser);
                args.putString("ownerId", postlist.get(position).getUDSD_ownerID());
                args.putString("discoverId", postlist.get(position).getUDSD_discoverID());
                args.putString("mediaUrl",postlist.get(position).getUDSD_mediaURL());
                args.putString("date",postlist.get(position).getDate());
                bottomSheetShowPost dialog = new bottomSheetShowPost();
                dialog.setArguments(args);
                dialog.show(getSupportFragmentManager(),"c");
                System.out.println("dapat_tekan");
            }
        };

    }

    public void addfriendfunc(String friendid,String current){

        System.out.println("current2: "+ current);
        System.out.println("friendid2: "+ friendid);
        String newfrenid = friendid.substring(4);
//        System.out.println("newfriendid: "+ friendid);
        user.child(current).child("userFriends").child(newfrenid).setValue("sent");
        user.child(newfrenid).child("userFriends").child(current).setValue("received").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    //  addfriendfunc(id,currentuser);
                    final Map newReq = new HashMap();
                    newReq.put("UNF_dismissStatus",false);
                    newReq.put("UNF_timestamp", ServerValue.TIMESTAMP);
                    System.out.println("newfriendid: "+ newfrenid);
                    user.child(newfrenid).child("userNotifications").child("UN_type").child("UN_friendRequest").child(current).setValue(newReq).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                            String url = "https://devbm.ml/notification/"+current+"/send-friend-request/"+newfrenid;

                            StringRequest sq = new StringRequest
                                    (Request.Method.POST, url, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {}
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                        }
                                    }){
                                protected Map<String,String> getParams(){
                                    Map<String,String> parr = new HashMap<String, String>();
//                                              parr.put("friendid", friendid);
                                    return parr;
                                }
                            };
                            sq.setRetryPolicy(new DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            queue.add(sq);
                            Toast.makeText(getApplicationContext(), "Request sent!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    addfriend.setVisibility(View.GONE);
                    friendadded.setText("Sent");
                    friendadded.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    public void sendnotificationtoserver(){
        String url = "https://api.pingchat.app/notification/";

        StringRequest sq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            protected Map<String,String> getParams(){
                Map<String,String> parr = new HashMap<String, String>();

                parr.put("type", "sendFriendRequest");
                parr.put("senderName", "yeash");
                parr.put("sound", "default");
                parr.put("receiverToken","yash");

                return parr;
            }
        };
       // queue.add(sq);
    }

    public void getBusiness(String id){
        ArrayList k = new ArrayList();
//        String url = "https://api.pingchat.app/partner/getPartner/";
        String url = "https://devbm.ml/users/"+currentuser+"/profile/"+id;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray jsonArray = response.getJSONArray("merchant");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            //JSONObject jsonObject = response.getJSONObject(i);

                            String creator = jsonObject.getString("businessCreator");
                            k.add(creator);

                        }
                        if(k.contains(id)){
                            business.setVisibility(View.VISIBLE);
                        }
                        //

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }
    public void getdetails(String id){
//        ArrayList postlist = new ArrayList();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait..");
        progressDialog.show();
        String url = "https://devbm.ml/users/"+currentuser+"/profile/"+id;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        Log.d("Volley", response.toString());
                        JSONObject json = null;

                        try {
                            String statusUser = response.getString("isUser");
                            String statusPartner = response.getString("isPartner");

                            if(statusUser.equals("true") && statusPartner.equals("false")){
                                System.out.println("user!"+statusUser);

                                String name = response.getString("userDisplayName");
                                displayname.setText(name);

                                String friendid = response.getString("userID");
                                System.out.println("friendidscan:"+friendid);

                                String avatarurl = response.getString("userAvatarURL");
                                if(avatarurl.startsWith("p")){
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatarurl);
                                    GlideApp.with(getApplicationContext()).load(storageReference).placeholder(R.drawable.loading_img).into(avatarimage);
                                }
                                else if(avatarurl.startsWith("h")){
                                    GlideApp.with(getApplicationContext()).load(avatarurl).placeholder(R.drawable.loading_img).into(avatarimage);
                                }

                                String userstat = response.getString("userAbout");
                                userstatus.setText(userstat);
                                String tfriend = response.getString("userFriendCount");
                                System.out.println("tfriend:"+tfriend);
                                totalfriend.setText(tfriend);
                                String tpost = response.getString("userTotalDiscoverCount");
                                System.out.println("tpost:"+tpost);
                                totalpost.setText(tpost);

                                JSONArray jsonArray = response.getJSONArray("userPostDiscover");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject value = jsonArray.getJSONObject(i);

                                    final UDS_DetailsClass discoverss = new UDS_DetailsClass();

                                    String discover_id = value.getString("UDSD_discoverID");
                                    String discover_type = value.getString("UDSD_mediaType");
                                    String discover_url = value.getString("UDSD_mediaURL");
                                    long discover_date = Long.parseLong(value.getString("UDSD_date"));
                                    String owner_id = value.getString("UDSD_ownerID");


                                    System.out.println("disc_url:"+ discover_url);

                                    if(jsonArray.isNull(0)){
                                        nopost.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        nopost.setVisibility(View.INVISIBLE);

                                        if (discover_type.equals("image")) {
                                            discoverss.setUDSD_mediaURL(discover_url);
                                            discoverss.setUDSD_mediaType(discover_type);
                                            discoverss.setUDSD_ownerID(owner_id);
                                            discoverss.setUDSD_discoverID(discover_id);
                                            discoverss.setUDSD_date(Long.parseLong(String.valueOf(discover_date)));
                                            postlist.add(discoverss);

                                        } else {
                                            System.out.println("caption only");
                                        }
                                    }
//
                                }
                                PostImageInProfileAdapter adapter = new PostImageInProfileAdapter(getApplicationContext(),postlist,imagerecycleradapterlist);
                                recyclerviewlistpost.setAdapter(adapter);

//                                friends.child(friendid).addListenerForSingleValueEvent(new ValueEventListener() {
//                                    @Override
//                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                        if(dataSnapshot.exists()){
//                                            String status = dataSnapshot.getValue().toString();
//                                            System.out.println("statusfriend:"+status);
//                                            if (status.equals("friend")) {
//                                                System.out.println("status:friend");
//                                                friendadded.setVisibility(View.VISIBLE);
//                                                addfriend.setVisibility(View.GONE);
//                                            }else if (status.equals("sent")) {
//                                                addfriend.setVisibility(View.GONE);
//                                                friendadded.setVisibility(View.VISIBLE);
//                                                friendadded.setText("Sent");
//                                            } else if (status.equals("received")) {
//                                                addfriend.setVisibility(View.VISIBLE);
//                                                addfriend.setText("Accept");
//                                            } else if (status.equals("Remove")) {
//                                                addfriend.setVisibility(View.VISIBLE);
//                                            } else if (status.equals("blocked")) {
//                                                addfriend.setVisibility(View.GONE);
//                                                friendadded.setVisibility(View.VISIBLE);
//                                                friendadded.setText("Blocked");
//                                            } else if (status.equals("block")) {
//                                                addfriend.setVisibility(View.GONE);
//                                                friendadded.setVisibility(View.VISIBLE);
//                                                friendadded.setText("Block");
//                                            }
//                                        }else{
//                                            System.out.println("status:notfriend");
//                                            addfriend.setVisibility(View.VISIBLE);
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                    }
//                                });
                            }else if(statusUser.equals("false") && statusPartner.equals("true")){
                                System.out.println("partner! "+statusPartner);

                                linear1.setVisibility(View.GONE);
                                linear2.setVisibility(View.VISIBLE);
                                rl29.setVisibility(View.GONE);
                                rl30.setVisibility(View.VISIBLE);
                                nopost.setVisibility(View.VISIBLE);
                                friend.setText("\nMembers");
                                totalfriend.setText("1");
                                totalpost.setText("0");
                                String p_name = response.getString("partnerName");
                                displayname.setText(p_name);
                                String p_cat = response.getString("partnerCategory");
                                userstatus.setText(p_cat);
                                String p_id = response.getString("partnerID");
                                String p_url = response.getString("profileURL");
                                if(p_url.startsWith("p")){
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(p_url);
                                    GlideApp.with(getApplicationContext()).load(storageReference).placeholder(R.drawable.loading_img).into(avatarimage);
                                }
                                else if(p_url.startsWith("h")){
                                    GlideApp.with(getApplicationContext()).load(p_url).placeholder(R.drawable.loading_img).into(avatarimage);
                                }

                                String p_add = response.getString("partnerAddress");
                                txt_add.setText(p_add);
                                String p_email = response.getString("partnerEmail");
                                String p_create = response.getString("partnerCreatedOn");


//                                Intent intent = new Intent(ViewProfileActivityAfterScan.this, ViewBusinessProfileActivity.class);
//                                intent.putExtra("p_id",p_id);
//                                intent.putExtra("p_name",p_name );
//                                intent.putExtra("p_cat",p_cat );
//                                intent.putExtra("p_url",p_url );
//                                intent.putExtra("p_add",p_add );
//                                intent.putExtra("p_email",p_email );
//                                intent.putExtra("p_create",p_create );
//                                startActivity(intent);
//                                finish();
                            }else{
                                finish();
                                String message = response.getString("message");
                                Toast.makeText(getApplicationContext(), "Nope! "+message,Toast.LENGTH_LONG).show();
                                System.out.println("not user or partner!");
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
//                            addfriend.setVisibility(View.VISIBLE);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void friendDetails(String friendid){
        String newfrenid = friendid.substring(4);
        friends.child(newfrenid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String status = dataSnapshot.getValue().toString();
//                    System.out.println("status:friend"+status);
                    if (status.equals("friend")) {
                        System.out.println("status:friend");
                        friendadded.setVisibility(View.VISIBLE);
                        addfriend.setVisibility(View.GONE);
                    }else if (status.equals("sent")) {
                        addfriend.setVisibility(View.GONE);
                        friendadded.setVisibility(View.VISIBLE);
                        friendadded.setText("Sent");
                    } else if (status.equals("received")) {
                        addfriend.setVisibility(View.VISIBLE);
                        addfriend.setText("Accept");
                    } else if (status.equals("Remove")) {
                        addfriend.setVisibility(View.VISIBLE);
                    } else if (status.equals("blocked")) {
                        addfriend.setVisibility(View.GONE);
                        friendadded.setVisibility(View.VISIBLE);
                        friendadded.setText("Blocked");
                    } else if (status.equals("block")) {
                        addfriend.setVisibility(View.GONE);
                        friendadded.setVisibility(View.VISIBLE);
                        friendadded.setText("Block");
                    }
                }else{
                    System.out.println("status:notfriend");
                    addfriend.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void popupimage(String imageurl){
        ImageView image;

        final AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
        final AlertDialog alertDialog = builder.create();
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.view_image_previewer,null);

        image = view.findViewById(R.id.rectangle);

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
        GlideApp.with(getApplicationContext()).load(storageReference).into(image);

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.show();

    }

    @Override
    public void onBackPressed()
    {   super.onBackPressed();
        finish();
    }
}
