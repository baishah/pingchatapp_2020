package com.app.pingchat.Profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.Friend.ContactsFragment;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Partner.PingPartnersActivity;
import com.app.pingchat.QRcodeViewActivity;
import com.app.pingchat.R;
import com.app.pingchat.ViewBusinessProfileActivity;
import com.app.pingchat.bottomMenuProfile;
import com.app.pingchat.bottomSheetShowFriends;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ViewProfileActivity extends Fragment implements View.OnClickListener, bottomMenuProfile.BottomSheetListener , bottomSheetShowFriends.BottomSheetListener, bottomSheetShowPost.BottomSheetListener{

    private DatabaseReference users,discovers,partner;
    private TextView displayname,userstatus,totalpost,totalfriend;
    private CircleImageView avatarimage;
    private RecyclerView recyclerviewphotopost,recyclerviewcaptionpost,recyclerviewtagpost;
    ArrayList <userDetailsClass> UD_DetailsClassList = new ArrayList<>();
    private ArrayList <UDS_DetailsClass> postlist =  new ArrayList<>();
    private ArrayList <UDS_DetailsClass> captionlist =  new ArrayList<>();
    private FirebaseAuth Mauth;
    private String currenct,datetimes, datess;
    Date datesss;
    private ImageButton msg,noti,contact,qrcode,menu_profile_btn;
    private View menuprofile;
    private ScrollView scrollView;
    private RequestQueue mQueue;
    PostImageInProfileAdapter.imagerecycleradapterlist imagerecycleradapterlist;

    private BottomSheetBehavior bottomSheetBehavior;
    private bottomMenuProfile bottomMenuProfile;
    private bottomSheetShowFriends bottomsheetshowFriends;
    private bottomSheetShowPost bottomSheetShowPost;

    private RelativeLayout friendNum, postNum;

    public TabLayout tabLayout;
    //    private ViewPager viewPager;
    public pagerAdapterProfile pagerAdapter;
    LinearLayoutManager manager;

    private Button photoButton,captionButton,tagButton;

    private LinearLayout discnocaption, discnotag, discnophoto;
//    private RelativeLayout discphoto;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.profile_activity_layout,container,false);

        Mauth = FirebaseAuth.getInstance();

        mQueue  = Volley.newRequestQueue(getContext());

        photoButton = rootView.findViewById(R.id.photoB);
        captionButton = rootView.findViewById(R.id.captionB);
        tagButton = rootView.findViewById(R.id.tagB);
        manager = new LinearLayoutManager(getContext());

        if(Mauth.getCurrentUser()!=null){
            currenct = Mauth.getCurrentUser().getUid();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            discovers = users.child(currenct).child("userDiscovers").child("UDS_List");
            discovers.keepSynced(true);
            partner = FirebaseDatabase.getInstance().getReference().child("Partners");
            partner.keepSynced(true);

            displayname = rootView.findViewById(R.id.user_name);
            userstatus = rootView.findViewById(R.id.txt_status);
            totalpost = rootView.findViewById(R.id.post_num);
            totalfriend = rootView.findViewById(R.id.friend_num);
            avatarimage = rootView.findViewById(R.id.pro_pic);
            scrollView = rootView.findViewById(R.id.scrollable1);
            scrollView.setFillViewport(true);
            recyclerviewphotopost = rootView.findViewById(R.id.discoverpost);
            recyclerviewcaptionpost = rootView.findViewById(R.id.discovercaption);
            recyclerviewtagpost = rootView.findViewById(R.id.discovertags);
            qrcode = rootView.findViewById(R.id.barIcon);
            discnophoto = rootView.findViewById(R.id.no_post_profile);
            discnocaption = rootView.findViewById(R.id.no_post_caption);
            discnotag = rootView.findViewById(R.id.no_post_tag);
            friendNum = rootView.findViewById(R.id.friendNum);
            postNum = rootView.findViewById(R.id.postNum);
//          msg = rootView.findViewById(R.id.msg_btn);
            contact = rootView.findViewById(R.id.contact_btn);
            menu_profile_btn = rootView.findViewById(R.id.menu_btn);
//          noti = rootView.findViewById(R.id.noti_btn);


            bottomMenuProfile = new bottomMenuProfile();
            bottomMenuProfile.setListener(this);

            bottomsheetshowFriends = new bottomSheetShowFriends();
            bottomsheetshowFriends.setListener(this);

            bottomSheetShowPost = new bottomSheetShowPost();
            bottomSheetShowPost.setListener(this);

//            noti.setOnClickListener(this);
            menu_profile_btn.setOnClickListener(this);
            contact.setOnClickListener(this);
//          msg.setOnClickListener(this);
            qrcode.setOnClickListener(this);

            recyclerviewphotopost.setLayoutManager(new GridLayoutManager(getContext(),3));
            recyclerviewcaptionpost.setLayoutManager(manager);
            getmetadata(currenct);
            gettotalfriend(currenct);
            gettotalpost(currenct);
            getpostpicture(currenct);

            imagerecycleradapterlist = new PostImageInProfileAdapter.imagerecycleradapterlist() {
                @Override
                public void onUserClickedImage(final int position) {
//                    popupimage(postlist.get(position).getUDSD_mediaURL());
                    Bundle args = new Bundle();
                    args.putString("key", currenct);
                    args.putString("ownerId", postlist.get(position).getUDSD_ownerID());
                    args.putString("discoverId", postlist.get(position).getUDSD_discoverID());
                    args.putString("mediaUrl",postlist.get(position).getUDSD_mediaURL());
                    args.putString("date",postlist.get(position).getDate());
                    bottomSheetShowPost dialog = new bottomSheetShowPost();
                    dialog.setArguments(args);
                    dialog.show(getFragmentManager(),"c");
                    System.out.println("dapat_tekan");

                }
            };

            friendNum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle args = new Bundle();
                    args.putString("key", currenct);
                    bottomSheetShowFriends dialog = new bottomSheetShowFriends();
                    dialog.setArguments(args);
                    dialog.show(getFragmentManager(),"c");
                    System.out.println("dapat_tekan");
                }
            });

//            postNum.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    manager.scrollToPositionWithOffset(1, 20);
//                }
//            });

            captionButton.setOnClickListener(this);
            tagButton.setOnClickListener(this);
            photoButton.setOnClickListener(this);
        }
        return rootView;
    }


    public void onClick(View view){
        switch(view.getId()){
            case R.id.menu_btn:
//                popupmenu();
                Bundle args = new Bundle();
                args.putString("key", "c");
                bottomMenuProfile dialog = new bottomMenuProfile();
                dialog.setArguments(args);
                dialog.show(getFragmentManager(),"c");
                System.out.println("dapat_tekan");
                break;

            case R.id.contact_btn:
                startActivity(new Intent(getContext(), ContactsFragment.class));
                break;

            case R.id.barIcon:
                generateqr(currenct);
//                Toast.makeText(getContext(), "In development mode.", Toast.LENGTH_SHORT).show();
                break;

            case R.id.photoB:
                discnocaption.setVisibility(View.INVISIBLE);
                discnotag.setVisibility(View.INVISIBLE);
                recyclerviewcaptionpost.setVisibility(View.GONE);
                if(postlist.size()!=0){
                    recyclerviewphotopost.setVisibility(View.VISIBLE);
                    discnophoto.setVisibility(View.GONE);
                }else{
                    discnophoto.setVisibility(View.VISIBLE);
                    recyclerviewphotopost.setVisibility(View.GONE);
                }
                break;
            case R.id.captionB:
                recyclerviewphotopost.setVisibility(View.GONE);
                discnotag.setVisibility(View.INVISIBLE);
                discnophoto.setVisibility(View.GONE);
                if(captionlist.size()!=0){
                    recyclerviewcaptionpost.setVisibility(View.VISIBLE);
                }
                else{
                    discnocaption.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tagB:
                recyclerviewcaptionpost.setVisibility(View.GONE);
                discnotag.setVisibility(View.VISIBLE);
                discnocaption.setVisibility(View.INVISIBLE);
                discnophoto.setVisibility(View.GONE);
                recyclerviewphotopost.setVisibility(View.GONE);
                break;
        }
    }

    private void getmetadata(String id){
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getContext());
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.setBackgroundColor(R.color.colorPrimary);
        circularProgressDrawable.start();

        users.child(id).child("userDetails").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild("UD_aboutMe")){
                    String status = dataSnapshot.child("UD_aboutMe").getValue().toString();
                    userstatus.setText(status);
                }

                if(dataSnapshot.hasChild("UD_displayName")){
                    String name = dataSnapshot.child("UD_displayName").getValue().toString();
                    displayname.setText(name);
                }

                if(dataSnapshot.hasChild("UD_avatarURL")){
                    String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    if(avatar.startsWith("p")){
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                        GlideApp.with(getContext().getApplicationContext()).load(storageReference).placeholder(R.drawable.loading_img).into(avatarimage);
                    }
                    else if(avatar.startsWith("h")){
                        GlideApp.with(getContext().getApplicationContext()).load(avatar).placeholder(R.drawable.loading_img).into(avatarimage);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    private void gettotalfriend(String id){
        users.child(id).child("userFriends").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int total = 0;
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String friend = dataSnapshot1.getValue().toString();

                    if(friend.equals("friend")){
                        total = total+1;
                    }
                }
                System.out.println("total friend "+total);
                String num = String.valueOf(total);
                totalfriend.setText(num);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void gettotalpost(final String id){
        final ArrayList postkey = new ArrayList();
        users.child(id).child("userDiscovers").child("UDS_List").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int total = 0;
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String key = dataSnapshot1.getValue().toString();
                    String postId = dataSnapshot1.getKey();
                    System.out.println("keyId:"+postId);
                    DataSnapshot userDetails = dataSnapshot1.child("UDS_Details");
                    String status = userDetails.child("UDSD_status").getValue().toString();
                    if(status.equals("true")){
                        postkey.add(key);
                        if(dataSnapshot.exists()){
                            total = total+1;
                        }
                    }
                }
                System.out.println("total post "+total);
                String num = String.valueOf(total);
                totalpost.setText(num);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    private void getpostpicture(final String id){
        final ArrayList postkey = new ArrayList();
        users.child(id).child("userDiscovers").child("UDS_List").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    discnophoto.setVisibility(View.VISIBLE);
                }
                else{
                    discnophoto.setVisibility(View.GONE);
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        String post = dataSnapshot1.getKey().toString();
                        postkey.add(post);
                    }
                    Collections.sort(postkey, Collections.reverseOrder());
                    for(int i=0;i<postkey.size();i++){
                        final UDS_DetailsClass discoverss = new UDS_DetailsClass();
                        discovers.child(postkey.get(i).toString()).child("UDS_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    UDS_DetailsClass d = dataSnapshot.getValue(UDS_DetailsClass.class);
                                    String postSenderid = d.getUDSD_ownerID();
                                    String postcaption = d.getUDSD_caption();
                                    String postimageurl = d.getUDSD_mediaURL();
                                    String postAddress = d.getUDSD_address();
                                    String postOwner = d.getUDSD_ownerID();
                                    String postID = d.getUDSD_discoverID();
                                    long datePost = d.getUDSD_date();
                                    String postmediatype = d.getUDSD_mediaType();
                                    boolean status = d.isUDSD_status();

                                    if(postSenderid.equals(id) && status==true){
                                        if(postmediatype.equals("noMedia")){
                                            System.out.println("noMedia");
//                                            recyclerviewcaptionpost.setVisibility(View.VISIBLE);
                                            discoverss.setUDSD_caption(postcaption);
                                            discoverss.setUDSD_address(postAddress);
                                            discoverss.setUDSD_ownerID(postOwner);
                                            discoverss.setUDSD_date(datePost);
                                            discoverss.setUDSD_discoverID(postID);
                                            discoverss.setUDSD_mediaType(postmediatype);
                                            captionlist.add(discoverss);
                                            CaptionRecyclerViewAdapter mAdapter = new CaptionRecyclerViewAdapter(getContext(),captionlist,currenct);
                                            recyclerviewcaptionpost.setAdapter(mAdapter);
                                        }else if(postmediatype.equals("image")){
                                            discoverss.setUDSD_mediaURL(postimageurl);
                                            discoverss.setUDSD_discoverID(postID);
                                            discoverss.setUDSD_ownerID(postOwner);
//                                            discoverss.setUDSD_date(datePost);
                                            discoverss.setDate(getdatetime(datePost));
                                            discoverss.setUDSD_mediaType(postmediatype);
                                            postlist.add(discoverss);
                                            System.out.println("postsenderid"+postSenderid);
                                            PostImageInProfileAdapter adapter = new PostImageInProfileAdapter(getContext(),postlist,imagerecycleradapterlist);
                                            recyclerviewphotopost.setAdapter(adapter);
                                        }else{
                                            System.out.println("video");
                                        }
                                    }
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

//    private void popupimage(String imageurl){
//        ImageView image;
//
//        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        final AlertDialog alertDialog = builder.create();
//        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_image_previewer,null);
//
//        image = view.findViewById(R.id.rectangle);
//
//        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageurl);
//        GlideApp.with(getContext()).load(storageReference).into(image);
//
//        alertDialog.setView(view);
//        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        alertDialog.getWindow().setGravity(Gravity.CENTER);
//        alertDialog.show();
//
//    }

    public void checkbusiness(){
        System.out.println("checking business");
        String url = "https://api.pingchat.app/partner/getPartner/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("merchant");
                            int businessexist = 0;

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                //JSONObject jsonObject = response.getJSONObject(i);
                                String businesscreator = jsonObject.getString("businessCreator");

                                if (businesscreator.equals(currenct)){
                                    businessexist = businessexist+1;
                                }
                            }

                            if(businessexist==0){
                                Intent i = new Intent(getActivity(), PingPartnersActivity.class);
                                startActivity(i);
                                System.out.println("tiada business creator");

                            }
                            else
                            {
                                Intent i = new Intent(getActivity(), ViewBusinessProfileActivity.class);
                                i.putExtra("id",currenct);
                                startActivity(i);
                                System.out.println("ada business creator");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    public void generateqr(String id){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        final Date d = new Date();
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.add(Calendar.HOUR_OF_DAY,24);
        String jj = calendar.getTime().toString();
        String hh = dateFormat.format(new Date(jj)).toString();

        try{
            Date date = dateFormat.parse(hh);
            final long unixtime =(long)date.getTime()/1000;
            RequestQueue queue = Volley.newRequestQueue(getContext());
            System.out.println("masuk sini dalam json");
            String url = "https://devbm.ml/users/"+id+"/qr-test";
            System.out.println("url:"+url);
            String timetemporary = Long.toString(unixtime)+"000";
            StringRequest sq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    Intent intent = new Intent(getContext(), QRcodeViewActivity.class);
                    intent.putExtra("id",currenct);
                    intent.putExtra("qr_code","user");
                    startActivity(intent);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {}
            })
            {
                protected Map<String,String> getParams(){
                    Map<String,String> parr = new HashMap<String, String>();

                    parr.put("isUser","true");
                    return parr;
                }
            };
            queue.add(sq);
            sq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 70000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 70000;
                }
                @Override
                public void retry(VolleyError error) throws VolleyError {}
            });
        }catch (Exception e){
        }
    }

    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Date  currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        String dateplusone = df.format(ccc);
        datess = dateplusone;
        return datess;
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
            System.out.println("datetimes"+datetimes);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = "2 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = "3 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = "4 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = "5 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = "6 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = "7 days ago";
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }

    public Date getdate(long timestamp){
        datesss = new Date(timestamp);
        return datesss;
    }

    @Override
    public void onProfilemenu(String text) {
    }

    @Override
    public void onClickComment(String text) {

    }
}