package com.app.pingchat.Profile;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.Discover.Discovers;
import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class PostImageInProfileAdapter extends RecyclerView.Adapter<PostImageInProfileAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    Context context;
    ArrayList<UDS_DetailsClass> listoffriend;
    imagerecycleradapterlist ilistlistener;
    String id;


    public PostImageInProfileAdapter(Context c, ArrayList<UDS_DetailsClass> f,imagerecycleradapterlist clicklistener){
        context = c;
        listoffriend = f;
        ilistlistener = clicklistener;
    }

    public PostImageInProfileAdapter(){
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(context).inflate(R.layout.post_image_inprofile,parent,false);
        return new MyViewHolder(v,ilistlistener);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();

        final UDS_DetailsClass discovers = listoffriend.get(position);

        if(discovers.getUDSD_mediaType().equals("noMedia")){
            holder.image.setVisibility(View.GONE);
        } else if (discovers.getUDSD_mediaType().equals("image")){
            CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
            circularProgressDrawable.setStrokeWidth(5f);
            circularProgressDrawable.setCenterRadius(30f);
            circularProgressDrawable.start();

            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(discovers.getUDSD_mediaURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(circularProgressDrawable)
                    .into(holder.image);
        }else{
            holder.image.setVisibility(View.GONE);
        }
//        else{
//            //do nothing
//
//        }

        //Picasso.with(context.getApplicationContext()).load(myfriend.getAvatarURL()).into(holder.i);

    }

    @Override
    public int getItemCount() {
        return listoffriend.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ImageView image;
        imagerecycleradapterlist ilistlistener;

        public MyViewHolder(View itemView,imagerecycleradapterlist clicklistener) {
            super(itemView);
            image = itemView.findViewById(R.id.postimage);
            ilistlistener = clicklistener;
            itemView.setOnClickListener(this);

        }
//        @Override
//        public boolean onLongClick(View v){
//            ilistlistener.onUserClickedImage(getAdapterPosition());
//            return true;
//        }

        @Override
        public void onClick(View v) {
            ilistlistener.onUserClickedImage(getAdapterPosition());
//            return true;
        }
    }
    public interface imagerecycleradapterlist{
        void onUserClickedImage(int position);
    }
}
