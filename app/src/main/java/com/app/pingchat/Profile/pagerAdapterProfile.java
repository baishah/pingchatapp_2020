package com.app.pingchat.Profile;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class pagerAdapterProfile extends FragmentPagerAdapter {

    private final List<Fragment> fragmentList = new ArrayList<>();
    private final List<String> FragmentListTitles = new ArrayList<>();


    int mNumOfTabs;

    public pagerAdapterProfile(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                PhotoProfile_Fragment tab1 = new PhotoProfile_Fragment();
                return tab1;
            case 1:
                CaptionProfile_Fragment tab2 = new CaptionProfile_Fragment();
                return tab2;
            case 2:
                TagProfile_Fragment tab3 = new TagProfile_Fragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

//    public pagerAdapterProfile(FragmentManager fm) {
//        super(fm);
//    }
//
//
//    @Override
//    public Fragment getItem(int position) {
//        return fragmentList.get(position);
//    }
//
//    @Override
//    public int getCount() {
//        return FragmentListTitles.size();
//    }
//
//    @Override
//    public CharSequence getPageTitle(int position) {
//        return FragmentListTitles.get(position);
//    }
//
//    public void AddFragment(Fragment fragment,String Title){
//        fragmentList.add(fragment);
//        FragmentListTitles.add(Title);
//    }
}
