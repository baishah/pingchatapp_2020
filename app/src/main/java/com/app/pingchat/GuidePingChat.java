package com.app.pingchat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.UserAuth.LoginActivity;

import java.util.ArrayList;

public class GuidePingChat extends AppCompatActivity implements View.OnClickListener {
    TextView skipBtn;
    private LinearLayout sliderDotspanel;
    Button nextbtn;
    private int[] layouts = {R.layout.onboarding_layout_1,R.layout.onboarding_layout_2,R.layout.onboarding_layout_3,R.layout.onboarding_layout_4, R.layout.onboarding_layout_5};
    private ImageView[] dots;
    private mPagerAdapter mpagerAdapter;
    private ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(new PreferenceManager(this).checkPreference())
        {
            loadHome();
        }

        setContentView(R.layout.guide_layout);

//        SharedPreferences preferences =  getSharedPreferences("my_preferences",
//                MODE_PRIVATE);

        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);
        createDots(0);
        mPager = (ViewPager) findViewById(R.id.slider);
        mpagerAdapter  = new mPagerAdapter(layouts,this);
        mPager.setAdapter(mpagerAdapter);

        skipBtn = (TextView) findViewById(R.id.skip_txt);
        skipBtn.setOnClickListener(this);

        nextbtn = (Button) findViewById(R.id.next_bt);
        nextbtn.setOnClickListener(this);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);
                if(position==layouts.length-1)
                {
                    nextbtn.setText("LOGIN");
                    skipBtn.setVisibility(View.INVISIBLE);
                }
                else
                {
                    nextbtn.setText("NEXT");
                    skipBtn.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void createDots(int current_position) {
        if(sliderDotspanel !=null)
            sliderDotspanel.removeAllViews();

        dots = new ImageView[layouts.length];

        for(int i = 0; i < layouts.length; i++) {

            dots[i] = new ImageView(this);
            if(i==current_position)
            {
                dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dots));
            }else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dots));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);
        }
    }

    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.skip_txt:
                loadHome();
                new PreferenceManager(this).writePreference();
                break;

            case R.id.next_bt:
                loadNextSlide();
                break;
        }

    }

    private void loadHome()
    {
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }

    private void loadNextSlide()
    {
        int next_slide = mPager.getCurrentItem()+1;

        if(next_slide<layouts.length)
        {
            mPager.setCurrentItem(next_slide);
        }
        else
        {
            loadHome();
            new PreferenceManager(this).writePreference();
        }
    }
}