package com.app.pingchat;

import android.app.Activity;
import android.app.ActivityManager;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;

import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

public class ArActivity extends AppCompatActivity {
  //  private static final String TAG = MainActivity.class.getSimpleName();
    private static final double MIN_OPENGL_VERSION = 3.0;

//Create a member variable for ModelRenderable//

    private ViewRenderable viewRenderable;

//Create a member variable for ArFragment//

    private ArFragment arCoreFragment;

    @RequiresApi(api = VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!checkDevice((this))) {
            return;
        }

        setContentView(R.layout.ar_activity);
        arCoreFragment = (ArFragment)

//Find the fragment, using fragment manager//
        getSupportFragmentManager().findFragmentById(R.id.ux_fragment);

        if (Build.VERSION.SDK_INT >= VERSION_CODES.N) {

//Build the ModelRenderable//

            ViewRenderable.builder()
                    .setView(this, R.layout.imgboard)
                    .build()
                    .thenAccept(renderable ->
                            {viewRenderable = renderable;
                                ImageView i = (ImageView) renderable.getView();
                                i.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Toast.makeText(getApplicationContext(), "coupon selected", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            });



//If an error occurs...//


        }

//Listen for onTap events//

        arCoreFragment.setOnTapArPlaneListener(
                (HitResult hitResult, Plane plane, MotionEvent motionEvent) -> {
                    if (viewRenderable == null) {
                        return;
                    }

                    Anchor anchor = hitResult.createAnchor();

//Build a node of type AnchorNode//

                    AnchorNode anchorNode = new AnchorNode(anchor);

//Connect the AnchorNode to the Scene//

                    anchorNode.setParent(arCoreFragment.getArSceneView().getScene());

//Build a node of type TransformableNode//

                    TransformableNode transformableNode = new TransformableNode(arCoreFragment.getTransformationSystem());

//Connect the TransformableNode to the AnchorNode//

                    transformableNode.setParent(anchorNode);

//Attach the Renderable//

                    transformableNode.setRenderable(viewRenderable);

//Set the node//

                    transformableNode.select();
                });
    }

    public static boolean checkDevice(final Activity activity) {

//If the device is running Android Marshmallow or earlier...//

        if (Build.VERSION.SDK_INT < VERSION_CODES.N) {

//...then print the following message to Logcat//

            Log.e("TAG", "Sceneform requires Android N or higher");
            activity.finish();
            return false;
        }
        String openGlVersionString =
                ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()

//Check the version of OpenGL ES//

                        .getGlEsVersion();

//If the device is running anything less than OpenGL ES 3.0...//

        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {

//...then print the following message to Logcat//

            Log.e("TAG", "Requires OpenGL ES 3.0 or higher");
            activity.finish();
            return false;
        }
        return true;
    }
}

