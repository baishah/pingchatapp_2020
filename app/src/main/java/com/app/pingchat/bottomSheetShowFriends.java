package com.app.pingchat;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.FriendsAdapter;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.Profile.FriendListAdapter;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class bottomSheetShowFriends extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;
    private RecyclerView friendList;
    ArrayList<userDetailsClass> mContactslist;
    FriendListAdapter friendsAdapter;
    FirebaseStorage storage;
    StorageReference storeRef;
    FirebaseAuth mAuth;
    private DatabaseReference friends,users,userFriends;
    private String current_id;
    ImageView nofriend;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.show_friend_list, container, false);

        friendList = v.findViewById(R.id.friendL);
        friendList.setLayoutManager(new LinearLayoutManager(getContext()));
        nofriend = v.findViewById(R.id.nocontacts);

        mContactslist = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser()!=null) {
            current_id = mAuth.getCurrentUser().getUid();
//            friends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_id).child("userFriends");
//            friends.keepSynced(true);
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            storage = FirebaseStorage.getInstance();
            storeRef = storage.getReference();

            String key = getArguments().getString("key");
            displayfriend(key);

        }

        return v;
    }

    public void displayfriend(String userid){
        ProgressDialog dialog= new ProgressDialog(getContext());
        dialog.setMessage("Loading contacts..");
        dialog.show();
        mContactslist = new ArrayList<>();
        ArrayList checkstatus = new ArrayList();
        users.child(userid).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        final userDetailsClass userDetailsClass = new userDetailsClass();
                        String status = dataSnapshot1.getValue().toString();
                        String id = dataSnapshot1.getKey();
                        checkstatus.add(status);
                        if(status.equals("friend")){
                            users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    userDetailsClass udc = dataSnapshot.getValue(userDetailsClass.class);
                                    userDetailsClass.setUD_userID(udc.getUD_userID());
                                    userDetailsClass.setUD_avatarURL(udc.getUD_avatarURL());
                                    userDetailsClass.setUD_aboutMe(udc.getUD_aboutMe());
                                    userDetailsClass.setUD_phoneNo(udc.getUD_phoneNo());
                                    userDetailsClass.setUD_displayName(udc.getUD_displayName());
                                    userDetailsClass.setUD_birthday(udc.getUD_birthday());
                                    userDetailsClass.setUD_email(udc.getUD_email());
                                    mContactslist.add(userDetailsClass);
                                    friendsAdapter = new FriendListAdapter(getContext(),mContactslist);
                                    friendList.setAdapter(friendsAdapter);
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                        }else if(status.equals("block")){
                            users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    userDetailsClass udc = dataSnapshot.getValue(userDetailsClass.class);
                                    userDetailsClass.setUD_userID(udc.getUD_userID());
                                    userDetailsClass.setUD_avatarURL(udc.getUD_avatarURL());
                                    userDetailsClass.setUD_aboutMe(udc.getUD_aboutMe());
                                    userDetailsClass.setUD_phoneNo(udc.getUD_phoneNo());
                                    userDetailsClass.setUD_displayName(udc.getUD_displayName());
                                    userDetailsClass.setUD_birthday(udc.getUD_birthday());
                                    userDetailsClass.setUD_email(udc.getUD_email());
                                    mContactslist.add(userDetailsClass);
                                    friendsAdapter = new FriendListAdapter(getContext(),mContactslist);
                                    friendList.setAdapter(friendsAdapter);
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                        }else if(status.equals("blocked")){

                            users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    userDetailsClass udc = dataSnapshot.getValue(userDetailsClass.class);
                                    userDetailsClass.setUD_userID(udc.getUD_userID());
                                    userDetailsClass.setUD_avatarURL(udc.getUD_avatarURL());
                                    userDetailsClass.setUD_aboutMe(udc.getUD_aboutMe());
                                    userDetailsClass.setUD_phoneNo(udc.getUD_phoneNo());
                                    userDetailsClass.setUD_displayName(udc.getUD_displayName());
                                    userDetailsClass.setUD_birthday(udc.getUD_birthday());
                                    userDetailsClass.setUD_email(udc.getUD_email());
                                    mContactslist.add(userDetailsClass);
                                    friendsAdapter = new FriendListAdapter(getContext(),mContactslist);
                                    friendList.setAdapter(friendsAdapter);
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                }else{
                    dialog.dismiss();
                    nofriend.setVisibility(View.VISIBLE);
                }
                if(!checkstatus.contains("friend")){
                    dialog.dismiss();
                    nofriend.setVisibility(View.VISIBLE);
                }
                else{
                    dialog.dismiss();
                    nofriend.setVisibility(View.GONE);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public interface BottomSheetListener {
    }
}
