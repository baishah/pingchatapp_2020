package com.app.pingchat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.app.pingchat.Discover.DiscoverSetup;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;

import static java.security.AccessController.getContext;

public class EditProfileActivity extends AppCompatActivity {


    private static final String TAG = "EditProfile";

    ImageView backbtn;
    private CircleImageView disppic;
    EditText editdisplayname, editabout_me, editphone;
    Button done;
    private DatabaseReference userinfo;
    FirebaseAuth mAuth;
    String current_user;
    StorageReference storageRef;
    FirebaseStorage storage;
    private byte[] avatar;
    public StorageReference userimage;
    public static Context contextOfApplication;
    String dpicture;
    ProgressDialog dialog;
    TextView changepic, editusername, editemail;


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_layout);

        backbtn = findViewById(R.id.backArrow);

        dialog = new ProgressDialog(this);

        contextOfApplication = getApplicationContext();
        editusername = findViewById(R.id.username);
        editdisplayname = findViewById(R.id.display_name);
        editabout_me = findViewById(R.id.about_me);
        disppic = findViewById(R.id.profile_photo);
        changepic = findViewById(R.id.changeProfilePhoto);
        editemail = findViewById(R.id.email);
        editphone = findViewById(R.id.phoneNumber);
        done = findViewById(R.id.saveChanges);

        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();

        userimage = FirebaseStorage.getInstance().getReference().child("profile");

        current_user = mAuth.getCurrentUser().getUid();
        userinfo = FirebaseDatabase.getInstance().getReference().child("Users");
        userinfo.keepSynced(true);
        getUserInfo();
    }


    @Override
    public void onStart() {
        super.onStart();

        changepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
                }
                else{
                    CropImage.startPickImageActivity(EditProfileActivity.this);
                }
            }
        });

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editdone();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(EditProfileActivity.this);
        }
    }


    private void getUserInfo(){
        userinfo.child(current_user).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if((dataSnapshot.exists())){
                    String retrievename = dataSnapshot.child("UD_displayName").getValue().toString();
                    dpicture = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    String aboutuser = dataSnapshot.child("UD_aboutMe").getValue().toString();
                    String phone = dataSnapshot.child("UD_phoneNo").getValue().toString();
                    String uname = dataSnapshot.child("UD_username").getValue().toString();
                    String email = dataSnapshot.child("UD_email").getValue().toString();

                    if(dpicture.startsWith("h")){
                        GlideApp.with(getApplicationContext())
                                .load(dpicture)
                                .into(disppic);
                    }
                    else{
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(dpicture);
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .into(disppic);
                    }
                    editdisplayname.setText(retrievename);
                    editdisplayname.setSelection(editdisplayname.getText().length());
                    editabout_me.setText(aboutuser);
                    editabout_me.setSelection(editabout_me.getText().length());
                    editphone.setText(phone);
                    editphone.setSelection(editphone.getText().length());
                    editusername.setText(uname);
                    editemail.setText(email);

                }
                else {}
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void editdone(){
        dialog.setMessage("Updating profile on progress\nPlease wait");
        dialog.show();
        final String status = editabout_me.getText().toString().trim();
        final String dname = editdisplayname.getText().toString().trim();
        final String phone = editphone.getText().toString().trim();
        if (status != null && dname != null && phone != null){
            userinfo.child(current_user).child("userDetails").child("UD_aboutMe").setValue(status).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        userinfo.child(current_user).child("userDetails").child("UD_displayName").setValue(dname).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    userinfo.child(current_user).child("userDetails").child("UD_phoneNo").setValue(phone).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                uploadNewImage();
                                                finish();
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }else{
            userinfo.child(current_user).child("userDetails").child("UD_aboutMe").setValue(status).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        userinfo.child(current_user).child("userDetails").child("UD_displayName").setValue(dname).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    dialog.dismiss();
                                    finish();
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private void uploadNewImage() {
//        dialog.setMessage("Profile photo changed successfully");
//        dialog.show();
        if (getAvatar() != null) {
            String key = userinfo.push().getKey();
            final StorageReference filepath = userimage.child(current_user + key + ".jpg");
            String namadidatabase = current_user + key + ".jpg";
            UploadTask uploadTask = filepath.putBytes(getAvatar());
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    userinfo.child(current_user).child("userDetails").child("UD_avatarURL").setValue("profile/" + namadidatabase).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(),"Update profile changed successfully...", Toast.LENGTH_SHORT).show();
//                            dialog.dismiss();
                        }
                    });
                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = contextOfApplication.getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    disppic.setImageBitmap(selectedImage);
                    setAvatar(datas);
                    System.out.println("avatar changed"+getAvatar());

                    Intent i = new Intent(this, filterActivitity.class);
                    i.putExtra("Image",datas);
                    startActivityForResult(i,100);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == 100){
            if(resultCode == Activity.RESULT_OK){
                byte[] bytes = data.getByteArrayExtra("PUBLIC_STRING_IDENTIFIER");
                Bitmap b = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                setAvatar(bytes);
                disppic.setImageBitmap(b);
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(this,data);

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(this);
        }
    }


    public void back(){
        finish();
    }

    public byte[] getAvatar() { return avatar; }
    public void setAvatar(byte[] avatar) { this.avatar = avatar; }

}