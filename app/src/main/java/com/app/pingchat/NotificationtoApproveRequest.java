package com.app.pingchat;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Friend.FriendRequestFragment;

public class NotificationtoApproveRequest extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_to_approve_layout);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new FriendRequestFragment()).commit();
    }
}
