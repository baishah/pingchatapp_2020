package com.app.pingchat;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.app.pingchat.rewards.BrowseFragment;
import com.app.pingchat.rewards.RewardsFragment;

public class PagerAdapterRewards extends FragmentPagerAdapter {
    int mNumOfTabs;

    public PagerAdapterRewards(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                RewardsFragment tab1 = new RewardsFragment();
                return tab1;
            case 1:
                BrowseFragment tab2 = new BrowseFragment();
                return tab2;
//            case 2:
//                Links_Fragment tab3 = new Links_Fragment();
//                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
