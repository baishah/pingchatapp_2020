package com.app.pingchat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Profile.ViewProfileActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ViewBusinessProfileActivity extends AppCompatActivity implements View.OnClickListener{

    ImageButton back, profile, email,qr_icon;
    TextView bizname,biztype,bizaddress, m_count;
    CircleImageView logo;
    private RequestQueue mQueue;
    private FirebaseAuth Mauth;
    private String currenct, id,ppid;
    private DatabaseReference users,partner;
    private ImageView nopost;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.business_profile);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        ppid = intent.getStringExtra("partnerid");

        mQueue  = Volley.newRequestQueue(ViewBusinessProfileActivity.this);

        Mauth = FirebaseAuth.getInstance();

        if(Mauth.getCurrentUser()!=null) {
            currenct = Mauth.getCurrentUser().getUid();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            partner = FirebaseDatabase.getInstance().getReference().child("Partners");
            partner.keepSynced(true);

            back = findViewById(R.id.back);
            bizname = findViewById(R.id.biz_name);
            biztype = findViewById(R.id.business_type);
            bizaddress = findViewById(R.id.txt_add);
            logo = findViewById(R.id.pro_pic);
//            profile = findViewById(R.id.profilebtn);
            email = findViewById(R.id.email_icon);
            qr_icon = findViewById(R.id.qr_icon);
            nopost = findViewById(R.id.no_post);
            m_count = findViewById(R.id.friend_num);

//            profile.setOnClickListener(this);
            email.setOnClickListener(this);
            qr_icon.setOnClickListener(this);



            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            getmetadata(currenct);
            gettotalfriend();



//            Intent intent = getIntent();
//            String id = intent.getStringExtra("id");


            //final String id = "f9NmbjpDWyOcfRZXxfn8BhSzvec2";

//        getbusinessprofile(id);

        }
    }

    private void getmetadata(String id){
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getApplication());
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.setBackgroundColor(R.color.colorPrimary);
        circularProgressDrawable.start();

        users.child(id).child("userPartnerDetails").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UPD_partnerID")){
                    String partner_id = dataSnapshot.child("UPD_partnerID").getValue().toString();
                    System.out.println("partner_id="+partner_id);
                    partner.child(partner_id).child("P_partnerDetails").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            nopost.setVisibility(View.VISIBLE);
//                            m_count.setText("1");
                            if(dataSnapshot.hasChild("P_partnerName")){
                                String name = dataSnapshot.child("P_partnerName").getValue().toString();
                                bizname.setText(name);
                            }
                            if(dataSnapshot.hasChild("P_partnerCategory")){
                                String bizcat = dataSnapshot.child("P_partnerCategory").getValue().toString();
                                biztype.setText(bizcat);
                            }
                            if(dataSnapshot.hasChild("P_partnerCaption")){

                                String biz_add = dataSnapshot.child("P_partnerCaption").getValue().toString();
                                bizaddress.setText(biz_add);
                            }
                            if(dataSnapshot.hasChild("P_partnerEmail")){
                                String bizemail= dataSnapshot.child("P_partnerEmail").getValue().toString();

                            }
                            if(dataSnapshot.hasChild("P_profileURL")){
                                String avatar = dataSnapshot.child("P_profileURL").getValue().toString();
                                if(avatar.startsWith("p")){
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                    GlideApp.with(getApplication().getApplicationContext()).load(storageReference).placeholder(R.drawable.loading_img).into(logo);
                                }
                                else if(avatar.startsWith("h")){
                                    GlideApp.with(getApplication().getApplicationContext()).load(avatar).placeholder(R.drawable.loading_img).into(logo);
                                }
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    @Override
    public void onClick(View v){

        switch (v.getId()) {
            case R.id.email_icon:

                partner.child(ppid).child("P_partnerDetails").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        nopost.setVisibility(View.VISIBLE);
//                            m_count.setText("1");
                        if(dataSnapshot.hasChild("P_partnerEmail")){
                            String bizemail= dataSnapshot.child("P_partnerEmail").getValue().toString();
                            Intent email = new Intent(Intent.ACTION_SEND);
                            email.putExtra(Intent.EXTRA_EMAIL, new String[]{bizemail});

                            //need this to prompts email client only
                            email.setType("message/rfc822");
                            startActivity(Intent.createChooser(email, "Choose an Email client :"));

                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

//                Intent email = new Intent(Intent.ACTION_SEND);
//                email.putExtra(Intent.EXTRA_EMAIL, new String[]{emailp});
//
//                //need this to prompts email client only
//                email.setType("message/rfc822");
//                startActivity(Intent.createChooser(email, "Choose an Email client :"));
//                Toast.makeText(getApplication(), "In development mode.", Toast.LENGTH_SHORT).show();
//                popupmenu();
//                Intent i = new Intent(Intent.ACTION_SEND);
//                i.setType("message/rfc822");
//                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"testing@gmail.com"});
//                i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
//                i.putExtra(Intent.EXTRA_TEXT   , "body of email");
//                try {
//                    startActivity(Intent.createChooser(i, "Send mail to..."));
//                } catch (android.content.ActivityNotFoundException ex) {
//                    Toast.makeText(getApplication(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
//                }
                break;

//            case R.id.profilebtn:
//                Toast.makeText(getApplication(), "In development mode.", Toast.LENGTH_SHORT).show();
////                startActivity(new Intent(getApplication(), ViewProfileActivity.class));
//                break;

            case R.id.qr_icon:
//                generateqr(currenct);

                users.child(currenct).child("userPartnerDetails").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild("UPD_partnerID")) {
                            String partner_id = dataSnapshot.child("UPD_partnerID").getValue().toString();
                            System.out.println("partner_id=" + partner_id);
                            generateqr(partner_id);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                break;
        }
    }

    private void gettotalfriend() {
        partner.child(ppid).child("P_partnerMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int total = 0;
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
//                    String friend = dataSnapshot1.getValue().toString();
//                    if(friend.equals("friend")){
                    total = total+1;
//                    }
                }
                System.out.println("total friend "+total);
                String num = String.valueOf(total);
                m_count.setText(num);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void generateqr(String id){

//        final ProgressDialog progressDialog = new ProgressDialog(getApplicationContext());
//        progressDialog.setMessage("Please wait...");
//        progressDialog.show();


        final Date d = new Date();
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.add(Calendar.HOUR_OF_DAY,24);
        String jj = calendar.getTime().toString();
        String hh = dateFormat.format(new Date(jj)).toString();

        try{
            Date date = dateFormat.parse(hh);
            final long unixtime =(long)date.getTime()/1000;
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            System.out.println("masuk sini dalam json");
//            String url = "https://api.pingchat.app/QR-Code/";
            String url = "https://devbm.ml/users/"+id+"/qr-partner";
            System.out.println("url:"+url);

            String timetemporary = Long.toString(unixtime)+"000";

            StringRequest sq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
//                    progressDialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(), QRcodeViewActivity.class);
                    intent.putExtra("partner_id",id);
                    intent.putExtra("qr_code","partner");
                    startActivity(intent);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {}
            })
            {
                protected Map<String,String> getParams(){
                    Map<String,String> parr = new HashMap<String, String>();

                    parr.put("isPartner","true");
                    return parr;
                }
            };
            queue.add(sq);
            sq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 70000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 70000;
                }
                @Override
                public void retry(VolleyError error) throws VolleyError {}
            });
        }catch (Exception e){
        }
    }


//    public void getbusinessprofile(final String id){
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Loading Your Business");
//        progressDialog.show();
//        String url = "https://api.pingchat.app/partner/getPartner/";
//
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
//            new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    progressDialog.dismiss();
//                    try {
//                        JSONArray jsonArray = response.getJSONArray("merchant");
//
//
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject jsonObject = jsonArray.getJSONObject(i);
//                            //JSONObject jsonObject = response.getJSONObject(i);
//
//                            String creator = jsonObject.getString("businessCreator");
//
//                            System.out.println("creator"+creator);
//                            if(creator.equals(id)){
//                                bizname.setText(jsonObject.getString("businessName"));
//                                biztype.setText(jsonObject.getString("businessCategory"));
//                                GlideApp.with(getApplicationContext())
//                                        .load(jsonObject.getString("businessProfileURL"))
//                                        .into(logo);
//                            }
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        });
//        request.setRetryPolicy(new DefaultRetryPolicy(70000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        mQueue.add(request);
//    }
}
