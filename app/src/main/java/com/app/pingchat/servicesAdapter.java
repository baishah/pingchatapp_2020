package com.app.pingchat;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class servicesAdapter extends RecyclerView.Adapter<servicesAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Services> sList;
    private String currentuser;
    private FirebaseAuth mAuths;
    StorageReference servisImage,storeRef;
    FirebaseStorage storage;

    public servicesAdapter( ArrayList<Services> serviceList) {
        this.sList = serviceList;
    }

    public servicesAdapter(Context c, ArrayList<Services> s, String y) {
        context = c;
        sList = s;
        currentuser = y;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.servis_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Services servis = sList.get(position);
        mAuths = FirebaseAuth.getInstance();
        currentuser = mAuths.getCurrentUser().getUid();
        servisImage = FirebaseStorage.getInstance().getReference().child("Services");
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        String web_url = servis.getSD_serviceURL();

        String imageurl = servis.getSM_mediaURL();
        String title = servis.getSD_serviceTitle();

        holder.servis_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtra("key","services");
                intent.putExtra("url",web_url);
                intent.putExtra("title",title);
                intent.putExtra("icon_url",imageurl);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

//        StorageReference storageReference1 = FirebaseStorage.getInstance().getReference().child(newdiscover.getUDSD_mediaURL());
//        GlideApp.with(context.getApplicationContext())
//                .load(imageurl)
//                .into(holder.servisbtn);
        storeRef.child(imageurl).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                GlideApp.with(context.getApplicationContext())
                        .load(uri)
                        .into(holder.servisbtn);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }

    @Override
    public int getItemCount() {
        return sList.size();
    }

    public static class MyViewHolder extends  RecyclerView.ViewHolder {

//        private final Context context;
        ImageView servisbtn;
        CardView servis_card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            servisbtn = itemView.findViewById(R.id.icon_btn);
            servis_card = itemView.findViewById(R.id.card);

//            cardVId.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    final Intent intent;
//                    switch (getAdapterPosition()){
//                        case 0:
//                            intent =  new Intent(context, updateAcivity.class);
//                            break;
//
//                        case 1:
//                            intent = new Intent(context, PingPartnersActivity.class);
//                            break;
//
//                        case 2:
//                            intent =  new Intent(context, ArActivity.class);
//                            break;
//
//                        case 3:
//                            intent =  new Intent(context, EventAcitivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            break;
//
//                        case 4:
//                            intent =  new Intent(context, MarketplaceActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            break;
//
//                        case 5:
//                            intent =  new Intent(context, RewardsHomeActivity.class);
//                            break;
//
//                        case 6:
//                            intent =  new Intent(context, ServicesActivity.class);
//                            break;
//
//                        case 7:
//                            intent =  new Intent(context, DonateSelection.class);
//                            break;
//
//                        case 8:
//                            intent =  new Intent(context, Settings2Fragment.class);
//                            break;
//
//                        default:
//                            intent =  new Intent(context, UnderDevelopmentActivity.class);
//                            break;
//                    }
//                    context.startActivity(intent);
//
//                }
//            });
        }

    }
}
