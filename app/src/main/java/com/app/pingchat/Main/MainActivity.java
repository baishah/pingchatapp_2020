package com.app.pingchat.Main;


import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.app.pingchat.BottomNavigationViewHelper;
import com.app.pingchat.Chat.ChatsFragment;
import com.app.pingchat.CustomViewPager;
import com.app.pingchat.Discover.DiscoverFragment;
import com.app.pingchat.Discover.DiscoverFragmentNew;
import com.app.pingchat.Map.MapsFragment;
import com.app.pingchat.MoreFragment;
import com.app.pingchat.Profile.ViewProfileActivity;
import com.app.pingchat.R;
import com.app.pingchat.Settings2Fragment;
import com.app.pingchat.UserAuth.LoginActivity;
import com.app.pingchat.UserAuth.Register2Activity;
import com.app.pingchat.ViewPagerAdapter;
import com.app.pingchat.bottomSheetCameraGallery;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity implements ComponentCallbacks2{

    public static final String MyPREFERENCESC = "MyPrefs" ;
    SharedPreferences sharedpreferences;
    ViewProfileActivity friendsFragment;
    ChatsFragment chatsFragment;
    MapsFragment mapsFragment;
    DiscoverFragmentNew discoverFragment;
    Settings2Fragment settingsFragment;
    MoreFragment moreFragment;
    MenuItem prevMenuItem;
    private CustomViewPager viewPager;
    BottomNavigationView bottomNavigationView;
    private FirebaseAuth mAuth;
    public static String userID;
    String fromDiscover,frombusinesssetup,fromgroupsetup,toprofile,fromnotiwakeup,name,friendid,image,type,chatidintent,fromnotichat;
    DatabaseReference userinfo;
    String currentusers;
    DatabaseReference users;
    long time;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        users = FirebaseDatabase.getInstance().getReference().child("Users");
        System.out.println("masuk sini main activity");

        chatidintent = getIntent().getStringExtra("chatid");
        type = getIntent().getStringExtra("type");
        friendid =getIntent().getStringExtra("friendid");
        name = getIntent().getStringExtra("name");
        image = getIntent().getStringExtra("avatarurl");

        Log.d(TAG, "onCreate: Type: " + type );

        if(mAuth.getCurrentUser()!=null){
            currentusers = mAuth.getCurrentUser().getUid();
            userinfo = FirebaseDatabase.getInstance().getReference().child("Users");

            userinfo.child(currentusers).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        System.out.println("users exist in database"+currentusers);
                        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
                        viewPager.setPagingEnabled(false);
                        viewPager.setOffscreenPageLimit(5);

                        Intent intent = getIntent();
                        fromDiscover = intent.getStringExtra("completediscover");
                        frombusinesssetup = intent.getStringExtra("completedbusiness");
                        fromgroupsetup = intent.getStringExtra("completedgroup");
                        fromnotiwakeup = intent.getStringExtra("fromnoti");
                        fromnotichat = intent.getStringExtra("chatfromnoti");
                        toprofile = intent.getStringExtra("id");
//                        name = getIntent().getStringExtra("name");
//                        image = getIntent().getStringExtra("avatarurl");
//                        type = getIntent().getStringExtra("type");
//                        friendid =getIntent().getStringExtra("friendid");
//                        chatidintent = getIntent().getStringExtra("chatid");


                        sharedpreferences = getApplicationContext().getSharedPreferences("MyPREFERENCESC", Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("key", "c");
                        editor.putString("chatid", chatidintent);
                        editor.putString("type", type);
                        editor.putString("friendid", friendid);
                        editor.putString("name", name);
                        editor.putString("image",image);

                        editor.commit();

                        System.out.println("typemain "+type);

                        System.out.println("from discover "+fromDiscover);
                        Log.d("Tag","main activity"+userID);
                        //getContactPermission();

                        bottomNavigationView= (BottomNavigationView)findViewById(R.id.bottomview);
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new MapsFragment()).commit();
                        bottomNavigationView.setItemIconTintList(null);
//            BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
                        bottomNavigationView.setOnNavigationItemSelectedListener(
                                new BottomNavigationView.OnNavigationItemSelectedListener() {
                                    @Override
                                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                                        switch (item.getItemId()) {
                                            case R.id.discover:
                                             new DiscoverFragment();
                                             //   getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,discoverFragment).commit();
                                                //viewPager.setCurrentItem(0);
                                                break;
                                            case R.id.chats:
                                              new ChatsFragment();
                                               // getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,chatsFragment).commit();
//                                                viewPager.setCurrentItem(1);
                                                break;
                                            case R.id.maps:
                                              //  viewPager.setCurrentItem(2);
                                                break;
                                            case R.id.friends:
                                             //   viewPager.setCurrentItem(3);
                                                break;
                                            case R.id.more:
                                              //  viewPager.setCurrentItem(4);
                                                break;
                                        }
                                        return false;
                                    }
                                });

                        bottomNavigationView.setOnNavigationItemReselectedListener(
                                new BottomNavigationView.OnNavigationItemReselectedListener() {
                                    @Override
                                    public void onNavigationItemReselected(@NonNull MenuItem item) {
                                        try {
                                            if((viewPager.getCurrentItem() == 0)||(viewPager.getCurrentItem() == 2) ) {
                                                RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.discoverlist);
                                                if(mRecyclerView != null) {
                                                    mRecyclerView.smoothScrollToPosition(0);
                                                }
                                            }
                                        } catch(NullPointerException npe) {}
                                    }
                                }
                        );


                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
                            @Override
                            public void onPageSelected(int position) {
                                if (prevMenuItem != null) { prevMenuItem.setChecked(false); }
                                else
                                {
                                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                                }
                                Log.d("page", "onPageSelected: "+position);
                                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                                prevMenuItem = bottomNavigationView.getMenu().getItem(position);
                            }
                            @Override
                            public void onPageScrollStateChanged(int state) {
                            }
                        });
                        viewPager.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event)
                            {
                                return true;
                            }
                        });

                        setupViewPager(viewPager);
                        if(fromDiscover!=null){
                            viewPager.setCurrentItem(0);
                        }
                        else if(frombusinesssetup!=null){
                            viewPager.setCurrentItem(3);
                        }
                        else if(fromgroupsetup!=null){
                            viewPager.setCurrentItem(1);
                            }
                        else if(toprofile!=null){
                            viewPager.setCurrentItem(3);
                        }else if(fromnotiwakeup!=null){
                            viewPager.setCurrentItem(2);
                        }
                        else{
                            viewPager.setCurrentItem(2);
                        }
                    }
                    else{
                        FirebaseUser user= mAuth.getCurrentUser();
                        String[] parts = user.getEmail().split("@");
                        String first = parts[0];
                        String second = parts[1];
                        String username = first;
                        Intent intent = new Intent(getApplicationContext(), Register2Activity.class);
                        intent.putExtra("type","google_signin");
                        intent.putExtra("emailU",user.getEmail());
                        intent.putExtra("photourl",user.getPhotoUrl().toString());
                        intent.putExtra("username",username);
                        intent.putExtra("id",user.getUid());
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else{
            Intent in = new Intent(MainActivity.this, LoginActivity.class);
            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(in);
        }

    }

    public void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        friendsFragment = new ViewProfileActivity();
        chatsFragment = new ChatsFragment();
        mapsFragment = new MapsFragment();
        discoverFragment = new DiscoverFragmentNew();
        settingsFragment = new Settings2Fragment();
        moreFragment = new MoreFragment();
        adapter.addFragment(discoverFragment);
        adapter.addFragment(chatsFragment);
        adapter.addFragment(mapsFragment);
        adapter.addFragment(friendsFragment);
        adapter.addFragment(moreFragment);
        viewPager.setAdapter(adapter);
    }
    public void updateuserstatus(String state){
        userinfo.child(currentusers).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    userinfo.child(currentusers).child("userStatus").child("US_onlineStatus").setValue(state);
                    userinfo.child(currentusers).child("userStatus").child("US_timestamp").setValue(ServerValue.TIMESTAMP);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public long gettimestamp(){
        Date date = new Date();
        time = date.getTime();

        System.out.println("time in miliseconds :"+time);

        Timestamp ts = new Timestamp(time);
        System.out.println("Current time stamp:" + ts);

        return time;
    }
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog alertDialog = builder.create();

        View view = LayoutInflater.from(this).inflate(R.layout.exit_popup_layout,null);

        Button yes = view.findViewById(R.id.yes);
        Button no = view.findViewById(R.id.no);
        Button x = view.findViewById(R.id.x);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateuserstatus("offline");
                alertDialog.dismiss();
                finish();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        return;
    }

}

