package com.app.pingchat.Map;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.location.LocationListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.ArActivityPartners;
import com.app.pingchat.CitizenAlert.AlertAdapter;
import com.app.pingchat.CitizenAlert.NewsSetup;
import com.app.pingchat.CitizenAlert.UCA_Details;
import com.app.pingchat.Friend.FriendSelectorAdapter;
import com.app.pingchat.Friend.contacts;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Ping.PingAdapter2;
import com.app.pingchat.Ping.PingSetup;
import com.app.pingchat.Ping.UP_DetailsClass;
import com.app.pingchat.Ping.pingMetaData;
import com.app.pingchat.PlaceFieldSelector;
import com.app.pingchat.QRCouponScan;
import com.app.pingchat.R;
import com.app.pingchat.UserAuth.LoginActivity;
import com.app.pingchat.ViewBusinessProfileOther;
import com.app.pingchat.ViewImageActivity;
import com.app.pingchat.bottomSheetMapMenu;
import com.app.pingchat.bottomSheetSelectGhost;
import com.app.pingchat.bottomSheetViewImage;
import com.app.pingchat.getpartneradapter;
import com.app.pingchat.partnerDetailsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.maps.android.SphericalUtil;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;

public class MapsFragment extends Fragment implements bottomSheetViewImage.BottomSheetListener, bottomSheetSelectGhost.BottomSheetListener, bottomSheetMapMenu.BottomSheetListener, OnMapReadyCallback{

    private static final String TAG = "MapsFragment";
    private static final int REQUEST_PHONE_CALL = 123;
    FirebaseAuth mAuth;
    private DatabaseReference userinfo,locations,userfriends, userChats, personalchat;
    private DatabaseReference users,userFriends,ping, chats,discovers,partner,gpx_eureka;
    private ValueEventListener mlistener;
    CircleImageView dp, pic;
    TextView uname, uabout,noconnectivity;
    String current_user;
    FirebaseStorage storage;
    StorageReference storeRef;

//    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private final int REQUEST_LOCATION_PERMISSION = 1;

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationManager locationManager;
    private List<partnerDetailsClass> getpartnerclassList;
    ArrayList<pingMetaData> pingmetadatalist = new ArrayList<>();
    ArrayList<UP_DetailsClass> up_detailsClassList = new ArrayList<>();
    ArrayList<UP_DetailsClass> upnearbypinglist= new ArrayList<>();
    ArrayList <userDetailsClass> UD_DetailsClassList = new ArrayList<>();
    ArrayList<UCA_Details> alertClassList = new ArrayList<>();
    boolean isBtnClicked = false;
    private RequestQueue mQueue;
    private RecyclerView.Adapter adapter,adapter2;
    public String image;
    GoogleMap map;
    String timeleft;
    BroadcastReceiver batteryBroadcast;
    IntentFilter mIntentFilter;

    double currentlan,currentlon;
    PingAdapter2.pingrecycleradapterlist pingrecycleradapterlist2;
    FriendSelectorAdapter.friendselrecycleradapterlist friendselrecycleradapterlist2;
    AlertAdapter.alertrecycleradapterlist alertrecycleradapterlist2;
    getpartneradapter.partnerlistrecycleradapter partnerlistrecycleradapter;
    private BottomSheetBehavior bottomSheetBehavior,bottomSheetBehavior2;
    ImageView precise,frozen,father;

    ArrayList<com.app.pingchat.Friend.contacts> list;
    ArrayList<String> pingkey;
    contacts contacts;
    long time;

    private Context context;

    private static MapsFragment instance = null;

    View markeroffline,markeronline,m;

    private RecyclerView friendselector,pingselector,pingpartner,alertnearby;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    //location 11/12/18
    private LocationRequest mLocationRequest;
    private Location mLastLocation;

    private ImageButton normal,addalert;
    private ImageButton frozens;
    private ImageButton fathers;

    private ImageButton currentghostmode;
    private String dpicture;

    public String getDpicture() { return dpicture; }

    public void setDpicture(String dpicture) { this.dpicture = dpicture; }

    String datetimes;
    String datess;
    String businessmodel;
    private DatabaseReference publicping, publiccitizen;
    private DatabaseReference userping;

    FriendSelectorAdapter friendSelectorAdapter;

    public ImageButton getCurrentghostmode() {
        return currentghostmode;
    }

    public void setCurrentghostmode(ImageButton currentghostmode) {
        this.currentghostmode = currentghostmode;
    }

    Uri uriSavedImage;
    private Uri avatar;
    private String pingImage;
    public Uri getAvatar() {
        return avatar;
    }
    public void setAvatar(Uri avatar) {
        this.avatar = avatar;
    }

    double lat,lon;
    String ghost;
    private Marker mMarker, mMarkerland;
    RelativeLayout layout;
    View viewer;
    View viewLayout;
    View friendpopup;
    View partnerpopup;
    View pingpopup;
    View alertpopup;
    private Button searchlocation,friendsselector,sendping,pingsselector,partnerselector,newsselector;
    private ImageButton submenu,currentloc,adsBtn;
    private HashMap<String, Marker> markersArray = new HashMap<String, Marker>();
    private HashMap<String, String> markersPingArr= new HashMap<String, String>();
    private HashMap<String, String> markerAlerts = new HashMap<String, String>();

    private  TextView topname,emel,name, message, dot, lokasi, one, two, three, four, five, noktah, masa;
    private ImageView photo;
    HashMap<String, Object> timestampCreated;

    private LocationListener locationListener;

    public double lastlatitude,lastlongitude;
    private String new_messageid;
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    //private getpartneradapter.partnerlistrecycleradapter y;

    PingAdapter2 pingAdapter;
    AlertAdapter alertAdapter;

    final static int PERMISSION_ALL = 1;
    final static String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    private ArrayList<String> allfriendkey = new ArrayList<>();
    private BitmapDescriptor icon;

    final ArrayList pinglist = new ArrayList();

    Date datesss;

    PlaceFieldSelector fieldSelector;

    private static final int REQ_CODE_VERSION_UPDATE = 530;
    private AppUpdateManager appUpdateManager;
    private InstallStateUpdatedListener installStateUpdatedListener;
    CoordinatorLayout coordinatorLayout;

    private bottomSheetSelectGhost bottomSheetSelectGhost;
    private bottomSheetMapMenu bottomSheetMapMenu;
    private bottomSheetViewImage bottomSheetViewImage;

    private String api_key = "AIzaSyDOewqTvU226Fe7ZTYIobI58oJomTnPIMQ";
    private String devicetoken,buildversion;

    private View bottomsheetping;
    private View bottomsheetpartner;
    private View bottomsheetalert;
    private View bottomsheet;

    private String userlocality;
    private EditText search_friend;
    boolean isKeyboardShowing = false;
    String messageType;
    private RequestQueue queue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.maps_fragment, null, false);
        mAuth = FirebaseAuth.getInstance();
        uname = (TextView) v.findViewById(R.id.uname);
        dp = (CircleImageView) v.findViewById(R.id.imageView3);
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        pingkey = new ArrayList<>();

        coordinatorLayout =v.findViewById(R.id.coordinatorlayout);
        checkForAppUpdate();
        instance = this;

        context = getContext();

        topname = v.findViewById(R.id.top_uname);
        emel = v.findViewById(R.id.mail);
        name = v.findViewById(R.id.u_name);
        pic = v.findViewById(R.id.user_image);
        message = v.findViewById(R.id.mess);

        searchlocation = v.findViewById(R.id.searchlocation);
        friendsselector = v.findViewById(R.id.friendselector);
        sendping = v.findViewById(R.id.sendping);
        pingsselector = v.findViewById(R.id.pingselector);
        partnerselector = v.findViewById(R.id.partnerselector);
        newsselector = v.findViewById(R.id.send_news);
        submenu = v.findViewById(R.id.submenu);
        currentloc = v.findViewById(R.id.currentloc);
        adsBtn = v.findViewById(R.id.adsOnOff);
        noconnectivity = v.findViewById(R.id.nonetwork);
        search_friend = v.findViewById(R.id.search_friends);

        mQueue  = Volley.newRequestQueue(getContext());
        getpartnerclassList = new ArrayList<>();

        //displaypartnerbottomsheet();
        bottomsheet = v.findViewById(R.id.bottom_sheet);
        bottomsheetping = v.findViewById(R.id.bottom_sheet_ping);
        final View bottomsubmenu = v.findViewById(R.id.submenu_sheet);
        bottomsheetpartner = v.findViewById(R.id.bottom_sheet_partner);
        bottomsheetalert = v.findViewById(R.id.bottom_sheet_alert);

        viewer = inflater.inflate(R.layout.user_detail_onmap_layout,null);
        viewLayout = inflater.inflate(R.layout.ping_detail_onmap_layout,null);
        friendpopup = inflater.inflate(R.layout.friend_sel_popup,null);
        partnerpopup = inflater.inflate(R.layout.pingpartner_map_item,null);
        pingpopup = inflater.inflate(R.layout.ping_sel_popup,null);
        alertpopup = inflater.inflate(R.layout.alert_item_view,null);

        queue = Volley.newRequestQueue(getContext());
        bottomSheetSelectGhost = new bottomSheetSelectGhost();
        bottomSheetSelectGhost.setListener(this);

        bottomSheetMapMenu = new bottomSheetMapMenu();
        bottomSheetMapMenu.setListener(this);

        bottomSheetViewImage = new bottomSheetViewImage();
        bottomSheetViewImage.setListener(this);

        addalert = v.findViewById(R.id.add_alert);

        addalert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotonews();
            }
        });
        search_friend.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) { filterfriend(editable.toString()); }
        });


        Places.initialize(getContext().getApplicationContext(),api_key);

        if (!Places.isInitialized()) {
            Places.
                    initialize(getContext().getApplicationContext(), api_key);
        }

        fieldSelector = new PlaceFieldSelector();


        bottomSheetBehavior = BottomSheetBehavior.from(bottomsheet);
        bottomSheetBehavior2 = BottomSheetBehavior.from(bottomsheetping);

        layout = v.findViewById(R.id.relat);

        pingselector = (RecyclerView) v.findViewById(R.id.pings);
        pingselector.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));

        friendselector = (RecyclerView) v.findViewById(R.id.dimap);
        friendselector.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));

        pingpartner = (RecyclerView) v.findViewById(R.id.partner);
        pingpartner.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));

        alertnearby = (RecyclerView) v.findViewById(R.id.alert_nearby);
        alertnearby.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));

        partner = FirebaseDatabase.getInstance().getReference().child("Partners");
        partner.keepSynced(true);

        personalchat = FirebaseDatabase.getInstance().getReference().child("Chats").child("Personal");
        personalchat.keepSynced(true);

        users = FirebaseDatabase.getInstance().getReference().child("Users");

        gpx_eureka = FirebaseDatabase.getInstance().getReference().child("gpx");

        list = new ArrayList<>();
        contacts = new contacts();

        normal = v.findViewById(R.id.ghostnormal);
        frozens = v.findViewById(R.id.ghostbuttonOff);
        fathers = v.findViewById(R.id.ghostbuttonOn);
        submenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetMapMenu.show(getActivity().getSupportFragmentManager(),"map_menu");
            }
        });

        bottomSheetBehavior2.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                String state = "N/A";
                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    state = "STATE_EXPANDED";
                } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    state = "STATE_COLLAPSED";
                }
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                String state = "N/A";

                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    state = "STATE_EXPANDED";
                } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    state = "STATE_COLLAPSED";
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        markeroffline =((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_offline, null);
        markeronline = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        requestLocationPermission();
        getContactPermission();
        getCameraPermissions();
        getExternalReadStorage();
        getExternalWriteStorage();
        getPhonecallPermissions();
        locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);

        friendselrecycleradapterlist2 = new FriendSelectorAdapter.friendselrecycleradapterlist(){
            @Override
            public void onUserClickedfriendlist(final int position){
                users.child(current_user).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            String myFriend = dataSnapshot.getKey();
                            String status = dataSnapshot.child(UD_DetailsClassList.get(position).getUD_userID()).getValue().toString();
                            System.out.println("friend id: "+UD_DetailsClassList.get(position).getUD_userID());
                            if (status.equals("friend")) {
                                final TextView uname = friendpopup.findViewById(R.id.user_name);
                                uname.setText(UD_DetailsClassList.get(position).getUD_displayName());
                                userinfo.child(UD_DetailsClassList.get(position).getUD_userID()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        DataSnapshot userstatus = dataSnapshot.child("userStatus");
                                        DataSnapshot userdetails = dataSnapshot.child("userDetails");
                                        String ghoststatus = userstatus.child("US_ghostStatus").getValue().toString();
                                        String on9status = userstatus.child("US_onlineStatus").getValue().toString();
                                        String latitude = userstatus.child("US_latitude").getValue().toString();
                                        String longitude = userstatus.child("US_longitude").getValue().toString();
                                        String btry_lvl = userstatus.child("US_battery").getValue().toString();
                                        int btry_int = Integer.parseInt(btry_lvl);
                                        final ImageView ghostStat = friendpopup.findViewById(R.id.ghost_sel);
                                        final TextView ghostText = friendpopup.findViewById(R.id.ghost_txt);
                                        final ImageView on9txt = friendpopup.findViewById(R.id.onlineStat);
                                        final TextView msg_time = friendpopup.findViewById(R.id.min);
                                        final TextView distance = friendpopup.findViewById(R.id.fr_location);
                                        final TextView battery = friendpopup.findViewById(R.id.battery_percent);
                                        final TextView mesejlast = friendpopup.findViewById(R.id.msg_last);
                                        final EditText chatmap = friendpopup.findViewById(R.id.typing_box);
                                        final Button sendBtn = friendpopup.findViewById(R.id.send_btn);
                                        final ImageView btry100 = friendpopup.findViewById(R.id.bateri_life_100);
                                        final ImageView btry79 = friendpopup.findViewById(R.id.bateri_80);
                                        final ImageView btry40 = friendpopup.findViewById(R.id.bateri_40);
                                        final ImageView btry20 = friendpopup.findViewById(R.id.bateri_10);
                                        if(on9status.equals("online")){
                                            on9txt.setImageResource(R.drawable.on9marker);
                                        } else{
                                            on9txt.setImageResource(R.drawable.of9marker);
                                        }
                                        if(btry_int >= 80){
                                            btry100.setVisibility(View.VISIBLE);
                                            btry79.setVisibility(View.GONE);
                                            btry40.setVisibility(View.GONE);
                                            btry20.setVisibility(View.GONE);
                                        }else if(btry_int < 80 && btry_int >= 50){
                                            btry100.setVisibility(View.GONE);
                                            btry40.setVisibility(View.GONE);
                                            btry20.setVisibility(View.GONE);
                                            btry79.setVisibility(View.VISIBLE);
                                        }else if(btry_int < 50 && btry_int >= 30){
                                            btry100.setVisibility(View.GONE);
                                            btry79.setVisibility(View.GONE);
                                            btry20.setVisibility(View.GONE);
                                            btry40.setVisibility(View.VISIBLE);
                                        }else if(btry_int < 30 ){
                                            btry100.setVisibility(View.GONE);
                                            btry79.setVisibility(View.GONE);
                                            btry40.setVisibility(View.GONE);
                                            btry20.setVisibility(View.VISIBLE);
                                        }

                                        sendBtn.setEnabled(false);
                                        chatmap.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void afterTextChanged(Editable s) {
                                                // TODO Auto-generated method stub
                                            }
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                                // TODO Auto-generated method stub
                                            }
                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                if(chatmap.length() == 0) {
                                                    sendBtn.setEnabled(false);
                                                    //Toast.makeText(ChatsActivity.this, "Cannot send an empty message", Toast.LENGTH_SHORT).show();
                                                }
                                                else{
                                                    sendBtn.setEnabled(true);
                                                }
                                            }
                                        });

                                        sendBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
//                                                            Toast.makeText(getContext(), "send button pressed", Toast.LENGTH_SHORT).show();
                                                userChats.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot.exists()) {
                                                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                                                String chatsID = dataSnapshot1.getKey();
                                                                String friendID = dataSnapshot1.child("UCP_friendID").getValue().toString();
                                                                if(friendID.equals(UD_DetailsClassList.get(position).getUD_userID())) {
                                                                    HashMap new_chat_details = new HashMap();
                                                                    new_messageid = personalchat.push().push().getKey();
                                                                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                                                                    new_chat_details.put("CP_chatID", chatsID);
                                                                    new_chat_details.put("CP_lastMessageID", new_messageid);
                                                                    new_chat_details.put("CP_lastMessage", chatmap.getText().toString());
                                                                    new_chat_details.put("CP_lastMessageTimestamp", timestamp.getTime());
                                                                    new_chat_details.put("CP_lastMessageMediaType", "text");

                                                                    HashMap personal_chat = new HashMap();
                                                                    personal_chat.put("CP_message", chatmap.getText().toString());
                                                                    personal_chat.put("CP_messageSenderID", current_user);
                                                                    personal_chat.put("CP_mediaType", "text");
                                                                    personal_chat.put("CP_messageTimestamp", timestamp.getTime());

                                                                    personalchat.child(chatsID).child("CP_personalDetails").setValue(new_chat_details);
                                                                    personalchat.child(chatsID).child("CP_personalChatSession").child(new_messageid).setValue(personal_chat);
                                                                    users.child(current_user).child("userConversations").child("Personal").child(chatsID).child("UCP_lastMessageID").setValue(new_messageid);
                                                                    users.child(friendID).child("userConversations").child("Personal").child(chatsID).child("UCP_lastMessageID").setValue(new_messageid);
                                                                    chatmap.setText("");
                                                                    sendNoti(chatsID, current_user, friendID);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        });
                                        chatmap.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                            @Override
                                            public void onFocusChange(View v, boolean hasFocus) {
                                                if (hasFocus) {
                                                    //got focus
//                                                        Toast.makeText(getContext(), "keyboard up", Toast.LENGTH_SHORT).show();
                                                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                                                } else {
                                                    //lost focus
//                                                        Toast.makeText(getContext(), "keyboard down", Toast.LENGTH_SHORT).show();
                                                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                                                }
                                            }
                                        });
                                        if (ghoststatus.equals("disable") || ghoststatus.equals("super")) {
//                                            Toast.makeText(getContext(), "Cannot retrieve location. User is in ghost mode", Toast.LENGTH_SHORT).show();
                                            layout.removeView(friendpopup);
                                            viewPopupwakeUp(UD_DetailsClassList.get(position).getUD_userID());
                                        } else if (ghoststatus.equals("accurate")) {
                                            if(!latitude.equals("0") && !longitude.equals("0")){
                                                ghostStat.setImageResource(R.drawable.pre_ghost);
                                                ghostText.setText("Ghost mode off");
                                                final LatLng location22 = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                                                map.clear();
                                                layout.removeView(friendpopup);
                                                layout.addView(friendpopup);
                                                Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.scale_up);
                                                friendpopup.startAnimation(a);
                                                final Marker currentmarker = markersArray.get(UD_DetailsClassList.get(position).getUD_userID());
                                                if (currentmarker != null) {

                                                    LatLng from = new LatLng(getLastlatitude(), getLastlongitude());
                                                    LatLng to = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

                                                    Double d = SphericalUtil.computeDistanceBetween(from, to);
                                                    Double dd = d / 1000;
//                                                        String.format("%.2f", d)
                                                    distance.setText(String.format("%.3f", dd));
                                                    battery.setText(btry_lvl + "%");
                                                    currentmarker.setPosition(location22);
                                                    currentmarker.setVisible(true);
                                                }
                                                displayChatonMap(UD_DetailsClassList.get(position).getUD_userID(),mesejlast,msg_time);
                                                if (UD_DetailsClassList.get(position).getUD_avatarURL().startsWith("h")) {
                                                    GlideApp.with(getContext())
                                                            .asBitmap()
                                                            .load(UD_DetailsClassList.get(position).getUD_avatarURL())
                                                            .into(new SimpleTarget<Bitmap>() {
                                                                @Override
                                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                                    Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                                    mMarker = map.addMarker(new MarkerOptions()
                                                                            .snippet(UD_DetailsClassList.get(position).getUD_userID())
                                                                            .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(), crop_image, markeroffline)))
                                                                            .position(location22));
                                                                    animateMarker(mMarker, location22, false);
                                                                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                                                                    markerAlerts.put(mMarker.getId(),"kawan");

                                                                }
                                                            });
                                                } else {
                                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(UD_DetailsClassList.get(position).getUD_avatarURL());
                                                    GlideApp.with(getContext())
                                                            .asBitmap()
                                                            .load(storageReference)
                                                            .into(new SimpleTarget<Bitmap>() {
                                                                @Override
                                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                                    Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                                    mMarker = map.addMarker(new MarkerOptions()
                                                                            .snippet(UD_DetailsClassList.get(position).getUD_userID())
                                                                            .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(), crop_image, markeroffline)))
                                                                            .position(location22));
                                                                    animateMarker(mMarker, location22, false);
                                                                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                                                                    markerAlerts.put(mMarker.getId(),"kawan");

                                                                }
                                                            });
                                                }
                                            }else{
                                                layout.removeView(friendpopup);
                                                viewPopupwakeUp(UD_DetailsClassList.get(position).getUD_userID());
                                            }
                                        } else if(ghoststatus.equals("frozen")){
                                            if(!latitude.equals("0") && !longitude.equals("0")){
                                                ghostStat.setImageResource(R.drawable.fro_ghost);
                                                ghostText.setText("Static ghost");
                                                final LatLng location22 = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                                                map.clear();
                                                layout.removeView(friendpopup);
                                                layout.addView(friendpopup);
                                                Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.scale_up);
                                                friendpopup.startAnimation(a);
                                                final Marker currentmarker = markersArray.get(UD_DetailsClassList.get(position).getUD_userID());
                                                if (currentmarker != null) {

                                                    LatLng from = new LatLng(getLastlatitude(),getLastlongitude());
                                                    LatLng to = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));

                                                    Double d = SphericalUtil.computeDistanceBetween(from,to);
                                                    Double dd = d/1000;
//                                                        String.format("%.2f", d)
                                                    distance.setText(String.format("%.3f", dd));
                                                    battery.setText(btry_lvl+"%");
                                                    currentmarker.setPosition(location22);
                                                    currentmarker.setVisible(true);
                                                }
                                                displayChatonMap(UD_DetailsClassList.get(position).getUD_userID(),mesejlast,msg_time);
                                                if (UD_DetailsClassList.get(position).getUD_avatarURL().startsWith("h")) {
                                                    GlideApp.with(getContext())
                                                            .asBitmap()
                                                            .load(UD_DetailsClassList.get(position).getUD_avatarURL())
                                                            .into(new SimpleTarget<Bitmap>() {
                                                                @Override
                                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                                    Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                                    mMarker = map.addMarker(new MarkerOptions()
                                                                            .snippet(UD_DetailsClassList.get(position).getUD_userID())
                                                                            .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(), crop_image, markeroffline)))
                                                                            .position(location22));
                                                                    animateMarker(mMarker, location22, false);
                                                                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                                                                    markerAlerts.put(mMarker.getId(),"kawan");

                                                                }
                                                            });
                                                } else {
                                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(UD_DetailsClassList.get(position).getUD_avatarURL());
                                                    GlideApp.with(getContext())
                                                        .asBitmap()
                                                        .load(storageReference)
                                                        .into(new SimpleTarget<Bitmap>() {
                                                            @Override
                                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                            Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                            mMarker = map.addMarker(new MarkerOptions()
                                                                .snippet(UD_DetailsClassList.get(position).getUD_userID())
                                                                .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(), crop_image, markeroffline)))
                                                                .position(location22));
                                                            animateMarker(mMarker, location22, false);
                                                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                                                            markerAlerts.put(mMarker.getId(),"kawan");

                                                        }
                                                    });
                                                }
                                            }else{
                                                layout.removeView(friendpopup);
                                                viewPopupwakeUp(UD_DetailsClassList.get(position).getUD_userID());
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                    }
                                });
                            } else if (status.equals("block")) {
                                Toast.makeText(getContext(), "You had blocked this user, please unblock to view this user", Toast.LENGTH_SHORT).show();
                            } else if (status.equals("blocked")) {
                                Toast.makeText(getContext(), "You had been blocked by this user", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        };

        pingrecycleradapterlist2= new PingAdapter2.pingrecycleradapterlist() {
            @Override
            public void onUserClickedpinglist(final int position) {
                Log.d(TAG,"pingkey"+ up_detailsClassList.get(position).getUPD_pingID());
                String pingkey = up_detailsClassList.get(position).getUPD_pingID();
                bottomSheetViewImage.setMediaURL(up_detailsClassList.get(position).getUPD_mediaURL());

                final TextView uname = pingpopup.findViewById(R.id.user_name);
                final TextView address = pingpopup.findViewById(R.id.address);
                final TextView date = pingpopup.findViewById(R.id.date_);
                final Button playbutton = pingpopup.findViewById(R.id.play_btn);
                final TextView hour = pingpopup.findViewById(R.id.hour);
                final TextView time = pingpopup.findViewById(R.id.time_);

                SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd,yyyy");
                String y = currentDate.format(up_detailsClassList.get(position).getUPD_dateStart());

                SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a");
                String z = currentTime.format(up_detailsClassList.get(position).getUPD_dateStart());

                uname.setText(up_detailsClassList.get(position).getDisplayName());
                address.setText(up_detailsClassList.get(position).getUPD_address());
                date.setText(y);
                time.setText(z);
                hour.setText(getTimeleft(String.valueOf(up_detailsClassList.get(position).getUPD_dateEnd())));

                System.out.println("pingdate "+ up_detailsClassList.get(position).getUPD_dateStart());
                playbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //gotoimageview("pingImage/"+pingkey+".jpg");
//                        bottomSheetViewImage.show(getActivity().getSupportFragmentManager(),"viewimage");
                        Bundle args = new Bundle();
                        args.putString("key", "p");
                        args.putString("url", up_detailsClassList.get(position).getUPD_mediaURL());
//                        args.putString("vurl", up_detailsClassList.get(position).getUPD_mediaThumbnailURL());
                        args.putString("type", up_detailsClassList.get(position).getUPD_mediaType());
                        args.putString("caption", up_detailsClassList.get(position).getUPD_caption());
                        args.putString("ownerid", up_detailsClassList.get(position).getUPD_ownerID());
                        args.putString("pingid", up_detailsClassList.get(position).getUPD_pingID());
                        args.putString("datestart", z);
                        bottomSheetViewImage dialog = new bottomSheetViewImage();
                        dialog.setArguments(args);
                        dialog.show(getFragmentManager(),"c");
                    }
                });
                userinfo.child(up_detailsClassList.get(position).getUPD_ownerID()).child("userPing").child("UP_List").child(up_detailsClassList.get(position).getUPD_pingID()).child("UP_Location").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String upl_latitude = dataSnapshot.child("UPL_latitude").getValue().toString();
                        String upl_longitude = dataSnapshot.child("UPL_longitude").getValue().toString();

                        final LatLng location22 = new LatLng(Double.parseDouble(upl_latitude), Double.parseDouble(upl_longitude));

                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                        map.clear();
                        final BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.locked_ping);
                        mMarker= map.addMarker(new MarkerOptions().snippet(up_detailsClassList.get(position).getUPD_ownerID()).icon(icon).position(location22));
                        markerAlerts.put(mMarker.getId(),"ping");
                        layout.removeView(pingpopup);
                        layout.addView(pingpopup);
                        Animation a = AnimationUtils.loadAnimation(getContext(),R.anim.scale_up);
                        pingpopup.startAnimation(a);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }
        };

        alertrecycleradapterlist2= new AlertAdapter.alertrecycleradapterlist() {
            @Override
            public void onUserClickedalertlist(int position) {
                userinfo.child(alertClassList.get(position).getUCAD_ownerID()).child("userCitizenAlert").child("UCA_List").child(alertClassList.get(position).getUCAD_alertID()).child("UCA_Location").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String ucl_latitude = dataSnapshot.child("UCAL_latitude").getValue().toString();
                        String ucl_longitude = dataSnapshot.child("UCAL_longitude").getValue().toString();

                        final LatLng location22 = new LatLng(Double.parseDouble(ucl_latitude), Double.parseDouble(ucl_longitude));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 25));

                        final String alertType = alertClassList.get(position).getUCAD_alertType();
                        final String alertId = alertClassList.get(position).getUCAD_alertID();

                        if (alertType.equals("accident")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.accident0),alertId,location22);
                        }
                        else if (alertType.equals("landslide")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.landslide0),alertId,location22);
                        }
                        else if (alertType.equals("rain")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.rain0),alertId,location22);
                        }
                        else if (alertType.equals("trafficJam")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.trafficjam0),alertId,location22);
                        }
                        else if (alertType.equals("police")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.police0),alertId,location22);
                        }
                        else if (alertType.equals("closure")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.closure0),alertId,location22);
                        }
                        else if (alertType.equals("hazard")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.hazard0),alertId,location22);
                        }
                        else if (alertType.equals("trafficLight")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.trafficlight0),alertId,location22);
                        }else if (alertType.equals("hot")) {
                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.hot0),alertId,location22);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }
        };
        if(!isConnected(getContext())) {
            noconnectivity.setVisibility(View.VISIBLE);
            //buildDialog(getContext()).show();
        }
        else {

            //Toast.makeText(getContext(),"Welcome", Toast.LENGTH_SHORT).show();
        }
        if (mAuth.getCurrentUser() == null) {

        } else {
            if(!mAuth.getCurrentUser().isEmailVerified()){
                Toast.makeText(getContext(), "Please verify your email!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(),LoginActivity.class));
            }
            else{
                if(Build.VERSION.SDK_INT >=23 && !isPermissionGranted()){
                    requestPermissions(PERMISSIONS,PERMISSION_ALL);
                }else{
                    System.out.println("masuk sini untuk request location");
                    requestPermissions(PERMISSIONS,PERMISSION_ALL);
                }
                if(!isLocationEnabled())
                {
                    showAlert(1);
                }
                current_user = mAuth.getCurrentUser().getUid();
                userFriends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Conversation").child("Personal");
                userFriends.keepSynced(true);
                userinfo = FirebaseDatabase.getInstance().getReference().child("Users");
                // userinfo.keepSynced(true);
                locations = FirebaseDatabase.getInstance().getReference().child("userStatus");
                locations.keepSynced(true);
                userfriends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Friends");
                userfriends.keepSynced(true);
                ping = FirebaseDatabase.getInstance().getReference().child("PingSession");
                ping.keepSynced(true);
                chats = FirebaseDatabase.getInstance().getReference().child("Chats");
                chats.keepSynced(true);
                discovers = FirebaseDatabase.getInstance().getReference().child("DiscoverSession").child("Friend");
                discovers.keepSynced(true);
                devicetoken = FirebaseInstanceId.getInstance().getToken();
                publicping = FirebaseDatabase.getInstance().getReference().child("PublicPing");
                userping = FirebaseDatabase.getInstance().getReference().child("User").child(current_user).child("userPing");
                publiccitizen = FirebaseDatabase.getInstance().getReference().child("PublicCitizenAlert");
                userChats = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("userConversations").child("Personal");
                userChats.keepSynced(true);
                RetrieveUserInfo();
                checkstatus();
                get_all_ping();
                loadfriendslocation();
                checkPingNearby();
                c2();
            }
        }
        return v;
    }

    private void displayChatonMap(String ud_userID, TextView mesejlast, TextView msg_time){
        userChats.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                        String chatsID = dataSnapshot1.getKey();
                        String friendID = dataSnapshot1.child("UCP_friendID").getValue().toString();
                        if (friendID.equals(ud_userID)) {
                            chats.child("Personal").child(chatsID).child("CP_personalDetails").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        String lastmsg = dataSnapshot.child("CP_lastMessage").getValue().toString();
                                        String msgTime = dataSnapshot.child("CP_lastMessageTimestamp").getValue().toString();
                                        mesejlast.setText(lastmsg);
                                        msg_time.setText(getdatetime(Long.valueOf(msgTime)));

                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void viewPopupwakeUp(String ud_userID) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        View view = LayoutInflater.from(getContext()).inflate(R.layout.wake_up_popup, null);

        final LottieAnimationView animationView = (LottieAnimationView) view.findViewById(R.id.animation_view);
        final TextView txt_wakeup = view.findViewById(R.id.textView3);
        final Button wakebtn = view.findViewById(R.id.wake_upbtn);
        final Button cancelbtn = view.findViewById(R.id.exit_alert);

        animationView.setAnimation("peace_animation.json");
        animationView.playAnimation();

        users.child(ud_userID).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UD_displayName")){
                    String name = dataSnapshot.child("UD_displayName").getValue().toString();
                    txt_wakeup.setText("Hmmm...It seems that "+name+" location is temporary disabled or hidden. "+name+" may be resting right now.");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        wakebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "send wake up notification", Toast.LENGTH_SHORT).show();
                sendNotiWakeUp(ud_userID);
                alertDialog.dismiss();
            }
        });

        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
    }

    private void sendNotiWakeUp(String ud_userID) {

        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/notification/"+current_user+"/wakeup/"+ud_userID;

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
//        System.out.println("chatid:"+chat_id);
//        System.out.println("url:"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public float getBatteryLevel() {
        Intent batteryIntent = getContext().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float btry_lvl = ((float)level / (float)scale) * 100.0f;
        int newBat = Math.round(btry_lvl);

        // Error checking that probably isn't needed but I added just in case.
        if(level == -1 || scale == -1) {

            System.out.println("battery:"+newBat);
            return 50.0f;
        }

        return newBat;
    }

    private void sendNoti(String chat_id, String currentid, String friendid){
        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/notification/"+currentid+"/sendMsg/"+friendid;

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
        String avatar_url = image;
        postParams.put("chatID",chat_id);
        postParams.put("type","personal");
        postParams.put("msgType",getMessageType());
        postParams.put("name",name);
//        System.out.println("chatid:"+chat_id);
//        System.out.println("url:"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    public boolean isConnected(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if(networkInfo!=null && networkInfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        }else return false;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMapToolbarEnabled(false);

        clikss(normal);
        clikss(frozens);
        clikss(fathers);

        displayuserLocation();
        mlistener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String lat = dataSnapshot.child("lat").getValue().toString();
                    String lon = dataSnapshot.child("lon").getValue().toString();
                    double latialert = Double.parseDouble(lat);
                    double longalert = Double.parseDouble(lon);
                    final LatLng location = new LatLng(latialert, longalert);
                    //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 25));
                    map.clear();
                    mMarker = map.addMarker(new MarkerOptions()
                            .snippet("eureka")
                            .position(location)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.tin1x)));
                    animateMarker(mMarker, location, false);
                    markerAlerts.put(mMarker.getId(),"ads");
                    map.setOnMarkerClickListener(MapsFragment.this::onMarkerClick);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        adsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBtnClicked) {
                    map.clear();
                    loadfriendslocation();
                    c2();
                    checkPingNearby();
                    gpx_eureka.child("EurekaDrinks").removeEventListener(mlistener);
                    System.out.println("unclicked");
                } else {
                  //  map.clear();
                    gpx_eureka.child("EurekaDrinks").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String lat = dataSnapshot.child("lat").getValue().toString();
                            String lon = dataSnapshot.child("lon").getValue().toString();
                            double latialert = Double.parseDouble(lat);
                            double longalert = Double.parseDouble(lon);
                            final LatLng location = new LatLng(latialert, longalert);
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 20));
                            gpx_eureka.child("EurekaDrinks").addValueEventListener(mlistener);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    System.out.println("clicked");
                }
                isBtnClicked = !isBtnClicked;
            }
        });
//        displayAdsOnMap();
        c2();
//        displayAdsOnMap();

        map.clear();
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(bottomSheetBehavior.getState()== BottomSheetBehavior.STATE_EXPANDED && layout != null){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    if(friendpopup.getVisibility()==View.VISIBLE){
                        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.scale_down);
                        friendpopup.startAnimation(animation);
                        layout.removeView(friendpopup);
                    }

                    if(partnerpopup.getVisibility()==View.VISIBLE){
                        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.scale_down);
                        partnerpopup.startAnimation(animation);
                        layout.removeView(partnerpopup);
                    }

                    if(pingpopup.getVisibility()==View.VISIBLE){
                        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.scale_down);
                        pingpopup.startAnimation(animation);
                        layout.removeView(pingpopup);
                    }
                    if(alertpopup.getVisibility()==View.VISIBLE){
                        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.scale_down);
                        alertpopup.startAnimation(animation);
                        layout.removeView(pingpopup);
                    }
                    Animation animate = AnimationUtils.loadAnimation(getContext(),R.anim.slide_up);
                    viewLayout.startAnimation(animate);
                    layout.removeView(viewLayout);
                }
            }
        });

        map.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.hide);
                friendpopup.setVisibility(View.INVISIBLE);
                submenu.setVisibility(View.GONE);
                currentloc.setVisibility(View.GONE);
                currentloc.startAnimation(animation);
                submenu.startAnimation(animation);
            }
        });

        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                Animation animation1 = AnimationUtils.loadAnimation(getContext(),R.anim.alpha);
                submenu.setVisibility(View.VISIBLE);
                currentloc.setVisibility(View.VISIBLE);
                currentloc.startAnimation(animation1);
                friendpopup.setVisibility(View.VISIBLE);
                submenu.startAnimation(animation1);
            }
        });

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                setLastlatitude(location.getLatitude());
                setLastlongitude(location.getLongitude());
                if(normal.getVisibility() == View.VISIBLE){
                    ghost = "accurate";
                    saveUserLocation(location.getLatitude(),location.getLongitude());
                }
                else if(frozens.getVisibility() == View.VISIBLE){
                    System.out.println("freeze!");
                    ghost = "frozen";
                }
                else if (fathers.getVisibility() == View.VISIBLE){
                    System.out.println("full ghostmode");
                    ghost = "disable";
                }
                checkPingNearby();
//                displayAdsOnMap();
//                checkArLocation();
//                displayARmarker();
            }
            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {}
            @Override
            public void onProviderEnabled(String s) {}
            @Override
            public void onProviderDisabled(String s) {}
        };

        currentloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.clear();
                if(!isLocationEnabled())
                {
                    showAlert(1);
                }
                else{

                    loadfriendslocation();
                 //   checkAlertNearby();
                    c2();
                    checkPingNearby();
                    if (getLastlatitude()!=0) {
                        System.out.println("location manager x null");
                        LatLng l = new LatLng(getLastlatitude(),getLastlongitude());
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(l,15));
                    }
                    else{
                        System.out.println("location manager == null");
                    }
                }
            }
        });

        if (Build.VERSION.SDK_INT < 23) {
            System.out.println("this want run < 23");
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                System.out.println("bnde ni run");
                return;
            } else {
                System.out.println("else request location run");
                map.setMyLocationEnabled(true);
                map.getUiSettings().setMyLocationButtonEnabled(false);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000,10,locationListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 10, locationListener);
            }
        } else {
            System.out.println("this want run else");
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                map.setMyLocationEnabled(true);
                map.getUiSettings().setMyLocationButtonEnabled(false);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 10, locationListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 10, locationListener);
            }
        }
    }

    public void checkPingNearby(){
        upnearbypinglist = new ArrayList<>();
        publicping.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    System.out.println("no public ping available");
                }
                else {
                    System.out.println("public ping available");
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        if (dataSnapshot1.hasChild("PP_ownerID")) {
                            final UP_DetailsClass up_detailsClass = new UP_DetailsClass();
                            String pcownerid = dataSnapshot1.child("PP_ownerID").getValue().toString();
                            userinfo.child(pcownerid).child("userPing").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        DataSnapshot upavailable = dataSnapshot.child("UP_Available");

                                        System.out.println("ping available");
                                        for (DataSnapshot dataSnapshot1 : upavailable.getChildren()) {
                                            String pavailable = dataSnapshot1.getKey();
                                            DataSnapshot uplist = dataSnapshot.child("UP_List").child(pavailable).child("UP_Location");
                                            String pavailabletype = dataSnapshot1.getValue().toString();

                                            if(pavailabletype.equals("public")){
                                                System.out.println("ping available id:" + pavailable);
                                                if(uplist.exists()){
                                                    String newLat = uplist.child("UPL_latitude").getValue().toString();
                                                    String newLon = uplist.child("UPL_longitude").getValue().toString();
                                                    double latialert = Double.parseDouble(newLat);
                                                    double longalert = Double.parseDouble(newLon);
                                                    double radiusInMeters = 500.0;
                                                    float[] distance = new float[2];

                                                    currentlan = MapsFragment.getInstance().lastlatitude;
                                                    currentlon = MapsFragment.getInstance().lastlongitude;

                                                    Location.distanceBetween(currentlan, currentlon, latialert, longalert, distance);

                                                    if (distance[0] >= radiusInMeters) {
                                                        System.out.println("ping jauh dri location" + distance);
                                                        Log.e(TAG, "error!!!");
                                                    }
                                                    else{
                                                        createmaerkerpingnearby(newLat,newLon,pcownerid);
                                                    }
                                                }
                                            }
                                        }
                                    }else { System.out.println("no ping available"); }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                            });
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        userinfo.child(current_user).child("userPing").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    System.out.println("ping available");
                    DataSnapshot upavailable = dataSnapshot.child("UP_Available");
                    for (DataSnapshot dataSnapshot1 : upavailable.getChildren()) {
                        String pavailable = dataSnapshot1.getKey();
                        DataSnapshot uplist = dataSnapshot.child("UP_List").child(pavailable).child("UP_Location");
                        String type = dataSnapshot1.getValue().toString();
                        System.out.println("friend available id:" + pavailable);
                        if(type.equals("friend")){
                            System.out.println("ping available id:" + pavailable);
                            if(uplist.exists()){
                                String newLat = uplist.child("UPL_latitude").getValue().toString();
                                String newLon = uplist.child("UPL_longitude").getValue().toString();
                                double latialert = Double.parseDouble(newLat);
                                double longalert = Double.parseDouble(newLon);
                                double radiusInMeters = 500.0;
                                float[] distance = new float[2];

                                currentlan = MapsFragment.getInstance().lastlatitude;
                                currentlon = MapsFragment.getInstance().lastlongitude;

                                Location.distanceBetween(currentlan, currentlon, latialert, longalert, distance);
                                if (distance[0] >= radiusInMeters) {
                                    System.out.println("ping jauh dri location" + distance);
                                    Log.e(TAG, "error!!!");
                                }
                                else{
                                    createmaerkerpingnearby(newLat,newLon,current_user);
                                }
                            }
                        }else{
                            System.out.println("friendping public");
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        userinfo.child(current_user).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        String friendid = dataSnapshot1.getKey();
                        String status = dataSnapshot1.getValue().toString();
                        if(status.equals("friend")){
                            userinfo.child(friendid).child("userPing").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        System.out.println("ping available");
                                        DataSnapshot upavailable = dataSnapshot.child("UP_Available");
                                        for (DataSnapshot dataSnapshot1 : upavailable.getChildren()) {
                                            String pavailable = dataSnapshot1.getKey();
                                            String type = dataSnapshot1.getValue().toString();
                                            DataSnapshot uplist = dataSnapshot.child("UP_List").child(pavailable).child("UP_Location");
                                            System.out.println("friend available id:" + pavailable);
                                            if(type.equals("friend")){
                                                if(uplist.exists()){
                                                    String newLat = uplist.child("UPL_latitude").getValue().toString();
                                                    String newLon = uplist.child("UPL_longitude").getValue().toString();
                                                    double latialert = Double.parseDouble(newLat);
                                                    double longalert = Double.parseDouble(newLon);
                                                    double radiusInMeters = 500.0;
                                                    float[] distance = new float[2];

                                                    currentlan = MapsFragment.getInstance().lastlatitude;
                                                    currentlon = MapsFragment.getInstance().lastlongitude;

                                                    Location.distanceBetween(currentlan, currentlon, latialert, longalert, distance);
                                                    if (distance[0] >= radiusInMeters) {
                                                        System.out.println("ping jauh dri location" + distance);
                                                        Log.e(TAG, "error!!!");
                                                    }else{
                                                        System.out.println("ping dekat dgn location" + distance);
                                                        createmaerkerpingnearby(newLat,newLon,friendid);
                                                    }
                                                }
                                            }else{ System.out.println("friendping public"); }
                                        }
                                    }else{ System.out.println("friendping not available"); }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                            });
                        }
                    }
                }
                else{ System.out.println("friendping data not exist"); }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }
    public void c2(){
        alertClassList = new ArrayList<>();
        publiccitizen.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("data dont exist");
                }
                else{
                    for (DataSnapshot d1:dataSnapshot.getChildren()){
                        final UCA_Details uca_details = new UCA_Details();
                        String pcakey = d1.getKey();
                        String pcakey_owner = d1.child("PC_ownerID").getValue().toString();
                        System.out.println("pckey c2"+pcakey);
                        userinfo.child(pcakey_owner).child("userCitizenAlert").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                DataSnapshot ucaavail = dataSnapshot.child("UCA_Available");
                                if(ucaavail.exists()){
                                    DataSnapshot ucaavailss =ucaavail.child(pcakey);
                                    if(ucaavailss.exists()){
                                        System.out.println("public citizen alert list avail c2"+pcakey);
                                        if(!pcakey_owner.equals(current_user)){
                                            DataSnapshot ucalocation = dataSnapshot.child("UCA_List").child(pcakey).child("UCA_Location");
                                            DataSnapshot ucadetails = dataSnapshot.child("UCA_List").child(pcakey).child("UCA_Details");

                                            UCA_Details uca_detailsdbClass = ucadetails.getValue(UCA_Details.class);
                                            UCA_Details uca_detailsdbClass1 = ucalocation.getValue(UCA_Details.class);
                                            System.out.println("public citizen alert 1:"+pcakey);

                                            double newLat1 = uca_detailsdbClass1.getUCAL_latitude();
                                            double newLon1 = uca_detailsdbClass1.getUCAL_longitude();

                                            String address = uca_detailsdbClass.getUCAD_address();
                                            String alertID = uca_detailsdbClass.getUCAD_alertID();
                                            String alertType = uca_detailsdbClass.getUCAD_alertType();
                                            long dateEnd = uca_detailsdbClass.getUCAD_dateEnd();
                                            long dateStart = uca_detailsdbClass.getUCAD_dateStart();
                                            String descripAlert = uca_detailsdbClass.getUCAD_description();
                                            String mediaType = uca_detailsdbClass.getUCAD_mediaType();
                                            String mediaURL = uca_detailsdbClass.getUCAD_mediaURL();
                                            String ownerID = uca_detailsdbClass.getUCAD_ownerID();
                                            String type = uca_detailsdbClass.getUCAD_type();

                                            uca_details.setUCAD_address(address);
                                            uca_details.setUCAD_alertID(alertID);
                                            uca_details.setUCAD_alertType(alertType);
                                            uca_details.setUCAD_dateEnd(dateEnd);
                                            uca_details.setUCAD_dateStart(dateStart);
                                            uca_details.setUCAD_description(descripAlert);
                                            uca_details.setUCAD_mediaType(mediaType);
                                            uca_details.setUCAD_mediaURL(mediaURL);
                                            uca_details.setUCAD_ownerID(ownerID);
                                            uca_details.setUCAD_type(type);
                                            uca_details.setUCAL_latitude(newLat1);
                                            uca_details.setUCAL_longitude(newLon1);
                                            alertClassList.add(uca_details);

                                            String newLat = ucalocation.child("UCAL_latitude").getValue().toString();
                                            String newLon = ucalocation.child("UCAL_longitude").getValue().toString();
                                            double latialert = Double.parseDouble(newLat);
                                            double longalert = Double.parseDouble(newLon);
                                            double radiusInMeters = 500.0;
                                            float[] distance = new float[2];

                                            currentlan = MapsFragment.getInstance().lastlatitude;
                                            currentlon = MapsFragment.getInstance().lastlongitude;

                                            Location.distanceBetween(currentlan, currentlon, latialert, longalert, distance);
                                            if (distance[0] >= radiusInMeters) {
                                                System.out.println("jauh dri location" + distance);
                                                Log.e(TAG, "error!!!");
//                                                            Toast.makeText(getContext(), "make sure you are within the radius", Toast.LENGTH_SHORT).show();
                                            }else{
                                                if(ucadetails.exists()){
//                                                        String alertType = ucadetails.child("UCAD_alertType").getValue().toString();
                                                    System.out.println("alerttype public dekat: "+alertID);
                                                    final LatLng location = new LatLng(Double.parseDouble(newLat), Double.parseDouble(newLon));

                                                    if (alertType.equals("accident")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.accident0),pcakey,location);
                                                    }
                                                    else if (alertType.equals("landslide")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.landslide0),pcakey,location);
                                                    }
                                                    else if (alertType.equals("rain")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.rain0),pcakey,location);
                                                    }
                                                    else if (alertType.equals("trafficJam")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.trafficjam0),pcakey,location);
                                                    }
                                                    else if (alertType.equals("police")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.police0),pcakey,location);
                                                    }
                                                    else if (alertType.equals("closure")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.closure0),pcakey,location);
                                                    }
                                                    else if (alertType.equals("hazard")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.hazard0),pcakey,location);
                                                    }
                                                    else if (alertType.equals("trafficLight")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.trafficlight0),pcakey,location);
                                                    }else if (alertType.equals("hot")) {
                                                        createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.hot0),pcakey,location);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        System.out.println("not exist");
                                    }
                                }
                                else{}
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        userinfo.child(current_user).child("userCitizenAlert").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataSnapshot ucavailable = dataSnapshot.child("UCA_Available");
                if(!ucavailable.exists()){
                    System.out.println("no user alert nearby");
                }
                else{
                    System.out.println("user alert nearby");
                    final UCA_Details uca_details = new UCA_Details();
                    for (DataSnapshot dataSnapshot1 : ucavailable.getChildren()) {
                        String pavailable = dataSnapshot1.getKey();
                        String type = dataSnapshot1.getValue().toString();
                        System.out.println("user alert nearby:"+pavailable);
                        if (type.equals("friend") || type.equals("public")){
                            DataSnapshot ucadetails = dataSnapshot.child("UCA_List").child(pavailable).child("UCA_Details");
                            DataSnapshot ucalocation = dataSnapshot.child("UCA_List").child(pavailable).child("UCA_Location");
                            if (ucadetails.exists()) {
//                            String type = ucadetails.child("UCAD_type").getValue().toString();
//                            if (type.equals("friend")) {
                                UCA_Details uca_detailsdbClass = ucadetails.getValue(UCA_Details.class);
                                UCA_Details uca_detailsdbClass1 = ucalocation.getValue(UCA_Details.class);
                                System.out.println("public citizen alert 1:"+pavailable);

                                double newLat1 = uca_detailsdbClass1.getUCAL_latitude();
                                double newLon1 = uca_detailsdbClass1.getUCAL_longitude();

                                String address = uca_detailsdbClass.getUCAD_address();
                                String alertID = uca_detailsdbClass.getUCAD_alertID();
                                String alertType = uca_detailsdbClass.getUCAD_alertType();
                                long dateEnd = uca_detailsdbClass.getUCAD_dateEnd();
                                long dateStart = uca_detailsdbClass.getUCAD_dateStart();
                                String descripAlert = uca_detailsdbClass.getUCAD_description();
                                String mediaType = uca_detailsdbClass.getUCAD_mediaType();
                                String mediaURL = uca_detailsdbClass.getUCAD_mediaURL();
                                String ownerID = uca_detailsdbClass.getUCAD_ownerID();
                                String type1 = uca_detailsdbClass.getUCAD_type();

                                uca_details.setUCAD_address(address);
                                uca_details.setUCAD_alertID(alertID);
                                uca_details.setUCAD_alertType(alertType);
                                uca_details.setUCAD_dateEnd(dateEnd);
                                uca_details.setUCAD_dateStart(dateStart);
                                uca_details.setUCAD_description(descripAlert);
                                uca_details.setUCAD_mediaType(mediaType);
                                uca_details.setUCAD_mediaURL(mediaURL);
                                uca_details.setUCAD_ownerID(ownerID);
                                uca_details.setUCAD_type(type1);
                                uca_details.setUCAL_latitude(newLat1);
                                uca_details.setUCAL_longitude(newLon1);
                                if (ucalocation.exists()) {
                                    String newLat = ucalocation.child("UCAL_latitude").getValue().toString();
                                    String newLon = ucalocation.child("UCAL_longitude").getValue().toString();
                                    double latialert = Double.parseDouble(newLat);
                                    double longalert = Double.parseDouble(newLon);
                                    double radiusInMeters = 500.0;
                                    float[] distance = new float[2];

                                    currentlan = MapsFragment.getInstance().lastlatitude;
                                    currentlon = MapsFragment.getInstance().lastlongitude;
                                    Location.distanceBetween(currentlan, currentlon, latialert, longalert, distance);
                                    if (distance[0] >= radiusInMeters) {
                                        System.out.println("jauh dri location" + distance);
                                        Log.e(TAG, "error!!!");
//                                                        Toast.makeText(getContext(), "make sure you are within the radius", Toast.LENGTH_SHORT).show();
                                    } else {
//                                        String alertType = ucadetails.child("UCAD_alertType").getValue().toString();
                                        final LatLng location = new LatLng(Double.parseDouble(newLat), Double.parseDouble(newLon));
                                        if (alertType.equals("accident")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.accident0), pavailable, location);
                                        } else if (alertType.equals("landslide")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.landslide0), pavailable, location);
                                        } else if (alertType.equals("rain")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.rain0), pavailable, location);
                                        } else if (alertType.equals("trafficJam")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.trafficjam0), pavailable, location);
                                        } else if (alertType.equals("police")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.police0), pavailable, location);
                                        } else if (alertType.equals("closure")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.closure0), pavailable, location);
                                        } else if (alertType.equals("hazard")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.hazard0), pavailable, location);
                                        } else if (alertType.equals("trafficLight")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.trafficlight0), pavailable, location);
                                        }else if (alertType.equals("hot")) {
                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.hot0),pavailable,location);
                                        }
                                    }
                                }
//                            }
                            }
                        }
                    }
                    alertClassList.add(uca_details);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        userinfo.child(current_user).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    final UCA_Details uca_details = new UCA_Details();
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        String friendid = dataSnapshot1.getKey();
                        System.out.println("friendid:"+friendid);
                        String status = dataSnapshot1.getValue().toString();
                        if (status.equals("friend")) {
                            System.out.println("ada friends alert nearby");
                            userinfo.child(friendid).child("userCitizenAlert").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    DataSnapshot ucaavail = dataSnapshot.child("UCA_Available");
                                    if (ucaavail.exists()) {
                                        System.out.println("friends alert nearby");
                                        for (DataSnapshot snapshot : ucaavail.getChildren()) {
                                            String pavailable = snapshot.getKey();
                                            String type = snapshot.getValue().toString();
                                            if (type.equals("friend")) {
                                                DataSnapshot ucadetails = dataSnapshot.child("UCA_List").child(pavailable).child("UCA_Details");
                                                DataSnapshot ucalocation = dataSnapshot.child("UCA_List").child(pavailable).child("UCA_Location");
                                                if (ucadetails.exists()) {
//                                                String type = ucadetails.child("UCAD_type").getValue().toString();
//                                                if (type.equals("friend")) {
                                                    UCA_Details uca_detailsdbClass = ucadetails.getValue(UCA_Details.class);
                                                    UCA_Details uca_detailsdbClass1 = ucalocation.getValue(UCA_Details.class);
                                                    System.out.println("public citizen alert 1:"+pavailable);

                                                    double newLat1 = uca_detailsdbClass1.getUCAL_latitude();
                                                    double newLon1 = uca_detailsdbClass1.getUCAL_longitude();

                                                    String address = uca_detailsdbClass.getUCAD_address();
                                                    String alertID = uca_detailsdbClass.getUCAD_alertID();
                                                    String alertType = uca_detailsdbClass.getUCAD_alertType();
                                                    long dateEnd = uca_detailsdbClass.getUCAD_dateEnd();
                                                    long dateStart = uca_detailsdbClass.getUCAD_dateStart();
                                                    String descripAlert = uca_detailsdbClass.getUCAD_description();
                                                    String mediaType = uca_detailsdbClass.getUCAD_mediaType();
                                                    String mediaURL = uca_detailsdbClass.getUCAD_mediaURL();
                                                    String ownerID = uca_detailsdbClass.getUCAD_ownerID();
                                                    String type1 = uca_detailsdbClass.getUCAD_type();

                                                    uca_details.setUCAD_address(address);
                                                    uca_details.setUCAD_alertID(alertID);
                                                    uca_details.setUCAD_alertType(alertType);
                                                    uca_details.setUCAD_dateEnd(dateEnd);
                                                    uca_details.setUCAD_dateStart(dateStart);
                                                    uca_details.setUCAD_description(descripAlert);
                                                    uca_details.setUCAD_mediaType(mediaType);
                                                    uca_details.setUCAD_mediaURL(mediaURL);
                                                    uca_details.setUCAD_ownerID(ownerID);
                                                    uca_details.setUCAD_type(type1);
                                                    uca_details.setUCAL_latitude(newLat1);
                                                    uca_details.setUCAL_longitude(newLon1);

                                                    alertClassList.add(uca_details);

                                                    String newLat = ucalocation.child("UCAL_latitude").getValue().toString();
                                                    String newLon = ucalocation.child("UCAL_longitude").getValue().toString();
                                                    double latialert = Double.parseDouble(newLat);
                                                    double longalert = Double.parseDouble(newLon);
                                                    double radiusInMeters = 500.0;
                                                    float[] distance = new float[2];

                                                    currentlan = MapsFragment.getInstance().lastlatitude;
                                                    currentlon = MapsFragment.getInstance().lastlongitude;

                                                    Location.distanceBetween(currentlan, currentlon, latialert, longalert, distance);
                                                    if (distance[0] >= radiusInMeters) {
                                                        System.out.println("jauh dri location" + distance);
                                                        Log.e(TAG, "error!!!");
//                                                         Toast.makeText(getContext(), "make sure you are within the radius", Toast.LENGTH_SHORT).show();
                                                    } else {
//                                                        String alertType = ucadetails.child("UCAD_alertType").getValue().toString();
                                                        final LatLng location = new LatLng(Double.parseDouble(newLat), Double.parseDouble(newLon));
                                                        if (alertType.equals("accident")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.accident0), pavailable, location);
                                                        } else if (alertType.equals("landslide")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.landslide0), pavailable, location);
                                                        } else if (alertType.equals("rain")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.rain0), pavailable, location);
                                                        } else if (alertType.equals("trafficJam")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.trafficjam0), pavailable, location);
                                                        } else if (alertType.equals("police")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.police0), pavailable, location);
                                                        } else if (alertType.equals("closure")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.closure0), pavailable, location);
                                                        } else if (alertType.equals("hazard")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.hazard0), pavailable, location);
                                                        } else if (alertType.equals("trafficLight")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.trafficlight0), pavailable, location);
                                                        }else if (alertType.equals("hot")) {
                                                            createmarkeralertnearby(BitmapDescriptorFactory.fromResource(R.drawable.hot0),pavailable,location);
                                                        }
                                                    }
//                                                }
                                                }
                                            }
                                        }
                                    }else{
                                        System.out.println("no friends alert nearby");
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                            });
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void createmaerkerpingnearby(String newLat,String newLon,String userid){
        final LatLng location = new LatLng(Double.parseDouble(newLat), Double.parseDouble(newLon));
        mMarker = map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.locked_ping))
                .snippet(userid)
                .position(location));

        map.setOnMarkerClickListener(MapsFragment.this::onMarkerClick);
        animateMarker(mMarker, location, false);
        markerAlerts.put(mMarker.getId(),"ping");
    }

    public void createmarkeralertnearby(BitmapDescriptor d,String alertavail,LatLng loc){
        Marker mMarker1 = map.addMarker(new MarkerOptions()
                .icon(d)
                .snippet(alertavail)
                .position(loc));
        animateMarker(mMarker1, loc, false);
        markerAlerts.put(mMarker1.getId(),alertavail);
        map.setOnMarkerClickListener(MapsFragment.this::onMarkerClick);
    }

    public void displayuserLocation(){
        userinfo.child(current_user).child("userStatus").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("US_latitude") && dataSnapshot.hasChild("US_longitude")){
                    String lat = dataSnapshot.child("US_latitude").getValue().toString();
                    String lon = dataSnapshot.child("US_longitude").getValue().toString();

                    if(!(lat.equals("0")&&lon.equals("0"))){
                        final LatLng userloc = new LatLng(Double.parseDouble(lat),Double.parseDouble(lon));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(userloc,15));

                        //   System.out.println("Locality :"+ getlocality(Double.parseDouble(lat),Double.parseDouble(lat)));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    private void popupghostmode(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog = builder.create();

        View view = LayoutInflater.from(getContext()).inflate(R.layout.choose_ghost_layout,null);

        RelativeLayout rr = view.findViewById(R.id.rr);
        RelativeLayout rr2 = view.findViewById(R.id.rr2);
        RelativeLayout rr3 = view.findViewById(R.id.rr3);

        precise = view.findViewById(R.id.h_precise);
        frozen = view.findViewById(R.id.h_frozen);
        father = view.findViewById(R.id.h_father);

        if(normal.getVisibility()==View.VISIBLE){
            precise.setVisibility(View.VISIBLE);
        }
        else if(frozens.getVisibility()==View.VISIBLE){
            frozen.setVisibility(View.VISIBLE);
        }
        else if(fathers.getVisibility()==View.VISIBLE){
            father.setVisibility(View.VISIBLE);

        }

        rr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                precise.setVisibility(View.VISIBLE);
                frozen.setVisibility(View.INVISIBLE);
                father.setVisibility(View.INVISIBLE);
                fathers.setVisibility(View.INVISIBLE);
                normal.setVisibility(View.VISIBLE);
                ghost = "normal";
                userinfo.child(current_user).child("userStatus").child("US_ghostStatus").setValue("accurate");
                System.out.println(ghost);
            }
        });

        rr2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.clear();
                precise.setVisibility(View.INVISIBLE);
                father.setVisibility(View.INVISIBLE);
                frozen.setVisibility(View.VISIBLE);
                frozens.setVisibility(View.VISIBLE);
                normal.setVisibility(View.INVISIBLE);
                ghost = "frozen";
                System.out.println(ghost);
                userinfo.child(current_user).child("userStatus").child("US_ghostStatus").setValue("frozen");
            }
        });

        rr3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                precise.setVisibility(View.INVISIBLE);
                frozen.setVisibility(View.INVISIBLE);
                father.setVisibility(View.VISIBLE);
                fathers.setVisibility(View.VISIBLE);
                normal.setVisibility(View.INVISIBLE);
                frozens.setVisibility(View.INVISIBLE);
                ghost = "father";
                System.out.println(ghost);
                userinfo.child(current_user).child("userStatus").child("US_ghostStatus").setValue("disable");
            }
        });
        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        alertDialog.setCanceledOnTouchOutside(true);
        return;
    }

    private void popupdisablefriendselector(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog = builder.create();
        View view = LayoutInflater.from(getContext()).inflate(R.layout.ghost_warning_layout,null);

        final Button chooseghost = view.findViewById(R.id.chooseghost);
        final Button deactivate = view.findViewById(R.id.deactive);

        chooseghost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupghostmode();
                alertDialog.dismiss();
            }
        });

        deactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fathers.setVisibility(View.INVISIBLE);
                normal.setVisibility(View.VISIBLE);
                frozens.setVisibility(View.INVISIBLE);
                ghost = "normal";
                System.out.println(ghost);
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    private void setLastlatitude(double latitude){
        this.lastlatitude = latitude;
    }

    private double getLastlatitude(){
        return lastlatitude;
    }

    private void setLastlongitude(double longitude){
        this.lastlongitude = longitude;
    }

    private double getLastlongitude(){
        return lastlongitude;
    }

    private boolean isLocationEnabled(){
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean isPermissionGranted(){
        if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED){
            Log.v("mylog","permission is granted");
            return true;
        }
        else{
            Log.v("mylog","permission not granted");
            return false;
        }
    }

    public void showAlert(final int status){
        String message,title,btntext;
        if (status == 1){
            message = "your locations settings is set to off. Please enable location";
            title = "Enable location";
            btntext = "Location Settings";
        }else{
            message = "Please allow this app to access location";
            title = "Permission success";
            btntext = "Grant";
        }
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setCancelable(false);
        dialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton(btntext, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(status == 1){
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                        }
                        else
                            requestPermissions(PERMISSIONS,PERMISSION_ALL);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }

    public void checklocation(){
        userinfo.child(current_user).child("userStatus").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild("US_ghostStatus")){
                    String ghostmode = dataSnapshot.child("US_ghostStatus").getValue().toString();
                    if(ghostmode.equals("accurate")){
                        loadfriendslocation();
                        normal.setVisibility(View.VISIBLE);
                        setCurrentghostmode(normal);
                        bottomSheetSelectGhost.setGhosttype("normal");
                        ghost = "accurate";
                    }
                    else if(ghostmode.equals("frozen")){
                        loadfriendslocation();
                        frozens.setVisibility(View.VISIBLE);
                        setCurrentghostmode(frozens);
                        bottomSheetSelectGhost.setGhosttype("frozens");
                        ghost = "frozen";
                    }
                    else if(ghostmode.equals("disable")){
                        map.clear();
                        fathers.setVisibility(View.VISIBLE);
                        setCurrentghostmode(fathers);
                        bottomSheetSelectGhost.setGhosttype("fathers");
                        ghost = "disable";
                    }
                }
                else{
                    fathers.setVisibility(View.VISIBLE);
                    System.out.println("ghostmode dont exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    public void checkstatus(){
        userinfo.child(current_user).child("userStatus").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    DataSnapshot sn = dataSnapshot.child("US_ghostStatus");
                    if(sn.exists()){
                        System.out.println("datasnapshot exists");
                        updateloginstate(getbuildversion(),devicetoken);
                        checklocation();
                        System.out.println("masuk sini checkstatus snapshot exist");
                    }
                    else{
                        System.out.println("datasnapshot not exists");
                        firsttimeloginstate(getbuildversion(),devicetoken);
                        fathers.setVisibility(View.VISIBLE);
                        bottomSheetSelectGhost.setGhosttype("fathers");
                        System.out.println("masuk sini checkstatus snapshot not exist");
                    }
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void firsttimeloginstate(String androidapi,String devicetoken){
        if((getLastlatitude()!=0)&&(getLastlongitude()!=0)){
            userinfo.child(current_user).child("userStatus").child("US_longitude").setValue(getLastlongitude());
            userinfo.child(current_user).child("userStatus").child("US_latitude").setValue(getLastlatitude());
        }
        else{
            userinfo.child(current_user).child("userStatus").child("US_longitude").setValue(0);
            userinfo.child(current_user).child("userStatus").child("US_latitude").setValue(0);
        }
        userinfo.child(current_user).child("userStatus").child("US_longitude").setValue(0);
        userinfo.child(current_user).child("userStatus").child("US_latitude").setValue(0);
        userinfo.child(current_user).child("userStatus").child("US_loginDevice").setValue(androidapi);
        userinfo.child(current_user).child("userStatus").child("US_onlineStatus").setValue("online");
        userinfo.child(current_user).child("userStatus").child("US_deviceToken").setValue(devicetoken);
        userinfo.child(current_user).child("userStatus").child("US_ghostStatus").setValue("disable");
        userinfo.child(current_user).child("userStatus").child("US_timestamp").setValue(ServerValue.TIMESTAMP);
        userinfo.child(current_user).child("userStatus").child("US_battery").setValue(getBatteryLevel());
    }

    public void updateloginstate(String androidapi,String devicetoken){
        userinfo.child(current_user).child("userStatus").child("US_loginDevice").setValue(androidapi);
        userinfo.child(current_user).child("userStatus").child("US_onlineStatus").setValue("online");
        userinfo.child(current_user).child("userStatus").child("US_deviceToken").setValue(devicetoken);
        userinfo.child(current_user).child("userStatus").child("US_timestamp").setValue(ServerValue.TIMESTAMP);
//        userinfo.child(current_user).child("userStatus").child("US_battery").setValue(100);
        userinfo.child(current_user).child("userStatus").child("US_battery").setValue(getBatteryLevel());
    }

    public void updatebeforeclose(String status,int battery){
        userinfo.child(current_user).child("userStatus").child("US_onlineStatus").setValue(status);
        userinfo.child(current_user).child("userStatus").child("US_timestamp").setValue(ServerValue.TIMESTAMP);
//        userinfo.child(current_user).child("userStatus").child("US_battery").setValue(battery);
  //      userinfo.child(current_user).child("userStatus").child("US_battery").setValue(getBatteryLevel());
    }


    private void RetrieveUserInfo(){
        userinfo.child(current_user).child("userDetails").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UD_displayName")){
                    String retrievename = dataSnapshot.child("UD_displayName").getValue().toString();
                    uname.setText(retrievename);
                }
                if(dataSnapshot.hasChild("UD_avatarURL")){
                    dpicture = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    if(dpicture.startsWith("p")){
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(dpicture);
                        if(getContext()!=null){
                            GlideApp.with(getContext().getApplicationContext())
                                    .load(storageReference)
                                    .placeholder(R.drawable.loading_img)
                                    .into(dp);
                        }
                    }
                    else if(dpicture.startsWith("h")){
                        if(getContext()!=null){
                            GlideApp.with(getContext().getApplicationContext())
                                    .load(dpicture)
                                    .placeholder(R.drawable.loading_img)
                                    .into(dp);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public String getbuildversion(){
        switch (Build.VERSION.SDK_INT){
            case 21:
                buildversion = "Android API 21";
                break;

            case 22:
                buildversion = "Android API 22";
                break;

            case 23:
                buildversion = "Android API 23";
                break;

            case 24:
                buildversion = "Android API 24";
                break;

            case 25:
                buildversion = "Android API 25";
                break;

            case 26:
                buildversion = "Android API 26";
                break;

            case 27:
                buildversion = "Android API 27";
                break;

            case 28:
                buildversion = "Android API 28";
                break;
        }
        return buildversion;
    }

    public static Bitmap createcustommarker(Context context, Bitmap resource, View m){
        //sini error kalau refresh balik2, cannot draw sini sb x dapat tu context
        Bitmap bitmap = null;
        try{
            CircleImageView markerImage = (CircleImageView) m.findViewById(R.id.user_dp);
            markerImage.setImageBitmap(resource);
            TextView txt_name = (TextView)m.findViewById(R.id.name);

            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            m.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
            m.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
            m.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
            m.buildDrawingCache();
            bitmap = Bitmap.createBitmap(m.getMeasuredWidth(), m.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            m.draw(canvas);
        }catch(Exception e)
        { e.printStackTrace(); }
        return bitmap;
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap,String c) {
        Log.e("test", "getcroppedbitmap executed");
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = Color.parseColor("#ffffff00");
        final Paint paint = new Paint();
        final Paint stroke = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        stroke.setAntiAlias(true);

        paint.setFilterBitmap(true);
        stroke.setFilterBitmap(true);

        paint.setDither(true);
        stroke.setDither(true);

        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        stroke.setColor(Color.parseColor(c));
        stroke.setStyle(Paint.Style.STROKE);
        stroke.setStrokeWidth(1f);
        canvas.drawCircle((bitmap.getWidth() / 2)+4, (bitmap.getHeight() / 2)+4,bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        canvas.drawCircle((bitmap.getWidth() / 2)+4,
                (bitmap.getHeight() / 2)+10, (bitmap.getWidth() / 2)+10 - stroke.getStrokeWidth(), stroke);
       // Bitmap _bmp = Bitmap.createScaledBitmap(output, 150, 150, false);
        return output;
    }

    public void loadfriendslocation(){
        if (mAuth.getCurrentUser() == null)
        {
            System.out.println("x login");
        }
        else {
            users.child(current_user).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot s : dataSnapshot.getChildren()) {
                        final String key = s.getKey().toString();
                        final String friendstatus = s.getValue().toString();
                        if(friendstatus.equals("friend")){
                            if (!key.equals(current_user)) {
                                loadusermarker(key);
                                allfriendkey.add(key);
                            }
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
        }
    }

    public void loadusermarker(final String key){
        //map.clear();
        users.child(key).child("userStatus").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                com.app.pingchat.location location1 = dataSnapshot.getValue(com.app.pingchat.location.class);
                final LatLng location = new LatLng(location1.US_latitude, location1.US_longitude);
                final long timestamp =1576720713;
                final String ghoststatus = dataSnapshot.child("US_ghostStatus").getValue().toString();
                final String lat = dataSnapshot.child("US_latitude").getValue().toString();
                final String lon = dataSnapshot.child("US_longitude").getValue().toString();
                final Date d = new Date();
                final SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String status = dataSnapshot.child("US_onlineStatus").getValue().toString();

                if(ghoststatus.equals("accurate") || ghoststatus.equals("frozen")){
                    if(!lat.equals("0") && !lon.equals("0")){
                        userinfo.child(key).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            final String name = dataSnapshot.child("UD_displayName").getValue().toString();
                            if(getContext() == null){ return; }
                            final String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();

                            System.out.println("friend avatar "+ avatar);
                            try{
                                if(avatar.startsWith("h")){
                                    System.out.println("kalau mula h masuk sini");
                                    Glide.with(getContext())
                                        .asBitmap()
                                        .load(avatar)
                                        .into(new SimpleTarget<Bitmap>(100,100) {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                System.out.println("dlm glide" + (d.getTime() - timestamp));

                                                users.child(key).child("userStatus").child("US_onlineStatus").addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        final String onlinestatus = dataSnapshot.getValue().toString();
                                                        final View s;
                                                        if(onlinestatus.equals("online")){
                                                            s = markeronline;
                                                        }
                                                        else{
                                                            s = markeroffline;
                                                        }
                                                        mMarker = map.addMarker(new MarkerOptions()
                                                                .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(),crop_image,s)))
                                                                .title(key)
                                                                .snippet(key)
                                                                .position(location));

                                                        markerAlerts.put(mMarker.getId(),"kawan");
                                                        map.setOnMarkerClickListener(MapsFragment.this::onMarkerClick);
                                                        animateMarker(mMarker,location,false);
                                                        markersArray.put(key,mMarker);
                                                    }
                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    }
                                                });
//
                                            }
                                        });
                                    }
                                    else{
                                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                        GlideApp.with(getContext())
                                            .asBitmap()
                                            .load(storageReference)
                                            .into(new SimpleTarget<Bitmap>() {
                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    Bitmap crop_image = getCroppedBitmap(resource,"#ffffff00");
                                                    System.out.println("dlm glide"+ (d.getTime()-timestamp));

                                                    users.child(key).child("userStatus").child("US_onlineStatus").addValueEventListener(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            final String onlinestatus = dataSnapshot.getValue().toString();
                                                            final View s;
                                                            if(onlinestatus.equals("online")){
                                                                s = markeronline;
                                                            }
                                                            else{
                                                                s = markeroffline;
                                                            }
                                                            mMarker = map.addMarker(new MarkerOptions()
                                                                    .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(),crop_image,s)))
                                                                    .title(key)
                                                                    .snippet(key)
                                                                    .position(location));

                                                            markerAlerts.put(mMarker.getId(),"kawan");
                                                            map.setOnMarkerClickListener(MapsFragment.this::onMarkerClick);
                                                            animateMarker(mMarker,location,false);
                                                            markersArray.put(key,mMarker);
                                                        }
                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        }
                                                    });
                                                }
                                            });
                                    }

                                }catch (Exception e){ e.printStackTrace(); }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                }else{
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void saveUserLocation(double lat,double lon){
        if (mAuth.getCurrentUser() == null)
        {
            System.out.println("x login");
        }
        else {
            current_user = mAuth.getCurrentUser().getUid();

            userinfo.child(current_user).child("userStatus").child("US_latitude").setValue(lat);
            userinfo.child(current_user).child("userStatus").child("US_longitude").setValue(lon);
            System.out.println("location updated");
        }
    }

    public void get_all_ping(){
        //public ping
        up_detailsClassList = new ArrayList<>();
        publicping.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("public ping not exist");
                }
                else{
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        final UP_DetailsClass up_detailsClass = new UP_DetailsClass();
                        if(dataSnapshot1.hasChild("PP_ownerID")){
                            String ppownerid = dataSnapshot1.child("PP_ownerID").getValue().toString();
                            String publicpingkey = dataSnapshot1.getKey();

//                            System.out.println("ppownerid: "+ppownerid);
//                            System.out.println("publicpingkey: "+publicpingkey);

                            userinfo.child(ppownerid).child("userPing").child("UP_List").child(publicpingkey).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot3) {
                                    UP_DetailsClass up_detailsdbClass = dataSnapshot3.child("UP_Details").getValue(UP_DetailsClass.class);
                                    System.out.println("address up details "+dataSnapshot3.child("UP_Details").child("UPD_address").getValue().toString());
                                    String address = up_detailsdbClass.getUPD_address();
                                    String caption = up_detailsdbClass.getUPD_caption();
                                    long dateend = up_detailsdbClass.getUPD_dateEnd();
                                    long datestart = up_detailsdbClass.getUPD_dateStart();
                                    String mediatype = up_detailsdbClass.getUPD_mediaType();
                                    String mediaThumbnail = up_detailsdbClass.getUPD_mediaThumbnailURL();
                                    String mediaurl = up_detailsdbClass.getUPD_mediaURL();
                                    String ownerid = up_detailsdbClass.getUPD_ownerID();
                                    String pingid = up_detailsdbClass.getUPD_pingID();
                                    String pingtype = up_detailsdbClass.getUPD_type();

                                    up_detailsClass.setDatetocompare(getdate(datestart));
                                    up_detailsClass.setUPD_address(address);
                                    up_detailsClass.setUPD_caption(caption);
                                    up_detailsClass.setUPD_dateEnd(dateend);
                                    up_detailsClass.setUPD_dateStart(datestart);
                                    up_detailsClass.setUPD_mediaType(mediatype);
                                    up_detailsClass.setUPD_mediaURL(mediaurl);
                                    up_detailsClass.setUPD_ownerID(ownerid);
                                    up_detailsClass.setUPD_pingID(pingid);
                                    up_detailsClass.setUPD_type(pingtype);

                                    userinfo.child(ppownerid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if(dataSnapshot.hasChild("UD_displayName")){
                                                String retrievename = dataSnapshot.child("UD_displayName").getValue().toString();
                                                up_detailsClass.setDisplayName(retrievename);
                                            }
                                            if(dataSnapshot.hasChild("UD_avatarURL")){
                                                String dpicture = dataSnapshot.child("UD_avatarURL").getValue().toString();
                                                up_detailsClass.setSenderImage(dpicture);
                                            }
                                            up_detailsClassList.add(up_detailsClass);
                                        }
                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {}
                                    });
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                            });
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });

        //userping
        userinfo.child(current_user).child("userPing").child("UP_List").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("user ping not exist");
                }
                else{
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        final UP_DetailsClass up_detailsClass = new UP_DetailsClass();
                        UP_DetailsClass up_detailsdbClass = dataSnapshot1.child("UP_Details").getValue(UP_DetailsClass.class);
                        String address = up_detailsdbClass.getUPD_address();
                        String caption = up_detailsdbClass.getUPD_caption();
                        long dateend = up_detailsdbClass.getUPD_dateEnd();
                        long datestart = up_detailsdbClass.getUPD_dateStart();
                        String mediatype = up_detailsdbClass.getUPD_mediaType();
                        String mediaurl = up_detailsdbClass.getUPD_mediaURL();
                        String mediaThumbnail = up_detailsdbClass.getUPD_mediaThumbnailURL();
                        String ownerid = up_detailsdbClass.getUPD_ownerID();
                        String pingid = up_detailsdbClass.getUPD_pingID();
                        String pingtype = up_detailsdbClass.getUPD_type();

                        if(pingtype.equals("friend")){
                            up_detailsClass.setUPD_address(address);
                            up_detailsClass.setUPD_caption(caption);
                            up_detailsClass.setUPD_dateEnd(dateend);
                            up_detailsClass.setUPD_dateStart(datestart);
                            up_detailsClass.setUPD_mediaType(mediatype);
                            up_detailsClass.setUPD_mediaURL(mediaurl);
                            up_detailsClass.setUPD_ownerID(ownerid);
                            up_detailsClass.setUPD_pingID(pingid);
                            up_detailsClass.setUPD_type(pingtype);
                            up_detailsClass.setDatetocompare(getdate(datestart));

                            userinfo.child(current_user).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.hasChild("UD_displayName")){
                                        String retrievename = dataSnapshot.child("UD_displayName").getValue().toString();
                                        up_detailsClass.setDisplayName(retrievename);
                                    }
                                    if(dataSnapshot.hasChild("UD_avatarURL")){
                                        String dpicture = dataSnapshot.child("UD_avatarURL").getValue().toString();
                                        up_detailsClass.setSenderImage(dpicture);
                                    }
                                    up_detailsClassList.add(up_detailsClass);
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                            });
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        //friendping
        userinfo.child(current_user).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        String friendid = dataSnapshot1.getKey();
                        String status = dataSnapshot1.getValue().toString();
                        if(status.equals("friend")){
                            userinfo.child(friendid).child("userPing").child("UP_List").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                                        final UP_DetailsClass up_detailsClass = new UP_DetailsClass();
                                        UP_DetailsClass up_detailsdbClass = dataSnapshot1.child("UP_Details").getValue(UP_DetailsClass.class);
                                        String address = up_detailsdbClass.getUPD_address();
                                        String caption = up_detailsdbClass.getUPD_caption();
                                        long dateend = up_detailsdbClass.getUPD_dateEnd();
                                        long datestart = up_detailsdbClass.getUPD_dateStart();
                                        String mediatype = up_detailsdbClass.getUPD_mediaType();
                                        String mediaurl = up_detailsdbClass.getUPD_mediaURL();
                                        String mediaThumbnail = up_detailsdbClass.getUPD_mediaThumbnailURL();
                                        String ownerid = up_detailsdbClass.getUPD_ownerID();
                                        String pingid = up_detailsdbClass.getUPD_pingID();
                                        String pingtype = up_detailsdbClass.getUPD_type();

                                        if(pingtype.equals("friend")){
                                            up_detailsClass.setUPD_address(address);
                                            up_detailsClass.setUPD_caption(caption);
                                            up_detailsClass.setUPD_dateEnd(dateend);
                                            up_detailsClass.setUPD_dateStart(datestart);
                                            up_detailsClass.setUPD_mediaType(mediatype);
                                            up_detailsClass.setUPD_mediaURL(mediaurl);
                                            up_detailsClass.setUPD_ownerID(ownerid);
                                            up_detailsClass.setUPD_pingID(pingid);
                                            up_detailsClass.setUPD_type(pingtype);
                                            up_detailsClass.setDatetocompare(getdate(datestart));

                                            userinfo.child(friendid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    if(dataSnapshot.hasChild("UD_displayName")){
                                                        String retrievename = dataSnapshot.child("UD_displayName").getValue().toString();
                                                        up_detailsClass.setDisplayName(retrievename);
                                                    }
                                                    if(dataSnapshot.hasChild("UD_avatarURL")){
                                                        String dpicture = dataSnapshot.child("UD_avatarURL").getValue().toString();
                                                        up_detailsClass.setSenderImage(dpicture);
                                                    }
                                                    up_detailsClassList.add(up_detailsClass);
                                                }
                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                                            });
                                        }
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                            });
                        }
                    }
                }
                else{ System.out.println("friendping data not exist"); }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void display_alert_bottomsheet(){
        ArrayList<UCA_Details> uca_details2 = new ArrayList<>();
        final Date d = new Date();
        for(int i = 0; i< alertClassList.size(); i++){
            System.out.println("jumlah alert yg ada:"+alertClassList.size());
            Long durasiping = alertClassList.get(i).getUCAD_dateEnd()- alertClassList.get(i).getUCAD_dateStart();
            Long berapalamalagi = d.getTime()- alertClassList.get(i).getUCAD_dateStart();

            System.out.println("jumlah alert "+alertClassList.get(i).getUCAD_alertID());
            System.out.println("berapa lama lagi "+berapalamalagi);
            if(berapalamalagi>durasiping){
                if(alertClassList.size() == 0) {

                }else{
                    Log.e("alert ","alert tamat tempoh "+ alertClassList.get(i).getUCAD_alertID());
                    move_avail_alert_to_expired(alertClassList.get(i).getUCAD_ownerID(),alertClassList.get(i).getUCAD_alertID());
                    uca_details2.add(alertClassList.get(i));
                }
            }
        }
        alertClassList.removeAll(uca_details2);
        if(alertClassList.size()!=0){
            System.out.println("jumlah alert lepas remove expired:"+alertClassList.size());
            alertAdapter = new AlertAdapter(getContext(), alertClassList,current_user,alertrecycleradapterlist2);
//            Collections.sort(alertClassList,UP_DetailsClass.uds_detailsClassComparator);
            alertnearby.setAdapter(alertAdapter);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        else{
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            Toast.makeText(getContext(), "No alert at the moment", Toast.LENGTH_SHORT).show();
        }
    }

    public void display_ping_bottomsheet(){
        ArrayList<UP_DetailsClass> up_details2 = new ArrayList<>();
        final Date d = new Date();

        for(int i = 0; i< up_detailsClassList.size(); i++){
            Long durasiping = up_detailsClassList.get(i).getUPD_dateEnd()- up_detailsClassList.get(i).getUPD_dateStart();
            Long berapalamalagi = d.getTime()- up_detailsClassList.get(i).getUPD_dateStart();

            System.out.println("jumlah ping "+up_detailsClassList.get(i).getUPD_pingID());
            System.out.println("berapa lama lagi "+berapalamalagi);
            if(berapalamalagi>durasiping){
                Log.e("ping ","ping tamat tempoh "+ up_detailsClassList.get(i).getUPD_pingID());
                move_avail_ping_to_expired(up_detailsClassList.get(i).getUPD_ownerID(),up_detailsClassList.get(i).getUPD_pingID());
                up_details2.add(up_detailsClassList.get(i));
            }
        }
        up_detailsClassList.removeAll(up_details2);
        if(up_detailsClassList.size()!=0){
            pingAdapter = new PingAdapter2(getContext(), up_detailsClassList,current_user,pingrecycleradapterlist2);
            Collections.sort(up_detailsClassList,UP_DetailsClass.uds_detailsClassComparator);
            pingselector.setAdapter(pingAdapter);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        else{
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            Toast.makeText(getContext(), "No Ping at the moment", Toast.LENGTH_SHORT).show();
        }
    }

    public void move_avail_alert_to_expired(String owner,String alertid){
        if(alertClassList.size() ==0){
            System.out.println("arraylist kosong");
        }else{
            userinfo.child(owner).child("userCitizenAlert").child("UCA_Available").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        String key = dataSnapshot1.getKey();
                        String type = dataSnapshot1.getValue().toString();
                        if(alertid.equals(key)){
                            userinfo.child(owner).child("userCitizenAlert").child("UCA_Available").child(key).removeValue();
                            userinfo.child(owner).child("userCitizenAlert").child("UCA_Expired").child(key).setValue(type);
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
        }
    }


    public void move_avail_ping_to_expired(String owner,String pingid){
        userinfo.child(owner).child("userPing").child("UP_Available").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String key = dataSnapshot1.getKey();
                    String type = dataSnapshot1.getValue().toString();
                    if(pingid.equals(key)){
                        userinfo.child(owner).child("userPing").child("UP_Available").child(key).removeValue();
                        userinfo.child(owner).child("userPing").child("UP_Expired").child(key).setValue(type);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    @Override
    public void onButtonClicked(String text) {
        switch (text){
            case "precise":
                fathers.setVisibility(View.INVISIBLE);
                normal.setVisibility(View.VISIBLE);
                ghost = "normal";
                userinfo.child(current_user).child("userStatus").child("US_ghostStatus").setValue("accurate");
                System.out.println(ghost);
                break;
            case "frozen":
                frozens.setVisibility(View.VISIBLE);
                normal.setVisibility(View.INVISIBLE);
                ghost = "frozen";
                System.out.println(ghost);
                userinfo.child(current_user).child("userStatus").child("US_ghostStatus").setValue("frozen");
                break;
            case "father":
                fathers.setVisibility(View.VISIBLE);
                normal.setVisibility(View.INVISIBLE);
                frozens.setVisibility(View.INVISIBLE);
                ghost = "father";
                System.out.println(ghost);
                userinfo.child(current_user).child("userStatus").child("US_ghostStatus").setValue("disable");
                break;
        }
    }

    @Override
    public void onButtonMapClicked(String text) {
        switch (text){
            case "search_location":
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN,fieldSelector.getAllFields()).build(getContext());
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                break;
            case "send_news":
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bottomSheetBehavior = BottomSheetBehavior.from(bottomsheetalert);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                display_alert_bottomsheet();
                break;
            case "send_ping":
                gotoping();
                break;
            case "friend_selector":
                display_friend_bottomsheet();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bottomSheetBehavior = BottomSheetBehavior.from(bottomsheet);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                //Toast.makeText(getContext(),"In development mode", Toast.LENGTH_SHORT).show();
                break;
            case "ping_selector":
                pingpopup.setVisibility(View.VISIBLE);
                System.out.println("pinglist "+ up_detailsClassList);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bottomSheetBehavior = BottomSheetBehavior.from(bottomsheetping);
                display_ping_bottomsheet();
                break;
            case "partner_selector":
                displaypartnerbottomsheet();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bottomSheetBehavior = BottomSheetBehavior.from(bottomsheetpartner);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
        }
    }

    private LatLngBounds bound = new LatLngBounds(
            new LatLng(6.035005859643654, 116.1281509994737), new LatLng(6.0845533, 116.1658231));
// Constrain the camera target to the Adelaide bounds.


    public void displayAdsOnMap(){
//        map.setLatLngBoundsForCameraTarget(bound);
//        map.clear();
        gpx_eureka.child("EurekaDrinks").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String lat = dataSnapshot.child("lat").getValue().toString();
                    String lon = dataSnapshot.child("lon").getValue().toString();
                    double latialert = Double.parseDouble(lat);
                    double longalert = Double.parseDouble(lon);
                    final LatLng location = new LatLng(latialert, longalert);
                  //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 25));
                    map.clear();
                    mMarker = map.addMarker(new MarkerOptions()
                            .snippet("eureka")
                            .position(location)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.tin1x)));
                    animateMarker(mMarker, location, false);
                    markerAlerts.put(mMarker.getId(),"ads");
                    map.setOnMarkerClickListener(MapsFragment.this::onMarkerClick);

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        gpx_eureka.child("MamaPOU").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if(dataSnapshot.exists()){
//                    String lat = dataSnapshot.child("lat").getValue().toString();
//                    String lon = dataSnapshot.child("lon").getValue().toString();
//                    double latialert = Double.parseDouble(lat);
//                    double longalert = Double.parseDouble(lon);
//                    final LatLng location = new LatLng(latialert, longalert);
////                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
//                    mMarker = map.addMarker(new MarkerOptions()
//                            .snippet("pau")
//                            .position(location)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.mama_pau)));
//                    animateMarker(mMarker, location, false);
//                    markerAlerts.put(mMarker.getId(),"ads");
//                    map.setOnMarkerClickListener(MapsFragment.this::onMarkerClick);
//
//                }
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
    }

    public void display_friend_bottomsheet(){
        UD_DetailsClassList= new ArrayList<>();
        users.child(current_user).child("userFriends").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot friendlist:dataSnapshot.getChildren()){
                    final userDetailsClass userDetailsClass = new userDetailsClass();
                    String status = friendlist.getValue().toString();
                    String friendid = friendlist.getKey();
                    if(status.equals("friend")){
                        users.child(friendid).child("userDetails").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                                String userid = dataSnapshot.child("UD_userID").getValue().toString();

                                userDetailsClass.setUD_displayName(name);
                                userDetailsClass.setUD_avatarURL(avatar);
                                userDetailsClass.setUD_userID(userid);

                                UD_DetailsClassList.add(userDetailsClass);
                                friendSelectorAdapter= new FriendSelectorAdapter(getContext(), UD_DetailsClassList,current_user,friendselrecycleradapterlist2);
                                friendselector.setAdapter(friendSelectorAdapter);
                                //System.out.println("name :"+name);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) { }
                        });

                    }else if(status.equals("blocked")){
                        users.child(friendid).child("userDetails").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                                String userid = dataSnapshot.child("UD_userID").getValue().toString();

                                userDetailsClass.setUD_displayName(name);
                                userDetailsClass.setUD_avatarURL(avatar);
                                userDetailsClass.setUD_userID(userid);

                                UD_DetailsClassList.add(userDetailsClass);
                                friendSelectorAdapter= new FriendSelectorAdapter(getContext(), UD_DetailsClassList,current_user,friendselrecycleradapterlist2);
                                friendselector.setAdapter(friendSelectorAdapter);
                                //System.out.println("name :"+name);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) { }
                        });

                    }else if(status.equals("block")){
                        users.child(friendid).child("userDetails").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                                String userid = dataSnapshot.child("UD_userID").getValue().toString();

                                userDetailsClass.setUD_displayName(name);
                                userDetailsClass.setUD_avatarURL(avatar);
                                userDetailsClass.setUD_userID(userid);

                                UD_DetailsClassList.add(userDetailsClass);
                                friendSelectorAdapter= new FriendSelectorAdapter(getContext(), UD_DetailsClassList,current_user,friendselrecycleradapterlist2);
                                friendselector.setAdapter(friendSelectorAdapter);
                                //System.out.println("name :"+name);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) { }
                        });
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    private void filterfriend(String text){
        ArrayList<userDetailsClass> filteredlist = new ArrayList<>();
        if(UD_DetailsClassList.size()!=0){
            for(userDetailsClass item:UD_DetailsClassList){
                if(item.getUD_displayName().toLowerCase().contains(text.toLowerCase())){
                    filteredlist.add(item);
                }
            }
            friendSelectorAdapter.filterlist(filteredlist);
            UD_DetailsClassList = filteredlist;
        }
    }

    @Override
    public void onClickImage(String text) {
    }

    public void displaypartnerbottomsheet() {
        getpartnerclassList = new ArrayList<>();
        partner.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {

                    String partnerverify = dataSnapshot1.child("P_partnerDetails").child("P_partnerVerify").getValue().toString();

                    System.out.println("how many partner:"+dataSnapshot1.getKey());
                    if (dataSnapshot1.hasChild("P_partnerDetails")) {
                        if(!partnerverify.equals("false")){
                            final partnerDetailsClass partnerDetails = new partnerDetailsClass();
                            partnerDetailsClass partnerdetails = dataSnapshot1.child("P_partnerDetails").getValue(partnerDetailsClass.class);
                            String partner_id = partnerdetails.getP_partnerID();
                            String upd_partnerid = partnerdetails.getUPD_partnerID();
                            String partner_name = partnerdetails.getP_partnerName();
                            long create_on = partnerdetails.getP_partnerCreatedOn();
                            String partner_phone = partnerdetails.getP_partnerPhone();
                            String partner_email = partnerdetails.getP_partnerEmail();
                            String partner_address = partnerdetails.getP_partnerAddress();
                            String partner_cate = partnerdetails.getP_partnerCategory();
                            String profile_url = partnerdetails.getP_profileURL();
                            String latitude = dataSnapshot1.child("P_partnerLocation").child("P_partnerLatitude").getValue().toString();
                            String longitude = dataSnapshot1.child("P_partnerLocation").child("P_partnerLongitude").getValue().toString();

                            System.out.println("url "+profile_url);

                            partnerDetails.setP_partnerID(partner_id);
                            partnerDetails.setUPD_partnerID(upd_partnerid);
                            partnerDetails.setP_partnerName(partner_name);
                            partnerDetails.setP_partnerCreatedOn(create_on);
                            partnerDetails.setP_partnerPhone(partner_phone);
                            partnerDetails.setP_partnerEmail(partner_email);
                            partnerDetails.setP_partnerAddress(partner_address);
                            partnerDetails.setP_partnerCategory(partner_cate);
                            partnerDetails.setP_profileURL(profile_url);
                            partnerDetails.setP_partnerLatitude(latitude);
                            partnerDetails.setP_partnerLongitude(longitude);

                            getpartnerclassList.add(partnerDetails);

                            partnerlistrecycleradapter = new getpartneradapter.partnerlistrecycleradapter() {
                                @Override
                                public void onUserClicked(int position) {
                                    final TextView companyname = partnerpopup.findViewById(R.id.company_name);
                                    final TextView number = partnerpopup.findViewById(R.id.phone_number);
                                    final TextView website = partnerpopup.findViewById(R.id.website);
                                    final ImageButton navigate = partnerpopup.findViewById(R.id.direct_btn);
                                    CircleImageView companylogo = partnerpopup.findViewById(R.id.company_logo);
                                    ImageButton pingar = partnerpopup.findViewById(R.id.ar_btn);
                                    RelativeLayout ownertxt = partnerpopup.findViewById(R.id.profile);

                                    Log.d(TAG, "selected business" + getpartnerclassList.get(position).getP_partnerName());
                                    double lat = Double.valueOf(getpartnerclassList.get(position).getP_partnerLatitude());
                                    double lon = Double.valueOf(getpartnerclassList.get(position).getP_partnerLongitude());

                                    LatLng latLng = new LatLng(lat, lon);
                                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                                    map.clear();
                                    final BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.office);
                                    mMarker = map.addMarker(new MarkerOptions()
                                            .icon(icon)
                                            .snippet(partner_id)
                                            .position(latLng));
                                    map.setOnMarkerClickListener(MapsFragment.this::onMarkerClick);
                                    markerAlerts.put(mMarker.getId(),"partner");

//                                    System.out.println("partnerID:"+getpartnerclassList.get(position).getP_partnerID());

                                    partner.child(getpartnerclassList.get(position).getP_partnerID()).child("P_partnerMember").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if(dataSnapshot.exists()){
                                                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                                                    String userID = dataSnapshot1.getKey();
                                                    System.out.println("userID:"+userID);
                                                    String role = dataSnapshot1.child("P_partnerRole").getValue().toString();
                                                    System.out.println("role:"+role);
                                                    if(role.equals("owner")){
                                                        ownertxt.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                Intent newIntent = new Intent(getActivity(), ViewBusinessProfileOther.class);
                                                                newIntent.putExtra("id", userID);
                                                                newIntent.putExtra("partnerID",getpartnerclassList.get(position).getP_partnerID());
                                                                newIntent.putExtra("email",getpartnerclassList.get(position).getP_partnerEmail());
                                                                startActivity(newIntent);
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                    companyname.setText(getpartnerclassList.get(position).getP_partnerName());
                                    number.setText(getpartnerclassList.get(position).getP_partnerPhone());
                                    number.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
                                            builder.setMessage("Go to Phone?")
                                                    .setCancelable(true)
                                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                                            callIntent.setData(Uri.parse("tel:"+getpartnerclassList.get(position).getP_partnerPhone()));
                                                            getPhonecallPermissions();
//                                                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                                                                getPhonecallPermissions();
//                                                                return;
//                                                            }
                                                            startActivity(callIntent);
                                                        }
                                                    })
                                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                            final androidx.appcompat.app.AlertDialog alert = builder.create();
                                            alert.show();
//                                            Intent callIntent = new Intent(Intent.ACTION_CALL);
//                                            callIntent.setData(Uri.parse(getpartnerclassList.get(position).getP_partnerPhone()));
//                                            startActivity(callIntent);
                                        }
                                    });
                                    website.setText(getpartnerclassList.get(position).getP_partnerEmail());
                                    website.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent email = new Intent(Intent.ACTION_SEND);
                                            email.putExtra(Intent.EXTRA_EMAIL, new String[]{ getpartnerclassList.get(position).getP_partnerEmail()});

                                            //need this to prompts email client only
                                            email.setType("message/rfc822");
                                            startActivity(Intent.createChooser(email, "Choose an Email client :"));
                                        }
                                    });
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(getpartnerclassList.get(position).getP_profileURL());
                                    GlideApp.with(getContext())
                                            .load(storageReference)
                                            .into(companylogo);

                                    navigate.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
                                            builder.setMessage("Open Google Maps?")
                                                    .setCancelable(true)
                                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Uri intentURi = Uri.parse("google.navigation:q="+lat+","+lon);
                                                            Intent mapIntent = new Intent(Intent.ACTION_VIEW,intentURi);
                                                            mapIntent.setPackage("com.google.android.apps.maps");
                                                            try{
                                                                if(mapIntent.resolveActivity(getContext().getPackageManager()) != null){
                                                                    startActivity(mapIntent);
                                                                }
                                                            }catch (NullPointerException e){
                                                                Log.e(TAG,"onClick:NullPointerException: Could't open map." + e.getMessage());
                                                                Toast.makeText(getContext(),"Couldn't open map", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    })
                                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                            final androidx.appcompat.app.AlertDialog alert = builder.create();
                                            alert.show();

                                        }
                                    });

//                                pingar.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        Intent newIntent = new Intent(getActivity(), ArActivityPartners.class);
//                                        newIntent.putExtra("model", getStringmodel(latLng));
//                                        newIntent.putExtra("latitude", getpartnerclassList.get(position).getP_partnerLatitude());
//                                        newIntent.putExtra("longitude", getpartnerclassList.get(position).getP_partnerLongitude());
//                                        startActivity(newIntent);
//                                    }
//                                });

                                    layout.removeView(partnerpopup);
                                    layout.addView(partnerpopup);
                                    Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.scale_up);
                                    partnerpopup.startAnimation(a);
                                }
                            };
                            if(getpartnerclassList.size()!=0){
                                adapter = new getpartneradapter(getContext(), getpartnerclassList, partnerlistrecycleradapter);
                                pingpartner.setAdapter(adapter);

                            }
                            else{
//                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                Toast.makeText(getContext(), "No Ping Partner at the moment", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        partner.child("P_partnerLocation").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    final partnerDetailsClass partnerdetails = new partnerDetailsClass();
                    partnerDetailsClass partnerDetails = dataSnapshot1.child("P_partnerLocation").getValue(partnerDetailsClass.class);
                    String part_lat = partnerDetails.getP_partnerLatitude();
                    String part_long = partnerDetails.getP_partnerLongitude();
                    if (dataSnapshot.exists()) {
                        partnerdetails.setP_partnerLatitude(part_lat);
                        partnerdetails.setP_partnerLongitude(part_long);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    private void showpopupPartner(String partnerID) {

    }

//    public void displaypartnerbottomsheet(){
//        getpartnerclassList = new ArrayList<>();
//        String url = "https://api.pingchat.app/partner/getPartner/";
//
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
//            new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    try {
//                        JSONArray jsonArray = response.getJSONArray("merchant");
//
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject jsonObject = jsonArray.getJSONObject(i);
//                            //JSONObject jsonObject = response.getJSONObject(i);
//
//                            getpartnerclass getpartnerclass = new getpartnerclass();
//                            getpartnerclass.setBusinessPartnerID(jsonObject.getString("businessPartnerID"));
//                            getpartnerclass.setBusinessCreator(jsonObject.getString("businessCreator"));
//                            getpartnerclass.setBusinessName(jsonObject.getString("businessName"));
//                            getpartnerclass.setBusinessRegister(jsonObject.getString("businessRegister"));
//                            getpartnerclass.setBusinessLastUpdate(jsonObject.getString("businessLastUpdate"));
//                            getpartnerclass.setBusinessPhone(jsonObject.getString("businessPhone"));
//                            getpartnerclass.setBusinessEmail(jsonObject.getString("businessEmail"));
//                            getpartnerclass.setBusinessLatitude(jsonObject.getString("businessLatitude"));
//                            getpartnerclass.setBusinessLongitude(jsonObject.getString("businessLongitude"));
//                            getpartnerclass.setBusinessActualAddress(jsonObject.getString("businessActualAddress"));
//                            getpartnerclass.setBusinessOnMapAddress(jsonObject.getString("businessOnMapAddress"));
//                            getpartnerclass.setBusinessCategory(jsonObject.getString("businessCategory"));
//                            getpartnerclass.setBusinessProfileURL(jsonObject.getString("businessProfileURL"));
//                            //getpartnerclassList=new ArrayList<>();
//                            getpartnerclassList.add(getpartnerclass);
//                            //getpartnerclassList = new ArrayList<>();
//                            partnerlistrecycleradapter = new getpartneradapter.partnerlistrecycleradapter() {
//                                @Override
//                                public void onUserClicked(int position) {
//                                    TextView companyname = partnerpopup.findViewById(R.id.company_name);
//                                    TextView number = partnerpopup.findViewById(R.id.phone_number);
//                                    TextView website = partnerpopup.findViewById(R.id.website);
//                                    CircleImageView companylogo = partnerpopup.findViewById(R.id.company_logo);
//                                    ImageButton pingar = partnerpopup.findViewById(R.id.ar_btn);
//
//                                    Log.d(TAG,"selected business"+getpartnerclassList.get(position).getBusinessName());
//                                    double lat = Double.valueOf(getpartnerclassList.get(position).getBusinessLatitude());
//                                    double lon = Double.valueOf(getpartnerclassList.get(position).getBusinessLongitude());
//
//                                    LatLng latLng = new LatLng(lat,lon);
//                                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//                                    map.clear();
//                                    final BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.office);
//                                    map.addMarker(new MarkerOptions()
//                                            .icon(icon)
//                                            .position(latLng));
//
//                                    companyname.setText(getpartnerclassList.get(position).getBusinessName());
//                                    number.setText(getpartnerclassList.get(position).getBusinessPhone());
//                                    website.setText(getpartnerclassList.get(position).getBusinessEmail());
//                                    GlideApp.with(getContext().getApplicationContext())
//                                            .load(getpartnerclassList.get(position).getBusinessProfileURL())
//                                            .into(companylogo);
//
//                                    pingar.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            Intent newIntent = new Intent(getActivity(), ArActivityPartners.class);
//                                            newIntent.putExtra("model",getStringmodel(latLng));
//                                            newIntent.putExtra("latitude",getpartnerclassList.get(position).getBusinessLatitude());
//                                            newIntent.putExtra("longitude",getpartnerclassList.get(position).getBusinessLongitude());
//                                            startActivity(newIntent);
//                                        }
//                                    });
//
//                                    layout.removeView(partnerpopup);
//                                    layout.addView(partnerpopup);
//                                    Animation a = AnimationUtils.loadAnimation(getContext(),R.anim.scale_up);
//                                    partnerpopup.startAnimation(a);
//                                }
//                            };
//                            adapter = new getpartneradapter(getContext(),getpartnerclassList,partnerlistrecycleradapter);
//                            pingpartner.setAdapter(adapter);
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    adapter.notifyDataSetChanged();
//                }
//            }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        });
//        mQueue.add(request);
//    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                icon = BitmapDescriptorFactory.fromResource(R.drawable.mapp);
                Place place;
                place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getLatLng());
                map.addMarker(new MarkerOptions()
                        .icon(icon)
                        .position(place.getLatLng())
                        .title(place.getAddress().toString()));
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(getActivity().getIntent());
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                        Intent i = new Intent(getActivity(), PingSetup.class).setData(contentURI);
                        setAvatar(contentURI);
                        i.putExtra("imagepath", contentURI.toString());
                        i.putExtra("lat",lat);
                        i.putExtra("lon",lon);
                        startActivity(i);
                        Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "anda gagal!sila coba dan coba", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                if (uriSavedImage != null) {
                    Intent i = new Intent(getActivity(), PingSetup.class).setData(uriSavedImage);
                    setAvatar(uriSavedImage);
                    i.putExtra("imagepath", uriSavedImage.toString());
                    startActivity(i);
                    Toast.makeText(getContext(), "uriSavedImage", Toast.LENGTH_SHORT).show();
                } else {
                    System.out.println("canceled");
                }
            }
            else{
                System.out.println("resultcode not equal to RESULT_OK");
            }
        }
        else if(requestCode == REQ_CODE_VERSION_UPDATE ){
            if (resultCode != RESULT_OK) { //RESULT_OK / RESULT_CANCELED / RESULT_IN_APP_UPDATE_FAILED
                Log.d("Tag","Update flow failed! Result code"+resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
                unregisterInstallStateUpdListener();
            }
        }
    }

    private void showPictureDialog(){
        getCameraPermissions();
        getExternalWriteStorage();
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Image");
        String[] pictureDialogItems = {"Take from camera","Select from gallery"};
        pictureDialog.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { switch (which) {
                case 0:
                    takePhotoFromCamera();
                    break;
                case 1:
                    choosePhotoFromGallery();
                    break;
            }
            }
        });
        pictureDialog.show();
    }

    public void choosePhotoFromGallery(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,3);
    }

    private void takePhotoFromCamera(){
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        String static_file_name = Environment.getExternalStorageDirectory()+"/PingChat/myPingCapture/image_capture.jpg";
        File file = new File(static_file_name);

        if(file.exists()){
            file.delete();
        }
        file.getParentFile().mkdir();
        uriSavedImage = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uriSavedImage);
        startActivityForResult(intent, 2);
    }

    public void getPhonecallPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.CALL_PHONE)) {
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                        100);
            }
        }
    }

    public void getCameraPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.CAMERA)) {
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        100);
            }
        }
    }

    public void getContactPermission(){
        if(ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.READ_CONTACTS)) {
                    // Show our own UI to explain to the user why we need to read the contacts
                    // before actually requesting the permission and showing the default UI
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                        1);
            }
        }

    }

    public void getExternalReadStorage() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},300);
            }
        }
    }

    public void getExternalWriteStorage() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},300);
            }
        }
    }

    public void animateMarker(final Marker marker, final LatLng toPosition, final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;
        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
//                    if (hideMarker) {
//                        marker.setVisible(false);
//                    } else {
//                        marker.setVisible(true);
//                    }
                }
            }
        });
    }

    public void animateMarker(final Marker marker)
    { ValueAnimator ani = ValueAnimator.ofFloat(0, 1); //change for (0,1) if you want a fade in
        ani.setDuration(300);
        ani.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                marker.setAlpha((float) animation.getAnimatedValue());
            }
        });
        ani.start();
    }

    public void setUri(String dapat){
        this.pingImage = dapat;
    }

    public String getUri(){
        return pingImage;
    }

    public void setPingImage(final String i){
        storeRef.child(i).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String dapat = uri.toString();
                System.out.println(dapat);
                setUri(dapat);
            }
        });
        this.pingImage = i;
    }
    public String getPingImage(){
        return pingImage;
    }

    public void moreuserdetail(String tname,String uname,String email,String gambar, String last){
        topname = viewer.findViewById(R.id.top_uname);
        emel = viewer.findViewById(R.id.mail);
        name = viewer.findViewById(R.id.u_name);
        message = viewer.findViewById(R.id.mess);
        pic = viewer.findViewById(R.id.user_image);

        topname.setText("Hi, I am " + tname);
        name.setText(uname);
        emel.setText(email);
        message.setText("Last message: " + last);
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(gambar);
        GlideApp.with(getContext().getApplicationContext())
                .load(storageReference)
                .into(pic);

//        Picasso.with(getContext()).load(gambar).into(pic);

        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation.setFillAfter(true);
        topname.startAnimation(animation);

        Animation animation1 = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation1.setFillAfter(true);
        name.startAnimation(animation1);

        Animation animation2 = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation2.setFillAfter(true);
        emel.startAnimation(animation2);

        Animation animation3 = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation2.setFillAfter(true);
        message.startAnimation(animation3);

        if(name.length() > 5) {
            textsize(12);
        }
        else if(name.length() <= 5) {
            textsize(15);
        }
        else if(emel.length() < 15) {
            textsize(15);
        }
        else if(name.length() > 5 && emel.length() < 15) {
            textsize(15);
        }
    }

    public void moreuserdetail2(String tname,String uname,String email,String gambar, String last){
        topname = viewer.findViewById(R.id.top_uname);
        emel = viewer.findViewById(R.id.mail);
        name = viewer.findViewById(R.id.u_name);
        message = viewer.findViewById(R.id.mess);
        pic = viewer.findViewById(R.id.user_image);

        topname.setText("Hi, I am " + tname);
        name.setText(uname);
        emel.setText(email);
        message.setText("Last message: Photo");
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(gambar);
        GlideApp.with(getContext().getApplicationContext())
                .load(storageReference)
                .into(pic);

        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation.setFillAfter(true);
        topname.startAnimation(animation);

        Animation animation1 = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation1.setFillAfter(true);
        name.startAnimation(animation1);

        Animation animation2 = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation2.setFillAfter(true);
        emel.startAnimation(animation2);

        Animation animation3 = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation2.setFillAfter(true);
        message.startAnimation(animation3);

        if(name.length() > 5) {
            textsize(12);
        }
        else if(name.length() <= 5) {
            textsize(15);
        }
        else if(emel.length() < 15) {
            textsize(15);
        }
        else if(name.length() > 5 && emel.length() < 15) {
            textsize(15);
        }
    }

    public void userdetail() {
        topname = viewer.findViewById(R.id.top_uname);  emel = viewer.findViewById(R.id.mail);
        name = viewer.findViewById(R.id.u_name);        message = viewer.findViewById(R.id.mess);
        pic = viewer.findViewById(R.id.user_image);     dot = viewer.findViewById(R.id.dot);
        dot.setVisibility(View.VISIBLE);
        emel.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_from_left);
        animation.setFillAfter(true);
        topname.startAnimation(animation);name.startAnimation(animation);
        emel.startAnimation(animation);message.startAnimation(animation);
        dot.startAnimation(animation);
    }

    public void othermoreuserdetail(String tname,String uname,String email,String gambar){
        userdetail();
        topname.setText("Hi, I am " + tname);
        name.setText(uname);
        emel.setText(email);
        message.setText("No last message");
        //Picasso.with(getContext()).load(gambar).into(pic);
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(gambar);
        GlideApp.with(getContext().getApplicationContext())
                .load(storageReference)
                .into(pic);

        if(name.length() > 5) {
            textsize(12);
        }
        else if(name.length() <= 5) {
            textsize(15);
        }
        else if(emel.length() < 15) {
            textsize(15);
        }

        else if(name.length() > 5 && emel.length() < 15) {
            textsize(15);
        }
    }

    public void textsize(float size) {
        emel.setTextSize(TypedValue.COMPLEX_UNIT_SP,size);
    }

    public void checkdevice(){
        userinfo.child(current_user).child("metaData").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.child("loginDevice").exists()){
                    if(Build.VERSION.SDK_INT >=26){
                        userinfo.child(current_user).child("metaData").child("loginDevice").setValue("android above API 26");
                    }
                    else{
                        userinfo.child(current_user).child("metaData").child("loginDevice").setValue("android below API 26");
                    }
                }
                else{
                    System.out.println("logindevice exists");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void gotoping(){
        Intent i = new Intent(getContext(), PingSetup.class);
        i.putExtra("username", uname.getText().toString());
        i.putExtra("avatar",getDpicture());
        if(Build.VERSION.SDK_INT>20){
            ActivityOptions options =
                    ActivityOptions.makeSceneTransitionAnimation(getActivity());
            startActivity(i,options.toBundle());
        }else {
            startActivity(i);
        }
        //ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getContext(),R.anim.slide_up,R.anim.slide_down);
        //startActivity(i,activityOptions.toBundle());
    }

    public void gotonews(){
        Intent i = new Intent(getContext(), NewsSetup.class);
        i.putExtra("username", uname.getText().toString());
        i.putExtra("avatar",getDpicture());
        if(Build.VERSION.SDK_INT>20){
            ActivityOptions options =
                    ActivityOptions.makeSceneTransitionAnimation(getActivity());
            startActivity(i,options.toBundle());
        }else {
            startActivity(i);
        }
        //ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getContext(),R.anim.slide_up,R.anim.slide_down);
        //startActivity(i,activityOptions.toBundle());
    }

    public void clikss(ImageButton b){
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                popupghostmode();
                bottomSheetSelectGhost.show(getActivity().getSupportFragmentManager(),"example");
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        userinfo.child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    if(mAuth.getCurrentUser()!=null && mAuth.getCurrentUser().isEmailVerified()){
                        updatebeforeclose ("offline",70);
                    }
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    {
                        System.out.println(ghost);
                        if(ghost == "accurate"){
                            System.out.println("ghostmode off");
                            context.startService(new Intent(context, RunGPSBackground.class));
                        }
                        else{
                            System.out.println("ghostmode onn");
                            context.stopService(new Intent(context, RunGPSBackground.class));
                        }
                    }
                }
                else{

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        checkNewAppVersionState();
        if(mAuth.getCurrentUser()!=null && mAuth.getCurrentUser().isEmailVerified()){
            userinfo.child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        updatebeforeclose("online",90);
                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                        {
                            System.out.println("ghostmode onn");
                            getActivity().stopService(new Intent(getContext(), RunGPSBackground.class));

                        }

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(mAuth.getCurrentUser()!=null && mAuth.getCurrentUser().isEmailVerified()){
            userinfo.child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                       // updatebeforeclose("offline",80);
                        userinfo.child(current_user).child("userStatus").child("US_onlineStatus").setValue("offline");
                        userinfo.child(current_user).child("userStatus").child("US_timestamp").setValue(ServerValue.TIMESTAMP);
                        context.stopService(new Intent(context, RunGPSBackground.class));
                        trimCache(getContext());
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Date  currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        String dateplusone = df.format(ccc);
        datess = dateplusone;
        return datess;
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = "2 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = "3 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = "4 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = "5 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = "6 days ago";
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = "7 days ago";
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }

    public void gotoimageview(String image){
        Intent i = new Intent(getContext().getApplicationContext(), ViewImageActivity.class);
        i.putExtra("test",image);
        startActivity(i);
    }

    public static MapsFragment getInstance(){
        return instance;
    }

    public long gettimestamp(){
        Date date = new Date();
        time = date.getTime();

        System.out.println("time in miliseconds :"+time);

        Timestamp ts = new Timestamp(time);
        System.out.println("Current time stamp:" + ts);
        return time;
    }

    public String getTimeleft(String y){
        Date date = new Date();
        long time = date.getTime();
        String timetarget =y;

        Long uu =Long.parseLong(timetarget);

        org.joda.time.Period period = new org.joda.time.Period(time,uu);
        System.out.println("seconds " + period.getSeconds());
        System.out.println("minutes"+period.getMinutes());
        System.out.println("hours " + period.getHours());
        timeleft = "Expiring in\n"+period.getHours()+":"+period.getMinutes()+" m";
        return timeleft;
    }

    private void popupSnackbarForCompleteUpdateAndUnregister() {
        Snackbar snackbar =
                Snackbar.make(coordinatorLayout, getString(R.string.update_downloaded), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.restart, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appUpdateManager.completeUpdate();
            }
        });
        snackbar.setActionTextColor(getResources().getColor(R.color.white));
        snackbar.show();

        unregisterInstallStateUpdListener();
    }

    private void checkNewAppVersionState() {
        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        appUpdateInfo -> {
                            //FLEXIBLE:
                            // If the update is downloaded but not installed,
                            // notify the user to complete the update.
                            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                                popupSnackbarForCompleteUpdateAndUnregister();
                            }

                            //IMMEDIATE:
                            if (appUpdateInfo.updateAvailability()
                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                // If an in-app update is already running, resume the update.
                                startAppUpdateImmediate(appUpdateInfo);
                            }
                        });
                }

    private void unregisterInstallStateUpdListener() {
        if (appUpdateManager != null && installStateUpdatedListener != null)
            appUpdateManager.unregisterListener(installStateUpdatedListener);
    }

    public void checkForAppUpdate() {
        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(getActivity());

        // Returns an intent object that you use to check for an update.
        com.google.android.play.core.tasks.Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Create a listener to track request state updates.
        installStateUpdatedListener = new InstallStateUpdatedListener() {
            @Override
            public void onStateUpdate(InstallState installState) {
                // Show module progress, log state, or install the update.
                if (installState.installStatus() == InstallStatus.DOWNLOADED)
                    // After the update is downloaded, show a notification
                    // and request user confirmation to restart the app.
                    popupSnackbarForCompleteUpdateAndUnregister();
            }
        };

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                // Request the update.
                if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                    // Before starting an update, register a listener for updates.
                    appUpdateManager.registerListener(installStateUpdatedListener);
                    // Start an update.
                    startAppUpdateFlexible(appUpdateInfo);
                } else if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE) ) {
                    // Start an update.
                    startAppUpdateImmediate(appUpdateInfo);
                }
            }
        });
    }

    private void startAppUpdateImmediate(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    getActivity(),
                    // Include a request code to later monitor this update request.
                    REQ_CODE_VERSION_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private void startAppUpdateFlexible(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    // The current activity making the update request.
                    getActivity(),
                    // Include a request code to later monitor this update request.
                    REQ_CODE_VERSION_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
            unregisterInstallStateUpdListener();
        }
    }

    public String getStringmodel(LatLng l){
        if(l.equals(new LatLng(5.9900187086420775,116.07907366007565))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1564673132179827/e8Sn9sfcoJJ/9G4AnxE1srU/borneotours1.gltf";
        }
        else if(l.equals(new LatLng(6.7224284207018625,117.05544468015432))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1564673400048214/1GZBexyfIG0/1_n3-WFl98a/sambalbako1.gltf";
        }
        else if(l.equals(new LatLng(51.5801010910293,0.0874212384223938))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1564673546000381/8WrqYwoug2S/fiUUJq6er7T/cookware1.gltf";
        }
        else if(l.equals(new LatLng(6.084759665318471,116.16583105176686))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1564673644389762/6Wxa8VNctLv/1DxSV8mNIYN/okane1.gltf";
        }
        else if(l.equals(new LatLng(6.084487621262044,116.16566978394987))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1564673748573748/bHButxRdw95/1_0MmYIcTQJ/pingchat2.gltf";
        }
        else if(l.equals(new LatLng(5.8951591,116.0480066))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1564673990337612/7XnNci8e-4K/1XsXNNEjTGv/nurainelectrical3.gltf";
        }
        else if(l.equals(new LatLng(1.5558528,110.3574882))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1564673888013756/cHA3CijeKRW/9Uajjy0GtzL/sapana6.gltf";
        }
        else if(l.equals(new LatLng(5.9517745022156054,116.05308640748261))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1564674065844073/eQi_Mwng172/1WudwcpS2KP/sutera8.gltf";
        }
        else if(l.equals(new LatLng(0,0))){
            businessmodel = "https://poly.googleusercontent.com/downloads/c/fp/1591212678436005/fhNFcBydj7T/7zVvuQkXFf_/model.gltf";
           // businessmodel="https://devbm.ml/gltf/eureka.gltf";
        }
        return businessmodel;
    }

    public Date getdate(long timestamp){
        datesss = new Date(timestamp);
        return datesss;
    }

    public String getlocality(double lat,double lon){
        try{
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(lat,lon,1);
            userlocality = addresses.get(0).getAddressLine(0);
        }catch (IOException io){

        }
        return userlocality;
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }

            System.out.println("clearing cache android");
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(REQUEST_LOCATION_PERMISSION)
    public void requestLocationPermission() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if(EasyPermissions.hasPermissions(getContext(), perms)) {
//            Toast.makeText(getContext(), "Permission already granted", Toast.LENGTH_SHORT).show();
        }
        else {
            EasyPermissions.requestPermissions(this, "Please grant the location permission", REQUEST_LOCATION_PERMISSION, perms);
        }
    }

//    public void displayARmarker(){
//        double aquaLat =6.009294;//6.084788;//6.163540;;//6.009093, 116.110744// 6.1634506, 6.009294//
//        double aquaLon = 116.1106;//116.165711;//116.167221; 116.1656587//116.1106
//        double flyLat=6.011592;//6.084788;//6.163540;;//6.009093, 116.110744// 6.1634506
//        double flyLon = 116.1119;//116.165711;//116.167221; 116.1656587
//        currentlan = MapsFragment.getInstance().lastlatitude;
//        currentlon = MapsFragment.getInstance().lastlongitude;
//        double radiusInMeters = 100.0;
//        float[] distance = new float[2];
//        final LatLng location = new LatLng(Double.parseDouble(String.valueOf(aquaLat)), Double.parseDouble(String.valueOf(aquaLon)));
//        Location.distanceBetween( currentlan, currentlon,aquaLat,aquaLon, distance);
//        if(distance[0] >= radiusInMeters) {
//            Log.e(TAG, "far from AR");
//        }else{
//            Log.e(TAG, "display marker");
//            Marker mMarkerar = map.addMarker(new MarkerOptions()
//                    .position(location)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_ar)));
//            animateMarker(mMarkerar, location, false);
//
//            final LatLng location1 = new LatLng(Double.parseDouble(String.valueOf(flyLat)), Double.parseDouble(String.valueOf(flyLon)));
//
//            Marker mMarkerar1 = map.addMarker(new MarkerOptions()
//                    .position(location1)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_ar)));
//
//            animateMarker(mMarkerar1, location1, false);
//
//        }
//    }

//    public void checkArLocation(){
//        double newLatitude =6.009571;//6.084788;//6.163540;;//6.009093, 116.110744// 6.1634506, 6.009571
//        double newLongitude = 116.111183;//116.165711;//116.167221; 116.1656587, 116.111183
//        currentlan = MapsFragment.getInstance().lastlatitude;
//        currentlon = MapsFragment.getInstance().lastlongitude;
//        double radiusInMeters = 100.0;
//        float[] distance = new float[2];
//        Location.distanceBetween( currentlan, currentlon,newLatitude,newLongitude, distance);
//
//        if(distance[0] >= radiusInMeters) {
//            Log.e(TAG, "far from AR");
//        }else{
//            Log.e(TAG, "within range AR");
//            final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
//            final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
//            View view = LayoutInflater.from(getContext()).inflate(R.layout.ar_popup_layout, null);
//            Button arBtn = view.findViewById(R.id.go_ar);
//            Button cancelBtn = view.findViewById(R.id.cancel_ar);
//            alertDialog.setView(view);
//            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            alertDialog.show();
//            alertDialog.setCanceledOnTouchOutside(false);
//
//            arBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent unityIntent = new Intent(getContext(), UnityPlayerActivity.class);
//                    startActivity(unityIntent);
//                }
//            });
//
//            cancelBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    alertDialog.dismiss();
//                }
//            });
//        }
//    }

    public boolean onMarkerClick(Marker marker) {
        String alert_id = markerAlerts.get(marker.getId());

        if(alert_id.equals(marker.getSnippet())) {
            System.out.println("clicked"+marker.getSnippet());
            final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
            final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
            View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_item_view, null);
            TextView titleAlert = view.findViewById(R.id.title_alert);
            ImageView imagealert = view.findViewById(R.id.img_alert);
            TextView user_name = view.findViewById(R.id.user_name);
            TextView timestamp = view.findViewById(R.id.timestamp);
            TextView descripttext = view.findViewById(R.id.descrip_text);
            Button seenBtn = view.findViewById(R.id.seen_btn);
            ImageButton delBtn = view.findViewById(R.id.delete_butt);
            Button exitBtn = view.findViewById(R.id.exit_alert);
            CircleImageView imgicon = view.findViewById(R.id.pro_pic);

            SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm aa");

            alertClassList = new ArrayList<>();
            publiccitizen.child(marker.getSnippet()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()) {
                        System.out.println("no public citizen alert");
                    } else {
                        if (dataSnapshot.hasChild("PC_ownerID")) {
                            String pcownerid = dataSnapshot.child("PC_ownerID").getValue().toString();
                            if(pcownerid.equals(current_user)){
                                delBtn.setVisibility(View.VISIBLE);
                            }else {
                                delBtn.setVisibility(View.GONE);
                            }
                            userinfo.child(pcownerid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.hasChild("UD_displayName")){
                                        String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                        user_name.setText(name);
                                        System.out.println("name :"+name);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
//
                            userinfo.child(pcownerid).child("userCitizenAlert").child("UCA_List").child(alert_id).child("UCA_Details").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()){
                                        String alertTitle = dataSnapshot.child("UCAD_alertType").getValue().toString();
                                        String descript = dataSnapshot.child("UCAD_description").getValue().toString();
                                        String imageUrl = dataSnapshot.child("UCAD_mediaURL").getValue().toString();
                                        String dateStart = currentTime.format(dataSnapshot.child("UCAD_dateStart").getValue());
                                        System.out.println("post time:"+dateStart);


                                        if(alertTitle.equals("accident")){
                                            titleAlert.setText("Accident");
                                            imgicon.setImageResource(R.drawable.accident3x);
                                        }else if(alertTitle.equals("landslide")){
                                            titleAlert.setText("Landslide");
                                            imgicon.setImageResource(R.drawable.landslide3x);
                                        }else if(alertTitle.equals("rain")){
                                            titleAlert.setText("Rain");
                                            imgicon.setImageResource(R.drawable.rain3x);
                                        }
                                        else if(alertTitle.equals("hazard")){
                                            titleAlert.setText("Hazard");
                                            imgicon.setImageResource(R.drawable.hazard3x);
                                        }
                                        else if(alertTitle.equals("police")){
                                            titleAlert.setText("Police");
                                            imgicon.setImageResource(R.drawable.police3x);
                                        }
                                        else if(alertTitle.equals("trafficJam")){
                                            titleAlert.setText("TrafficJam");
                                            imgicon.setImageResource(R.drawable.trafficjam3x);
                                        }else if(alertTitle.equals("trafficLight")){
                                            titleAlert.setText("TrafficLight");
                                            imgicon.setImageResource(R.drawable.trafficlight3x);
                                        }
                                        else if(alertTitle.equals("closure")){
                                            titleAlert.setText("Closure");
                                            imgicon.setImageResource(R.drawable.closure3x);
                                        }else if (alertTitle.equals("hot")) {
                                            titleAlert.setText("Hot");
                                            imgicon.setImageResource(R.drawable.hot3x);
                                        }

                                        descripttext.setText(descript);
                                        timestamp.setText(dateStart);
                                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getContext());
                                        circularProgressDrawable.setStrokeWidth(5f);
                                        circularProgressDrawable.setCenterRadius(10f);
                                        circularProgressDrawable.start();

                                        if (!imageUrl.startsWith("h")) {
                                            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageUrl);
                                            GlideApp.with(getContext())
                                                    .load(storageReference)
                                                    .placeholder(circularProgressDrawable)
                                                    .into(imagealert);
                                        } else {
                                            GlideApp.with(getContext())
                                                    .load(imageUrl)
                                                    .placeholder(circularProgressDrawable)
                                                    .into(imagealert);
                                        }
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
            userinfo.child(current_user).child("userCitizenAlert").child("UCA_Available").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild(marker.getSnippet())) {
                        String type = dataSnapshot.child(marker.getSnippet()).getValue().toString();
                        if (type.equals("friend")) {
                            userinfo.child(current_user).child("userCitizenAlert").child("UCA_List").child(marker.getSnippet()).child("UCA_Details").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()){
                                        String alertTitle = dataSnapshot.child("UCAD_alertType").getValue().toString();
                                        String descript = dataSnapshot.child("UCAD_description").getValue().toString();
                                        String imageUrl = dataSnapshot.child("UCAD_mediaURL").getValue().toString();
                                        String dateStart = currentTime.format(dataSnapshot.child("UCAD_dateStart").getValue());

                                        if (alertTitle.equals("accident")) {
                                            titleAlert.setText("Accident");
                                            imgicon.setImageResource(R.drawable.accident3x);
                                        } else if (alertTitle.equals("landslide")) {
                                            titleAlert.setText("Landslide");
                                            imgicon.setImageResource(R.drawable.landslide3x);
                                        } else if (alertTitle.equals("rain")) {
                                            titleAlert.setText("Rain");
                                            imgicon.setImageResource(R.drawable.rain3x);
                                        } else if (alertTitle.equals("hazard")) {
                                            titleAlert.setText("Hazard");
                                            imgicon.setImageResource(R.drawable.hazard3x);
                                        } else if (alertTitle.equals("police")) {
                                            titleAlert.setText("Police");
                                            imgicon.setImageResource(R.drawable.police3x);
                                        } else if (alertTitle.equals("trafficJam")) {
                                            titleAlert.setText("TrafficJam");
                                            imgicon.setImageResource(R.drawable.trafficjam3x);
                                        } else if (alertTitle.equals("trafficLight")) {
                                            titleAlert.setText("TrafficLight");
                                            imgicon.setImageResource(R.drawable.trafficlight3x);
                                        } else if (alertTitle.equals("closure")) {
                                            titleAlert.setText("Closure");
                                            imgicon.setImageResource(R.drawable.closure3x);
                                        }else if (alertTitle.equals("hot")) {
                                            titleAlert.setText("Hot");
                                            imgicon.setImageResource(R.drawable.hot3x);
                                        }
                                        descripttext.setText(descript);
                                        timestamp.setText(dateStart);
                                        delBtn.setVisibility(View.VISIBLE);
                                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getContext());
                                        circularProgressDrawable.setStrokeWidth(5f);
                                        circularProgressDrawable.setCenterRadius(10f);
                                        circularProgressDrawable.start();

                                        if (!imageUrl.startsWith("h")) {
                                            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageUrl);
                                            GlideApp.with(getContext())
                                                    .load(storageReference)
                                                    .placeholder(circularProgressDrawable)
                                                    .into(imagealert);
                                        } else {
                                            GlideApp.with(getContext())
                                                    .load(imageUrl)
                                                    .placeholder(circularProgressDrawable)
                                                    .into(imagealert);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            userinfo.child(current_user).child("userDetails").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild("UD_displayName")) {
                        String name = dataSnapshot.child("UD_displayName").getValue().toString();
                        user_name.setText(name);
                        System.out.println("name :" + name);
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }

            });

            userinfo.child(current_user).child("userFriends").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            String friendid = dataSnapshot1.getKey();
                            String status = dataSnapshot1.getValue().toString();
                            if (status.equals("friend")) {
                                userinfo.child(friendid).child("userCitizenAlert").child("UCA_Available").addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()) {
                                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                                String favailbaleid = dataSnapshot1.getKey();
                                                if(favailbaleid.equals(marker.getSnippet())){
//                                                    Toast.makeText(getContext(), "clicked"+favailbaleid, Toast.LENGTH_SHORT).show();
                                                    userinfo.child(friendid).child("userDetails").addValueEventListener(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            if (dataSnapshot.hasChild("UD_displayName")) {
                                                                String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                                                user_name.setText(name);
                                                                System.out.println("name :" + name);
                                                            }
                                                        }
                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        }
                                                    });
                                                    userinfo.child(friendid).child("userCitizenAlert").child("UCA_List").child(favailbaleid).child("UCA_Details").addValueEventListener(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            if (dataSnapshot.exists()) {
                                                                String type = dataSnapshot.child("UCAD_type").getValue().toString();
                                                                if (type.equals("friend")) {
                                                                    userinfo.child(friendid).child("userCitizenAlert").child("UCA_List").child(favailbaleid).child("UCA_Details").addValueEventListener(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                            if(dataSnapshot.exists()){
                                                                                String alertTitle = dataSnapshot.child("UCAD_alertType").getValue().toString();
                                                                                String descript = dataSnapshot.child("UCAD_description").getValue().toString();
                                                                                String imageUrl = dataSnapshot.child("UCAD_mediaURL").getValue().toString();
                                                                                String dateStart = currentTime.format(dataSnapshot.child("UCAD_dateStart").getValue());

                                                                                if (alertTitle.equals("accident")) {
                                                                                    titleAlert.setText("Accident");
                                                                                    imgicon.setImageResource(R.drawable.accident3x);
                                                                                } else if (alertTitle.equals("landslide")) {
                                                                                    titleAlert.setText("Landslide");
                                                                                    imgicon.setImageResource(R.drawable.landslide3x);
                                                                                } else if (alertTitle.equals("rain")) {
                                                                                    titleAlert.setText("Rain");
                                                                                    imgicon.setImageResource(R.drawable.rain3x);
                                                                                } else if (alertTitle.equals("hazard")) {
                                                                                    titleAlert.setText("Hazard");
                                                                                    imgicon.setImageResource(R.drawable.hazard3x);
                                                                                } else if (alertTitle.equals("police")) {
                                                                                    titleAlert.setText("Police");
                                                                                    imgicon.setImageResource(R.drawable.police3x);
                                                                                } else if (alertTitle.equals("trafficJam")) {
                                                                                    titleAlert.setText("TrafficJam");
                                                                                    imgicon.setImageResource(R.drawable.trafficjam3x);
                                                                                } else if (alertTitle.equals("trafficLight")) {
                                                                                    titleAlert.setText("TrafficLight");
                                                                                    imgicon.setImageResource(R.drawable.trafficlight3x);
                                                                                } else if (alertTitle.equals("closure")) {
                                                                                    titleAlert.setText("Closure");
                                                                                    imgicon.setImageResource(R.drawable.closure3x);
                                                                                }else if (alertTitle.equals("hot")) {
                                                                                    titleAlert.setText("Hot");
                                                                                    imgicon.setImageResource(R.drawable.hot3x);
                                                                                }
                                                                                descripttext.setText(descript);
                                                                                timestamp.setText(dateStart);
                                                                                CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getContext());
                                                                                circularProgressDrawable.setStrokeWidth(5f);
                                                                                circularProgressDrawable.setCenterRadius(10f);
                                                                                circularProgressDrawable.start();

                                                                                if (!imageUrl.startsWith("h")) {
                                                                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(imageUrl);
                                                                                    GlideApp.with(getContext())
                                                                                            .load(storageReference)
                                                                                            .placeholder(circularProgressDrawable)
                                                                                            .into(imagealert);
                                                                                } else {
                                                                                    GlideApp.with(getContext())
                                                                                            .load(imageUrl)
                                                                                            .placeholder(circularProgressDrawable)
                                                                                            .into(imagealert);
                                                                                }
                                                                            }
                                                                        }
                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                        }
                                                                    });

                                                                }
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });
                                                }
                                            }

                                        }

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            alertDialog.setView(view);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(true);

            exitBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProgressDialog dialog= new ProgressDialog(getContext());
                    dialog.setMessage("Please wait... Delete in progress");
                    dialog.show();
                    userinfo.child(current_user).child("userCitizenAlert").child("UCA_Available").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                                String key = dataSnapshot1.getKey();
                                String type = dataSnapshot1.getValue().toString();
                                if(alert_id.equals(key)){
                                    dialog.dismiss();
                                    Toast.makeText(getContext(), "Message deleted!", Toast.LENGTH_SHORT).show();
                                    userinfo.child(current_user).child("userCitizenAlert").child("UCA_Available").child(key).removeValue();
                                    userinfo.child(current_user).child("userCitizenAlert").child("UCA_Expired").child(key).setValue(type);
                                }
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                }
            });
        }
        else if(alert_id.equals("kawan")){
            String user_id = marker.getSnippet();
            final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
            final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
            View view = LayoutInflater.from(getContext()).inflate(R.layout.friend_sel_popup, null);
            final TextView uname = view.findViewById(R.id.user_name);
            userinfo.child(user_id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        DataSnapshot userstatus = dataSnapshot.child("userStatus");
                        DataSnapshot userdetails = dataSnapshot.child("userDetails");
                        String ghoststatus = userstatus.child("US_ghostStatus").getValue().toString();
                        String on9status = userstatus.child("US_onlineStatus").getValue().toString();
                        String latitude = userstatus.child("US_latitude").getValue().toString();
                        String longitude = userstatus.child("US_longitude").getValue().toString();
                        String btry_lvl = userstatus.child("US_battery").getValue().toString();
                        int btry_int = Integer.parseInt(btry_lvl);
                        String username = userdetails.child("UD_displayName").getValue().toString();
                        String useravatar = userdetails.child("UD_avatarURL").getValue().toString();
                        uname.setText(username);
                        final ImageView ghostStat = view.findViewById(R.id.ghost_sel);
                        final TextView ghostText = view.findViewById(R.id.ghost_txt);
                        final ImageView on9txt = view.findViewById(R.id.onlineStat);
                        final TextView msg_time = view.findViewById(R.id.min);
                        final TextView distance = view.findViewById(R.id.fr_location);
                        final TextView battery = view.findViewById(R.id.battery_percent);
                        final TextView mesejlast = view.findViewById(R.id.msg_last);
                        final EditText chatmap = view.findViewById(R.id.typing_box);
                        final Button sendBtn = view.findViewById(R.id.send_btn);
                        final ImageView btry100 = view.findViewById(R.id.bateri_life_100);
                        final ImageView btry79 = view.findViewById(R.id.bateri_80);
                        final ImageView btry40 = view.findViewById(R.id.bateri_40);
                        final ImageView btry20 = view.findViewById(R.id.bateri_10);
                        if(on9status.equals("online")){
                            on9txt.setImageResource(R.drawable.on9marker);
                        } else{
                            on9txt.setImageResource(R.drawable.of9marker);
                        }
                        if(btry_int >= 80){
                            btry100.setVisibility(View.VISIBLE);
                            btry79.setVisibility(View.GONE);
                            btry40.setVisibility(View.GONE);
                            btry20.setVisibility(View.GONE);
                        }else if(btry_int < 80 && btry_int >= 50){
                            btry100.setVisibility(View.GONE);
                            btry40.setVisibility(View.GONE);
                            btry20.setVisibility(View.GONE);
                            btry79.setVisibility(View.VISIBLE);
                        }else if(btry_int < 50 && btry_int >= 30){
                            btry100.setVisibility(View.GONE);
                            btry79.setVisibility(View.GONE);
                            btry20.setVisibility(View.GONE);
                            btry40.setVisibility(View.VISIBLE);
                        }else if(btry_int < 30 ){
                            btry100.setVisibility(View.GONE);
                            btry79.setVisibility(View.GONE);
                            btry40.setVisibility(View.GONE);
                            btry20.setVisibility(View.VISIBLE);
                        }
                        chatmap.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                } else {
                                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                                }
                            }
                        });
                        sendBtn.setEnabled(false);
                        chatmap.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void afterTextChanged(Editable s) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if(chatmap.length() == 0) {
                                    sendBtn.setEnabled(false);
                                    //Toast.makeText(ChatsActivity.this, "Cannot send an empty message", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    sendBtn.setEnabled(true);

                                }
                            }
                        });
                        sendBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                userChats.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                                String chatsID = dataSnapshot1.getKey();
                                                String friendID = dataSnapshot1.child("UCP_friendID").getValue().toString();
                                                if(friendID.equals(user_id)) {
                                                    HashMap new_chat_details = new HashMap();
                                                    new_messageid = personalchat.push().push().getKey();

                                                    new_chat_details.put("CP_chatID", chatsID);
                                                    new_chat_details.put("CP_lastMessageID", new_messageid);
                                                    new_chat_details.put("CP_lastMessage", chatmap.getText().toString());
                                                    new_chat_details.put("CP_lastMessageTimestamp", timestamp.getTime());
                                                    new_chat_details.put("CP_lastMessageMediaType", "text");

                                                    HashMap personal_chat = new HashMap();
                                                    personal_chat.put("CP_message", chatmap.getText().toString());
                                                    personal_chat.put("CP_messageSenderID", current_user);
                                                    personal_chat.put("CP_mediaType", "text");
                                                    personal_chat.put("CP_messageTimestamp", timestamp.getTime());

                                                    personalchat.child(chatsID).child("CP_personalDetails").setValue(new_chat_details);
                                                    personalchat.child(chatsID).child("CP_personalChatSession").child(new_messageid).setValue(personal_chat);
                                                    users.child(current_user).child("userConversations").child("Personal").child(chatsID).child("UCP_lastMessageID").setValue(new_messageid);
                                                    users.child(friendID).child("userConversations").child("Personal").child(chatsID).child("UCP_lastMessageID").setValue(new_messageid);
                                                    chatmap.setText("");
                                                    sendNoti(chatsID, current_user, friendID);
                                                }
                                            }
                                        }
                                    }
                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                    }
                                });
                            }
                        });
                        if (ghoststatus.equals("disable")) {
                            Toast.makeText(getContext(), "Cannot retrieve location. User is in ghost mode", Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        } else if (ghoststatus.equals("accurate")) {
                            ghostStat.setImageResource(R.drawable.pre_ghost);
                            ghostText.setText("Ghost mode off");
                            final LatLng location22 = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                            final Marker currentmarker = markersArray.get(user_id);
                            if (currentmarker != null) {
                                LatLng from = new LatLng(getLastlatitude(),getLastlongitude());
                                LatLng to = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));

                                Double d = SphericalUtil.computeDistanceBetween(from,to);
                                Double dd = d/1000;
//                                                        String.format("%.2f", d)
                                distance.setText(String.format("%.3f", dd));
                                battery.setText(btry_lvl+"%");
                                currentmarker.setPosition(location22);
                                currentmarker.setVisible(true);
                            }
                            displayChatonMap(user_id,mesejlast,msg_time);
                            if (useravatar.startsWith("h")) {
                                GlideApp.with(getContext())
                                        .asBitmap()
                                        .load(useravatar)
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                mMarker = map.addMarker(new MarkerOptions()
                                                        .snippet(user_id)
                                                        .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(), crop_image, markeroffline)))
                                                        .position(location22));
                                                animateMarker(mMarker, location22, false);
                                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                                                markerAlerts.put(mMarker.getId(),"kawan");

                                            }
                                        });
                            } else {
                                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(useravatar);
                                GlideApp.with(getContext())
                                        .asBitmap()
                                        .load(storageReference)
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                mMarker = map.addMarker(new MarkerOptions()
                                                        .snippet(user_id)
                                                        .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(), crop_image, markeroffline)))
                                                        .position(location22));
                                                animateMarker(mMarker, location22, false);
                                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                                                markerAlerts.put(mMarker.getId(),"kawan");

                                            }
                                        });
                            }
                        } else if(ghoststatus.equals("frozen")) {
                            ghostStat.setImageResource(R.drawable.fro_ghost);
                            ghostText.setText("Static ghost");
                            final LatLng location22 = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                            final Marker currentmarker = markersArray.get(user_id);
                            if (currentmarker != null) {
                                LatLng from = new LatLng(getLastlatitude(),getLastlongitude());
                                LatLng to = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));

                                Double d = SphericalUtil.computeDistanceBetween(from,to);
                                Double dd = d/1000;
//                                                        String.format("%.2f", d)
                                distance.setText(String.format("%.3f", dd));
                                battery.setText(btry_lvl+"%");
                                currentmarker.setPosition(location22);
                                currentmarker.setVisible(true);
                            }
                            displayChatonMap(user_id,mesejlast,msg_time);
                            if (useravatar.startsWith("h")) {
                                GlideApp.with(getContext())
                                        .asBitmap()
                                        .load(useravatar)
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                mMarker = map.addMarker(new MarkerOptions()
                                                        .snippet(user_id)
                                                        .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(), crop_image, markeroffline)))
                                                        .position(location22));
                                                animateMarker(mMarker, location22, false);
                                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                                                markerAlerts.put(mMarker.getId(),"kawan");

                                            }
                                        });
                            } else {
                                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(useravatar);
                                GlideApp.with(getContext())
                                        .asBitmap()
                                        .load(storageReference)
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                Bitmap crop_image = getCroppedBitmap(resource, "#ffffff00");
                                                mMarker = map.addMarker(new MarkerOptions()
                                                        .snippet(user_id)
                                                        .icon(BitmapDescriptorFactory.fromBitmap(createcustommarker(getContext(), crop_image, markeroffline)))
                                                        .position(location22));
                                                animateMarker(mMarker, location22, false);
                                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(location22, 15));
                                                markerAlerts.put(mMarker.getId(),"kawan");

                                            }
                                        });
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            alertDialog.setView(view);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(true);
//            Toast.makeText(getContext(), "friend popup clicked", Toast.LENGTH_SHORT).show();
        }
        else if(alert_id.equals("ping")){

            String user_id = marker.getSnippet();

            final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
            final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
            View view = LayoutInflater.from(getContext()).inflate(R.layout.ping_sel_popup, null);
//
            final TextView uname = view.findViewById(R.id.user_name);
            final TextView address = view.findViewById(R.id.address);
            final TextView date = view.findViewById(R.id.date_);
            final Button playbutton = view.findViewById(R.id.play_btn);
            final TextView hour = view.findViewById(R.id.hour);
            final TextView time = view.findViewById(R.id.time_);

            publicping.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()) {
                        System.out.println("no public ping");
                    } else {
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            final UCA_Details uca_details = new UCA_Details();
                            if (dataSnapshot1.hasChild("PP_ownerID")) {
                                String pcownerid = dataSnapshot1.child("PP_ownerID").getValue().toString();
                                String publicpingkey = dataSnapshot1.getKey();
                                if(user_id.equals(pcownerid)){
                                    System.out.println("userid:"+pcownerid);
                                    userinfo.child(pcownerid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if(dataSnapshot.exists()){
                                                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                                                    String name = dataSnapshot1.getValue().toString();
                                                    uname.setText(name);
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
//
                                    userinfo.child(pcownerid).child("userPing").child("UP_List").child(publicpingkey).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                                            if(dataSnapshot.exists()){
                                                UP_DetailsClass up_detailsdbClass = dataSnapshot2.child("UP_Details").getValue(UP_DetailsClass.class);
                                                String addressP = up_detailsdbClass.getUPD_address();
                                                String caption = up_detailsdbClass.getUPD_caption();
                                                long dateend = up_detailsdbClass.getUPD_dateEnd();
                                                long datestart = up_detailsdbClass.getUPD_dateStart();
                                                String mediatype = up_detailsdbClass.getUPD_mediaType();
                                                String mediaurl = up_detailsdbClass.getUPD_mediaURL();
                                                String mediaThumbnail = up_detailsdbClass.getUPD_mediaThumbnailURL();
                                                String ownerid = up_detailsdbClass.getUPD_ownerID();
                                                String pingid = up_detailsdbClass.getUPD_pingID();
                                                String pingtype = up_detailsdbClass.getUPD_type();

                                                SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd,yyyy");
                                                String y = currentDate.format(datestart);

                                                SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a");
                                                String z = currentTime.format(datestart);
                                                address.setText(addressP);
                                                date.setText(y);
                                                time.setText(z);
                                                hour.setText(getTimeleft(String.valueOf(dateend)));

                                                playbutton.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        //gotoimageview("pingImage/"+pingkey+".jpg");
//                                                        bottomSheetViewImage.show(getActivity().getSupportFragmentManager(),"viewimage");
                                                        Bundle args = new Bundle();
                                                        args.putString("key", "pm");
                                                        args.putString("url", mediaurl);
                                                        args.putString("vurl", mediaThumbnail);
                                                        args.putString("type", mediatype);
                                                        args.putString("caption", caption);
                                                        args.putString("ownerid", ownerid);
                                                        args.putString("pingid", pingid);
                                                        args.putString("datestart", z);
                                                        bottomSheetViewImage dialog = new bottomSheetViewImage();
                                                        dialog.setArguments(args);
                                                        dialog.show(getFragmentManager(),"c");
                                                    }
                                                });
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            userinfo.child(user_id).child("userPing").child("UP_Available").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(!dataSnapshot.exists()){

                    }else{
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            String pingidavailable = dataSnapshot1.getKey();
                            String pingType = dataSnapshot1.getValue().toString();
                            if(pingType.equals("friend")){
                                userinfo.child(user_id).child("userPing").child("UP_List").child(pingidavailable).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()){
                                            final UP_DetailsClass up_detailsClass = new UP_DetailsClass();
                                            UP_DetailsClass up_detailsdbClass = dataSnapshot.child("UP_Details").getValue(UP_DetailsClass.class);
                                            String addressP = up_detailsdbClass.getUPD_address();
                                            String caption = up_detailsdbClass.getUPD_caption();
                                            long dateend = up_detailsdbClass.getUPD_dateEnd();
                                            long datestart = up_detailsdbClass.getUPD_dateStart();
                                            String mediatype = up_detailsdbClass.getUPD_mediaType();
                                            String mediaurl = up_detailsdbClass.getUPD_mediaURL();
                                            String mediaThumbnail = up_detailsdbClass.getUPD_mediaThumbnailURL();
                                            String ownerid = up_detailsdbClass.getUPD_ownerID();
                                            String pingid = up_detailsdbClass.getUPD_pingID();
                                            String pingtype = up_detailsdbClass.getUPD_type();

                                            userinfo.child(user_id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    if(dataSnapshot.hasChild("UD_displayName")){
                                                        String retrievename = dataSnapshot.child("UD_displayName").getValue().toString();
                                                        uname.setText(retrievename);
                                                    }
                                                }
                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                                            });

                                            SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd,yyyy");
                                            String y = currentDate.format(datestart);

                                            SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a");
                                            String z = currentTime.format(datestart);
                                            address.setText(addressP);
                                            date.setText(y);
                                            time.setText(z);
                                            hour.setText(getTimeleft(String.valueOf(dateend)));

                                            playbutton.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    //gotoimageview("pingImage/"+pingkey+".jpg");
                                                    //                                                        bottomSheetViewImage.show(getActivity().getSupportFragmentManager(),"viewimage");
                                                    Bundle args = new Bundle();
                                                    args.putString("key", "pm");
                                                    args.putString("url", mediaurl);
//                                                        args.putString("vurl", mediaThumbnail);
                                                    args.putString("type", mediatype);
                                                    args.putString("caption", caption);
                                                    args.putString("ownerid", ownerid);
                                                    args.putString("pingid", pingid);
                                                    args.putString("datestart", z);
                                                    bottomSheetViewImage dialog = new bottomSheetViewImage();
                                                    dialog.setArguments(args);
                                                    dialog.show(getFragmentManager(),"c");
                                                }
                                            });
                                            //                        }
                                        }
                                    }
                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                                });
                            }
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            alertDialog.setView(view);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(true);
        }else if(alert_id.equals("ads")){
            System.out.println("ads:Eureka");
            String user_id = marker.getSnippet();
            final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
            final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
            View view = LayoutInflater.from(getContext()).inflate(R.layout.ads_poster_ar, null);
            final Button scanqr = view.findViewById(R.id.scanqrads);
            final Button tryAr = view.findViewById(R.id.tryAr);
            final ImageButton cancelbtn = view.findViewById(R.id.x_btn);
            final ImageView adsimg = view.findViewById(R.id.imageAds);
            final ImageView adsimg1 = view.findViewById(R.id.imageAds1);

            if(user_id.equals("eureka")){
                adsimg.setVisibility(View.VISIBLE);
                adsimg1.setVisibility(View.GONE);
            }else{
                adsimg.setVisibility(View.GONE);
                adsimg1.setVisibility(View.VISIBLE);
            }

            scanqr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), QRCouponScan.class);
                    startActivity(intent);
                }
            });

            tryAr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    Intent newIntent = new Intent(getActivity(), ArActivityPartners.class);
                    newIntent.putExtra("model", getStringmodel(new LatLng(0,0)));
                    startActivity(newIntent);
                }
            });

            cancelbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.setView(view);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(true);
        }else if(alert_id.equals("partner")){
            String partnerid = marker.getSnippet();
            System.out.println("partnerclick");
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            final AlertDialog alertDialog= builder.create();
            View view = LayoutInflater.from(context).inflate(R.layout.pingpartner_map_item, null);

            final TextView companyname = view.findViewById(R.id.company_name);
            final TextView number = view.findViewById(R.id.phone_number);
            final TextView website = view.findViewById(R.id.website);
            final ImageButton navigate = view.findViewById(R.id.direct_btn);
            CircleImageView companylogo = view.findViewById(R.id.company_logo);
            ImageButton pingar = view.findViewById(R.id.ar_btn);
            RelativeLayout ownertxt = view.findViewById(R.id.profile);

            partner.child(partnerid).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        final partnerDetailsClass partnerDetails = new partnerDetailsClass();
                        partnerDetailsClass partnerdetails = dataSnapshot.child("P_partnerDetails").getValue(partnerDetailsClass.class);
                        String partner_id = partnerdetails.getP_partnerID();
                        String upd_partnerid = partnerdetails.getUPD_partnerID();
                        String partner_name = partnerdetails.getP_partnerName();
                        long create_on = partnerdetails.getP_partnerCreatedOn();
                        String partner_phone = partnerdetails.getP_partnerPhone();
                        String partner_email = partnerdetails.getP_partnerEmail();
                        String partner_address = partnerdetails.getP_partnerAddress();
                        String partner_cate = partnerdetails.getP_partnerCategory();
                        String profile_url = partnerdetails.getP_profileURL();
                        String latitude = dataSnapshot.child("P_partnerLocation").child("P_partnerLatitude").getValue().toString();
                        String longitude = dataSnapshot.child("P_partnerLocation").child("P_partnerLongitude").getValue().toString();

                        double lat1 = Double.valueOf(latitude);
                        double lon1 = Double.valueOf(longitude);

                        partner.child(partnerid).child("P_partnerMember").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                                        String userID = dataSnapshot1.getKey();
                                        System.out.println("userID:"+userID);
                                        String role = dataSnapshot1.child("P_partnerRole").getValue().toString();
                                        System.out.println("role:"+role);
                                        if(role.equals("owner")){
                                            ownertxt.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent newIntent = new Intent(getActivity(), ViewBusinessProfileOther.class);
                                                    newIntent.putExtra("id", userID);
                                                    newIntent.putExtra("partnerID",partnerid);
                                                    newIntent.putExtra("email",partner_email);
                                                    startActivity(newIntent);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        companyname.setText(partner_name);
                        number.setText(partner_phone);
                        number.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
                                builder.setMessage("Go to Phone?")
                                        .setCancelable(true)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                                callIntent.setData(Uri.parse("tel:"+partner_phone));
                                                getPhonecallPermissions();
//                                                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                                                                getPhonecallPermissions();
//                                                                return;
//                                                            }
                                                startActivity(callIntent);
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                final androidx.appcompat.app.AlertDialog alert = builder.create();
                                alert.show();
//                                            Intent callIntent = new Intent(Intent.ACTION_CALL);
//                                            callIntent.setData(Uri.parse(getpartnerclassList.get(position).getP_partnerPhone()));
//                                            startActivity(callIntent);
                            }
                        });

                        website.setText(partner_email);
                        website.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent email = new Intent(Intent.ACTION_SEND);
                                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ partner_email});

                                //need this to prompts email client only
                                email.setType("message/rfc822");
                                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                            }
                        });
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(profile_url);
                        GlideApp.with(getContext())
                                .load(storageReference)
                                .into(companylogo);

                        navigate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
                                builder.setMessage("Open Google Maps?")
                                        .setCancelable(true)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Uri intentURi = Uri.parse("google.navigation:q="+lat1+","+lon1);
                                                Intent mapIntent = new Intent(Intent.ACTION_VIEW,intentURi);
                                                mapIntent.setPackage("com.google.android.apps.maps");
                                                try{
                                                    if(mapIntent.resolveActivity(getContext().getPackageManager()) != null){
                                                        startActivity(mapIntent);
                                                    }
                                                }catch (NullPointerException e){
                                                    Log.e(TAG,"onClick:NullPointerException: Could't open map." + e.getMessage());
                                                    Toast.makeText(getContext(),"Couldn't open map", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                final androidx.appcompat.app.AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            alertDialog.setView(view);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(true);
        }
        return false;
    }

    public String getMessageType() { return messageType; }
    public void setMessageType(String messageType) { this.messageType = messageType; }

}


