package com.app.pingchat.Map;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RunGPSBackground  extends Service{

    private static final String TAG = "BOOMBOOMTESTGPS";
    private LocationManager locationManager = null;
    private static final int LOCATION_INTERVAL = 600000;
    private static final float LOCATION_DISTANCE = 0f;
    private FirebaseAuth mAuth;
    private String current_user;
    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "Channel_Id";
    private class LocationListener implements android.location.LocationListener{

        Location mLastLocation;
        public LocationListener(String provider){
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location)
        {
            Log.e(TAG, "onLocationChanged: " + location);
            System.out.println("running background gps = " + location.getLatitude()
                    + " " + location.getLongitude());
            mLastLocation.set(location);
            saveUserLocation(location.getLatitude(),location.getLongitude());
        }

        @Override
        public void onProviderDisabled(String provider)
        {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }
        @Override
        public void onProviderEnabled(String provider)
        {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {return null;}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        startInForeground();
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//            startInForeground();
//
//        }
//        else{
//            startownforeground();
//        }

        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate()
    {
        mAuth = FirebaseAuth.getInstance();
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                mLocationListeners[1]);
    } catch (java.lang.SecurityException ex) {
        Log.i(TAG, "fail to request location update, ignore", ex);
    } catch (IllegalArgumentException ex) {
        Log.d(TAG, "network provider does not exist, " + ex.getMessage());
    }
        try {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (locationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    locationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(this.LOCATION_SERVICE);
        }
    }

    private void startInForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,notificationIntent,0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,NOTIF_CHANNEL_ID)
            .setOngoing(true)
            .setSmallIcon(R.drawable.pinglogo)
            .setContentTitle("PingChat App")
            .setContentText("Service is running background")
            .setContentIntent(pendingIntent);

        Notification notification=builder.build();
        if(Build.VERSION.SDK_INT>=26) {
            NotificationChannel channel = new NotificationChannel(NOTIF_CHANNEL_ID, "updating location", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription("running background");
            channel.setSound(null,null);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
        startForeground(1, notification);
       // stopForeground(true);
    }

    public void saveUserLocation(double lat,double lon){
        if (mAuth.getCurrentUser() == null)
        {
            System.out.println("x login");
        }
        else
        {
            current_user = mAuth.getCurrentUser().getUid();
            DatabaseReference location = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("userStatus");
            location.child("US_latitude").setValue(lat);
            location.child("US_longitude").setValue(lon);
            //location.child(current_user).child("time").setValue(ServerValue.TIMESTAMP);
            System.out.println("updated");
        }
    }
}
