package com.app.pingchat;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Chat.Messages;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessagesGroupAdapter extends RecyclerView.Adapter<MessagesGroupAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    Context context;
    ArrayList<Messages> messages;

    String urll;

    public MessagesGroupAdapter(ArrayList<Messages> m){
        messages = m;
    }

    public MessagesGroupAdapter(Context c, ArrayList<Messages> m){
        context = c;
        messages= m;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.chat_right,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();

        final String currentuser = mAuth.getCurrentUser().getUid();
        final Messages gmessages = messages.get(position);

        if(gmessages.getCP_mediaType().equals("notifier")){
            holder.notifier.setVisibility(View.VISIBLE);
            holder.notifier.setText(gmessages.getCP_message());
            holder.gsenderpic.setVisibility(View.GONE);
            holder.groupreceivertimepic.setVisibility(View.GONE);
            holder.greceiver.setVisibility(View.GONE);

            holder.sendertime.setVisibility(View.GONE);
            holder.sendertime2.setVisibility(View.GONE);
            holder.sendpictureLO.setVisibility(View.GONE);

            holder.gsenderpic.setVisibility(View.GONE);
            holder.greceiver.setVisibility(View.GONE);
            holder.groupreceivertimepic.setVisibility(View.GONE);
            holder.groupreceiver.setVisibility(View.GONE);

            holder.groupreceivertime.setVisibility(View.GONE);

            holder.sender.setVisibility(View.GONE);
            holder.sendertime.setVisibility(View.GONE);
        }
        else if(gmessages.getCP_mediaType().equals("text")){
            holder.notifier.setVisibility(View.GONE);
            holder.gsenderpic.setVisibility(View.GONE);
            holder.groupreceivertimepic.setVisibility(View.GONE);
            holder.greceiver.setVisibility(View.GONE);

            holder.sendertime2.setVisibility(View.GONE);
            holder.sendpictureLO.setVisibility(View.GONE);

            if(!gmessages.getCP_messageSenderID().equals(currentuser)){
                holder.sender.setVisibility(View.INVISIBLE);
                holder.sendertime.setVisibility(View.INVISIBLE);
                holder.groupreceivertimepic.setVisibility(View.INVISIBLE);

                holder.gsenderpic.setVisibility(View.VISIBLE);
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(gmessages.getAvatarurl());
                GlideApp.with(context.getApplicationContext())
                        .load(storageReference)
                        .into(holder.gsenderpic);
                holder.greceiver.setVisibility(View.VISIBLE);
                holder.groupreceiver.setVisibility(View.VISIBLE);
                holder.groupreceiver.setText(gmessages.getCP_message());
                holder.groupreceivertime.setVisibility(View.VISIBLE);
                holder.groupreceivertime.setText(gmessages.getDate());
            }
            else{

                holder.gsenderpic.setVisibility(View.INVISIBLE);
                holder.greceiver.setVisibility(View.INVISIBLE);
                holder.groupreceivertimepic.setVisibility(View.INVISIBLE);
                holder.groupreceiver.setVisibility(View.INVISIBLE);
                holder.groupreceivertime.setVisibility(View.INVISIBLE);

                holder.sender.setVisibility(View.VISIBLE);
                holder.sendertime.setVisibility(View.VISIBLE);
                holder.sendertime.setText(gmessages.getDate());
                holder.sender.setText(gmessages.getCP_message());
            }
        }
        else{

            holder.gsenderpic.setVisibility(View.GONE);
            holder.greceiver.setVisibility(View.INVISIBLE);
            holder.groupreceivertimepic.setVisibility(View.GONE);
            holder.groupreceiver.setVisibility(View.GONE);

            holder.sender.setVisibility(View.GONE);
            holder.sendertime.setVisibility(View.GONE);
            holder.notifier.setVisibility(View.GONE);
            holder.sendertime2.setVisibility(View.GONE);
            holder.groupreceivertime.setVisibility(View.GONE);

            if(!gmessages.getCP_messageSenderID().equals(currentuser)){

                holder.sendpictureLO.setVisibility(View.INVISIBLE);
                holder.gsenderpic.setVisibility(View.INVISIBLE);
                holder.gsenderpic.setVisibility(View.VISIBLE);
                holder.greceivepicture.setVisibility(View.VISIBLE);
                StorageReference storageReference1 = FirebaseStorage.getInstance().getReference().child(gmessages.getAvatarurl());
                GlideApp.with(context.getApplicationContext())
                        .load(storageReference1)
                        .into(holder.gsenderpic);
                holder.groupreceivertimepic.setText(gmessages.getDate());
                holder.groupreceivertimepic.setVisibility(View.VISIBLE);
                holder.greceiver.setVisibility(View.VISIBLE);
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(gmessages.getCP_message());
                GlideApp.with(context.getApplicationContext())
                        .load(storageReference)
                        .into(holder.grpicture);
            }
            else{

                holder.gsenderpic.setVisibility(View.INVISIBLE);
                holder.groupreceivertimepic.setText(gmessages.getDate());
                holder.groupreceivertimepic.setVisibility(View.INVISIBLE);
                holder.greceiver.setVisibility(View.INVISIBLE);
                holder.gsenderpic.setVisibility(View.VISIBLE);
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(gmessages.getCP_message());
                GlideApp.with(context.getApplicationContext())
                        .load(storageReference)
                        .into(holder.sendpicture);

            }

        }



    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView notifier,sender,sendertime,sendertime2,receiver,groupreceiver,groupreceivertime,groupreceivertimepic,receivertime2,receivertime;
        RelativeLayout greceiver,receivepictureLO,greceivepicture,sendpictureLO;
        CircleImageView gsenderpic;

        ImageView sendpicture,receivepicture,grpicture;
        public MyViewHolder(View itemView) {
            super(itemView);

            notifier = itemView.findViewById(R.id.notifier);
            sender = itemView.findViewById(R.id.sender);
            sendertime = itemView.findViewById(R.id.sendertime);
            sendertime2 = itemView.findViewById(R.id.sendertime2);
            receiver = itemView.findViewById(R.id.receiver);
            groupreceiver = itemView.findViewById(R.id.groupreceiver);
            groupreceivertime = itemView.findViewById(R.id.groupreceivertime);
            groupreceivertimepic = itemView.findViewById(R.id.groupreceivertimepic);
            receivertime2 = itemView.findViewById(R.id.receivertime2);
            receivertime = itemView.findViewById(R.id.receivertime);

            gsenderpic  =itemView.findViewById(R.id.gsenderpic);
            greceiver  =itemView.findViewById(R.id.greceiver);
            receivepictureLO  =itemView.findViewById(R.id.receivepictureLO);
            greceivepicture  =itemView.findViewById(R.id.greceivepicture);
            sendpictureLO = itemView.findViewById(R.id.sendpictureLO);

            sendpicture = itemView.findViewById(R.id.sendpicture);
            receivepicture = itemView.findViewById(R.id.receivepicture);

            grpicture = itemView.findViewById(R.id.grpicture);
           }
    }
    public void filterList(ArrayList<Messages> filteredlist){
        messages = filteredlist;
        notifyDataSetChanged();
    }

    public String getUrll() { return urll; }

    public void setUrll(String urll) {
        storeRef.child(urll).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String dapat = uri.toString();
                System.out.println(dapat);

            }
        });
        this.urll = urll;
    }
}
