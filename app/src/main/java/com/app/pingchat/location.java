package com.app.pingchat;

public class location {
    public double US_latitude;
    public double US_longitude;
    public String userID;
    //public String ghostMode;

    public location(){
    }

    public location(double latitude,double longitude,String userID,String ghostMode){
        this.US_latitude = latitude;
        this.US_longitude = longitude;
        this.userID = userID;
        //this.ghostMode = ghostMode;
    }

    public location (double latitude,double longitude){
        this.US_latitude = latitude;
        this.US_longitude = longitude;
    }

    public double getLatitude() {
        return US_latitude;
    }

    public void setLatitude(double latitude) {
        this.US_latitude = latitude;
    }

    public double getLongitude() {
        return US_longitude;
    }

    public void setLongitude(double longitude) {
        this.US_longitude = longitude;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

//    public String getGhostMode() { return ghostMode; }
//
//    public void setGhostMode(String ghostMode) { this.ghostMode = ghostMode; }
}


