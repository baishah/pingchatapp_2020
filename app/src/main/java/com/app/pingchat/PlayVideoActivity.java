package com.app.pingchat;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class PlayVideoActivity extends AppCompatActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_video_layout);

        VideoView v = findViewById(R.id.playvideo);
        ImageButton back = findViewById(R.id.back);
        ImageButton playbtn = findViewById(R.id.playbtn);
        ProgressBar progressBar = findViewById(R.id.progrss);
        Intent intent=getIntent();

        String test = intent.getStringExtra("test");

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(test);
        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                progressBar.setVisibility(View.GONE);
                v.setVideoURI(uri);
                if(!v.isPlaying()){
                    v.setVideoURI(uri);
                    v.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            playbtn.setImageResource(R.drawable.play_icon1);
                        }
                    });

                    }else{
                        v.pause();
                        playbtn.setImageResource(R.drawable.play_icon1);
                    }

                v.requestFocus();
                v.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            mediaPlayer.setLooping(true);
                            progressBar.setVisibility(View.INVISIBLE);
//                            v.start();
//                            holder.videoView.start();
//                            holder.playbtn.setImageResource(R.drawable.pausewhite);
                        }
                    });
            }
        });

        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!v.isPlaying()){
                    v.start();
                    playbtn.setImageResource(R.drawable.pausewhite);
                }else{
                    v.pause();
                    playbtn.setImageResource(R.drawable.play_icon1);
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        v.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                if(i==mediaPlayer.MEDIA_INFO_BUFFERING_START){
                    progressBar.setVisibility(View.VISIBLE);
                }
                else if( i== mediaPlayer.MEDIA_INFO_BUFFERING_END){
                    progressBar.setVisibility(View.INVISIBLE);
                }
                return false;
            }
        });

    }
}
