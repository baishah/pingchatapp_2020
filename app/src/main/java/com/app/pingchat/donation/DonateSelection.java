package com.app.pingchat.donation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DonateSelection extends AppCompatActivity {
    static int position;
    Button backButton;
    static Button nextButton;
    ImageView nolist;
    ArrayList<DonationItem> donateList;
    private RequestQueue mQueue;
    FirebaseAuth mAuth;
    private String current_user;
    RelativeLayout rl0;
    private Context context;


    String charity[], description[], more[];
    int images[]= {R.drawable.photo2, R.drawable.image1, R.drawable.image2, R.drawable.image3};
    RecyclerView charityList;
    boolean checkState[] = new boolean[20];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate_selection);

        backButton = findViewById(R.id.backButton);
        nextButton= findViewById(R.id.nextButton);
        charityList = findViewById(R.id.charityList);
        nolist = findViewById(R.id.no_list);
        rl0 = findViewById(R.id.r0);

        context = this;

//        charity = getResources().getStringArray(R.array.charity);
//        description = getResources().getStringArray(R.array.description);
//        more = getResources().getStringArray(R.array.more);
        mAuth = FirebaseAuth.getInstance();
        current_user = mAuth.getCurrentUser().getUid();
        mQueue = Volley.newRequestQueue(this);

        for(int i = 0; i<images.length;i++) {
            System.out.println(i);
            checkState[i] = false;
        }

        getDonationCount(current_user);



//        DonationAdapter charityListAdapter = new DonationAdapter(this, charity, description, images, checkState);
//        charityList.setAdapter(charityListAdapter);
        charityList.setLayoutManager(new LinearLayoutManager(this));

        nextButton.setEnabled(false);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DonateSelection.this, DonationAmount.class);
                intent.putExtra("charityData", charity[position]);
                intent.putExtra("charityImage", images[position]);
                startActivity(intent);
            }
        });
    }

    public void getDonationCount(String c_user){

//        donateList = new ArrayList();
//        DonationAdapter donateAdapter = new DonationAdapter(getApplicationContext(),donateList,c_user);
//        charityList.setAdapter(donateAdapter);

        String url = "https://devbm.ml/donation/all";

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {

                            String countdonate = response.getString("countdonation");

                            if(countdonate.equals("0")){
                                nolist.setVisibility(View.VISIBLE);
                                rl0.setVisibility(View.GONE);
                            }else
                                {
                                    getDonationlist(c_user);
                                    nolist.setVisibility(View.GONE);
                                    rl0.setVisibility(View.VISIBLE);
                                }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);
    }


    public void getDonationlist(String c_user){

        donateList = new ArrayList();
        DonationAdapter donateAdapter = new DonationAdapter(context,donateList,c_user);
        charityList.setAdapter(donateAdapter);


        String url = "https://devbm.ml/donation/all";

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("datadonation");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject value = jsonArray.getJSONObject(i);

                                String donationId = value.getString("DD_donationID");
                                long startDate = Long.parseLong(value.getString("DD_donationStartDate"));
                                long endDate = Long.parseLong(value.getString("DD_donationEndDate"));
                                String donateTitle = value.getString("DD_donationTitle");
                                String donateUrl = value.getString("DD_donationURL");
                                String donateAddress = value.getString("DD_donationAddress");
                                String donateDescription = value.getString("DD_donationDescription");
                                String d_latitude = value.getString("DL_latitude");
                                String d_longitude = value.getString("DL_longitude");
                                String d_mediaType = value.getString("DM_mediaType");
                                String d_mediaURL = value.getString("DM_mediaURL");
                                String d_mediakey = value.getString("DM_mediaKey");

                                DonationItem donationItem = new DonationItem();

                                donationItem.setDD_donationID(donationId);
                                donationItem.setDD_donationStartDate(startDate);
                                donationItem.setDD_donationTitle(donateTitle);
                                donationItem.setDD_donationURL(donateUrl);
                                donationItem.setDD_donationAddress(donateAddress);
                                donationItem.setDD_donationDescription(donateDescription);
                                donationItem.setDL_latitude(d_latitude);
                                donationItem.setDL_longitude(d_longitude);
                                donationItem.setDM_mediaType(d_mediaType);
                                donationItem.setDM_mediaURL(d_mediaURL);
                                donationItem.setDM_mediaKey(d_mediakey);



                                donateList.add(donationItem);
                            }
//                            progressDialog.dismiss();
                            donateAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);
    }

    public static void enableNextButton() {
        nextButton.setEnabled(true);
        nextButton.setBackgroundResource(R.drawable.round_blue);
    }


    public static void setPosition(int position) {
        DonateSelection.position = position;
    }

    public void onBackPressed(){
        finish();
//            super.onBackPressed();
    }
}
