package com.app.pingchat.donation;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.app.pingchat.WebActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class DonationAdapter extends RecyclerView.Adapter<DonationAdapter.CharityListViewHolder> {

    int images[];
    boolean checkState = false;
    Context context;
    private FirebaseAuth mAuths;
    ArrayList<DonationItem> donateList;
    private String current_user;
    StorageReference donateimage,storeRef;
    FirebaseStorage storage;
    boolean isTextViewClicked = false;

    public DonationAdapter(ArrayList<DonationItem> charityList) {
        this.donateList = charityList;
//        alldiscoverarraylist2Full = new ArrayList<>(Discoverlist);

    }

    public DonationAdapter(Context ct, ArrayList<DonationItem> d, String y) {
        this.context = ct;
//        charityData = charity;
//        descriptionData= description;
//        images = image;
        this.current_user = y;
        this.donateList = d;

    }

    @NonNull
    @Override
    public CharityListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.donate_item, parent, false);
        return new CharityListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CharityListViewHolder holder,final int position) {
        final DonationItem donationItem = donateList.get(position);
        mAuths = FirebaseAuth.getInstance();
        current_user = mAuths.getCurrentUser().getUid();
        donateimage = FirebaseStorage.getInstance().getReference().child("donation");
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        String avatar_url = donationItem.getDM_mediaURL();
        String mediakey= donationItem.getDM_mediaKey();
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(10f);
        circularProgressDrawable.setCenterRadius(50f);
        circularProgressDrawable.start();


        String web_url = donationItem.getDD_donationURL();
        String title = donationItem.getDD_donationTitle();
        holder.titleText.setText(donationItem.getDD_donationTitle());
        holder.descText.setText(donationItem.getDD_donationDescription());
        checkState = false;
//        holder.charityImage
//        GlideApp.with(context.getApplicationContext())
//                .load(avatar_url)
//                .placeholder(circularProgressDrawable)
//                .into(holder.charityImage);

        storeRef.child(avatar_url).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                GlideApp.with(context.getApplicationContext())
                .load(uri)
                .placeholder(circularProgressDrawable)
                .into(holder.charityImage);
                System.out.println("media_url:"+uri);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });

        holder.web_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtra("key","donate");
                intent.putExtra("url",web_url);
                intent.putExtra("title",title);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

//        holder.whiteBox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                holder.whiteBox.setBackgroundResource(R.drawable.blue_box);
//                holder.titleText.setTextColor(Color.WHITE);
//                holder.descText.setTextColor(Color.WHITE);
//                holder.showmore.setTextColor(Color.WHITE);
//                DonateSelection.enableNextButton();
//                checkState = false;
//                DonateSelection.setPosition(position);
//            }
//        });



//        if(avatar_url.startsWith("p")){
//            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar_url);
//            GlideApp.with(context).load(storageReference).placeholder(circularProgressDrawable).into(holder.charityImage);
//        }
//        else if(avatar_url.startsWith("h")){
//            GlideApp.with(context.getApplicationContext())
//                    .load(avatar_url)
//                    .placeholder(circularProgressDrawable)
//                    .into(holder.charityImage);
//        }


//        if(checkState[position]) {
//            holder.whiteBox.setBackgroundResource(R.drawable.blue_box);
//            holder.titleText.setTextColor(Color.WHITE);
//            holder.descText.setTextColor(Color.WHITE);
//            holder.showmore.setTextColor(Color.WHITE);
//            DonateSelection.enableNextButton();
//            checkState[position] = false;
//            DonateSelection.setPosition(position);
//        } else {
//            holder.whiteBox.setBackgroundResource(R.drawable.edit_box);
//
//        }

//        holder.charityLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////
//                if(checkState[position]) {
//                    checkState[position]=false;
//                } else {
//                    checkState[position]=true;
//                }
//                notifyDataSetChanged();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return donateList.size();
    }

    public class CharityListViewHolder extends RecyclerView.ViewHolder {
        TextView titleText, descText,less;
        Button web_site;
        ImageView charityImage;

        CardView whiteBox;
        RelativeLayout charityLayout;

        public CharityListViewHolder(@NonNull View itemView) {
            super(itemView);

            titleText = itemView.findViewById(R.id.titleText);
            descText = itemView.findViewById(R.id.descText);
            web_site = itemView.findViewById(R.id.web_site);
            charityImage = itemView.findViewById(R.id.imageholder);
            whiteBox = itemView.findViewById(R.id.card1);
            charityLayout = itemView.findViewById(R.id.charityLayout);
            less = itemView.findViewById(R.id.lesss);


            descText.post(new Runnable() {
                @Override
                public void run() {
//            showmore.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//
//                @Override
//                public boolean onPreDraw() {
//                    // Remove listener because we don't want this called before _every_ frame
//                    showmore.getViewTreeObserver().removeOnPreDrawListener(this);
//                    // Drawing happens after layout so we can assume getLineCount() returns the correct value
//                    if(showmore.getLineCount() > 2) {
                    descText.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isTextViewClicked) {
                                //This will shrink textview to 2 lines if it is expanded.
                               descText.setMaxLines(3);
                                isTextViewClicked = false;
                                System.out.println("false");
                                less.setVisibility(View.GONE);
                            } else {
                                //This will expand the textview if it is of 2 lines
                                descText.setMaxLines(Integer.MAX_VALUE);
                                isTextViewClicked = true;
                                System.out.println("true");
                                less.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    less.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            less.setVisibility(View.GONE);
                            System.out.println("less");
                            descText.setMaxLines(3);
                        }
                    });
//                    else{
//                        less.setVisibility(View.GONE);
////                        status1.setMaxLines(1);
//                    }
//                    return true; // true because we don't want to skip this frame
                }
            });


        }

    }
}
