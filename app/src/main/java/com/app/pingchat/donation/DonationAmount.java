package com.app.pingchat.donation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.pingchat.R;

public class DonationAmount extends AppCompatActivity {
    Button backButton;
    Button nextButton;
    TextView rm;
    EditText amountInput;
    TextView charityDataText;
    ImageView charityImageView;
    String charityData;
    int charityImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation_amount);

        charityDataText = findViewById(R.id.charityDataText);
        charityImageView = findViewById(R.id.charityImageView);
        backButton = findViewById(R.id.backButton);
        nextButton = findViewById(R.id.nextButton);
        rm = findViewById(R.id.rm);
        amountInput = findViewById(R.id.amountInput);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DonationAmount.this, DonateSelection.class));
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DonationAmount.this, DonateActivity.class));
            }
        });

        amountInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    ViewCompat.setBackgroundTintList(v, ColorStateList.valueOf(getResources().getColor(R.color.dark_blue)));
                    rm.setTextColor(Color.parseColor("#000000"));
                    System.out.println("Success");

                } else {
                    ViewCompat.setBackgroundTintList(v, ColorStateList.valueOf(getResources().getColor(R.color.grey)));
                    rm.setTextColor(Color.parseColor("#979797"));
                }
            }
        });

        amountInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!amountInput.getText().toString().isEmpty()){
                    nextButton.setBackground(getResources().getDrawable(R.drawable.round_blue));
                    nextButton.setEnabled(true);
                } else {
                    nextButton.setBackground(getResources().getDrawable(R.drawable.round_gray));
                    nextButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        getData();
        setData();
    }

    private void  getData() {
        if(getIntent().hasExtra("charityImage") && getIntent().hasExtra("charityData")) {
            charityData= getIntent().getStringExtra("charityData");
            charityImage = getIntent().getIntExtra("charityImage", 1);
        } else {
            Toast.makeText(this, "No Data.", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        charityDataText.setText(charityData);
        charityImageView.setImageResource(charityImage);
    }
}
