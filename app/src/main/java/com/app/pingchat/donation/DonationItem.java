package com.app.pingchat.donation;

public class DonationItem  {

    public String DD_donationAddress;
    public String DD_donationDescription;
    public long DD_donationEndDate;
    public String DD_donationID;
    public long DD_donationStartDate;
    public String DD_donationTitle;
    public String DD_donationURL;
    public String DL_latitude;
    public String DL_longitude;
    public String DM_mediaType;
    public String DM_mediaURL;
    public String DM_mediaKey;


    public DonationItem() {
    }

    public String getDD_donationAddress() {
        return DD_donationAddress;
    }

    public void setDD_donationAddress(String DD_donationAddress) {
        this.DD_donationAddress = DD_donationAddress;
    }

    public String getDD_donationDescription() {
        return DD_donationDescription;
    }

    public void setDD_donationDescription(String DD_donationDescription) {
        this.DD_donationDescription = DD_donationDescription;
    }

    public long getDD_donationEndDate() {
        return DD_donationEndDate;
    }

    public void setDD_donationEndDate(long DD_donationEndDate) {
        this.DD_donationEndDate = DD_donationEndDate;
    }

    public String getDD_donationID() {
        return DD_donationID;
    }

    public void setDD_donationID(String DD_donationID) {
        this.DD_donationID = DD_donationID;
    }

    public long getDD_donationStartDate() {
        return DD_donationStartDate;
    }

    public void setDD_donationStartDate(long DD_donationStartDate) {
        this.DD_donationStartDate = DD_donationStartDate;
    }

    public String getDD_donationTitle() {
        return DD_donationTitle;
    }

    public void setDD_donationTitle(String DD_donationTitle) {
        this.DD_donationTitle = DD_donationTitle;
    }

    public String getDD_donationURL() {
        return DD_donationURL;
    }

    public void setDD_donationURL(String DD_donationURL) {
        this.DD_donationURL = DD_donationURL;
    }

    public String getDL_latitude() {
        return DL_latitude;
    }

    public void setDL_latitude(String DL_latitude) {
        this.DL_latitude = DL_latitude;
    }

    public String getDL_longitude() {
        return DL_longitude;
    }

    public void setDL_longitude(String DL_longitude) {
        this.DL_longitude = DL_longitude;
    }

    public String getDM_mediaType() {
        return DM_mediaType;
    }

    public void setDM_mediaType(String DM_mediaType) {
        this.DM_mediaType = DM_mediaType;
    }

    public String getDM_mediaURL() {
        return DM_mediaURL;
    }

    public void setDM_mediaURL(String DM_mediaURL) {
        this.DM_mediaURL = DM_mediaURL;
    }

    public String getDM_mediaKey() {
        return DM_mediaKey;
    }

    public void setDM_mediaKey(String DM_mediaKey) {
        this.DM_mediaKey = DM_mediaKey;
    }
}
