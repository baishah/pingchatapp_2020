package com.app.pingchat;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.ar.sceneform.ux.ArFragment;

public class AR_model_fragment extends ArFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPlaneDiscoveryController().hide();
        getPlaneDiscoveryController().setInstructionView(null);
    }
}
