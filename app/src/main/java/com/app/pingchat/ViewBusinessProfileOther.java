package com.app.pingchat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewBusinessProfileOther extends AppCompatActivity {

    ImageButton back, msg_btn, email,bs_icon,web_icon,friendBtn,addfriendBtn;
    TextView bizname,biztype,bizaddress, m_count,friendtxt;
    CircleImageView logo;
    private RequestQueue mQueue;
    private FirebaseAuth mAuth;
    private String currenct, id, partnerid,emailp;
    private DatabaseReference users,partner;
    private ImageView nopost;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.business_profile_otherview);
        Intent intent = getIntent();

        mAuth = FirebaseAuth.getInstance();
        currenct = mAuth.getCurrentUser().getUid();
        id = intent.getStringExtra("id");
        partnerid = intent.getStringExtra("partnerID");
        emailp = intent.getStringExtra("email");

        users = FirebaseDatabase.getInstance().getReference().child("Users");
        users.keepSynced(true);
        partner = FirebaseDatabase.getInstance().getReference().child("Partners");
        partner.keepSynced(true);

        mQueue = Volley.newRequestQueue(this);

        back = findViewById(R.id.back);
        bizname = findViewById(R.id.biz_name);
        biztype = findViewById(R.id.business_type);
        bizaddress = findViewById(R.id.txt_add);
        logo = findViewById(R.id.pro_pic);
        msg_btn = findViewById(R.id.msg_btn);
        web_icon = findViewById(R.id.web_icon);
        email = findViewById(R.id.email_icon);
        bs_icon = findViewById(R.id.bs_icon);
        nopost = findViewById(R.id.no_post);
        m_count = findViewById(R.id.friend_num);
        friendBtn = findViewById(R.id.frend_btn);
        addfriendBtn = findViewById(R.id.addfrendbtn);
        friendtxt = findViewById(R.id.contct_txt);
        getfriendMetadata(id);
        checkFollow(partnerid);
        gettotalfriend();

//        web_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), "In development mode.", Toast.LENGTH_SHORT).show();
//            }
//        });

        addfriendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                follow(id,partnerid);
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{emailp });

                //need this to prompts email client only
                email.setType("message/rfc822");
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        });

    }

    private void checkFollow(String partnerid) {
        partner.child(partnerid).child("P_partnerMember").child(currenct).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    addfriendBtn.setVisibility(View.GONE);
                    friendBtn.setVisibility(View.VISIBLE);
                    friendtxt.setText("Following");
                }else{
                    addfriendBtn.setVisibility(View.VISIBLE);
                    friendBtn.setVisibility(View.GONE);
                    friendtxt.setText("Follow");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void follow(String id,String partnerid) {
        System.out.println("idpartner:"+partnerid);
        final Map partnerMember = new HashMap();

        partnerMember.put("P_partnerJoined", ServerValue.TIMESTAMP);
        partnerMember.put("P_partnerRole", "member");
        users.child(currenct).child("userFollowedPartner").child(partnerid).setValue(ServerValue.TIMESTAMP);
            partner.child(partnerid).child("P_partnerMember").child(currenct).setValue(partnerMember).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
                        //  addfriendfunc(id,currentuser);
//                        users.child(friendid).child("userNotifications").child("UN_type").child("UN_friendRequest").child(current).setValue(newReq).addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
//                                String url = "https://devbm.ml/notification/"+current+"/send-friend-request/"+friendid;
//
//                                StringRequest sq = new StringRequest
//                                        (Request.Method.POST, url, new Response.Listener<String>() {
//                                            @Override
//                                            public void onResponse(String response) {}
//                                        }, new Response.ErrorListener() {
//                                            @Override
//                                            public void onErrorResponse(VolleyError error) {
//
//                                            }
//                                        }){
//                                    protected Map<String,String> getParams(){
//                                        Map<String,String> parr = new HashMap<String, String>();
////                                              parr.put("friendid", friendid);
//                                        return parr;
//                                    }
//                                };
//                                sq.setRetryPolicy(new DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//                                queue.add(sq);
//                                Toast.makeText(getApplicationContext(), "Request sent!", Toast.LENGTH_SHORT).show();
//                            }
//                        });
                        addfriendBtn.setVisibility(View.GONE);
                        friendtxt.setText("Following");
                        friendBtn.setVisibility(View.VISIBLE);

                    }
                }
            });
    }
    private void gettotalfriend() {
        partner.child(partnerid).child("P_partnerMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int total = 0;
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
//                    String friend = dataSnapshot1.getValue().toString();
//                    if(friend.equals("friend")){
                        total = total+1;
//                    }
                }
                System.out.println("total friend "+total);
                String num = String.valueOf(total);
                m_count.setText(num);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    private void getfriendMetadata(String id) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getApplication());
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.setBackgroundColor(R.color.colorPrimary);
        circularProgressDrawable.start();

        users.child(id).child("userPartnerDetails").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UPD_partnerID")){
                    String partner_id = dataSnapshot.child("UPD_partnerID").getValue().toString();
                    System.out.println("partner_id="+partner_id);
                    partner.child(partner_id).child("P_partnerDetails").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            nopost.setVisibility(View.VISIBLE);
//                            m_count.setText("1");
                            if(dataSnapshot.hasChild("P_partnerName")){
                                String name = dataSnapshot.child("P_partnerName").getValue().toString();
                                bizname.setText(name);
                            }
                            if(dataSnapshot.hasChild("P_partnerCategory")){
                                String bizcat = dataSnapshot.child("P_partnerCategory").getValue().toString();
                                biztype.setText(bizcat);
                            }
                            if(dataSnapshot.hasChild("P_partnerCaption")){

                                String biz_add = dataSnapshot.child("P_partnerCaption").getValue().toString();
                                bizaddress.setText(biz_add);
                            }
                            if(dataSnapshot.hasChild("P_partnerEmail")){
                                String bizemail= dataSnapshot.child("P_partnerEmail").getValue().toString();

                            }
                            if(dataSnapshot.hasChild("P_profileURL")){
                                String avatar = dataSnapshot.child("P_profileURL").getValue().toString();
                                if(avatar.startsWith("p")){
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                    GlideApp.with(getApplication().getApplicationContext()).load(storageReference).placeholder(R.drawable.loading_img).into(logo);
                                }
                                else if(avatar.startsWith("h")){
                                    GlideApp.with(getApplication().getApplicationContext()).load(avatar).placeholder(R.drawable.loading_img).into(logo);
                                }
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

}
