package com.app.pingchat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Map.MapsFragment;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MarketplaceActivity extends AppCompatActivity {
    private RequestQueue mQueue;
    private RecyclerView mRecyclerView, mRecyclerViewNear;
    private MarketAdapter mMarketAdapter;
    private NearbyMarketAdapter nearbyMarketAdapter;
    private ArrayList<MarketItem> mRandomList;
    private ArrayList<MarketItem> mNearbyList;
    private FirebaseAuth Mauth;
    private String currenct;
    double currentlan,currentlon;
    LottieAnimationView animationView;
    LocationManager locationManager;
    ProgressDialog progressDialog;


    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.market_tmk_layout);

        Mauth = FirebaseAuth.getInstance();
        mQueue = Volley.newRequestQueue(this);

        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
        }else{
//            showGPSDisabledAlertToUser();
            showAlert();
        }

        if(Mauth.getCurrentUser()!=null) {
            currenct = Mauth.getCurrentUser().getUid();

            mRecyclerView = findViewById(R.id.marketpost);
            mRecyclerViewNear = findViewById(R.id.nearbyM);
//        mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
            mRecyclerViewNear.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));

            mRandomList = new ArrayList<>();
            mNearbyList = new ArrayList<>();

            currentlan = MapsFragment.getInstance().lastlatitude;
            currentlon = MapsFragment.getInstance().lastlongitude;

            animationView = (LottieAnimationView) findViewById(R.id.animation_view);
            animationView.setAnimation("loading_list.json");
            animationView.playAnimation();
            animationView.setVisibility(View.VISIBLE);

            getNearbyM(currenct,currentlan, currentlon);
            getmarketapi(currenct,currentlan, currentlon);

//            progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage("Loading MarketPlace list");
//            progressDialog.show();
        }

    }

    private void getNearbyM(String id, double lat, double lon){

        // if  you want cancel, you can use this
        // animationView.cancelAnimation();
        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/localAPI/nearbyList/"+id;

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        postParams.put("lat",lat);
        postParams.put("lon",lon);

        nearbyMarketAdapter = new NearbyMarketAdapter(MarketplaceActivity.this,mNearbyList);
        mRecyclerViewNear.setAdapter(nearbyMarketAdapter);

        nearbyMarketAdapter.setOnItemClickListener(new MarketAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, String pID, String pOwner) {
                System.out.println("marketowner:"+pOwner);
                startNewActivity(getApplicationContext(),"com.tamuku.nana.latesttamuku",pID,pOwner);
            }
        });

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("nearbyMarketplace");

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject value = jsonArray.getJSONObject(i);

                                String pName = value.getString("productName");
                                String pPrice = value.getString("productPrice");
                                String pDesc = value.getString("productDesc");
                                String pUrl = value.getString("productPhoto");
                                String pId = value.getString("productID");
                                String pOwner = value.getString("productIDowner");

//                                System.out.println("marketList:"+mMarketList);

                                MarketItem marketItem = new MarketItem();
                                marketItem.setmDesc(pDesc);
                                marketItem.setmImageUrl(pUrl);
                                marketItem.setmName(pName);
                                marketItem.setmPrice(pPrice);
                                marketItem.setmPID(pId);
                                marketItem.setmPowner(pOwner);

                                mNearbyList.add(marketItem);
                            }
//                            animationView.cancelAnimation();
//                            animationView.setVisibility(View.GONE);
//                            progressDialog.dismiss();
                            nearbyMarketAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);

    }

    private void getmarketapi(String id, double lat, double lon){
        // if  you want cancel, you can use this
        // animationView.cancelAnimation();
        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/localAPI/nearbyList/"+id;

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        postParams.put("lat",lat);
        postParams.put("lon",lon);

        mMarketAdapter = new MarketAdapter(MarketplaceActivity.this, mRandomList);
        mRecyclerView.setAdapter(mMarketAdapter);

        mMarketAdapter.setOnItemClickListener(new MarketAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position,String pID,String pOwner) {
//                Toast.makeText(getApplicationContext(), "WE HERE!", Toast.LENGTH_SHORT).show();
                System.out.println("marketowner:"+pOwner);
                startNewActivity(getApplicationContext(),"com.tamuku.nana.latesttamuku",pID,pOwner);
            }
        });

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("randomProduct");
                            int n = Math.min(50, jsonArray.length());
                            for (int i = 0; i < n; i++){
                                JSONObject value = jsonArray.getJSONObject(i);

                                String pName = value.getString("productName");
                                String pPrice = value.getString("productPrice");
                                String pDesc = value.getString("productDesc");
                                String pUrl = value.getString("productPhoto");
                                String pID = value.getString("productID");
                                String pOwner = value.getString("productIDowner");


//                                System.out.println("marketowner:"+pOwner);
//
//                                System.out.println("marketList:"+mMarketList);

                                MarketItem marketItem = new MarketItem();
                                marketItem.setmDesc(pDesc);
                                marketItem.setmImageUrl(pUrl);
                                marketItem.setmName(pName);
                                marketItem.setmPrice(pPrice);
                                marketItem.setmPID(pID);
                                marketItem.setmPowner(pOwner);

                                mRandomList.add(marketItem);
                            }
//                            animationView.cancelAnimation();
//                            animationView.setVisibility(View.GONE);
//                            progressDialog.dismiss();
                            animationView.cancelAnimation();
                            animationView.setVisibility(View.GONE);
                            mMarketAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);

    }

    public void showAlert(){
        String message,title,btntext;

        message = "Please turn on Location Services to view nearby listing.";
        title = "Location not found!";
        btntext = "Location Settings";

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton(btntext, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent callGPSSettingIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }

    public void startNewActivity(Context context, String packageName, String pID, String pOwner) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);

        if (intent != null) {
            // We found the activity now start the activity
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra("productID", pID);
            intent.putExtra("ownerID", pOwner);
            context.startActivity(intent);
        } else {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
            context.startActivity(intent);
        }
    }

    public void onBackPressed(){
//        super.onBackPressed();
        finish();
    }
}

