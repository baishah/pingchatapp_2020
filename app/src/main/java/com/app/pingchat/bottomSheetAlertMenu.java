package com.app.pingchat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class bottomSheetAlertMenu extends BottomSheetDialogFragment implements View.OnClickListener {
    private BottomSheetListener mListener;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }
    private Button hot,rain,landslide,hazard,trafficlight,accident,police,closure,trafficjam;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_alert_sticker, container, false);

        hot = v.findViewById(R.id.hot);
        rain = v.findViewById(R.id.rain);
        landslide = v.findViewById(R.id.landslide);
        hazard = v.findViewById(R.id.hazard);
        trafficlight = v.findViewById(R.id.trafficlight);
        accident = v.findViewById(R.id.accident);
        police = v.findViewById(R.id.police);
        closure = v.findViewById(R.id.closure);
        trafficjam = v.findViewById(R.id.trafficjam);

        hot.setOnClickListener(this);
        rain.setOnClickListener(this);
        landslide.setOnClickListener(this);
        hazard.setOnClickListener(this);
        trafficlight.setOnClickListener(this);
        accident.setOnClickListener(this);
        police.setOnClickListener(this);
        closure.setOnClickListener(this);
        trafficjam.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.hot:
                mListener.onButtonAlertSticker("hot");
                break;
            case R.id.rain:
                mListener.onButtonAlertSticker("rain");
                break;
            case R.id.landslide:
                mListener.onButtonAlertSticker("landslide");
                break;
            case R.id.hazard:
                mListener.onButtonAlertSticker("hazard");
                dismiss();
                break;
            case R.id.trafficlight:
                mListener.onButtonAlertSticker("trafficLight");
                dismiss();
                break;
            case R.id.accident:
                mListener.onButtonAlertSticker("accident");
                dismiss();
                break;
            case R.id.police:
                mListener.onButtonAlertSticker("police");
                dismiss();
                break;
            case R.id.closure:
                mListener.onButtonAlertSticker("closure");
                dismiss();
                break;
            case R.id.trafficjam:
                mListener.onButtonAlertSticker("trafficJam");
                dismiss();
                break;
        }
    }

    public interface BottomSheetListener {
        void onButtonAlertSticker(String text);
    }

}
