package com.app.pingchat;

public class getpartnerclass {

    String businessPartnerID;

    String businessCreator;
    String businessName;
    String businessRegister;
    String businessLastUpdate;
    String businessPhone;
    String businessEmail;
    String businessLatitude;
    String businessLongitude;
    String businessActualAddress;
    String businessOnMapAddress;
    String businessCategory;
    String businessProfileURL;

    public getpartnerclass(){};

    public getpartnerclass(String businessPartnerID, String businessCreator, String businessName, String businessRegister, String businessLastUpdate, String businessPhone, String businessEmail, String businessLatitude, String businessLongitude, String businessActualAddress, String businessOnMapAddress, String businessCategory, String businessProfileURL) {
        this.businessPartnerID = businessPartnerID;
        this.businessCreator = businessCreator;
        this.businessName = businessName;
        this.businessRegister = businessRegister;
        this.businessLastUpdate = businessLastUpdate;
        this.businessPhone = businessPhone;
        this.businessEmail = businessEmail;
        this.businessLatitude = businessLatitude;
        this.businessLongitude = businessLongitude;
        this.businessActualAddress = businessActualAddress;
        this.businessOnMapAddress = businessOnMapAddress;
        this.businessCategory = businessCategory;
        this.businessProfileURL = businessProfileURL;
    }

    public String getBusinessPartnerID() {
        return businessPartnerID;
    }

    public void setBusinessPartnerID(String businessPartnerID) {
        this.businessPartnerID = businessPartnerID;
    }

    public String getBusinessCreator() {
        return businessCreator;
    }

    public void setBusinessCreator(String businessCreator) {
        this.businessCreator = businessCreator;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessRegister() {
        return businessRegister;
    }

    public void setBusinessRegister(String businessRegister) {
        this.businessRegister = businessRegister;
    }

    public String getBusinessLastUpdate() {
        return businessLastUpdate;
    }

    public void setBusinessLastUpdate(String businessLastUpdate) {
        this.businessLastUpdate = businessLastUpdate;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }

    public String getBusinessLatitude() {
        return businessLatitude;
    }

    public void setBusinessLatitude(String businessLatitude) {
        this.businessLatitude = businessLatitude;
    }

    public String getBusinessLongitude() {
        return businessLongitude;
    }

    public void setBusinessLongitude(String businessLongitude) {
        this.businessLongitude = businessLongitude;
    }

    public String getBusinessActualAddress() {
        return businessActualAddress;
    }

    public void setBusinessActualAddress(String businessActualAddress) {
        this.businessActualAddress = businessActualAddress;
    }

    public String getBusinessOnMapAddress() {
        return businessOnMapAddress;
    }

    public void setBusinessOnMapAddress(String businessOnMapAddress) {
        this.businessOnMapAddress = businessOnMapAddress;
    }

    public String getBusinessCategory() {
        return businessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        this.businessCategory = businessCategory;
    }

    public String getBusinessProfileURL() {
        return businessProfileURL;
    }

    public void setBusinessProfileURL(String businessProfileURL) {
        this.businessProfileURL = businessProfileURL;
    }



}
