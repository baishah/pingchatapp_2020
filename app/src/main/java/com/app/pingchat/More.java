package com.app.pingchat;

public class More {

    private String btnName;
    private int thumbnail;

    public More()
    {

    }

    public More(String btnName, int thumbnail) {
        this.btnName = btnName;
        this.thumbnail = thumbnail;
    }

    public String getBtnName() {
        return btnName;
    }

    public int getThumbnail() {
        return thumbnail;
    }
}
