package com.app.pingchat.Chat;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class pdfGAdapter extends RecyclerView.Adapter<pdfGAdapter.MyViewHolder> {
    Context context;
    ArrayList<Messages_G> mediaGList;

    public pdfGAdapter(Context c, ArrayList<Messages_G> f){
        context = c;
        mediaGList = f;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(context).inflate(R.layout.pdf_item,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull pdfGAdapter.MyViewHolder holder, int position) {
        final Messages_G msg = mediaGList.get(position);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        System.out.println("mediaList_adapter:"+mediaGList.size());
        holder.pdfv.setText(msg.getCG_mediaURL());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                StorageReference pdfreceiver = storageRef.child(msg.getCG_mediaURL());
                pdfreceiver.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(uri.toString()));
                        context.startActivity(intent);
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mediaGList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView pdfv;
        public MyViewHolder(View itemView) {
            super(itemView);
            pdfv = itemView.findViewById(R.id.pdf_r);
        }
    }
}
