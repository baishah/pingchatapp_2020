package com.app.pingchat.Chat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.GlideApp;
import com.app.pingchat.PlayVideoActivity;
import com.app.pingchat.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class videoAdapter extends RecyclerView.Adapter<videoAdapter.MyViewHolder> {
    Context context;
    ArrayList<Messages> mediaList;

    public videoAdapter(Context c, ArrayList<Messages> f){
        context = c;
        mediaList = f;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(context).inflate(R.layout.post_image_inprofile,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Messages msg = mediaList.get(position);
//        String msgType = msg.getCP_mediaType();
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        System.out.println("mediaList_adapter:"+mediaList.size());

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(msg.getCP_mediaThumbnailURL());
        GlideApp.with(context.getApplicationContext())
                .load(storageReference)
                .placeholder(circularProgressDrawable)
                .into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PlayVideoActivity.class);
                i.putExtra("test",msg.getCP_mediaURL());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.postimage);
        }
    }
}
