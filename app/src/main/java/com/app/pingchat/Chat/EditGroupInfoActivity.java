package com.app.pingchat.Chat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.EditProfileActivity;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.app.pingchat.filterActivitity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class EditGroupInfoActivity  extends BottomSheetDialogFragment {

    private BottomSheetListener mListener;

    private static final String TAG = "EditProfile";

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }

//    ImageView backbtn;
    private CircleImageView disppic;
    EditText editdisplayname, descrip_text;
    Button done;
    private DatabaseReference userinfo,chats;
    FirebaseAuth mAuth;
    String current_user,chatidintent,key,uid;
    StorageReference storageRef;
    FirebaseStorage storage;
    private byte[] avatar;
    public StorageReference groupimg;
    public static Context contextOfApplication;
    String dpicture;
    ProgressDialog dialog;
    TextView changepic, editusername, editemail;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.edit_group_info, container, false);

        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
        groupimg = FirebaseStorage.getInstance().getReference().child("groupProfileImage");

        dialog = new ProgressDialog(getContext());

        current_user = mAuth.getCurrentUser().getUid();
        userinfo = FirebaseDatabase.getInstance().getReference().child("Users");
        userinfo.keepSynced(true);
        chats = FirebaseDatabase.getInstance().getReference().child("Chats");

        chatidintent = getArguments().getString("chatId");
        key = getArguments().getString("key");
        uid = getArguments().getString("uid");

        contextOfApplication = getContext();

        editdisplayname = v.findViewById(R.id.display_name);
        descrip_text = v.findViewById(R.id.descrip_text);
        disppic = v.findViewById(R.id.profile_photo);
        changepic = v.findViewById(R.id.changeProfilePhoto);
        done = v.findViewById(R.id.saveChanges);

//        chatidintent = getIntent().getStringExtra("chatId");
//        key = getIntent().getStringExtra("key");
//        uid = getIntent().getStringExtra("uid");

        changepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
                }
                else{
                    CropImage.startPickImageActivity(getContext(),EditGroupInfoActivity.this);
                }
            }
        });

//        backbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                back();
//            }
//        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editdone(chatidintent);
            }
        });

        System.out.println("chatid_group:"+chatidintent);
        getUserInfo(chatidintent);
        return v;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(getContext(),this);
        }
    }

    private void getUserInfo(String chatid){
        chats.child("Group").child(chatid).child("CG_groupDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    if(dataSnapshot.hasChild("CG_groupName")){
                        String name = dataSnapshot.child("CG_groupName").getValue().toString();
                        editdisplayname.setText(name);
                    }
                    if(dataSnapshot.hasChild("CG_groupDescription")){
                        String gdes = dataSnapshot.child("CG_groupDescription").getValue().toString();
                        descrip_text.setText(gdes);
                        System.out.println("group des:"+gdes);
//                        groupDes.setSelection(groupDes.getText().length());
                    }
                    if(dataSnapshot.hasChild("CG_groupProfileURL")){
                        String groupPic = dataSnapshot.child("CG_groupProfileURL").getValue().toString();
                        if(groupPic.startsWith("h")){
                            GlideApp.with(getContext())
                                    .load(groupPic)
                                    .into(disppic);
                        }
                        else{
                            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(groupPic);
                            GlideApp.with(getContext())
                                    .load(storageReference)
                                    .into(disppic);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void editdone(String chatid){
        dialog.setMessage("Updating profile on progress\nPlease wait");
        dialog.show();
        final String descript = descrip_text.getText().toString().trim();
        final String dname = editdisplayname.getText().toString().trim();

        if (descript != null && dname != null){
            chats.child("Group").child(chatid).child("CG_groupDetails").child("CG_groupName").setValue(dname).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        chats.child("Group").child(chatid).child("CG_groupDetails").child("CG_groupDescription").setValue(descript).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    uploadNewImage();
                                    dialog.dismiss();
                                    dismiss();
                                }
                            }
                        });
                    }
                }
            });
        }else{
            chats.child("Group").child(chatid).child("CG_groupDetails").child("CG_groupName").setValue(dname).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        chats.child("Group").child(chatid).child("CG_groupDetails").child("CG_groupDescription").setValue(descript).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    dialog.dismiss();
                                    dismiss();
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private void uploadNewImage() {
//        dialog.setMessage("Profile photo changed successfully");
//        dialog.show();
        if (getAvatar() != null) {
            String key = chats.push().getKey();
            System.out.println("new_id:"+key);
            final StorageReference filepath = groupimg.child(chatidintent+key+".jpg");
            String namadidatabase = chatidintent + key + ".jpg";
            UploadTask uploadTask = filepath.putBytes(getAvatar());
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                    dialog.dismiss();
                    Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    chats.child("Group").child(chatidintent).child("CG_groupDetails").child("CG_groupProfileURL").setValue("groupProfileImage/"+namadidatabase).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
//                            Toast.makeText(getContext(),"Update profile changed successfully...", Toast.LENGTH_SHORT).show();
//                            dialog.dismiss();
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = contextOfApplication.getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    disppic.setImageBitmap(selectedImage);
                    setAvatar(datas);
                    System.out.println("avatar changed"+getAvatar());

                    Intent i = new Intent(getContext(), filterActivitity.class);
                    i.putExtra("Image",datas);
                    startActivityForResult(i,100);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == 100){
            if(resultCode == RESULT_OK){
                byte[] bytes = data.getByteArrayExtra("PUBLIC_STRING_IDENTIFIER");
                Bitmap b = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                setAvatar(bytes);
                disppic.setImageBitmap(b);
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(getContext(),data);

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(getContext(),this);
        }
    }


    public void back(){
        dismiss();
    }

    public byte[] getAvatar() { return avatar; }
    public void setAvatar(byte[] avatar) { this.avatar = avatar; }

    public interface BottomSheetListener {
        void onClickImage(String text);
    }

}
