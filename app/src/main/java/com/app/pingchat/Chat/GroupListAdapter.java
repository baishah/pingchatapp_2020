package com.app.pingchat.Chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    Context context;
    ArrayList<Groups> groupsArrayList;


    String urll;

    public GroupListAdapter(ArrayList<Groups> groupList){
        groupsArrayList = groupList;
    }

    public GroupListAdapter(Context c, ArrayList<Groups> g){
        context = c;
        groupsArrayList = g;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.chat_list_card,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();

        final String currentuser = mAuth.getCurrentUser().getUid();

        final Groups myGroups = groupsArrayList.get(position);
        holder.u.setText(myGroups.getGroupName());
        holder.s.setText(myGroups.getLastMessage());
        holder.t.setText(myGroups.getDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(myGroups.getGroupProfileURL());
        GlideApp.with(context.getApplicationContext())
                .load(storageReference)
                .into(holder.i);
    }

    @Override
    public int getItemCount() {
        return groupsArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s,p,t;
        CircleImageView i;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.name);
            s = itemView.findViewById(R.id.message);
            i = itemView.findViewById(R.id.gambar);
            t = itemView.findViewById(R.id.time);
        }
    }
    public void filterList(ArrayList<Groups> filteredlist){
        groupsArrayList = filteredlist;
        notifyDataSetChanged();
    }

}
