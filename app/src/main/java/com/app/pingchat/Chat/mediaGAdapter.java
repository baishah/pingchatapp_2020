package com.app.pingchat.Chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import afu.org.checkerframework.checker.nullness.qual.NonNull;

public class mediaGAdapter extends RecyclerView.Adapter<mediaGAdapter.MyViewHolder> {
    Context context;
    ArrayList<Messages_G> mediaGList;

    public mediaGAdapter(Context c,ArrayList<Messages_G> f2){
        context = c;
        mediaGList = f2;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(context).inflate(R.layout.post_image_inprofile,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final Messages_G msgG = mediaGList.get(position);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        String msgGType = msgG.getCG_mediaType();
//        if (msgGType.equals("image")){
        holder.image.setVisibility(View.VISIBLE);
        if(msgG.getCG_mediaURL().startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(msgG.getCG_mediaURL())
                    .placeholder(circularProgressDrawable)
                    .into(holder.image);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(msgG.getCG_mediaURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .placeholder(circularProgressDrawable)
                    .into(holder.image);
        }
//        }

    }

    @Override
    public int getItemCount() {
        return mediaGList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.postimage);
        }
    }
}

