package com.app.pingchat.Chat;

import java.util.Comparator;
import java.util.Date;

public class ChatListClass {
    public String chat_id,chat_type,last_chat_id,message_timestamp,date,friendid,displayname,dp,lastmessage;
    Date datetocompare;

    public ChatListClass(String message_timestamp,String chat_id, String chat_type, String last_chat_id) {
        this.chat_id = chat_id;
        this.chat_type = chat_type;
        this.last_chat_id = last_chat_id;
        this.message_timestamp = message_timestamp;
    }

    public ChatListClass(){}

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getChat_type() {
        return chat_type;
    }

    public void setChat_type(String chat_type) {
        this.chat_type = chat_type;
    }

    public String getLast_chat_id() {
        return last_chat_id;
    }

    public void setLast_chat_id(String last_chat_id) {
        this.last_chat_id = last_chat_id;
    }

    public String getMessage_timestamp() {
        return message_timestamp;
    }

    public void setMessage_timestamp(String message_timestamp) {
        this.message_timestamp = message_timestamp;
    }

    public Date getDatetocompare() {
        return datetocompare;
    }

    public void setDatetocompare(Date datetocompare) {
        this.datetocompare = datetocompare;
    }

    public String getDate() {
        return date;
    }

    public String getFriendid() {
        return friendid;
    }

    public void setFriendid(String friendid) {
        this.friendid = friendid;
    }

    public String getDisplayname() {
        return displayname;
    }

    public String getDp() { return dp; }

    public void setDp(String dp) { this.dp = dp; }

    public String getLastmessage() { return lastmessage; }

    public void setLastmessage(String lastmessage) { this.lastmessage = lastmessage; }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public static Comparator<ChatListClass> chatdatecomparator1 = new Comparator<ChatListClass>() {
        @Override
        public int compare(ChatListClass o1, ChatListClass o2) {
            return o2.getDatetocompare().compareTo(o1.getDatetocompare());
        }
    };
}
