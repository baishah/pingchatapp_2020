package com.app.pingchat.Chat;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Friend.FriendsAdapter;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.R;
import com.app.pingchat.UserAuth.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class ChatMenuActivity extends AppCompatActivity implements View.OnClickListener {

    FirebaseAuth mAuth;
    private DatabaseReference friends,users,userFriends;
    private ListView listView;
    private String current_id;
    private RecyclerView friendList;
    private EditText search_friend;
    private ImageView back;

    ArrayList<userDetailsClass> mContactslist;
    FriendsAdapter friendsAdapter;

    FirebaseStorage storage;
    StorageReference storeRef;


    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_menu_layout);

        RelativeLayout newgroup = findViewById(R.id.new_group);
        search_friend = findViewById(R.id.search_friends);
        back = findViewById(R.id.backbutton);

        newgroup.setOnClickListener(this);

        back.setOnClickListener(this);

        friendList = findViewById(R.id.friendlistR);
        friendList.setLayoutManager(new LinearLayoutManager(this));

        mContactslist = new ArrayList<userDetailsClass>();

        search_friend.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onStart(){
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser()!=null) {
            current_id = mAuth.getCurrentUser().getUid();
            friends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_id).child("userFriends");
            friends.keepSynced(true);
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            storage = FirebaseStorage.getInstance();
            storeRef = storage.getReference();
            displayfriend();

        }
    }

    public void onClick(View view){
        switch(view.getId()){
            case R.id.backbutton:
                finish();
                break;

            case R.id.new_group:
                startActivity(new Intent(ChatMenuActivity.this, GroupActivity.class));
                break;
        }
    }

    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    public void displayfriend(){
        mContactslist = new ArrayList<>();
        friends.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){

                }
                else{
                    for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        final userDetailsClass fren = new userDetailsClass();
                        final String friendid = dataSnapshot1.getKey().toString();
                        final String status = dataSnapshot1.getValue().toString();
                        users.child(friendid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(status.equals("friend")){
                                    userDetailsClass f = dataSnapshot.getValue(userDetailsClass.class);
                                    final String avatarurl = f.getUD_avatarURL();
                                    final String displayname = f.getUD_displayName();
                                    final String userabout = f.getUD_aboutMe();
                                    final String id = f.getUD_userID();

                                    System.out.println("avatar url"+avatarurl);
                                    fren.setUD_displayName(displayname);
                                    fren.setUD_aboutMe(userabout);
                                    fren.setUD_avatarURL(avatarurl);
                                    fren.setUD_userID(id);
                                    mContactslist.add(fren);
                                    friendsAdapter = new FriendsAdapter(ChatMenuActivity.this,mContactslist);
                                    friendList.setAdapter(friendsAdapter);
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void filter(String text){
        ArrayList<userDetailsClass> filteredlist = new ArrayList<>();
        if(mContactslist.size()!=0){
            for(int i=0;i<mContactslist.size();i++)
            {
                Log.e("list","friend "+mContactslist.get(i).getUD_displayName() );
            }

            for (userDetailsClass item:mContactslist){
                if(item.getUD_displayName().toLowerCase().contains(text.toLowerCase())){
                    filteredlist.add(item);
                }
            }
            friendsAdapter.filterList(filteredlist);
        }
        else{
            System.out.println("kenapa ada sini?" );
        }
    }
}

