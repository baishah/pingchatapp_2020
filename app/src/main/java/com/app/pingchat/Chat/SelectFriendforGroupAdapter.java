package com.app.pingchat.Chat;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SelectFriendforGroupAdapter extends RecyclerView.Adapter<SelectFriendforGroupAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,notification;
    Context context;
    ArrayList<userDetailsClass> mContactslist;

    SparseBooleanArray sparseBooleanArray;
    String yuyu;

    public ArrayList<String> itemArrayList = new ArrayList<>();
    private selectedrecyclerlistadapter mclicklistener;

    public SelectFriendforGroupAdapter(ArrayList<userDetailsClass> Friendlist){
        mContactslist = Friendlist;
    }

    public SelectFriendforGroupAdapter(Context c, ArrayList<userDetailsClass> f,selectedrecyclerlistadapter clicklistener, SparseBooleanArray sparseBooleanArray1){
        context = c;
        mContactslist = f;
        sparseBooleanArray = sparseBooleanArray1;
        this.mclicklistener = clicklistener;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.select_friend_for_group_layout,parent,false);
        return new MyViewHolder(v,mclicklistener);
       // return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.select_friend_for_group_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();

        final userDetailsClass mynewfriend = mContactslist.get(position);
        holder.u.setText(mynewfriend.getUD_displayName());
        holder.s.setText(mynewfriend.getUD_aboutMe());

        if(mynewfriend.getUD_avatarURL().startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(mynewfriend.getUD_avatarURL())
                    .into(holder.i);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(mynewfriend.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }


//        if(mynewfriend.getUserID().equals(getselected())){
//            holder.done.setVisibility(View.VISIBLE);
//            holder.selected.setVisibility(View.VISIBLE);
//        }

        if(sparseBooleanArray.get(position)){
            holder.done.setVisibility(View.VISIBLE);
            holder.selected.setVisibility(View.VISIBLE);
        }
        else{
            holder.done.setVisibility(View.INVISIBLE);
            holder.selected.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return mContactslist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ImageView done,selected;
        TextView u,s,p;
        CircleImageView i;
        Button add;
        selectedrecyclerlistadapter mclicklistener;
        public MyViewHolder(View itemView, selectedrecyclerlistadapter clicklistener) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            i = itemView.findViewById(R.id.gambar);
            done = itemView.findViewById(R.id.bg_done);
            selected = itemView.findViewById(R.id.selected);
            itemView.setOnClickListener(this);
            mclicklistener = clicklistener;

        }

        @Override
        public void onClick(View v) {
            mclicklistener.onUserClicked(getAdapterPosition());
        }

//        @Override
//        public void onClick(View view) {
//            if (!sparseBooleanArray.get(getAdapterPosition()))
//            {
//                sparseBooleanArray.put(getAdapterPosition(),true);
//                itemArrayList.add(mContactslist.get(getAdapterPosition()).getUserID());
//                Log.e("itemArrayList " ,"----"+itemArrayList);
//                setArraylist(itemArrayList);
//                notifyItemChanged(getAdapterPosition());
//            }
//            else // if clicked item is already selected
//            {
//                sparseBooleanArray.put(getAdapterPosition(),false);
//                itemArrayList.remove(mContactslist.get(getAdapterPosition()).getUserID());
//                Log.e("itemArrayList " ,"----"+itemArrayList);
//                setArraylist(itemArrayList);
//                notifyItemChanged(getAdapterPosition());
//            }
//
//        }
    }
    public void filterList(ArrayList<userDetailsClass> filteredlist){
        mContactslist = filteredlist;
        notifyDataSetChanged();
    }

    public void setArraylist(ArrayList<String> set){
        this.itemArrayList = set;
    }

    public ArrayList<String> getItemArraylist() {
        System.out.println("dalam getitemarraylist"+itemArrayList);
        return itemArrayList;
    }

    public void lebih3(){
        if(getItemArraylist().size()==3){
            System.out.println("lebih dari 3");
        }

        else{
            System.out.println("x lebih 3");
        }
    }

    public interface selectedrecyclerlistadapter{
        void onUserClicked(int position);
    }

    public int getposition(String valuea){
        int length = mContactslist.size();
        for(int i=0;i<length;i++){
            if(mContactslist.get(i).getUD_userID().equals(valuea)){
                return i;
            }
        }
        return -1;
    }

    public void setselected(String s){
        this.yuyu = s;
    }

    public String getselected(){
        return yuyu;
    }

}
