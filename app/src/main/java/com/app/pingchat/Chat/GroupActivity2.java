package com.app.pingchat.Chat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.CompleteCreateGroup;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupActivity2 extends AppCompatActivity implements View.OnClickListener{

    RecyclerView selectedfriends;
    SelectFriendforGroupAdapter selectFriendforGroupAdapter;
    com.app.pingchat.Chat.getselectedfriend2adapter getselectedfriend2adapter;
    SelectFriendforGroupAdapter.selectedrecyclerlistadapter selectedrecyclerlistadapter;
    getselectedfriend2adapter.getselectedrecyclerlistadapter getselectedrecyclerlistadapter;
    SparseBooleanArray s = new SparseBooleanArray();
    public static Context contextOfApplication;

    FirebaseAuth mAuth;
    DatabaseReference groupchatsessiondb,chatsdb,userdb;
    StorageReference groupprofile;

    CircleImageView group_image;
    private byte[] avatar;
    String groupname,groupsubject;
    EditText group_name,group_subject;

    ArrayList<userDetailsClass> selectedfriend;
    String currentuserid;

    String currentusername;
    ProgressBar progressBar;

    ImageView backbutton;


    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_2_layout);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        selectedfriend = (ArrayList<userDetailsClass>) args.getSerializable("ARRAYLIST");

        System.out.println("passing serializeable arraylist "+selectedfriend.size());

        contextOfApplication = getApplicationContext();

        mAuth = FirebaseAuth.getInstance();
        currentuserid = mAuth.getCurrentUser().getUid();
        System.out.println("currentuserid"+currentuserid);
        groupchatsessiondb = FirebaseDatabase.getInstance().getReference().child("GroupChatSessions");
        chatsdb = FirebaseDatabase.getInstance().getReference().child("Chats");
        userdb = FirebaseDatabase.getInstance().getReference().child("Users");
        groupprofile = FirebaseStorage.getInstance().getReference().child("groupProfileImage");

        getcurrentusernamefromdb(currentuserid);

        TextView num_participant = findViewById(R.id.partici_num);
        group_name = findViewById(R.id.user_name_);
        group_subject = findViewById(R.id.group_subject);
        ImageButton next = findViewById(R.id.ok_button);
        group_image = findViewById(R.id.group_image);
        backbutton = findViewById(R.id.backbutton);

        group_image.setOnClickListener(this);
        next.setOnClickListener(this);
        backbutton.setOnClickListener(this);

        groupname = group_name.getText().toString();
        group_name.setText(groupname);
        groupsubject = group_subject.getText().toString();
        group_subject.setText(groupsubject);
        progressBar = findViewById(R.id.progresssbar);

        num_participant.setText(String.valueOf(selectedfriend.size()));

        selectedfriends = findViewById(R.id.friendlistR);

        selectedfriends.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        selectedfriends.smoothScrollToPosition(selectedfriend.size()-1);

        getselectedfriend2adapter = new getselectedfriend2adapter(this,selectedfriend,getselectedrecyclerlistadapter);
        selectedfriends.setAdapter(getselectedfriend2adapter);

        selectedrecyclerlistadapter = new SelectFriendforGroupAdapter.selectedrecyclerlistadapter() {
            @Override
            public void onUserClicked(int position) {
                System.out.println("tekan selectedrecyclerlistadapter");
            }
        };
    }

    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = contextOfApplication.getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
                    // userprofileimage.setVisibility(View.VISIBLE);
                    group_image.setImageBitmap(selectedImage);
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    setAvatar(datas);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(this,data);
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 786 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CropImage.startPickImageActivity(GroupActivity2.this);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.group_image:
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},786);
                }
                else{
                    CropImage.startPickImageActivity(GroupActivity2.this);
                }
                break;

            case R.id.ok_button:
                senddatatodatabase();
                break;

            case R.id.backbutton:
                finish();
                break;
        }

    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public String getCurrentusername() {
        return currentusername;
    }

    public void setCurrentusername(String currentusername) {
        this.currentusername = currentusername;
    }

    public void getcurrentusernamefromdb(String id){
        userdb.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UD_displayName")){
                    String displayname = dataSnapshot.child("UD_displayName").getValue().toString();
                    setCurrentusername(displayname);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void senddatatodatabase(){
        groupname = group_name.getText().toString();
        groupsubject = group_subject.getText().toString();
        String new_chat_groupid = chatsdb.push().getKey();
        String new_messageid = chatsdb.push().push().getKey();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String groupprofileurl = "groupProfileImage/"+new_chat_groupid+".jpg";

        HashMap new_group_details = new HashMap();
        new_group_details.put("CG_chatID",new_chat_groupid);
        new_group_details.put("CG_createdOn",timestamp.getTime());
        new_group_details.put("CG_groupName",groupname);
        new_group_details.put("CG_groupDescription",groupsubject);
        new_group_details.put("CG_groupProfileURL",groupprofileurl);
        new_group_details.put("CG_lastMessage",getCurrentusername()+" created a group");
        new_group_details.put("CG_lastMessageID",new_messageid);
        new_group_details.put("CG_lastMessageSenderID",currentuserid);
        new_group_details.put("CG_lastMessageTimestamp",timestamp.getTime());
        new_group_details.put("CG_lastMessageMediaType","notifier");

        HashMap group_chat = new HashMap();
        group_chat.put("CG_message",getCurrentusername()+" created a group");
        group_chat.put("CG_messageSenderID",currentuserid);
        group_chat.put("CG_mediaType","notifier");
        group_chat.put("CG_messageTimestamp",timestamp.getTime());

        HashMap group_member_friend = new HashMap();
        group_member_friend.put("CG_role","member");
        group_member_friend.put("CG_joinedDate",timestamp.getTime());

        HashMap group_member = new HashMap();
        group_member.put("CG_role","owner");
        group_member.put("CG_joinedDate",timestamp.getTime());

        HashMap user_conv = new HashMap();
        user_conv.put("UCG_status",true);
        user_conv.put("UCG_role","owner");
        user_conv.put("UCG_joinDate",timestamp.getTime());
        user_conv.put("UCG_lastSeenMessageID",new_messageid);
        user_conv.put("UCG_lastMessageID",new_messageid);

        HashMap user_conv_friend = new HashMap();
        user_conv_friend.put("UCG_status",true);
        user_conv_friend.put("UCG_role","member");
        user_conv_friend.put("UCG_joinDate",timestamp.getTime());
        user_conv_friend.put("UCG_lastSeenMessageID",new_messageid);
        user_conv_friend.put("UCG_lastMessageID",new_messageid);

        if((getAvatar()!=null) && (groupname !=null && !groupname.isEmpty())){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating your group...");
        progressDialog.show();

        StorageReference filepath = groupprofile.child(new_chat_groupid+".jpg");
        UploadTask uploadTask = filepath.putBytes(getAvatar());

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                chatsdb.child("Group").child(new_chat_groupid).child("CG_groupDetails").setValue(new_group_details);
                chatsdb.child("Group").child(new_chat_groupid).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);
                chatsdb.child("Group").child(new_chat_groupid).child("CG_groupMember").child(currentuserid).setValue(group_member);
                 for(userDetailsClass j : selectedfriend){
                    chatsdb.child("Group").child(new_chat_groupid).child("CG_groupMember").child(j.getUD_userID()).setValue(group_member_friend);
                    userdb.child(j.getUD_userID()).child("userConversations").child("Group").child(new_chat_groupid).setValue(user_conv_friend);
                }
                userdb.child(currentuserid).child("userConversations").child("Group").child(new_chat_groupid).setValue(user_conv).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            progressDialog.dismiss();
                            Intent newIntent = new Intent(GroupActivity2.this, CompleteCreateGroup.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);
                        }
                    }
                });
            }
        });
        }
        else{
            Toast.makeText(GroupActivity2.this, "Please fill in all information", Toast.LENGTH_SHORT).show();
        }
    }
}

