package com.app.pingchat.Chat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.CompleteCreateGroup;
import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.MarketItem;
import com.app.pingchat.MarketplaceActivity;
import com.app.pingchat.NearbyMarketAdapter;
import com.app.pingchat.Profile.FriendListAdapter;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class addNewMember extends BottomSheetDialogFragment implements View.OnClickListener, addMemberAdapter.OnAddListener{
    private BottomSheetListener mListener;
    private RecyclerView friendList,selectedFriendList;
    ArrayList<userDetailsClass> mContactslist;
    ArrayList<userDetailsClass> memberlist;
    ArrayList<userDetailsClass> newList;
    FirebaseAuth mAuth;
    private DatabaseReference users,friends,chats,chat;
    FriendListAdapter friendsAdapter;
    private TextView next;
    private String current_id,currentusername;
    FirebaseStorage storage;
    StorageReference storeRef;
    ImageView nofriend;
    private RequestQueue mQueue;
    addMemberAdapter addmemberAdapter;

    private ArrayList <String> list_of_member = new ArrayList();

    private ArrayList<userDetailsClass> itemarraylist=new ArrayList<>();

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.show_friend_list, container, false);

        nofriend = v.findViewById(R.id.nocontacts);
//        next = v.findViewById(R.id.done);
        friendList = (RecyclerView) v.findViewById(R.id.friendL);
        selectedFriendList = v.findViewById(R.id.selected);
        friendList.setLayoutManager(new LinearLayoutManager(getContext()));
        selectedFriendList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));

        mAuth = FirebaseAuth.getInstance();
        mQueue = Volley.newRequestQueue(getContext());
        if(mAuth.getCurrentUser()!=null) {
            current_id = mAuth.getCurrentUser().getUid();
//            friends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_id).child("userFriends");
//            friends.keepSynced(true);
            chats = FirebaseDatabase.getInstance().getReference().child("Chats");
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            chat = users.child(current_id).child("userConversations").child("Group");
            chat.keepSynced(true);
            storage = FirebaseStorage.getInstance();
            storeRef = storage.getReference();
        }

        String key = getArguments().getString("key");
        getNonMember(key,current_id);
        getMember(key);

//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sendDataMember(key,itemarraylist);
//            }
//        });

        return v;
    }

    public ArrayList getMember(String chatid){
        chats.child("Group").child(chatid).child("CG_groupMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String member = dataSnapshot1.getKey();
                    list_of_member.add(member);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        return  list_of_member;
    }

    private void getNonMember(String chatId, String currentid){
//        ProgressDialog dialog= new ProgressDialog(getContext());
//        dialog.setMessage("Loading contacts..");
//        dialog.show();
        mContactslist = new ArrayList<>();
        ArrayList checkstatus = new ArrayList();
        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/users/"+currentid+"/groupChat/"+chatId+"/nonmember";

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley:", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("notMember");

                            for (int i = 0; i < jsonArray.length(); i++){
                                final userDetailsClass userDetailsClass = new userDetailsClass();
//                                JSONObject id = jsonArray.getJSONObject(i);
                                String idU = jsonArray.getString(i);
                                System.out.println("iduser:"+idU);

                                users.child(idU).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        userDetailsClass udc = dataSnapshot.getValue(userDetailsClass.class);
                                        userDetailsClass.setUD_userID(udc.getUD_userID());
                                        userDetailsClass.setUD_avatarURL(udc.getUD_avatarURL());
                                        userDetailsClass.setUD_aboutMe(udc.getUD_aboutMe());
                                        userDetailsClass.setUD_phoneNo(udc.getUD_phoneNo());
                                        userDetailsClass.setUD_displayName(udc.getUD_displayName());
                                        userDetailsClass.setUD_birthday(udc.getUD_birthday());
                                        userDetailsClass.setUD_email(udc.getUD_email());
                                        mContactslist.add(userDetailsClass);

                                        addmemberAdapter = new addMemberAdapter(getContext(),mContactslist,currentid,chatId,addNewMember.this::onAddClick);
                                        friendList.setAdapter(addmemberAdapter);
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);
    }

    public String getCurrentusername() {
        return currentusername;
    }

    public void setCurrentusername(String currentusername) {
        this.currentusername = currentusername;
    }

    public void getcurrentusernamefromdb(String id){
        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UD_displayName")){
                    String displayname = dataSnapshot.child("UD_displayName").getValue().toString();
                    setCurrentusername(displayname);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
//    public void removeItem(int position) {
//        itemarraylist.remove(position);
//        getselectedfriendadapter.notifyItemRemoved(position);
//    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.one:
                mListener.onButtonaddMember("recyclersss");
                dismiss();
                break;
        }
    }

    @Override
    public void onAddClick(int position, String chatID) {
        mContactslist.get(position);
        System.out.println("onAddClick: clicked");
        String clickId = mContactslist.get(position).getUD_userID();
        String name = mContactslist.get(position).getUD_displayName();
//        System.out.println("onAddClick:" +current_id);

        String new_messageid = chat.push().push().getKey();
        users.child(current_id).child("userConversations").child("Group").child(chatID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UCG_lastMessageID")){
                    String lastmessageid = dataSnapshot.child("UCG_lastMessageID").getValue().toString();
                    System.out.println("mssageid:"+lastmessageid);
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                    HashMap group_member_friend = new HashMap();
                    group_member_friend.put("CG_role","member");
                    group_member_friend.put("CG_joinedDate",timestamp.getTime());

                    HashMap user_conv_friend = new HashMap();
                    user_conv_friend.put("UCG_status",true);
                    user_conv_friend.put("UCG_role","member");
                    user_conv_friend.put("UCG_joinDate",timestamp.getTime());
                    user_conv_friend.put("UCG_lastMessageID",new_messageid);
                    user_conv_friend.put("UCG_lastSeenMessageID",new_messageid);

                    chats.child("Group").child(chatID).child("CG_groupMember").child(clickId).setValue(group_member_friend);
                    users.child(clickId).child("userConversations").child("Group").child(chatID).setValue(user_conv_friend).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            HashMap group_chat = new HashMap();
                            group_chat.put("CG_message",name+" added");
                            group_chat.put("CG_messageSenderID",current_id);
                            group_chat.put("CG_mediaType","status");
                            group_chat.put("CG_messageTimestamp",timestamp.getTime());

                            chats.child("Group").child(chatID).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);
//                                        usersRef.child(userId).child("userConversations").child("Group").child(chatId).child("UCG_lastMessageID").setValue(new_messageid);
                            chats.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessage").setValue(name+" added");
                            chats.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
                            chats.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
                            chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageSenderID").setValue(current_id);
                            for(String i:list_of_member){
                                users.child(i).child("userConversations").child("Group").child(chatID).child("UCG_lastMessageID").setValue(new_messageid);
//                                                          users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
                            }

                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        Toast.makeText(getContext(), "Success..", Toast.LENGTH_SHORT).show();
        dismiss();
    }

    public interface BottomSheetListener {
        void onButtonaddMember(String text);
    }
}