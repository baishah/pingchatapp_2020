package com.app.pingchat.Chat;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.app.pingchat.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class bottomSheetChatMenu extends BottomSheetDialogFragment implements View.OnClickListener {


    private BottomSheetListener mListener;

    public void setListener(bottomSheetChatMenu.BottomSheetListener listener) {
        this.mListener = listener;
    }

    private Button docs_btn,cam_btn,gallery_btn,loc_btn,audio_btn,contact_btn, mic_btn;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.chat_item_layout, container, false);

        docs_btn = v.findViewById(R.id.docs_btn);
        cam_btn = v.findViewById(R.id.cam_btn);
        gallery_btn = v.findViewById(R.id.gallery_btn);
        loc_btn = v.findViewById(R.id.loc_btn);
        audio_btn = v.findViewById(R.id.audio_btn);
        contact_btn= v.findViewById(R.id.contct_btn);

        docs_btn.setOnClickListener(this);
        cam_btn.setOnClickListener(this);
        gallery_btn.setOnClickListener(this);
        loc_btn.setOnClickListener(this);
        audio_btn.setOnClickListener(this);
        contact_btn.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.docs_btn:
                mListener.onButtonChatClicked("docs_btn");
                dismiss();
                break;
            case R.id.cam_btn:
                mListener.onButtonChatClicked("cam_btn");
                dismiss();
                break;
            case R.id.gallery_btn:
                mListener.onButtonChatClicked("gallery_btn");
                dismiss();
                break;
            case R.id.loc_btn:
                mListener.onButtonChatClicked("loc_btn");
                dismiss();
                break;
            case R.id.audio_btn:
                mListener.onButtonChatClicked("audio_btn");
                dismiss();
                break;
            case R.id.contct_btn:
                mListener.onButtonChatClicked("contct_btn");
                dismiss();
                break;
        }
    }

    public interface BottomSheetListener {
        void onButtonChatClicked(String text);
    }

}
