package com.app.pingchat.Chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.CitizenAlert.UCA_Details;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static java.security.AccessController.getContext;

public class ChatInfoPersonalActivity extends AppCompatActivity {

    private static final String TAG = "ChatInfoActivity";

    private DatabaseReference users, chats, chat;
    private CircleImageView userphoto;
    private ImageView media_btn;
    private String currenct, userPic,chatidintent,userid;
    private byte[] avatar;
    private FirebaseAuth Mauth;
    private StorageReference userImage;
    private TextView mediatxt, create_on, memberNum;
    private EditText userName_, userDes;
    private Button doneBtn,showBtn,backbtn;
    private RecyclerView listofmember,media_group_;
    private  ArrayList memberlist = new ArrayList();
    private ArrayList<Messages> mediaList = new ArrayList<>();
    private GroupMemberAdapter groupMemberAdapter;
    private LinearLayout discnophoto;
    private Context context;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_info_layout);

        userName_ = findViewById(R.id.user_name_);
        userphoto = findViewById(R.id.user_photo);
        showBtn = findViewById(R.id.showMedia);
//        create_on = findViewById(R.id.date_created);
//        memberNum = findViewById(R.id.num_participant);
        userDes = findViewById(R.id.descrip_text);
        discnophoto = findViewById(R.id.no_post_profile);
        backbtn = findViewById(R.id.arrow);

//        listofmember = findViewById(R.id.friendlistM);
        media_btn = findViewById(R.id.next_bt);
        mediatxt = findViewById(R.id.media_txt_);
        media_group_ = findViewById(R.id.media_group_);
        context = this;
        chatidintent = getIntent().getStringExtra("chatid");
        userid = getIntent().getStringExtra("userid");
        // doneBtn = findViewById(R.id.saveChanges);


        Mauth = FirebaseAuth.getInstance();
        if (Mauth.getCurrentUser() != null) {
            currenct = Mauth.getCurrentUser().getUid();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            chat = FirebaseDatabase.getInstance().getReference().child("Chats");
            chats = users.child(currenct).child("userConversations").child("Group");
            chats.keepSynced(true);
            userImage = FirebaseStorage.getInstance().getReference().child("profile");
            media_group_.setLayoutManager(new GridLayoutManager(getApplicationContext(),3));
//            listofmember.setLayoutManager(new LinearLayoutManager(context));
            getpersonalinfo();
            //getmember();
            getMedia();
        }

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ShowAllMedia.class);
                i.putExtra("chatId",chatidintent);
                i.putExtra("key","personal");
                i.putExtra("uid",userid);
                startActivity(i);
            }
        });
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getMedia() {
        mediaList = new ArrayList<>();
        chat.child("Personal").child(chatidintent).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("not exist");
                }else{
                    DataSnapshot personalSession = dataSnapshot.child("CP_personalChatSession");
                    System.out.println("child:"+personalSession);
                    if(personalSession.exists()){
                        for (DataSnapshot dataSnapshot2 : personalSession.getChildren()) {
                            final Messages mesagge = new Messages();
                            String msgId = dataSnapshot2.getKey();
//                                System.out.println("msgId:"+msgId);
                            chat.child("Personal").child(chatidintent).child("CP_personalChatSession").child(msgId).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                        for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                    String msgType = dataSnapshot.child("CP_mediaType").getValue().toString();
//                                            System.out.println("msgType:"+msgType);
                                    Messages m = dataSnapshot.getValue(Messages.class);
                                    System.out.println("mesage:"+m);
                                    if(msgType.equals("image")){
//                                                            String mediaType = m.getCP_mediaType();
//                                                            System.out.println("mediaType:"+mediaType);
                                        String mediaUrl = m.getCP_mediaURL();
                                        mesagge.setCP_mediaType(msgType);
                                        mesagge.setCP_mediaURL(mediaUrl);

                                        mediaList.add(mesagge);
//                                                            System.out.println("mediaPList:"+mediaList.size());
                                        mediaAdapter adapter = new mediaAdapter(getApplicationContext(),mediaList);
                                        media_group_.setAdapter(adapter);
                                    }
                                    if(mediaList.isEmpty()){
                                        discnophoto.setVisibility(View.VISIBLE);
                                    }else{
                                        discnophoto.setVisibility(View.GONE);

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void getpersonalinfo() {
        users.child(userid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if ((dataSnapshot.exists())) {
                    if(dataSnapshot.hasChild("UD_displayName")){
                        String gname = dataSnapshot.child("UD_displayName").getValue().toString();
                        userName_.setText(gname);
                        userName_.setSelection(userName_.getText().length());
                        System.out.println("name:"+gname);
                    }
                    if(dataSnapshot.hasChild("UD_aboutMe")){
                        String gdes = dataSnapshot.child("UD_aboutMe").getValue().toString();
                        userDes.setText(gdes);
                        userDes.setSelection(userDes.getText().length());
                    }
                    if(dataSnapshot.hasChild("UD_avatarURL")){
                        userPic = dataSnapshot.child("UD_avatarURL").getValue().toString();
//                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(userPic);
//                        GlideApp.with(getApplicationContext())
//                                .load(storageReference)
//                                .into(userphoto);
                        if(userPic.startsWith("h")){
                            GlideApp.with(context.getApplicationContext())
                                    .load(userPic)
                                    .placeholder(R.drawable.loading_img)
                                    .into(userphoto);
                        }
                        else{
                            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(userPic);
                            GlideApp.with(context.getApplicationContext())
                                    .load(storageReference)
                                    .placeholder(R.drawable.loading_img)
                                    .into(userphoto);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

//    private void uploadNewImage() {
////        dialog.setMessage("Profile photo changed successfully");
////        dialog.show();
//        if (getAvatar() != null) {
//            String key = userinfo.push().getKey();
//            final StorageReference filepath = groupImage.child(current_user + key + ".jpg");
//            String namadidatabase = current_user + key + ".jpg";
//            UploadTask uploadTask = filepath.putBytes(getAvatar());
//            uploadTask.addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
////                    dialog.dismiss();
//                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    chat.child("Group").child(chatId).child("CG_groupDetails").child("CG_groupName").child("CG_groupProfileURL").setValue("profile/" + namadidatabase).addOnSuccessListener(new OnSuccessListener<Void>() {
//                        @Override
//                        public void onSuccess(Void aVoid) {
//                            Toast.makeText(getApplicationContext(), "Update profile changed successfully...", Toast.LENGTH_SHORT).show();
////                            dialog.dismiss();
//                        }
//                    });
//                }
//            });
//
//        }
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                final Uri imageUri = result.getUri();
//                final InputStream imageStream;
//                try {
//                    imageStream = contextOfApplication.getContentResolver().openInputStream(imageUri);
//                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it
//                    byte[] datas = byteArrayOutputStream.toByteArray();
//                    groupPhoto.setImageBitmap(selectedImage);
//                    setAvatar(datas);
//                    System.out.println("avatar changed" + getAvatar());
//
//                    Intent i = new Intent(this, filterActivitity.class);
//                    i.putExtra("Image", datas);
//                    startActivityForResult(i, 100);
//
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
//            }
//        } else if (requestCode == 100) {
//            if (resultCode == Activity.RESULT_OK) {
//                byte[] bytes = data.getByteArrayExtra("PUBLIC_STRING_IDENTIFIER");
//                Bitmap b = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
//                setAvatar(bytes);
//                groupPhoto.setImageBitmap(b);
//            }
//        } else if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//
//            CropImage.activity(imageUri)
//                    .setGuidelines(CropImageView.Guidelines.ON)
//                    .setCropShape(CropImageView.CropShape.OVAL)
//                    .setBorderLineColor(Color.BLUE)
//                    .setBorderLineThickness(1)
//                    .setFixAspectRatio(true)
//                    .start(this);
//        }
//    }
//
//
//    public void back() {
//        finish();
//    }
//
//    public byte[] getAvatar() {
//        return avatar;
//    }
//
//    public void setAvatar(byte[] avatar) {
//        this.avatar = avatar;
//    }


}