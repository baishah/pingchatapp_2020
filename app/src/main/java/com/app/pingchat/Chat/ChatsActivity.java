package com.app.pingchat.Chat;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.renderscript.ScriptIntrinsicBLAS;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Ping.PingSetup;
import com.app.pingchat.PlayVideoActivity;
import com.app.pingchat.R;
import com.app.pingchat.ViewImageActivity;
import com.app.pingchat.filterActivitity;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatsActivity extends AppCompatActivity  implements bottomSheetChatMenu.BottomSheetListener ,MediaPlayer.OnPreparedListener{

    private static final String TAG = "ChatsAcitivty";
    private static final int RECORD_REQUEST_CODE = 200;
    private static final int WRITE_REQUEST_CODE = 123;

    private EditText editText;
    private String friendid,currentid,chatIDkey, imageID;
    public String chatIdkeyouter,y;
    private ImageView sendmessage,preview_image,preview_docs;
    private ImageButton plusbutton, sendbutton,micbutton;
    public DatabaseReference usersFriends,chats,personalchat,groupchat,friendofUser,notificationmessage,users;
    public StorageReference messageimage;
    private ListView listView;
    private FirebaseAuth mAuth;
    private bottomSheetChatMenu bottomSheetChatMenu;
    private byte[] avatar;
    private String messageType;
    public StorageReference chatsstorage;
    private Uri fileuri;
    private Uri videouri;
    private Button backBtn;

    private static final int GALLERY_PICK = 1;

    ArrayList<Messages> list;
    Messages messages;
    private FirebaseListAdapter<Messages> adapter;
    private FirebaseListAdapter<Messages_G> adapter2;
    CircleImageView frienddp;

    FirebaseStorage storage;

    String datetimes;
    String datess;
    String day,name,image,type,chatid;

    public static Context contextOfApplication;
    String chatidintent;

    private ArrayList <String> list_of_member = new ArrayList();
    private String new_chatid ;
    private String new_messageid ;


    private MediaRecorder recorder;
    private String fileName = null;
    private static final String LOG_TAG = "Record_log";

    private static int oTime =0, sTime =0, eTime =0, fTime = 5000, bTime = 5000;
    private Handler hdlr = new Handler();
    private Runnable runnable;
    private int mediaFileLength;
    private int realtimeLength;
    private int resumePosition;

    private boolean isSpeakButtonLongPressed = false;

    private long startTime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chats_activity);
        editText = findViewById(R.id.editText);
        plusbutton = findViewById(R.id.plus_button);
        sendbutton =  findViewById(R.id.send_button);
        micbutton = findViewById(R.id.mic_btn);
        preview_image = findViewById(R.id.previewimage);
        preview_docs = findViewById(R.id.previewdoc);
        frienddp = findViewById(R.id.frienddp);
        backBtn = findViewById(R.id.back_btn_chat);
        name = getIntent().getStringExtra("name");
        image = getIntent().getStringExtra("avatarurl");
        type = getIntent().getStringExtra("type");
        friendid =getIntent().getStringExtra("friendid");
        chatidintent = getIntent().getStringExtra("chatid");
        mAuth = FirebaseAuth.getInstance();

        Toolbar toolbar = findViewById(R.id.toolbarmessage);
        setMessageType("text");
        System.out.println("CHAT ID "+chatidintent);
        System.out.println("avatar:"+image);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // requestAudioPermission();
            //requestStoragePermission();
            checkPermissions();
            getCameraPermissions();
        }

        fileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        fileName += "/" + UUID.randomUUID().toString()+".mp3";

        if(image.startsWith("h")){
            GlideApp.with(getApplicationContext())
                    .load(image)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .into(frienddp);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(image);
            GlideApp.with(getApplicationContext())
                    .load(storageReference)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .into(frienddp);
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent newIntent = new Intent(ChatsActivity.this, ChatsFragment.class);
//                newIntent.putExtra("chatid",chatidintent);
//                startActivity(newIntent);
            }
        });

        frienddp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("group")){
                    Intent newIntent = new Intent(ChatsActivity.this, ChatInfoActivity.class);
                    newIntent.putExtra("chatid",chatidintent);
                    startActivity(newIntent);
                }
                else if(type.equals("personal")){
                    Intent newIntent = new Intent(ChatsActivity.this, ChatInfoPersonalActivity.class);
                    newIntent.putExtra("userid",friendid);
                    newIntent.putExtra("chatid",chatidintent);
                    startActivity(newIntent);
                }
            }
        });

        chats = FirebaseDatabase.getInstance().getReference().child("Chats");
        chats.keepSynced(true);
        personalchat = FirebaseDatabase.getInstance().getReference().child("Chats").child("Personal");
        personalchat.keepSynced(true);
        groupchat = FirebaseDatabase.getInstance().getReference().child("Chats").child("Group");
        groupchat.keepSynced(true);
        users = FirebaseDatabase.getInstance().getReference().child("Users");
        users.keepSynced(true);
        notificationmessage = FirebaseDatabase.getInstance().getReference().child("Notification").child("NewMessage");
        messageimage = FirebaseStorage.getInstance().getReference().child("messageImage");
        chatsstorage = FirebaseStorage.getInstance().getReference().child("chats");

        bottomSheetChatMenu = new bottomSheetChatMenu();
        bottomSheetChatMenu.setListener(this);

//        micbutton.setOnLongClickListener(speakHoldListener);
//        micbutton.setOnTouchListener(speakTouchListener);

        new_chatid = personalchat.push().getKey();

        list = new ArrayList<>();
        messages = new Messages();

        listView = findViewById(R.id.messages_view);
//        editText = findViewById(R.id.editText);

        toolbar.setTitle(name);
        if(type.equals("personal")){
            users.child(friendid).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    DataSnapshot userstatus = dataSnapshot.child("userStatus");
                    String on9status = userstatus.child("US_onlineStatus").getValue().toString();
                    System.out.println("on9status"+on9status);
                    if(on9status.equals("online")){
                        toolbar.setSubtitle("online");
                    }else{
                        toolbar.setSubtitle("");
                       System.out.println("user offline");
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        }
        toolbar.setElevation(1);

        setSupportActionBar(toolbar);


        if(mAuth.getCurrentUser()!=null){
            currentid = mAuth.getCurrentUser().getUid();
            updatebeforeclose("online");
            loadmessage(chatidintent);
        }

        plusbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetChatMenu.show(getSupportFragmentManager(),"chat_menu");
            }
        });

        sendbutton.setEnabled(false);

        micbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSpeakButtonLongPressed = false;
//                if (v.performLongClick()) {
                // view perform long click
//                } else {
                Toast.makeText(ChatsActivity.this, "Hold button to make voice message.", Toast.LENGTH_LONG).show();
//                }
            }
        });

        micbutton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isSpeakButtonLongPressed = true;
                float x = (float) 1.50;
                float y = (float) 1.50;

                micbutton.setScaleX(x);
                micbutton.setScaleY(y);
                micbutton.setBackgroundResource(R.drawable.round_blue_mic);
//                recordTimeText.setVisibility(View.VISIBLE);
                startRecording();
                return true;
            }
        });

        micbutton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (isSpeakButtonLongPressed) {
                        // Do something when the button is released.
                        isSpeakButtonLongPressed = false;
                        float x = 1;
                        float y = 1;

                        micbutton.setScaleX(x);
                        micbutton.setScaleY(y);
                        micbutton.setBackgroundResource(R.drawable.round_blue_mic);
                        stopRecording();
                    }
//                    recordTimeText.setVisibility(View.GONE);
                }
                return false;
            }
        });

        MediaPlayer mediaPlayer = new MediaPlayer();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(editText.length() == 0) {
                    sendbutton.setEnabled(false);
                    //Toast.makeText(ChatsActivity.this, "Cannot send an empty message", Toast.LENGTH_SHORT).show();
                }
                else{
                    sendbutton.setEnabled(true);

                }
            }
        });
    }

    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
        recorder.setOutputFile(fileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

//        startTime = SystemClock.uptimeMillis();
//        timer = new Timer();
//        MyTimerTask myTimerTask = new MyTimerTask();
//        timer.schedule(myTimerTask, 1000, 1000);
        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
        recorder.start();
    }

//    class MyTimerTask extends TimerTask {
//        @Override
//        public void run() {
//            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
//            updatedTime = timeSwapBuff + timeInMilliseconds;
//            final String hms = String.format(
//                    "%02d:%02d",
//                    TimeUnit.MILLISECONDS.toMinutes(updatedTime)
//                            - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
//                            .toHours(updatedTime)),
//                    TimeUnit.MILLISECONDS.toSeconds(updatedTime)
//                            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
//                            .toMinutes(updatedTime)));
//            long lastsec = TimeUnit.MILLISECONDS.toSeconds(updatedTime)
//                    - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
//                    .toMinutes(updatedTime));
//            System.out.println(lastsec + " hms " + hms);
//            runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//                    try {
//                        if (recordTimeText != null)
//                            recordTimeText.setText(hms);
//                    } catch (Exception e) {
//                        // TODO: handle exception
//                    }
//
//                }
//            });
//        }
//    }

    private void stopRecording() {

        recorder.stop();
        recorder.release();
        recorder = null;

        onStart();
        if(type.equals("personal")){
            send_chat_audio(chatidintent);
        }
        else if(type.equals("group")){
            send_group_audio(chatidintent);
        }

//        if (timer != null) {
//            timer.cancel();
//        }
//        if (recordTimeText.getText().toString().equals("00:00")) {
//            return;
//        }
//        recordTimeText.setText("00:00");
    }

    public void recordVideo(){
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(intent,101);
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if(resultCode==RESULT_OK){
                final Uri imageUri = result.getUri();
                final InputStream imageStream;
                try {
                    imageStream = getApplicationContext().getContentResolver().openInputStream(imageUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream); //compress it

                    setMessageType("image");
                    plusbutton.setVisibility(View.GONE);
                    preview_image.setVisibility(View.VISIBLE);
                    preview_image.setImageBitmap(selectedImage);
                    byte[] datas = byteArrayOutputStream.toByteArray();
                    setAvatar(datas);

                    Intent i = new Intent(this, filterActivitity.class);
                    i.putExtra("Image",datas);
                    startActivityForResult(i,100);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                Exception error = result.getError();
            }
        }
        else if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getPickImageResultUri(this,data);
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    .setBorderLineColor(Color.BLUE)
                    .setBorderLineThickness(1)
                    .setFixAspectRatio(true)
                    .start(this);
        }
        else if(requestCode == 100){
            if(resultCode == Activity.RESULT_OK){
                byte[] bytes = data.getByteArrayExtra("PUBLIC_STRING_IDENTIFIER");
                Bitmap b = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                setAvatar(bytes);
                plusbutton.setVisibility(View.GONE);
                preview_image.setImageBitmap(b);
            }
        }
        else if(requestCode == 438 && resultCode == RESULT_OK && data!=null && data.getData()!=null){
            setFileuri(data.getData());
            setMessageType("pdf");
            plusbutton.setVisibility(View.GONE);
            preview_docs.setVisibility(View.VISIBLE);
        }
        else if(requestCode==101){
            if(resultCode == RESULT_OK){
                Uri videouri = data.getData();
                String picturePath = getPath( getApplicationContext(), videouri );
                Log.d("Picture Path", picturePath);

                setMessageType("video");
                plusbutton.setVisibility(View.GONE);
                preview_image.setVisibility(View.VISIBLE);

                setVideouri(videouri);
//               GlideApp.with(this)
//                       .asBitmap()
//                       .load(Uri.fromFile(new File(picturePath)))
//                       .into(preview_image);

                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(picturePath,
                        MediaStore.Images.Thumbnails.MINI_KIND);
                preview_image.setImageBitmap(thumb);
                preview_image.setDrawingCacheEnabled(true);
                preview_image.buildDrawingCache();
                Bitmap bitmap = ((BitmapDrawable) preview_image.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] datasii = baos.toByteArray();
                setAvatar(datasii);

            }
            else if(resultCode == RESULT_CANCELED){
                Toast.makeText(this,"Videouri cancelled",Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(this,"Failed to record video",Toast.LENGTH_LONG).show();
            }
        }
    }

    private Bitmap createThumbnailAtTime(String filePath, int timeInSeconds){
        MediaMetadataRetriever mMMR = new MediaMetadataRetriever();
        mMMR.setDataSource(filePath);
        //api time unit is microseconds
        return mMMR.getFrameAtTime(timeInSeconds*1000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
    }


    private void loadmessage(String chatid){
        if(type.equals("personal")){
            displaypersonalmsg(chatid);
        }
        else {
            displaygroupmsg(chatid);
            getMember(chatid);
        }
    }

    public static String getPath( Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Video.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }


    public void displaypersonalmsg(String chatid){
        Query query = chats.child("Personal").child(chatid).child("CP_personalChatSession");
        FirebaseListOptions<Messages> options = new FirebaseListOptions.Builder<Messages>()
                .setQuery(query,Messages.class)
                .setLayout(R.layout.chat_right)
                .build();
        adapter = new FirebaseListAdapter<Messages>(options) {
            @Override
            protected void populateView(@NonNull View v, final @NonNull Messages model, int position) {
                TextView sender = v.findViewById(R.id.sender);
                TextView receiver =  v.findViewById(R.id.receiver);
                TextView sendertime = v.findViewById(R.id.sendertime);
                TextView receivertime = v.findViewById(R.id.receivertime);
                TextView sendertime2 = v.findViewById(R.id.sendertime2);
                TextView sendertime3 = v.findViewById(R.id.sendertime3);
                TextView sendertime4 = v.findViewById(R.id.sendertime4);
                TextView sendertime5 = v.findViewById(R.id.sendertime5);
                TextView receivertime2 = v.findViewById(R.id.receivertime2);
                TextView receivertime3 = v.findViewById(R.id.receivertime3);
                TextView receivertime4 = v.findViewById(R.id.receivertime4);
                TextView receivertime5 = v.findViewById(R.id.receivertime5);
                ImageView sendPhoto =  v.findViewById(R.id.sendpicture);
                ImageView receivePhoto =  v.findViewById(R.id.receivepicture);
                ImageView sendVideo = v.findViewById(R.id.sendvideo);
                ImageView receivevideo = v.findViewById(R.id.receivevideo);
                SeekBar seekbarsend = v.findViewById(R.id.sendseekBar);
                SeekBar seekbarrcv = v.findViewById(R.id.rcvseekBar);
                ImageButton playsend = v.findViewById(R.id.playBtn1);
                ImageButton playrcv = v.findViewById(R.id.playBtn);
                ImageButton downsend = v.findViewById(R.id.pausesend);
                ImageButton downrcv = v.findViewById(R.id.pausercv);
                ImageButton playbacksend = v.findViewById(R.id.playbcksend);
                ImageButton playbackrcv = v.findViewById(R.id.playbckrcv);
                TextView sendseektime = v.findViewById(R.id.sendseektime);
                TextView rcvseektime = v.findViewById(R.id.rcvseektime);

                TextView captionsenderpdf = v.findViewById(R.id.captionsenderpdf);
                TextView captionreceiverpdf = v.findViewById(R.id.captionreceiverpdf);
                TextView notifier = v.findViewById(R.id.notifier);
                TextView captionsender = v.findViewById(R.id.captionsender);
                TextView captionreceiver = v.findViewById(R.id.captionreceiver);
                TextView captionsendervideo = v.findViewById(R.id.captionsenderv);
                TextView captionreceivevideo = v.findViewById(R.id.captionreceiverv);


                RelativeLayout receivePhotoLayout =  v.findViewById(R.id.receivepictureLO);
                RelativeLayout sendPhotoLayout =  v.findViewById(R.id.sendpictureLO);
                RelativeLayout sendPDFLayout = v.findViewById(R.id.sendpdfLO);
                RelativeLayout receiverPDFLayout = v.findViewById(R.id.receiverpdfLO);
                RelativeLayout sendAudioLayout = v.findViewById(R.id.sendaudioLO);
                RelativeLayout rcvAudioLayout = v.findViewById(R.id.receiveraudioLO);
                RelativeLayout sendVideoLayout = v.findViewById(R.id.sendvideoLO);
                RelativeLayout receiveVideoLayout = v.findViewById(R.id.receivevideoLO);

                ImageButton video = v.findViewById(R.id.playvideo);
                ImageButton videor = v.findViewById(R.id.playvideor);

                video.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getApplicationContext(), PlayVideoActivity.class);
                        i.putExtra("test",model.getCP_mediaURL());
                        startActivity(i);
                    }
                });

                videor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getApplicationContext(), PlayVideoActivity.class);
                        i.putExtra("test",model.getCP_mediaURL());
                        startActivity(i);
                    }
                });

                receivePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gotoimageview(model.getCP_mediaURL());
                    }
                });

                sendPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gotoimageview(model.getCP_mediaURL());
                    }
                });

                String senderID = model.getCP_messageSenderID();
                sender.setText(model.getCP_message());
                receiver.setVisibility(View.INVISIBLE);
                receivertime.setVisibility(View.INVISIBLE);

                final String[] url = new String[1];
                final MediaPlayer mediaPlayer = new MediaPlayer();

                playsend.setOnClickListener(new View.OnClickListener() {
                    final ProgressDialog mDialog = new ProgressDialog(ChatsActivity.this);
                    @Override
                    public void onClick(View view) {
                        mDialog.setMessage("Please wait");
                        mDialog.show();
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        if (url[0] == null){
                            StorageReference audiostorage = FirebaseStorage.getInstance().getReference().child(model.getCP_mediaURL());
                            audiostorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    playAudio(String.valueOf(uri));
                                    url[0] = String.valueOf(uri);
                                    eTime = mediaPlayer.getDuration();
                                    sendseektime.setText(String.format("%dmin:%dsec", TimeUnit.MILLISECONDS.toMinutes(eTime),
                                            TimeUnit.MILLISECONDS.toSeconds(eTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(eTime))));
                                }
                            })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.i("TAG", e.getMessage());
                                        }
                                    });
                        }else {
                            playAudio(url[0]);
                            mDialog.dismiss();
                        }
                    }
                    private void playAudio(String uri)

                    {
                        if (uri != null) {
                            mDialog.dismiss();
                            downsend.setVisibility(View.VISIBLE);
                            playsend.setVisibility(View.GONE);
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            try {
                                Log.d(TAG, "onClick: "+uri);
                                mediaPlayer.reset();
                                mediaPlayer.setDataSource(uri);
                                mediaPlayer.setOnPreparedListener(ChatsActivity.this);

                                mediaPlayer.prepare();
                                eTime = mediaPlayer.getDuration();
                                sTime = mediaPlayer.getCurrentPosition();
                                seekbarsend.setMax(eTime);
                                seekbarsend.setProgress(sTime);

                                mediaPlayer.start();
                                UpdateSongTime.run();
                                //   Toast.makeText(getApplicationContext(), "Uri:" + uri, Toast.LENGTH_SHORT).show();

                                if(mediaPlayer.isPlaying())
                                {
                                    hdlr.postDelayed(UpdateSongTime, 100);

                                }else {
                                    hdlr.removeCallbacks(UpdateSongTime);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    private Runnable UpdateSongTime = new Runnable() {
                        @Override
                        public void run() {
                            if (null != mediaPlayer) {
                                sTime = mediaPlayer.getCurrentPosition();
                                sendseektime.setText(String.format("%dmin:%dsec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                                        TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
                                seekbarsend.setProgress(sTime);
                                hdlr.postDelayed(this, 100);
                            }
                        }
                    };

                });

                seekbarsend.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(mediaPlayer != null && fromUser){
                            mediaPlayer.seekTo(progress * 100);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });

                downsend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                            resumePosition = mediaPlayer.getCurrentPosition();
                            playbacksend.setVisibility(View.VISIBLE);
                            downsend.setVisibility(View.GONE);
                        }
                    }
                });
                playbacksend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mediaPlayer!=null && !mediaPlayer.isPlaying()) {
                            mediaPlayer.seekTo(resumePosition);
                            mediaPlayer.start();
                            playbacksend.setVisibility(View.GONE);
                            downsend.setVisibility(View.VISIBLE);
                        }
                    }
                });

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // do whatever you want
                        mediaPlayer.reset();
                        mediaPlayer.seekTo(0);
                        mediaPlayer.stop();
                        playsend.setVisibility(View.VISIBLE);
                        downsend.setVisibility(View.GONE);
                        playbacksend.setVisibility(View.GONE);
                        playrcv.setVisibility(View.VISIBLE);
                        downrcv.setVisibility(View.GONE);
                    }

                });

                playrcv.setOnClickListener(new View.OnClickListener() {
                    final ProgressDialog mDialog = new ProgressDialog(ChatsActivity.this);
                    @Override
                    public void onClick(View view) {
                        mDialog.setMessage("Please wait");
                        mDialog.show();
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        if (url[0] == null){
                            StorageReference audiostorage = FirebaseStorage.getInstance().getReference().child(model.getCP_mediaURL());
                            audiostorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
//                                    model.setCP_mediaURL(String.valueOf(uri));
                                    playAudio(String.valueOf(uri));
                                    url[0] = String.valueOf(uri);
                                }
                            })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.i("TAG", e.getMessage());
                                        }
                                    });
                        }else {
                            playAudio(String.valueOf(url[0]));
                            mDialog.dismiss();
                        }
                    }
                    private void playAudio(String uri)
                    {
                        if (uri != null) {
                            mDialog.dismiss();
                            downrcv.setVisibility(View.VISIBLE);
                            playrcv.setVisibility(View.GONE);
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            try {
                                Log.d(TAG, "onClick: "+uri);
                                mediaPlayer.reset();
                                mediaPlayer.setDataSource(uri);
                                mediaPlayer.setOnPreparedListener(ChatsActivity.this);

                                mediaPlayer.prepare();
                                eTime = mediaPlayer.getDuration();
                                sTime = mediaPlayer.getCurrentPosition();
                                seekbarrcv.setMax(eTime);
                                seekbarrcv.setProgress(sTime);

                                if(mediaPlayer.isPlaying())
                                {
                                    hdlr.postDelayed(UpdateSongTime, 100);
                                }else {

                                    hdlr.removeCallbacks(UpdateSongTime);
                                }
                                mediaPlayer.start();
                                UpdateSongTime.run();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            // Toast.makeText(getApplicationContext(), "Uri:" + uri, Toast.LENGTH_SHORT).show();
                        }
                    }
                    private Runnable UpdateSongTime = new Runnable() {
                        @Override
                        public void run() {
                            if (null != mediaPlayer) {
                                sTime = mediaPlayer.getCurrentPosition();
                                rcvseektime.setText(String.format("%dmin:%dsec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                                        TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
                                seekbarrcv.setProgress(sTime);
                                hdlr.postDelayed(this, 100);
                            }
                        }
                    };
                });
                seekbarrcv.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(mediaPlayer != null && fromUser){
                            mediaPlayer.seekTo(progress * 100);
                        }
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });

                downrcv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                            resumePosition = mediaPlayer.getCurrentPosition();
                            playbackrcv.setVisibility(View.VISIBLE);
                            downrcv.setVisibility(View.GONE);
                        }
                    }
                });
                playbackrcv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mediaPlayer!=null && !mediaPlayer.isPlaying()) {
                            mediaPlayer.seekTo(resumePosition);
                            mediaPlayer.start();
                            playbackrcv.setVisibility(View.GONE);
                            downrcv.setVisibility(View.VISIBLE);
                        }
                    }
                });

                if(model.getCP_mediaType().equals("notifier")){
                    sendPDFLayout.setVisibility(View.GONE);
                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime3.setVisibility(View.GONE);
                    receiverPDFLayout.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    seekbarsend.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    receivertime4.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    receiveVideoLayout.setVisibility(View.GONE);

                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);
                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);

                    notifier.setText(model.getCP_message());
                    notifier.setVisibility(View.VISIBLE);
                    sendertime5.setVisibility(View.GONE);
                    receivertime5.setVisibility(View.GONE);
                }

                else if(model.getCP_mediaType().equals("text")) {
                    sendPDFLayout.setVisibility(View.GONE);
                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime3.setVisibility(View.GONE);
                    receiverPDFLayout.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    receiveVideoLayout.setVisibility(View.GONE);

                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    seekbarsend.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    receivertime4.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);
                    receivertime5.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime3.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        receiver.setText(model.getCP_message());
                        receivertime.setText(getdatetime(model.getCP_messageTimestamp()));
                        receiver.setVisibility(View.VISIBLE);
                        receivertime.setVisibility(View.VISIBLE);
                    }
                    else{
                        sender.setVisibility(View.VISIBLE);
                        sendertime.setVisibility(View.VISIBLE);
                        sender.setText(model.getCP_message());
                        sendertime.setText(getdatetime(model.getCP_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                    }
                }
                else if(model.getCP_mediaType().equals("image")){
                    sendPDFLayout.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime3.setVisibility(View.GONE);
                    receiverPDFLayout.setVisibility(View.GONE);

                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    seekbarsend.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    receivertime4.setVisibility(View.GONE);

                    receiveVideoLayout.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);
                    receivertime5.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        captionreceiver.setVisibility(View.VISIBLE);
                        captionreceiver.setText(model.getCP_message());
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.GONE);
                        receivertime2.setText(getdatetime(model.getCP_messageTimestamp()));
                        receivertime2.setVisibility(View.VISIBLE);
                        sendertime3.setVisibility(View.INVISIBLE);
                        receivertime3.setVisibility(View.INVISIBLE);
                        sendPhoto.setVisibility(View.GONE);
                        sendPhotoLayout.setVisibility(View.GONE);
                        receivePhotoLayout.setVisibility(View.VISIBLE);
                        receivePhoto.setVisibility(View.VISIBLE);
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(model.getCP_mediaURL());
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                .into(receivePhoto);
                    }
                    else{
                        captionsender.setVisibility(View.VISIBLE);
                        captionsender.setText(model.getCP_message());
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.GONE);
                        sendertime2.setVisibility(View.VISIBLE);
                        sendertime2.setText(getdatetime(model.getCP_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        receivertime2.setVisibility(View.INVISIBLE);
                        sendPhoto.setVisibility(View.VISIBLE);
                        sendPhotoLayout.setVisibility(View.VISIBLE);
                        receivePhoto.setVisibility(View.GONE);
                        receivePhotoLayout.setVisibility(View.GONE);
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(model.getCP_mediaURL());
                        GlideApp.with(getApplicationContext()).load(storageReference) .diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(sendPhoto);
                    }
                }
                else if(model.getCP_mediaType().equals("pdf")){
                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);

                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    seekbarsend.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    receivertime4.setVisibility(View.GONE);

                    sendVideoLayout.setVisibility(View.GONE);
                    receiveVideoLayout.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    receiverPDFLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);
                    receivertime5.setVisibility(View.GONE);
                    receivertime3.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.GONE);
//                        receivertime2.setText(getdatetime(model.getCP_messageTimestamp()));
//                        receivertime2.setVisibility(View.VISIBLE);
                        sendPhoto.setVisibility(View.GONE);
                        sendPhotoLayout.setVisibility(View.GONE);
                        sendPDFLayout.setVisibility(View.GONE);
//                        receivePhotoLayout.setVisibility(View.VISIBLE);
//                        receivePhoto.setVisibility(View.VISIBLE);
                        captionreceiverpdf.setText(model.getCP_message());
                        receivertime3.setVisibility(View.VISIBLE);
                        receivertime3.setText(getdatetime(model.getCP_messageTimestamp()));
                        receiverPDFLayout.setVisibility(View.VISIBLE);
                        receiverPDFLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                System.out.println("url pdf"+model.getCP_mediaURL());
                                StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                                StorageReference pdfreceiver = storageRef.child(model.getCP_mediaURL());
                                pdfreceiver.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(uri.toString()));
                                        startActivity(intent);
                                    }
                                });
                            }
                        });
                    }
                    else{
                        captionsender.setVisibility(View.GONE);
                        captionsenderpdf.setText(model.getCP_message());
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.GONE);
                        sendertime2.setVisibility(View.GONE);
                        sendertime3.setVisibility(View.VISIBLE);
                        sendertime3.setText(getdatetime(model.getCP_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        sendPhoto.setVisibility(View.GONE);
                        sendPhotoLayout.setVisibility(View.GONE);
                        receivePhoto.setVisibility(View.GONE);
                        receivePhotoLayout.setVisibility(View.GONE);
                        receiverPDFLayout.setVisibility(View.GONE);
                        sendPDFLayout.setVisibility(View.VISIBLE);
                        sendPDFLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                System.out.println("url pdf"+model.getCP_mediaURL());
                                StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                                StorageReference pdfsender = storageRef.child(model.getCP_mediaURL());
                                pdfsender.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(uri.toString()));
                                        startActivity(intent);
                                    }
                                });
                            }
                        });
                    }
                }
                else if(model.getCP_mediaType().equals("audio")) {

                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime3.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    receivertime4.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);
                    seekbarrcv.setVisibility(View.GONE);
                    seekbarsend.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    receiverPDFLayout.setVisibility(View.GONE);

                    sendVideoLayout.setVisibility(View.GONE);
                    receiveVideoLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);
                    receivertime5.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        rcvAudioLayout.setVisibility(View.VISIBLE);
                        receivertime4.setText(getdatetime(model.getCP_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        receivertime4.setVisibility(View.VISIBLE);
                        seekbarsend.setVisibility(View.INVISIBLE);
                        seekbarrcv.setVisibility(View.VISIBLE);
                    }
                    else{
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.VISIBLE);
                        seekbarrcv.setVisibility(View.INVISIBLE);
                        seekbarsend.setVisibility(View.VISIBLE);
                        sendAudioLayout.setVisibility(View.VISIBLE);
                        sendertime4.setText(getdatetime(model.getCP_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                    }
                }
                else if(model.getCP_mediaType().equals("video")){
                    System.out.println("ini adalah video");
                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime3.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    receivertime4.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);
                    seekbarrcv.setVisibility(View.GONE);
                    seekbarsend.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    receiverPDFLayout.setVisibility(View.GONE);
                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);
                    //       sendVideoLayout.setVisibility(View.GONE);
                    //       receiveVideoLayout.setVisibility(View.GONE);
//                    sendertime5.setVisibility(View.GONE);
//                    receivertime5.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        sendVideoLayout.setVisibility(View.INVISIBLE);
                        sendertime5.setVisibility(View.INVISIBLE);
                        receiveVideoLayout.setVisibility(View.VISIBLE);
                        receivertime5.setVisibility(View.VISIBLE);
                        receivertime5.setText(getdatetime(model.getCP_messageTimestamp()));
                        captionreceivevideo.setText(model.getCP_message());
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(model.getCP_mediaThumbnailURL());
                        System.out.println("video receiver:"+ model.getCP_mediaThumbnailURL());
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                .into(receivevideo);
                    }
                    else{
                        sendPhotoLayout.setVisibility(View.INVISIBLE);
                        receiveVideoLayout.setVisibility(View.INVISIBLE);
                        receivertime5.setVisibility(View.INVISIBLE);
                        sendVideoLayout.setVisibility(View.VISIBLE);
                        sendertime5.setVisibility(View.VISIBLE);
                        sendertime5.setText(getdatetime(model.getCP_messageTimestamp()));
                        captionsendervideo.setText(model.getCP_message());
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(model.getCP_mediaThumbnailURL());
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                .into(sendVideo);
                    }
                }
                //   listView.setSelection(adapter.getCount()-1);
            }
        };
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        adapter.startListening();
    }

    public void displaygroupmsg(String chatid){
        Query query = chats.child("Group").child(chatid).child("CG_groupChatSession");
        FirebaseListOptions<Messages_G> options = new FirebaseListOptions.Builder<Messages_G>()
                .setQuery(query,Messages_G.class)
                .setLayout(R.layout.chat_right)
                .build();
        adapter2 = new FirebaseListAdapter<Messages_G>(options) {
            @Override
            protected void populateView(@NonNull View v, final @NonNull Messages_G model, int position) {
                TextView sender =  v.findViewById(R.id.sender);
                TextView receiver = v.findViewById(R.id.receiver);
                TextView notifier = v.findViewById(R.id.notifier);
                TextView sendertime = v.findViewById(R.id.sendertime);
                TextView receivertime = v.findViewById(R.id.receivertime);
                TextView sendertime2 = v.findViewById(R.id.sendertime2);
                TextView sendertime3 = v.findViewById(R.id.sendertime3);
                TextView sendertime4 = v.findViewById(R.id.sendertime4);
                TextView receivertime4 = v.findViewById(R.id.receivertime4);
                TextView receivertime2 = v.findViewById(R.id.receivertime2);
                TextView sendertime5 = v.findViewById(R.id.sendertime5);
                CircleImageView gsenderpic = v.findViewById(R.id.gsenderpic);
                TextView greceivetimepic = v.findViewById(R.id.groupreceivertimepic);
                TextView greceivetimepdf = v.findViewById(R.id.groupreceivertimepdf);
                TextView greceivetimevideo = v.findViewById(R.id.groupreceivertimevideo);
                TextView gmessagereceiver = v.findViewById(R.id.groupreceiver);
                TextView greceivertime = v.findViewById(R.id.groupreceivertime);
                ImageView sendPhoto =  v.findViewById(R.id.sendpicture);
                ImageView grpicture =  v.findViewById(R.id.grpicture);
                ImageView grpicturev = v.findViewById(R.id.grpicturev);
                ImageView receivePhoto =  v.findViewById(R.id.receivepicture);
                ImageView sendVideo = v.findViewById(R.id.sendvideo);

                SeekBar seekbarsend = v.findViewById(R.id.sendseekBar);
                SeekBar seekbarrcv = v.findViewById(R.id.grcvseekBar);
                TextView grcvauditime = v.findViewById(R.id.grcvaudiotime);
                CircleImageView gsenderaudipic = v.findViewById(R.id.gsenderpicaudi);
                ImageButton playsend = v.findViewById(R.id.playBtn1);
                ImageButton gplayrcv = v.findViewById(R.id.gplayBtn);
                ImageButton downrcv = v.findViewById(R.id.gpausercv);
                ImageButton downsend = v.findViewById(R.id.pausesend);
                ImageButton playbacksend = v.findViewById(R.id.playbcksend);
                ImageButton playbackrcv = v.findViewById(R.id.gplaybackrcv);
                TextView sendseektime = v.findViewById(R.id.sendseektime);
                TextView grcvseektime = v.findViewById(R.id.grcvseektime);

                RelativeLayout receivePhotoLayout =  v.findViewById(R.id.receivepictureLO);
                RelativeLayout sendPhotoLayout =  v.findViewById(R.id.sendpictureLO);
                RelativeLayout gaudiorcvLayout = v.findViewById(R.id.grcvaudioLO);
                RelativeLayout rcvAudioLayout = v.findViewById(R.id.receiveraudioLO);
                RelativeLayout sendAudioLayout = v.findViewById(R.id.sendaudioLO);
                RelativeLayout greceiver = v.findViewById(R.id.greceiver);
                RelativeLayout greceiverpic = v.findViewById(R.id.greceivepicture);
                RelativeLayout greceiverpdf = v.findViewById(R.id.greceivepdf);
                RelativeLayout greceivervideo = v.findViewById(R.id.greceivevideo);
                RelativeLayout sendVideoLayout = v.findViewById(R.id.sendvideoLO);

                TextView captionsenderg = v.findViewById(R.id.captionsender);
                TextView captionreceiverg = v.findViewById(R.id.captionreceiverg);
                TextView captionreceiverpdfg = v.findViewById(R.id.captionreceiverpdfg);
                RelativeLayout sendPDFLayout = v.findViewById(R.id.sendpdfLO);
                TextView captionsenderpdfg = v.findViewById(R.id.captionsenderpdf);
                TextView captionsendervideo = v.findViewById(R.id.captionsenderv);
                TextView captionreceivervg = v.findViewById(R.id.captionreceivervg);

                ImageButton video = v.findViewById(R.id.playvideo);
                ImageButton videog = v.findViewById(R.id.playvideog);
                final MediaPlayer mediaPlayer = new MediaPlayer();
                final String[] url = new String[1];

                video.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getApplicationContext(), PlayVideoActivity.class);
                        i.putExtra("test",model.getCG_mediaURL());
                        startActivity(i);
                    }
                });

                videog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getApplicationContext(), PlayVideoActivity.class);
                        i.putExtra("test",model.getCG_mediaURL());
                        startActivity(i);
                    }
                });

                playsend.setOnClickListener(new View.OnClickListener() {
                    final ProgressDialog mDialog = new ProgressDialog(ChatsActivity.this);
                    @Override
                    public void onClick(View view) {
                        mDialog.setMessage("Please wait");
                        mDialog.show();
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        if (url[0] == null){
                            StorageReference audiostorage = FirebaseStorage.getInstance().getReference().child(model.getCG_mediaURL());
                            audiostorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
//                                    model.setCP_mediaURL(String.valueOf(uri));
                                    playAudio(String.valueOf(uri));
                                    url[0] = String.valueOf(uri);
                                    eTime = mediaPlayer.getDuration();
                                    sendseektime.setText(String.format("%dmin:%dsec", TimeUnit.MILLISECONDS.toMinutes(eTime),
                                            TimeUnit.MILLISECONDS.toSeconds(eTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(eTime))));
                                }
                            })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.i("TAG", e.getMessage());
                                        }
                                    });
                        }else {
                            playAudio(url[0]);
                            mDialog.dismiss();
                        }
                    }
                    private void playAudio(String uri)

                    {
                        if (uri != null) {
                            mDialog.dismiss();
                            downsend.setVisibility(View.VISIBLE);
                            playsend.setVisibility(View.GONE);
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            try {
                                Log.d(TAG, "onClick: "+uri);
                                mediaPlayer.reset();
                                mediaPlayer.setDataSource(uri);
                                mediaPlayer.setOnPreparedListener(ChatsActivity.this);

                                mediaPlayer.prepare();
                                eTime = mediaPlayer.getDuration();
                                sTime = mediaPlayer.getCurrentPosition();
                                seekbarsend.setMax(eTime);
                                seekbarsend.setProgress(sTime);

                                mediaPlayer.start();
                                UpdateSongTime.run();
                                //   Toast.makeText(getApplicationContext(), "Uri:" + uri, Toast.LENGTH_SHORT).show();

                                if(mediaPlayer.isPlaying())
                                {
                                    hdlr.postDelayed(UpdateSongTime, 100);
                                }else {
                                    hdlr.removeCallbacks(UpdateSongTime);
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                    private Runnable UpdateSongTime = new Runnable() {
                        @Override
                        public void run() {
                            if (null != mediaPlayer) {
                                sTime = mediaPlayer.getCurrentPosition();
                                sendseektime.setText(String.format("%dmin:%dsec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                                        TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
                                seekbarsend.setProgress(sTime);
                                hdlr.postDelayed(this, 100);
                            }
                        }
                    };

                });

                seekbarsend.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(mediaPlayer != null && fromUser){
                            mediaPlayer.seekTo(progress * 100);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });

                downsend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                            resumePosition = mediaPlayer.getCurrentPosition();
                            playbacksend.setVisibility(View.VISIBLE);
                            downsend.setVisibility(View.GONE);
                        }
                    }
                });
                playbacksend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mediaPlayer!=null && !mediaPlayer.isPlaying()) {
                            mediaPlayer.seekTo(resumePosition);
                            mediaPlayer.start();
                            playbacksend.setVisibility(View.GONE);
                            downsend.setVisibility(View.VISIBLE);
                        }
                    }
                });

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // do whatever you want
                        mediaPlayer.seekTo(0);
                        mediaPlayer.stop();
                        playsend.setVisibility(View.VISIBLE);
                        downsend.setVisibility(View.GONE);
                        gplayrcv.setVisibility(View.VISIBLE);
                        downrcv.setVisibility(View.GONE);
                    }

                });

                gplayrcv.setOnClickListener(new View.OnClickListener() {
                    final ProgressDialog mDialog = new ProgressDialog(ChatsActivity.this);
                    @Override
                    public void onClick(View view) {
                        mDialog.setMessage("Please wait");
                        mDialog.show();
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        if (url[0] == null){
                            StorageReference audiostorage = FirebaseStorage.getInstance().getReference().child(model.getCG_mediaURL());
                            audiostorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    playAudio(String.valueOf(uri));
                                    url[0] = String.valueOf(uri);
                                }
                            })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.i("TAG", e.getMessage());
                                        }
                                    });
                        }else {
                            playAudio(String.valueOf(url[0]));
                            mDialog.dismiss();
                        }
                    }
                    private void playAudio(String uri)
                    {
                        if (uri != null) {
                            mDialog.dismiss();
                            downrcv.setVisibility(View.VISIBLE);
                            gplayrcv.setVisibility(View.GONE);
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            try {
                                Log.d(TAG, "onClick: "+uri);
                                mediaPlayer.reset();
                                mediaPlayer.setDataSource(uri);
                                mediaPlayer.setOnPreparedListener(ChatsActivity.this);

                                mediaPlayer.prepare();
                                eTime = mediaPlayer.getDuration();
                                sTime = mediaPlayer.getCurrentPosition();
                                seekbarrcv.setMax(eTime);
                                seekbarrcv.setProgress(sTime);

                                mediaPlayer.start();
                                UpdateSongTime.run();

                                //  Toast.makeText(getApplicationContext(), "Uri:" + uri, Toast.LENGTH_SHORT).show();

                                if(mediaPlayer.isPlaying())
                                {
                                    hdlr.postDelayed(UpdateSongTime, 100);
                                }else {
                                }


                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                        }
                    }
                    private Runnable UpdateSongTime = new Runnable() {
                        @Override
                        public void run() {
                            if (null != mediaPlayer) {

                                sTime = mediaPlayer.getCurrentPosition();
                                grcvseektime.setText(String.format("%dmin:%dsec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                                        TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
                                seekbarrcv.setProgress(sTime);
                                hdlr.postDelayed(this, 100);

                            }
                        }
                    };
                });
                seekbarrcv.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(mediaPlayer != null && fromUser){
                            mediaPlayer.seekTo(progress * 100);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });

                downrcv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                            resumePosition = mediaPlayer.getCurrentPosition();
                            playbackrcv.setVisibility(View.VISIBLE);
                            downrcv.setVisibility(View.GONE);
                        }
                    }
                });
                playbackrcv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mediaPlayer!=null && !mediaPlayer.isPlaying()) {
                            mediaPlayer.seekTo(resumePosition);
                            mediaPlayer.start();
                            playbackrcv.setVisibility(View.GONE);
                            downrcv.setVisibility(View.VISIBLE);
                        }
                    }
                });

                grpicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gotoimageview(model.getCG_mediaURL());
                    }
                });

                sendPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gotoimageview(model.getCG_mediaURL());
                    }
                });

                String senderID = model.getCG_messageSenderID();
                sender.setText(model.getCG_message());
                receiver.setVisibility(View.INVISIBLE);
                receivertime.setVisibility(View.INVISIBLE);

                if(model.getCG_mediaType().equals("notifier")) {
                    greceiverpdf.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);

                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);
                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);

                    greceiverpic.setVisibility(View.GONE);
                    greceivetimepic.setVisibility(View.GONE);

                    greceivertime.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    gmessagereceiver.setVisibility(View.GONE);

                    notifier.setText(model.getCG_message());
                    notifier.setVisibility(View.VISIBLE);
                    greceivetimepdf.setVisibility(View.GONE);

                    seekbarrcv.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    seekbarrcv.setVisibility(View.GONE);
                    gaudiorcvLayout.setVisibility(View.GONE);

                    receivertime4.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);

                    greceivervideo.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    greceivetimevideo.setVisibility(View.GONE);
                    gsenderaudipic.setVisibility(View.GONE);
                    grcvauditime.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        gsenderpic.setVisibility(View.INVISIBLE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        receiver.setText(model.getCG_message());
                        receivertime.setText(getdatetime(model.getCG_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        gsenderaudipic.setVisibility(View.INVISIBLE);
                        grcvauditime.setVisibility(View.INVISIBLE);

                    }
                    else{
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        gsenderpic.setVisibility(View.INVISIBLE);
                        sender.setText(model.getCG_message());
                        sendertime.setText(getdatetime(model.getCG_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        gsenderaudipic.setVisibility(View.INVISIBLE);
                        grcvauditime.setVisibility(View.INVISIBLE);
                    }
                } else if(model.getCG_mediaType().equals("status")) {
                    greceiverpdf.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);

                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);
                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);

                    greceiverpic.setVisibility(View.GONE);
                    greceivetimepic.setVisibility(View.GONE);

                    greceivertime.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    gmessagereceiver.setVisibility(View.GONE);

                    notifier.setText(model.getCG_message());
                    notifier.setVisibility(View.VISIBLE);
                    greceivetimepdf.setVisibility(View.GONE);

                    seekbarrcv.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    seekbarrcv.setVisibility(View.GONE);
                    gaudiorcvLayout.setVisibility(View.GONE);

                    receivertime4.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);

                    greceivervideo.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    greceivetimevideo.setVisibility(View.GONE);
                    gsenderaudipic.setVisibility(View.GONE);
                    grcvauditime.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        gsenderpic.setVisibility(View.INVISIBLE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        receiver.setText(model.getCG_message());
                        receivertime.setText(getdatetime(model.getCG_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        gsenderaudipic.setVisibility(View.INVISIBLE);
                        grcvauditime.setVisibility(View.INVISIBLE);

                    }
                    else{
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        gsenderpic.setVisibility(View.INVISIBLE);
                        sender.setText(model.getCG_message());
                        sendertime.setText(getdatetime(model.getCG_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        gsenderaudipic.setVisibility(View.INVISIBLE);
                        grcvauditime.setVisibility(View.INVISIBLE);
                    }
                }
                else if(model.getCG_mediaType().equals("text")){
                    greceiverpdf.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    sendertime.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    greceiverpic.setVisibility(View.GONE);
                    greceivetimepic.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    greceivetimepdf.setVisibility(View.GONE);
                    gaudiorcvLayout.setVisibility(View.GONE);
                    gsenderaudipic.setVisibility(View.GONE);
                    grcvauditime.setVisibility(View.GONE);
                    seekbarrcv.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);
                    greceivervideo.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    greceivetimevideo.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){

                        gsenderpic.setVisibility(View.VISIBLE);
                        greceiver.setVisibility(View.VISIBLE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        gmessagereceiver.setVisibility(View.VISIBLE);
                        gmessagereceiver.setText(model.getCG_message());
                        greceivertime.setText(getdatetime(model.getCG_messageTimestamp()));
                        greceivertime.setVisibility(View.VISIBLE);

                        users.child(senderID).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();

                                if(avatar.startsWith("h")){
                                    GlideApp.with(getApplicationContext())
                                            .load(avatar)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderpic);
                                }
                                else{
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                    GlideApp.with(getApplicationContext())
                                            .load(storageReference)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderpic);
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                    else{
                        grcvauditime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        gsenderpic.setVisibility(View.INVISIBLE);
                        greceiver.setVisibility(View.INVISIBLE);
                        sender.setVisibility(View.VISIBLE);
                        sendertime.setVisibility(View.VISIBLE);
                        sender.setText(model.getCG_message());
                        sendertime.setText(getdatetime(model.getCG_messageTimestamp()));
                        greceivertime.setVisibility(View.INVISIBLE);
                    }
                }
                else if(model.getCG_mediaType().equals("image")){
                    greceiverpdf.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    receiver.setVisibility(View.GONE);
                    receivertime.setVisibility(View.GONE);
                    greceiver.setVisibility(View.INVISIBLE);
                    greceivertime.setVisibility(View.INVISIBLE);
                    sendertime3.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    greceivetimepdf.setVisibility(View.GONE);

                    gaudiorcvLayout.setVisibility(View.GONE);
                    gsenderaudipic.setVisibility(View.GONE);
                    grcvauditime.setVisibility(View.GONE);
                    seekbarrcv.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);
                    greceivervideo.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    greceivetimevideo.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        captionreceiverg.setVisibility(View.VISIBLE);
                        captionreceiverg.setText(model.getCG_message());
                        gsenderpic.setVisibility(View.VISIBLE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        sendertime2.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        greceivetimepic.setText(getdatetime(model.getCG_messageTimestamp()));
                        greceivetimepic.setVisibility(View.VISIBLE);
                        sendPhoto.setVisibility(View.GONE);
                        sendPhotoLayout.setVisibility(View.GONE);
                        greceiverpic.setVisibility(View.VISIBLE);
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(model.getCG_mediaURL());
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                .into(grpicture);

                        users.child(senderID).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();

                                if(avatar.startsWith("h")){
                                    GlideApp.with(getApplicationContext()).load(avatar).diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(gsenderpic);
                                }
                                else{
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                    GlideApp.with(getApplicationContext())
                                            .load(storageReference)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderpic);
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                    else{
                        captionsenderg.setVisibility(View.VISIBLE);
                        captionsenderg.setText(model.getCG_message());
                        gsenderpic.setVisibility(View.INVISIBLE);
                        greceivertime.setVisibility(View.INVISIBLE);
                        greceiver.setVisibility(View.INVISIBLE);
                        greceiverpic.setVisibility(View.INVISIBLE);
                        greceivetimepic.setVisibility(View.INVISIBLE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.GONE);
                        sendertime2.setVisibility(View.VISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        sendertime2.setText(getdatetime(model.getCG_messageTimestamp()));
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        sendPhoto.setVisibility(View.VISIBLE);
                        sendPhotoLayout.setVisibility(View.VISIBLE);
                        receivePhoto.setVisibility(View.GONE);
                        receivePhotoLayout.setVisibility(View.GONE);
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(model.getCG_mediaURL());
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                .into(sendPhoto);
                    }
                }
                else if(model.getCG_mediaType().equals("pdf")){
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    receiver.setVisibility(View.GONE);
                    receivertime.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    greceiver.setVisibility(View.INVISIBLE);
                    greceivertime.setVisibility(View.INVISIBLE);
                    greceivetimepic.setVisibility(View.INVISIBLE);

                    gaudiorcvLayout.setVisibility(View.GONE);
                    gsenderaudipic.setVisibility(View.GONE);
                    grcvauditime.setVisibility(View.GONE);
                    seekbarrcv.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    greceivervideo.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    greceivetimevideo.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        captionreceiverpdfg.setVisibility(View.VISIBLE);
                        captionreceiverpdfg.setText(model.getCG_message());
                        gsenderpic.setVisibility(View.VISIBLE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        sendertime3.setVisibility(View.INVISIBLE);
                        sendertime2.setVisibility(View.INVISIBLE);
                        receiver.setVisibility(View.INVISIBLE);
                        receivertime.setVisibility(View.INVISIBLE);
                        greceivetimepdf.setVisibility(View.VISIBLE);
                        greceivetimepdf.setText(getdatetime(model.getCG_messageTimestamp()));
                        sendPhoto.setVisibility(View.GONE);
                        sendPhotoLayout.setVisibility(View.GONE);
                        greceiverpdf.setVisibility(View.VISIBLE);
                        greceiverpdf.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                System.out.println("url pdf"+model.getCG_mediaURL());
                                StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                                StorageReference pdfreceiver = storageRef.child(model.getCG_mediaURL());
                                pdfreceiver.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(uri.toString()));
                                        startActivity(intent);
                                    }
                                });
                            }
                        });
                        users.child(senderID).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();

                                if(avatar.startsWith("h")){
                                    GlideApp.with(getApplicationContext())
                                            .load(avatar)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderpic);
                                }
                                else{
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                    GlideApp.with(getApplicationContext())
                                            .load(storageReference)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderpic);
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                    else{
                        captionsenderpdfg.setText(model.getCG_message());
                        gsenderpic.setVisibility(View.GONE);
                        greceivertime.setVisibility(View.GONE);
                        greceivetimepdf.setVisibility(View.GONE);
                        greceiver.setVisibility(View.INVISIBLE);
                        greceiverpdf.setVisibility(View.INVISIBLE);
                        greceiverpic.setVisibility(View.GONE);
                        greceivetimepic.setVisibility(View.GONE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        sendertime2.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        sendertime3.setVisibility(View.VISIBLE);
                        sendertime3.setText(getdatetime(model.getCG_messageTimestamp()));
                        receiver.setVisibility(View.GONE);
                        receivertime.setVisibility(View.GONE);
                        sendPhoto.setVisibility(View.GONE);
                        sendPhotoLayout.setVisibility(View.GONE);
                        receivePhoto.setVisibility(View.GONE);
                        receivePhotoLayout.setVisibility(View.GONE);
                        sendPDFLayout.setVisibility(View.VISIBLE);
                        sendPDFLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                System.out.println("url pdf"+model.getCG_mediaURL());
                                StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                                StorageReference pdfsender = storageRef.child(model.getCG_mediaURL());
                                pdfsender.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(uri.toString()));
                                        startActivity(intent);
                                    }
                                });
                            }
                        });
                    }
                }
                else if(model.getCG_mediaType().equals("audio")){

                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    greceiverpic.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    greceivetimepic.setVisibility(View.GONE);
                    gsenderaudipic.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);
                    greceiverpdf.setVisibility(View.GONE);
                    greceivetimepdf.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    sendVideoLayout.setVisibility(View.GONE);
                    sendertime5.setVisibility(View.GONE);
                    greceivervideo.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    greceivetimevideo.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){

                        gsenderaudipic.setVisibility(View.VISIBLE);
                        greceiver.setVisibility(View.INVISIBLE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        gmessagereceiver.setVisibility(View.INVISIBLE);
                        greceivetimepdf.setVisibility(View.INVISIBLE);
                        gaudiorcvLayout.setVisibility(View.VISIBLE);
                        seekbarrcv.setVisibility(View.VISIBLE);
                        sendAudioLayout.setVisibility(View.INVISIBLE);
                        gmessagereceiver.setText(model.getCG_message());
                        grcvauditime.setText(getdatetime(model.getCG_messageTimestamp()));
                        grcvauditime.setVisibility(View.VISIBLE);

                        users.child(senderID).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();

                                if(avatar.startsWith("h")){
                                    GlideApp.with(getApplicationContext())
                                            .load(avatar)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderaudipic);
                                }
                                else{
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                    GlideApp.with(getApplicationContext())
                                            .load(storageReference)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderaudipic);
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                    else{
                        gsenderaudipic.setVisibility(View.INVISIBLE);
                        greceiver.setVisibility(View.INVISIBLE);
                        sender.setVisibility(View.INVISIBLE);
                        sendertime.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.VISIBLE);
                        sendAudioLayout.setVisibility(View.VISIBLE);
                        gaudiorcvLayout.setVisibility(View.INVISIBLE);
                        greceivetimepdf.setVisibility(View.INVISIBLE);
                        sender.setText(model.getCG_message());
                        sendertime4.setText(getdatetime(model.getCG_messageTimestamp()));
                        grcvauditime.setVisibility(View.INVISIBLE);
                        greceivertime.setVisibility(View.INVISIBLE);
                    }
                }
                else if(model.getCG_mediaType().equals("video")){
                    System.out.println("ini adalah video untuk group");
                    greceiverpdf.setVisibility(View.GONE);
                    sendPDFLayout.setVisibility(View.GONE);
                    sendPhoto.setVisibility(View.GONE);
                    sendPhotoLayout.setVisibility(View.GONE);
                    receivePhoto.setVisibility(View.GONE);
                    receivePhotoLayout.setVisibility(View.GONE);
                    sendertime2.setVisibility(View.GONE);
                    sendertime3.setVisibility(View.GONE);
                    receivertime2.setVisibility(View.GONE);

                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);
                    sender.setVisibility(View.INVISIBLE);
                    sendertime.setVisibility(View.INVISIBLE);
                    receiver.setVisibility(View.INVISIBLE);
                    receivertime.setVisibility(View.INVISIBLE);

                    greceiverpic.setVisibility(View.GONE);
                    greceivetimepic.setVisibility(View.GONE);

                    greceivertime.setVisibility(View.GONE);
                    gsenderpic.setVisibility(View.GONE);
                    gmessagereceiver.setVisibility(View.GONE);

                    notifier.setText(model.getCG_message());
                    notifier.setVisibility(View.VISIBLE);
                    greceivetimepdf.setVisibility(View.GONE);

                    seekbarrcv.setVisibility(View.GONE);
                    sendAudioLayout.setVisibility(View.GONE);
                    rcvAudioLayout.setVisibility(View.GONE);
                    seekbarrcv.setVisibility(View.GONE);
                    gaudiorcvLayout.setVisibility(View.GONE);
                    gsenderaudipic.setVisibility(View.GONE);
                    grcvauditime.setVisibility(View.GONE);

                    receivertime4.setVisibility(View.GONE);
                    sendertime4.setVisibility(View.GONE);
                    notifier.setVisibility(View.GONE);

                    if(!senderID.equals(currentid)){
                        sendVideoLayout.setVisibility(View.INVISIBLE);
                        sendertime5.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        greceivetimepdf.setVisibility(View.INVISIBLE);
                        gsenderaudipic.setVisibility(View.INVISIBLE);
                        grcvauditime.setVisibility(View.INVISIBLE);
                        greceivervideo.setVisibility(View.VISIBLE);
                        gsenderpic.setVisibility(View.VISIBLE);
                        greceivetimevideo.setVisibility(View.VISIBLE);
                        captionreceivervg.setText(model.getCG_message());
                        greceivetimevideo.setText(getdatetime(model.getCG_messageTimestamp()));
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(model.getCG_mediaThumbnailURL());
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                .into(grpicturev);

                        users.child(senderID).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();

                                if(avatar.startsWith("h")){
                                    GlideApp.with(getApplicationContext())
                                            .load(avatar)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderpic);
                                }
                                else{
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                    GlideApp.with(getApplicationContext())
                                            .load(storageReference)
                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                            .into(gsenderpic);
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                    else{
                        gsenderpic.setVisibility(View.INVISIBLE);
                        greceivervideo.setVisibility(View.INVISIBLE);
                        gsenderpic.setVisibility(View.INVISIBLE);
                        greceivetimepdf.setVisibility(View.INVISIBLE);
                        gsenderaudipic.setVisibility(View.INVISIBLE);
                        greceivetimevideo.setVisibility(View.INVISIBLE);
                        grcvauditime.setVisibility(View.INVISIBLE);
                        sendPhotoLayout.setVisibility(View.INVISIBLE);
                        sendertime4.setVisibility(View.INVISIBLE);
                        sendVideoLayout.setVisibility(View.VISIBLE);
                        sendertime5.setVisibility(View.VISIBLE);
                        sendertime5.setText(getdatetime(model.getCG_messageTimestamp()));
                        captionsendervideo.setText(model.getCG_message());
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(model.getCG_mediaThumbnailURL());
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                .into(sendVideo);
                    }
                }
            }
        };
        listView.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();
        adapter2.startListening();
    }

    public void sendButtonClicked(View view){
        onStart();
        if(type.equals("personal")){
            if(getMessageType().equals("text")){
                send_chat_text(chatidintent);
            }

            else if(getMessageType().equals("image")){
                send_chat_image(chatidintent);
            }
            else if(getMessageType().equals("pdf")){
                send_chat_pdf(chatidintent);
            }
            else if(getMessageType().equals("video")){
                send_chat_video(chatidintent);
            }
        }
        else if(type.equals("group")){
            if(getMessageType().equals("text")){
                send_group_text(chatidintent);
            }
            else if(getMessageType().equals("image")){
                send_group_image(chatidintent);
            }
            else if(getMessageType().equals("pdf")){
                send_group_pdf(chatidintent);
            }
            else if(getMessageType().equals("video")){
                send_group_video(chatidintent);
            }
        }
    }

    private void send_group_text(String chatid){
        new_messageid = groupchat.push().push().getKey();
        new_messageid = personalchat.push().push().getKey();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        HashMap group_chat = new HashMap();
        group_chat.put("CG_mediaType","text");
        group_chat.put("CG_message",editText.getText().toString());
        group_chat.put("CG_messageSenderID",currentid);
        group_chat.put("CG_messageTimestamp",timestamp.getTime());

        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessage").setValue(editText.getText().toString());
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaType").setValue("text");
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageSenderID").setValue(currentid);
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());

        groupchat.child(chatid).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);

        for(String i:list_of_member){
            users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastMessageID").setValue(new_messageid);
//            users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
        }
        editText.setText("");
        setMessageType("text");
        sendGroupNoti(chatid,new_messageid);

    }
    private void send_chat_text(String chatid){
        HashMap new_chat_details = new HashMap();
        new_messageid = personalchat.push().push().getKey();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        new_chat_details.put("CP_chatID",chatid);
        new_chat_details.put("CP_lastMessageID",new_messageid);
        new_chat_details.put("CP_lastMessage",editText.getText().toString());
        new_chat_details.put("CP_lastMessageTimestamp",timestamp.getTime());
        new_chat_details.put("CP_lastMessageMediaType","text");

        HashMap personal_chat = new HashMap();
        personal_chat.put("CP_message",editText.getText().toString());
        personal_chat.put("CP_messageSenderID",currentid);
        personal_chat.put("CP_mediaType","text");
        personal_chat.put("CP_messageTimestamp",timestamp.getTime());

        personalchat.child(chatid).child("CP_personalDetails").setValue(new_chat_details);
        personalchat.child(chatid).child("CP_personalChatSession").child(new_messageid).setValue(personal_chat);
        users.child(currentid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
        users.child(friendid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
        editText.setText("");
        setMessageType("text");
        sendNoti(chatid);

    }

    private void send_chat_image(String chatid){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Uploading image..");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap new_chat_details = new HashMap();
        new_messageid = personalchat.push().push().getKey();
        String imageUrl = "chats/"+chatid+"/"+new_messageid+".jpg";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        new_chat_details.put("CP_chatID",chatid);
        new_chat_details.put("CP_lastMessageID",new_messageid);
        new_chat_details.put("CP_lastMessage",editText.getText().toString());
        new_chat_details.put("CP_lastMessageTimestamp",timestamp.getTime());
        new_chat_details.put("CP_lastMessageMediaType","image");
        new_chat_details.put("CP_lastMessageMediaURL",imageUrl);

        HashMap personal_chat = new HashMap();
        personal_chat.put("CP_message",editText.getText().toString());
        personal_chat.put("CP_messageSenderID",currentid);
        personal_chat.put("CP_mediaType","image");
        personal_chat.put("CP_mediaURL",imageUrl);
        personal_chat.put("CP_messageTimestamp",timestamp.getTime());

        StorageReference filepath = chatsstorage.child(chatid+"/"+new_messageid+".jpg");
        UploadTask uploadTask = filepath.putBytes(getAvatar());
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(ChatsActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                personalchat.child(chatid).child("CP_personalDetails").setValue(new_chat_details);
                personalchat.child(chatid).child("CP_personalChatSession").child(new_messageid).setValue(personal_chat);
                users.child(currentid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
                users.child(friendid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
                dialog.dismiss();
            }
        });
        editText.setText("");
        plusbutton.setVisibility(View.VISIBLE);
        preview_image.setVisibility(View.GONE);
        setMessageType("text");
        setAvatar(new byte[0]);
        sendNoti(chatid);
    }
    private void send_chat_video(String chatid){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Uploading video..");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap new_chat_details = new HashMap();
        new_messageid = personalchat.push().push().getKey();
        String imageUrl = "chats/"+chatid+"/"+new_messageid+"thumbnail"+".jpg";
        String videoUrl = "chats/"+chatid+"/"+new_messageid+".3gp";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        new_chat_details.put("CP_chatID",chatid);
        new_chat_details.put("CP_lastMessageID",new_messageid);
        new_chat_details.put("CP_lastMessage",editText.getText().toString());
        new_chat_details.put("CP_lastMessageTimestamp",timestamp.getTime());
        new_chat_details.put("CP_lastMessageMediaType","video");
        new_chat_details.put("CP_lastMessageMediaURL",videoUrl);

        HashMap personal_chat = new HashMap();
        personal_chat.put("CP_message",editText.getText().toString());
        personal_chat.put("CP_messageSenderID",currentid);
        personal_chat.put("CP_mediaType","video");
        personal_chat.put("CP_mediaURL",videoUrl);
        personal_chat.put("CP_mediaThumbnailURL",imageUrl);
        personal_chat.put("CP_messageTimestamp",timestamp.getTime());

        StorageReference videopath = chatsstorage.child(chatid+"/"+new_messageid+".3gp");

        if(getAvatar()!=null){
            StorageReference filepath = chatsstorage.child(chatid+"/"+new_messageid+"thumbnail"+".jpg");
            UploadTask uploadTask = filepath.putBytes(getAvatar());
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    dialog.dismiss();
                    Toast.makeText(ChatsActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if(getVideouri()!=null){
                        UploadTask uploadvideo = videopath.putFile(getVideouri());
                        uploadvideo.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                personalchat.child(chatid).child("CP_personalDetails").setValue(new_chat_details);
                                personalchat.child(chatid).child("CP_personalChatSession").child(new_messageid).setValue(personal_chat);
                                users.child(currentid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
                                users.child(friendid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
                                dialog.dismiss();
                            }
                        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                double p = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                                dialog.setMessage((int) p+" % Uploading...");
                            }
                        });
                    }
                    else{
                        Toast.makeText(ChatsActivity.this,"nothing to upload",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else{
            System.out.println("get avatar null");
        }
        editText.setText("");
        plusbutton.setVisibility(View.VISIBLE);
        preview_image.setVisibility(View.GONE);
        setMessageType("text");
        setAvatar(new byte[0]);
        sendNoti(chatid);
    }

    private void send_chat_pdf(String chatid){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Please wait,we are sending that file...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        new_messageid = groupchat.push().push().getKey();
        new_messageid = personalchat.push().push().getKey();
        String pdfurl = "chats/"+chatid+"/"+new_messageid+".pdf";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        HashMap new_chat_details = new HashMap();
        new_chat_details.put("CP_chatID",chatid);
        new_chat_details.put("CP_lastMessageID",new_messageid);
        new_chat_details.put("CP_lastMessage",editText.getText().toString());
        new_chat_details.put("CP_lastMessageTimestamp",timestamp.getTime());
        new_chat_details.put("CP_lastMessageMediaType","pdf");
        new_chat_details.put("CP_lastMessageMediaURL",pdfurl);

        HashMap personal_chat = new HashMap();
        personal_chat.put("CP_message",editText.getText().toString());
        personal_chat.put("CP_messageSenderID",currentid);
        personal_chat.put("CP_mediaType","pdf");
        personal_chat.put("CP_mediaURL",pdfurl);
        personal_chat.put("CP_messageTimestamp",timestamp.getTime());

        StorageReference filepath = chatsstorage.child(chatid+"/"+new_messageid+".pdf");

        filepath.putFile(getFileuri()).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(task.isSuccessful()){
                    personalchat.child(chatid).child("CP_personalDetails").setValue(new_chat_details);
                    personalchat.child(chatid).child("CP_personalChatSession").child(new_messageid).setValue(personal_chat);
                    users.child(currentid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
                    users.child(friendid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
                    dialog.dismiss();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(ChatsActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double p = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                dialog.setMessage((int) p+" % Uploading...");
            }
        });
        preview_docs.setVisibility(View.GONE);
        plusbutton.setVisibility(View.VISIBLE);
        editText.setText("");
        setMessageType("text");
        sendNoti(chatid);
    }

    private void send_chat_audio(String chatid){
        HashMap new_chat_details = new HashMap();
        new_messageid = personalchat.push().push().getKey();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        String mediaAudioUrl = ("chats/" + chatidintent + "/" + new_messageid + ".aac");

        new_chat_details.put("CP_chatID",chatid);
        new_chat_details.put("CP_lastMessageID",new_messageid);
        new_chat_details.put("CP_lastMessage",editText.getText().toString());
        new_chat_details.put("CP_lastMessageTimestamp",timestamp.getTime());
        new_chat_details.put("CP_lastMessageMediaType","audio");
        new_chat_details.put("CP_lastMessageMediaURL","");

        HashMap personal_chat = new HashMap();
        personal_chat.put("CP_message",editText.getText().toString());
        personal_chat.put("CP_messageSenderID",currentid);
        personal_chat.put("CP_mediaType","audio");
        personal_chat.put("CP_messageTimestamp",timestamp.getTime());
        personal_chat.put("CP_mediaURL",mediaAudioUrl);
        personal_chat.put("CP_mediaThumbnailURL","");
        personal_chat.put("CP_messageSeenStatus","");


        personalchat.child(chatid).child("CP_personalDetails").setValue(new_chat_details);
        users.child(currentid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
        users.child(friendid).child("userConversations").child("Personal").child(chatid).child("UCP_lastMessageID").setValue(new_messageid);
        editText.setText("");
        personalchat.child(chatid).child("CP_personalChatSession").child(new_messageid).setValue(personal_chat).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "upload");
                    StorageReference filepath = chatsstorage.child(chatidintent).child(new_messageid+".aac");
                    Uri uri = Uri.fromFile(new File(fileName));
                    filepath.putFile(uri);
                } else {
                    String message = task.getException().toString();
                    Toast.makeText(getApplicationContext(), "error:" + message, Toast.LENGTH_SHORT).show();

                }
            }
        });
        sendNoti(chatid);
        setMessageType("text");

    }

    private void send_group_image(String chatid){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Uploading image..");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        new_messageid = groupchat.push().push().getKey();
        new_messageid = personalchat.push().push().getKey();
        String imageUrl = "chats/"+chatid+"/"+new_messageid+".jpg";
        String lastmessage = editText.getText().toString();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        HashMap group_chat = new HashMap();
        group_chat.put("CG_mediaType","image");
        group_chat.put("CG_message",editText.getText().toString());
        group_chat.put("CG_messageSenderID",currentid);
        group_chat.put("CG_messageTimestamp",timestamp.getTime());
        group_chat.put("CG_mediaURL",imageUrl);

        StorageReference filepath = chatsstorage.child(chatid+"/"+new_messageid+".jpg");
        UploadTask uploadTask = filepath.putBytes(getAvatar());
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(ChatsActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessage").setValue(lastmessage);
                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaType").setValue("image");
                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageSenderID").setValue(currentid);
                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaURL").setValue(imageUrl);


                groupchat.child(chatid).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);

                for(String i:list_of_member){
                    users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastMessageID").setValue(new_messageid);
//                    users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
                }
                dialog.dismiss();
            }
        });
        editText.setText("");
        plusbutton.setVisibility(View.VISIBLE);
        preview_image.setVisibility(View.GONE);
        setMessageType("text");
        setAvatar(new byte[0]);
        sendGroupNoti(chatid,new_messageid);
    }

    private void send_group_audio(String chatid){
        new_messageid = groupchat.push().push().getKey();
        new_messageid = personalchat.push().push().getKey();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        String gAudioUrl = ("chats/" + chatidintent + "/" + new_messageid + ".aac");

        HashMap group_chat = new HashMap();
        group_chat.put("CG_mediaType","audio");
        group_chat.put("CG_message",editText.getText().toString());
        group_chat.put("CG_messageSenderID",currentid);
        group_chat.put("CG_messageTimestamp",timestamp.getTime());
        group_chat.put("CG_mediaURL",gAudioUrl);
        group_chat.put("CG_mediaThumbnailURL","");
        group_chat.put("CG_messageSeenStatus","");

        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessage").setValue(editText.getText().toString());
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaType").setValue("audio");
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageSenderID").setValue(currentid);
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
        groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaURL").setValue("");

//        groupchat.child(chatid).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);
        groupchat.child(chatid).child("CG_groupChatSession").child(new_messageid).setValue(group_chat).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
//                mProgress.dismiss();
                    Log.d(TAG, "upload");
                    StorageReference filepath = chatsstorage.child(chatidintent).child(new_messageid+".aac");
                    Uri uri = Uri.fromFile(new File(fileName));
                    filepath.putFile(uri);
                } else {
//                mProgress.dismiss();
                    String message = task.getException().toString();
                    Toast.makeText(getApplicationContext(), "error:" + message, Toast.LENGTH_SHORT).show();

                }
            }
        });
        for(String i:list_of_member){
//            users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
            users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastMessageID").setValue(new_messageid);
        }
        editText.setText("");
        setMessageType("text");
        sendGroupNoti(chatid,new_messageid);
    }


    private void send_group_pdf(String chatid){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Please wait,we are sending that file...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        new_messageid = groupchat.push().push().getKey();
        new_messageid = personalchat.push().push().getKey();
        String pdfUrl = "chats/"+chatid+"/"+new_messageid+".pdf";
        String lastmessage = editText.getText().toString();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        HashMap group_chat = new HashMap();
        group_chat.put("CG_mediaType","pdf");
        group_chat.put("CG_message",editText.getText().toString());
        group_chat.put("CG_messageSenderID",currentid);
        group_chat.put("CG_messageTimestamp",timestamp.getTime());
        group_chat.put("CG_mediaURL",pdfUrl);

        StorageReference filepath = chatsstorage.child(chatid+"/"+new_messageid+".pdf");

        filepath.putFile(getFileuri()).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(task.isSuccessful()){
                    groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessage").setValue(lastmessage);
                    groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaType").setValue("pdf");
                    groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageSenderID").setValue(currentid);
                    groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
                    groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
                    groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaURL").setValue(pdfUrl);

                    groupchat.child(chatid).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);

                    for(String i:list_of_member){
                        users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastMessageID").setValue(new_messageid);
//                        users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);

                    }
                    dialog.dismiss();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(ChatsActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double p = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                dialog.setMessage((int) p+" % Uploading...");
            }
        });
        preview_docs.setVisibility(View.GONE);
        plusbutton.setVisibility(View.VISIBLE);
        editText.setText("");
        setMessageType("text");
        sendGroupNoti(chatid,new_messageid);
    }

    private void send_group_video(String chatid){
        ProgressDialog dialog= new ProgressDialog(this);
        dialog.setMessage("Uploading video..");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        new_messageid = personalchat.push().push().getKey();
        String imageUrl = "chats/"+chatid+"/"+new_messageid+"thumbnail"+".jpg";
        String videoUrl = "chats/"+chatid+"/"+new_messageid+".3gp";
        String lastmessage = editText.getText().toString();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        HashMap group_chat = new HashMap();
        group_chat.put("CG_mediaType","video");
        group_chat.put("CG_message",editText.getText().toString());
        group_chat.put("CG_messageSenderID",currentid);
        group_chat.put("CG_messageTimestamp",timestamp.getTime());
        group_chat.put("CG_mediaURL",videoUrl);
        group_chat.put("CG_mediaThumbnailURL",imageUrl);

        StorageReference videopath = chatsstorage.child(chatid+"/"+new_messageid+".3gp");

        if(getAvatar()!=null){
            StorageReference filepath = chatsstorage.child(chatid+"/"+new_messageid+"thumbnail"+".jpg");
            UploadTask uploadTask = filepath.putBytes(getAvatar());
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    dialog.dismiss();
                    Toast.makeText(ChatsActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if(getVideouri()!=null){
                        UploadTask uploadvideo = videopath.putFile(getVideouri());
                        uploadvideo.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessage").setValue(lastmessage);
                                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaType").setValue("video");
                                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageSenderID").setValue(currentid);
                                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
                                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
                                groupchat.child(chatid).child("CG_groupDetails").child("CG_lastMessageMediaURL").setValue(videoUrl);

                                groupchat.child(chatid).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);

                                for(String i:list_of_member){
                                    users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastMessageID").setValue(new_messageid);
//                                    users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
                                }
                                dialog.dismiss();
                            }
                        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                double p = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                                dialog.setMessage((int) p+" % Uploading...");
                            }
                        });
                    }
                    else{
                        Toast.makeText(ChatsActivity.this,"nothing to upload",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else{
            System.out.println("get avatar null");
        }
        editText.setText("");
        plusbutton.setVisibility(View.VISIBLE);
        preview_image.setVisibility(View.GONE);
        setMessageType("text");
        setAvatar(new byte[0]);
        sendGroupNoti(chatid,new_messageid);
    }

    private void sendNoti(String chat_id){
        RequestQueue queue = Volley.newRequestQueue(this);
        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/notification/"+currentid+"/sendMsg/"+friendid;

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
        String avatar_url = image;
        postParams.put("chatID",chat_id);
        postParams.put("type","personal");
        postParams.put("msgType",getMessageType());
        postParams.put("name",name);
//        System.out.println("chatid:"+chat_id);
//        System.out.println("url:"+url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    private void sendGroupNoti(String chat_id,String messageId){
        RequestQueue queue = Volley.newRequestQueue(this);
        System.out.println("masuk sini dalam json");
        String url = "https://devbm.ml/notification/"+currentid+"/sendMsgGroup/"+chat_id;

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();
        String avatar_url = image;
        postParams.put("chatID",messageId);
        postParams.put("msgType",getMessageType());
        postParams.put("type","group");
        postParams.put("msgText","");
        System.out.println("chatid:"+messageId);
        System.out.println("msgtype:"+getMessageType());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    public ArrayList getMember(String chatid){
        chats.child("Group").child(chatid).child("CG_groupMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String member = dataSnapshot1.getKey();
                    list_of_member.add(member);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        return  list_of_member;
    }

    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat hari = new SimpleDateFormat("EEEE");
        Date  currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        String dateplusone = df.format(ccc);
        setDay(hari.format(ccc));
        datess = dateplusone;
        return datess;
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = getDay();
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }
    public void updatebeforeclose(String status){
        users.child(currentid).child("userStatus").child("US_onlineStatus").setValue(status);
        users.child(currentid).child("userStatus").child("US_timestamp").setValue(ServerValue.TIMESTAMP);
//        userinfo.child(current_user).child("userStatus").child("US_battery").setValue(battery);
//        users.child(currentid).child("userStatus").child("US_battery").setValue(getBatteryLevel());
    }
    public String getDay() { return day; }
    public void setDay(String day) { this.day = day; }

    public void gotoimageview(String image){
        Intent i = new Intent(getApplicationContext(), ViewImageActivity.class);
        i.putExtra("test",image);
        startActivity(i);
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent();
        intent.putExtra("back",123);
        setResult(RESULT_OK,intent);
        finish();
    }

    public static boolean isNumeric(String strNum) {
        try {
            Long d = Long.parseLong(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public void onButtonChatClicked(String text) {
        switch (text){
            case "docs_btn":
                uploadfile();
                break;
            case "cam_btn":
                CropImage.startPickImageActivity(this);
                break;
            case "gallery_btn":
                recordVideo();
                break;
            case "loc_btn":
                Toast.makeText(this,"In development mode", Toast.LENGTH_SHORT).show();
                break;
            case "audio_btn":
                Toast.makeText(this,"In development mode", Toast.LENGTH_SHORT).show();
                break;
            case "contct_btn":
                Toast.makeText(this,"In development mode", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    private void uploadfile(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        startActivityForResult(intent.createChooser(intent,"Select file"),438);
    }

    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
    };

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission GRANTED");
            }
            else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
            return;
        }
    }

    private void requestAudioPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECORD_AUDIO)) {

            new AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed to access MIC")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(ChatsActivity.this,
                                    new String[] {Manifest.permission.RECORD_AUDIO}, RECORD_REQUEST_CODE);
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.RECORD_AUDIO}, RECORD_REQUEST_CODE);
        }
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            new AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed to access Storage")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(ChatsActivity.this,
                                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_REQUEST_CODE);
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_REQUEST_CODE);
        }
    }

    public void getCameraPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.CAMERA)) {
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        WRITE_REQUEST_CODE);
            }
        }
    }

    public Uri getFileuri() { return fileuri; }
    public void setFileuri(Uri fileuri) { this.fileuri = fileuri; }
    private void setAvatar(byte[] bytes) { this.avatar = bytes; }
    public byte[] getAvatar() { return avatar; }
    public String getMessageType() { return messageType; }
    public void setMessageType(String messageType) { this.messageType = messageType; }
    public Uri getVideouri() { return videouri; }
    public void setVideouri(Uri videouri) { this.videouri = videouri; }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        mediaPlayer.getDuration();
    }

}