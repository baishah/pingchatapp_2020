package com.app.pingchat.Chat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.Friends;
import com.app.pingchat.R;
import com.app.pingchat.Friend.contacts;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class ChatsFragment extends Fragment implements ChatListAdapter.OnLongBtnListener{
    private static final String TAG = "ChatsFragment";
    FirebaseAuth mAuth;
    private String currentuser, chatidintent;
    private ListView listView;
    private DatabaseReference userconv,chatsdb, friends;

    ArrayList<com.app.pingchat.Friend.contacts> list;
    ArrayAdapter<contacts> adapter;
    ArrayList<Chats> list2;

    ArrayList<Friends> friendChatList;
    ArrayList<Groups> groupChatList;
    Chats chats;
    contacts contacts;

    FirebaseStorage storage;
    StorageReference storeRef;
    //    private Toolbar toolbar;
    SearchView searchView;

    private RecyclerView friendChat;
    ArrayList<ChatListClass>friendChatss;
    ChatListAdapter chatListAdapter;

    private Context context;

    String datetimes;
    String datess;
    String day;

    ImageButton chatmenu;
    ImageView nochat;

    String name, friendid,chatid,image,type,key;

//    public static final String MyPREFERENCESC = "MyPrefs" ;


    ArrayList friendidkey = new ArrayList();
    private ArrayList<ChatListClass> keychat = new ArrayList<>();

    Date datesss;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //  super.onCreate(savedInstanceState);
        // setContentView(R.layout.chats_fragment);
        View view = inflater.inflate(R.layout.chats_fragment,container,false);


        mAuth = FirebaseAuth.getInstance();
        friendChat = view.findViewById(R.id.chat_listR);
        friendChat.setLayoutManager(new LinearLayoutManager(getContext()));
        nochat = view.findViewById(R.id.nochatlist);
//        toolbar = view.findViewById(R.id.toolbar);
        searchView = view.findViewById(R.id.search_view);
//        TextView cet = view.findViewById(R.id.cet);
        chatmenu = view.findViewById(R.id.chat_menu);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
//        searchView = view.findViewById(R.id.search_view);
        context = getContext();

        friendChatList = new ArrayList<Friends>();
        groupChatList = new ArrayList<Groups>();
        list = new ArrayList<>();
        list2 = new ArrayList<>();
        chats = new Chats();
        contacts = new contacts();

        SharedPreferences prefs = context.getSharedPreferences("MyPREFERENCESC" ,0);
        key = prefs.getString("key", null);
        image = prefs.getString("image", null);
        type= prefs.getString("type", null);
        name = prefs.getString("name", null);
        friendid = prefs.getString("friendid", null);
        chatid = prefs.getString("chatid", null);
//        String key = getArguments().getString("key");
//        String imageurl = getArguments().getString("image");
//        String type = getArguments().getString("type");
//        String name = getArguments().getString("name");
//        String friendid = getArguments().getString("friendid");
//        String chatId = getArguments().getString("chatid");

        System.out.println("keychatpref: " + prefs);

        chatmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),ChatMenuActivity.class));
            }
        });
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        if(mAuth.getCurrentUser() != null){
            currentuser = mAuth.getCurrentUser().getUid();

            userconv = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userConversations");
            userconv.keepSynced(true);
            chatsdb= FirebaseDatabase.getInstance().getReference().child("Chats");
            chatsdb.keepSynced(true);
            friends = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userFriends");
            friends.keepSynced(true);
            storage = FirebaseStorage.getInstance();
            storeRef = storage.getReference();
            display_chat_list();
        }

        if(type != null) {
            System.out.println("typechat: " + type);
            System.out.println("keychat: " + key);
            System.out.println("friendidchat: " + friendid);
            if (type.equals("personal")) {
                friends.child(friendid).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            System.out.println("sini masuk dia" );
//                            String myFriend = dataSnapshot.getKey();
                            String status = dataSnapshot.getValue(String.class);
                            System.out.println("status:friend " + status);
                            if (status.equals("friend")) {
//                                System.out.println("get friend id " + myfriend.getFriendid());
                                Intent in = new Intent(context, ChatsActivity.class);
                                in.putExtra("friendid", friendid);
                                in.putExtra("name", name);
                                in.putExtra("avatarurl", image);
                                in.putExtra("chatid", chatid);
                                in.putExtra("type", type);
                                startActivityForResult(in, 123);

                            }else if(status.equals("block")){
                                Toast.makeText(context, "You had been blocked by this user", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, "not available", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }else if (type.equals("group")) {
                Intent in = new Intent(context, ChatsActivity.class);
                in.putExtra("name", name);
                in.putExtra("avatarurl", image);
                in.putExtra("chatid", chatid);
                in.putExtra("type", type);
                startActivityForResult(in, 123);
            }
        }else{
            System.out.println("nodata");
        }
        return view;
    }

    @Override
    public void onStart(){
        super.onStart();

    }

//    private void filter(String text){
//        ArrayList<ChatListClass> filteredlist = new ArrayList<>();
//        System.out.println("user ff punya "+friendChatss.size());
//        if(friendChatss.size()!=0) {
//            for (int i = 0; i < friendChatss.size(); i++) {
//                Log.e("list", "friend " + friendChatss.get(i).getDisplayname());
//            }
//            for (ChatListClass item : friendChatss) {
//                if (item.getDisplayname().toLowerCase().contains(text.toLowerCase())) {
//                    filteredlist.add(item);
//                }
//            }
//            chatListAdapter.filterList(filteredlist);
//        }
//    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public  void display_chat_list(){
//        ArrayList <ChatListClass>keychat = new ArrayList();
        keychat = new ArrayList<>();
        chatsdb.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataSnapshot g = dataSnapshot.child("Group");
                DataSnapshot p = dataSnapshot.child("Personal");
                userconv.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            nochat.setVisibility(View.INVISIBLE);
                            DataSnapshot group = dataSnapshot.child("Group");
                            if(group.exists()){
                                for(DataSnapshot dataSnapshot1:group.getChildren()){
                                    String gchatId = dataSnapshot1.getKey();
                                    if(dataSnapshot1.hasChild("UCG_status")) {
                                        String status = dataSnapshot1.child("UCG_status").getValue().toString();
                                        if(status.equals("true")) {
                                            ChatListClass chatListClass1 = new ChatListClass();
                                            String groupkey = dataSnapshot1.getKey();
                                            userconv.child("Group").child(gchatId).child("UCG_status").setValue(true);
                                            String lastgroupmessagekey = dataSnapshot1.child("UCG_lastMessageID").getValue().toString();
//                                            String lastgroupmessagekey = dataSnapshot1.child("UCG_lastSeenMessageID").getValue().toString();
                                            String lasttimestampg = g.child(groupkey).child("CG_groupChatSession").child(lastgroupmessagekey).child("CG_messageTimestamp").getValue().toString();

                                            chatListClass1.setChat_id(groupkey);
                                            chatListClass1.setChat_type("group");
                                            chatListClass1.setLast_chat_id(lastgroupmessagekey);
                                            chatListClass1.setMessage_timestamp(lasttimestampg);
                                            chatListClass1.setDatetocompare(getdate(Long.valueOf(lasttimestampg)));
                                            chatListClass1.setDate(getdatetime(Long.valueOf(lasttimestampg)));
                                            System.out.println("groupkey " + groupkey);
                                            keychat.add(chatListClass1);
                                        }else{
                                            userconv.child("Group").child(gchatId).child("UCG_status").setValue(false);
                                        }
                                    }
                                }
                            }
                            DataSnapshot personal = dataSnapshot.child("Personal");
                            if(personal.exists()){
                                for (DataSnapshot dataSnapshot2:personal.getChildren()){
                                    String chatId = dataSnapshot2.getKey();
                                    if(dataSnapshot2.hasChild("UCP_status")) {
                                        String status = dataSnapshot2.child("UCP_status").getValue().toString();
                                        if(status.equals("true")) {
                                            Log.d(TAG, "status:"+status);
                                            userconv.child("Personal").child(chatId).child("UCP_status").setValue(true);
                                            ChatListClass chatListClass2 = new ChatListClass();
                                            String personalkey = dataSnapshot2.getKey();
                                            String lastpersonalmessagekey= dataSnapshot2.child("UCP_lastMessageID").getValue().toString();
//                                            String lastseenpersonalmessagekey = dataSnapshot2.child("UCP_lastSeenMessageID").getValue().toString();
                                            String lasttimestampp = p.child(personalkey).child("CP_personalChatSession").child(lastpersonalmessagekey).child("CP_messageTimestamp").getValue().toString();
                                            String friendid = dataSnapshot2.child("UCP_friendID").getValue().toString();

                                            chatListClass2.setFriendid(friendid);
                                            chatListClass2.setChat_id(personalkey);
                                            chatListClass2.setChat_type("personal");
                                            chatListClass2.setLast_chat_id(lastpersonalmessagekey);
                                            System.out.println("personalkey " + personalkey);
                                            chatListClass2.setMessage_timestamp(lasttimestampp);
                                            chatListClass2.setDatetocompare(getdate(Long.valueOf(lasttimestampp)));
                                            chatListClass2.setDate(getdatetime(Long.valueOf(lasttimestampp)));
                                            keychat.add(chatListClass2);
                                        }else{
                                            userconv.child("Personal").child(chatId).child("UCP_status").setValue(false);
                                        }
//
                                    }

                                }
                            }
                            chatListAdapter = new ChatListAdapter(ChatsFragment.this,getContext(),keychat, currentuser, ChatsFragment.this::onItemLongClick);
                            chatListAdapter.notifyDataSetChanged();
                            Collections.sort(keychat,ChatListClass.chatdatecomparator1);
                            friendChat.setAdapter(chatListAdapter);
                        }
                        else{
                            nochat.setVisibility(View.VISIBLE);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        menu.clear();
//        inflater.inflate(R.menu.menu2, menu);
//        MenuItem searchItem = menu.findItem(R.id.action_search);
//        searchView = (SearchView) searchItem.getActionView();
//        searchView.setLayoutParams(new ActionBar.LayoutParams(Gravity.END));

//        searchView.onActionViewCollapsed();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                chatListAdapter.getFilter().filter(newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    public String getcalendar(int i){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat hari = new SimpleDateFormat("EEEE");
        Date  currentdate = new Date();
        Calendar cc = Calendar.getInstance();
        cc.setTime(currentdate);

        cc.add(Calendar.DATE,i);
        Date ccc=cc.getTime();
        setDay(hari.format(ccc));
        String dateplusone = df.format(ccc);
        datess = dateplusone;
        return datess;
    }

    public String getdatetime(Long timestamp){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat sfd = new SimpleDateFormat("hh:mm aa");
        final SimpleDateFormat sfd1 = new SimpleDateFormat("dd MMM");
        Date c = Calendar.getInstance().getTime();

        String datenow = df.format(c);
        String datetimestamp = df.format(timestamp);

        if(datenow.equals(datetimestamp)){
            datetimes = sfd.format(timestamp);
        }
        else if(datetimestamp.equals(getcalendar(-1))){
            datetimes = "yesterday";
        }
        else if(datetimestamp.equals(getcalendar(-2))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-3))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-4))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-5))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-6))){
            datetimes = getDay();
        }
        else if(datetimestamp.equals(getcalendar(-7))){
            datetimes = getDay();
        }
        else{
            datetimes= sfd1.format(timestamp);
        }
        return datetimes;
    }

    public String getDay() { return day; }
    public void setDay(String day) { this.day = day; }

    public Date getdate(long timestamp){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        datesss =new Date(timestamp);
        return datesss;
    }

    @Override
    public void onItemLongClick(int position) {
        System.out.println("onLongItemClick: clicked");
        keychat.get(position);
        String chatID = keychat.get(position).getChat_id();
        String chatType = keychat.get(position).getChat_type();
        if(chatType.equals("personal")){
            CharSequence option[] = new CharSequence[]
                    {
                            "Delete Chat",
                            "Cancel",
                    };
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Menu");
            builder.setItems(option, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (i == 0) {
                        System.out.println("delete");
                        keychat.remove(position);
                        chatListAdapter.notifyItemRemoved(position);
                        DatabaseReference mUsers = userconv.child("Personal").child(chatID);
                        mUsers.child("UCP_status").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(context, "Chats deleted!...", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if (i == 1) {
                        System.out.println("cancel");
//                        chatListAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                    }
                }
            });
            builder.show();
        }else {
            CharSequence option[] = new CharSequence[]
                    {
                            "Clear Chat",
                            "Cancel",
                    };
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Menu");
            builder.setItems(option, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (i == 0) {
                        System.out.println("clear");

                    } else if (i == 1) {
                        System.out.println("cancel");
                    }
                }
            });
            builder.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
   //     ChatsFragment.this.onActivityResult(requestCode,resultCode,data);
        if (requestCode == 123) {
            if(resultCode == RESULT_OK) {
                display_chat_list();
                System.out.println("display_chat_list();");
            }
        }
    }
}

