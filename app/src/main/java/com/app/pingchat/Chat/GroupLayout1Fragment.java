package com.app.pingchat.Chat;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;

public class GroupLayout1Fragment extends Fragment implements View.OnClickListener {

    FirebaseAuth mAuth;
    private DatabaseReference friends,users,userFriends;
    private ListView listView;
    private String current_id;
    private RecyclerView friendList,selectedFriendList;
    private EditText search_friend;

    ArrayList<userDetailsClass> mContactslist;
    SelectFriendforGroupAdapter selectFriendforGroupAdapter;
    com.app.pingchat.Chat.getselectedfriendadapter getselectedfriendadapter;

    ImageButton nexts;

    FirebaseStorage storage;
    StorageReference storeRef;

    SelectFriendforGroupAdapter.selectedrecyclerlistadapter selectedrecyclerlistadapter;
    getselectedfriendadapter.getselectedrecyclerlistadapter getselectedrecyclerlistadapter;


    private static GroupLayout1Fragment instance = null;
    private ArrayList<userDetailsClass> itemarraylist=new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.group_1_layout,container,false);

        search_friend = (EditText) rootView.findViewById(R.id.search_friends);

        friendList = (RecyclerView) rootView.findViewById(R.id.friendlistR);
        friendList.setLayoutManager(new LinearLayoutManager(getContext()));

        selectedFriendList = (RecyclerView) rootView.findViewById(R.id.selected);
        selectedFriendList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));

        nexts = rootView.findViewById(R.id.next);

        mContactslist = new ArrayList<>();

        search_friend.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {

              //  filter(s.toString());
            }
        });

        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser()!=null) {
            current_id = mAuth.getCurrentUser().getUid();
            friends = FirebaseDatabase.getInstance().getReference().child("Users").child(current_id).child("userFriends");
            friends.keepSynced(true);
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            storage = FirebaseStorage.getInstance();
            storeRef = storage.getReference();
            displayfriend();

        }
       nexts.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.next:
                System.out.println("id next clicked");
                Intent intent = new Intent(getContext(), GroupActivity2.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST",itemarraylist);
                intent.putExtra("BUNDLE",args);
                startActivity(intent);
                break;


        }
    }

    @Override
    public void onStart(){
        super.onStart();

    }

    public void displayfriend(){
        mContactslist = new ArrayList<>();
        friends.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    final userDetailsClass fren = new userDetailsClass();
                    final String friendid = dataSnapshot1.getKey().toString();
                    final String status = dataSnapshot1.getValue().toString();
                    System.out.println("friend id"+friendid);
                    users.child(friendid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(status.equals("friend")){
                                userDetailsClass f = dataSnapshot.getValue(userDetailsClass.class);
                                final String avatarurl = f.getUD_avatarURL();
                                final String displayname = f.getUD_displayName();
                                final String userabout = f.getUD_aboutMe();
                                final String id = f.getUD_userID();

                                System.out.println("avatar url"+avatarurl);
                                fren.setUD_displayName(displayname);
                                fren.setUD_aboutMe(userabout);
                                fren.setUD_avatarURL(avatarurl);
                                fren.setUD_userID(id);
                                mContactslist.add(fren);

                                SparseBooleanArray s = new SparseBooleanArray();
                                selectedrecyclerlistadapter = new SelectFriendforGroupAdapter.selectedrecyclerlistadapter() {
                                    @Override
                                    public void onUserClicked(int position) {

                                    //    selectFriendforGroupAdapter.setselected((mContactslist.get(position).getUserID()));
                                        final userDetailsClass frenss = new userDetailsClass();
                                        frenss.setUD_displayName(mContactslist.get(position).getUD_displayName());
                                        frenss.setUD_aboutMe(mContactslist.get(position).getUD_aboutMe());
                                        frenss.setUD_avatarURL(mContactslist.get(position).getUD_avatarURL());
                                        frenss.setUD_userID(mContactslist.get(position).getUD_userID());
                                       if(!s.get(position)){
                                           s.put(position,true);
                                           selectFriendforGroupAdapter.notifyItemChanged(position);
                                           itemarraylist.add(frenss);
                                           System.out.println("itemarraylist "+itemarraylist);
                                       }
                                       else{
                                           s.put(position,false);
                                           selectFriendforGroupAdapter.notifyItemChanged(position);
                                           removeItem(getselectedfriendadapter.getposition(mContactslist.get(position).getUD_userID()));
                                           itemarraylist.remove(frenss);
                                           System.out.println("itemarraylist "+itemarraylist);
                                       }

                                       if(itemarraylist.size()>1){
                                           nexts.setVisibility(View.VISIBLE);
                                       }
                                       else {
                                           nexts.setVisibility(View.INVISIBLE);
                                       }

                                       if(itemarraylist.size()!=0){
                                           selectedFriendList.setVisibility(View.VISIBLE);
                                       }
                                       else{
                                           selectedFriendList.setVisibility(View.VISIBLE);
                                       }

                                       getselectedrecyclerlistadapter = new getselectedfriendadapter.getselectedrecyclerlistadapter() {
                                           @Override
                                           public void onUserClicked(int position) {
                                               s.put(selectFriendforGroupAdapter.getposition(itemarraylist.get(position).getUD_userID()),false);
                                               selectFriendforGroupAdapter.notifyItemChanged(selectFriendforGroupAdapter.getposition(itemarraylist.get(position).getUD_userID()));
                                               removeItem(position);
                                               getselectedfriendadapter.notifyItemChanged(position);
                                              // itemarraylist.remove(frenss);
                                               System.out.println("itemarraylist1 "+getselectedfriendadapter.getItemArraylist());

                                               if(getselectedfriendadapter.getItemArraylist().size()>1){
                                                   nexts.setVisibility(View.VISIBLE);
                                               }
                                               else {
                                                   nexts.setVisibility(View.INVISIBLE);
                                               }
                                           }
                                       };
                                       getselectedfriendadapter = new getselectedfriendadapter(getContext(),itemarraylist,getselectedrecyclerlistadapter);
                                       if(itemarraylist.size()>0){
                                           selectedFriendList.smoothScrollToPosition(itemarraylist.size()-1);
                                       }
                                       selectedFriendList.setAdapter(getselectedfriendadapter);
                                    }
                                };
                                selectFriendforGroupAdapter = new SelectFriendforGroupAdapter(getContext(),mContactslist,selectedrecyclerlistadapter,s);
                                friendList.setAdapter(selectFriendforGroupAdapter);
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void removeItem(int position) {
        itemarraylist.remove(position);
        getselectedfriendadapter.notifyItemRemoved(position);
    }

    private void filter(String text){
        ArrayList<userDetailsClass> filteredlist = new ArrayList<>();
        if(mContactslist.size()!=0){
            for(int i=0;i<mContactslist.size();i++)
            {
                Log.e("list","friend "+mContactslist.get(i).getUD_displayName() );
            }

            for (userDetailsClass item:mContactslist){
                if(item.getUD_displayName().toLowerCase().contains(text.toLowerCase())){
                    filteredlist.add(item);
                }
            }
            selectFriendforGroupAdapter.filterList(filteredlist);
        }
        else{
            System.out.println("kenapa ada sini?" );
        }
    }


}
