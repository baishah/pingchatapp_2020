package com.app.pingchat.Chat;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.R;
import com.app.pingchat.bottomSheetViewImage;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatInfoActivity extends AppCompatActivity implements addNewMember.BottomSheetListener,EditGroupInfoActivity.BottomSheetListener{

    private static final String TAG = "GroupInfoActivity";

//    static int position;
    private DatabaseReference users, chats, chat;
    private CircleImageView groupPhoto;
    private String currenct, groupPic,chatidintent,currentusername;
    private byte[] avatar;
    private FirebaseAuth Mauth;
    private StorageReference groupImage;
    private TextView userName, create_on, memberNum,add_part_txt,groupDes,groupName;
    private Button showBtn,edit_group,exit_txt_,backbtn;
    private ImageButton add_parti_ico;
    private RecyclerView listofmember,media_group_;
    private ArrayList<Messages_G> mediaList = new ArrayList<>();
    private GroupMemberAdapter groupMemberAdapter;
    private Context context;
    private LinearLayout discnophoto;
    ArrayList<userDetailsClass> newList;
    private addNewMember bottomsheetaddMembers;
    private EditGroupInfoActivity editGroupInfoActivity;
    SelectedOwnerAdapter selectedOwnerAdapter;
    boolean checkState[] = new boolean[20];
    private ArrayList<userDetailsClass> member_list=new ArrayList<userDetailsClass>();
    private ArrayList <String> list_of_member = new ArrayList();
    private RequestQueue mQueue;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_info_layout);

        groupName = findViewById(R.id.user_name_);
        userName = findViewById(R.id.user_name);
        groupPhoto = findViewById(R.id.user_photo);
        create_on = findViewById(R.id.date_created);
        memberNum = findViewById(R.id.num_participant);
        groupDes = findViewById(R.id.descrip_text);
        listofmember = findViewById(R.id.friendlistM);
        media_group_ = findViewById(R.id.media_group_);
        showBtn = findViewById(R.id.showMedia);
        edit_group = findViewById(R.id.edit_group);
        add_parti_ico = findViewById(R.id.add_parti_ico);
        add_part_txt = findViewById(R.id.add_part_txt);
        exit_txt_ = findViewById(R.id.exit_txt_);
        discnophoto = findViewById(R.id.no_post_profile);
        backbtn = findViewById(R.id.arrow);
        context = this;
        chatidintent = getIntent().getStringExtra("chatid");
        // doneBtn = findViewById(R.id.saveChanges);

        bottomsheetaddMembers = new addNewMember();
        bottomsheetaddMembers.setListener(this);

        editGroupInfoActivity = new EditGroupInfoActivity();
        editGroupInfoActivity.setListener(this);
        mQueue = Volley.newRequestQueue(getApplicationContext());

        Mauth = FirebaseAuth.getInstance();
        if (Mauth.getCurrentUser() != null) {
            currenct = Mauth.getCurrentUser().getUid();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            chat = FirebaseDatabase.getInstance().getReference().child("Chats");
            chats = users.child(currenct).child("userConversations").child("Group");
            chats.keepSynced(true);
            groupImage = FirebaseStorage.getInstance().getReference().child("groupProfileImage");
            listofmember.setLayoutManager(new LinearLayoutManager(context));
            media_group_.setLayoutManager(new GridLayoutManager(getApplicationContext(),3));

            getcurrentusernamefromdb(currenct);
            getgroupinfo();
            showAddBtn(chatidintent);
            getmember();
            getMedia();
        }

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ShowAllMedia.class);
                i.putExtra("chatId",chatidintent);
                i.putExtra("uid",currenct);
                i.putExtra("key","group");
                startActivity(i);
            }
        });
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        add_parti_ico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewParticipant(chatidintent);
            }
        });
        edit_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), EditGroupInfoActivity.class);
//                i.putExtra("chatId",chatidintent);
//                i.putExtra("key","group");
//                startActivity(i);

                Bundle args = new Bundle();
                args.putString("key", "group");
                args.putString("chatId", chatidintent);
                args.putString("uid", currenct);
                EditGroupInfoActivity dialog = new EditGroupInfoActivity();
                dialog.setArguments(args);
                dialog.show(getSupportFragmentManager(),"c");
            }
        });

        exit_txt_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leftGroupChat(chatidintent);
            }
        });
    }

    private void leftGroupChat(String chatid) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String new_messageid = chats.push().push().getKey();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Left group, are you sure?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userDetailsClass owner =  findmemberusingenhancedloop("owner",member_list);
                        userDetailsClass admin = findmemberusingenhancedloop("admin",member_list);
                        String m = member_list.get(0).getUD_userID();
                        if(owner!=null){
                            if(currenct.equals(owner.getUD_userID())){
                                if(admin!=null){
                                  System.out.println("admin ada juga, you can exit");
                                  beforeleft(chatid,new_messageid,timestamp);
                                }
                                else{
                                    System.out.println("will assign admin randomly");
                                    beforeleft(chatid,new_messageid,timestamp);
                                    chat.child("Group").child(chatid).child("CG_groupMember").child(m).child("CG_role").setValue("admin");
                                }
                            }
                            else{
                                System.out.println("not owner, can exit");
                                beforeleft(chatid,new_messageid,timestamp);
                            }
                        }
                        else{
                            if(admin!=null){
                                System.out.println("admin ada juga, you can exit");
                                beforeleft(chatid,new_messageid,timestamp);
                            }
                            else{
                                beforeleft(chatid,new_messageid,timestamp);
                                chat.child("Group").child(chatid).child("CG_groupMember").child(m).child("CG_role").setValue("admin");
                                System.out.println("will assign admin randomly");
                            }
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public userDetailsClass findmemberusingenhancedloop(String role,ArrayList<userDetailsClass> member_list){
        for(userDetailsClass member:member_list){
            if(member.getCG_Role().equals(role)){
                return member;
            }
        }
        return null;
    }

    public void beforeleft(String chatid,String new_messageid,Timestamp timestamp){
        HashMap group_chat = new HashMap();
        group_chat.put("CG_message",getCurrentusername()+" left");
        group_chat.put("CG_messageSenderID",currenct);
        group_chat.put("CG_mediaType","status");
        group_chat.put("CG_messageTimestamp",timestamp.getTime());
        chat.child("Group").child(chatid).child("CG_groupChatSession").child(new_messageid).setValue(group_chat).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), " Success..", Toast.LENGTH_SHORT).show();
                chat.child("Group").child(chatid).child("CG_groupMember").child(currenct).removeValue();
                chats.child(chatid).removeValue();
                getMember(chatid);
                chat.child("Group").child(chatid).child("CG_groupDetails").child("CG_lastMessage").setValue(getCurrentusername()+ " left");
                chat.child("Group").child(chatid).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
                chat.child("Group").child(chatid).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
                chat.child("Group").child(chatid).child("CG_groupDetails").child("CG_lastMessageSenderID").setValue(currenct);
                for(String i:list_of_member){
                    users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastMessageID").setValue(new_messageid);
//                                                          users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
                }
                Toast.makeText(context, "You left..", Toast.LENGTH_SHORT).show();
                finish();
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("completedgroup", "chats");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
    }
    public ArrayList getMember(String chatid){
        chat.child("Group").child(chatid).child("CG_groupMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String member = dataSnapshot1.getKey();
                    list_of_member.add(member);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        return  list_of_member;
    }

    private void showAddBtn(String chatId){
        chat.child("Group").child(chatId).child("CG_groupMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("not exist");
                }else{
                    for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                        String memberId = dataSnapshot2.getKey();
                        System.out.println("memberId:"+memberId);
                        if(currenct.equals(memberId)){
                            String gRole = dataSnapshot.child(memberId).child("CG_role").getValue().toString();
                            System.out.println("role:"+gRole);
                            if(gRole.equals("owner")){
                                add_parti_ico.setVisibility(View.VISIBLE);
                                edit_group.setVisibility(View.VISIBLE);
                                add_part_txt.setVisibility(View.VISIBLE);
                            }else if(gRole.equals("admin")){
                                add_parti_ico.setVisibility(View.VISIBLE);
                                edit_group.setVisibility(View.VISIBLE);
                                add_part_txt.setVisibility(View.VISIBLE);
                            }
                            else{
                                add_parti_ico.setVisibility(View.GONE);
                                edit_group.setVisibility(View.GONE);
                                add_part_txt.setVisibility(View.GONE);
                            }
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void addNewParticipant(String chatId){
        Bundle args = new Bundle();
        args.putString("key", chatId);
        addNewMember dialog = new addNewMember();
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(),"c");
        System.out.println("dapat_tekan");
    }

    private void getMedia() {
        mediaList = new ArrayList<>();
        chat.child("Group").child(chatidintent).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("not exist");
                }else{
                    DataSnapshot groupSession = dataSnapshot.child("CG_groupChatSession");
                    System.out.println("child:"+groupSession);
                    if(groupSession.exists()){
                        for (DataSnapshot dataSnapshot2 : groupSession.getChildren()) {
                            final Messages_G mesagge = new Messages_G();
                            String msgId = dataSnapshot2.getKey();
                            System.out.println("msgId:"+msgId);
                            chat.child("Group").child(chatidintent).child("CG_groupChatSession").child(msgId).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String msgType = dataSnapshot.child("CG_mediaType").getValue().toString();
                                    System.out.println("msgType:"+msgType);
                                    Messages_G m = dataSnapshot.getValue(Messages_G.class);
                                    System.out.println("mesage:"+m);
                                    if(msgType.equals("image")){
                                        String mediaType = m.getCG_mediaType();
                                        System.out.println("mediaType:"+mediaType);
                                        String mediaUrl = m.getCG_mediaURL();
                                        mesagge.setCG_mediaType(mediaType);
                                        mesagge.setCG_mediaURL(mediaUrl);
                                        mediaList.add(mesagge);
                                        System.out.println("mediaList:"+mediaList.size());
                                        mediaGAdapter adapter = new mediaGAdapter(getApplicationContext(),mediaList);
                                        media_group_.setAdapter(adapter);
                                    }
                                    if(mediaList.isEmpty()){
                                        discnophoto.setVisibility(View.VISIBLE);
                                    }else{
                                        discnophoto.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getmember(){
        member_list = new ArrayList<userDetailsClass>();
        ArrayList memberlist = new ArrayList();
        chat.child("Group").child(chatidintent).child("CG_groupMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int total = 0;
                for (DataSnapshot dataSnapshot3 : dataSnapshot.getChildren()) {
                    final userDetailsClass userDetailsClass = new userDetailsClass();
                    String friend = dataSnapshot3.getValue().toString();
                    total = total + 1;
                    String num = String.valueOf(total);
                    memberNum.setText(num);
                    if (dataSnapshot3.hasChild("CG_role")) {
                        String friendid = dataSnapshot3.getKey();
                        String role = dataSnapshot3.child("CG_role").getValue().toString();
                        Log.e(TAG, "Role" + role);

                        userDetailsClass.setUD_userID(friendid);
                        userDetailsClass.setCG_Role(role);
                        member_list.add(userDetailsClass);
                        System.out.println("member_list: "+member_list);
//                        if(role.equals("owner")){
                            users.child(friendid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if ((dataSnapshot.exists())) {
                                        String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                        String avatarurl = dataSnapshot.child("UD_avatarURL").getValue().toString();
//                                        userName.setText(name);
//                                        if (role.equals("owner")) {
//                                            userName.setText(name);
//                                            System.out.println("friendid_group:"+friendid);
//                                            System.out.println("name:"+name);
//                                            Log.e(TAG, "admin" + name);
//                                        }else{
//                                            userName.setText("Someone");
//
//                                        }
                                        userDetailsClass.setUD_userID(friendid);
                                        userDetailsClass.setUD_displayName(name);
                                        userDetailsClass.setUD_avatarURL(avatarurl);
                                        memberlist.add(userDetailsClass);
                                        selectedOwnerAdapter = new SelectedOwnerAdapter(context,memberlist,currenct,chatidintent);
//                                        groupMemberAdapter = new GroupMemberAdapter(context,memberlist,currenct,chatidintent);
                                        listofmember.setAdapter(selectedOwnerAdapter);
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                            });
//                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

    private void getgroupinfo() {
        chat.child("Group").child(chatidintent).child("CG_groupDetails").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if ((dataSnapshot.exists())) {
                    if(dataSnapshot.hasChild("CG_groupName")){
                        String gname = dataSnapshot.child("CG_groupName").getValue().toString();
                        groupName.setText(gname);
//                        groupName.setSelection(groupName.getText().length());
                        System.out.println("group name:"+gname);
                    }
                    if(dataSnapshot.hasChild("CG_groupDescription")){
                        String gdes = dataSnapshot.child("CG_groupDescription").getValue().toString();
                        groupDes.setText(gdes);
                        System.out.println("group des:"+gdes);
//                        groupDes.setSelection(groupDes.getText().length());
                    }
                    if(dataSnapshot.hasChild("CG_groupProfileURL")){
                        groupPic = dataSnapshot.child("CG_groupProfileURL").getValue().toString();
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(groupPic);
                        GlideApp.with(getApplicationContext())
                                .load(storageReference)
                                .into(groupPhoto);
                    }
                    if(dataSnapshot.hasChild("CG_createdOn")){
                        String longV = dataSnapshot.child("CG_createdOn").getValue().toString();
                        long millisecond = Long.parseLong(longV);
                        SimpleDateFormat formatter = new SimpleDateFormat("  dd/MM/yyyy");//formating according to my need
                        String date = formatter.format(millisecond);
                        create_on.setText(date);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        chat.child("Group").child(chatidintent).child("CG_groupMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot3 : dataSnapshot.getChildren()) {
                    final userDetailsClass userDetailsClass = new userDetailsClass();
                    if (dataSnapshot3.hasChild("CG_role")) {
                        String friendid = dataSnapshot3.getKey();
                        String role = dataSnapshot3.child("CG_role").getValue().toString();
                        if(role.equals("owner")){
                        users.child(friendid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if ((dataSnapshot.exists())) {
                                    String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                        userName.setText(name);
                                        if (role.equals("owner")) {
                                            userName.setText(name);
                                        }else{
                                            userName.setText("Someone");
                                        }
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

//    public void editdone() {
//        final String gname = groupName.getText().toString().trim();
//        final String gdes = groupDes.getText().toString().trim();
//        chat.child("Group").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
//                        String chatId = dataSnapshot1.getKey();
//                        chat.child("Group").child(chatId).child("CG_groupDetails").child("CG_groupName").setValue(gname).addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                if (task.isSuccessful()) {
//                                    chat.child("Group").child(chatId).child("CG_groupDetails").child("CG_description").setValue(gdes).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isSuccessful()) {
////                                                uploadNewImage();
//                                                finish();
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        });
//                    }
//                }
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {}
//        });
//    }

    public String getCurrentusername() {
        return currentusername;
    }

    public void setCurrentusername(String currentusername) {
        this.currentusername = currentusername;
    }

    public void getcurrentusernamefromdb(String id){
        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("UD_displayName")){
                    String displayname = dataSnapshot.child("UD_displayName").getValue().toString();
                    setCurrentusername(displayname);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

    @Override
    public void onButtonaddMember(String text){
        switch (text){
            case "recyclersss":
                System.out.println(text);
                break;
        }
    }
    @Override
    public void onClickImage(String text) {

    }
}