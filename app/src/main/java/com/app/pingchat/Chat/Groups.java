package com.app.pingchat.Chat;

public class Groups {
    public String chatType;
    public String groupChatID;
    public String groupName;
    public String groupProfileURL;
    public String lastMessage;
    public long lastMessageDate;
    public String lastMessageSender;
    public String lastMessageType;
    public String date;

    public Groups(){}

    public Groups(String chatType, String groupChatID, String groupName, String groupProfileURL, String lastMessage, long lastMessageDate, String lastMessageSender, String lastMessageType) {
        this.chatType = chatType;
        this.groupChatID = groupChatID;
        this.groupName = groupName;
        this.groupProfileURL = groupProfileURL;
        this.lastMessage = lastMessage;
        this.lastMessageDate = lastMessageDate;
        this.lastMessageSender = lastMessageSender;
        this.lastMessageType = lastMessageType;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getGroupChatID() {
        return groupChatID;
    }

    public void setGroupChatID(String groupChatID) {
        this.groupChatID = groupChatID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupProfileURL() {
        return groupProfileURL;
    }

    public void setGroupProfileURL(String groupProfileURL) {
        this.groupProfileURL = groupProfileURL;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public long getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(long lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public String getLastMessageSender() {
        return lastMessageSender;
    }

    public void setLastMessageSender(String lastMessageSender) {
        this.lastMessageSender = lastMessageSender;
    }

    public String getLastMessageType() {
        return lastMessageType;
    }

    public void setLastMessageType(String lastMessageType) {
        this.lastMessageType = lastMessageType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
