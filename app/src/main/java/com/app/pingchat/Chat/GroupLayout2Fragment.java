package com.app.pingchat.Chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.Friends;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class GroupLayout2Fragment extends Fragment implements View.OnClickListener {

    FirebaseAuth mAuth;
    private DatabaseReference friends,users,userFriends;
    private ListView listView;
    private String current_id;
    private RecyclerView friendList,selectedFriendList;
    private EditText search_friend;



    ArrayList<Friends> mContactslist;
    SelectFriendforGroupAdapter selectFriendforGroupAdapter;
    com.app.pingchat.Chat.getselectedfriendadapter getselectedfriendadapter;

    ArrayList<String> friendskey;
    ArrayList<String> alluserid;

    ImageButton nexts;

    FirebaseStorage storage;
    StorageReference storeRef;


    List<Friends> mlist=new ArrayList<>();

    String av;

    public String getAv() {
        return av;
    }

    public void setAv(String av) {
        this.av = av;
    }

    private static GroupLayout2Fragment instance = null;
    private ArrayList<Friends> itemarraylist=new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.group_2_layout,container,false);



        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.next:
                System.out.println("id next clicked");
                break;
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }




}
