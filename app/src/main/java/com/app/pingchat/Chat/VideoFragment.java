package com.app.pingchat.Chat;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class VideoFragment extends Fragment {
    RecyclerView videoRv;
    private ArrayList<Messages> mediaList = new ArrayList<>();
    private ArrayList<Messages_G> mediaGList = new ArrayList<>();
    private FirebaseAuth Mauth;
    private String currenct,chatId,key;
    private DatabaseReference users, chats, chat;
    LinearLayoutManager manager;
    private LinearLayout discnophoto;
    public static final String MyPREFERENCES = "MyPrefs" ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rv = inflater.inflate(R.layout.image_fragment, container, false);

        videoRv = rv.findViewById(R.id.image_recycler);
        discnophoto = rv.findViewById(R.id.no_post_profile);

        SharedPreferences prefs = getContext().getSharedPreferences(MyPREFERENCES , MODE_PRIVATE);
        chatId = prefs.getString("chatid", null);
        key = prefs.getString("key", null);

        Mauth = FirebaseAuth.getInstance();
        if (Mauth.getCurrentUser() != null) {
            currenct = Mauth.getCurrentUser().getUid();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            chat = FirebaseDatabase.getInstance().getReference().child("Chats");
            chats = users.child(currenct).child("userConversations").child("Group");
            chats.keepSynced(true);
//            userImage = FirebaseStorage.getInstance().getReference().child("profile");
        }

        videoRv.setLayoutManager(new GridLayoutManager(getContext(),3));
//        imageRv.setLayoutManager(manager);
        System.out.println("key value:"+key);
        if(key.equals("personal")){
            getMedia(chatId);
        }else{
            getGMedia(chatId);
        }
        return rv;
    }

    private void getGMedia(String chatid) {
        mediaGList = new ArrayList<>();
        chat.child("Group").child(chatid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("not exist");
                }else{
                    DataSnapshot groupSession = dataSnapshot.child("CG_groupChatSession");
                    System.out.println("child:"+groupSession);
                    if(groupSession.exists()){
                        for (DataSnapshot dataSnapshot2 : groupSession.getChildren()) {
                            final Messages_G mesaggeG = new Messages_G();
                            String msgId = dataSnapshot2.getKey();
                            System.out.println("msgGId:"+msgId);
                            chat.child("Group").child(chatid).child("CG_groupChatSession").child(msgId).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String msgType = dataSnapshot.child("CG_mediaType").getValue().toString();
                                    Messages_G m = dataSnapshot.getValue(Messages_G.class);

                                    if(msgType.equals("video")){
                                        System.out.println("msgGID1:"+msgId);
                                        discnophoto.setVisibility(View.GONE);
                                        String mediathumb = m.getCG_mediaThumbnailURL();
                                        String mediaUrl = m.getCG_mediaURL();
                                        mesaggeG.setCG_mediaThumbnailURL(mediathumb);
                                        mesaggeG.setCG_mediaURL(mediaUrl);

                                        mediaGList.add(mesaggeG);
                                        VideoGAdapter adapter = new VideoGAdapter(getContext(),mediaGList);
                                        videoRv.setAdapter(adapter);

                                        System.out.println("mediaGList:" + mediaList.size());
                                    }
                                    if(mediaGList.size()==0){
                                        discnophoto.setVisibility(View.VISIBLE);
                                    }else{
                                        discnophoto.setVisibility(View.GONE);
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getMedia(String chatid) {
        mediaList = new ArrayList<>();
        chat.child("Personal").child(chatid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("not exist");
                }else{
                    DataSnapshot personalSession = dataSnapshot.child("CP_personalChatSession");
                    System.out.println("child:"+personalSession);
                    if(personalSession.exists()){
                        for (DataSnapshot dataSnapshot2 : personalSession.getChildren()) {
                            final Messages mesagge = new Messages();
                            String msgId = dataSnapshot2.getKey();
                            chat.child("Personal").child(chatid).child("CP_personalChatSession").child(msgId).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String msgType = dataSnapshot.child("CP_mediaType").getValue().toString();
                                    System.out.println("msgType:"+msgType);
                                    Messages m = dataSnapshot.getValue(Messages.class);
                                    System.out.println("mesage:"+m);
                                    if(msgType.equals("video")){
                                        discnophoto.setVisibility(View.GONE);
                                        String mediaType = m.getCP_mediaType();
                                        String mediathumb = m.getCP_mediaThumbnailURL();
                                        String mediaUrl = m.getCP_mediaURL();
                                        mesagge.setCP_mediaType(mediaType);
                                        mesagge.setCP_mediaThumbnailURL(mediathumb);
                                        mesagge.setCP_mediaURL(mediaUrl);

                                        mediaList.add(mesagge);
                                        videoAdapter adapter = new videoAdapter(getContext(),mediaList);
                                        videoRv.setAdapter(adapter);
                                    }
                                    System.out.println("mediaList:"+mediaList.size());
                                    if(mediaList.size()==0){
                                        discnophoto.setVisibility(View.VISIBLE);
                                    }else{
                                        discnophoto.setVisibility(View.GONE);

                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
