package com.app.pingchat.Chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class getselectedfriendadapter extends RecyclerView.Adapter<getselectedfriendadapter.ViewHolder>{

    private Context context;
    private List<userDetailsClass> list;
    String userid;
    private getselectedrecyclerlistadapter mclicklistener;

    public getselectedfriendadapter(Context context, List<userDetailsClass> list,getselectedrecyclerlistadapter clicklistener){
        this.context = context;
        this.list = list;
        this.mclicklistener = clicklistener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.selected_user_forgroup_layout,parent,false);
        return new ViewHolder(v,mclicklistener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final userDetailsClass f = list.get(position);

        System.out.println("f.getavatarurl"+f.getUD_avatarURL());

        if(f.getUD_avatarURL().startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(f.getUD_avatarURL())
                    .into(holder.i);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(f.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView businessPartnerID, businessCreator, businessName,businessRegister,businessLastUpdate,businessPhone,businessEmail,businessLatitude,businessLongitude,
        businessActualAddress,businessOnMapAddress,businessCategory,businessProfileURL;

        CircleImageView i;
        ImageButton canceluser;
        getselectedrecyclerlistadapter mclicklistener;

        public ViewHolder(View itemView,getselectedrecyclerlistadapter clicklistener) {
            super(itemView);
            i = itemView.findViewById(R.id.user_dp);
            canceluser = itemView.findViewById(R.id.canceluser);
            canceluser.setOnClickListener(this);
            mclicklistener = clicklistener;

        }


        @Override
        public void onClick(View v) {
            mclicklistener.onUserClicked(getAdapterPosition());
//            list.remove(getAdapterPosition());
//            notifyItemRemoved(getAdapterPosition());
        }
    }

   public int getposition(String valuea){
        int length = list.size();
        for (int i =0;i<length;i++){
            if(list.get(i).getUD_userID().equals(valuea)){
                return i;
            }
        }
       return -1;
   }

    public interface getselectedrecyclerlistadapter{
        void onUserClicked(int position);
    }

    public List<userDetailsClass> getItemArraylist() {
        System.out.println("dalam getitemarraylist"+list);
        return list;
    }

}
