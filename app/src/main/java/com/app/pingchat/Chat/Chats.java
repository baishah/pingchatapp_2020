package com.app.pingchat.Chat;

import com.google.firebase.storage.StorageReference;

import java.util.Comparator;
import java.util.Date;

public class Chats{

    String chatID;
    String chatName;
    String chatProfile;
    String chatType;
    String lastMessage;
    String messageType;
    String sender;
    String groupChatID;
    String groupName;
    String groupProfileURL;
    String lastChatType;
    String lastMessageSender;
    String lastMessageType;

    String avatar;
    String name;
    String message;
    String date;
    String friendid;
    Date datetocompare;
    String type;

    StorageReference storageReference;

    Long messageDate,lastMessageDate;

    public Chats(StorageReference s,String friendid,String type,Date datetocompare,String chatID, String chatName, String chatProfile, String chatType, String lastMessage, String messageType, String sender, String groupChatID, String groupName, String groupProfileURL, String lastChatType, String lastMessageSender, String lastMessageType, String avatar, String name, String message, String date, Long messageDate, Long lastMessageDate) {
        this.chatID = chatID;
        this.chatName = chatName;
        this.chatProfile = chatProfile;
        this.chatType = chatType;
        this.lastMessage = lastMessage;
        this.messageType = messageType;
        this.sender = sender;
        this.groupChatID = groupChatID;
        this.groupName = groupName;
        this.groupProfileURL = groupProfileURL;
        this.lastChatType = lastChatType;
        this.lastMessageSender = lastMessageSender;
        this.lastMessageType = lastMessageType;
        this.avatar = avatar;
        this.name = name;
        this.message = message;
        this.date = date;
        this.messageDate = messageDate;
        this.lastMessageDate = lastMessageDate;
        this.datetocompare = datetocompare;
        this.type = type;
        this.friendid = friendid;
        this.storageReference = s;
    }


    public Chats(){}

    public StorageReference getStorageReference() {
        return storageReference;
    }

    public void setStorageReference(StorageReference storageReference) {
        this.storageReference = storageReference;
    }

    public String getChatID() {
        return chatID;
    }

    public void setChatID(String chatID) {
        this.chatID = chatID;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public String getChatProfile() {
        return chatProfile;
    }

    public void setChatProfile(String chatProfile) {
        this.chatProfile = chatProfile;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getGroupChatID() {
        return groupChatID;
    }

    public void setGroupChatID(String groupChatID) {
        this.groupChatID = groupChatID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupProfileURL() {
        return groupProfileURL;
    }

    public void setGroupProfileURL(String groupProfileURL) {
        this.groupProfileURL = groupProfileURL;
    }

    public String getLastChatType() {
        return lastChatType;
    }

    public void setLastChatType(String lastChatType) {
        this.lastChatType = lastChatType;
    }

    public String getLastMessageSender() {
        return lastMessageSender;
    }

    public void setLastMessageSender(String lastMessageSender) {
        this.lastMessageSender = lastMessageSender;
    }

    public String getLastMessageType() {
        return lastMessageType;
    }

    public void setLastMessageType(String lastMessageType) {
        this.lastMessageType = lastMessageType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Long messageDate) {
        this.messageDate = messageDate;
    }

    public Long getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(Long lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public Date getDatetocompare() {
        return datetocompare;
    }

    public void setDatetocompare(Date datetocompare) {
        this.datetocompare = datetocompare;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFriendid() {
        return friendid;
    }

    public void setFriendid(String friendid) {
        this.friendid = friendid;
    }

    public static Comparator<Chats> chatdatecomparator = new Comparator<Chats>() {
        @Override
        public int compare(Chats o1, Chats o2) {
            String chatname1 = o1.getName().toUpperCase();
            String chatname2 = o2.getName().toUpperCase();

            return chatname2.compareTo(chatname1);
        }
    };

    public static Comparator<Chats> chatdatecomparator1 = new Comparator<Chats>() {
        @Override
        public int compare(Chats o1, Chats o2) {
         return o2.getDatetocompare().compareTo(o1.getDatetocompare());
        }
    };
}
