package com.app.pingchat.Chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.GlideApp;
import com.app.pingchat.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class mediaAdapter extends RecyclerView.Adapter<mediaAdapter.MyViewHolder> {
    Context context;
    ArrayList<Messages> mediaList;
//    private final int limit = 3;

    public mediaAdapter(Context c, ArrayList<Messages> f){
        context = c;
        mediaList = f;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(context).inflate(R.layout.post_image_inprofile,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Messages msg = mediaList.get(position);
//        String msgType = msg.getCP_mediaType();
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        holder.image.setVisibility(View.GONE);

//        if (msg.getCP_mediaType().equals("image")){
        holder.image.setVisibility(View.VISIBLE);
        if(msg.getCP_mediaURL().startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(msg.getCP_mediaURL())
                    .placeholder(circularProgressDrawable)
                    .into(holder.image);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(msg.getCP_mediaURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .placeholder(circularProgressDrawable)
                    .into(holder.image);
        }
        System.out.println("mediaList_adapter:"+mediaList.size());
//        }
//        else if(msgType.equals("video")){
////            holder.image.setVisibility(View.GONE);
//            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(msg.getCP_mediaThumbnailURL());
//            GlideApp.with(context.getApplicationContext())
//                    .load(storageReference)
//                    .placeholder(circularProgressDrawable)
//                    .into(holder.image);
//        }
//        else if(msgType.equals("audio")){ holder.image.setVisibility(View.GONE);}
//        else if(msgType.equals("pdf")){ holder.image.setVisibility(View.GONE);}
//        else if(msgType.equals("text")){ holder.image.setVisibility(View.GONE);}
//        else if(msgType.equals("video")){ holder.image.setVisibility(View.GONE);}
//        else {holder.image.setVisibility(View.GONE);}

    }

    @Override
    public int getItemCount() {
        return mediaList.size();
//        return Math.min(mediaList.size(), 3);
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.postimage);
        }
    }
}
