package com.app.pingchat.Chat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.Profile.ViewUserProfileActivity;
import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.lang.reflect.Array;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupMemberAdapter extends RecyclerView.Adapter<GroupMemberAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,notification;
    Context context;
    ArrayList<userDetailsClass> mContactslist;
    private String currentuser;
    Array datas;
    Friends[] baru;
    //private List<Friends> mlist;
    Friends[] mlist;
    private FirebaseAuth mAuths;

    public GroupMemberAdapter(ArrayList<userDetailsClass> Friendlist){
        mContactslist = Friendlist;
    }

    public GroupMemberAdapter(Context c, ArrayList<userDetailsClass> f, String y){
        context = c;
        mContactslist = f;
        currentuser = y;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.friends_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        mAuths = FirebaseAuth.getInstance();
        currentuser = mAuths.getCurrentUser().getUid();

        final userDetailsClass mynewfriend = mContactslist.get(position);
        holder.u.setText(mynewfriend.getUD_displayName());
        holder.s.setText(mynewfriend.getUD_aboutMe());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mynewfriend.getUD_userID().equals(currentuser)){
                    Intent i = new Intent(context, MainActivity.class);
                    i.putExtra("id", "profile");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }else{
                    Intent i = new Intent(context, ViewUserProfileActivity.class);
                    i.putExtra("id", mynewfriend.getUD_userID());
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
            }
        });

        if(mynewfriend.getUD_avatarURL().startsWith("p")){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(mynewfriend.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }
        else{
            GlideApp.with(context.getApplicationContext())
                    .load(mynewfriend.getUD_avatarURL())
                    .into(holder.i);
        }
    }

    @Override
    public int getItemCount() {
        return mContactslist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView u,s,p;
        CircleImageView i;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            i = itemView.findViewById(R.id.gambar);

        }
    }
    public void filterList(ArrayList<userDetailsClass> filteredlist){
        mContactslist = filteredlist;
        notifyDataSetChanged();
    }
}
