package com.app.pingchat.Chat;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class pdfFragment extends Fragment {
    RecyclerView pdfRv;
    private ArrayList<Messages> mediaList = new ArrayList<>();
    private ArrayList<Messages_G> mediaGList = new ArrayList<>();
    private FirebaseAuth Mauth;
    private String currenct,chatId,key;
    private DatabaseReference users, chats, chat;
    LinearLayoutManager manager;
    private LinearLayout discnophoto;
    public static final String MyPREFERENCES = "MyPrefs" ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rv = inflater.inflate(R.layout.image_fragment, container, false);

        pdfRv = rv.findViewById(R.id.image_recycler);
        discnophoto = rv.findViewById(R.id.no_post_profile);

        manager = new LinearLayoutManager(getContext());

        SharedPreferences prefs = getContext().getSharedPreferences(MyPREFERENCES , MODE_PRIVATE);
        chatId = prefs.getString("chatid", null);
        key = prefs.getString("key", null);

        Mauth = FirebaseAuth.getInstance();
        if (Mauth.getCurrentUser() != null) {
            currenct = Mauth.getCurrentUser().getUid();
            users = FirebaseDatabase.getInstance().getReference().child("Users");
            users.keepSynced(true);
            chat = FirebaseDatabase.getInstance().getReference().child("Chats");
            chats = users.child(currenct).child("userConversations").child("Group");
            chats.keepSynced(true);
            //            userImage = FirebaseStorage.getInstance().getReference().child("profile");
        }

//            pdfRv.setLayoutManager(new GridLayoutManager(getContext(),3));
        pdfRv.setLayoutManager(manager);
        System.out.println("key value:"+key);
        if(key.equals("personal")){
            getMedia(chatId);
        }else{
            getGMedia(chatId);
        }

        return rv;
    }

    private void getGMedia(String chatid) {
        mediaGList = new ArrayList<>();
        chat.child("Group").child(chatid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("not exist");
                }else{
                    DataSnapshot groupSession = dataSnapshot.child("CG_groupChatSession");
                    System.out.println("child:"+groupSession);
                    if(groupSession.exists()){
                        for (DataSnapshot dataSnapshot2 : groupSession.getChildren()) {
                            final Messages_G mesagge = new Messages_G();
                            String msgId = dataSnapshot2.getKey();

                            chat.child("Group").child(chatid).child("CG_groupChatSession").child(msgId).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String msgType = dataSnapshot.child("CG_mediaType").getValue().toString();
                                    System.out.println("msgType:"+msgType);
                                    Messages_G m = dataSnapshot.getValue(Messages_G.class);
                                    System.out.println("mesage:"+m);
                                    if(msgType.equals("pdf")){
                                        String mediaUrl = m.getCG_mediaURL();
                                        System.out.println("msgIdpdf:"+msgId);
                                        mesagge.setCG_mediaURL(mediaUrl);

                                        mediaGList.add(mesagge);
                                        pdfGAdapter adapter = new pdfGAdapter(getContext(),mediaGList);
                                        pdfRv.setAdapter(adapter);
                                        System.out.println("mediaListpdf:"+mediaGList.size());
                                    }

                                    if(mediaGList.size()==0){
                                        discnophoto.setVisibility(View.VISIBLE);
                                    }else{
                                        discnophoto.setVisibility(View.GONE);
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getMedia(String chatid) {
        mediaList = new ArrayList<>();
        chat.child("Personal").child(chatid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    System.out.println("not exist");
                }else{
                    DataSnapshot personalSession = dataSnapshot.child("CP_personalChatSession");
                    System.out.println("child:"+personalSession);
                    if(personalSession.exists()){
                        for (DataSnapshot dataSnapshot2 : personalSession.getChildren()) {
                            final Messages mesagge = new Messages();
                            String msgId = dataSnapshot2.getKey();

                            chat.child("Personal").child(chatid).child("CP_personalChatSession").child(msgId).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String msgType = dataSnapshot.child("CP_mediaType").getValue().toString();
                                    System.out.println("msgType:"+msgType);
                                    Messages m = dataSnapshot.getValue(Messages.class);
                                    System.out.println("mesage:"+m);
                                    if(msgType.equals("pdf")){
                                        String mediaUrl = m.getCP_mediaURL();
                                        System.out.println("msgIdpdf:"+msgId);
                                        mesagge.setCP_mediaURL(mediaUrl);

                                        mediaList.add(mesagge);
                                        pdfAdapter adapter = new pdfAdapter(getContext(),mediaList);
                                        pdfRv.setAdapter(adapter);
                                    }
                                    if(mediaList.size()==0){
                                        discnophoto.setVisibility(View.VISIBLE);
                                    }else{
                                        discnophoto.setVisibility(View.GONE);
                                    }
                                    System.out.println("mediaList:"+mediaList.size());
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
