package com.app.pingchat.Chat;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.Profile.ViewUserProfileActivity;
import com.app.pingchat.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class addMemberAdapter extends RecyclerView.Adapter<addMemberAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,chat,chats;
    Context context;
    ArrayList<userDetailsClass> mContactslist;
    private String currentUser,chatId,userRole,currentusername;

    private OnAddListener mOnAddListener;

    public addMemberAdapter(ArrayList<userDetailsClass> Friendlist){
        mContactslist = Friendlist;

    }

    public addMemberAdapter(Context c, ArrayList<userDetailsClass> f, String y,String z, OnAddListener onAddListener){
        context = c;
        mContactslist = f;
        currentUser = y;
        chatId = z;
        this.mOnAddListener = onAddListener;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.add_friend_card,parent,false);
        return new MyViewHolder(v,mOnAddListener);
        // return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.select_friend_for_group_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final addMemberAdapter.MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        chat = FirebaseDatabase.getInstance().getReference().child("Chats");
        chats = usersRef.child(currentUser).child("userConversations").child("Group");
        chats.keepSynced(true);

        final userDetailsClass mynewfriend = mContactslist.get(position);
        holder.u.setText(mynewfriend.getUD_displayName());
        String avatar = mynewfriend.getUD_avatarURL();
        String userId = mynewfriend.getUD_userID();
        String name = mynewfriend.getUD_displayName();

        holder.p.setVisibility(View.GONE);

        if(avatar.startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(avatar)
                    .into(holder.i);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }
    }

    @Override
    public int getItemCount() {
        return mContactslist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView u,s,p;
        CircleImageView i;
        Button add;
        OnAddListener onAddListener;
        public MyViewHolder(View itemView, OnAddListener onAddListener) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            p = itemView.findViewById(R.id.line3);
            i = itemView.findViewById(R.id.gambar);
            add = itemView.findViewById(R.id.add);
            this.onAddListener = onAddListener;
            add.setOnClickListener(this);
//            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onAddListener.onAddClick(getAdapterPosition(), chatId);
        }
    }

    public interface OnAddListener{
        void onAddClick(int position, String chatID);
    }
}
