package com.app.pingchat.Chat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.app.pingchat.Discover.UDS_DetailsClass;
import com.app.pingchat.Friend.Friends;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Profile.ViewUserProfileActivity;
import com.app.pingchat.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> implements Filterable{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference chats,users,friend;
    Context context;
    Fragment myfragment;
    ArrayList<ChatListClass> filteredChatListfull; //filter
    ArrayList<ChatListClass> friendChatss; //ori
    MyFilter chatsFilter;

    private OnLongBtnListener mOnlongBtn;

    String groupname,profileurl,profileurlp,lastmessage,lastmessagep,names,currentid, lastsenderid,name,chatType,lastmessageid;

    String urll;

    public ChatListAdapter(ArrayList<ChatListClass> Friendlist){

        this.friendChatss = Friendlist;

    }

    public ChatListAdapter(Fragment pcontext, Context c, ArrayList<ChatListClass> f, String currentid, OnLongBtnListener onLongBtnListener){
        context = c;
        friendChatss = f;
        filteredChatListfull = new ArrayList<ChatListClass>();
        this.currentid = currentid;
        myfragment = pcontext;
        this.mOnlongBtn = onLongBtnListener;
    }


//    public void filterList(ArrayList<ChatListClass> filteredlist){
//        friendChatss = filteredlist;
//        notifyDataSetChanged();
//    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.chat_list_card,parent,false));
        View v = LayoutInflater.from(context).inflate(R.layout.chat_list_card,parent,false);
        return new MyViewHolder(v,mOnlongBtn);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        chats = FirebaseDatabase.getInstance().getReference().child("Chats");
        users = FirebaseDatabase.getInstance().getReference().child("Users");
        friend = FirebaseDatabase.getInstance().getReference().child("Users").child(currentid).child("userFriends");
        friend.keepSynced(true);

        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();

        final String currentuser = mAuth.getCurrentUser().getUid();
        final ChatListClass myfriend = friendChatss.get(position);
        String chatId = myfriend.getChat_id();

        chats.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataSnapshot groupdetails = dataSnapshot.child("Group");
                DataSnapshot personaldetails = dataSnapshot.child("Personal");

                if(myfriend.getChat_type().equals("group")){
                    groupname = groupdetails.child(myfriend.getChat_id()).child("CG_groupDetails").child("CG_groupName").getValue().toString();
                    profileurl = groupdetails.child(myfriend.getChat_id()).child("CG_groupDetails").child("CG_groupProfileURL").getValue().toString();
                    lastmessage = groupdetails.child(myfriend.getChat_id()).child("CG_groupDetails").child("CG_lastMessage").getValue().toString();
                    lastmessageid = groupdetails.child(myfriend.getChat_id()).child("CG_groupDetails").child("CG_lastMessageID").getValue().toString();
                    lastsenderid = groupdetails.child(myfriend.getChat_id()).child("CG_groupDetails").child("CG_lastMessageSenderID").getValue().toString();
                    chatType = groupdetails.child(myfriend.getChat_id()).child("CG_groupChatSession").child(lastmessageid).child("CG_mediaType").getValue().toString();

                    System.out.println("lastmessagegroup "+lastmessage);
                    System.out.println("chat id group"+myfriend.getChat_id());
//                    System.out.println("chatType:"+chatType);
                    holder.o.setVisibility(View.GONE);

                    if(chatType.equals("status")||chatType.equals("notifier")){
                        holder.n.setVisibility(View.GONE);
                    }else{
                        holder.n.setVisibility(View.VISIBLE);
                        if(lastsenderid.equals(currentuser)){
                            holder.n.setText("You"+":");
                        }else{
                            users.child(lastsenderid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    name = dataSnapshot.child("UD_displayName").getValue().toString();
                                    holder.n.setText(name+":");
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                    myfriend.setDisplayname(groupname);
                    myfriend.setDp(profileurl);
                    myfriend.setLastmessage(lastmessage);
                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(profileurl);
                    GlideApp.with(context.getApplicationContext())
                            .asBitmap()
                            .load(storageReference)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .placeholder(R.drawable.loading_img)
                            .into(new SimpleTarget<Bitmap>(100,100) {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    holder.i.setImageBitmap(resource);
                                }
                            });
                    holder.u.setText(groupname);
                    holder.s.setText(myfriend.getLastmessage());

                }
                else if(myfriend.getChat_type().equals("personal")){
                    lastmessagep = personaldetails.child(myfriend.getChat_id()).child("CP_personalDetails").child("CP_lastMessage").getValue().toString();
                    holder.s.setText(lastmessagep);
                    users.child(myfriend.getFriendid()).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            names = dataSnapshot.child("UD_displayName").getValue().toString();
                            profileurlp = dataSnapshot.child("UD_avatarURL").getValue().toString();

                            myfriend.setDisplayname(names);
                            myfriend.setDp(profileurlp);
                            myfriend.setLastmessage(lastmessagep);
                            holder.n.setVisibility(View.GONE);

                            System.out.println("lastmessagepersonal "+lastmessagep);
                            System.out.println("chat id personal"+myfriend.getChat_id());

                            if(profileurlp.startsWith("h")){
                                GlideApp.with(context.getApplicationContext())
                                        .asBitmap()
                                        .load(profileurlp)
                                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                        .placeholder(R.drawable.loading_img)
                                        .into(new SimpleTarget<Bitmap>(100,100) {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                        holder.i.setImageBitmap(resource);
                                    }
                                });
                            }
                            else{
                                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(profileurlp);
                                GlideApp.with(context.getApplicationContext())
                                    .asBitmap()
                                    .load(storageReference)
                                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                    .placeholder(R.drawable.loading_img)
                                    .into(new SimpleTarget<Bitmap>(100,100) {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            holder.i.setImageBitmap(resource);
                                        }
                                    });
                            }
                            holder.u.setText(names);
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                    holder.o.setVisibility(View.VISIBLE);
                    users.child(myfriend.getFriendid()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            DataSnapshot userstatus = dataSnapshot.child("userStatus");
                            String on9status = userstatus.child("US_onlineStatus").getValue().toString();
                            System.out.println("on9status"+on9status);
                            if(on9status.equals("online")){
                                holder.o.setImageResource(R.drawable.on9marker);
                            } else{
                                holder.o.setImageResource(R.drawable.of9marker);
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        holder.t.setText(myfriend.getDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myfriend.getChat_type().equals("personal")){
                    users.child(currentuser).child("userFriends").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                String myFriend = myfriend.getFriendid();
                                String status = dataSnapshot.child(myFriend).getValue(String.class);
                                if (status.equals("friend")){
                                    System.out.println("get friend id "+myfriend.getFriendid());
                                    Intent in = new Intent(context, ChatsActivity.class);
                                    in.putExtra("friendid",myfriend.getFriendid());
                                    in.putExtra("name",myfriend.getDisplayname());
                                    in.putExtra("avatarurl",myfriend.getDp());
                                    in.putExtra("chatid",myfriend.getChat_id());
                                    in.putExtra("type","personal");
                                    myfragment.startActivityForResult(in,123);

                                }else if(status.equals("block")) {
//                                    Toast.makeText(context, "You had blocked this user, please unblock to sent message", Toast.LENGTH_SHORT).show();
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    final AlertDialog alertDialog = builder.create();

                                    View view = LayoutInflater.from(context).inflate(R.layout.unfriends_popup_layout,null);

                                    TextView friendname = view.findViewById(R.id.friend_name);
                                    TextView cancelBtn = view.findViewById(R.id.cancel);
                                    TextView unfriendBtn = view.findViewById(R.id.unfriend_txt);
                                    CircleImageView avatarimg = view.findViewById(R.id.pro_pic);
                                    TextView block = view.findViewById(R.id.un_friend);

                                    block.setText("Unblock");
                                    unfriendBtn.setText("Unblock");

                                    users.child(myFriend).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                            if(dataSnapshot.hasChild("UD_displayName")){
                                                String name = dataSnapshot.child("UD_displayName").getValue().toString();
                                                friendname.setText(name);
                                                System.out.println("name :"+name);
                                            }

                                            if(dataSnapshot.hasChild("UD_avatarURL")){
                                                String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                                                System.out.println("url :"+ avatar);
                                                if(avatar.startsWith("p")){
                                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                                                    GlideApp.with(context)
                                                            .asBitmap()
                                                            .load(storageReference)
                                                            .placeholder(R.drawable.loading_img)
                                                            . into(new SimpleTarget<Bitmap>(100,100) {
                                                                @Override
                                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                                    avatarimg.setImageBitmap(resource);
                                                                }
                                                            });
                                                }
                                                else if(avatar.startsWith("h")){
                                                    GlideApp.with(context)
                                                        .asBitmap()
                                                        .load(avatar).placeholder(R.drawable.loading_img)
                                                        . into(new SimpleTarget<Bitmap>(100,100) {
                                                            @Override
                                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                                avatarimg.setImageBitmap(resource);
                                                            }
                                                        });
                                                }
                                            }

                                            cancelBtn.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    alertDialog.dismiss();
                                                }
                                            });

                                            unfriendBtn.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    alertDialog.dismiss();
//                                                confirmUnblock(myFriend,currentuser);
                                                    ProgressDialog dialog= new ProgressDialog(context);
                                                    dialog.setMessage("Unblock friend.. Please wait");
                                                    dialog.setCancelable(false);
                                                    dialog.setCanceledOnTouchOutside(false);
                                                    dialog.show();

                                                    friend.child(myFriend).setValue("friend").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            users.child(myFriend).child("userFriends").child(currentuser).setValue("friend").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    dialog.dismiss();
                                                                    Toast.makeText(context, "Unblock successfully", Toast.LENGTH_SHORT).show();
                                                                    //                        startActivity(new Intent(ViewUserProfileActivity.this, ViewProfileActivity.class));
                                                                }
                                                            });
                                                        }
                                                    });

                                                }
                                            });

                                        }
                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                    alertDialog.setView(view);
                                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    alertDialog.show();
                                    alertDialog.setCanceledOnTouchOutside(true);
                                    return;
                                }else if(status.equals("blocked")){
                                    Toast.makeText(context, "You had been blocked by this user", Toast.LENGTH_SHORT).show();
                                }
//                                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
//                                    String friendid = dataSnapshot2.getKey();
//                                    String status = dataSnapshot2.getValue().toString();
//                                    if(status.equals("friend")) {
//                                        System.out.println("get friend id "+myfriend.getFriendid());
//                                        Intent in = new Intent(context, ChatsActivity.class);
//                                        in.putExtra("friendid",myfriend.getFriendid());
//                                        in.putExtra("name",myfriend.getDisplayname());
//                                        in.putExtra("avatarurl",myfriend.getDp());
//                                        in.putExtra("chatid",myfriend.getChat_id());
//                                        in.putExtra("type","personal");
//                                        context.startActivity(in);
//                                    }else if(status.equals("block")) {
//                                        Toast.makeText(context, "You had blocked this user, please unblock to sent message", Toast.LENGTH_SHORT).show();
//                                    }else if(status.equals("blocked")){
//                                        Toast.makeText(context, "You had been blocked by this user", Toast.LENGTH_SHORT).show();
//                                    }
//                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
                else if(myfriend.getChat_type().equals("group")){
                    Intent in = new Intent(context, ChatsActivity.class);
                    in.putExtra("name",myfriend.getDisplayname());
                    in.putExtra("avatarurl",myfriend.getDp());
                    in.putExtra("chatid",myfriend.getChat_id());
                    in.putExtra("type","group");
                    myfragment.startActivityForResult(in,123);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return friendChatss.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener
    {
        TextView u,s,p,t,n;
        CircleImageView i;
        Button add;
        ImageView o;
        OnLongBtnListener onLongBtn;
        public MyViewHolder(View itemView, OnLongBtnListener onLongBtn) {
            super(itemView);

            u = itemView.findViewById(R.id.name);
            s = itemView.findViewById(R.id.message);
            i = itemView.findViewById(R.id.gambar);
            t = itemView.findViewById(R.id.time);
            n = itemView.findViewById(R.id.username);
            o = itemView.findViewById(R.id.onlineStat);
            this.onLongBtn = onLongBtn;
            itemView.setOnLongClickListener(this);

        }

        @Override
        public boolean onLongClick(View v) {
            onLongBtn.onItemLongClick(getAdapterPosition());
            return true;
        }

//        @Override
//        public void onClick(View v) {
//            onSwipeBtn.onAddClick(getAdapterPosition());
//        }
    }

    public void removeItem(int position)
    {
        friendChatss.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public Filter getFilter() {
        if(chatsFilter == null)
        {
            filteredChatListfull.clear();
            filteredChatListfull.addAll(this.friendChatss);
            chatsFilter = new ChatListAdapter.MyFilter(this,filteredChatListfull);
        }
        return chatsFilter;
    }

    private static class MyFilter extends Filter {

        private final ChatListAdapter chatListAdapter;
        private  final ArrayList<ChatListClass> filteredChatList; //filter
        private final ArrayList<ChatListClass> friendChatssOri;

        private MyFilter(ChatListAdapter myAdapter, ArrayList<ChatListClass>friendChatss) {
            this.chatListAdapter= myAdapter;
            this.friendChatssOri = friendChatss;
            this.filteredChatList = new ArrayList<ChatListClass>();
        }
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            filteredChatList.clear();
            final FilterResults results = new FilterResults();
            if (charSequence.length() == 0){
                filteredChatList.addAll(friendChatssOri);
            }else {
                final String filterPattern = charSequence.toString().toLowerCase().trim();
                for ( ChatListClass listClass : friendChatssOri){
                    if (listClass.getDisplayname().toLowerCase().contains(filterPattern)){
                        filteredChatList.add(listClass);
                    }
                }
            }

            results.values = filteredChatList;
            results.count = filteredChatList.size();
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            chatListAdapter.friendChatss.clear();
            chatListAdapter.friendChatss.addAll((ArrayList<ChatListClass>)filterResults.values);
            chatListAdapter.notifyDataSetChanged();

        }
    }

//    private Filter discoverFilter = new Filter() {
//        @Override
//        protected FilterResults performFiltering(CharSequence charSequence) {
//            ArrayList<ChatListClass>filteredList = new ArrayList<>();
//
//            if(charSequence == null || charSequence.length() == 0){
//                filteredList.addAll(friendChatss);
//            }else {
//                String filterPattern = charSequence.toString().toLowerCase().trim();
//
//                for(ChatListClass listClass : friendChatss){
//                    if(listClass.getDisplayname().toLowerCase().contains(filterPattern)){
//                        filteredList.add(listClass);
//                    }
//                }
//            }
//            FilterResults results = new FilterResults();
//            results.values = filteredList;
//
//            return results;
//        }
//
//        @Override
//        protected void publishResults(CharSequence charSequence, FilterResults results) {
//            friendChatss.clear();
//            friendChatss.addAll((ArrayList) results.values);
//            notifyDataSetChanged();
//        }
//    };

    public interface OnLongBtnListener{
        void onItemLongClick(int position);
    }

}
