package com.app.pingchat.Chat;

public class Messages {
    private String CP_message;
    private String CP_mediaURL;
    private long CP_messageTimestamp;
    private String CP_mediaType;
    private String CP_messageSenderID;
    private String date;
    private String CP_mediaThumbnailURL;
    String name;
    String avatarurl;

    public Messages() {
    }

    public Messages(String CP_mediaURL,String CP_message, long CP_messageTimestamp, String CP_mediaType, String CP_messageSenderID, String date, String name, String avatarurl) {
        this.CP_message = CP_message;
        this.CP_messageTimestamp = CP_messageTimestamp;
        this.CP_mediaType = CP_mediaType;
        this.CP_messageSenderID = CP_messageSenderID;
        this.date = date;
        this.name = name;
        this.avatarurl = avatarurl;
        this.CP_mediaURL = CP_mediaURL;
    }

    public String getCP_message() {
        return CP_message;
    }

    public void setCP_message(String CP_message) {
        this.CP_message = CP_message;
    }

    public long getCP_messageTimestamp() {
        return CP_messageTimestamp;
    }

    public void setCP_messageTimestamp(long CP_messageTimestamp) {
        this.CP_messageTimestamp = CP_messageTimestamp;
    }

    public String getCP_mediaType() {
        return CP_mediaType;
    }

    public void setCP_mediaType(String CP_mediaType) {
        this.CP_mediaType = CP_mediaType;
    }

    public String getCP_messageSenderID() {
        return CP_messageSenderID;
    }

    public void setCP_messageSenderID(String CP_messageSenderID) {
        this.CP_messageSenderID = CP_messageSenderID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarurl() {
        return avatarurl;
    }

    public void setAvatarurl(String avatarurl) {
        this.avatarurl = avatarurl;
    }

    public String getCP_mediaURL() { return CP_mediaURL; }

    public void setCP_mediaURL(String CP_mediaURL) { this.CP_mediaURL = CP_mediaURL; }

    public String getCP_mediaThumbnailURL() { return CP_mediaThumbnailURL; }

    public void setCP_mediaThumbnailURL(String CP_mediaThumbnailURL) { this.CP_mediaThumbnailURL = CP_mediaThumbnailURL; }
}
