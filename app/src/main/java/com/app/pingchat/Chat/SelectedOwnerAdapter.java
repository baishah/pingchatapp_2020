package com.app.pingchat.Chat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pingchat.Discover.editDiscoverActivity;
import com.app.pingchat.Friend.userDetailsClass;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.Profile.ViewUserProfileActivity;
import com.app.pingchat.R;
import com.google.android.datatransport.Event;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.android.volley.VolleyLog.TAG;

public class SelectedOwnerAdapter extends RecyclerView.Adapter<SelectedOwnerAdapter.MyViewHolder>{
    FirebaseStorage storage;
    StorageReference storeRef;
    DatabaseReference usersRef,chat,chats;
    Context context;
    ArrayList<userDetailsClass> mContactslist;
    boolean checkState[];
    private String currentUser,chatId,userRole,currentusername;
    SelectedOwnerAdapter selectedOwnerAdapter;
    private ArrayList <String> list_of_member = new ArrayList();

    int row_index = -1;

    public ArrayList<String> itemArrayList = new ArrayList<>();

    public SelectedOwnerAdapter(ArrayList<userDetailsClass> Friendlist){
        mContactslist = Friendlist;
    }

    public SelectedOwnerAdapter(Context c, ArrayList<userDetailsClass> f, String y,String z){
        context = c;
        mContactslist = f;
        currentUser = y;
        chatId = z;
//        this.mclicklistener = clicklistener;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.select_friend_for_group_layout,parent,false);
        return new MyViewHolder(v);
        // return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.select_friend_for_group_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final SelectedOwnerAdapter.MyViewHolder holder, int position) {
        storage = FirebaseStorage.getInstance();
        storeRef = storage.getReference();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        chat = FirebaseDatabase.getInstance().getReference().child("Chats");
        chats = usersRef.child(currentUser).child("userConversations").child("Group");
        chats.keepSynced(true);

        final userDetailsClass mynewfriend = mContactslist.get(position);
        holder.u.setText(mynewfriend.getUD_displayName());
        holder.s.setText(mynewfriend.getUD_aboutMe());
        String userId = mynewfriend.getUD_userID();
        String name = mynewfriend.getUD_displayName();
        String avatar = mynewfriend.getUD_avatarURL();
        getMember(chatId);

        chat.child("Group").child(chatId).child("CG_groupMember").child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    System.out.println("not exist");
                } else {
                    String role = dataSnapshot.child("CG_role").getValue().toString();
                    if (role.equals("member")){
                        System.out.println("role3:"+role);
                        holder.p.setVisibility(View.GONE);
                    }
                    else if(role.equals("admin")){
                        holder.p.setText("Admin");
                        System.out.println("role2:"+role);
                        holder.p.setVisibility(View.VISIBLE);
                    }
                    else{
                        holder.p.setText("Owner");
                        holder.p.setVisibility(View.VISIBLE);
                        System.out.println("role1:"+role);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if(mynewfriend.getUD_avatarURL().startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(mynewfriend.getUD_avatarURL())
                    .into(holder.i);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(mynewfriend.getUD_avatarURL());
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(holder.i);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userId.equals(currentUser)){
                    System.out.println("this is you");
                }else{
                    chat.child("Group").child(chatId).child("CG_groupMember").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (!dataSnapshot.exists()) {
                                System.out.println("not exist");
                            } else {
                                final userDetailsClass userDetailsClass = new userDetailsClass();
                                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                    String memberId = dataSnapshot2.getKey();
                                    userDetailsClass.setUD_userID(memberId);
                                    if (memberId.equals(currentUser)) {
                                        String role = dataSnapshot2.child("CG_role").getValue().toString();
                                        if (role.equals("owner")) {
                                            System.out.println("role_current_user:"+role);
                                            String role_clicked = dataSnapshot.child(userId).child("CG_role").getValue().toString();
                                            System.out.println("role_clicked:"+role_clicked);
                                            if(role_clicked.equals("member")){
                                                CharSequence option[] = new CharSequence[]
                                                        {
                                                                "View Profile",
                                                                "Set as Admin",
                                                                "Remove user",
                                                                "Cancel"
                                                        };
                                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                                            final AlertDialog alertDialog = builder.create();
                                                builder.setTitle("Menu");
                                                builder.setItems(option, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        if (i == 0) {
                                                            System.out.println("profile");
                                                            if (mynewfriend.getUD_userID().equals(currentUser)) {
                                                                Intent j = new Intent(context, MainActivity.class);
                                                                j.putExtra("id", "profile");
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            } else {
                                                                Intent j = new Intent(context, ViewUserProfileActivity.class);
                                                                j.putExtra("id", mynewfriend.getUD_userID());
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            }
                                                        } else if (i == 1) {
                                                            System.out.println("admin");
//                                                        alertDialog.dismiss();
                                                            chat.child("Group").child(chatId).child("CG_groupMember").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                    if (dataSnapshot.hasChild("CG_role")) {
                                                                        String role = dataSnapshot.child("CG_role").getValue().toString();
                                                                        System.out.println("role:" + role);
                                                                        if (role.equals("member")) {
                                                                            openPopup(userId, name, avatar, chatId);
                                                                        } else {
                                                                            Toast.makeText(context, "This user is already an admin", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                }
                                                            });
                                                        } else if (i == 2) {
                                                            System.out.println("remove");
                                                            System.out.println("remove_user_clicked"+userId);
                                                            removeUser(userId, name, avatar, chatId);

                                                        }
                                                    }
                                                });
                                                builder.show();
                                            }else if(role_clicked.equals("admin")){
                                                System.out.println("role:admin");
                                                CharSequence option[] = new CharSequence[]
                                                        {
                                                                "View Profile",
                                                                "Set as Member",
                                                                "Remove User",
                                                                "Cancel"
                                                        };
                                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                                            final AlertDialog alertDialog = builder.create();
                                                builder.setTitle("Menu");
                                                builder.setItems(option, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        if (i == 0) {
                                                            System.out.println("profile");
                                                            if (mynewfriend.getUD_userID().equals(currentUser)) {
                                                                Intent j = new Intent(context, MainActivity.class);
                                                                j.putExtra("id", "profile");
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            } else {
                                                                Intent j = new Intent(context, ViewUserProfileActivity.class);
                                                                j.putExtra("id", mynewfriend.getUD_userID());
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            }
                                                        } else if (i == 1) {
                                                            System.out.println("demoted");
                                                            chat.child("Group").child(chatId).child("CG_groupMember").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                    if (dataSnapshot.hasChild("CG_role")) {
                                                                        String role = dataSnapshot.child("CG_role").getValue().toString();
                                                                        System.out.println("role:" + role);
                                                                        if (role.equals("admin")) {
                                                                            demotedUser(userId, name, avatar, chatId);
                                                                        } else {
                                                                            Toast.makeText(context, "This user is already a member", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                }
                                                            });
                                                        }else if(i == 2){
                                                            System.out.println("remove");
                                                            removeUser(userId, name, avatar, chatId);
                                                        }
                                                    }
                                                });
                                                builder.show();
                                            }
                                        } else if (role.equals("admin")) {
                                            String role_clicked = dataSnapshot.child(userId).child("CG_role").getValue().toString();
                                            System.out.println("role_clicked:"+role_clicked);
                                            if(role_clicked.equals("member")){
                                                CharSequence option[] = new CharSequence[]
                                                        {
                                                                "View Profile",
                                                                "Set as Admin",
                                                                "Remove User",
                                                                "Cancel"
                                                        };
                                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                                            final AlertDialog alertDialog = builder.create();
                                                builder.setTitle("Menu");
                                                builder.setItems(option, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        if (i == 0) {
                                                            System.out.println("profile");
                                                            if (mynewfriend.getUD_userID().equals(currentUser)) {
                                                                Intent j = new Intent(context, MainActivity.class);
                                                                j.putExtra("id", "profile");
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            } else {
                                                                Intent j = new Intent(context, ViewUserProfileActivity.class);
                                                                j.putExtra("id", mynewfriend.getUD_userID());
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            }
                                                        } else if (i == 1) {
                                                            System.out.println("admin");
//                                                        alertDialog.dismiss();
                                                            chat.child("Group").child(chatId).child("CG_groupMember").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                    if (dataSnapshot.hasChild("CG_role")) {
                                                                        String role = dataSnapshot.child("CG_role").getValue().toString();
                                                                        System.out.println("role:" + role);
                                                                        if (role.equals("member")) {
                                                                            openPopup(userId, name, avatar, chatId);
                                                                        } else {
                                                                            Toast.makeText(context, "This user is already an admin", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                }
                                                            });
                                                        } else if (i == 2) {
                                                            System.out.println("remove");
                                                            removeUser(userId, name, avatar, chatId);
                                                        }
                                                    }
                                                });
                                                builder.show();
                                            }else if(role_clicked.equals("admin")){
                                                System.out.println("role:admin");
                                                CharSequence option[] = new CharSequence[]
                                                        {
                                                                "View Profile",
                                                                "Cancel"
                                                        };
                                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                                            final AlertDialog alertDialog = builder.create();
                                                builder.setTitle("Menu");
                                                builder.setItems(option, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        if (i == 0) {
                                                            System.out.println("profile");
                                                            if (mynewfriend.getUD_userID().equals(currentUser)) {
                                                                Intent j = new Intent(context, MainActivity.class);
                                                                j.putExtra("id", "profile");
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            } else {
                                                                Intent j = new Intent(context, ViewUserProfileActivity.class);
                                                                j.putExtra("id", mynewfriend.getUD_userID());
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            }
                                                        }
                                                    }
                                                });
                                                builder.show();
                                            }else {
                                                CharSequence option[] = new CharSequence[]
                                                        {
                                                                "View Profile",
                                                                "Cancel"
                                                        };
                                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                                builder.setTitle("Menu");
                                                builder.setItems(option, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        if (i == 0) {
                                                            System.out.println("profile1");
                                                            if (mynewfriend.getUD_userID().equals(currentUser)) {
                                                                Intent j = new Intent(context, MainActivity.class);
                                                                j.putExtra("id", "profile");
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            } else {
                                                                Intent j = new Intent(context, ViewUserProfileActivity.class);
                                                                j.putExtra("id", mynewfriend.getUD_userID());
                                                                j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                context.startActivity(j);
                                                            }
                                                        }
                                                    }
                                                });
                                                builder.show();
                                            }
                                        } else {
                                            CharSequence option[] = new CharSequence[]
                                                    {
                                                            "View Profile",
                                                            "Cancel"
                                                    };
                                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                            builder.setTitle("Menu");
                                            builder.setItems(option, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    if (i == 0) {
                                                        System.out.println("profile1");
                                                        if (mynewfriend.getUD_userID().equals(currentUser)) {
                                                            Intent j = new Intent(context, MainActivity.class);
                                                            j.putExtra("id", "profile");
                                                            j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            context.startActivity(j);
                                                        } else {
                                                            Intent j = new Intent(context, ViewUserProfileActivity.class);
                                                            j.putExtra("id", mynewfriend.getUD_userID());
                                                            j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            context.startActivity(j);
                                                        }
                                                    }
                                                }
                                            });
                                            builder.show();
                                        }
                                    }
                                }
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });




//        if(sparseBooleanArray.get(position)){
//            holder.done.setVisibility(View.VISIBLE);
//            holder.selected.setVisibility(View.VISIBLE);
//        }
//        else{
//            holder.done.setVisibility(View.INVISIBLE);
//            holder.selected.setVisibility(View.INVISIBLE);
//        }

    }

    @Override
    public int getItemCount() {
        return mContactslist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView done,selected;
        TextView u,s,p;
        CircleImageView i;
        Button add;
        public MyViewHolder(View itemView) {
            super(itemView);

            u = itemView.findViewById(R.id.line1);
            s = itemView.findViewById(R.id.line2);
            p = itemView.findViewById(R.id.line3);
            i = itemView.findViewById(R.id.gambar);
            done = itemView.findViewById(R.id.bg_done);
            selected = itemView.findViewById(R.id.selected);
//            itemView.setOnClickListener(this);

        }
    }

    public void filterList(ArrayList<userDetailsClass> filteredlist){
        mContactslist = filteredlist;
        notifyDataSetChanged();
    }

    public void removeUser(String userID, String name, String avatar,String chatID){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String new_messageid = chat.push().push().getKey();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = LayoutInflater.from(context).inflate(R.layout.set_admin, null);

        final ImageView image1 = view.findViewById(R.id.pro_pic);
        final TextView okay = view.findViewById(R.id.unfriend_txt);
        final TextView user = view.findViewById(R.id.un_friend);
        final TextView removetxt = view.findViewById(R.id.text1);

        removetxt.setText("Remove this user?");

        user.setText(name);

        if(avatar.startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(avatar)
                    .into(image1);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(image1);
        }

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Remove this user?");
                HashMap group_chat = new HashMap();
                group_chat.put("CG_message",name+" removed");
                group_chat.put("CG_messageSenderID",currentUser);
                group_chat.put("CG_mediaType","status");
                group_chat.put("CG_messageTimestamp",timestamp.getTime());
                chat.child("Group").child(chatID).child("CG_groupChatSession").child(new_messageid).setValue(group_chat).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        chat.child("Group").child(chatID).child("CG_groupMember").child(userID).removeValue();
                        usersRef.child(userID).child("userConversations").child("Group").child(chatID).removeValue();
                        getMember(chatID);
                        chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessage").setValue(name+ " removed");
                        chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
                        chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
                        chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageSenderID").setValue(currentUser);
                        for(String i:list_of_member){
                            usersRef.child(i).child("userConversations").child("Group").child(chatID).child("UCG_lastMessageID").setValue(new_messageid);
//                                                          users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
                        }

                        Toast.makeText(context, " Removed..", Toast.LENGTH_SHORT).show();
//                        Toast.makeText(context, name+" left..", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
//                        Intent i = new Intent(context, MainActivity.class);
//                        i.putExtra("completedgroup", "chats");
//                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(i);
                    }
                });
            }
        });

        alertDialog.setView(view);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);

    }

    public void demotedUser(String userID, String name, String avatar,String chatID){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String new_messageid = chat.push().push().getKey();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = LayoutInflater.from(context).inflate(R.layout.set_admin, null);

        final ImageView image1 = view.findViewById(R.id.pro_pic);
        final TextView okay = view.findViewById(R.id.unfriend_txt);
        final TextView user = view.findViewById(R.id.un_friend);
        final TextView settxt = view.findViewById(R.id.text1);

        settxt.setText("Demote to member?");

        user.setText(name);

        if(avatar.startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(avatar)
                    .into(image1);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(image1);
        }

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("set as member");
                chat.child("Group").child(chatId).child("CG_groupMember").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild("CG_role")) {
                            String role = dataSnapshot.child("CG_role").getValue().toString();
                            System.out.println("role:" + role);
                            if(role.equals("admin")){
                                chat.child("Group").child(chatId).child("CG_groupMember").child(userID).child("CG_role").setValue("member");
                                usersRef.child(userID).child("userConversations").child("Group").child(chatID).child("UCG_role").setValue("member");
                                Toast.makeText(context, "Success..", Toast.LENGTH_SHORT).show();
//                                HashMap group_chat = new HashMap();
//                                group_chat.put("CG_message",name+" demoted");
//                                group_chat.put("CG_messageSenderID",currentUser);
//                                group_chat.put("CG_mediaType","status");
//                                group_chat.put("CG_messageTimestamp",timestamp.getTime());

//                                chat.child("Group").child(chatID).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);
//                                chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessage").setValue(name+" demoted");
//                                chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
//                                chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
//                                for(String i:list_of_member){
//                                    usersRef.child(i).child("userConversations").child("Group").child(chatID).child("UCG_lastMessageID").setValue(new_messageid);
////                                    users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
//                                }
                                alertDialog.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        alertDialog.setView(view);
//                                                        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
    }

    public void openPopup(String userID, String name, String avatar,String chatID){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String new_messageid = chat.push().push().getKey();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = LayoutInflater.from(context).inflate(R.layout.set_admin, null);

        final ImageView image1 = view.findViewById(R.id.pro_pic);
        final TextView okay = view.findViewById(R.id.unfriend_txt);
        final TextView user = view.findViewById(R.id.un_friend);

        user.setText(name);

        if(avatar.startsWith("h")){
            GlideApp.with(context.getApplicationContext())
                    .load(avatar)
                    .into(image1);
        }
        else{
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
            GlideApp.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(image1);
        }

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("set as admin");
                chat.child("Group").child(chatId).child("CG_groupMember").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild("CG_role")) {
                            String role = dataSnapshot.child("CG_role").getValue().toString();
                            System.out.println("role:" + role);
                            if(role.equals("member")){
                                chat.child("Group").child(chatId).child("CG_groupMember").child(userID).child("CG_role").setValue("admin");
                                usersRef.child(userID).child("userConversations").child("Group").child(chatID).child("UCG_role").setValue("admin");
                                Toast.makeText(context, "Success..", Toast.LENGTH_SHORT).show();
//                                HashMap group_chat = new HashMap();
//                                group_chat.put("CG_message",name+" promoted");
//                                group_chat.put("CG_messageSenderID",currentUser);
//                                group_chat.put("CG_mediaType","status");
//                                group_chat.put("CG_messageTimestamp",timestamp.getTime());
//
//                                chat.child("Group").child(chatID).child("CG_groupChatSession").child(new_messageid).setValue(group_chat);
//                                chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessage").setValue(name+" promoted");
//                                chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageTimestamp").setValue(timestamp.getTime());
//                                chat.child("Group").child(chatID).child("CG_groupDetails").child("CG_lastMessageID").setValue(new_messageid);
//                                for(String i:list_of_member){
//                                    usersRef.child(i).child("userConversations").child("Group").child(chatID).child("UCG_lastMessageID").setValue(new_messageid);
////                                    users.child(i).child("userConversations").child("Group").child(chatid).child("UCG_lastSeenMessageID").setValue(new_messageid);
//                                }
                                alertDialog.dismiss();



                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        alertDialog.setView(view);
//                                                        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
    }

    public ArrayList getMember(String chatid){
        chat.child("Group").child(chatid).child("CG_groupMember").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    String member = dataSnapshot1.getKey();
                    list_of_member.add(member);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        return  list_of_member;
    }

}
