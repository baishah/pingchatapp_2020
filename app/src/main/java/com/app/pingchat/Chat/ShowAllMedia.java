package com.app.pingchat.Chat;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.app.pingchat.Friend.AddNewFriendFragment;
import com.app.pingchat.Friend.FriendRequestFragment;
import com.app.pingchat.Friend.FriendsFragment;
import com.app.pingchat.GlideApp;
import com.app.pingchat.Main.TabAccessorAdapter;
import com.app.pingchat.PagerAdapterRewards;
import com.app.pingchat.R;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;

public class ShowAllMedia extends AppCompatActivity {

    private String chatidintent, key,uid;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabAccessorAdapter tabAccessorAdapter;
    private TextView nameU;
    private Button backbtn;
    private DatabaseReference users,chat;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_media_home);

        users = FirebaseDatabase.getInstance().getReference().child("Users");
        users.keepSynced(true);
        chat = FirebaseDatabase.getInstance().getReference().child("Chats");

        chatidintent = getIntent().getStringExtra("chatId");
        key = getIntent().getStringExtra("key");
        uid = getIntent().getStringExtra("uid");
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        System.out.println("chatid:"+chatidintent);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("chatid", chatidintent);
        editor.putString("key", key);
        editor.commit();

        tabLayout = findViewById(R.id.tab_layout1);
        viewPager = findViewById(R.id.pager1);
        nameU = findViewById(R.id.namaUser);
        backbtn = findViewById(R.id.backBtn);


        if(key.equals("personal")){
            displayName(uid);
        }else{
            displayNameG(chatidintent);
        }

        setupViewPager(viewPager);
        tabAccessorAdapter = new TabAccessorAdapter(getSupportFragmentManager());

        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3271DB"));
        tabLayout.setTabTextColors(Color.parseColor("#979797"),Color.parseColor("#3271DB"));
        tabLayout.setupWithViewPager(viewPager);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void displayName(String uid) {
        users.child(uid).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if ((dataSnapshot.exists())) {
                    if(dataSnapshot.hasChild("UD_displayName")){
                        String gname = dataSnapshot.child("UD_displayName").getValue().toString();
                        nameU.setText(gname);
//                        userName_.setSelection(userName_.getText().length());
                        System.out.println("name:"+gname);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
    private void displayNameG(String chatidintent) {
        chat.child("Group").child(chatidintent).child("CG_groupDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if ((dataSnapshot.exists())) {
                    if(dataSnapshot.hasChild("CG_groupName")){
                        String gname = dataSnapshot.child("CG_groupName").getValue().toString();
                        nameU.setText(gname);
//                        groupName.setSelection(groupName.getText().length());
                        System.out.println("group name:"+gname);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void setupViewPager(ViewPager viewPager){
        TabAccessorAdapter adapter = new TabAccessorAdapter(getSupportFragmentManager());
        adapter.addFragment(new ImageFragment(), "Image");
        adapter.addFragment(new VideoFragment(), "Video");
        adapter.addFragment(new pdfFragment(), "Docs");
        viewPager.setAdapter(adapter);
    }
}
