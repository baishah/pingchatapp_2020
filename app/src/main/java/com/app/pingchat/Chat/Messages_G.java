package com.app.pingchat.Chat;

public class Messages_G {
    private String CG_message;
    private long CG_messageTimestamp;
    private String CG_mediaType;
    private String CG_messageSenderID;
    private String date;
    private String CG_mediaURL;
    private String CG_mediaThumbnailURL;
    String name;
    String avatarurl;

    public Messages_G() {
    }

    public Messages_G(String CG_mediaURL,String CP_message, long CP_messageTimestamp, String CP_mediaType, String CP_messageSenderID, String date, String name, String avatarurl,String CG_mediaThumbnailURL) {
        this.CG_message = CP_message;
        this.CG_messageTimestamp = CP_messageTimestamp;
        this.CG_mediaType = CP_mediaType;
        this.CG_messageSenderID = CP_messageSenderID;
        this.date = date;
        this.name = name;
        this.avatarurl = avatarurl;
        this.CG_mediaURL = CG_mediaURL;
        this.CG_mediaThumbnailURL = CG_mediaThumbnailURL;
    }

    public String getCG_message() {
        return CG_message;
    }

    public void setCG_message(String CG_message) {
        this.CG_message = CG_message;
    }

    public long getCG_messageTimestamp() {
        return CG_messageTimestamp;
    }

    public void setCG_messageTimestamp(long CG_messageTimestamp) {
        this.CG_messageTimestamp = CG_messageTimestamp;
    }

    public String getCG_mediaType() {
        return CG_mediaType;
    }

    public void setCG_mediaType(String CG_mediaType) {
        this.CG_mediaType = CG_mediaType;
    }

    public String getCG_messageSenderID() {
        return CG_messageSenderID;
    }

    public void setCG_messageSenderID(String CG_messageSenderID) {
        this.CG_messageSenderID = CG_messageSenderID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarurl() {
        return avatarurl;
    }

    public void setAvatarurl(String avatarurl) {
        this.avatarurl = avatarurl;
    }

    public String getCG_mediaURL() { return CG_mediaURL; }

    public void setCG_mediaURL(String CG_mediaURL) { this.CG_mediaURL = CG_mediaURL; }

    public String getCG_mediaThumbnailURL() {
        return CG_mediaThumbnailURL;
    }

    public void setCG_mediaThumbnailURL(String CG_mediaThumbnailURL) {
        this.CG_mediaThumbnailURL = CG_mediaThumbnailURL;
    }
}
