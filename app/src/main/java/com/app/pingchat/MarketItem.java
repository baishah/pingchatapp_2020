package com.app.pingchat;

public class MarketItem {
    private String mImageUrl;
    private String mName;
    private String mPrice;
    private String mDesc;
    private String mPID;
    private String mPowner;

    public MarketItem() {
    }
//
//    public MarketItem(String pUrl, String pName, String pPrice, String pDesc){
//        mImageUrl = pUrl;
//        mName = pName;
//        mPrice = pPrice;
//        mDesc = pDesc;
//    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public void setmPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public void setmDesc(String mDesc) {
        this.mDesc = mDesc;
    }

    public String getmImageUrl(){
        return mImageUrl;
    }

    public String getmName(){
        return mName;
    }

    public String getmPrice(){
        return mPrice;
    }

    public String getmDesc(){
        return mDesc;
    }

    public String getmPID() {
        return mPID;
    }

    public void setmPID(String mPID) {
        this.mPID = mPID;
    }

    public String getmPowner() {
        return mPowner;
    }

    public void setmPowner(String mPowner) {
        this.mPowner = mPowner;
    }
}
