package com.app.pingchat;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Map.MapsFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.ar.core.Anchor;
import com.google.ar.core.Frame;
import com.google.ar.core.Pose;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.assets.RenderableSource;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import uk.co.appoly.arcorelocation.LocationScene;

public class ArActivityPartners extends AppCompatActivity {

    private static final String TAG = "ArActivity";
    private static final double MIN_OPENGL_VERSION = 3.0;

    ArFragment arFragment;
    public SensorManager sensorManager;
    double newLatitude;
    double newLongitude;

    private ViewRenderable viewRenderable;

//    double newLatitude = 6.009093;//tanjung lipat food court
//    double newLongitude =116.110744;

    double mMyLatitude;
    double mMyLongitude;

    private AnchorNode mAnchorNode;
    private DatabaseReference userRewards;
    private FirebaseAuth mauth;
    String model;
    private LocationScene locationScene;

    private String userid;

    @Override
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!checkDevice(this)) {
            return;
        }
        setContentView(R.layout.ar_activity);
        Intent intent = getIntent();
        mauth= FirebaseAuth.getInstance();
        userid = mauth.getCurrentUser().getUid();

//        String lat = intent.getStringExtra("latitude");
//        String lon = intent.getStringExtra("longitude");
        model = intent.getStringExtra("model");
//
//        newLatitude = Double.valueOf(lat);
//        newLongitude = Double.valueOf(lon);
        userRewards = FirebaseDatabase.getInstance().getReference().child("Users").child(userid).child("userRewards");

        if(!isNullOrEmpty(model)){
            mMyLatitude = MapsFragment.getInstance().lastlatitude;
            mMyLongitude = MapsFragment.getInstance().lastlongitude;

            double radiusInMeters = 100.0;
            float[] distance = new float[2];

            arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);

            //kena improve lagi sini supaya dia check beberapa second
        /*    Location.distanceBetween( mMyLatitude, mMyLongitude,newLatitude,newLongitude, distance);
            if( distance[0] >= radiusInMeters ){
                Log.e(TAG, "error!!!");
                arFragment.getPlaneDiscoveryController().hide();
                arFragment.getPlaneDiscoveryController().setInstructionView(null);
                Toast.makeText(this,"make sure you are within the radius", Toast.LENGTH_SHORT).show();
            } else {*/
                arFragment.getPlaneDiscoveryController().hide();
                arFragment.getPlaneDiscoveryController().setInstructionView(null);
                arFragment.getArSceneView().getScene().addOnUpdateListener(updatelistener);
//                Toast.makeText(this,"latitude: "+mMyLatitude+" longitude: "+mMyLongitude+ "\nGoal Lat:" +newLatitude + "\tGoal Long:" +newLongitude + "\tDis:"+radiusInMeters, Toast.LENGTH_SHORT).show();
//            }
        }
//        else{
//            Toast.makeText(this,"\n" + "Business not yet approved", Toast.LENGTH_SHORT).show();
//        }

    }

    private Scene.OnUpdateListener updatelistener = frameTime -> {
        Frame frame = arFragment.getArSceneView().getArFrame();

        if(frame == null) {
            return;
        }

        if(arFragment.getArSceneView().getArFrame().getCamera().getTrackingState()!=TrackingState.TRACKING){
            System.out.println("camera x track apa2");
            return;
        }

//        if(locationScene == null){
//            locationScene = new LocationScene(this,this,arFragment.getArSceneView());
//            LocationMarker layoutlocationmarker = new LocationMarker(mMyLatitude,mMyLongitude,addObjectModel(model);)
//        }
        if(mAnchorNode == null){
            addObjectModel(Uri.parse(model));
            Log.e(TAG, "mana!!!");
            removeUpdateListener();
        }

    };

    private void addObjectModel(Uri object) {
        Frame frame = arFragment.getArSceneView().getArFrame();

        arFragment.getArSceneView().getScene()
                .removeOnUpdateListener(updatelistener);

        if(frame != null) {
            Vector3 cameraPos = arFragment.getArSceneView().getScene().getCamera().getWorldPosition();
            Vector3 cameraForward = arFragment.getArSceneView().getScene().getCamera().getForward();
            Vector3 position = Vector3.add(cameraPos, cameraForward.scaled(3.0f));

            Pose pose = Pose.makeTranslation(position.x, position.y, position.z);
            Anchor anchor = arFragment.getArSceneView().getSession().createAnchor(pose);
            placeObject(anchor,object);
        }
    }

    private void addNodeToScene(Anchor createAnchor, ModelRenderable renderable, Uri object) {
        AnchorNode anchorNode = new AnchorNode(createAnchor);
        RotatingNode rotatingNode =  new RotatingNode(true, 30.0f);
        TransformableNode transformableNode = new TransformableNode(arFragment.getTransformationSystem());
        rotatingNode.setRenderable(renderable);
        rotatingNode.addChild(transformableNode);
        rotatingNode.setParent(anchorNode);
        Log.e(TAG, "kluaR!!!");
        arFragment.getArSceneView().getScene().addChild(anchorNode);
        anchorNode.setOnTapListener((hitTestResult, motionEvent) -> {
            String titles = "Eureka";
            String key = userRewards.push().getKey();
            HashMap rewardredeemed = new HashMap();
            rewardredeemed.put("R_ID", key);
            rewardredeemed.put("R_status", true);
            rewardredeemed.put("R_Timestamp", ServerValue.TIMESTAMP);
            rewardredeemed.put("R_Title", "Eureka");
            rewardredeemed.put("R_Description", "Minuman Bertenaga Setiap Masa Di Mana Jua");

            userRewards.child("EUREKA").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        for(DataSnapshot dataSnapshot1 :dataSnapshot.getChildren()){
                            String title = dataSnapshot1.child("R_Title").getValue().toString();
                            if(!title.equals(titles)){
                                userRewards.child("EUREKA").child(key).setValue(rewardredeemed).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Toast.makeText (ArActivityPartners.this, "Coupon claimed", Toast.LENGTH_SHORT).show();
//                                            rotatingNode.onDeactivate();
                                        }
                                    }
                                });
                            }
                            else{
                                Toast.makeText(ArActivityPartners.this, "Coupon already claimed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else{
                        userRewards.child("EUREKA").child(key).setValue(rewardredeemed).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText (ArActivityPartners.this, "Coupon claimed", Toast.LENGTH_SHORT).show();
//                                    rotatingNode.onDeactivate();
                                }
                            }
                        });
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
        });
//        transformableNode.setOnTapListener((hitTestResult, motionEvent) -> {
//            Toast.makeText(ArActivityPartners.this, "3D model tapped ", Toast.LENGTH_SHORT).show();
//            System.out.println("3D model tapped");
//
//            ViewRenderable.builder()
//                    .setView(this, R.layout.imgboard)
//                    .build()
//                    .thenAccept(renderable2 ->
//                    {viewRenderable = renderable2;
//                        ImageView i = (ImageView) renderable2.getView();
//                        i.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Toast.makeText(getApplicationContext(), "coupon selected", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    });
//        });
        transformableNode.select();
    }

    private void placeObject(Anchor anchor, Uri object) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ModelRenderable.builder()
                    .setSource(this, RenderableSource.builder().setSource(this, object, RenderableSource.SourceType.GLTF2).build())
                    .setRegistryId(object)
                    .build()
                    .thenAccept(modelRenderable -> addNodeToScene(anchor, modelRenderable, object))
                    .exceptionally(throwable -> null);
        }
    }

    private Point getScreenCenter() {

        if(arFragment == null || arFragment.getView() == null) {
            return new Point(0,0);
        }

        int w = arFragment.getView().getWidth()/2;
        int h = arFragment.getView().getHeight()/2;
        return new Point(w, h);
    }

    private void removeUpdateListener() {
        arFragment.getArSceneView().getScene().removeOnUpdateListener(updatelistener);
    }

    public boolean checkDevice(final Activity activity) {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {

            Log.e(TAG, "Sceneform requires Android N or higher");
            Toast.makeText(ArActivityPartners.this, "This Device is not support ARcore ", Toast.LENGTH_SHORT).show();
            //            activity.finish();
            return false;
        }
        String openGlVersionString =
                ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Requires OpenGL ES 3.0 or higher");
            activity.finish();
            return false;
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) == null) {
            //                actionEvent.setText("GYROSCOPE supports");
            Toast.makeText(ArActivityPartners.this, "No Gyroscope Support ", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.isEmpty())
            return false;
        return true;
    }

    @Override
    public void onBackPressed(){
        finish();
    }
}

