package com.app.pingchat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.app.pingchat.Main.MainActivity;
import com.app.pingchat.Profile.ViewProfileActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;

public class bottomMenuProfile extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;
    RelativeLayout edit_pro,setting,blockuser;
    private DatabaseReference users,friend;
    FirebaseAuth mAuth;
    String currentuser;
    Button setting1,edit_pro1,blockuser1;

    public void setListener(BottomSheetListener listener) {
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_menu_layout, container, false);

        edit_pro = v.findViewById(R.id.edit_profil);
        setting = v.findViewById(R.id.setting_pr);
        blockuser = v.findViewById(R.id.block_btn);
        setting1 = v.findViewById(R.id.setting_pr1);
        edit_pro1 = v.findViewById(R.id.edit_profil1);
        blockuser1 = v.findViewById(R.id.block_btn1);

        mAuth = FirebaseAuth.getInstance();
        currentuser = mAuth.getCurrentUser().getUid();

        users = FirebaseDatabase.getInstance().getReference().child("Users");
        users.keepSynced(true);
        friend = FirebaseDatabase.getInstance().getReference().child("Users").child(currentuser).child("userFriends");
        friend.keepSynced(true);

        String key = getArguments().getString("key");
        String friendid = getArguments().getString("friend_id");

        edit_pro1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivity(intent);
                dismiss();
            }
        });

        setting1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Settings2Fragment.class);
                startActivity(intent);
                dismiss();
            }
        });

        blockuser1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockUser(friendid);
            }
        });

        if(key.equals("c")){
            edit_pro.setVisibility(View.VISIBLE);
            setting.setVisibility(View.VISIBLE);
            blockuser.setVisibility(View.GONE);
        }else{
            edit_pro.setVisibility(View.GONE);
            setting.setVisibility(View.GONE);
            blockuser.setVisibility(View.VISIBLE);
        }

        return v;
    }

    public void blockUser(String id){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog = builder.create();

        View view = LayoutInflater.from(getContext()).inflate(R.layout.unfriends_popup_layout,null);

        TextView friendname = view.findViewById(R.id.friend_name);
        TextView cancelBtn = view.findViewById(R.id.cancel);
        TextView unfriendBtn = view.findViewById(R.id.unfriend_txt);
        CircleImageView avatarimg = view.findViewById(R.id.pro_pic);

        users.child(id).child("userDetails").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild("UD_displayName")){
                    String name = dataSnapshot.child("UD_displayName").getValue().toString();
                    friendname.setText(name);
                    System.out.println("name :"+name);
                }

                if(dataSnapshot.hasChild("UD_avatarURL")){
                    String avatar = dataSnapshot.child("UD_avatarURL").getValue().toString();
                    System.out.println("url :"+ avatar);
                    if(avatar.startsWith("p")){
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(avatar);
                        GlideApp.with(getContext()).load(storageReference).placeholder(R.drawable.loading_img).into(avatarimg);
                    }
                    else if(avatar.startsWith("h")){
                        GlideApp.with(getContext()).load(avatar).placeholder(R.drawable.loading_img).into(avatarimg);
                    }
                }

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                unfriendBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        confirmBlock(id);
//                       contct_icon.setVisibility(View.GONE);
//                       unfrenbtn.setVisibility(View.GONE);
//                       unblock_btn.setVisibility(View.VISIBLE);
//                       unblock_txt.setVisibility(View.VISIBLE);
//                       users.child(currentuser).child("userConversations").child()
                    }
                });



            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        return;
    }

    private void confirmBlock(String id) {
        ProgressDialog dialog= new ProgressDialog(getContext());
        dialog.setMessage("Block friend.. Please wait");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        friend.child(id).setValue("block").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                users.child(id).child("userFriends").child(currentuser).setValue("blocked").addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        dialog.dismiss();
                        Toast.makeText(getContext(), "Blocked successfully", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getContext(), MainActivity.class));

                    }
                });
            }
        });
    }


    public interface BottomSheetListener {
        void onProfilemenu(String text);
    }
}
