package com.app.pingchat;

public class partnerDetailsClass {

    private String P_partnerID;

    private String P_partnerName;
    private String UPD_partnerID;
    private String UPD_eligibility;
    private String UPD_role;
    private String UPD_verification;
    private String P_partnerPhone;
    private String P_partnerEmail;
    private String P_partnerAddress;
    private String P_partnerCategory;
    private String P_profileURL;
    private String P_partnerLatitude;
    private String P_partnerLongitude;
    private String P_partnerMediaID;
    private String P_partnerMediaType;
    private String P_partnerMediaURL;

    private long P_partnerCreatedOn;

    public partnerDetailsClass(){};

    public partnerDetailsClass(String P_partnerID, String UPD_partnerID, String P_partnerName, long P_partnerCreatedOn, String P_partnerPhone, String P_partnerEmail, String P_partnerAddress, String P_partnerCategory, String P_profileURL, String P_partnerRole, String P_partnerJoined, String UPD_verification,String UPD_role, String UPD_eligibility) {
        this.P_partnerID = P_partnerID;
        this.UPD_partnerID = UPD_partnerID;
        this.P_partnerName = P_partnerName;
        this.P_partnerCreatedOn = P_partnerCreatedOn;
        this.P_partnerPhone = P_partnerPhone;
        this.P_partnerEmail = P_partnerEmail;
        this.P_partnerAddress = P_partnerAddress;
        this.P_partnerCategory = P_partnerCategory;
        this.P_profileURL = P_profileURL;
        this.UPD_eligibility = UPD_eligibility;
        this.UPD_role = UPD_role;
        this.UPD_verification = UPD_verification;
    }

    public partnerDetailsClass(String P_partnerLatitude, String P_partnerLongitude) {
        this.P_partnerLatitude = P_partnerLatitude;
        this.P_partnerLongitude = P_partnerLongitude;
    }

    public partnerDetailsClass(String P_partnerMediaID, String P_partnerMediaType,String P_partnerMediaURL) {
        this.P_partnerMediaURL = P_partnerMediaURL;
        this.P_partnerMediaType = P_partnerMediaType;
        this.P_partnerMediaID = P_partnerMediaID;
    }

    public String getUPD_partnerID(){ return UPD_partnerID;}

    public void setUPD_partnerID(String UPD_partnerID){ this.UPD_partnerID = UPD_partnerID;}

    public String getP_partnerID() {
        return P_partnerID;
    }

    public void setP_partnerID(String P_partnerID) {
        this.P_partnerID = P_partnerID;
    }

    public String getP_partnerName() {
        return P_partnerName;
    }

    public void setP_partnerName(String P_partnerName) {
        this.P_partnerName = P_partnerName;
    }

    public long getP_partnerCreatedOn() {
        return P_partnerCreatedOn;
    }

    public void setP_partnerCreatedOn(long P_partnerCreatedOn) {
        this.P_partnerCreatedOn = P_partnerCreatedOn;
    }
    public String getP_partnerPhone() {
        return P_partnerPhone;
    }

    public void setP_partnerPhone(String P_partnerPhone) {
        this.P_partnerPhone = P_partnerPhone;
    }

    public String getP_partnerEmail() {
        return P_partnerEmail;
    }

    public void setP_partnerEmail(String P_partnerEmail) {
        this.P_partnerEmail = P_partnerEmail;
    }

    public String getP_partnerLatitude() {
        return P_partnerLatitude;
    }

    public void setP_partnerLatitude(String P_partnerLatitude) {
        this.P_partnerLatitude = P_partnerLatitude; }

    public String getP_partnerLongitude() {
        return P_partnerLongitude;
    }

    public void setP_partnerLongitude(String P_partnerLongitude) {
        this.P_partnerLongitude = P_partnerLongitude;
    }

    public String getP_partnerAddress() {
        return P_partnerAddress;
    }

    public void setP_partnerAddress(String P_partnerAddress) {
        this.P_partnerAddress = P_partnerAddress;
    }

    public String getP_partnerCategory() {
        return P_partnerCategory;
    }

    public void setP_partnerCategory(String P_partnerCategory) {
        this.P_partnerCategory = P_partnerCategory;
    }

    public String getP_profileURL() {
        return P_profileURL;
    }

    public void setP_profileURL(String P_profileURL) {
        this.P_profileURL = P_profileURL;
    }

    public String getP_partnerMediaURL() {
        return P_partnerMediaURL;
    }

    public void setP_partnerMediaURL(String P_partnerMediaURL) {
        this.P_partnerMediaURL = P_partnerMediaURL;
    }

    public String getP_partnerMediaType() {
        return P_partnerMediaType;
    }

    public void setP_partnerMediaType(String P_partnerMediaType) {
        this.P_partnerMediaType = P_partnerMediaType;
    }
    public String getP_partnerMediaID() {
        return P_partnerMediaID;
    }

    public void setP_partnerMediaID(String P_partnerMediaID) {
        this.P_partnerMediaID = P_partnerMediaID;
    }

    public String getUPD_verification() {
        return UPD_verification;
    }

    public void setUPD_verification(String UPD_verification) {
        this.UPD_verification = UPD_verification;
    }
}