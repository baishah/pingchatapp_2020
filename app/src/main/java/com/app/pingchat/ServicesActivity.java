package com.app.pingchat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.pingchat.Discover.NW_detailsClass;
import com.app.pingchat.Discover.NewsListAdapter;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ServicesActivity extends AppCompatActivity {

    private Button backbtn;
    ArrayList<Services> servicesList;
    RecyclerView service_rv;
    FirebaseAuth mAuth;
    String current_user;
    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services2);

        mAuth = FirebaseAuth.getInstance();
        current_user = mAuth.getCurrentUser().getUid();
        mQueue = Volley.newRequestQueue(this);


        service_rv = (RecyclerView)findViewById(R.id.service_btn);
        service_rv.setLayoutManager(new GridLayoutManager(this,3));

        getServices(current_user);

        backbtn = findViewById(R.id.backBtn);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    public void getServices(String c_user){
        servicesList = new ArrayList();
        servicesAdapter s_adapter = new servicesAdapter(getApplicationContext(), servicesList, c_user);
        service_rv.setAdapter(s_adapter);

        String url = "https://devbm.ml/services/list";

        final HashMap<Object, Object> postParams = new HashMap<Object, Object>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Volley", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("dataservice");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject value = jsonArray.getJSONObject(i);

                                String service_ID = value.getString("SD_serviceID");
                                long service_date = Long.parseLong(value.getString("SD_serviceStartDate"));
                                String serviceTitle = value.getString("SD_serviceTitle");
                                String sd_serviceURL = value.getString("SD_serviceURL");
                                String sd_serviceAddress = value.getString("SD_serviceAddress");
                                String sd_serviceDescription = value.getString("SD_serviceDescription");
                                String sl_longitude = value.getString("SL_longitude");
                                String sl_latitude = value.getString("SL_latitude");
                                String sm_mediaType = value.getString("SM_mediaType");
                                String sm_mediaURL = value.getString("SM_mediaURL");

                                Services services = new Services();

                                services.setSD_serviceID(service_ID);
                                services.setSD_serviceStartDate(service_date);
                                services.setSD_serviceTitle(serviceTitle);
                                services.setSD_serviceURL(sd_serviceURL);
                                services.setSD_serviceAddress(sd_serviceAddress);
                                services.setSD_serviceDescription(sd_serviceDescription);
                                services.setSL_longitude(sl_longitude);
                                services.setSL_latitude(sl_latitude);
                                services.setSM_mediaType(sm_mediaType);
                                services.setSM_mediaURL(sm_mediaURL);

                                servicesList.add(services);
                            }
//                            progressDialog.dismiss();
                            s_adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjReq);
    }

}
