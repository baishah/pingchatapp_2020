package com.app.pingchat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.app.pingchat.Main.MainActivity;

public class CompleteSendBugsReport extends AppCompatActivity {
    private Button backHome;
    private TextView sendAnother;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_layout_2);

        backHome = findViewById(R.id.back_home);
        sendAnother = findViewById(R.id.send_anothe);

        backHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        sendAnother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BugsReportActivity.class);
                startActivity(intent);
            }
        });
    }
}
